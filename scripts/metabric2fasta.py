#!env python
import csv
import sys

file = sys.argv[1]

ids = set()
data = {}
chr = {}
start = {}
end = {}
num = {}
dataexp = {}

with open(file, 'rb') as fd:
    reader = csv.DictReader(fd, delimiter='\t')
    count = 0
    for row in reader:
        id = row['METABRIC_ID']
        val = int(round(2 * 2**float(row['seg.mean'])))
        val = min(9, val) # limit max cn
        val = str(val)
        chrval = int(row['chrom'])
        #startval = int(row['loc.start'])
        #endval = int(row['loc.end'])
        numval = int(row['num.mark'])

        ids.add(id)
        if not data.has_key(id):
            data[id] = list()
            chr[id] = list()
            #start[id] = list()
            #end[id] = list()
            num[id] = list()
        
        if len(data[id])>0 and data[id][-1] == val and chr[id][-1] == chrval: # just increase size
            num[id][-1] += numval
        else:
            data[id].append(val)
            chr[id].append(chrval)
            #start[id].append(startval)
            #end[id].append(endval)
            num[id].append(numval)
        count+=1

if len(ids)==0:
    sys.exit(0)


length_limit = 0

datanew = {}
numnew = {}
while len(data[list(ids)[0]]) > 0:

    # find the smallest chunk
    sSize = float('inf')
    for id in ids:
        if sSize > num[id][0]: # new smallest
            sSize = num[id][0]

    # add data to new dataset
    for id in ids:
        if not datanew.has_key(id):
            datanew[id] = ""
            numnew[id] = list()
        if sSize > length_limit:
            datanew[id] += data[id][0]
            numnew[id].append(sSize)
        else:
            if len(numnew[id])==0:
                numnew[id].append(sSize)
            else:
                numnew[id][-1] += sSize

#    if len(datanew[list(ids)[0]]) != len(datanew[list(ids)[1]]):
#        print "bla"
        
    # reduce old dataset
    for id in ids:
        if sSize == num[id][0]:
            num[id].pop(0)
            data[id].pop(0)
        elif sSize < num[id][0]:
            num[id][0] = num[id][0] - sSize
        else:
            print "ERROR"
            break

for id in ids:
    print ">%s" % id
    print datanew[id]




    
        
