#!/bin/bash

if [ "$#" -ne 1 ]; then
    echo "USAGE: $0 <version>"
    exit
fi

ver=$1
folder="../release/alvis-$ver"

if [ -d $folder ]; then
    echo "Folder $folder exists, delete first!"
    exit
fi

cp -r ../dist $folder
cp -r ../data $folder
cp -r ../Rmodules $folder
cp -r ../examples $folder
cp -r ../doc $folder
cp ../README.md $folder
cp ../agpl.txt $folder
rm $folder/README.txt

cd ../release
zip -r -9 "alvis-$ver.zip" "alvis-$ver"


#tar --exclude=metabric --exclude=Yoder2014 -czvf /tmp/alvis.tgz ../dist
#scp /tmp/alvis.tgz rfs@ebi-001.ebi.ac.uk:~/public_html
#rm /tmp/alvis.tgz
