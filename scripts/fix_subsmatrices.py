#!/usr/local/bin/python

import sys
import os
import os.path
import csv

if len(sys.argv)<2:
    print "USAGE: " + sys.argv[0] + " <target_dir>";
    sys.exit()

dir=sys.argv[1]

files = [ f for f in os.listdir(dir) if os.path.isfile(dir+"/"+f) ]

for f in files:
    remove=False

    if f=="matrix_ids" or f[-4:]==".mod": 
        continue

    print f
    with open(dir+"/"+f, 'rb') as fd:
        lines = [l.rstrip("\n").rstrip() for l in fd]

    lines_mod = []
    for l in lines:
        add_whitespace=False
        entries=[]
        if len(l)==0: continue

        if l[0]=="#": #comment line
            entries = [l]
        elif l[0]=="*":
            continue
        else:
            if l[0]==" ":
                add_whitespace=True
            entries = l.split()
            if entries[-1]=="*":
                remove=True
            if remove:
                entries=entries[:-1]
        if add_whitespace:
            lines_mod.append("\t" + "\t".join(entries))
        else:
            lines_mod.append("\t".join(entries))

    with open(dir+"/"+f+".mod", "wb") as fd:
        fd.write("\n".join(lines_mod))
        
