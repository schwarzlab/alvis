# ALVIS - Explorative Analysis and Visualisation of Multiple Sequence Alignments #

ALVIS is a standalone Java application for the explorative analysis and visualisation of multiple sequence alignments (MSAs) for sequences of amino acids, nucleotides and user-defined alphabets.
ALVIS implements the #Sequence Bundles# visualisation technique, an alternative to sequence logos for MSAs which maintains horizontal dependencies.
ALVIS also includes the features for fisher scoring and correspondence analysis on MSAs formerly found in CAMA (https://academic.oup.com/nar/article/37/18/5959/1088889).

## History: ##
* 12/09/2020 ALVIS v0.2 maintenance and beta release; ALVIS now runs on JDK 14.
* 29/08/2020 Move of ALVIS repository to https://bitbucket.org/schwarzlab/alvis
* 05/05/2016 ALVIS v0.1 initial alpha release

## Installation: ##

* Download the ALVIS repository using `git clone https://bitbucket.org/schwarzlab/alvis/src/master/` or download the binary release from the downloads section.
* In case of the latter, unzip the archive to a folder of your choice and execute the JAR using `java -jar Alvis.jar`

## Requirements: ##

* Java 14
* OPTIONAL: R with a working rJava installation and the  "kernlab" package installed for some advanced features

## Troubleshooting: ##

* Should you encounter out-of-memory errors, start Alvis with the additional JVM command line switch -xMx4096 for 4GB of RAM (adjust to your system)
* Getting rJava to work can prove difficult. Please consult the rJava documentation for support, most Alvis features are available without it.

## Documentation ##

* A preliminary user guide is available inside the "doc" folder.
* A demonstration / tutorial video can be found here: https://vimeo.com/146710536

## License: ##

Copyright 2015-2020 - Roland Schwarz & Asif Tamuri

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License version 3 as published by the Free Software Foundation.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License version 3 for more details.
You should have received a copy of the GNU Affero General Public License version 3 along with this program. If not, see https://www.gnu.org/licenses/

## Useful Links: ##

* Try the web version of Sequence Bundles at: http://science-practice.com/projects/sequence-bundles/

## Please cite: ##

Nucl. Acids Res. (2016) doi: 10.1093/nar/gkw022 (http://nar.oxfordjournals.org/content/early/2016/01/26/nar.gkw022.full)  
**ALVIS: interactive non-aggregative visualization and explorative analysis of multiple sequence alignments**  
Roland F. Schwarz, Asif U. Tamuri, Marek Kultys, James King, James Godwin, Ana M. Florescu, Jörg Schultz and Nick Goldman  

and:  

BMC Proc. (2014) (http://bmcproc.biomedcentral.com/articles/10.1186/1753-6561-8-S2-S8)  
**Sequence Bundles: a novel method for visualising, discovering and exploring sequence motifs**  
Marek Kultys, Lydia Nicholas, Roland Schwarz, Nick Goldman and James King  


For questions and bug reports, please contact me at

roland@schwarzlab.de

September 2020, Roland Schwarz