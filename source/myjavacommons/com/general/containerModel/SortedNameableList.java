

package com.general.containerModel;

import java.util.*;



/**
 * <p>Title: YANA - Yet Another Network Analyzer</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Dep. of Bioinformatics, University Wuerzburg</p>
 *
 * @author Roland Schwarz
 * @version 0.8
 */
public class SortedNameableList<T extends Nameable> extends ArrayList<T> implements FastNameSearch<T>{
  private static final Comparator c=new NameableComparator();

  public SortedNameableList() {
    super();
  }

  public SortedNameableList(int capacity) {
    super(capacity);
  }

  public T findByName(String name) {
    int index=Collections.binarySearch(this,name,c);
    if (index<0)
      return null;

    return get(index);
  }

  public boolean add(T o) {
    int index=Collections.binarySearch(this,o,c);
    if (index<0)
      index=-index-1; // see api docs

    super.add(index,o);
    return true;
  }

  public void add(int index, T element) {
    throw new UnsupportedOperationException();
  }

  public boolean addAll(Collection<? extends T> c) {
    boolean retval=false;
    for (T element:c)
      retval |= add(element);

    return retval;
  }

  public boolean addAll(int index, Collection<? extends T> c) {
    throw new UnsupportedOperationException();
  }


}
