package com.general.containerModel;

/**
 * <p>Title: YANA - Yet Another Network Analyzer</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Dep. of Bioinformatics, University Wuerzburg</p>
 *
 * @author not attributable
 * @version 0.8
 */
public interface ListenerAdapter<E> {
  public void setCallback(ListenableCollection<E> callback);
  public void addItemListener(Iterable<E> items);
  public void removeItemListener(Iterable<E> items);
}
