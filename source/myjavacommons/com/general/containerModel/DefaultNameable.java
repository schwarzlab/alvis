/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.general.containerModel;

import java.io.Serializable;

/**
 *
 * @author Roland Schwarz <rfs32@cam.ac.uk>
 */
public class DefaultNameable implements Nameable, Serializable{
	private final static long serialVersionUID=1L;
	String name;
	String description;
	public DefaultNameable() {
		
	}
	public DefaultNameable(String name, String description){
		this.name=name;
		this.description=description;
	}
	
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	
}
