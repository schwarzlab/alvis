/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.general.containerModel.impl;

import java.util.Map;

/**
 * Provides a default value for the "get" method of the Map interface.
 * 
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 * @param <E>
 * @param <F>
 */
public class MapDefaultValueDecorator<E,F> extends MapDecorator<E, F> {
	F defaultValue;
	private boolean allowDefaultValue;
	
	/**
	 * Decorates the given map with a default value. Calls to the get method that would otherwise
	 * return null now return the default value. If @param allowDefaultValue is set, calls to the put
	 * method with the default value attempt to remove the element instead.
	 * 
	 * @param map
	 * @param defaultValue
	 * @param allowDefaultValue 
	 */
	public MapDefaultValueDecorator(Map<E,F> map, F defaultValue, boolean allowDefaultValue) {
		super(map);
		this.defaultValue = defaultValue;
		this.allowDefaultValue = allowDefaultValue;
	}

	public MapDefaultValueDecorator(Map<E,F> map, F defaultValue) {
		this(map, defaultValue, true);
	}

	@Override
	public F get(Object key) {
		F retval = super.get(key); //To change body of generated methods, choose Tools | Templates.
		if (retval==null) {
			retval = defaultValue;
		}
		return retval;
	}

	@Override
	public F put(E key, F value) {
		if (!allowDefaultValue && value.equals(defaultValue)) {
			F previousKey = super.get(key);
			remove(key);
			System.out.println("warning: put default value");			
			return previousKey;
		}
		return super.put(key, value); //To change body of generated methods, choose Tools | Templates.
	}

	
	
}
