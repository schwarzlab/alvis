package com.general.containerModel.impl;

import java.util.*;

import javax.swing.event.EventListenerList;

import com.general.containerModel.*;

/**
 * <p>Title: YANA - Yet Another Network Analyzer</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Dep. of Bioinformatics, University Wuerzburg</p>
 *
 * @author Roland Schwarz
 * @version 0.8
 */
public class ListenableListImpl<E> implements ListenableList<E> {
  private List<E> delegate;
  private EventListenerList listenerList=new EventListenerList();
  private boolean hasFastNameSearch=false;
  private final ListenerAdapter<E> adapter;

  public ListenableListImpl(List<E> delegate, ListenerAdapter<E> adapter) {
    this.delegate=delegate;
    if (delegate instanceof FastNameSearch)
      hasFastNameSearch=true;
    this.adapter=adapter;
    adapter.setCallback(this);
  }

  // ***********************************************************************************************************
  private void addItemListener(E item) {
    adapter.addItemListener(Collections.singleton(item));
  }

  private void removeItemListener(E item) {
    adapter.removeItemListener(Collections.singleton(item));
  }

  private void addItemListener(Iterable<E> items) {
    adapter.addItemListener(items);
  }

  private void removeItemListener(Iterable<E> items) {
    adapter.removeItemListener(items);
  }
  // **************************************************************************************************************

  // fire... methods
  public void fireItemChanged(E item) {
    CollectionEvent<E> e=new CollectionEvent<E>(this,Collections.singleton(item));
    CollectionListener[] listeners=listenerList.getListeners(CollectionListener.class);
    for (CollectionListener l:listeners) {
      l.itemChanged(e);
    }
  }

  private void fireItemAdded(E item) {
    fireItemAdded(Collections.singleton(item));
  }

  private void fireItemAdded(Collection<E> items) {
    CollectionEvent<E> e=new CollectionEvent<E>(this,items);
    CollectionListener[] listeners=listenerList.getListeners(CollectionListener.class);
    for (CollectionListener l:listeners) {
      l.itemAdded(e);
    }
  }

  private void fireItemRemoved(E item) {
    fireItemRemoved(Collections.singleton(item));
  }

  private void fireItemRemoved(Collection<E> r) {
    CollectionEvent<E> e=new CollectionEvent<E>(this,r);
    CollectionListener[] listeners=listenerList.getListeners(CollectionListener.class);
    for (CollectionListener l:listeners) {
      l.itemRemoved(e);
    }
  }

  // add and remove listener methods
  public void addCollectionListener(CollectionListener l) {
    listenerList.add(CollectionListener.class,l);
  }

  public void removeCollectionListener(CollectionListener l) {
    listenerList.remove(CollectionListener.class,l);
  }

  // Collection interface
  public Iterator<E> iterator() {
    return delegate.iterator();
  }

  public boolean addAll(Collection<? extends E> c) {
    List<E> added=new ArrayList<E>();
    for (E rea:c) {
      if(delegate.add(rea)) {
        added.add(rea);
      }
    }
    addItemListener(added);
    fireItemAdded(added);
    return added.size()>0;
  }

  public void clear() {
    List<E> all=new ArrayList<E>(this);
    delegate.clear();
    removeItemListener(this);
    fireItemRemoved(all);
  }

  public boolean contains(Object o) {
    return delegate.contains(o);
  }

  public boolean containsAll(Collection<?> c) {
    return delegate.containsAll(c);
  }

  public int hashCode() {
    return delegate.hashCode();
  }

  public boolean isEmpty() {
    return delegate.isEmpty();
  }

  public boolean remove(Object o) {
    if(delegate.remove(o)) {
      removeItemListener((E)o);
      fireItemRemoved((E) o);
      return true;
    }
    return false;
  }

  public boolean removeAll(Collection<?> c) {
    List<E> removed=new ArrayList<E>();
    for (Object o:c) {
      if(delegate.remove(o)) {
        removed.add((E)o);
      }
    }
    removeItemListener(removed);
    fireItemRemoved(removed);
    return removed.size()>0;
  }

  public boolean retainAll(Collection<?> c) {
    throw new UnsupportedOperationException();
  }

  public int size() {
    return delegate.size();
  }

  public Object[] toArray() {
    return delegate.toArray();
  }

  public boolean add(E o) {
    if(delegate.add(o)) {
      addItemListener(o);
      fireItemAdded(o);
      return true;
    }
    return false;
  }

  public boolean equals(Object o) {
    return delegate.equals(o);
  }

  public Object[] toArray(Object[] a) {
    return delegate.toArray(a);
  }

  public boolean addAll(int index, Collection c) {
    throw new UnsupportedOperationException();
  }

  public E get(int index) {
    return delegate.get(index);
  }

  public E set(int index, E element) {
    E old=delegate.set(index,element);
    removeItemListener(old);
    addItemListener(element);
    fireItemRemoved(old);
    fireItemAdded(element);
    return old;
  }

  public void add(int index, E element) {
    delegate.add(index,element);
    addItemListener(element);
    fireItemAdded(element);
  }

  public E remove(int index) {
    E old=delegate.remove(index);
    removeItemListener(old);
    fireItemRemoved(old);
    return old;
  }

  public int indexOf(Object o) {
    return delegate.indexOf(o);
  }

  public int lastIndexOf(Object o) {
    return delegate.lastIndexOf(o);
  }

  public ListIterator<E> listIterator() {
    return delegate.listIterator();
  }

  public ListIterator<E> listIterator(int index) {
    return delegate.listIterator(index);
  }

  public List<E> subList(int fromIndex, int toIndex) {
    return delegate.subList(fromIndex,toIndex);
  }

  public E findByName(String name) {
    if (hasFastNameSearch)
      return (E)((FastNameSearch)delegate).findByName(name);
    else
      throw new UnsupportedOperationException("FastNameSearch not implemented");
  }

  public boolean hasFastNameSearch() {
    return hasFastNameSearch;
  }

	@Override
	public void setValueIsAdjusting(boolean value) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public boolean getValueIsAdjusting() {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

}
