/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.general.containerModel.impl;

import com.dictiography.collections.IndexedNavigableMap;
import com.general.containerModel.ListenableCollection;
import java.util.Comparator;
import java.util.NavigableMap;
import java.util.NavigableSet;
import java.util.SortedMap;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public class IndexedMapListenerDecorator<E,F> extends MapListenerDecorator<E, F> implements IndexedNavigableMap<E,F>, ListenableCollection<E>{
	private final IndexedNavigableMap<E,F> map;
	
	public IndexedMapListenerDecorator(IndexedNavigableMap<E,F> map) {
		super(map);
		this.map = map;
	}
	
	public static IndexedMapListenerDecorator getInstance(IndexedNavigableMap map) {
		int id = System.identityHashCode(map);
		IndexedMapListenerDecorator instance = (IndexedMapListenerDecorator) instanceMap.get(id);
		if (instance==null){
			instance = new IndexedMapListenerDecorator(map);
			instanceMap.put(id, instance);
		}
		return instance;
	}
	
	@Override
	public E exactKey(int index) {
		return map.exactKey(index);
	}

	@Override
	public Entry<E, F> exactEntry(int index) {
		return map.exactEntry(index);
	}

	@Override
	public int keyIndex(E k) {
		return map.keyIndex(k);
	}

	@Override
	public Entry<E, F> lowerEntry(E key) {
		return map.lowerEntry(key);
	}

	@Override
	public E lowerKey(E key) {
		return map.lowerKey(key);
	}

	@Override
	public Entry<E, F> floorEntry(E key) {
		return map.floorEntry(key);
	}

	@Override
	public E floorKey(E key) {
		return map.floorKey(key);
	}

	@Override
	public Entry<E, F> ceilingEntry(E key) {
		return map.ceilingEntry(key);
	}

	@Override
	public E ceilingKey(E key) {
		return map.ceilingKey(key);
	}

	@Override
	public Entry<E, F> higherEntry(E key) {
		return map.higherEntry(key);
	}

	@Override
	public E higherKey(E key) {
		return map.higherKey(key);
	}

	@Override
	public Entry<E, F> firstEntry() {
		return map.firstEntry();
	}

	@Override
	public Entry<E, F> lastEntry() {
		return map.lastEntry();
	}

	@Override
	public Entry<E, F> pollFirstEntry() {
		return map.pollFirstEntry();
	}

	@Override
	public Entry<E, F> pollLastEntry() {
		return map.pollLastEntry();
	}

	@Override
	public NavigableMap<E, F> descendingMap() {
		return map.descendingMap();
	}

	@Override
	public NavigableSet<E> navigableKeySet() {
		return map.navigableKeySet();
	}

	@Override
	public NavigableSet<E> descendingKeySet() {
		return map.descendingKeySet();
	}

	@Override
	public NavigableMap<E, F> subMap(E fromKey, boolean fromInclusive, E toKey, boolean toInclusive) {
		return map.subMap(fromKey, fromInclusive, toKey, toInclusive);
	}

	@Override
	public NavigableMap<E, F> headMap(E toKey, boolean inclusive) {
		return map.headMap(toKey, inclusive);
	}

	@Override
	public NavigableMap<E, F> tailMap(E fromKey, boolean inclusive) {
		return map.tailMap(fromKey, inclusive);
	}

	@Override
	public SortedMap<E, F> subMap(E fromKey, E toKey) {
		return map.subMap(fromKey, toKey);
	}

	@Override
	public SortedMap<E, F> headMap(E toKey) {
		return map.headMap(toKey);
	}

	@Override
	public SortedMap<E, F> tailMap(E fromKey) {
		return map.tailMap(fromKey);
	}

	@Override
	public Comparator<? super E> comparator() {
		return map.comparator();
	}

	@Override
	public E firstKey() {
		return map.firstKey();
	}

	@Override
	public E lastKey() {
		return map.lastKey();
	}

}
