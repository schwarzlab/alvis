/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.general.containerModel.impl;

import com.general.containerModel.CollectionListener;
import com.general.containerModel.CollectionListenerSupport;
import com.general.containerModel.ListenableCollection;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public class MapListenerDecorator<E,F> extends MapDecorator<E, F> implements ListenableCollection<E> {
	protected transient CollectionListenerSupport<E> cls;
	protected static final Map<Integer, MapListenerDecorator> instanceMap = new HashMap<>();

	public MapListenerDecorator(Map<E,F> map) {
		super(map);
		if (map instanceof MapListenerDecorator) {
			throw new IllegalArgumentException("Map must not be of MapListenerDecorator type!");
		}
		this.cls=new CollectionListenerSupport<>(this);
	}

	private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();
		this.cls=new CollectionListenerSupport<>(this);
    }
	
	public static MapListenerDecorator getInstance(Map map) {
		if (map==null) return null;
		
		int id = System.identityHashCode(map);
		MapListenerDecorator instance = instanceMap.get(id);
		if (instance==null){
			instance = new MapListenerDecorator(map);
			instanceMap.put(id, instance);
		}
		return instance;
	}
	
	@Override
	public F put(E key, F value) {
		F retval = map.put(key, value);
		if (retval==null) { //new entry
			cls.fireItemAdded(Collections.singleton(key));
		} else {
			cls.fireItemChanged(Collections.singleton(key));
		}
		return retval;
	}

	@Override
	public F remove(Object key) {
		F retval = map.remove(key);
		if (retval!=null){
			cls.fireItemRemoved(Collections.singleton((E)key));
		}
		return retval;
	}

	@Override
	public void putAll(Map<? extends E, ? extends F> m) {
		map.putAll(m);
		cls.fireItemAdded((Set<E>)m.keySet());
	}

	@Override
	public void clear() {
		Set<E> set = new HashSet<E>(map.keySet());
		map.clear();
		cls.fireItemRemoved(set);
	}

	@Override
	public void addCollectionListener(CollectionListener<E> l){
		cls.addCollectionListener(l);
	}
	
	@Override
	public void removeCollectionListener(CollectionListener<E> l){
		cls.removeCollectionListener(l);
	}

	@Override
	public void setValueIsAdjusting(boolean value) {
		cls.setValueIsAdjusting(value);
	}

	@Override
	public boolean getValueIsAdjusting() {
		return cls.getValueIsAdjusting();
	}
	
	
}
