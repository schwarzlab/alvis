/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.general.containerModel.impl;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * Base decorator class for Maps with generic types E and F. All map decorators can extend this base class.
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 * @param <E>
 * @param <F>
 */
public class MapDecorator<E,F> implements Map<E,F>, Serializable{
	private final static long serialVersionUID=1L;
	protected final Map<E,F> map;
	
	public MapDecorator(Map<E,F> map) {
		this.map=map;
	}

	@Override
	public int size() {
		return map.size();
	}

	@Override
	public boolean isEmpty() {
		return map.isEmpty();
	}

	@Override
	public boolean containsKey(Object key) {
		return map.containsKey(key);
	}

	@Override
	public boolean containsValue(Object value) {
		return map.containsValue(value);
	}

	@Override
	public F get(Object key) {
		return map.get(key);
	}

	@Override
	public F put(E key, F value) {
		return map.put(key, value);
	}

	@Override
	public F remove(Object key) {
		return map.remove(key);
	}

	@Override
	public void putAll(Map<? extends E, ? extends F> m) {
		map.putAll(m);
	}

	@Override
	public void clear() {
		map.clear();
	}

	@Override
	public Set<E> keySet() {
		return map.keySet();
	}

	@Override
	public Collection<F> values() {
		return map.values();
	}

	@Override
	public Set<Entry<E, F>> entrySet() {
		return map.entrySet();
	}

	@Override
	public int hashCode() {
		return map.hashCode(); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public boolean equals(Object obj) {
		return map.equals(obj); //To change body of generated methods, choose Tools | Templates.
	}
	
	

	
}
