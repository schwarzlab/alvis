package com.general.containerModel;

import java.util.EventListener;

/**
 * <p>Title: YANA - Yet Another Network Analyzer</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Dep. of Bioinformatics, University Wuerzburg</p>
 *
 * @author not attributable
 * @version 0.8
 */
public interface CollectionListener<S> extends EventListener {
  public void itemAdded(CollectionEvent<S> e);
  public void itemRemoved(CollectionEvent<S> e);
  public void itemChanged(CollectionEvent<S> e);
  public void collectionChanged(CollectionEvent<S> e);
}
