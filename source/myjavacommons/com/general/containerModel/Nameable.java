package com.general.containerModel;

/**
 * <p>
 * Title: YANA - Yet Another Network Analyzer</p>
 *
 * <p>
 * Description: </p>
 *
 * <p>
 * Copyright: Copyright (c) 2005</p>
 *
 * <p>
 * Company: Dep. of Bioinformatics, University Wuerzburg</p>
 *
 * @author Roland Schwarz
 * @version 0.8
 */
public interface Nameable {
	public String getName();
	public String getDescription();
}
