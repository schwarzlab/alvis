/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.general.containerModel;

import java.util.List;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public interface ListenableList<E> extends List<E>, ListenableCollection<E> {
	
}
