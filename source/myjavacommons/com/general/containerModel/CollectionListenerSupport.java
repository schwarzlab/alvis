/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.general.containerModel;

import java.util.Collection;
import java.util.Collections;
import javax.swing.event.EventListenerList;

/**
 * This class provides support to equip a Collection with listener support. 
 * Any new collection (or any Object) can redirect add, remove and fire methods to this support class.
 * 
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public class CollectionListenerSupport<E> {
	EventListenerList listenerList = new EventListenerList();
	private Object source;
	private boolean valueIsAdjusting=false;
	
	
	public CollectionListenerSupport(Object source) {
		this.source=source;
	}
	
  // add and remove listener methods
  public void addCollectionListener(CollectionListener<E> l) {
    listenerList.add(CollectionListener.class,l);
  }

  public void removeCollectionListener(CollectionListener<E> l) {
    listenerList.remove(CollectionListener.class,l);
  }

// fire... methods
  public void fireItemChanged(CollectionEvent<E> e) {
    CollectionListener[] listeners=listenerList.getListeners(CollectionListener.class);
    for (CollectionListener l:listeners) {
      l.itemChanged(e);
    }
  }

  public void fireItemAdded(CollectionEvent<E> e) {
    CollectionListener[] listeners=listenerList.getListeners(CollectionListener.class);
    for (CollectionListener l:listeners) {
      l.itemAdded(e);
    }
	
  }

  public void fireItemRemoved(CollectionEvent<E> e) {
    CollectionListener[] listeners=listenerList.getListeners(CollectionListener.class);
    for (CollectionListener l:listeners) {
      l.itemRemoved(e);
    }
  }
  
  public void fireCollectionChanged(CollectionEvent<E> e) {
	CollectionListener[] listeners=listenerList.getListeners(CollectionListener.class);
	for (CollectionListener l:listeners) {
	  l.collectionChanged(e);
	}
  }

    public void fireItemChanged(Collection<E> items) {
		fireItemChanged(new CollectionEvent<>(source, items, getValueIsAdjusting()));
	}

  public void fireItemAdded(Collection<E> items) {
		fireItemAdded(new CollectionEvent<>(source, items, getValueIsAdjusting()));
  }

  public void fireItemRemoved(Collection<E> items) {
		fireItemRemoved(new CollectionEvent<>(source, items, getValueIsAdjusting()));  }
  
  public void fireCollectionChanged() {
		fireCollectionChanged(new CollectionEvent<E>(source, Collections.EMPTY_LIST, getValueIsAdjusting()));
  }

	/**
	 * @return the valueIsAdjusting
	 */
	public boolean getValueIsAdjusting() {
		return valueIsAdjusting;
	}

	/**
	 * @param valueIsAdjusting the valueIsAdjusting to set
	 */
	public void setValueIsAdjusting(boolean valueIsAdjusting) { setValueIsAdjusting(valueIsAdjusting, true); }
	public void setValueIsAdjusting(boolean valueIsAdjusting, boolean notifyListeners) {
		if (valueIsAdjusting!=this.valueIsAdjusting) {
			this.valueIsAdjusting = valueIsAdjusting;
			if(!this.valueIsAdjusting && notifyListeners) {
				CollectionEvent<E> e = new CollectionEvent(source, Collections.EMPTY_LIST, false);
				fireCollectionChanged(e);
			}
		}
	}
  
	
}
