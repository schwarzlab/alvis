package com.general.containerModel;

import java.util.Collection;


/**
 * <p>Title: YANA - Yet Another Network Analyzer</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Dep. of Bioinformatics, University Wuerzburg</p>
 *
 * @author not attributable
 * @version 0.8
 */
public class Nameables {
  public static Nameable findByName(Collection<? extends Nameable> nameables, String name) {
    Nameable n=null;
    if (nameables instanceof FastNameSearch) {
      try {
        n = (Nameable) ((FastNameSearch) nameables).findByName(name);
      } catch (UnsupportedOperationException e) {
        n = slowFindByName(nameables, name);
      }
    } else {
      n = slowFindByName(nameables, name);
    }
    return n;
  }

  public static Nameable slowFindByName(Collection<? extends Nameable> nameables,String name) {
    for (Nameable n:nameables) {
      if (n.getName().equalsIgnoreCase(name))
        return n;
    }
    return null;
  }
}
