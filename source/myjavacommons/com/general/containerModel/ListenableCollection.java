package com.general.containerModel;

/**
 * <p>Title: YANA - Yet Another Network Analyzer</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Dep. of Bioinformatics, University Wuerzburg</p>
 *
 * @author not attributable
 * @version 0.8
 */
public interface ListenableCollection<E> {
  public void addCollectionListener(CollectionListener<E> l);
  public void removeCollectionListener(CollectionListener<E> l);
  public void setValueIsAdjusting(boolean value);
  public boolean getValueIsAdjusting();
}
