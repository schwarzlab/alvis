package com.general.containerModel;

import java.util.Comparator;


/**
 * <p>Title: YANA - Yet Another Network Analyzer</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Dep. of Bioinformatics, University Wuerzburg</p>
 *
 * @author Roland Schwarz
 * @version 0.8
 */
public class NameableComparator implements Comparator {
  public NameableComparator() {
  }

  public int compare(Object o1, Object o2) {
    if ((o1 instanceof String) && (o2 instanceof Nameable)) {
      return ((String)o1).compareToIgnoreCase(((Nameable)o2).getName());
    } else if ((o2 instanceof String) && (o1 instanceof Nameable)) {
      return ((Nameable)o1).getName().compareToIgnoreCase(((String)o2));
    } else if ((o1 instanceof Nameable)&&(o2 instanceof Nameable)) {
      return ((Nameable)o1).getName().compareToIgnoreCase(((Nameable)o2).getName());
    }
    throw new ClassCastException();
  }

  public boolean equals(Object obj) {
    return false;
  }
}
