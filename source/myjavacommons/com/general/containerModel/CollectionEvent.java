package com.general.containerModel;

import java.util.EventObject;
import java.util.Collection;

/**
 * <p>Title: YANA - Yet Another Network Analyzer</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Dep. of Bioinformatics, University Wuerzburg</p>
 *
 * @author not attributable
 * @version 0.8
 */
public class CollectionEvent<S> extends EventObject {
  private Collection<S> changedElements;
  private boolean valueIsAdjusting;
  
  public CollectionEvent(Object source, Collection<S> changedElements) {
    super(source);
    this.changedElements=changedElements;
	this.valueIsAdjusting=true;
  }
  
  public CollectionEvent(Object source, Collection<S> changedElements, boolean adjusting) {
    super(source);
    this.changedElements=changedElements;
	this.valueIsAdjusting=adjusting;
  }
  

  public Collection<S> getChangedElements() {
    return changedElements;
  }

	/**
	 * @return the valueIsAdjusting
	 */
	public boolean getValueIsAdjusting() {
		return valueIsAdjusting;
	}

	/**
	 * @param valueIsAdjusting the valueIsAdjusting to set
	 */
	public void setValueIsAdjusting(boolean valueIsAdjusting) {
		this.valueIsAdjusting = valueIsAdjusting;
	}
}
