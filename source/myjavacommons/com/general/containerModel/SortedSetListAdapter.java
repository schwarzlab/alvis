package com.general.containerModel;

import java.util.*;

/**
 * <p>Title: YANA - Yet Another Network Analyzer</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Dep. of Bioinformatics, University Wuerzburg</p>
 *
 * @author not attributable
 * @version 0.8
 */
public class SortedSetListAdapter<E> implements List<E>{
  private static final String UOE_TEXT="Operation would destroy the ordering of the SortedSet!";
  private SortedSet<E> adaptee;
  public SortedSetListAdapter(SortedSet<E> adaptee) {
    this.adaptee=adaptee;
  }

  public int size() {
    return adaptee.size();
  }

  public boolean isEmpty() {
    return adaptee.isEmpty();
  }

  public boolean contains(Object o) {
    return adaptee.contains(o);
  }

  public Iterator<E> iterator() {
    return adaptee.iterator();
  }

  public Object[] toArray() {
    return adaptee.toArray();
  }

  public Object[] toArray(Object[] a) {
    return adaptee.toArray(a);
  }

  public boolean add(E o) {
    return adaptee.add(o);
  }

  public boolean remove(Object o) {
    return adaptee.remove(o);
  }

  public boolean containsAll(Collection c) {
    return adaptee.containsAll(c);
  }

  public boolean addAll(Collection<? extends E> c) {
    return adaptee.addAll(c);
  }

  public boolean removeAll(Collection c) {
    return adaptee.removeAll(c);
  }

  public boolean retainAll(Collection c) {
    return adaptee.retainAll(c);
  }

  public void clear() {
    adaptee.clear();
  }

  public boolean equals(Object o) {
    return adaptee.equals(o);
  }

  public int hashCode() {
    return adaptee.hashCode();
  }

  public boolean addAll(int index, Collection<? extends E> c) {
    throw new UnsupportedOperationException(UOE_TEXT);
  }

  public E get(int index) {
    int i=0;
    for (E e:adaptee) {
      if (index==i++) {
        return e;
      }
    }
    throw new IndexOutOfBoundsException(String.valueOf(index));
  }

  public E set(int index, Object element) {
    throw new UnsupportedOperationException(UOE_TEXT);
  }

  public void add(int index, Object element) {
    throw new UnsupportedOperationException(UOE_TEXT);
  }

  public E remove(int index) {
    int i=0;
    for (E e:adaptee) {
      if (index==i++) {
        adaptee.remove(e);
        return e;
      }
    }
    throw new IndexOutOfBoundsException();
  }

  public int indexOf(Object o) {
    int i=0;
    for (E e:adaptee) {
      if (e.equals(o))
        return i;
      i++;
    }
    return -1;
  }

  public int lastIndexOf(Object o) {
    return indexOf(o);
  }

  public ListIterator<E> listIterator() {
    return new SortedSetListIterator();
  }

  public ListIterator<E> listIterator(int index) {
    return new SortedSetListIterator(index);
  }

  public List<E> subList(int fromIndex, int toIndex) {
    List<E> list=new ArrayList<E>(toIndex-fromIndex+50);
    int i=0;
    for (E e:adaptee) {
      if ((i>=fromIndex) && (i<toIndex)) {
        list.add(e);
      }
      if (i>=toIndex)
        break;
    }
    return list;
  }


  private class SortedSetListIterator implements ListIterator<E> {
    private int index=-1;
    public SortedSetListIterator() {
    }
    public SortedSetListIterator(int index) {
      this.index=index-1;
    }

    public boolean hasNext() {
      return (index<size()-1);
    }

    public E next() {
      return get(++index);
    }

    public boolean hasPrevious() {
      return (index>0);
    }

    public E previous() {
      return get(--index);
    }

    public int nextIndex() {
      return index+1;
    }

    public int previousIndex() {
      return index-1;
    }

    public void remove() {
      throw new UnsupportedOperationException(UOE_TEXT);
    }

    public void set(Object o) {
      throw new UnsupportedOperationException(UOE_TEXT);
    }

    public void add(Object o) {
      throw new UnsupportedOperationException(UOE_TEXT);
    }
  }
}
