/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.general.containerModel;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public class CollectionAdapter<S> implements CollectionListener<S>{
	@Override
	public void itemAdded(CollectionEvent<S> e) {
	}

	@Override
	public void itemRemoved(CollectionEvent<S> e) {
	}

	@Override
	public void itemChanged(CollectionEvent<S> e) {
	}

	@Override
	public void collectionChanged(CollectionEvent<S> e) {
	}

}
