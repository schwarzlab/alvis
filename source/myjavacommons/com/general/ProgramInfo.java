package com.general;

/**
 * <p>Title: YANA</p>
 * <p>Description: Yet Another Network Analyzer</p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: University of Würzburg, Department for Bioinformatics</p>
 * @author Roland Schwarz
 * @version 0.9
 */

public class ProgramInfo {
  public String PROGRAM_SHORT_NAME="";
  public String PROGRAM_LONG_NAME="";
  public String PROGRAM_VERSION="";

  public String AUTHOR_NAME="";
  public String AUTHOR_DEPARTMENT="";
  public String AUTHOR_CONTACT="";
}
