/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.general.patterns;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class is just for demonstration purposes.
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public class SingletonPattern<T> {
	Map<Integer, T> instanceMap=new HashMap<>();
	
	/**
	 * Returns the singleton instance of the given class
	 * 
	 * @param theclass
	 * @return 
	 */
	T getInstance(Class<T> theclass) {
		return getInstance(theclass, theclass);
	}
	
	/**
	 * Asserts that only one instance per object exists. Returns that instance for
	 * the given object.
	 * 
	 * @param theclass
	 * @param object
	 * @return 
	 */
	T getInstance(Class<T> theclass, Object object){
		int id = System.identityHashCode(object);
		T instance = instanceMap.get(object);
		if (instance==null){
			try {
				instance = theclass.newInstance();
				instanceMap.put(id, instance);
			} catch (InstantiationException | IllegalAccessException ex) {
				Logger.getLogger(SingletonPattern.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		return instance;
	}
}
