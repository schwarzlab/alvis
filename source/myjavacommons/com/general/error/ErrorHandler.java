/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.general.error;

import java.util.logging.Logger;

/**
 *
 * @author Roland Schwarz <rfs32@cam.ac.uk>
 */
public class ErrorHandler implements Thread.UncaughtExceptionHandler {
	private static final Logger LOG = Logger.getLogger(ErrorHandler.class.getName());
	private static ErrorHandler _INSTANCE;
	private ErrorHandler(){}
	
	public static synchronized ErrorHandler getInstance() {
		if (_INSTANCE==null){
			_INSTANCE=new ErrorHandler();
		}
		return _INSTANCE;
	}
	
	
	
	@Override
	public void uncaughtException(Thread t, Throwable e) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}
	
}
