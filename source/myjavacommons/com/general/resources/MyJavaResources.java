package com.general.resources;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.net.URL;
import javax.swing.Icon;
import javax.swing.ImageIcon;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public class MyJavaResources {
    public static ImageIcon OK_32x32;
	public static ImageIcon OK_16x16;
	public static ImageIcon NOTOK_32x32;
	public static ImageIcon NOTOK_16x16;
	public static ImageIcon DELETE_32x32;
	public static ImageIcon DELETE_64x64;
	public static ImageIcon SELECT_ALL_32x32;
	public static ImageIcon SELECT_ALL_64x64;
	public static ImageIcon SELECT_ALL_128x128;
	
    
    // load the resources, if that gets too time consuming remove and put explicitely on a worker thread
    static {
        loadResources();
    }

	public static Icon createColoredSquare(int size, Color color) {
		return new ColoredSquareIcon(size, color);
	}
    
	private MyJavaResources() {} // static only
    
	public static void init(){} // to force loading if desired
	
    private static void loadResources() {
		OK_32x32=createImageIcon("images/ok_32x32.png");
		OK_16x16 = new ImageIcon(OK_32x32.getImage().getScaledInstance(16, 16, Image.SCALE_SMOOTH));
		NOTOK_32x32=createImageIcon("images/notok_32x32.png");
		NOTOK_16x16 = new ImageIcon(NOTOK_32x32.getImage().getScaledInstance(16, 16, Image.SCALE_SMOOTH));		
		DELETE_32x32=createImageIcon("images/red_cross_32x32.png");
		DELETE_64x64=createImageIcon("images/red_cross_64x64.png");
		SELECT_ALL_32x32=createImageIcon("images/select_all_32x32.png");		
		SELECT_ALL_64x64=createImageIcon("images/select_all_64x64.png");		
		SELECT_ALL_128x128=createImageIcon("images/select_all_128x128.png");		
	}
    
    private static ImageIcon createImageIcon(String resource) {
        ImageIcon icon = null;
        URL imageURL = MyJavaResources.class.getResource(resource);
        if (imageURL != null) {
            icon = new ImageIcon(imageURL);
        }        
        return icon;
    }
	
	private static class ColoredSquareIcon implements Icon {
		private int size;
		private Color color;
		public ColoredSquareIcon(int size, Color color) {
			this.size=size;
			this.color=color;
		}
		
		@Override
		public void paintIcon(Component c, Graphics g, int x, int y) {
			Graphics2D gc = (Graphics2D)g;
			gc.setColor(color);
			gc.fillRect(2, y, getIconWidth(), getIconHeight());
		}

		@Override
		public int getIconWidth() {
			return size;
		}

		@Override
		public int getIconHeight() {
			return size;
		}
		
	}	
	
}
