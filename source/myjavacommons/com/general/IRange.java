package com.general;

/**
 * <p>Title: YANA</p>
 *
 * <p>Description: Yet Another Network Analyzer</p>
 *
 * <p>Copyright: Copyright (c) 2004</p>
 *
 * <p>Company: Dep. of Bioinformatics - University Wuerzburg</p>
 *
 * @author Roland Schwarz
 * @version 0.4
 */
public interface IRange<T extends Comparable> {
  public void setFrom(T from);
  public T getFrom();
  public void setTo(T to);
  public T getTo();
  public boolean isInRange(T target);
  public T fitToRange(T target);
}
