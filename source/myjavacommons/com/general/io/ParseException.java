package com.general.io;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */

public class ParseException extends Exception {
  public ParseException() {
      super();
  }

  public ParseException(String message) {
      super(message);
  }

  public ParseException(String message, Throwable cause) {
      super(message, cause);
  }

  public ParseException(Throwable cause) {
      super(cause);
  }
}
