package com.general.utils;

import com.general.mvc.view.IDisplayable;
import java.awt.*;
import java.awt.event.*;
import java.util.Arrays;
import javax.swing.*;


public class GUIutils {

	public static int[] getSelectedIndices(ListSelectionModel sm) {
		int iMin = sm.getMinSelectionIndex();
		int iMax = sm.getMaxSelectionIndex();
		if ((iMin < 0) || (iMax < 0)) {
			return new int[0];
		}
		int[] rvTmp = new int[1 + (iMax - iMin)];
		int n = 0;
		for (int i = iMin; i <= iMax; i++) {
			if (sm.isSelectedIndex(i)) {
				rvTmp[n++] = i;
			}
		}
		int[] rv = new int[n];
		System.arraycopy(rvTmp, 0, rv, 0, n);
		return rv;
	}
	
    public GUIutils() {
    }

    public static void setCancelButton(JComponent component, final JButton cancelButton) {
        final String CANCEL_ACTION_KEY="CANCEL";

        KeyStroke keyStroke=KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0, false);
        component.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(keyStroke, CANCEL_ACTION_KEY);

        component.getActionMap().put(CANCEL_ACTION_KEY, new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                cancelButton.doClick();
            }
        });
    }

    public static void setCancelButton(JComponent component, final Action cancelAction) {
        final String CANCEL_ACTION_KEY="CANCEL";

        KeyStroke keyStroke=KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0, false);
        component.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(keyStroke, CANCEL_ACTION_KEY);

        component.getActionMap().put(CANCEL_ACTION_KEY, cancelAction);
    }

    public static void setDefaultButton(JComponent component, final JButton okButton) {
        final String OK_ACTION_KEY="OK";

        JRootPane rootPane=component.getRootPane();
        if (rootPane!=null)
          rootPane.setDefaultButton(okButton);

        KeyStroke keyStroke=KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, false);
        component.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(keyStroke, OK_ACTION_KEY);

        component.getActionMap().put(OK_ACTION_KEY, new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                okButton.doClick();
            }
        });
    }

    public static void setLookAndFeel(Window window,String laf) throws UnsupportedLookAndFeelException{
      try {
        UIManager.setLookAndFeel(laf);
      } catch (Exception ex) {
        com.general.Log.out(ex);
        if (ex instanceof UnsupportedLookAndFeelException) {
          throw((UnsupportedLookAndFeelException) ex);
        }
      }
      if (window!=null) {
        SwingUtilities.updateComponentTreeUI(window);
        window.pack();
      }
    }

    public static Point getPointForCentering(Window window, boolean centerOnScreen) {
      int parentMiddleX, parentMiddleY, thisMiddleX, thisMiddleY,pointX, pointY;

      if ((window.getOwner()==null)||(centerOnScreen)) {
          Dimension screenSize=Toolkit.getDefaultToolkit().getScreenSize();
          parentMiddleX =  screenSize.width / 2;
          parentMiddleY = screenSize.height / 2;
      }
      else {
          // center on owner window, usually the MainFrame
          parentMiddleX = window.getOwner().getX() + (window.getOwner().getWidth() / 2);
          parentMiddleY = window.getOwner().getY() + (window.getOwner().getHeight() / 2);
      }

      thisMiddleX = window.getWidth() / 2;
      thisMiddleY = window.getHeight() / 2;
      pointX=(parentMiddleX<thisMiddleX?0:parentMiddleX-thisMiddleX);
      pointY=(parentMiddleY<thisMiddleY?0:parentMiddleY-thisMiddleY);
      return new Point(pointX, pointY);
    }

    public static boolean isAbstractButtonAndSelected(Object object) {
      boolean show=false;
      if (object!=null) {
        if (object instanceof AbstractButton) {
          show=((AbstractButton)object).isSelected();
        }
      }
      return show;
    }

    public static void closeWindow(IDisplayable dis) {
      Window window=SwingUtilities.getWindowAncestor(dis.getComponent());
      if (window!=null) {
        window.setVisible(false);
      }
    }

    public static boolean isVisible(IDisplayable dis) {
      Window window=SwingUtilities.getWindowAncestor(dis.getComponent());
      if (window!=null) {
        return window.isVisible();
      } else {
        return false;
      }
    }

	public static void setQualityRenderingHints(Graphics2D g) {
		setQualityRenderingHints(g, true, true);
	}
	
	public static void setQualityRenderingHints(Graphics2D g, boolean antialiasing, boolean speed) {
		g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,RenderingHints.VALUE_INTERPOLATION_BICUBIC);
//		g.setRenderingHint(RenderingHints.KEY_RENDERING,RenderingHints.VALUE_RENDER_QUALITY);		
		if (speed) {
			g.setRenderingHint(RenderingHints.KEY_RENDERING,RenderingHints.VALUE_RENDER_SPEED);					
		} else {
			g.setRenderingHint(RenderingHints.KEY_RENDERING,RenderingHints.VALUE_RENDER_QUALITY);					
		}

		if (antialiasing) {
			g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		} else {
			g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
			g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
		}
	}
	
	public static Color[] createColorGradient(Color from, Color to, double[] values, int ncolors){
		return createColorGradient(from, to, values, ncolors, Color.WHITE);
	}
	public static Color[] createColorGradient(Color from, Color to, double[] values, int ncolors, Color nanColor){
		double maxval = GeneralUtils.max(values);
		double minval = GeneralUtils.min(values);
		return createColorGradient(from, to, values, minval, maxval, ncolors, nanColor);
	}
	public static Color[] createColorGradient(Color from, Color to, double[] values, double minval, double maxval, int ncolors, Color nanColor){
		Color[] retval=new Color[values.length];
		double span = Math.abs(maxval - minval);
		double box = span / ncolors;
		Color[] theColors = interpolateColors(from, to, ncolors);
		for (int i=0;i<values.length;i++){
			double value = values[i];
			if (!Double.isNaN(value)) {
				int index = (int)Math.floor((Math.abs(value-minval))/box);
				if (index>=ncolors) {
					index=ncolors-1;
				}
				retval[i]=theColors[index];
			} else {
				retval[i]=nanColor;
			}
		}
		
		return retval;
	}
	
	private static Color[] interpolateColors(Color from, Color to, int ncolors){
		if (ncolors<2) return null;
		Color[] retval = new Color[ncolors];
		double pc = 1.0 / ncolors;
		for (int i=0;i<ncolors;i++){
			double mix = 1-(pc*(i+1));
			int red = (int)Math.round(from.getRed()*mix + to.getRed()*(1-mix));
			int green =(int)Math.round(from.getGreen()*mix + to.getGreen()*(1-mix));
			int blue = (int)Math.round(from.getBlue()*mix + to.getBlue()*(1-mix));
			int alpha = (int)Math.round(from.getAlpha()*mix + to.getAlpha()*(1-mix));
			Color c = new Color(red, green, blue, alpha);
			retval[i]=c;
		}
		
		return retval;
	}
}
