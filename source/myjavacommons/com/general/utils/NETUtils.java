package com.general.utils;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class NETUtils {
  public NETUtils() {
  }

  public static String getLocalHostName() {
    String hostname="";
    try {
      InetAddress localHost = InetAddress.getLocalHost();
      hostname=localHost.getHostName();
    } catch (UnknownHostException ex) {
      hostname="<unknown>";
    }
    return hostname;
  }
}
