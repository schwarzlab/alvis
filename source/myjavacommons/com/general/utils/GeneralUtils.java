/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.general.utils;

import com.dictiography.collections.IndexedNavigableMap;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public class GeneralUtils {

	public static int[] whichTrue(boolean[] in) {
		// translate array of boolean into array of matching indices
		int nMatches = 0;
		for (int i = 0; i < in.length; i++) {
			if (in[i]) {
				nMatches++;
			}
		}

		int[] result = new int[nMatches];
		if (nMatches > 0) {
			int k = 0;
			for (int j = 0; j < in.length; j++) {
				if (in[j]) {
					result[k++] = j;
				}
			}
		}
		return result;
	}

	public static double sum(double[] in) {
		double retval = 0;
		for (double i : in) {
			retval += i;
		}
		return retval;
	}

	public static double max(double[] in) {
		double retval = Integer.MIN_VALUE;
		for (double i : in) {
			if (!Double.isNaN(i) && i>retval) retval=i;
		}
		return retval;
	}

	public static double min(double[] in) {
		double retval = Integer.MAX_VALUE;
		for (double i : in) {
			if (i<retval) retval=i;
		}
		return retval;
	}
	
	public static int sum(int[] in) {
		int retval = 0;
		for (int i : in) {
			retval += i;
		}
		return retval;
	}

	public static Double sum(Double[] in) {
		double retval = 0;
		for (double i : in) {
			retval += i;
		}
		return retval;
	}

	public static Integer sum(Integer[] in) {
		int retval = 0;
		for (int i : in) {
			retval += i;
		}
		return retval;
	}

	public static int[] range(int from, int to) {
		return range(from, to, 1);
	}

	public static int[] range(int from, int to, int stepsize) {
		int length = Math.abs(to - from);
		int[] retval = new int[length];
		int value = from;
		for (int count = 0; count < length; count++) {
			retval[count] = value;
			value += stepsize;
		}
		return retval;
	}

	public static int cumsum(int in) {
		int sum = 0;
		for (int i = 1; i <= in; i++) {
			sum += i;
		}
		return sum;
	}

	public static float[] doubleArrayToFloat(double[] in) {
		float[] retval = new float[in.length];
		for (int i = 0; i < in.length; i++) {
			retval[i] = (float) in[i];
		}
		return retval;
	}

	public static int[] integerArrayToInt(Integer[] in) {
		int[] retval = new int[in.length];
		for (int i = 0; i < in.length; i++) {
			retval[i] = in[i];
		}
		return retval;
	}
	
	public static double[] doubleArrayToDouble(Double[] in) {
		double[] retval = new double[in.length];
		for (int i = 0; i < in.length; i++) {
			retval[i] = in[i];
		}
		return retval;
	}

	public static int[] order(final List list, final Comparator comparator) {
		Integer[] indices = new Integer[list.size()];
		for (int i = 0; i < list.size(); i++) {
			indices[i] = i;
		}
		Arrays.sort(indices, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				Integer i1 = (Integer) o1;
				Integer i2 = (Integer) o2;
				return comparator.compare(list.get(i1), list.get(i2));
			}
		});
		return integerArrayToInt(indices);
	}

	public static int[] order(final IndexedNavigableMap list, final Comparator comparator, final boolean byValue) {
		Integer[] indices = new Integer[list.size()];
		for (int i = 0; i < list.size(); i++) {
			indices[i] = i;
		}
		Arrays.sort(indices, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				Integer i1 = (Integer) o1;
				Integer i2 = (Integer) o2;
				if (byValue) {
					return comparator.compare(list.exactEntry(i1).getValue(), list.exactEntry(i2).getValue());
				} else {
					return comparator.compare(list.exactKey(i1), list.exactKey(i2));
				}
			}
		});
		return integerArrayToInt(indices);
	}

	public static int[] order(final double[] values) {
		Integer[] indices = new Integer[values.length];
		for (int i = 0; i < values.length; i++) {
			indices[i] = i;
		}
		Arrays.sort(indices, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				Integer i1 = (Integer) o1;
				Integer i2 = (Integer) o2;
				return Double.compare(values[i1], values[i2]);
			}
		});
		return integerArrayToInt(indices);
	}

	public static int[] order(final int[] values) {
		Integer[] indices = new Integer[values.length];
		for (int i = 0; i < values.length; i++) {
			indices[i] = i;
		}
		Arrays.sort(indices, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				Integer i1 = (Integer) o1;
				Integer i2 = (Integer) o2;
				return Integer.compare(values[i1], values[i2]);
			}
		});
		return integerArrayToInt(indices);
	}
	
	public static void addLibraryPath(String s) throws IOException, IllegalAccessException, NoSuchFieldException {
		System.setProperty("java.library.path", System.getProperty("java.library.path") + File.pathSeparator + s);
		Field fieldSysPath = ClassLoader.class.getDeclaredField( "sys_paths" );
		fieldSysPath.setAccessible( true );
		fieldSysPath.set( null, null );

	}
}
