package com.general.utils;

import java.io.*;
import org.jblas.DoubleMatrix;

/**
 * <p>Title: METATOOL Input File Generator</p>
 * <p>Description: Generates input files for METATOOL</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: Biozentrum University Wuerzburg</p>
 * @author Roland Schwarz
 * @version 0.5
 */

public class IOUtils {
    public IOUtils() {
    }

    public static String readAll(File file) throws IOException{
        BufferedReader in = new BufferedReader(new FileReader(file));
        StringBuffer text=new StringBuffer();
        int c=0;
        while ((c = in.read())!=-1) {
            text.append((char)c);
        }
        return text.toString();
    }

    public static void writeTextToFile(String text, File file) throws IOException {
      BufferedWriter writer=new BufferedWriter(new FileWriter(file));
      writer.write(text);
      writer.flush();
	  writer.close();
    }

	public static String matrixToCSV(DoubleMatrix matrix, Object[] rowHeaders, Object[] colHeaders){
		StringBuilder text=new StringBuilder();

		if (colHeaders!=null && colHeaders.length>0) {	
			for (int j=0;j<matrix.columns;j++) {
				text.append(",");
				text.append(colHeaders[j]);
			}
			text.append("\n");			
		}
		for (int i=0;i<matrix.rows;i++){
			for (int j=0;j<matrix.columns;j++){
				if (rowHeaders!=null) {
					text.append(rowHeaders[i]);
					text.append(",");
				}
				text.append(matrix.get(i, j));
				if (j<(matrix.columns-1)){
					text.append(",");
				} else {
					text.append("\n");
				}
			}
		}
		return text.toString();
	}
}
