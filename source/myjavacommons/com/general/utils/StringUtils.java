package com.general.utils;

/**
 * <p>Title: YANA</p>
 * <p>Description: Yet Another Network Analyzer</p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: University of Wuerzburg, Department for Bioinformatics</p>
 * @author Roland Schwarz
 * @version 0.9
 */

public class StringUtils {
  private StringUtils() {
  }

  public static String noSpace(String text, boolean mayBeEmpty) {
    if (text==null) {
      text="";
    }
    else {
      text=text.replaceAll(" ","_");
    }
    if (!mayBeEmpty && text.length()==0) {
      text="[unknown]";
    }
    return text;
  }

  public static String noSpace(String text) {
    return noSpace(text,false);
  }
  
  public static String firstCharToCapital(String text) {
	  return text.substring(0, 1).toUpperCase() + text.substring(1).toLowerCase();
  }
}
