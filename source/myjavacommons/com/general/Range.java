package com.general;

/**
 * <p>Title: YANA</p>
 *
 * <p>Description: Yet Another Network Analyzer</p>
 *
 * <p>Copyright: Copyright (c) 2004</p>
 *
 * <p>Company: Dep. of Bioinformatics - University Wuerzburg</p>
 *
 * @author Roland Schwarz
 * @version 0.4
 */
public class Range<T extends Comparable> implements IRange<T>{
  private T from=null;
  private T to=null;

  public Range(T from, T to) {
    this.from=from;
    this.to=to;
  }

  public boolean isInRange(T target) {
    return ((from.compareTo(target)<=0) && (to.compareTo(target)>=0));
  }

  public T fitToRange(T target) {
    if (from.compareTo(target)>0) {
      return from;
    } else if (to.compareTo(target)<0) {
      return to;
    }
    return target;
  }

  public void setFrom(T from) {
    this.from=from;
  }

  public T getFrom() {
    return from;
  }

  public void setTo(T to) {
    this.to=to;
  }

  public T getTo() {
    return this.to;
  }

}
