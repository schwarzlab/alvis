package com.general.mvc.view;



import javax.swing.JPanel;

import java.awt.GridLayout;

/**
 * <p>Title: YANA</p>
 *
 * <p>Description: Yet Another Network Analyzer</p>
 *
 * <p>Copyright: Copyright (c) 2004</p>
 *
 * <p>Company: Dep. of Bioinformatics - University Wuerzburg</p>
 *
 * @author Roland Schwarz
 * @version 0.4
 */
public class JPanelViewManager
    implements IViewManager {
  private JPanel panel=null;
  private static final GridLayout layout=new GridLayout(0,1,0,0);

  public JPanelViewManager(JPanel panel) {
    this.panel=panel;
    panel.setLayout(layout);
  }

  /**
   * addView
   *
   * @param view IView
   * @todo Implement this yana.gui.IViewManager method
   */
  public void addView(IView view) {
    panel.add(view.getComponent());
    panel.getParent().validate();
  }

  /**
   * removeView
   *
   * @param view IView
   * @todo Implement this yana.gui.IViewManager method
   */
  public void removeView(IView view) {
    panel.remove(view.getComponent());
    panel.getParent().validate();
  }

  /**
   * removeAllViews
   *
   * @todo Implement this yana.gui.IViewManager method
   */
  public void removeAllViews() {
    panel.removeAll();
    panel.getParent().validate();
  }

  /**
   * activateView
   *
   * @param view IView
   * @todo Implement this yana.gui.IViewManager method
   */
  public void activateView(IView view) {
    view.getComponent().requestFocus();
  }
}
