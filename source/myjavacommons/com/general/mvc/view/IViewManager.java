package com.general.mvc.view;



/**
 * <p>Title: YANA</p>
 *
 * <p>Description: Yet Another Network Analyzer</p>
 *
 * <p>Copyright: Copyright (c) 2004</p>
 *
 * <p>Company: Dep. of Bioinformatics - University Wuerzburg</p>
 *
 * @author Roland Schwarz
 * @version 0.4
 */
public interface IViewManager {
  public void addView(IView view);
  public void removeView(IView view);
  public void removeAllViews();
  public void activateView(IView view);
}
