package com.general.mvc.view;


import javax.swing.JTabbedPane;
import java.awt.Container;
import com.general.Log;

/**
 * <p>Title: YANA</p>
 *
 * <p>Description: Yet Another Network Analyzer</p>
 *
 * <p>Copyright: Copyright (c) 2004</p>
 *
 * <p>Company: Dep. of Bioinformatics - University Wuerzburg</p>
 *
 * @author Roland Schwarz
 * @version 0.4
 */
public class JTabbedPaneViewManager implements IViewManager{
  protected Container pane=null;


  public JTabbedPaneViewManager(Container pane) {
    this.pane=pane;
  }

  public void addView(IView view) {
    pane.add(view.getComponent(),view.getTitle());
//    pane.validate();
  }

  public void removeView(IView view) {
    pane.remove(view.getComponent());
//    pane.validate();
  }

  public void removeAllViews() {
    pane.removeAll();
//    pane.validate();
  }

  public void activateView(IView view) {
    if (pane instanceof JTabbedPane) {
      JTabbedPane tp=(JTabbedPane)pane;
      try {
        tp.setSelectedComponent(view.getComponent());
      }
      catch (IllegalArgumentException ex) {
        Log.out(ex);
      }
    }
  }
}
