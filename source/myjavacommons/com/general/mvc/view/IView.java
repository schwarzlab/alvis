package com.general.mvc.view;


public interface IView extends IDisplayable{
  public void save();
  public boolean verify();
}
