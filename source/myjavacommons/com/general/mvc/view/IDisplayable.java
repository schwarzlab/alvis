package com.general.mvc.view;

import java.awt.*;

public interface IDisplayable {
  public Component getComponent();
  public String getTitle();
  public void closing();
}
