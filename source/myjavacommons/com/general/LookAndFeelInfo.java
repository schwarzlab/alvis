package com.general;

/**
 * <p>Title: YANA</p>
 * <p>Description: Yet Another Network Analyzer</p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: University of Wuerzburg, Department for Bioinformatics</p>
 * @author Roland Schwarz
 * @version 0.9
 */

public class LookAndFeelInfo {
  private String name;
  private String description;
  private String className;

  public LookAndFeelInfo() {
  }

  public LookAndFeelInfo(String name, String className) {
    this.name=name;
    this.className=className;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getClassName() {
    return className;
  }

  public void setClassName(String className) {
    this.className = className;
  }

  public boolean equals(Object o) {
    if (o instanceof LookAndFeelInfo) {
      return className.equals(((LookAndFeelInfo)o).getClassName());
    }
    else {
      return super.equals(o);
    }
  }

  public String toString() {
    return name;
  }
}
