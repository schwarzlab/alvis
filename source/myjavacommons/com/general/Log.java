package com.general;

/**
 * <p>Title: METATOOL Input File Generator</p>
 * <p>Description: Generates input files for METATOOL</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: Biozentrum University Wuerzburg</p>
 * @author Roland Schwarz
 * @version 0.5
 */

public class Log {
    private static boolean debug = false;

    private Log() {
    }

    public static void out(String text) {
        if (debug) {
            System.out.println(text);
        }
    }

    public static void out(Throwable ex) {
        if (debug && ex!=null) {
            System.out.print(ex.getMessage());
            System.out.println();
            ex.printStackTrace();
        }
    }

    public static void out(Object o) {
      if (debug) {
        System.out.println(o);
      }
    }

    public static void out(Object[] o) {
      if (debug) {
        for (Object ob:o)
          System.out.println(o);
      }
    }

    public static boolean isDebug() {
        return debug;
    }

    public static void setDebug(boolean fDebug) {
        debug = fDebug;
    }
}
