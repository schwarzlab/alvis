/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.general.gui.layers;

import com.general.resources.MyJavaResources;
import com.general.utils.GUIutils;
import java.awt.AWTEvent;
import java.awt.Color;
import java.awt.Composite;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import javax.swing.JComponent;
import javax.swing.JLayer;
import javax.swing.JList;
import javax.swing.Timer;
import javax.swing.plaf.LayerUI;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public class ListButtonLayerUI extends LayerUI<JList>{
	private boolean mActive;
	private Timer timer	;
	private Rectangle buttonSelectArea;
	private Rectangle buttonDeleteArea;
	private ActionListener deleteButtonActionListener=null;
	private Rectangle activeArea;
	private Color color=new Color(0,0,0,30);
	
	public ListButtonLayerUI(){
		
	}
	
	public ListButtonLayerUI(Color color) {
		this.color=color;
	}
	
	@Override
	public void paint(Graphics g, JComponent c) {
		super.paint(g, c); //To change body of generated methods, choose Tools | Templates.
		if (!mActive) return;
		
		Graphics2D g2 = (Graphics2D)g.create();
		GUIutils.setQualityRenderingHints(g2);
		
//		int height=c.getHeight();
//		int width=24;
//		int offsetX =c.getWidth()-width; 
//		g2.translate(offsetX, 0);
		int height=c.getVisibleRect().height;
		int width=24;
		int offsetX =c.getVisibleRect().width-width; 
		g2.translate(offsetX + c.getVisibleRect().x, c.getVisibleRect().y);
		
		// draw faded out rectangle
		// Gray it out.
		Composite urComposite = g2.getComposite();
//		g2.setComposite(AlphaComposite.getInstance(
//			AlphaComposite.SRC_OVER, 0.15f));

		g2.setColor(color);
		g2.fillRect(0, 0, width, height);
		g2.setComposite(urComposite);
		
		// draw buttons
		g2.drawImage(MyJavaResources.SELECT_ALL_32x32.getImage(), 0,0,width,width,null);
		if (deleteButtonActionListener!=null) {
			g2.drawImage(MyJavaResources.DELETE_32x32.getImage(), 0,width+5,width,width,null);
		}
		
		activeArea = new Rectangle(offsetX,0,width,height);
		buttonSelectArea = new Rectangle(offsetX, 0, width, width);
		buttonDeleteArea = new Rectangle(offsetX, width+5, width, width);
	}

	@Override
	public void installUI(JComponent c) {
		super.installUI(c);
		final JLayer jlayer = (JLayer)c;
		jlayer.setLayerEventMask(AWTEvent.MOUSE_EVENT_MASK | AWTEvent.MOUSE_MOTION_EVENT_MASK);

		timer = new Timer(1000, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				mActive=false;
				jlayer.repaint();
			}
		});
		timer.setRepeats(false);
		timer.start();
	}

	@Override
	protected void processMouseEvent(MouseEvent e, JLayer<? extends JList> l) {
		if (e.getID() == MouseEvent.MOUSE_ENTERED) {
			mActive = true;
		} else if (e.getID() == MouseEvent.MOUSE_EXITED) {
			mActive = false;
		} else if (e.getID() == MouseEvent.MOUSE_CLICKED) {
			if (buttonDeleteArea.contains(e.getPoint())) {
				if (deleteButtonActionListener!=null){
					ActionEvent actionEvent = new ActionEvent(l, ActionEvent.ACTION_FIRST, "delete", e.getModifiers());
					deleteButtonActionListener.actionPerformed(actionEvent);
				}
			} else if (buttonSelectArea.contains(e.getPoint())) {
				l.getView().getSelectionModel().addSelectionInterval(0, l.getView().getModel().getSize()-1);
			} 
		}
		
		if (mActive && activeArea!=null && activeArea.contains(e.getPoint())) {
			e.consume();		
		}
		l.repaint();		
	}

	@Override
	protected void processMouseMotionEvent(MouseEvent e, JLayer<? extends JList> l) {
		mActive=true;
		l.repaint();
		timer.restart();
	}

	/**
	 * @return the deleteButtonActionListener
	 */
	public ActionListener getDeleteButtonActionListener() {
		return deleteButtonActionListener;
	}

	/**
	 * @param deleteButtonActionListener the deleteButtonActionListener to set
	 */
	public void setDeleteButtonActionListener(ActionListener deleteButtonActionListener) {
		this.deleteButtonActionListener = deleteButtonActionListener;
	}

	/**
	 * @return the color
	 */
	public Color getColor() {
		return color;
	}

	/**
	 * @param color the color to set
	 */
	public void setColor(Color color) {
		this.color = color;
	}

	
	
	
}
