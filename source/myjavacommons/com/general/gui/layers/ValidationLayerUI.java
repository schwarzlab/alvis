/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.general.gui.layers;

import com.general.resources.MyJavaResources;
import com.general.utils.GUIutils;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JLayer;
import javax.swing.JTextField;
import javax.swing.plaf.LayerUI;

public class ValidationLayerUI extends LayerUI {
	private boolean valid=true;

	@Override
	public void paint (Graphics g, JComponent c) {
		super.paint (g, c);

		JLayer jlayer = (JLayer)c;
		if (jlayer.getView() instanceof JFormattedTextField){
			JFormattedTextField ftf = (JFormattedTextField)jlayer.getView();
			this.valid = ftf.isEditValid();
		}
		Graphics2D g2 = (Graphics2D)g.create();
		GUIutils.setQualityRenderingHints(g2);
		
		int height = (int)(c.getHeight()*0.75);
		int width = height;
		
		if (this.valid) {
			g2.drawImage(MyJavaResources.OK_32x32.getImage(), c.getWidth()-width-5, (c.getHeight()-height)/2, width, height, null);
		}
		if (!this.valid) {
			g2.drawImage(MyJavaResources.NOTOK_32x32.getImage(), c.getWidth()-width-5, (c.getHeight()-height)/2, width, height, null);
//			int w = c.getWidth();
//			int h = c.getHeight();
//			int s = 8;
//			int pad = 4;
//			int x = w - pad - s;
//			int y = (h - s) / 2;
//			g2.setPaint(Color.red);
//			g2.fillRect(x, y, s + 1, s + 1);
//			g2.setPaint(Color.white);
//			g2.drawLine(x, y, x + s, y + s);
//			g2.drawLine(x, y + s, x + s, y);
//			g2.dispose();
		}
	}

	/**
	 * @return the isValid
	 */
	public boolean isValid() {
		return valid;
	}

	/**
	 * @param isValid the isValid to set
	 */
	public void setValid(boolean isValid) {
		this.valid = isValid;
	}
}