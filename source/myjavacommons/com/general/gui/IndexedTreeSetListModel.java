/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.general.gui;

import com.dictiography.collections.IndexedTreeSet;
import java.util.Collection;
import java.util.Comparator;
import javax.swing.AbstractListModel;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 * 
 */
public class IndexedTreeSetListModel<T> extends AbstractListModel<T> {
	private final IndexedTreeSet<T> data;
	
	public IndexedTreeSetListModel() {
		data = new IndexedTreeSet<>();
	}
	public IndexedTreeSetListModel(Comparator<T> comparator) {
		data = new IndexedTreeSet<>(comparator);
	}
	public IndexedTreeSetListModel(Comparator<T> comparator, Collection<? extends T> data) {
		this.data = new IndexedTreeSet<>(comparator);
		this.data.addAll(data);
	}
	

	@Override
	public int getSize() {
		return data.size();
	}

	@Override
	public T getElementAt(int index) {
		return data.exact(index);
	}
	
	public void clear() {
		int max = data.size()-1;
		data.clear();
		if (max>=0) {
			fireIntervalRemoved(this, 0, max);
		}
	}
	
	public boolean addElement(T object) {
		boolean retval = data.add(object);
		int index = data.entryIndex(object);
		fireIntervalAdded(this, index, index);
		return retval;
	}
	
	public boolean removeElement(T object) {
		if (data.contains(object)) {
			int index = data.entryIndex(object);
			boolean retval = data.remove(object);
			fireIntervalRemoved(this, index, index);
			return retval;
		} else {
			return false;
		}
	}
	
	
	
}
