package com.general.gui.dialog;

import com.general.gui.noia.NoiaIconRepository;
import com.general.gui.panels.MultiLineTextPanel;
import com.general.mvc.view.IDisplayable;
import com.general.mvc.view.IView;
import com.general.utils.GUIutils;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.FlowLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileFilter;

/**
 * <p>�berschrift: Ridom MLST2</p>
 * <p>Beschreibung: </p>
 * <p>Copyright: Copyright (c) Ridom GmbH 2002 - 2003</p>
 * <p>Organisation: Ridom GmbH</p>
 * @author Roland Schwarz
 * @version 0.9
 */

public class DialogFactory {
    public static File lastDir=new File(".");
    private static Component defaultComponent=null;
	
    public DialogFactory() {
    }

	public static void setDefaultComponent(Component c) {
		defaultComponent=c;
	}
	
	public static Component getDefaultComponent() {
		return defaultComponent;
	}
	
    public static File showChooseDirectoryDialog() { return showChooseDirectoryDialog(defaultComponent); }
	public static File showChooseDirectoryDialog(Component parentComponent) {
        File chosenDirectory=null;

        // display directory chooser dialog
        JFileChooser fileChooser=new JFileChooser(lastDir);
        fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        fileChooser.setDialogTitle("Choose a directory...");
        fileChooser.setMultiSelectionEnabled(false);
        if (fileChooser.showOpenDialog(parentComponent)==JFileChooser.APPROVE_OPTION) {
            // user confirmed the dialog
            chosenDirectory=fileChooser.getSelectedFile();
            lastDir=fileChooser.getCurrentDirectory();
        }
        return chosenDirectory;
    }

    public static File showSingleFileOpenDialog() { return showSingleFileOpenDialog(defaultComponent, null); }
    public static File showSingleFileOpenDialog(Component parentComponent) { return showSingleFileOpenDialog(parentComponent, null); }
    public static File showSingleFileOpenDialog(FileFilter[] filters) { return showSingleFileOpenDialog(defaultComponent, filters); }
    public static File showSingleFileOpenDialog(Component parentComponent, FileFilter[] filters) {
        File chosenFile=null;

        // display file chooser dialog
	JFileChooser fileChooser=createFileOpenDialog("Choose your file...",  filters);
        fileChooser.setCurrentDirectory(lastDir);
        if (fileChooser.showOpenDialog(parentComponent)==JFileChooser.APPROVE_OPTION) {
            // user confirmed the dialog
            chosenFile=fileChooser.getSelectedFile();
            lastDir=fileChooser.getCurrentDirectory();
        }

        return chosenFile;
    }

    public static File[] showMultiFileOpenDialog() { return showMultiFileOpenDialog(defaultComponent, null); }
	public static File[] showMultiFileOpenDialog(Component parentComponent) { return showMultiFileOpenDialog(parentComponent, null); }
	public static File[] showMultiFileOpenDialog(Component parentComponent, FileFilter[] filters) {
        File[] chosenFiles=null;

        // display file chooser dialog
        JFileChooser fileChooser=new JFileChooser(lastDir);
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fileChooser.setDialogTitle("Choose your files...");
        fileChooser.setMultiSelectionEnabled(true);
        if (fileChooser.showOpenDialog(parentComponent)==JFileChooser.APPROVE_OPTION) {
            // user confirmed the dialog
            chosenFiles=fileChooser.getSelectedFiles();
            lastDir=fileChooser.getCurrentDirectory();
        }

        return chosenFiles;
    }

	public static File showFileSaveDialog() { return showFileSaveDialog(defaultComponent, null, null); }
    public static File showFileSaveDialog(FileFilter[] filters) {	return showFileSaveDialog(defaultComponent, filters, null); }
	public static File showFileSaveDialog(Component parentComponent) { return showFileSaveDialog(parentComponent, null, null); }
    public static File showFileSaveDialog(FileFilter[] filters, JComponent accessory) { return showFileSaveDialog(defaultComponent, filters, accessory); }
	public static File showFileSaveDialog(Component parentComponent, FileFilter[] filters, JComponent accessory) {
        File chosenFile=null;
        JFileChooser fileChooser=createFileSaveDialog("Choose the file to save to:",  filters);
		if (accessory!=null) {
			fileChooser.setAccessory(accessory);
		}
        if (fileChooser.showSaveDialog(parentComponent)==JFileChooser.APPROVE_OPTION) {
            // user confirmed the dialog
            chosenFile=fileChooser.getSelectedFile();
            lastDir=fileChooser.getCurrentDirectory();
            if (chosenFile.exists()) {
                if (JOptionPane.showConfirmDialog(parentComponent,
                                              "File already exists! Overwrite?",
                                              "Confirm:",
                                              JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE,NoiaIconRepository.createImageIcon(NoiaIconRepository.Images.IMG_32X32_ACTIONS_HELP_PNG))==1) {
                    chosenFile=null;
                }
            }
        }
        return chosenFile;
    }

	public static JFileChooser createFileSaveDialog(String title) {	return createFileSaveDialog(title, null); }
    public static JFileChooser createFileSaveDialog(String title, FileFilter[] filters) {
      JFileChooser fileChooser=new JFileChooser(lastDir);
      fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
      fileChooser.setDialogTitle(title);
      fileChooser.setMultiSelectionEnabled(false);
	  boolean first=true;
		if (filters!=null) {
			for (FileFilter ff: filters) {
				fileChooser.addChoosableFileFilter(ff);
				if (first) {
					fileChooser.setFileFilter(ff);
					first=false;
				}
			}
		}
      return fileChooser;
    }

	public static JFileChooser createFileOpenDialog(String title) { return createFileOpenDialog(title, null); }
    public static JFileChooser createFileOpenDialog(String title, FileFilter[] filters) {
      JFileChooser fileChooser=new JFileChooser(lastDir);
      fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
      fileChooser.setDialogTitle(title);
      fileChooser.setMultiSelectionEnabled(false);
	  boolean first=true;
	  if (filters!=null) {
			for (FileFilter ff: filters) {
				fileChooser.addChoosableFileFilter(ff);
				if (first) {
					fileChooser.setFileFilter(ff);
					first=false;
				}

			}
		}
     return fileChooser;
    }

    public static JFileChooser createDirOpenDialog(String title) {
      JFileChooser fileChooser=new JFileChooser(lastDir);
      fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
      fileChooser.setDialogTitle(title);
      fileChooser.setMultiSelectionEnabled(false);
      return fileChooser;
    }

    public static void setLastDir(File newlastDir) {
      lastDir = newlastDir;
    }

    public static void showWarningsDialog(List<? extends Throwable> warnings,String title,Icon icon) { showWarningsDialog(defaultComponent, warnings, title, icon); }
	public static void showWarningsDialog(Component parentComponent, List<? extends Throwable> warnings,String title,Icon icon) {
      StringBuffer message=new StringBuffer();
      for (Throwable t:warnings) {
        message.append(t.getMessage());
        message.append('\n');
      }
      JOptionPane.showMessageDialog(parentComponent,new MultiLineTextPanel(message.toString()),title,JOptionPane.ERROR_MESSAGE,icon);
    }

    public static void showErrorMessage(String errorMessage, String errorTitle, Icon icon) { showErrorMessage(defaultComponent, errorMessage, errorTitle, icon);}
	public static void showErrorMessage(Component parentComponent, String errorMessage, String errorTitle, Icon icon) {
		MultiLineTextPanel panel = new MultiLineTextPanel(errorMessage);
		JOptionPane.showMessageDialog(parentComponent,panel,errorTitle,JOptionPane.ERROR_MESSAGE,icon);
    }
    public static void showErrorMessage(Exception ex, String errorTitle) { 
		StringWriter sw = new StringWriter();
		ex.printStackTrace(new PrintWriter(sw));
		showErrorMessage(defaultComponent, ex.getMessage() + "\n" + sw.toString(), errorTitle); 
	}
	
	public static void showErrorMessage(String errorMessage, String errorTitle) { showErrorMessage(defaultComponent, errorMessage, errorTitle); }
	public static void showErrorMessage(Component parentComponent, String errorMessage, String errorTitle) {
      showErrorMessage(parentComponent,errorMessage,errorTitle,NoiaIconRepository.createImageIcon(NoiaIconRepository.Images.IMG_32X32_ACTIONS_MESSAGEBOX_CRITICAL_PNG));
    }

    public static boolean showFileOverwriteConfirmDialog(File file) { return showFileOverwriteConfirmDialog(file, defaultComponent); }
	public static boolean showFileOverwriteConfirmDialog(File file, Component parent) {
      if (JOptionPane.showConfirmDialog(parent,
                                        "File '"+file.getAbsolutePath()+"' already exists.\nReplace?",
                                        "Confirm replace",
                                        JOptionPane.YES_NO_OPTION,
                                        JOptionPane.QUESTION_MESSAGE)==JOptionPane.YES_OPTION) {
        return true;
      }
      return false;
    }

    public static boolean showConfirmDialog(String message) { return showConfirmDialog(message, defaultComponent); }
	public static boolean showConfirmDialog(String message, Component parent) {
      if (JOptionPane.showConfirmDialog(parent,
                                        message,
                                        "Confirm:",
                                        JOptionPane.OK_CANCEL_OPTION,
                                        JOptionPane.QUESTION_MESSAGE)==JOptionPane.OK_OPTION) {
        return true;
      }
      return false;
    }
	
    public static File getLastDir() {
      return lastDir;
    }

   
	public static boolean showModalOKCancelDialog(final IView view) { return showModalOKCancelDialog(view, defaultComponent); }
	public static boolean showModalOKCancelDialog(final IView view, Component parentComponent) {
      boolean retval=false;

      final JOptionPane pane = new JOptionPane(view.getComponent(),JOptionPane.PLAIN_MESSAGE,JOptionPane.OK_CANCEL_OPTION);
//      final JDialog dlg=pane.createDialog(parentComponent, view.getTitle());
	  final JDialog dlg = new JDialog(SwingUtilities.windowForComponent(parentComponent), view.getTitle(), Dialog.ModalityType.APPLICATION_MODAL);
	  dlg.setContentPane(pane);
	  dlg.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
      dlg.setResizable(true);
      dlg.setModal(true);

      dlg.addWindowListener(new WindowAdapter() {
		@Override
        public void windowClosing(WindowEvent e) {
          pane.setValue(JOptionPane.CANCEL_OPTION);
        }
      });
	  
      pane.addPropertyChangeListener(JOptionPane.VALUE_PROPERTY, new PropertyChangeListener() {
		@Override
        public void propertyChange(PropertyChangeEvent e) {
          if (dlg.isVisible() && (e.getSource() == pane)) {

            if (e.getNewValue()==JOptionPane.UNINITIALIZED_VALUE) {
              // ignore reset
              return;
            }

            int value=(Integer)e.getNewValue();
            switch (value) {
              case JOptionPane.OK_OPTION:
                // ok clicked
                if(view.verify()) {
                  view.save();
                  dlg.setVisible(false);
                  view.closing();
                } else {
                  // do reset, otherwise a second click on the same button will not raise an event
                  pane.setValue(JOptionPane.UNINITIALIZED_VALUE);
                }
                break;
              case JOptionPane.CANCEL_OPTION:
                // cancel clicked
                dlg.setVisible(false);
                view.closing();
                break;
              default:
                // do nothing
            }
          }
        }
      });

      dlg.pack();
      dlg.setLocationRelativeTo(parentComponent);
      dlg.setVisible(true);
      if (pane.getValue().equals(JOptionPane.OK_OPTION)) {
        return true;
      }
      return false;
    }

	public static boolean showOkCancelApplyDialog(final IView view, boolean modal) { return showOkCancelApplyDialog(view, modal, defaultComponent); }
	public static boolean showOkCancelApplyDialog(final IView view, boolean modal, Component parentComponent) {
		JDialog.ModalityType modality;
		if (modal) {
			modality = JDialog.ModalityType.APPLICATION_MODAL;
		} else {
			modality = JDialog.ModalityType.MODELESS;
		}
		JDialog dlg = new JDialog(SwingUtilities.windowForComponent(parentComponent), view.getTitle(),  modality);
		dlg.setLayout(new BorderLayout());
		ButtonPanel panButtons = new ButtonPanel(view, true);
		dlg.add(panButtons, BorderLayout.SOUTH);
		dlg.add(view.getComponent(), BorderLayout.CENTER);
		dlg.setTitle(view.getTitle());
		dlg.pack();
		dlg.addWindowListener(new WindowAdapter() {
		  @Override
		  public void windowClosing(WindowEvent e) {
			view.closing();
		  }
		});
		dlg.setLocationRelativeTo(parentComponent);
		dlg.setVisible(true);
		return panButtons.getChoice()==ButtonPanel.ButtonPanelChoice.OK;
	}
	
	public static JDialog showModelessCloseDialog(final IDisplayable dis) { return showModelessCloseDialog(dis, defaultComponent); }
	public static JDialog showModelessCloseDialog(final IDisplayable dis, Component parentComponent) {
      JOptionPane pane = new JOptionPane(dis.getComponent(),JOptionPane.PLAIN_MESSAGE,JOptionPane.OK_OPTION,null,new Object[]{"Close"});
      JDialog dlg=pane.createDialog(parentComponent,dis.getTitle());
      dlg.setResizable(true);
      dlg.setModal(false);
      dlg.setAlwaysOnTop(false);
      dlg.addWindowListener(new WindowAdapter() {
        public void windowClosing(WindowEvent e) {
          dis.closing();
        }
      });
      dlg.setVisible(true);
      return dlg;
    }

    public static JFrame showInFrame(final IDisplayable dis) {
      JFrame frm=new JFrame(dis.getTitle());
      frm.getContentPane().add(dis.getComponent());
      //frm.setIconImage(MainFrame.getInstance().getIconImage());
      frm.addWindowListener(new WindowAdapter() {
        public void windowClosing(WindowEvent e) {
          dis.closing();
        }
      });
      frm.pack();
      frm.setLocation(GUIutils.getPointForCentering(frm,false));
      frm.setVisible(true);
      return frm;
    }

	public static String showInputDialog(Object message, Object initialValue) { return showInputDialog(defaultComponent, message, initialValue); }
	public static String showInputDialog(Component parentComponent, Object message, Object initialValue) {
		return JOptionPane.showInputDialog(parentComponent, message, initialValue);
	}
	
	public static Object showInputDialog(Object message, String title, Object[] selectionValues, Object initialSelection){
		return JOptionPane.showInputDialog(defaultComponent, message, title, JOptionPane.QUESTION_MESSAGE, null, selectionValues, initialSelection);
	}
	
	public static void showInformationDialog(Object message, String title) { showInformationDialog(defaultComponent, message, title); }
	public static void showInformationDialog(Component parentComponent, Object message, String title) {
		JOptionPane.showMessageDialog(parentComponent, message, title, JOptionPane.INFORMATION_MESSAGE, NoiaIconRepository.createImageIcon(NoiaIconRepository.Images.IMG_32X32_ACTIONS_INFO_PNG));
	}

	
	public static class ButtonPanel extends JPanel {

		public static enum ButtonPanelChoice { NONE, OK, CANCEL };
		private final JButton cmdOk = new JButton("OK");
		private final JButton cmdCancel = new JButton("Cancel");
		private final JButton cmdApply = new JButton("Apply");
		private final IView view;
		ButtonPanelChoice choice = ButtonPanelChoice.NONE;
		
		public ButtonPanel(IView view) {
			this(view, true);
		}
		
		public ButtonPanel(IView view, boolean showApplyButton) {
			this.view = view;
			this.setLayout(new FlowLayout(FlowLayout.RIGHT));
			this.add(cmdOk);
			this.add(cmdCancel);
			if (showApplyButton) {
				this.add(cmdApply);
			}
			setupListeners();
		}

		private void setupListeners() {
			cmdOk.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					if (view.verify()) {
						view.save();
						choice = ButtonPanelChoice.OK;
						SwingUtilities.windowForComponent(ButtonPanel.this).dispose();
					}
				}
			});
			
			cmdCancel.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					choice = ButtonPanelChoice.CANCEL;
					SwingUtilities.windowForComponent(ButtonPanel.this).dispose();
				}
			});
			
			cmdApply.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					if (view.verify()) {
						choice =  ButtonPanelChoice.NONE;
						view.save();
					}
				}
			});
		}
		
		public ButtonPanelChoice getChoice() {
			return choice;
		}
	}
}
