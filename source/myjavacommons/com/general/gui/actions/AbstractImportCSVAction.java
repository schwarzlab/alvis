package com.general.gui.actions;

import java.io.*;

import java.awt.event.*;
import javax.swing.*;

import com.general.gui.dialog.*;
import com.general.io.*;
import com.generationjava.io.*;
import java.awt.Component;
import com.general.Log;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */

public abstract class AbstractImportCSVAction extends AbstractAction {

  public AbstractImportCSVAction(Icon icon) {
    super("Import from CSV...",icon);
    putValue(SHORT_DESCRIPTION,"Import from CSV file");
  }

  public void actionPerformed(ActionEvent e) {
    if (!(e.getSource()instanceof Component)) {
      Log.out("Invalid Action source");
      return;
    }

    Component source=(Component)e.getSource();
    JFileChooser fileChooser=DialogFactory.createFileOpenDialog("Choose CSV file:");
    if (fileChooser.showOpenDialog(source)==JFileChooser.APPROVE_OPTION) {
      File file = fileChooser.getSelectedFile();
      if (file!=null) {
        try {
          CsvReader reader = new CsvReader(new FileReader(file));
          reader.setBlockDelimiter('\n');
          reader.setFieldDelimiter(',');
          String[] line=null;
          line =reader.readLine(); // throw away first (header) line
          while ((line=reader.readLine())!=null) {
            importLine(line);
          }
        }
        catch (FileNotFoundException ex) {
          DialogFactory.showErrorMessage(source,ex.getMessage(),"File not found error");
        }
        catch (IOException ex) {
          DialogFactory.showErrorMessage(source,ex.getMessage(),"IO error");
        }
        catch (ParseException ex) {
          DialogFactory.showErrorMessage(source,ex.getMessage(),"Parse error");
        }
      }
      DialogFactory.setLastDir(file);
      finish();
    }
  }

  abstract protected void importLine(String[] line) throws ParseException;

  protected void finish(){}
}
