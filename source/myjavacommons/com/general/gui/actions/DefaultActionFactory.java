package com.general.gui.actions;

import com.general.gui.noia.NoiaIconRepository;
import java.awt.*;
import java.awt.print.*;
import javax.swing.*;
import javax.swing.table.*;

import com.general.gui.panels.table.*;

public class DefaultActionFactory {

  public static PrintAction createPrintAction(Printable p) {
    return new PrintAction(NoiaIconRepository.createImageIcon(NoiaIconRepository.Images.IMG_22X22_ACTIONS_FILEPRINT_PNG),p);
  }

  public static TableExportAction createCSVTableExportAction(TableModel tableModel,Component parent) {
    return new TableExportAction(NoiaIconRepository.createImageIcon(NoiaIconRepository.Images.IMG_22X22_ACTIONS_ARK_EXTRACT_PNG),tableModel,CSVTableExporter.getInstance(),parent);
  }

//  public static MatrixExportAction createCSVMatrixExportAction(Matrix matrix, Component parent) {
//    return new MatrixExportAction(IconRepository.SMALL_EXPORT_ICON,matrix,CSVTableExporter.getInstance(),parent);
//  }

  public static SaveTextToFileAction createSaveTextToFileAction(String text, Component parent) {
    return new SaveTextToFileAction(NoiaIconRepository.createImageIcon(NoiaIconRepository.Images.IMG_22X22_ACTIONS_ARK_EXTRACT_PNG),text,parent);
  }

  public static PrintAction createPrintAction(JTable t) {
	  ImageIcon icon = NoiaIconRepository.createImageIcon(NoiaIconRepository.Images.IMG_22X22_ACTIONS_FILEPRINT_PNG);
    return new PrintAction(icon,t,null);
  }

  public static PrintAction createPrintAction(TablePanel p) {
    return new PrintAction(NoiaIconRepository.createImageIcon(NoiaIconRepository.Images.IMG_22X22_ACTIONS_FILEPRINT_PNG),p.getTable(),p.getTitle());
  }

  public static SearchInTableAction createSearchInTableAction(JTable table) {
    return new SearchInTableAction(table);
  }
}
