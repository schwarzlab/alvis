package com.general.gui.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.net.URL;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;

import javax.swing.event.EventListenerList;

/**
 * Abstract Action for the JLF. Defines some useful methods.
 *
 * @version 1.6 06/12/00
 * @author  Mark Davidson
 */
public class MacroAction extends AbstractAction {

    // The listener to action events
    private EventListenerList listeners;

    //
    // These next public methods may belong in the AbstractAction class.
    //

    /**
     * Gets the value from the key Action.ACTION_COMMAND_KEY
     */
    public String getActionCommand()  {
        return (String)getValue(Action.ACTION_COMMAND_KEY);
    }

    /**
     * Gets the value from the key Action.SHORT_DESCRIPTION
     */
    public String getShortDescription()  {
        return (String)getValue(Action.SHORT_DESCRIPTION);
    }

    /**
     * Gets the value from the key Action.LONG_DESCRIPTION
     */
    public String getLongDescription()  {
        return (String)getValue(Action.LONG_DESCRIPTION);
    }

    public String getName() {
      return (String)getValue(Action.NAME);
    }


    // ActionListener registration and invocation.

    /**
     * Forwards the ActionEvent to the registered listener.
     */
    public void actionPerformed(ActionEvent evt)  {
        if (listeners != null) {
            Object[] listenerList = listeners.getListenerList();

            // Recreate the ActionEvent and stuff the value of the ACTION_COMMAND_KEY
            ActionEvent e = new ActionEvent(evt.getSource(), evt.getID(),
                                            (String)getValue(Action.ACTION_COMMAND_KEY));
            // Because we get the listenerList as an array containing in pairs the type of
            // the listener and the listener itself we have to take always the second
            for (int i = 0; i <= listenerList.length-2; i += 2) {
                ((Action)listenerList[i+1]).actionPerformed(e);
            }
        }
    }

    public void addAction(Action l)  {
        if (listeners == null) {
            listeners = new EventListenerList();
        }
        listeners.add(Action.class, l);
    }

    public void removeAction(Action l)  {
        if (listeners == null) {
            return;
        }
        listeners.remove(Action.class, l);
    }

    public void setEnabled(boolean val) {
      if (listeners != null) {
        Object[] listenerList = listeners.getListenerList();
        for (int i = 0; i <= listenerList.length-2; i += 2) {
          ((Action)listenerList[i+1]).setEnabled(val);
        }
      }
    }

}
