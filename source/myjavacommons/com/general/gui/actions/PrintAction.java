package com.general.gui.actions;

import java.awt.event.*;
import java.awt.print.*;
import javax.swing.*;

import com.general.gui.dialog.*;


public class PrintAction extends AbstractAction{
  private IPrinter state=null;
  private Printable printable=null;
  private JTable table=null;
  private String title="";

  public PrintAction(Icon icon,Printable printable) {
    super("Print...",icon);
    putValue(SHORT_DESCRIPTION,"Prints the current view");
    this.printable=printable;
    this.state=PrintablePrinter.getInstance();
  }

  public PrintAction(Icon icon,JTable table, String title) {
    super("Print...",icon);
    putValue(SHORT_DESCRIPTION,"Prints the current view");
    this.table=table;
    this.title=title;
    this.state=JTablePrinter.getInstance();
  }

  /**
   * actionPerformed
   *
   * @param e ActionEvent
   */
  public void actionPerformed(ActionEvent e) {
    try {
      state.doPrint(this);
    }
    catch (PrinterException ex) {
      DialogFactory.showErrorMessage(table,ex.getMessage(),"Print error");
    }
  }

  public Printable getPrintable() {
    return this.printable;
  }

  public JTable getTable() {
    return this.table;
  }

  public String getTitle() {
    return this.title;
  }

  public void setTitle(String title) {
    this.title = title;
  }
}
