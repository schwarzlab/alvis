package com.general.gui.actions;

import java.awt.print.PrinterException;

/**
 * <p>Title: YANA</p>
 *
 * <p>Description: Yet Another Network Analyzer</p>
 *
 * <p>Copyright: Copyright (c) 2004</p>
 *
 * <p>Company: Dep. of Bioinformatics - University Wuerzburg</p>
 *
 * @author Roland Schwarz
 * @version 0.4
 */
public interface IPrinter {
  public void doPrint(PrintAction action) throws PrinterException;
}
