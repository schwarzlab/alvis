package com.general.gui.actions;

import java.awt.Component;
import java.awt.event.ActionEvent;

import javax.swing.*;
import javax.swing.table.TableModel;

import com.general.gui.noia.NoiaIconRepository;
import com.general.gui.panels.MultiLineTextPanel;
import com.general.gui.panels.table.ITableExporter;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */

public class TableExportAction extends AbstractAction{
  private TableModel tableModel=null;
  private ITableExporter tableExporter=null;
  private Component parent=null;

  public TableExportAction(Icon icon,TableModel tableModel, ITableExporter tableExporter, Component parent) {
    super(tableExporter.toString(),icon);
    putValue(SHORT_DESCRIPTION,tableExporter.toString());
    this.tableExporter=tableExporter;
    this.tableModel=tableModel;
    this.parent=parent;
  }

  /**
   * actionPerformed
   *
   * @param e ActionEvent
   */
  public void actionPerformed(ActionEvent e) {
      if (tableModel!=null)
          JOptionPane.showMessageDialog(parent,new MultiLineTextPanel(tableExporter.export(tableModel)),"Export data",JOptionPane.PLAIN_MESSAGE);
      else
          JOptionPane.showMessageDialog(parent,
                                        "No data to export",
                                        "Export data",
                                        JOptionPane.WARNING_MESSAGE,
                                        NoiaIconRepository.createImageIcon(NoiaIconRepository.Images.IMG_32X32_ACTIONS_MESSAGEBOX_WARNING_PNG));
  }


}
