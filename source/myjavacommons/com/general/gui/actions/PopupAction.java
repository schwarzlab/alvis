package com.general.gui.actions;

import java.awt.event.*;
import javax.swing.*;
import java.awt.Component;

/**
 * <p>Title: YANA</p>
 *
 * <p>Description: Yet Another Network Analyzer</p>
 *
 * <p>Copyright: Copyright (c) 2004</p>
 *
 * <p>Company: Dep. of Bioinformatics - University Wuerzburg</p>
 *
 * @author Roland Schwarz
 * @version 0.4
 */
public class PopupAction
    extends AbstractAction {
  private final JPopupMenu popup = new JPopupMenu();

  public PopupAction() {}

  public PopupAction(String name) {
    super(name);
  }

  public PopupAction(String name,Icon icon) {
    super(name,icon);
  }

  /**
   * Invoked when an action occurs.
   *
   * @param e ActionEvent
   * @todo Implement this java.awt.event.ActionListener method
   */
  public void actionPerformed(ActionEvent e) {
    Object o=e.getSource();
    if (o!=null && o instanceof Component) {
      Component comp=(Component)o;
      popup.show(comp,comp.getWidth(),0);
    }
  }

  public JMenuItem add(Action action) {
    return popup.add(action);
  }

  public JMenuItem add(JMenuItem item) {
    return popup.add(item);
  }
}
