package com.general.gui.actions;

import java.io.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import com.general.gui.dialog.*;
import com.general.utils.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */

public class SaveTextToFileAction extends AbstractAction{
  private String text="";
  private Component parent=null;

  public SaveTextToFileAction(Icon icon,String text, Component parent) {
    super("Save to file...",icon);
    putValue(SHORT_DESCRIPTION,"Save the text to a file");
    this.text=text;
    this.parent=parent;
  }

  /**
   * actionPerformed
   *
   * @param e ActionEvent
   */
  public void actionPerformed(ActionEvent e) {
//    JFileChooser fileChooser=DialogFactory.createFileSaveDialog("Choose file:");
//    if (fileChooser.showSaveDialog(parent)==JFileChooser.APPROVE_OPTION) {
//      boolean write=true;
//      File file=fileChooser.getSelectedFile();
//      if (file.exists()) {
//        if (!DialogFactory.showFileOverwriteConfirmDialog(file,parent)) {
//          write=false;
//        }
//      }
    File file=DialogFactory.showFileSaveDialog(parent);

    if (file!=null) {
      try {
        IOUtils.writeTextToFile(text, file);
      } catch (IOException ex) {
        DialogFactory.showErrorMessage(parent,ex.getMessage(),"Error writing file");
      }
    }
  }

}
