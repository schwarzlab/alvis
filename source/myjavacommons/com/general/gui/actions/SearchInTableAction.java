package com.general.gui.actions;

import java.awt.event.*;
import javax.swing.*;

import com.general.gui.panels.table.*;
import com.general.gui.noia.NoiaIconRepository;

/**
 * <p>Title: YANA</p>
 *
 * <p>Description: Yet Another Network Analyzer</p>
 *
 * <p>Copyright: Copyright (c) 2004 Roland Schwarz\nThis software is licensed
 * under the GPL.\n See the licensing file that came with this distribution or
 * http://www.gnu.org/copyleft/gpl.html</p>
 *
 * <p>Company: Dep. of Bioinformatics - University Wuerzburg</p>
 *
 * @author Roland Schwarz
 * @version 0.4
 */
public class SearchInTableAction
    extends AbstractAction {
  private final JTable table;

  private static TableSearchPanel searchPanel=new TableSearchPanel();
  private static JOptionPane pane=new JOptionPane(searchPanel,JOptionPane.INFORMATION_MESSAGE,
                                                  0,
                                                  NoiaIconRepository.createImageIcon(NoiaIconRepository.Images.IMG_32X32_ACTIONS_FIND_PNG),
                                                  new Object[]{"Close"});
  private static JDialog dialog=pane.createDialog(null,searchPanel.getTitle());
  static {
    dialog.setAlwaysOnTop(true);
    dialog.setModal(false);
    dialog.pack();
  }

  public SearchInTableAction(JTable searchTable) {
    super("Search...",NoiaIconRepository.createImageIcon(NoiaIconRepository.Images.IMG_22X22_ACTIONS_FIND_PNG));
    putValue(SHORT_DESCRIPTION,"Search in table using regular expression!");
    this.table=searchTable;

    this.table.addFocusListener(new FocusListener() {
      public void focusGained(FocusEvent e) {
        searchPanel.setTable(table);
      }
      public void focusLost(FocusEvent e) {
      }
    });
  }

  /**
   * Invoked when an action occurs.
   *
   * @param e ActionEvent
   * @todo Implement this java.awt.event.ActionListener method
   */
  public void actionPerformed(ActionEvent e) {
    searchPanel.setTable(table);
    dialog.setVisible(true);
  }
}
