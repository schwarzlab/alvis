package com.general.gui.actions;

import javax.swing.JTable;
import java.awt.print.*;
import java.text.MessageFormat;

/**
 * <p>Title: YANA</p>
 *
 * <p>Description: Yet Another Network Analyzer</p>
 *
 * <p>Copyright: Copyright (c) 2004</p>
 *
 * <p>Company: Dep. of Bioinformatics - University Wuerzburg</p>
 *
 * @author Roland Schwarz
 * @version 0.4
 */
public class JTablePrinter implements IPrinter{
  private static JTablePrinter _INSTANCE = null;

  public static JTablePrinter getInstance() {
    if (_INSTANCE == null) {
      _INSTANCE = new JTablePrinter();
    }
    return _INSTANCE;
  }

  private JTablePrinter() {
  }

  public void doPrint(PrintAction action) throws PrinterException{
    JTable table=action.getTable();
    String title=action.getTitle();
    if (title==null) {
      title="";
    }

    if (table!=null) {
      table.print(JTable.PrintMode.FIT_WIDTH,new MessageFormat(title), new MessageFormat("Page {0}"),true,null,true);
    }
  }

}
