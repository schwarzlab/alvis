package com.general.gui.actions;

import java.lang.ref.*;
import java.util.*;

import javax.swing.*;


/**
 * <p>Title: YANA</p>
 * <p>Description: Yet Another Network Analyzer</p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: University of Wuerzburg, Department for Bioinformatics</p>
 * @author Roland Schwarz
 * @version 0.9
 */

public class ActionCollector {

  protected Set<WeakReference<Action>> actions=new HashSet<WeakReference<Action>>();
  private boolean enabled=false;

  public ActionCollector() {
  }

  public ActionCollector(boolean initiallyEnabled) {
    this.enabled=initiallyEnabled;
  }

  public synchronized void addAction(Action a) {
    WeakReference<Action> ref=new WeakReference<Action>(a);
    if (!this.containsAction(a)) {
      a.setEnabled(this.isEnabled());
      actions.add(ref);
    }
  }

  public synchronized void deleteAction(Action a) {
    boolean found=false;
    Set<WeakReference> delete = new HashSet<WeakReference>();
    for (WeakReference<Action> wr : actions) {
      Action action=wr.get();
      if ((action==null) || (action==a)){
        delete.add(wr);
      }
    }
    actions.removeAll(delete);
  }


  public void setEnabled(boolean arg) {
    this.enabled=arg;
    WeakReference<Action>[] snapshot;
    synchronized(this) {
      snapshot = (WeakReference<Action>[])actions.toArray(new WeakReference[]{});
    }

    for (WeakReference<Action> wr : snapshot) {
      Action action = wr.get();
      if (action != null) {
        action.setEnabled(arg);
      }
    }
  }

  public boolean isEnabled() {
    return enabled;
  }

  public synchronized void deleteActions() {
    actions.clear();
  }

  public synchronized int countActions() {
    return actions.size();
  }

  private boolean containsAction(Action a) {
    boolean found=false;
    for (WeakReference<Action> wr : actions) {
      Action observer=wr.get();
      if (observer!=null) {
        if (observer == a) {
          found=true;
          break;
        }
      }
    }
    return found;
  }
}
