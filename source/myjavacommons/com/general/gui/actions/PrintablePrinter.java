package com.general.gui.actions;

import javax.print.attribute.*;
import javax.print.attribute.standard.*;

import java.awt.print.*;

import com.general.*;
import com.general.gui.dialog.*;

/**
 * <p>Title: YANA</p>
 *
 * <p>Description: Yet Another Network Analyzer</p>
 *
 * <p>Copyright: Copyright (c) 2004</p>
 *
 * <p>Company: Dep. of Bioinformatics - University Wuerzburg</p>
 *
 * @author Roland Schwarz
 * @version 0.4
 */
public class PrintablePrinter implements IPrinter{
  private static PrintablePrinter _INSTANCE = null;

  public static PrintablePrinter getInstance() {
    if (_INSTANCE == null) {
      _INSTANCE = new PrintablePrinter();
    }
    return _INSTANCE;
  }
  private PrintablePrinter() {
  }

  public void doPrint(PrintAction action) throws PrinterException{
    Printable printable=action.getPrintable();
    if (printable!=null) {
        // get printer job
        PrinterJob printerJob = PrinterJob.getPrinterJob();
        printerJob.setPrintable(printable);

        // display print dialog and print
        PrintRequestAttributeSet set = new HashPrintRequestAttributeSet();
        set.add(MediaSizeName.ISO_A4);
        set.add(OrientationRequested.PORTRAIT);

        boolean doPrint = printerJob.printDialog(set);
        if (doPrint) {
          printerJob.print(set);
        }
      }
    }
  }
