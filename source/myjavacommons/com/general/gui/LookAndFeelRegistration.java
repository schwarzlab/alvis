package com.general.gui;

import java.util.*;

import javax.swing.*;
import javax.swing.event.*;

import com.general.*;

/**
 * <p>Title: YANA</p>
 * <p>Description: Yet Another Network Analyzer</p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: University of Wuerzburg, Department for Bioinformatics</p>
 * @author Roland Schwarz
 * @version 0.9
 */

public class LookAndFeelRegistration implements ComboBoxModel {
  private static LookAndFeelRegistration _INSTANCE=null;
  private List<LookAndFeelInfo> lafs=new ArrayList<LookAndFeelInfo>();
  private EventListenerList listenerList=new EventListenerList();
  private Object selectedItem=null;

  private LookAndFeelRegistration() {}
  public static LookAndFeelRegistration getInstance() {
    if (_INSTANCE==null) {
      _INSTANCE=new LookAndFeelRegistration();
    }
    return _INSTANCE;
  }

  public void registerLookAndFeel(LookAndFeelInfo lafInfo) {
    if (!lafs.contains(lafInfo)) {
      lafs.add(lafInfo);
    }
  }

  public void loadInstalledLookAndFeels() {
    for (UIManager.LookAndFeelInfo uiManagerLaF : UIManager.getInstalledLookAndFeels()) {
      LookAndFeelInfo info=new LookAndFeelInfo();
      info.setClassName(uiManagerLaF.getClassName());
      info.setName(uiManagerLaF.getName());
      registerLookAndFeel(info);
    }
  }

  /**
   * setSelectedItem
   *
   * @param anItem Object
   */
  public void setSelectedItem(Object anItem) {
    this.selectedItem=anItem;
  }

  public void setSelectedLAF(String className) {
    for (LookAndFeelInfo laf : lafs) {
      if (laf.getClassName().equals(className)) {
        setSelectedItem(laf);
        break;
      }
    }
  }

  /**
   * getSelectedItem
   *
   * @return Object
   */
  public Object getSelectedItem() {
    return this.selectedItem;
  }

  public LookAndFeelInfo getSelectedLAF() {
    return (LookAndFeelInfo)getSelectedItem();
  }

  /**
   * getSize
   *
   * @return int
   */
  public int getSize() {
    return lafs.size();
  }

  /**
   * getElementAt
   *
   * @param index int
   * @return Object
   */
  public Object getElementAt(int index) {
    return lafs.get(index);
  }

  /**
   * addListDataListener
   *
   * @param l ListDataListener
   */
  public void addListDataListener(ListDataListener l) {
    listenerList.add(ListDataListener.class, l);
  }

  /**
   * removeListDataListener
   *
   * @param l ListDataListener
   */
  public void removeListDataListener(ListDataListener l) {
    listenerList.remove(ListDataListener.class,l);
  }
}
