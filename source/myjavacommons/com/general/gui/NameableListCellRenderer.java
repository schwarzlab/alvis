/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.general.gui;

import com.general.containerModel.Nameable;
import java.awt.Component;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JLabel;
import javax.swing.JList;

/**
 *
 * @author Roland Schwarz <rfs32@cam.ac.uk>
 */
public class NameableListCellRenderer extends DefaultListCellRenderer{

	@Override
	public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
		 JLabel label = (JLabel) super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus); //To change body of generated methods, choose Tools | Templates.
		 Nameable name = (Nameable)value;
		 if (name!=null){
			 label.setText(name.getName());
			 label.setToolTipText(name.getDescription());
		 }
		 return label;
	}
	
}
