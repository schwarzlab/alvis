package com.general.gui.panels;

import com.general.mvc.view.IDisplayable;
import java.awt.*;
import javax.swing.*;

import com.general.gui.actions.DefaultActionFactory;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author unbekannt
 * @version 1.0
 */

public class MultiLineTextPanel
    extends JPanel
    implements IDisplayable {
  private JTextArea textArea = new JTextArea();
  private JScrollPane spane = new JScrollPane(textArea);
  private FlowLayout flowLayout = new FlowLayout(FlowLayout.LEFT);
  private JPanel buttonPanel = new JPanel(flowLayout);
  private JButton exportButton=new JButton();
  private String title = "";

  public MultiLineTextPanel() {
    this("", "");
  }

  public MultiLineTextPanel(String text) {
    this(text, "");
  }

  public MultiLineTextPanel(String text, String title) {
    setText(text);
    this.title = title;
    init();
  }

  private void init() {
    this.setLayout(new BorderLayout());
    this.add(spane, BorderLayout.CENTER);
    this.add(buttonPanel, BorderLayout.NORTH);
    buttonPanel.add(exportButton);
    this.textArea.setAutoscrolls(true);
    this.textArea.setEditable(false);
    this.textArea.setLineWrap(false);
    this.textArea.setMargin(new Insets(5, 5, 5, 5));
    this.setPreferredSize(new Dimension(600, 500));
  }

  public void setText(String text) {
    this.textArea.setText(text);
    this.exportButton.setAction(DefaultActionFactory.createSaveTextToFileAction(text,this));
  }

  public String getText() {
    return this.textArea.getText();
  }

  /**
   * getGraphicalRepresentation
   *
   * @return Component
   * @todo Implement this com.general.mvc.Displayable method
   */
  public Component getComponent() {
    return this;
  }

  /**
   * getTitle
   *
   * @return String
   * @todo Implement this com.general.mvc.Displayable method
   */
  public String getTitle() {
    return title;
  }

  /**
   * closing
   *
   * @todo Implement this com.general.mvc.Displayable method
   */
  public void closing() {
  }
}
