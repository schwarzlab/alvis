package com.general.gui.panels;


import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

import com.general.gui.*;

/**
 * <p>Title: YANA</p>
 * <p>Description: Yet Another Network Analyzer</p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: University of Wuerzburg, Department for Bioinformatics</p>
 * @author Roland Schwarz
 * @version 0.9
 */

public class LookAndFeelSelectionPanel extends JPanel {

  private LookAndFeelRegistration subject=null;
  TitledBorder titledBorder1;
  JComboBox cmbLAF = new JComboBox();
  GridBagLayout gridBagLayout1 = new GridBagLayout();
  JScrollPane spanDescription = new JScrollPane();
  JTextArea txtDescription = new JTextArea();

  public LookAndFeelSelectionPanel(LookAndFeelRegistration subject) {
    this.subject=subject;
    cmbLAF=new JComboBox(subject);
    try {
      jbInit();
    }
    catch(Exception e) {
      e.printStackTrace();
    }

  }

  private void jbInit() throws Exception {
    //titledBorder1 = new TitledBorder(BorderFactory.createLineBorder(new Color(153, 153, 153),2),"Dateiformat:");
    //this.setBorder(titledBorder1);
    this.setLayout(gridBagLayout1);
    cmbLAF.setPreferredSize(new Dimension(150, 23));
    cmbLAF.addItemListener(new java.awt.event.ItemListener() {
      public void itemStateChanged(ItemEvent e) {
        cmbStrategy_itemStateChanged(e);
      }
    });
    txtDescription.setFont(new java.awt.Font("SansSerif", 0, 12));
    txtDescription.setEditable(false);
    txtDescription.setText("");
    txtDescription.setLineWrap(true);
    this.add(cmbLAF,  new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0
            ,GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0));
    //this.add(spanDescription,   new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0
    //    ,GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 0, 0));
    spanDescription.getViewport().add(txtDescription, null);
  }

  private void cmbStrategy_itemStateChanged(ItemEvent e) {
    txtDescription.setText(subject.getSelectedLAF().getDescription());
  }

}
