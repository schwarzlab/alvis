package com.general.gui.panels.table;

import com.general.mvc.view.IDisplayable;
import java.util.regex.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

import com.general.*;
import com.general.utils.GUIutils;

/**
 * <p>Title: YANA</p>
 *
 * <p>Description: Yet Another Network Analyzer</p>
 *
 * <p>Copyright: Copyright (c) 2004 Roland Schwarz\nThis software is licensed
 * under the GPL.\n See the licensing file that came with this distribution or
 * http://www.gnu.org/copyleft/gpl.html</p>
 *
 * <p>Company: Dep. of Bioinformatics - University Wuerzburg</p>
 *
 * @author Roland Schwarz
 * @version 0.4
 */
public class TableSearchPanel
    extends JPanel implements IDisplayable, DocumentListener, TableModelListener{
  GridBagLayout gridBagLayout1 = new GridBagLayout();
  JLabel lblFind = new JLabel();
  JTextField txtSearch = new JTextField();
  JCheckBox chkMatchWord = new JCheckBox();
  JCheckBox chkMatchCase = new JCheckBox();
  JLabel lblCount = new JLabel();
  JTextField txtCount = new JTextField();
  private Pattern pattern=Pattern.compile("");
  private JTable table=null;
  JButton cmdSearch = new JButton();

  public TableSearchPanel() {
    try {
      jbInit();
      init();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
    recompilePattern();
  }

  private void init() {
    GUIutils.setDefaultButton(this,cmdSearch);
  }

  private void jbInit() throws Exception {
    this.setLayout(gridBagLayout1);
    this.txtSearch.getDocument().addDocumentListener(this);
    this.chkMatchCase.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        recompilePattern();
        doSearch();
      }
    });

    this.chkMatchWord.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        doSearch();
      }
    });
    this.setPreferredSize(new Dimension(400,100));
    lblFind.setText("Find what");
    txtSearch.setText("");
    chkMatchWord.setText("Match whole word only");
    chkMatchCase.setText("Match case");
    lblCount.setText("Occurences:");
    txtCount.setEditable(false);
    txtCount.setText("");
    txtCount.setMinimumSize(new Dimension(80,txtCount.getMinimumSize().height));
    cmdSearch.setText("Search");

    cmdSearch.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        cmdSearch_actionPerformed(e);
      }
    });

    this.add(chkMatchWord, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0
                                                  , GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 1, 5), 0, 0));
    this.add(chkMatchCase, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0
                                                  , GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(1, 5, 5, 5), 0, 0));
    this.add(lblFind, new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0
                                             , GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 0), 0, 0));
    this.add(txtSearch, new GridBagConstraints(0, 1, 4, 1, 1.0, 0.0
                                               , GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0));
    this.add(txtCount, new GridBagConstraints(3, 2, 1, 1, 0.0, 0.0
                                              , GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0));
    this.add(lblCount, new GridBagConstraints(1, 2, 2, 1, 1.0, 0.0
                                              , GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 5, 1, 5), 0, 0));
    this.add(cmdSearch, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0
                                               , GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
  }

  public Component getComponent() {
    return this;
  }

  public String getTitle() {
    return "Search";
  }

  public void closing() {
  }

  private void doSearch() {
    if ((!GUIutils.isVisible(this)) || (this.table==null))
      return;

    Log.out("DO SEARCH");
    int rows=this.table.getRowCount();
    int cols=this.table.getColumnCount();
    int count=0;

    this.table.clearSelection();

    if (pattern.pattern().trim().length()>0) {
      for (int row = 0; row < rows; row++) {
        for (int col = 0; col < cols; col++) {
          String probe = table.getValueAt(row, col).toString();
          Matcher matcher = pattern.matcher(probe);
          boolean found = false;
          if (chkMatchWord.isSelected()) {
            found = matcher.matches();
            count++;
          }
          else {
            while (matcher.find()) {
              found = true;
              count++;
            }
          }
          if (found) {
            table.addRowSelectionInterval(row, row);
          }
        }
      }
      txtCount.setText(String.valueOf(count)+" ("+table.getSelectedRowCount()+")");
      txtCount.setToolTipText("Expression '"+pattern.pattern()+"' was found "+count+" times in "+table.getSelectedRowCount()+" rows.");
    }
    else {
      txtCount.setText("");
      txtCount.setToolTipText("");
    }
  }

  public void setTable(JTable table) {
    if (this.table!=null) {
      table.getModel().removeTableModelListener(this);
    }
    this.table=table;
    this.table.getModel().addTableModelListener(this);
  }

  private void recompilePattern() {
    int flags=0;
    if (!chkMatchCase.isSelected()) {
      flags|=Pattern.CASE_INSENSITIVE;
    }
    try {
      this.pattern = Pattern.compile(txtSearch.getText(), flags);
    } catch (Exception ex) {
      this.pattern=Pattern.compile("");
    }
  }

  // DocumentListenerInterface
  public void insertUpdate(DocumentEvent e) {
    recompilePattern();
//    doSearch();
  }

  public void removeUpdate(DocumentEvent e) {
    recompilePattern();
//    doSearch();
  }

  public void changedUpdate(DocumentEvent e) {
  }

  // TableModelListener
  public void tableChanged(TableModelEvent e) {
//    doSearch();
  }

  public void cmdSearch_actionPerformed(ActionEvent e) {
    doSearch();
  }

}
