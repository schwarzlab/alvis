package com.general.gui.panels.table;

import java.util.*;
import javax.swing.JTable;

/**
 * <p>Title: YANA - Yet Another Network Analyzer</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Dep. of Bioinformatics, University Wuerzburg</p>
 *
 * @author not attributable
 * @version 0.8
 */
public class TablePanelEvent<E> extends EventObject {
  private TablePanel<E> source;
  public TablePanelEvent(TablePanel<E> source) {
    super(source);
    this.source=source;
  }

  public TablePanel<E> getSource() {
    return source;
  }

  public JTable getTable() {
    return source.getTable();
  }

  public List<E> getSelectedItems() {
    return source.getSelectedItems();
  }

  public int[] getSelection() {
    return source.getTable().getSelectedRows();
  }

  public List<E> getData() {
    return source.getData();
  }
}
