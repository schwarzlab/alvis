package com.general.gui.panels.table;

import javax.swing.table.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */

public class CSVTableExporter implements ITableExporter{
  private static CSVTableExporter _INSTANCE=null;

  private CSVTableExporter() {
  }

  public static CSVTableExporter getInstance() {
    if (_INSTANCE==null) {
      _INSTANCE=new CSVTableExporter();
    }
    return _INSTANCE;
  }

  /**
   * export
   *
   * @param model TableModel
   * @return String
   */
  public String export(TableModel model) {
    StringBuffer buffer=new StringBuffer();
    for (int col=0;col<model.getColumnCount();col++) {
      buffer.append("\"");
      buffer.append(model.getColumnName(col));
      buffer.append("\"");
      if (col<model.getColumnCount()-1) {
        buffer.append(",");
      }
    }
    buffer.append("\n");

    for (int row=0;row<model.getRowCount();row++) {
      for (int col=0;col<model.getColumnCount();col++) {
        buffer.append("\"");
        buffer.append(model.getValueAt(row,col));
        buffer.append("\"");
        if (col<model.getColumnCount()-1) {
          buffer.append(",");
        }
      }
      if (row<model.getRowCount()-1) {
        buffer.append("\n");
      }
    }

    return buffer.toString();
  }

  /**
   * getName
   *
   * @return String
   */
  public String toString() {
    return "Export to CSV file";
  }

}
