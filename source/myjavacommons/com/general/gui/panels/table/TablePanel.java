package com.general.gui.panels.table;

import java.awt.*;
import java.text.MessageFormat;
import java.util.List;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;

import com.general.Log;
import com.general.gui.TableSorter;
import com.general.gui.actions.DefaultActionFactory;
import com.general.gui.panels.table.renderer.MultiLineToolTip;
import com.general.gui.panels.table.renderer.TextTableCellRenderer;
import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collections;
import com.general.gui.actions.PrintAction;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */

public class TablePanel<E> extends JPanel implements ListSelectionListener, MouseListener{
  public static enum SortingType {NOT_SORTED,ASCENDING,DESCENDING};

  private TableModel tableModel=null;
  private List<E> data=null;
  private String title="";
  private BorderLayout borderLayout1 = new BorderLayout();
  private JScrollPane scrollPane = new JScrollPane();
  private JTable table = null;
  private JToolBar toolbarNorth=new JToolBar(JToolBar.HORIZONTAL);
  private JToolBar toolbarWest=new JToolBar(JToolBar.VERTICAL);
  private JButton cmdExport=new JButton();
  private boolean readonly=false;
  private TableSorter sorter=new TableSorter() {
    public boolean isCellEditable(int rowIndex,int colIndex) {
      if (isReadonly()) {
        return false;
      }
      return super.isCellEditable(rowIndex,colIndex);
    }
  };
  private JLabel lblCount=new JLabel();
  private EventListenerList listenerList=new EventListenerList();
  private JButton defaultButton=null;
  private PrintAction printAction=null;

// ***************************************************
  public TablePanel() {
    this(null,null,null);
  }

  public TablePanel(String title) {
    this(null,null,title);
  }

  public TablePanel(TableModel tableModel) {
    this(tableModel,null,null);
  }

  public TablePanel(List<E> data) {
    this(null,data,null);
  }

  public TablePanel(TableModel tableModel, List<E> data) {
    this(tableModel,data,null);
  }

  public TablePanel(TableModel tableModel, List<E> data, String title) {
    this.data=data;
    this.table=new JTable() {
      public JToolTip createToolTip() {
        MultiLineToolTip tt = new MultiLineToolTip();
        tt.setMaxWidth(400);
        return tt;
      }
    };
    this.printAction=DefaultActionFactory.createPrintAction(this);

    table.setModel(sorter);
    sorter.setTableHeader(table.getTableHeader());
    setSortingStatus(0,SortingType.ASCENDING);
    setTableModel(tableModel);
    setTitle(title);
    sorter.addTableModelListener(new TableModelListener() {
      public void tableChanged(TableModelEvent e) {
        sorterChanged(e);
      }
    });
    table.getSelectionModel().addListSelectionListener(this);
    table.addMouseListener(this);
    table.setDefaultRenderer(String.class,new TextTableCellRenderer());
    try {
      jbInit();
      init();
    }
    catch(Exception e) {
      Log.out(e);
    }
  }
  // *******************************************************************************************************************


  private void jbInit() throws Exception{
    this.setLayout(borderLayout1);
    this.setPreferredSize(new Dimension(180, 403));
    this.add(scrollPane, BorderLayout.CENTER);
    this.add(toolbarNorth, BorderLayout.NORTH);
    this.add(toolbarWest,BorderLayout.WEST);
    scrollPane.getViewport().add(table, null);
  }

  private void init() {
    scrollPane.setViewportBorder(BorderFactory.createRaisedBevelBorder());
    scrollPane.getViewport().setBackground(Color.WHITE);
    scrollPane.setCorner(JScrollPane.UPPER_RIGHT_CORNER,lblCount);
    table.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    table.setColumnSelectionAllowed(false);
    MessageFormat header=new MessageFormat((getTitle()==null?"":getTitle()));
    MessageFormat footer=new MessageFormat("Page {0}");
    toolbarNorth.add(this.printAction);
    toolbarNorth.add(cmdExport);
    toolbarNorth.add(DefaultActionFactory.createSearchInTableAction(this.table));
    table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
    scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
    resizeColumns();
  }

  public void resizeColumns() {
    AutofitTableColumns.autoResizeTable(table,true);
  }

  public JTable getTable() {
    return table;
  }

  public void resort() {
//    sorter.setSortingStatus(sorter.);
  }

  public void setSortingStatus(int column, SortingType status) {
    if (sorter==null) {
      return;
    }

    switch (status) {
      case NOT_SORTED:
        sorter.setSortingStatus(column,TableSorter.NOT_SORTED);
        break;
      case ASCENDING:
        sorter.setSortingStatus(column,TableSorter.ASCENDING);
        break;
      case DESCENDING:
        sorter.setSortingStatus(column,TableSorter.DESCENDING);
        break;
    }
  }

  public void setTableModel(TableModel tableModel) {
      if (tableModel!=null && tableModel==this.tableModel)
          return;

    this.tableModel = tableModel;
    if (tableModel!=null) {
      sorter.setTableModel(tableModel);
    }
    else {
      sorter.setTableModel(new DefaultTableModel());
    }
    cmdExport.setAction(DefaultActionFactory.createCSVTableExportAction(this.tableModel,this));
    cmdExport.setText("");
    AutofitTableColumns.autoResizeTable(table,true);
  }

  public void setPreferredColumnWidth(int colIndex, int width) {
    table.getColumnModel().getColumn(colIndex).setPreferredWidth(width);
  }

  public void activateResizeLastColumnMode() {
    table.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
    for (int i=0;i<table.getColumnCount()-1;i++) {
      TableColumn col=table.getColumnModel().getColumn(i);
      col.setMaxWidth(1000);
      setPreferredColumnWidth(i,100);
    }
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
    if ((title!=null) && (title.length()>0)) {
      this.setBorder(BorderFactory.createTitledBorder(title));
      this.printAction.setTitle(title);
    }
    else {
      this.setBorder(null);
    }
  }

  public boolean isReadonly() {
    return readonly;
  }
  public void setReadonly(boolean readonly) {
    this.readonly = readonly;
  }

  public JButton addNorth(Action action) {
    return addNorth(action,false);
  }

  public JButton addNorth(Action action, boolean isDefaultAction) {
    JButton button = toolbarNorth.add(action);
    if (isDefaultAction)
      this.defaultButton=button;
    return button;
  }

  public JButton addNorth(TableAction<E> action) {
    return addNorth(action,false);
  }

  public JButton addNorth(TableAction<E> action, boolean isDefaultAction) {
    JButton button=addNorth((Action)action);
    TablePanelListener ta=(TablePanelListener)action;
    listenerList.add(TablePanelListener.class,ta);
    fireInitialized(ta);
    if (isDefaultAction)
      this.defaultButton=button;
    return button;
  }

  public JButton addWest(Action action) {
    return addWest(action,false);
  }

  public JButton addWest(Action action, boolean isDefaultAction) {
    JButton button = toolbarWest.add(action);
    if (isDefaultAction)
      this.defaultButton=button;
    return button;
  }

  public JButton addWest(TableAction<E> action) {
    return addWest(action,false);
  }

  public JButton addWest(TableAction<E> action, boolean isDefaultAction) {
    JButton button=addWest((Action)action);
    TablePanelListener ta=(TablePanelListener)action;
    listenerList.add(TablePanelListener.class,ta);
    fireInitialized(ta);
    if (isDefaultAction)
      this.defaultButton=button;
    return button;
  }

  public TableModel getTableModel() {
    return this.tableModel;
  }

  public List<E> getData() {
    return this.data;
  }

  public List<E> getSelectedItems() {
    int[] selection=table.getSelectedRows();
    List<E> list=new ArrayList<E>(selection.length);
    for (int index:selection) {
      int realIndex=sorter.modelIndex(index);
      list.add(data.get(realIndex));
    }
    return Collections.unmodifiableList(list);
  }

  // Event generator methods
  private void fireSelectionChanged() {
    TablePanelEvent<E> e=new TablePanelEvent<E>(this);
    TablePanelListener[] listeners=listenerList.getListeners(TablePanelListener.class);
    for (TablePanelListener l:listeners)
      l.selectionChanged(e);
  }

  private void fireTableChanged() {
    TablePanelEvent<E> e=new TablePanelEvent<E>(this);
    TablePanelListener[] listeners=listenerList.getListeners(TablePanelListener.class);
    for (TablePanelListener l:listeners)
      l.tableChanged(e);
  }

  private void fireInitialized(TablePanelListener<E> l) {
    TablePanelEvent<E> e=new TablePanelEvent<E>(this);
    l.initialized(e);
  }

  // ListSelectionListener
  public void valueChanged(ListSelectionEvent e) {
    fireSelectionChanged();
  }

  // TableModelListener
  public void sorterChanged(TableModelEvent e) {
    // sorter changes everytime the "real" model changes + on sorting events
    if (e.getSource()!=sorter) {
      String text=String.valueOf(sorter.getRowCount());
      lblCount.setText(text);
      lblCount.setToolTipText(text);
      AutofitTableColumns.autoResizeTable(table,true);
    }
    fireTableChanged();
  }

  // MouseListener
  public void mouseClicked(MouseEvent e) {
    if ((defaultButton!=null) && (e.getClickCount()==2)) { // DoubleClickck
      defaultButton.doClick();
    }
  }


  public void mousePressed(MouseEvent e) {
  }

  public void mouseReleased(MouseEvent e) {
  }

  public void mouseEntered(MouseEvent e) {
  }

  public void mouseExited(MouseEvent e) {
  }
}
