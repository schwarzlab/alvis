package com.general.gui.panels.table;

import javax.swing.event.PopupMenuListener;
import javax.swing.event.PopupMenuEvent;

/**
 * <p>Title: YANA</p>
 *
 * <p>Description: Yet Another Network Analyzer</p>
 *
 * <p>Copyright: Copyright (c) 2004</p>
 *
 * <p>Company: Dep. of Bioinformatics - University Wuerzburg</p>
 *
 * @author Roland Schwarz
 * @version 0.4
 */
public class PopupMenuAdapter implements PopupMenuListener{
  public PopupMenuAdapter() {
  }

  public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
  }

  public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
  }

  public void popupMenuCanceled(PopupMenuEvent e) {
  }
}
