package com.general.gui.panels.table.renderer;

import java.util.*;

import java.awt.*;
import javax.swing.*;
import javax.swing.plaf.*;
import javax.swing.plaf.basic.*;
import com.general.Log;

/**
 * @author Zafir Anjum
 */


public class MultiLineToolTip
    extends JToolTip {
  private static final String uiClassID = "ToolTipUI";

  String tipText;
  JComponent component;

  public MultiLineToolTip() {
    updateUI();
  }

  public void updateUI() {
    setUI(MultiLineToolTipUI.createUI(this));
  }

  public void setMaxWidth(int pixels) {
    this.maxWidth = pixels;
  }

  public void setWordWrap(boolean flag) {
    this.wordWrap=flag;
  }

  public int getMaxWidth() {
    return maxWidth;
  }

  public boolean isWordWrap() {
    return wordWrap;
  }

  protected int maxWidth = 0;
  protected boolean wordWrap = true;
}

class MultiLineToolTipUI
    extends BasicToolTipUI {
  static MultiLineToolTipUI sharedInstance = new MultiLineToolTipUI();
  Font smallFont;
  static JToolTip tip;
  protected CellRendererPane rendererPane;

  private JTextArea textArea;

  public static ComponentUI createUI(JComponent c) {
    return sharedInstance;
  }

  public MultiLineToolTipUI() {
    super();
  }

  public void installUI(JComponent c) {
    super.installUI(c);
    tip = (JToolTip) c;
    rendererPane = new CellRendererPane();
    c.add(rendererPane);
  }

  public void uninstallUI(JComponent c) {
    super.uninstallUI(c);

    c.remove(rendererPane);
    rendererPane = null;
  }

  public void paint(Graphics g, JComponent c) {
    Dimension size = c.getSize();
    textArea.setBackground(c.getBackground());
    rendererPane.paintComponent(g, textArea, c, 1, 1,
                                size.width - 1, size.height - 1, true);
  }

  public Dimension getPreferredSize(JComponent c) {
    String tipText = ( (JToolTip) c).getTipText();
    boolean wordWrap=((MultiLineToolTip)c).isWordWrap();
    int maxWidth=((MultiLineToolTip)c).getMaxWidth();
    if (tipText == null) {
      return new Dimension(0, 0);
    }
    textArea = new JTextArea();

    Insets margin=new Insets(4,4,4,4);
	textArea.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.BLACK), BorderFactory.createEmptyBorder(margin.top, margin.left, margin.bottom, margin.right)));

    rendererPane.removeAll();
    rendererPane.add(textArea);
    textArea.setWrapStyleWord(wordWrap);

    FontMetrics fm=c.getFontMetrics(c.getFont());

    textArea.setLineWrap(false);
    String formattedText=formatString(tipText,maxWidth,fm,textArea.getWrapStyleWord());
    textArea.setText(formattedText);
    Dimension d=computeStringSize(fm,formattedText);
    d.width+=margin.left+margin.right;
    d.height+=margin.top+margin.bottom;

    return d;
  }

  private String formatString(String text, int allowedWidth,FontMetrics fm,boolean wordWrap) {
    String unformattedText=text.trim();
    StringBuffer formattedText=new StringBuffer(text.length());
    int lastSpaceIndex=0,lastNewlineIndex=0, offset=0;

    for (int pos=0;pos<unformattedText.length();pos++) {
      char c = unformattedText.charAt(pos);
      if (Character.isSpaceChar(c))
        lastSpaceIndex=pos;
      if (c=='\n')
        lastNewlineIndex=pos;

      int currentWidth=SwingUtilities.computeStringWidth(fm,unformattedText.substring(lastNewlineIndex,pos+1));

      // Changed RFS 14.02.2007, if the maxwidth is not set, expand as far as possible
      if (allowedWidth<=0)
        allowedWidth=SwingUtilities.computeStringWidth(fm,unformattedText);

      if (currentWidth>allowedWidth) {
        if (lastSpaceIndex==0 || !wordWrap) {
          formattedText.append('\n');
          lastNewlineIndex=pos;
          formattedText.append(c);
          offset++;
        } else {
          formattedText.append(c);
          formattedText.setCharAt(lastSpaceIndex+offset,'\n');
          lastNewlineIndex=lastSpaceIndex;
          lastSpaceIndex=0;
        }
      } else {
        formattedText.append(c);
      }

    }

    return formattedText.toString();
  }

  /**
   * computeStringWidth
   *
   * @param fm FontMetrics
   * @param tipText String
   * @return int
   */
  private Dimension computeStringSize(FontMetrics fm, String text) {
    int maxLength=0;
    int rowCount=0;
    StringTokenizer tokenizer=new StringTokenizer(text,"\n");
    while (tokenizer.hasMoreTokens()) {
      rowCount++;
      String token=tokenizer.nextToken();
      int length=SwingUtilities.computeStringWidth(fm,token);
      if (length>maxLength)
        maxLength=length;
    }

    return new Dimension(maxLength+2,rowCount*fm.getHeight());
  }

  public Dimension getMinimumSize(JComponent c) {
    return getPreferredSize(c);
  }

  public Dimension getMaximumSize(JComponent c) {
    return getPreferredSize(c);
  }
}
