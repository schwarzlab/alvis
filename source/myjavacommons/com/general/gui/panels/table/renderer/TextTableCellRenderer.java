package com.general.gui.panels.table.renderer;

import java.awt.*;
import javax.swing.*;
import javax.swing.table.*;

/**
 * <p>Title: YANA</p>
 *
 * <p>Description: Yet Another Network Analyzer</p>
 *
 * <p>Copyright: Copyright (c) 2004</p>
 *
 * <p>Company: Dep. of Bioinformatics - University Wuerzburg</p>
 *
 * @author Roland Schwarz
 * @version 0.4
 */
public class TextTableCellRenderer extends DefaultTableCellRenderer{


  public TextTableCellRenderer() {
  }

  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
                                                 int row, int column) {

    setToolTipText(value.toString());
    return super.getTableCellRendererComponent(table,value,isSelected,hasFocus,row,column);
  }

}
