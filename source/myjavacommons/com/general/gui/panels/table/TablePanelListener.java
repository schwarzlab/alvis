package com.general.gui.panels.table;

import java.util.EventListener;

/**
 * <p>Title: YANA - Yet Another Network Analyzer</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Dep. of Bioinformatics, University Wuerzburg</p>
 *
 * @author not attributable
 * @version 0.8
 */
public interface TablePanelListener<E> extends EventListener {
  public void initialized(TablePanelEvent<E> e);
  public void selectionChanged(TablePanelEvent<E> e);
  public void tableChanged(TablePanelEvent<E> e);
}
