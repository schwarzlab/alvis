package com.general.gui.panels.table;

import javax.swing.table.TableModel;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */

public interface ITableExporter {
  public String export(TableModel model);
}
