package com.general.gui.panels.table;

import javax.swing.AbstractAction;
import javax.swing.Icon;



import java.util.List;

import javax.swing.JTable;

public abstract class AbstractTableAction<E> extends AbstractAction implements
    TableAction<E> {
  private boolean locallyEnabled = true;
  private List<E> data = null;
  private TablePanel<E> panel = null;
  private JTable table = null;
  private int[] selection = null;

  public AbstractTableAction(String name, Icon icon) {
    super(name, icon);
  }

  public AbstractTableAction(String name) {
    this(name, null);
  }

  public AbstractTableAction() {
    this("", null);
  }

  /**
   * Returns the enabled state of the <code>Action</code>.
   *
   * @return true if this <code>Action</code> is enabled
   * @todo Implement this javax.swing.Action method
   */
  public boolean isEnabled() {
    return (super.isEnabled() && this.locallyEnabled);
  }

  protected void setLocallyEnabled(boolean b) {
    boolean oldValue = isEnabled();
    this.locallyEnabled = b;
    firePropertyChange("enabled", Boolean.valueOf(oldValue), Boolean.valueOf(isEnabled()));
  }

  public List<E> getData() {
    return data;
  }

  public TablePanel<E> getTablePanel() {
    return panel;
  }

  public JTable getTable() {
    return table;
  }

  public int[] getSelection() {
    return getTable().getSelectedRows();
  }

  public List<E> getSelectedItems() {
    return panel.getSelectedItems();
  }

  // Event handlers
  public void selectionChanged(TablePanelEvent<E> e) {}

  public void tableChanged(TablePanelEvent<E> e) {}

  public void initialized(TablePanelEvent<E> e) {
    this.data = e.getData();
    this.panel = e.getSource();
    this.table = e.getTable();
  }
}
