package com.general.gui.panels.table;

import javax.swing.Action;

public interface TableAction<E> extends Action, TablePanelListener<E> {
}
