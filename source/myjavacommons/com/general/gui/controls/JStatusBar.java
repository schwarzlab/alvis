package com.general.gui.controls;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Insets;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JToolTip;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import prefuse.util.ui.JCustomTooltip;

public class JStatusBar
		extends JPanel {

	/**
	 * @return the tooltipBackground
	 */
	public Color getTooltipBackground() {
		return tooltipBackground;
	}

	/**
	 * @param tooltipBackground the tooltipBackground to set
	 */
	public void setTooltipBackground(Color tooltipBackground) {
		this.tooltipBackground = tooltipBackground;
	}

	/**
	 * @return the tooltipBorder
	 */
	public Border getTooltipBorder() {
		return tooltipBorder;
	}

	/**
	 * @param tooltipBorder the tooltipBorder to set
	 */
	public void setTooltipBorder(Border tooltipBorder) {
		this.tooltipBorder = tooltipBorder;
	}

	public final static int BORDER_STYLE_EMPTY = 0;
	public final static int BORDER_STYLE_ETCHED = 1;
	public final static int BORDER_STYLE_RAISED = 2;
	public final static int BORDER_STYLE_LOWERED = 3;
	public final static int BORDER_STYLE_LINE = 4;
	public final static int AUTO_WIDTH = -1;
	public final static int ALL_PANES = -1;

	private final static int DEFAULT_HEIGHT = 20;
	private final static Insets DEFAULT_INSETS = new Insets(4, 4, 4, 4);

	private int paneCount = 1;
//    private JTextField[] labels = new MultilineToolTipTextField [1];
	private StatusLabel[] labels = new StatusLabel[1];
	private boolean[] autoLabels = {true};
	private int heightOfBar = DEFAULT_HEIGHT;
	Color tooltipBackground = null;
	Border tooltipBorder = null;

	public JStatusBar() {
	}

	public JStatusBar(int paneCount) {
		initialize(paneCount);
	}

	public int getPaneCount() {
		return paneCount;
	}

	public void initialize(int paneCount) {
		this.paneCount = paneCount;
		labels = new StatusLabel[paneCount];
		autoLabels = new boolean[paneCount];
		removeAll();
		for (int i = 0; i < paneCount; i++) {
			labels[i] = new StatusLabel();
			labels[i].setOpaque(true);
			labels[i].setBackground(Color.WHITE);
			labels[i].setHorizontalTextPosition(SwingConstants.LEADING);
			autoLabels[i] = true;
			add(labels[i]);
		}
//		FlowLayout layout = new FlowLayout(paneAlignment, 4, 2);
		BoxLayout layout = new BoxLayout(this, BoxLayout.X_AXIS);
		setLayout(layout);
	}

	public void setPaneInfo(int index, int borderStyle, int width) {
		if (index == ALL_PANES) {
			for (int i = 0; i < this.paneCount; i++) {
				setPaneInfo(i, borderStyle, width);
			}
			return;
		}

		if (width != AUTO_WIDTH) {
			labels[index].setPreferredSize(new Dimension(width, heightOfBar));
			autoLabels[index] = false;
		} else {
			labels[index].setPreferredSize(null);
			if (labels[index].getText().length() == 0) {
				labels[index].setVisible(false);
			}
			autoLabels[index] = true;
		}

		if (borderStyle == BORDER_STYLE_ETCHED) {
			EtchedBorder border = new EtchedBorder(EtchedBorder.LOWERED) {
				@Override
				public Insets getBorderInsets(Component c) {
					return DEFAULT_INSETS;
				}
				@Override
				public Insets getBorderInsets(Component c, Insets insets) {
					return DEFAULT_INSETS;
				}
			};
			this.labels[index].setBorder(border);
		} else if (borderStyle == BORDER_STYLE_RAISED) {
			BevelBorder border = new BevelBorder(BevelBorder.RAISED) {
				@Override
				public Insets getBorderInsets(Component c) {
					return DEFAULT_INSETS;
				}
				@Override
				public Insets getBorderInsets(Component c, Insets insets) {
					return DEFAULT_INSETS;
				}
			};
			this.labels[index].setBorder(border);
		} else if (borderStyle == BORDER_STYLE_LOWERED) {
			BevelBorder border = new BevelBorder(BevelBorder.LOWERED) {
				@Override
				public Insets getBorderInsets(Component c) {
					return DEFAULT_INSETS;
				}
				@Override
				public Insets getBorderInsets(Component c, Insets insets) {
					return DEFAULT_INSETS;
				}
			};
			this.labels[index].setBorder(border);
		} else if (borderStyle == BORDER_STYLE_LINE) {
			LineBorder border = new LineBorder(Color.BLACK) {
				@Override
				public Insets getBorderInsets(Component c) {
					return DEFAULT_INSETS;
				}
				@Override
				public Insets getBorderInsets(Component c, Insets insets) {
					return DEFAULT_INSETS;
				}
			};
			this.labels[index].setBorder(border);
		} else {
			this.labels[index].setBorder(BorderFactory.createEmptyBorder());
		}

	}

	public void clear() {
		for (int i = 0; i < paneCount; i++) {
			clearPaneText(i);
		}
	}

	public void setPaneText(int index, String text) {
		labels[index].setText(text);
		if (autoLabels[index] == true) {
			if (text.length() == 0) {
				labels[index].setVisible(false);
			} else {
				labels[index].setVisible(true);
			}
		}
		doLayout();
		invalidate();
		repaint();
		getParent().doLayout();
		getParent().invalidate();
		getParent().repaint();
//		SwingUtilities.windowForComponent(this).pack();
	}

	public void setPaneToolTipText(int index, String text) {
		labels[index].setToolTipText(text);
	}

	public void clearPaneText(int index) {
		setPaneText(index, "");
	}

	public String getPaneText(int index) {
		return this.labels[index].getText();
	}

	public void setPaneIcon(int index, Icon icon) {
		this.labels[index].setIcon(icon);
	}
	
	public void addGlue(int index){
		this.add(Box.createHorizontalGlue(), index);
	}

	private class StatusLabel extends JLabel {

		public StatusLabel(String text, Icon icon, int horizontalAlignment) {
			super(text, icon, horizontalAlignment);
		}

		public StatusLabel(String text, int horizontalAlignment) {
			super(text, horizontalAlignment);
		}

		public StatusLabel(String text) {
			super(text);
		}

		public StatusLabel(Icon image, int horizontalAlignment) {
			super(image, horizontalAlignment);
		}

		public StatusLabel(Icon image) {
			super(image);
		}

		public StatusLabel() {
		}

		@Override
		public JToolTip createToolTip() {
			JPanel panel = new JPanel(new BorderLayout());
			panel.setOpaque(true);
			panel.setBackground(new Color(0,0,0,0));

			JTextArea label = new JTextArea(getToolTipText(null));
			label.setEditable(false);
			if (tooltipBackground != null) {
				label.setBackground(tooltipBackground);
			}
			if (tooltipBorder != null) {
				label.setBorder(tooltipBorder);
			} 
			panel.add(label, BorderLayout.CENTER);
			JCustomTooltip tt = new JCustomTooltip(this, panel, false);
			tt.setOpaque(false);
			return tt;
		}
	}
}
