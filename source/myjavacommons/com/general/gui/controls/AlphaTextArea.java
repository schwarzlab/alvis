/*
Wotonomy: OpenStep design patterns for pure Java applications.
Copyright (C) 2000 Blacksmith, Inc.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, see http://www.gnu.org
*/

package com.general.gui.controls;

import java.awt.event.KeyEvent;



/**
* AlphaTextField is a "smart" text field that restricts the user's input.  The
* input can be restricted to alphabetic, alphanumeric, or all characters.  The
* maximum number of characters can also be limited.
* The defaults for this component is alphabetic only string of unlimited length.
*
* @author rob@straylight.princeton.com
* @author $Author: mpowers $
* @version $Revision: 1.1.1.1 $
*/
public class AlphaTextArea extends SmartTextArea
{

/*******************************
* CONSTANTS
*******************************/

/**
* Sets the input to alphabetic characters only.  The characters "a-z" and "A-Z"
* are the only valid characters.  All other characters will be ignored.
* @see #getAlphaType()
*/
   public static final int ALPHABETIC = 0;

/**
* Sets the input to alphanumeric characters only.  The characters "a-z", "A-Z"
* and "0-9" are the only valid characters.  All other characters will be ignored.
* @see #getAlphaType()
*/
   public static final int ALPHANUMERIC = 1;

/**
* Sets the input to alphanumeric characters and a few special characters only.
* The valid characters are "a-z", "A-Z", "0-9", space, "-", "_", "\", and ":".
* This is helpful for file names (with paths) as input strings.
* All other characters will be ignored.
* @see #getAlphaType()
*/
   public static final int ALPHANUMERIC_PLUS = 2;

/**
* Sets the input to all characters without restriction.
* @see #getAlphaType()
*/
   public static final int ALL = 3;


/*******************************
* DATA MEMBERS
*******************************/

   // The level of input restrictions, defaults to ALPHABETIC
   private int alphaType;

   // The maximum length of the input string, defaults to 0, no maximum
   private int stringLength;


/*******************************
* PUBLIC METHODS
*******************************/

/**
* The default constructor of this class.  The default string of this text
* field is set to the empty string ("").  The maximum length is set to 0,
* which specifies no limit.
*/
   public AlphaTextArea()
   {
      this("", 0,0);
   }

/**
* Constructor of this class with the initial text of the text field specified.
* The maximum length is set to 0, which specifies no limit.
* @param text Initial text of the text field.
*/
   public AlphaTextArea(String text)
   {
      this(text, 0,0);
   }

/**
* Constructor of this class with width (in columns) and initial text of the
* text field specified.  The maximum length is set to 0, which specifies no limit.
* @param text Initial text of the text field.
* @param columns The width of the text field in characters.
*/
   public AlphaTextArea(String text, int rows,int columns)
   {
      super(text, rows,columns);
   }

/**
* Constructor that allows the user to set the Alpha type of the text field
* and the maximum string length.
* @param anAlphaType The character restriction type.
* @param aLength The maximum number of characters allowed in the string.
*/
   public AlphaTextArea(int anAlphaType, int aLength)
   {
       super("", 0,0 );
       setAlphaType( anAlphaType );
       setStringLength( aLength );
   }

/**
* Gets the current restriction type of this text field.
* @see #ALPHABETIC
* @see #ALPHANUMERIC
* @see #ALPHANUMERIC_PLUS
* @see #ALL
* @return The current restriction type as defined by the constansts of this class.
*/
   public int getAlphaType()
   {
      return alphaType;
   }

/**
* Sets the restriction type of this text field.
* @see #ALPHABETIC
* @see #ALPHANUMERIC
* @see #ALPHANUMERIC_PLUS
* @see #ALL
* @param newAlphaType The restriction of this text field.
*/
   public void setAlphaType(int newAlphaType)
   {
      switch (newAlphaType)
      {
         case ALPHABETIC:
         case ALPHANUMERIC:
         case ALPHANUMERIC_PLUS:
         case ALL:
         {
            alphaType = newAlphaType;
            break;
         }
         default:
         {
            alphaType = ALPHABETIC;
            break;
         }
      }
   }

/**
* Sets the maximum string length of this text field.  If the length is set to
* zero, then there is no limit.  The default string length is zero.  Negative
* sizes will set the length to zero.
* @param newStringLength The maximum length of the string that the user can input.
*/
   public void setStringLength(int newStringLength)
   {
      if (newStringLength < 0)
      {
         stringLength = 0;
      }
      else
      {
         stringLength = newStringLength;
      }
   }

/**
* Gets the current length of the maximum string size the user can enter.
* @return The maximum length the string of the text field can be.
*/
   public int getStringLength()
   {
      return stringLength;
   }

   public String getText() {
     return super.getText().trim();
   }

/*******************************
* PROTECTED METHODS
*******************************/

   protected boolean isValidCharacter(char aChar)
   {
      // if its a non-printable character, then its ok
//      if (((aChar < ' ') || (aChar > '~')))
      if ((aChar==BACKSPACE) || (aChar==DELETE) || (aChar==PASTE))
      {
         return true;
      }

      // can only be a printable character now, check it for validation
      return isValidCharacterType(aChar);
   }

   protected boolean isValidString(String aString)
   {
      if (aString.length() > stringLength)
      {
         return false;
      }

      for (int i = 0; i < aString.length(); ++i)
      {
         if (!(isValidCharacterType(aString.charAt(i))))
         {
            return false;
         }
      }

      return true;
   }

   protected void postProcessing()
   {
      // No need to do anything.
   }


/*******************************
* PROTECTED METHODS
*******************************/

   private boolean isValidCharacterType(char aChar)
   {
     if (aChar==KeyEvent.VK_ENTER) {
       return true;
     }

      switch (alphaType)
      {
         case ALPHABETIC:
         {
            if (!(isValidAlphabeticCharacter(aChar)))
            {
               return false;
            }
            break;
         }
         case ALPHANUMERIC:
         {
            if (!(isValidAlphanumericCharacter(aChar)))
            {
               return false;
            }
            break;
         }
         case ALPHANUMERIC_PLUS:
         {
            if (!(isValidAlphanumericPlusCharacter(aChar)))
            {
               return false;
            }
            break;
         }
         case ALL:
         {
            if (!(isValidAllCharacter(aChar)))
            {
               return false;
            }
            break;
         }
         default:
         {
            return false;
         }
      }

      return true;
   }

   private boolean isValidAlphabeticCharacter(char aChar)
   {
      if (((aChar < 'A') || (aChar > 'Z')) && ((aChar < 'a') || (aChar > 'z')))
      {
         return false;
      }
      return true;
   }

   private boolean isValidAlphanumericCharacter(char aChar)
   {
      if (((aChar < 'A') || (aChar > 'Z')) && ((aChar < 'a') || (aChar > 'z')) && ((aChar < '0') || (aChar > '9')))
      {
         return false;
      }
      return true;
   }

   private boolean isValidAlphanumericPlusCharacter(char aChar)
   {
      if (((aChar < 'A') || (aChar > 'Z')) && ((aChar < 'a') || (aChar > 'z')) && ((aChar < '0') || (aChar > '9')))
      {
         if ((aChar != ' ') && (aChar != '_') && (aChar != '-') && (aChar != ':') && (aChar != '\\'))
         {
            return false;
         }
      }
      return true;
   }

   private boolean isValidAllCharacter(char aChar)
   {
      if ((aChar < ' ') || (aChar > '~'))
      {
         return false;
      }
      return true;
   }

}
