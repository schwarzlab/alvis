package com.general.gui.controls;

import javax.swing.text.*;

/**
 * <p>Title: YANA</p>
 *
 * <p>Description: Yet Another Network Analyzer</p>
 *
 * <p>Copyright: Copyright (c) 2004</p>
 *
 * <p>Company: Dep. of Bioinformatics - University Wuerzburg</p>
 *
 * @author Roland Schwarz
 * @version 0.4
 */
public class TextFieldLimit extends PlainDocument{
  private int limit;
  // optional uppercase conversion
  private boolean toUppercase = false;

  public TextFieldLimit(int limit) {
    super();
    this.limit = limit;
  }

  public TextFieldLimit(int limit, boolean upper) {
    super();
    this.limit = limit;
    toUppercase = upper;
  }

  public void insertString(int offset, String  str, AttributeSet attr) throws BadLocationException {
    if (str == null) return;

    if ((getLength() + str.length()) <= limit) {
      if (toUppercase) {
        str = str.toUpperCase();
      }
      super.insertString(offset, str, attr);
    }
  }
}
