package com.general.gui.dnd;

import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.DataFlavor;

/**
 * <p>Title: YANA</p>
 * <p>Description: Yet Another Network Analyzer</p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: University of Wuerzburg, Department for Bioinformatics</p>
 * @author Roland Schwarz
 * @version 0.9
 */

public class DefaultDnDFactory
    implements DnDFactory {
  private static DefaultDnDFactory _INSTANCE=null;
  private DefaultDnDFactory() {}

  public static DefaultDnDFactory getInstance() {
    if (_INSTANCE==null) {
      _INSTANCE=new DefaultDnDFactory();
    }
    return _INSTANCE;
  }

  /**
   * createTransferable
   *
   * @param target Object
   * @return Transferable
   * @todo Implement this com.general.gui.dnd.DnDFactory method
   */
  public Transferable createTransferable(final Object target) {
    return new Transferable() {
      /**
       * getTransferDataFlavors
       *
       * @return DataFlavor[]
       */
      public DataFlavor[] getTransferDataFlavors() {
        return new DataFlavor[]{getDataFlavor()};
      }

      /**
       * isDataFlavorSupported
       *
       * @param flavor DataFlavor
       * @return boolean
       */
      public boolean isDataFlavorSupported(DataFlavor flavor) {
        return (flavor.match(getDataFlavor()));
      }

      /**
       * getTransferData
       *
       * @param flavor DataFlavor
       * @return Object
       */
      public Object getTransferData(DataFlavor flavor) {
        return target.toString();
      }
    };
  }

  /**
   * getDataFlavor
   *
   * @param target Object
   * @return DataFlavor
   * @todo Implement this com.general.gui.dnd.DnDFactory method
   */
  public DataFlavor getDataFlavor() {
    return DataFlavor.stringFlavor;
  }

}
