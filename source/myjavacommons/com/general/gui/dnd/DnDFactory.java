package com.general.gui.dnd;

import java.awt.datatransfer.*;

/**
 * <p>Title: YANA</p>
 * <p>Description: Yet Another Network Analyzer</p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: University of Wuerzburg, Department for Bioinformatics</p>
 * @author Roland Schwarz
 * @version 0.9
 */

public interface DnDFactory {
  public Transferable createTransferable(Object target);
  public DataFlavor getDataFlavor();
}
