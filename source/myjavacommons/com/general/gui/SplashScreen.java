package com.general.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JWindow;

/**
 * <p>
 * Title: YANA</p>
 * <p>
 * Description: Yet Another Network Analyzer</p>
 * <p>
 * Copyright: Copyright (c) 2004</p>
 * <p>
 * Company: University of Wuerzburg, Department for Bioinformatics</p>
 *
 * @author Roland Schwarz
 * @version 0.9
 */
public class SplashScreen extends JWindow {

	JLabel lblPicture;
	JLabel lblStatus = new JLabel(" ", JLabel.CENTER);
	private ImageIcon logo;
	private Color DEFAULT_BACKGROUND_COLOR=new Color(66, 148, 208);

	public SplashScreen(ImageIcon logo) {
		this.logo = logo;
		this.lblPicture = new JLabel(this.logo);

		try {
			jbInit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void jbInit() throws Exception {
		lblStatus.setBackground(DEFAULT_BACKGROUND_COLOR);
		lblStatus.setForeground(Color.white);
		lblStatus.setBorder(BorderFactory.createEtchedBorder());
		lblStatus.setOpaque(true);
		this.getContentPane().add(lblPicture, BorderLayout.CENTER);
		this.getContentPane().add(lblStatus, BorderLayout.SOUTH);
	}

	public void setStatusText(String comment) {
		lblStatus.setText(comment);
	}

	@Override
	public void setVisible(boolean b) {
		if (b) {
			pack();
			setLocationRelativeTo(null);
			setAlwaysOnTop(true);
		}
		super.setVisible(b);
	}

	public void setStatusBackgroundColor(Color color){
		lblStatus.setBackground(color);
	}
	
	public Color getStatusBackgroundColor() {
		return lblStatus.getBackground();
	}

	public void setStatusForegroundColor(Color color){
		lblStatus.setForeground(color);
	}
	
	public Color getStatusForegroundColor() {
		return lblStatus.getForeground();
	}
	
}
