package com.general.gui.fileHistory;

import java.util.EventListener;

import com.general.gui.*;

/**
 * <p>Title: YANA</p>
 *
 * <p>Description: Yet Another Network Analyzer</p>
 *
 * <p>Copyright: Copyright (c) 2004 Roland Schwarz\nThis software is licensed
 * under the GPL.\n See the licensing file that came with this distribution or
 * http://www.gnu.org/copyleft/gpl.html</p>
 *
 * <p>Company: Dep. of Bioinformatics - University Wuerzburg</p>
 *
 * @author Roland Schwarz
 * @version 0.4
 */
public interface FileHistoryListener extends EventListener{
  public void historyChanged(FileHistoryEvent e);
}
