package com.general.gui.fileHistory;

import java.util.*;
import java.io.File;
import javax.swing.event.EventListenerList;

/**
 * <p>Title: YANA</p>
 *
 * <p>Description: Yet Another Network Analyzer</p>
 *
 * <p>Copyright: Copyright (c) 2004 Roland Schwarz\nThis software is licensed
 * under the GPL.\n See the licensing file that came with this distribution or
 * http://www.gnu.org/copyleft/gpl.html</p>
 *
 * <p>Company: Dep. of Bioinformatics - University Wuerzburg</p>
 *
 * @author Roland Schwarz
 * @version 0.4
 */
public class FileHistory {
  private static FileHistory _INSTANCE=null;
  private final static int HISTORY_COUNT=20;
  private final static String HISTORY_TAG="file_history";
  private final List<File> fileList=new ArrayList<>(HISTORY_COUNT);
  private final EventListenerList listenerList=new EventListenerList();

  public static FileHistory getInstance() {
    if (_INSTANCE==null)
      _INSTANCE=new FileHistory();
    return _INSTANCE;
  }

  private FileHistory() {
  }

  public void registerFile(File file) {
    if (fileList.contains(file))
      fileList.remove(file);
    fileList.add(0,file);
    fireHistoryChanged();
  }

  public int size() {
	  return fileList.size();
  }
  
  public File elementAt(int historyIndex) {
    File file=null;
    if (historyIndex>=0 && historyIndex<fileList.size())
      file=fileList.get(historyIndex);
    return file;
  }

  public Properties toProperties() {
	  Properties props = new Properties();
	  return toProperties(props);
  }
  
  public Properties toProperties(Properties props) {
    int i=0;
    for (File file:fileList) {
      props.setProperty(HISTORY_TAG+i,file.getAbsolutePath());
      i++;
    }
    return props;
  }

  public void fromProperties(Properties props) {
    fileList.clear();
    for (int i=0;i<HISTORY_COUNT;i++) {
      String tag=HISTORY_TAG+i;
      String value=props.getProperty(tag);
      if (value!=null) {
        fileList.add(i,new File(value));
      }
    }
  }

  // event generator methods
  public void addFileHistoryListener(FileHistoryListener l) {
    listenerList.add(FileHistoryListener.class,l);
  }

  public void removeFileHistoryListener(FileHistoryListener l) {
    listenerList.remove(FileHistoryListener.class,l);
  }

  private void fireHistoryChanged() {
    FileHistoryEvent e= new FileHistoryEvent(this);
    FileHistoryListener[] listeners=listenerList.getListeners(FileHistoryListener.class);
    for (FileHistoryListener l:listeners)
      l.historyChanged(e);
  }
}
