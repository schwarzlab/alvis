package com.general.gui.fileHistory;

import java.io.*;

import java.awt.event.*;
import javax.swing.*;



/**
 * <p>Title: YANA</p>
 *
 * <p>Description: Yet Another Network Analyzer</p>
 *
 * <p>Copyright: Copyright (c) 2004 Roland Schwarz\nThis software is licensed
 * under the GPL.\n See the licensing file that came with this distribution or
 * http://www.gnu.org/copyleft/gpl.html</p>
 *
 * <p>Company: Dep. of Bioinformatics - University Wuerzburg</p>
 *
 * @author Roland Schwarz
 * @version 0.4
 */
public abstract class FileHistoryAction
    extends AbstractAction implements FileHistoryListener{
  private int historyIndex;
  private File file=null;

  public FileHistoryAction(int historyIndex) {
    FileHistory.getInstance().addFileHistoryListener(this);
    this.historyIndex=historyIndex;
    updateAction();
  }

  private void updateAction() {
    file=FileHistory.getInstance().elementAt(historyIndex);
    if (file!=null) {
      putValue(NAME, historyIndex+1+": "+file.getName());
      putValue(SHORT_DESCRIPTION,file.getAbsolutePath());
      putValue(MNEMONIC_KEY,KeyStroke.getKeyStroke(String.valueOf(historyIndex+1)).getKeyCode());
      setEnabled(true);
    } else {
      putValue(NAME, "");
      putValue(SHORT_DESCRIPTION,"");
      setEnabled(false);
    }
  }

  public File getFile() {
    return file;
  }

  /**
   * Invoked when an action occurs.
   *
   * @param e ActionEvent
   * @todo Implement this java.awt.event.ActionListener method
   */
  public abstract void actionPerformed(ActionEvent e);

  // HistoryListener interface
  public void historyChanged(FileHistoryEvent e) {
    updateAction();
  }
}
