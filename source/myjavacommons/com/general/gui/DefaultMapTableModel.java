package com.general.gui;

import java.util.*;

import javax.swing.table.*;

/**
 * <p>Title: YANA</p>
 *
 * <p>Description: Yet Another Network Analyzer</p>
 *
 * <p>Copyright: Copyright (c) 2004</p>
 *
 * <p>Company: Dep. of Bioinformatics - University Wuerzburg</p>
 *
 * @author Roland Schwarz
 * @version 0.4
 */
public class DefaultMapTableModel<K,V> extends AbstractTableModel{
  private String[] headers=null;
  private SortedMap<K,V> sortedMap=null;
  private Map<K,V> subject=null;

  public DefaultMapTableModel(Map<K,V> subject,String[] columnHeaders) {
    this.subject=subject;
    this.headers=columnHeaders;
    updateMapping();
  }

  public void updateMapping() {
    this.sortedMap=new TreeMap<K,V>(subject);
    fireTableDataChanged();
  }

  public int getRowCount() {
    return sortedMap.keySet().size();
  }

  public int getColumnCount() {
    return headers.length;
  }

  protected Map.Entry<K,V> entryAt(int index) {
    int i=0;
    for (Map.Entry<K,V> entry : sortedMap.entrySet()) {
      if (i==index) {
        return entry;
      }
      i++;
    }
    return null;
  }

  public Object getValueAt(int rowIndex, int columnIndex) {
    Map.Entry entry=entryAt(rowIndex);
    switch (columnIndex) {
      case 0:
        return entry.getKey();
      case 1:
        return entry.getValue();
    }
    return null;
  }

  /**
   * Returns the most specific superclass for all the cell values in the column.
   *
   * @param columnIndex the index of the column
   * @return the common ancestor class of the object values in the model.
   * @todo Implement this javax.swing.table.TableModel method
   */
  public Class getColumnClass(int columnIndex) {
    Object o =getValueAt(0,columnIndex);
    if (o==null) {
      return null;
    }
    else {
      return o.getClass();
    }
  }

  /**
   * Returns the name of the column at <code>columnIndex</code>.
   *
   * @param columnIndex the index of the column
   * @return the name of the column
   * @todo Implement this javax.swing.table.TableModel method
   */
  public String getColumnName(int columnIndex) {
    return headers[columnIndex];
  }

  /**
   * Returns true if the cell at <code>rowIndex</code> and
   * <code>columnIndex</code> is editable.
   *
   * @param rowIndex the row whose value to be queried
   * @param columnIndex the column whose value to be queried
   * @return true if the cell is editable
   * @todo Implement this javax.swing.table.TableModel method
   */
  public boolean isCellEditable(int rowIndex, int columnIndex) {
    return false;
  }

  /**
   * Sets the value in the cell at <code>columnIndex</code> and
   * <code>rowIndex</code> to <code>aValue</code>.
   *
   * @param aValue the new value
   * @param rowIndex the row whose value is to be changed
   * @param columnIndex the column whose value is to be changed
   * @todo Implement this javax.swing.table.TableModel method
   */
  public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
  }

}
