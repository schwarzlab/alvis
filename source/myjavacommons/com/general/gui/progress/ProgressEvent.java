package com.general.gui.progress;

import java.util.EventObject;

import com.general.gui.*;

/**
 * <p>Title: YANA - Yet Another Network Analyzer</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Dep. of Bioinformatics, University Wuerzburg</p>
 *
 * @author Roland Schwarz
 * @version 0.8
 */
public class ProgressEvent extends EventObject {
  public ProgressEvent(Progressable source) {
    super(source);
  }

  @Override
  public Progressable getSource() {
    return (Progressable)super.getSource();
  }
}
