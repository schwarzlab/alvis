package com.general.gui.progress;

import java.util.EventListener;

import com.general.gui.*;

/**
 * <p>Title: YANA - Yet Another Network Analyzer</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Dep. of Bioinformatics, University Wuerzburg</p>
 *
 * @author Roland Schwarz
 * @version 0.8
 */
public interface ProgressListener extends EventListener{
	public void progressChanged(ProgressEvent e);
}
