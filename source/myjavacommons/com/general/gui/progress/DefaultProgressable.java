package com.general.gui.progress;

import javax.swing.DefaultBoundedRangeModel;
import javax.swing.event.EventListenerList;
import javax.swing.SwingUtilities;
import com.general.Log;

/**
 * <p>
 * Title: YANA - Yet Another Network Analyzer</p>
 *
 * <p>
 * Description: </p>
 *
 * <p>
 * Copyright: Copyright (c) 2005</p>
 *
 * <p>
 * Company: Dep. of Bioinformatics, University Wuerzburg</p>
 *
 * @author Roland Schwarz
 * @version 0.8
 */
public class DefaultProgressable extends DefaultBoundedRangeModel implements Progressable {

	private final EventListenerList listenerList = new EventListenerList();
	private String statusText = "";
	private String statusDescription = null;
	private boolean indeterminate = false;
	private int state = Progressable.STATE_IDLE;
	Object task;

	public DefaultProgressable() {
		this(null, "", true);
	}

	public DefaultProgressable(String statusText) {
		this(null, statusText, true);
	}

	public DefaultProgressable(String statusText, boolean indeterminate) {
		this(null, statusText, indeterminate);
	}

	public DefaultProgressable(Object task, String statusText, boolean indeterminate) {
		setStatusText(statusText);
		setIndeterminate(indeterminate);
		this.task = task;
	}

	public String getStatusText() {
		return statusText;
	}

	public void setStatusText(String text) {
		this.statusText = text;
		fireProgressChanged();
	}

	@Override
	public void setValue(int n) {
		super.setValue(n); //To change body of generated methods, choose Tools | Templates.
		fireProgressChanged();
	}

	@Override
	public void setMaximum(int n) {
		if (getMaximum() != n) {
			super.setMaximum(n); //To change body of generated methods, choose Tools | Templates.
			fireProgressChanged();
		}
	}

	@Override
	public void setMinimum(int n) {
		if (getMinimum() != n) {
			super.setMinimum(n); //To change body of generated methods, choose Tools | Templates.
			fireProgressChanged();
		}
	}

	public void addProgressListener(ProgressListener l) {
		listenerList.add(ProgressListener.class, l);
	}

	public void removeProgressListener(ProgressListener l) {
		listenerList.remove(ProgressListener.class, l);
	}

	protected void fireProgressChanged() {
		final ProgressEvent e = new ProgressEvent(this);
		final ProgressListener[] listeners = listenerList.getListeners(ProgressListener.class);
		for (final ProgressListener l : listeners) {
			if (SwingUtilities.isEventDispatchThread()) {
				l.progressChanged(e);
			} else {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						l.progressChanged(e);
					}
				});
			}
			
		}
	}

	public boolean isIndeterminate() {
		return this.indeterminate;
	}

	public void setIndeterminate(boolean flag) {
		if (this.indeterminate != flag) {
			this.indeterminate = flag;
			fireProgressChanged();
		}
	}

	@Override
	public int getState() {
		return state;
	}

	public void setState(int state) {
		if (state != this.state) {
			this.state = state;
			fireProgressChanged();
		}
	}

	@Override
	public Object getTask() {
		return task;
	}

	/**
	 * @param task the task to set
	 */
	public void setTask(Object task) {
		this.task = task;
	}

	/**
	 * @return the statusDescription
	 */
	@Override
	public String getStatusDescription() {
		return statusDescription;
	}

	/**
	 * @param statusDescription the statusDescription to set
	 */
	public void setStatusDescription(String statusDescription) {
		if (this.statusDescription==null || !this.statusDescription.equals(statusDescription)) {
			this.statusDescription = statusDescription;			
			fireProgressChanged();
		}
	}
}
