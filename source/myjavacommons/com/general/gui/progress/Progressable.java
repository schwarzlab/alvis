package com.general.gui.progress;

import javax.swing.BoundedRangeModel;
import com.general.gui.*;

/**
 * <p>Title: YANA - Yet Another Network Analyzer</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Dep. of Bioinformatics, University Wuerzburg</p>
 *
 * @author Roland Schwarz
 * @version 0.8
 */
public interface Progressable extends BoundedRangeModel {
	public static final int STATE_IDLE=0;
	public static final int STATE_RUNNING=1;
	public String getStatusText();
	public String getStatusDescription();
	public boolean isIndeterminate();
	public void addProgressListener(ProgressListener l);
	public void removeProgressListener(ProgressListener l);
	public int getState();
	public Object getTask();
}
