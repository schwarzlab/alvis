package com.general.gui.progress;

import com.general.containerModel.impl.MapDefaultValueDecorator;
import com.general.mvc.view.IView;
import com.general.resources.MyJavaResources;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextArea;
import javax.swing.JToolTip;
import javax.swing.RootPaneContainer;
import javax.swing.SwingWorker;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.plaf.basic.BasicInternalFrameUI;
import javax.swing.plaf.basic.BasicRootPaneUI;
import prefuse.util.ui.JCustomTooltip;

/**
 * <p>
 * Title: YANA - Yet Another Network Analyzer</p>
 *
 * <p>
 * Description: </p>
 *
 * <p>
 * Copyright: Copyright (c) 2005</p>
 *
 * <p>
 * Company: Dep. of Bioinformatics, University Wuerzburg</p>
 *
 * @author Roland Schwarz
 * @version 0.8
 */
public class ProgressView extends JInternalFrame implements IView, ProgressListener, ComponentListener {

	List<Progressable> models;
	Map<Progressable, JProgressBar> progressBarForModel;
	Map<Progressable, JButton> buttonForModel;
	Map<Progressable, Boolean> visibilityMap;

	Component anchor;

	public static final int ANCHOR_LEFT = 0;
	public static final int ANCHOR_RIGHT = 1;
	public static final int ANCHOR_TOP = 0;
	public static final int ANCHOR_BOTTOM = 2;
	public static final int ANCHOR_INSIDE_X = 0;
	public static final int ANCHOR_OUTSIDE_X = 4;
	public static final int ANCHOR_INSIDE_Y = 0;
	public static final int ANCHOR_OUTSIDE_Y = 8;

	int anchorPosition;
	int index;
	private Color tooltipBackground;
	private Border tooltipBorder;

	public ProgressView(RootPaneContainer container) {
		this(container, null, 0);
	}

	public ProgressView(RootPaneContainer container, Component anchor, int anchorPosition) {
		super(null, false, false, false, false);
		models = new ArrayList<>();
		progressBarForModel = new HashMap<>();
		tooltipBorder = BorderFactory.createEtchedBorder(EtchedBorder.LOWERED);
		buttonForModel = new HashMap<>();
		visibilityMap = new MapDefaultValueDecorator<>(new HashMap<Progressable, Boolean>(), false);
		this.anchor = anchor;
		this.anchorPosition = anchorPosition;
		if (this.anchor!=null) {
			this.anchor.addComponentListener(this);
		}
		updateLocation();
		BasicInternalFrameUI ui = new BasicInternalFrameUI(this);
		this.setUI(ui);
		BasicRootPaneUI rpui = new BasicRootPaneUI();
		this.getRootPane().setUI(rpui);
		ui.setNorthPane(null);
		this.getRootPane().setOpaque(false);
		this.setOpaque(false);
		this.setFocusable(false);
		this.getContentPane().setLayout(new GridBagLayout());
		this.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		this.pack();
		container.getLayeredPane().add(this);
	}

	@Override
	public Component getComponent() {
		return this;
	}

	@Override
	public void closing() {
		for (Progressable p : models) {
			p.removeProgressListener(this);
		}
	}

	@Override
	public void save() {
	}

	@Override
	public boolean verify() {
		return false;
	}

	public void addModel(Progressable model) { addModel(model, true); }
	public void addModel(final Progressable model, boolean autoRemove) {
		models.add(model);
		model.addProgressListener(this);
		JProgressBar progressBar = createProgressBar();
		progressBar.setModel(model);
		progressBarForModel.put(model, progressBar);
		visibilityMap.put(model, false);
		JButton cmdCancel = createCancelButton();
		buttonForModel.put(model, cmdCancel);
		if (autoRemove) {
			if (model.getTask() instanceof SwingWorker) {
				((SwingWorker)model.getTask()).addPropertyChangeListener(new PropertyChangeListener() {
					@Override
					public void propertyChange(PropertyChangeEvent evt) {
						if (evt.getPropertyName().equals("state") && evt.getNewValue().equals(SwingWorker.StateValue.DONE)) {
							removeModel(model);
						}
					}
				});
			}
		}
	}

	public void removeModel(Progressable p) {
		p.removeProgressListener(this);
		models.remove(p);
		JProgressBar bar = progressBarForModel.remove(p);
		if (bar!=null){
			this.getContentPane().remove(bar);
		}
		JButton button = buttonForModel.remove(p);
		if (button!=null){
			this.getContentPane().remove(button);
		}
		visibilityMap.remove(p);		
	}

	private JProgressBar createProgressBar() {
		JProgressBar bar = new JProgressBar(){
			@Override
			public JToolTip createToolTip() {
				JPanel panel = new JPanel(new BorderLayout());
				panel.setOpaque(false);

				JTextArea label = new JTextArea(getToolTipText(null));
				label.setEditable(false);
				label.setBackground(tooltipBackground);
				label.setBorder(tooltipBorder);

				panel.add(label, BorderLayout.CENTER);
				JCustomTooltip tt = new JCustomTooltip(this, panel, false);
				tt.setOpaque(false);
				return tt;
			}
		};
		return bar;
	}
	
	public int getNumberOfVisibleItems() {
		int count = 0;
		for (Progressable p : models) {
			if (visibilityMap.get(p)) {
				count++;
			}
		}
		return count;
	}

	@Override
	public void setBackground(Color color) {
		this.getContentPane().setBackground(color);
	}

	public void setTooltipBackground(Color color) {
		if (color==null) {
			color = new Color(0,0,0,0);
		}
		this.tooltipBackground=color;
	}
	
	public void setTooltipBorder(Border border){
		this.tooltipBorder = border;
	}
	
	private void updateProgress(ProgressEvent e) {
		if (e != null) {
			Progressable model = e.getSource();
			JProgressBar progressBar = progressBarForModel.get(model);
			progressBar.setStringPainted(true);
			progressBar.setIndeterminate(model.isIndeterminate());
			if (!model.isIndeterminate()) {
				progressBar.setString(model.getStatusText() + "  [" + Math.round(progressBar.getPercentComplete() * 100) + "%]");
			} else {
				progressBar.setString(model.getStatusText());
			}
			if (model.getStatusDescription()!=null){
				progressBar.setToolTipText(model.getStatusDescription());
			}
			JButton cmdCancel = buttonForModel.get(model);
			cmdCancel.setAction(createCancelAction(model.getTask()));
			FontMetrics fm = progressBar.getFontMetrics(progressBar.getFont());
			int width = fm.stringWidth(progressBar.getString());
			this.setSize(new Dimension(width+20, this.getSize().height));

			if (model.getState() == Progressable.STATE_RUNNING && !visibilityMap.get(model)) {
				Insets insets = new Insets(0, 2, 0, 2);
				this.getContentPane().add(progressBar, new GridBagConstraints(0, index, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, insets, 0, 0));
				this.getContentPane().add(cmdCancel, new GridBagConstraints(1, index, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, insets, 0, 0));					
				this.setVisible(true);
				index++;
				visibilityMap.put(model, true);
			} else if (model.getState() == Progressable.STATE_IDLE && visibilityMap.get(model)) {
				this.getContentPane().remove(progressBar);
				this.getContentPane().remove(cmdCancel);
				visibilityMap.put(model, false);
				index--;
			}
			boolean hide = true;
			for (Progressable p : models) {
				hide &= (p.getState() == Progressable.STATE_IDLE);
			}
			if (hide) { // all idle
				this.setVisible(false);
			}
			this.pack();
			validate();
			repaint();
			updateLocation();													
		}
	}

	private JButton createCancelButton() {
		JButton cmdCancel= new JButton();
		cmdCancel.setBorder(null);
		cmdCancel.setOpaque(false);
		return cmdCancel;
	}
	
	private Action createCancelAction(Object task){
		if (task instanceof SwingWorker){
			return new SwingWorkerCancelAction((SwingWorker)task);
		} else {
			return null;
		}
	}
	
	private Point determineAnchorPosition() {
		Point p = new Point();
		boolean isRight = (anchorPosition & ANCHOR_RIGHT) == ANCHOR_RIGHT;
		boolean isLeft = !isRight;
		boolean isBottom = (anchorPosition & ANCHOR_BOTTOM) == ANCHOR_BOTTOM;
		boolean isTop = !isBottom;
		boolean isOutsideX = (anchorPosition & ANCHOR_OUTSIDE_X) == ANCHOR_OUTSIDE_X;
		boolean isInsideX = !isOutsideX;
		boolean isOutsideY = (anchorPosition & ANCHOR_OUTSIDE_Y) == ANCHOR_OUTSIDE_Y;
		boolean isInsideY = !isOutsideY;
		int anchorX = anchor.getX();
		int anchorY = anchor.getY();
		int anchorWidth = anchor.getWidth();
		int anchorHeight = anchor.getHeight();

		if (isBottom) {
			anchorY += anchorHeight;
		}
		if (isRight) {
			anchorX += anchorWidth;
		}

		if ((isLeft && isInsideX) || (isRight && isOutsideX)) {
			p.x = anchorX;
		} else {
			p.x = anchorX - this.getPreferredSize().width;
		}

		if ((isTop && isInsideY) || (isBottom && isOutsideY)) {
			p.y = anchorY;
		} else {
			p.y = anchorY - this.getPreferredSize().height;
		}
		return p;
	}

	@Override
	public void progressChanged(ProgressEvent e) {
		updateProgress(e);
	}

	/**
	 * @return the anchor
	 */
	public Component getAnchor() {
		return anchor;
	}

	/**
	 * @param anchor the anchor to set
	 * @param anchorPosition
	 */
	public void setAnchor(Component anchor, int anchorPosition) {
		if (this.anchor!=null){
			this.anchor.removeComponentListener(this);
		}
		this.anchor = anchor;
		this.anchor.addComponentListener(this);
		this.anchorPosition = anchorPosition;
		updateLocation();
	}

	/**
	 * @return the anchorPosition
	 */
	public int getAnchorPosition() {
		return anchorPosition;
	}

	/**
	 * @param anchorPosition the anchorPosition to set
	 */
	public void setAnchorPosition(int anchorPosition) {
		this.anchorPosition = anchorPosition;
		updateLocation();
	}

	private void updateLocation() {
		if (anchor != null) {
			Point p = determineAnchorPosition();
			setLocation(p);
		}
	}
	
	@Override
	public void componentResized(ComponentEvent e) {
		updateLocation();
	}

	@Override
	public void componentMoved(ComponentEvent e) {
		updateLocation();
	}

	@Override
	public void componentShown(ComponentEvent e) {
		updateLocation();
	}

	@Override
	public void componentHidden(ComponentEvent e) {
		updateLocation();
	}

	private class SwingWorkerCancelAction extends AbstractAction {
		private SwingWorker worker;
		SwingWorkerCancelAction(SwingWorker worker){
			putValue(SMALL_ICON, new ImageIcon(MyJavaResources.DELETE_32x32.getImage().getScaledInstance(16, 16, Image.SCALE_SMOOTH)));
			this.worker=worker;
		}
		
		@Override
		public void actionPerformed(ActionEvent e) {
			worker.cancel(true);
		}
		
	}
}
