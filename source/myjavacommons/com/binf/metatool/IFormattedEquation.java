package com.binf.metatool;

import java.util.*;
/**
 * <p>Title: YANA</p>
 * <p>Description: Yet Another Network Analyzer</p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: University of Wuerzburg, Department for Bioinformatics</p>
 * @author Roland Schwarz
 * @version 0.9
 */

public interface IFormattedEquation {
  public String getEnzymeName();
  public String[] getSubstrates();
  public String[] getProducts();
  public int getSubstrateCoefficient(String substrate);
  public int getProductCoefficient(String product);
}
