package com.binf.metatool;

import java.io.IOException;
import java.util.*;

/**
 * <p>Title: YANA</p>
 * <p>Description: Yet Another Network Analyzer</p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: University of Wuerzburg, Department for Bioinformatics</p>
 * @author Roland Schwarz
 * @version 0.9
 */

public interface IMetatoolDataAccess {
  public Set<String> getInternalMetabolites();
  public Set<String> getExternalMetabolites();
  public Set<String> getReversibleReactions();
  public Set<String> getIrreversibleReactions();
  public IFormattedEquation getReactionEquation(String reaction) throws IOException;
  public int getThreshold();
}
