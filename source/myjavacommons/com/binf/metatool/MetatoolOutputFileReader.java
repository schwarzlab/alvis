package com.binf.metatool;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import com.general.Log;

public class MetatoolOutputFileReader {
  private File file=null;
  private List<String> emodes=new ArrayList<String>(10000);
  private List<Integer> convexBasis=new ArrayList<Integer>(1000);
  public static final String EMODE_START_TAG="<EMODE_DATA>";
  public static final String EMODE_END_TAG="</EMODE_DATA>";
  public static final String CB_TAG="Convex basis:";

  public MetatoolOutputFileReader(File file) {
    this.file=file;
  }

  /**
   * getElementaryModes
   *
   * @return List
   */
  public List<String> getElementaryModes() {
    return emodes;
  }

  public List<Integer> getConvexBasis() {
    return convexBasis;
  }

  /**
   * load
   *
   */
  public void read() throws IOException{
    BufferedReader reader = new BufferedReader(new FileReader(file));
    int i=0;
    // read until emode start tag
    String line = null;
    boolean emodeBlock = false;
    while ( (line = reader.readLine()) != null) {

      if (line.indexOf(EMODE_END_TAG) >= 0) {
        emodeBlock = false;
      }

      if (emodeBlock) {
        emodes.add(line);
//        Log.out("added line :"+i++);
      }

      if (line.indexOf(EMODE_START_TAG) >= 0) {
        emodeBlock = true;
      }

      int index=line.indexOf(CB_TAG);
      if (index>=0) {
        extractConvexBasis(line.substring(index+CB_TAG.length()));
      }
    }

  }

  /**
   * extractConvexBasis
   *
   * @param line String
   */
  private void extractConvexBasis(String line) {
    StringTokenizer st=new StringTokenizer(line," ",false);
    while (st.hasMoreTokens()) {
      String token=st.nextToken();
      try {
        Integer index = Integer.valueOf(token);
        convexBasis.add(index-1); // metatool starts counting at "1", our array starts at "0"
      } catch (NumberFormatException ex) {
        Log.out(ex);
      }
    }
  }


}
