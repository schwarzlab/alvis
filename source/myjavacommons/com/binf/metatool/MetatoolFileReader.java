package com.binf.metatool;

import java.io.*;
import java.util.*;
/**
 * <p>Title: METATOOL Input File Generator</p>
 * <p>Description: Generates input files for METATOOL</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: Biozentrum University Wuerzburg</p>
 * @author Roland Schwarz
 * @version 0.5
 */

public class MetatoolFileReader
    implements IMetatoolDataAccess {
  protected List<String> reactionEquations=new ArrayList<String>();
  protected String enzRevData="",enzIrevData="",metIntData="",metExtData="",threshData="0";

    public MetatoolFileReader() {}

    public static boolean isMetatoolInputFile(File file) {
      boolean retval=true;
      StringBuffer content=new StringBuffer();
      try {
        BufferedReader in = new BufferedReader(new FileReader(file));
        String input = in.readLine();
        while (input!=null) {
          content.append(input);
          input=in.readLine();
        }
      } catch (IOException ex) {
        retval=false;
      }

      String all=content.toString();
      retval=
          (all.indexOf("-ENZREV")>=0) &&
          (all.indexOf("-ENZIRREV")>=0) &&
          (all.indexOf("-METINT")>=0) &&
          (all.indexOf("-METEXT")>=0) &&
          (all.indexOf("-CAT")>=0);
      return retval;
    }

    public void read(File inputFile) throws IOException{

        BufferedReader in=new BufferedReader(new FileReader(inputFile));
        String input=in.readLine();

        String mode="";

        while (input!=null) {
          input=input.trim();
          if (input.equals("-ENZREV")) {
            mode="ENZREV";
          }
          else if (input.equals("-ENZIRREV")) {
            mode="ENZIRREV";
          }
          else if (input.equals("-METINT")) {
            mode="METINT";
          }
          else if (input.equals("-METEXT")) {
            mode="METEXT";
          }
          else if (input.equals("-CAT")) {
            mode = "CAT";
          }
          else if (input.equals("-THRESH")) {
            mode = "THRESH";
          }
          else {
            input += " ";
            if (mode.equals("ENZREV")) {
              enzRevData += input;
            }
            else if (mode.equals("ENZIRREV")) {
              enzIrevData += input;
            }
            else if (mode.equals("METINT")) {
              metIntData += input;
            }
            else if (mode.equals("METEXT")) {
              metExtData += input;
            }
            else if (mode.equals("CAT")) {
              String equation = input.trim();
              if (equation.length() > 0) {
                reactionEquations.add(equation);
              }
            }
            else if (mode.equals("THRESH")) {
              threshData = input;
            }
          }
          input = in.readLine();
        }

        metIntData = metIntData.trim();
        metExtData = metExtData.trim();
        enzRevData = enzRevData.trim();
        enzIrevData = enzIrevData.trim();
    }

    private Set<String> extractItems(String text) {
      Set<String> data=new HashSet<String>();
      StringTokenizer token=new StringTokenizer(text," ",false);
      while (token.hasMoreTokens()) {
        String item=token.nextToken().trim();
        data.add(item);
      }
      return data;
    }


    //******************************************************************
    // MetatoolDataAccess Interface
    //******************************************************************

    /**
   * getInternalMetabolites
   *
   * @return String[]
   * @todo Implement this com.binf.metatool.MetatoolDataAccess method
   */
  public Set<String> getInternalMetabolites() {
    return extractItems(metIntData);
  }

  /**
   * getExternalMetabolites
   *
   * @return String[]
   * @todo Implement this com.binf.metatool.MetatoolDataAccess method
   */
  public Set<String> getExternalMetabolites() {
    return extractItems(metExtData);
  }

  /**
   * getReversibleReactions
   *
   * @return String[]
   * @todo Implement this com.binf.metatool.MetatoolDataAccess method
   */
  public Set<String> getReversibleReactions() {
    return extractItems(enzRevData);
  }

  /**
   * getIrreversibleReactions
   *
   * @return String[]
   * @todo Implement this com.binf.metatool.MetatoolDataAccess method
   */
  public Set<String> getIrreversibleReactions() {
    return extractItems(enzIrevData);
  }

  /**
   * getReactionEquationFor
   *
   * @param reaction String
   * @return String
   * @todo Implement this com.binf.metatool.MetatoolDataAccess method
   */
  public IFormattedEquation getReactionEquation(String reaction) throws IOException{
    for (String equ : reactionEquations) {
      StringTokenizer tok = new StringTokenizer(equ, ":", false);
      try {
        String enzyme = tok.nextToken().trim();
        if (enzyme.equals(reaction)) {
          return MetatoolEquationFormatter.parseEquation(equ);
        }
      } catch (NoSuchElementException ex) {
        throw new IOException("Error analysing equation for enzyme: "+reaction);
      }
    }
    return null;
  }

  /**
   * getThreshold
   *
   * @return int
   */
  public int getThreshold() {
    try {
      return Integer.valueOf(threshData);
    } catch (NumberFormatException ex) {
      return 0;
    }
  }
}
