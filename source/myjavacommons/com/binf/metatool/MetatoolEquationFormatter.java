package com.binf.metatool;

import java.util.StringTokenizer;
import java.util.NoSuchElementException;
import java.util.Vector;
import java.util.*;
import java.util.Hashtable;

/**
 * <p>Title: YANA</p>
 * <p>Description: Yet Another Network Analyzer</p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: University of Wuerzburg, Department for Bioinformatics</p>
 * @author Roland Schwarz
 * @version 0.9
 */

public class MetatoolEquationFormatter
    implements IFormattedEquation {
  private String equation=null;
  private String enzymeName="";
  private List<String> substrates=new ArrayList<String>();
  private Hashtable<String, Integer> substrateCoefficients=new Hashtable<String, Integer>();
  private List<String> products=new ArrayList<String>();
  private Hashtable<String, Integer> productCoefficients=new Hashtable<String, Integer>();

  private MetatoolEquationFormatter(String equation) {
    this.equation=equation.trim();
    if (this.equation.endsWith(".")) {
      this.equation=this.equation.substring(0,equation.length()-1).trim();
    }
    parseEquation();
  }

  public static String getEquation(IFormattedEquation reaction) {
    StringBuffer equation=new StringBuffer();
    equation.append(reaction.getEnzymeName());
    equation.append(" : ");
    for (int i=0;i<reaction.getSubstrates().length;i++) {
      int coefficient=Math.abs(reaction.getSubstrateCoefficient(reaction.getSubstrates()[i]));
      String metabolite=reaction.getSubstrates()[i];
      if (coefficient>1) {
        equation.append(coefficient);
        equation.append(" ");
      }
      equation.append(metabolite);
      if (i<reaction.getSubstrates().length-1) {
        equation.append(" + ");
      }
    }

    equation.append(" = ");

    for (int i=0;i<reaction.getProducts().length;i++) {
      int coefficient=Math.abs(reaction.getProductCoefficient(reaction.getProducts()[i]));
      String metabolite=reaction.getProducts()[i];
      if (coefficient>1) {
        equation.append(coefficient);
        equation.append(" ");
      }
      equation.append(metabolite);
      if (i<reaction.getProducts().length-1) {
        equation.append(" + ");
      }
    }

    equation.append(" .");
    return equation.toString();
  }

  public static IFormattedEquation parseEquation(String equation) {
    IFormattedEquation formatter=new MetatoolEquationFormatter(equation);
    return formatter;
  }

  /**
   * parseEquation
   */
  private void parseEquation() {
    String rest="";
    StringTokenizer tokenizeMain=new StringTokenizer(equation,":",false);
    try {
      this.enzymeName = tokenizeMain.nextToken().trim();
      rest = tokenizeMain.nextToken();
      StringTokenizer tokenizeRest=new StringTokenizer(rest,"=",false);
      String substrates=tokenizeRest.nextToken().trim();
      String products=tokenizeRest.nextToken().trim();
      parseSubstrates(substrates);
      parseProducts(products);
    } catch (NoSuchElementException ex) {

    }

  }

  private void parseSubstrates(String text) {
    parseMetabolites(text,this.substrates,this.substrateCoefficients);
  }

  private void parseProducts(String text) {
    parseMetabolites(text,this.products,this.productCoefficients);
  }

  private void parseMetabolites(String metabolites, List<String> metaboliteList, Hashtable<String,Integer> coefficients) {
    String[] split = metabolites.split(" \\+ ");
//    StringTokenizer tk=new StringTokenizer(metabolites," + ",false);
    for (String metabolite : split) {
//      String metabolite=tk.nextToken().trim();
      StringTokenizer metTok=new StringTokenizer(metabolite.trim()," ",false);
      String whatever=metTok.nextToken().trim();
      Integer coefficient=new Integer(1);
      String realMetabolite="";
      try {
        coefficient=Integer.valueOf(whatever);
        realMetabolite=metTok.nextToken().trim();
      } catch (NumberFormatException ex) {
        realMetabolite=whatever;
      }
      metaboliteList.add(realMetabolite);
      coefficients.put(realMetabolite,coefficient);
    }
  }

  /**
   * getEnzymeName
   *
   * @return String
   * @todo Implement this com.binf.metatool.FormattableReaction method
   */
  public String getEnzymeName() {
    return enzymeName;
  }

  /**
   * getSubstrates
   *
   * @return String[]
   * @todo Implement this com.binf.metatool.FormattableReaction method
   */
  public String[] getSubstrates() {
    return (String[])this.substrates.toArray(new String[]{});
  }

  /**
   * getProducts
   *
   * @return String[]
   * @todo Implement this com.binf.metatool.FormattableReaction method
   */
  public String[] getProducts() {
    return (String[])this.products.toArray(new String[]{});
  }

  /**
   * getSubstrateCoefficient
   *
   * @param substrate String
   * @return int
   * @todo Implement this com.binf.metatool.FormattableReaction method
   */
  public int getSubstrateCoefficient(String substrate) {
    return ((Integer)substrateCoefficients.get(substrate)).intValue();
  }

  /**
   * getProductCoefficient
   *
   * @param product String
   * @return int
   * @todo Implement this com.binf.metatool.FormattableReaction method
   */
  public int getProductCoefficient(String product) {
    return ((Integer)productCoefficients.get(product)).intValue();
  }
}
