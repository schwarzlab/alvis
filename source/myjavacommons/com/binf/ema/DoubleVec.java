/*
 * Copyright (C) 2005  Christoph Kaleta
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.binf.ema;

import java.util.Collections;
import java.util.Vector;


/**
 * Data structure for fast saving and combination of double-vectors (for EMA)
 *  @author Christoph Kaleta
 */
public class DoubleVec {
    int capacityIncrement;
    int dataSize;
    double[][] fieldVec;		//field of fields where data is saved
    int size;
    DoubleVec oldVec;
    int fieldCount;
    int cPos;				   //position in current field
    boolean [][] skipEntry;	    //entrys that should be ignored due to condition seven
    double[] zeroVec;			//simple zero-entry
    int recurCount;
    int doubleCount;
    int maxFieldCount=1000;	    //maximum number of fields

    /**
     *
     * @param capacityIncrement Number of entrys per field
     * @param dataSize Size of an entry
     * @param oldVec previous DoubleVec
     */
    public DoubleVec(int capacityIncrement, int dataSize,DoubleVec oldVec){
        recurCount=0;
        doubleCount=0;

    	this.capacityIncrement=capacityIncrement;
        this.dataSize=dataSize;
        this.oldVec=oldVec;

        //Enable Garbage Collection for unused DoubleVecs
        if (oldVec!=null)
            oldVec.oldVec=null;

        //initialize field of fields
        fieldVec=new double[maxFieldCount][];

        fieldCount=0;

        //initialize first field
        fieldVec[0]=new double[capacityIncrement*dataSize];
        size=0;
        cPos=0;

        zeroVec=new double[dataSize];
    }

    /**
     * combines two entrys such that their first position becomes zero
     * @param fieldPos1 number of field of first entry
     * @param pos1	position in field
     * @param fieldPos2 number of field of second entry
     * @param pos2 position in field
     * @throws NumericalException
     *
     */
    public void add(int fieldPos1,int pos1,int fieldPos2, int pos2) throws NumericalException{
        pos1=pos1*(dataSize+1)+1; pos2=pos2*(dataSize+1)+1;			//do not need first position of vector again, since zero
        size++;

        //Check if enough place in this field
        if (cPos==capacityIncrement){
            fieldCount++;
            fieldVec[fieldCount]=new double[capacityIncrement*dataSize];
            cPos=0;
        }

        //transfer data
        double [] vec1=oldVec.fieldVec[fieldPos1],vec2=oldVec.fieldVec[fieldPos2];
        double [] target=fieldVec[fieldCount];
        int tPos=cPos*dataSize;

        //retrieve factors such that first entrys add to zero
        //double kgV=MathUtils.kgV(Math.abs(vec1[pos1-1]),Math.abs(vec2[pos2-1]));
        //double fac1=kgV/vec1[pos1-1];
        //double fac2=-kgV/vec2[pos2-1];
        double fac1=Math.abs(vec2[pos2-1]);
        double fac2=Math.abs(vec1[pos1-1]);

        if (vec1[pos1-1]*fac1+vec2[pos2-1]*fac2!=0) throw new NumericalException(NumericalException.NUMERICAL_PROBLEMS);

        for (int i=0;i<dataSize;i++){
            if ((vec1[pos1]!=0)||(vec2[pos2]!=0)){
            	target[tPos]=vec1[pos1]*fac1+vec2[pos2]*fac2;

                //To avoid numerical problems
                if (Math.abs(target[tPos])<MathUtils.eps)
                    target[tPos]=0;
            }
            pos1++; pos2++; tPos++;
        }

        //Size increased
        cPos++;
    }

    /**
     * Finds all non-zero doubles at first position and returns them in either positive or negative, if first position is zero,
     * entry is directly transfered to new DoubleVec
     * @param positive value (return value)
     * @param negative value (returned value)
     * @param newFloatVec new DoubleVec
     * @return number of positive and negative entrys
     */
    public int[] getNonzeroValues(int [][] positives, int [][] negatives,DoubleVec newFloatVec, IntVec newIntVec){
        int posCount=-1;
        int negCount=-1;

        double [] vec;
        int maxPos=capacityIncrement;

        for (int i=0;i<=fieldCount;i++){
            vec= fieldVec[i];

            //just go till last entry in last field
            if (i==fieldCount) maxPos=cPos;

            //Check just entrys that have not been deleted by condition seven
            for (int j=0;j<maxPos;j++)if (!skipEntry[i][j]){
                //Compute real position in dataset
                int jPos=j*dataSize;
                //negative value at first position
                if (vec[jPos]<0){
                    negCount++;
                    negatives[negCount][0]=i;negatives[negCount][1]=j;
                }

                //zero value -> directly transfer to new DoubleVec
                if (vec[jPos]==0){
                    newFloatVec.directAdd(i,j);
                    newIntVec.directAdd(i,j);
                }

                //positive value
                if (vec[jPos]>0){
                    posCount++;
                    positives[posCount][0]=i; positives[posCount][1]=j;
                }
            }
        }

        int []res={posCount,negCount};

        return res;
    }

    /**
     *
     * @return Vector with entrys indicating the number of combination candidates for each row
     */
    public int[] countCombinationCandidates(){
        int [] pos=new int[dataSize]; //positive indices
        int [] neg=new int[dataSize]; //negative indices

        double [] vec;
        int maxPos=capacityIncrement;

        for (int i=0;i<=fieldCount;i++){
            vec= fieldVec[i];

            //just go till last entry in last field
            if (i==fieldCount) maxPos=cPos;

            //Check just entrys that have not been deleted by condition seven
            for (int j=0;j<maxPos;j++)if (!skipEntry[i][j]){
                for (int k=0;k<dataSize;k++){
                    if (fieldVec[i][j*dataSize+k] > MathUtils.eps) pos[k]++;
                    if (fieldVec[i][j*dataSize+k] < -MathUtils.eps) neg[k]++;
                }
            }
        }

        int[] combCount=new int[dataSize];
        for (int k=0;k<dataSize;k++)
            combCount[k]=pos[k]*neg[k];

        return combCount;
    }

    public int getDifToBestCombination(){
        Vector<Integer> intVec=new Vector<Integer>();

        int[] combCount=countCombinationCandidates();
        int next=combCount[0];

        for (int i=0;i<dataSize;i++)
            intVec.add(combCount[i]);

        Collections.sort(intVec);

        return next-intVec.get(0);
    }

    public void switchToBest(){
        int[] combCount=countCombinationCandidates();
        int ind1=0;

        //Find best candidate
        int ind2=0;
        for (int i=0;i<dataSize;i++)if (combCount[i]<combCount[ind2]) ind2=i;

        //Switch corresponding entrys
        double [] vec;
        int maxPos=capacityIncrement;
        double temp=0.0;

        //Switch both positions in all entrys
        for (int i=0;i<=fieldCount;i++){
            vec= fieldVec[i];

            //just go till last entry in last field
            if (i==fieldCount) maxPos=cPos;

            //Check just entrys that have not been deleted by condition seven
            for (int j=0;j<maxPos;j++)if (!skipEntry[i][j]){
                temp = vec[j*dataSize+ind1];
                vec[j*dataSize+ind1]=vec[j*dataSize+ind2];
                vec[j*dataSize+ind2]=temp;
            }
        }
    }

    /**
     * avoids usage of a specific entry
     * @param pos index of entry
     */
    public void skipRow(int pos){
    	int fieldPos=pos/capacityIncrement;
    	pos= pos%capacityIncrement;
    	skipEntry[fieldPos][pos]=true;
    }

    /**
     * transfers an entry directly from old DoubleVec into data field
     * @param fieldPos Number of field of entry
     * @param pos position in field
     */
    public void directAdd(int fieldPos,int pos){
        size++;
        pos=pos*(dataSize+1)+1;

        //Check if enough place in this field
        if (cPos==capacityIncrement){
            fieldCount++;
            fieldVec[fieldCount]=new double[capacityIncrement*dataSize];
            cPos=0;
        }

        //transfer entry
        System.arraycopy(oldVec.fieldVec[fieldPos],pos,fieldVec[fieldCount],cPos*dataSize,dataSize);

        // size increased
        cPos++;
    }

    public void flat(){
        double [] vec;
        int maxPos=capacityIncrement;

        for (int i=0;i<=fieldCount;i++){
            vec= fieldVec[i];

            //just go till last entry in last field
            if (i==fieldCount) maxPos=cPos;

            //Check just entrys that have not been deleted by condition seven
            for (int j=0;j<maxPos;j++)if (!skipEntry[i][j]){
                for (int k=0;k<dataSize;k++){
                    if (Math.abs(vec[j*dataSize+k]) < MathUtils.eps) vec[j*dataSize+k]=0;
                }
            }
        }
    }

    /**
     * Saves an entry directly into the data field
     * @param vec entry
     */
    public void insert(double [] vec){
        size++;

        //Check if enough place in this field
        if (cPos==capacityIncrement){
            fieldCount++;
            fieldVec[fieldCount]=new double[capacityIncrement*dataSize];
            cPos=0;
        }

        //transfer entry
        System.arraycopy(vec,0,fieldVec[fieldCount],cPos*dataSize,dataSize);

        // size increased
        cPos++;
    }


}
