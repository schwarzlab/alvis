/*
 * Copyright (C) 2005  Christoph Kaleta
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.binf.ema;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

/**
 * @author Christoph Kaleta
 *
 * Some utilities for Vector-Computation (especially for EMA)
 */
public class MathUtils {

    /**
     * Maximal numerical deviation for correct result
     */
    public static final double eps=1e-6;

	public static int [] pows;	//list of powers of 2 for bit-Coding


    /**
     * Static constructor, initializes some Data-Structures and lists
	 */
    static{
        pows=new int[32]; pows[0]=1;
        for (int i=1;i<32;i++){
            pows[i]=2*pows[i-1];
        }
    }

    /**
	 * Prints a matrix on the screen
	 * @param matrix Matrix to print
	 */
	public static void printMatrix(double [][]matrix){
		for (int i=0;i<matrix.length;i++)if(matrix[i]!=null){
			for (int j=0;j<matrix[i].length;j++){
				if (matrix[i][j]>=0) System.out.print("  ");
				else System.out.print(" ");
				System.out.print(matrix[i][j]);

			}
			System.out.println();
		}
		System.out.println();
	}

	/**
	 * Write a matrix to a file
	 * @param fname Name of the file
	 * @param matrix Matrix to write
	 */
	public static void matrixToFile(String fname,double[][] matrix){
	    BufferedWriter bw;
        try {
            bw = new BufferedWriter(new FileWriter(new File(fname)));
            for (int i=0;i<matrix.length;i++){
    			for (int j=0;j<matrix[i].length;j++){
    				if (j!=0) bw.write(" ");
    				bw.write(Double.toString(matrix[i][j]));
    			}
    			bw.newLine();
    		}
    		bw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

	}

	/**
	 * Gauss-Algorithmus zur L�sung linearer Gleichungssysteme
	 * (Testversion des Algorithmus, arbeitet vorerst nur mit Gleichungssystem die nur
	 * eine L�sung haben)
	 *
	 * @param mat Matrix mit dem kompletten Table des Gausverfahrens (M|b)
	 * @return L�sung (x) des linearen Gleichungssystems M*x=b
	 * @throws NumericalException
	 *
	 */
	public static double[] solveGauss(double[][] mat)throws NumericalException{
		//Erzeuge als erstes obere Dreiecksform
		if (mat[0].length<2)
            throw new NumericalException(NumericalException.NUMERICAL_PROBLEMS);

		double[][] newMat=new double [mat[0].length-1][];
		for (int i=0;i<mat.length;i++){
			double [] vec=mat[i];
			int pos=0;

			// Suche nach Eintrag in vec der ungleich null ist gleichzeitig in newMat noch nicht besetzt ist
			// Falls vec[pos] ungleich null ist und gleichzeitg schon ein Eintrag in newMat existiert wird
			// vec so mit dem korrespondierenden Eintrag kombiniert, dass vec[pos]=0

			//TODO Suche erst den maximalen Eintrag f�r diese Spalte um numerische Stabibilit�t zu garantieren
			while(pos<(mat[0].length-1)&&!( (vec[pos]!=0) && (newMat[pos]==null))){
				//Muss Vektor mit einem anderen kombinieren, da an dieser Position bereits ein
				//Element in der Dreiecksmatrix steht
				if (vec[pos]!=0){
					double [] combVec=newMat[pos];
					double kgV=kgV(Math.abs(vec[pos]),Math.abs(combVec[pos]));
					vec=addVec(vec,combVec,kgV/vec[pos],kgV/(-combVec[pos]));
				}
				pos++;
			}

			//Passende Linearkombination von vec wurde gefunden -> f�ge in Matrix ein
			if (pos<(mat[0].length-1)){
				reduceVec(vec);
				newMat[pos]=vec;
			}
		}

		//�berpr�fe, ob Ausgangsmatrix korrekten Rang hat (d.h. komplette obere Dreiecksform gefunden)
		for (int i=0;i<newMat.length;i++) if (newMat[i]==null)
            throw new NumericalException(NumericalException.NUMERICAL_PROBLEMS);

		//Erzeuge aus oberer Dreiecksform Diagonalenform
		for (int i=newMat.length-1;i>=0;i--)if(newMat[i]!=null){
			double [] vec =newMat[i];
			for (int j=newMat.length-1;j>i;j--)if (vec[j]!=0){
				double [] combVec=newMat[j];
				double kgV=kgV(Math.abs(vec[j]),Math.abs(combVec[j]));
				vec=addVec(vec,combVec,kgV/vec[j],kgV/(-combVec[j]));
				reduceVec(vec);
			}
		}

		//In der letzten Zeile von newMat steht die L�sung des Gleichungssystems
		double [] res=new double[newMat.length];
		for (int i=0;i<newMat.length;i++) res[i]=newMat[i][mat[0].length-1]/newMat[i][i];

		int minPos=0;

		//while ((pos<res.length)&&res[pos]==0) pos++;
		for (int i=0;i<res.length;i++) if (res[i]!=0 && res[i]<res[minPos]) minPos=i;

		double fac=1/res[minPos];
		//Versuche alle Eintr�ge mit 1 zu normalisieren

		for (int i=0;i<res.length;i++)if (res[i]!=0) res[i]*=fac;

		//Ein bisschen gl�tten...
		roundVec(res);

		//Erzeuge Integer-Artiges Ergebnis
		double [] memVec=new double[res.length];
		System.arraycopy(res,0,memVec,0,res.length);
		makeInteger(res);



		return res;
	}

	/**
	 * Rundet die Flie�komma-Eintr�ge eines Vektors auf die ersten 7 Stellen um numerische Exaktheit zu verbessern
	 * @param vec Vektor der gerundet werden soll
	 */
	public static void roundVec(double[] vec){
        for (int i=0;i<vec.length;i++)
            if (vec[i]!=0)
                vec[i]=((double) Math.round(vec[i]*1e7))/1e7;
	}

	/**
	 * �berpr�ft den Kernel einer Matrix
	 * @param matrix St�chiometrische Matrix
	 * @param kernel Kernel
	 */
	public static void checkKernel(double[][] matrix, double[][] kernel) throws NumericalException{
		for ( int i=0;i<matrix.length;i++){
			for (int j=0;j<kernel[0].length;j++){
				double sum=0.0f;
				for (int k=0;k<kernel.length;k++) sum+= matrix[i][k]*kernel[k][j];
				if (Math.abs(sum)> eps) throw new NumericalException(NumericalException.WRONG_KERNEL);
			}
		}
	}



	/**
	 * F�hrt das eigentliche Gauss'sche Eleminationsverfahren durch
	 * @param matrix Vorbereitete Matrix f�r das Verfahren
	 * @param rows Zahl der Reihen, die bearbeitet werden sollen
	 * @return Position ab der der Nullspace beginnt
	 */
	public static int gaussianElimination(double[][] matrix, int rows)throws NumericalException{
		//Wird erh�ht, wenn ein Metabolit gar nicht verwendet wird
		int correc=0;

		//F�hre Verfahren f�r <rows> Spalten durch
		for (int i=0;i<rows;i++){
			//Suche nach ersten Eintrag
			int pos=i-correc;

            //Ist die Matrix von vollem Rang und gibt keine L�sung?
            if (pos>=matrix.length)
                throw new NumericalException(NumericalException.FULL_RANK_MATRIX);

			//int inPos=0;
			boolean foundPos=(Math.abs(matrix[pos][i])>eps);
			for (int j=i-correc;j<matrix.length;j++) if (Math.abs(matrix[j][i])>Math.abs(matrix[pos][i]) &&
                    Math.abs(matrix[j][i])>eps){
				pos=j;
				foundPos=true;
			}

			//while (pos<matrix.length&&matrix[pos][i]==0) pos++;

			//Setze Eintrag an spezifische Position in der Matrix und rechne mal -1, wenn
			//der betreffende Eintrag negativ ist
			if (foundPos){//(pos<matrix.length){
                //if (matrix[pos][i]<0) matrix[pos]=multVec(matrix[pos],-1);

                //Normalize
                matrix[pos]=multVec(matrix[pos],1.0/matrix[pos][i]);

                //Setze Vektor an i-te Position in der Matrix
				permRows(matrix,pos,i-correc);

				//Zeile, mit der jetzt gearbeitet wird
				int mainRow=i-correc;

				//gehe alle Zeilen durch und �berpr�fe, ob Nicht-Nulleintr�ge vorhanden sind
				for (int j=mainRow+1;j<matrix.length;j++)if(Math.abs(matrix[j][i])>eps){
					/*double kgV=kgV(Math.abs(matrix[mainRow][i]),Math.abs(matrix[j][i]));
					matrix[j]=addVec(matrix[j],matrix[mainRow],kgV/matrix[j][i],kgV/(-matrix[mainRow][i]));

					//Reduce Vec if possible
					MathUtils.reduceVec(matrix[j]);*/
                    matrix[j]=addVec(matrix[j],matrix[mainRow],1,-matrix[j][i]);

					//roundVec(matrix[j]);
					if (Math.abs(matrix[j][i])>0.0001)
                        throw new NumericalException(NumericalException.NUMERICAL_PROBLEMS);


				}

			}
			else
                correc++;

		}

		for (int i=rows-correc;i<matrix.length;i++)
			for (int j=0;j<rows;j++) if (Math.abs(matrix[i][j])>0.0001)
				throw new NumericalException(NumericalException.NUMERICAL_PROBLEMS);

		//Reduce entrys
		//for (int i=rows-correc;i<matrix.length;i++) reduceVec(matrix[i]);

		return rows-correc;
	}

    /**
     *
     * @param matrix Matrix
     * @return Nullspace of matrix
     * @throws NumericalException
     */
    public static double [][] computeNullspace(double[][] matrix) throws NumericalException{
        //Check if matrix has entrys
        if (matrix==null || matrix.length==0 || matrix[0]==null)
            return new double [][]{{1.0}};

        //Transpose matrix and append identity matrix
        int m = matrix.length;
        int n = matrix[0].length;

        double [][] workMatrix=new double[n][n+m];

        //Transpose
        for (int i=0;i<m;i++)
            for (int j=0;j<n;j++)
                workMatrix[j][i]=matrix[i][j];

        //ID
        for (int i=0;i<n;i++)
            workMatrix[i][m+i] = 1.0;

        //Gaussian Elemination
        int resStart=gaussianElimination(workMatrix,m);

        double [][] nullSpace=new double[n][n-resStart];

        //Transpose result-Matrix
        for (int i=resStart;i<n;i++)
            for (int j=m;j<n+m;j++) nullSpace[j-m][i-resStart] = workMatrix[i][j];

        //Check nullspace for numerical consistency
        checkKernel(matrix,nullSpace);

        return nullSpace;
    }

	/**
	 *
	 * @param vec Vektor
	 * @param fac Faktor
	 * @return vec*fac
	 */
	public static double [] multVec(double[] vec,double fac){
		for (int i=0;i<vec.length;i++) vec[i]*=fac;
		return vec;
	}

	/**
	 *
	 * @param vec1 Vektor
	 * @param vec2 Vektor
	 * @param fac1 Faktor 1
	 * @param fac2 Faktor 2
	 * @return vec1*fac1+fac*vec2
	 */
	public static double[] addVec(double[] vec1, double[] vec2,double fac1,double fac2){
		for (int i=0;i<vec1.length;i++){
			vec1[i]=vec1[i]*fac1+vec2[i]*fac2;
		}
		return vec1;
	}

	public static double kgV(double num1, double num2) throws NumericalException{
		return num1*num2/ggT(num1,num2);
	}

	public static double ggT(double num1, double num2) throws NumericalException{
		if (num1<0 || num2 < 0) throw new NumericalException(NumericalException.NEGATIVE_GGT);
		double ur1=num1,ur2=num2;
		if (Math.round(num1)-num1!=0 ||Math.round(num2)-num2!=0 )
			System.err.println("Can not compute gcd of floating point values!");
		if (num1==num2) return num1;
		while ((num1!=num2)&& num1!=0 && num2!=0){
			if (num1>num2) num1-=num2;
			else num2-=num1;
		}
		return (num1!=0)? num1:num2;
	}

	/**
	 * Reduces a Vector to its minimal Integer-Entrys (|720.0 360.0| becomes |2.0 1.0|)
	 * @param vec Vector
	 * @throws NumericalException
	 */
	public static void reduceVec(double [] vec) throws NumericalException{
		double ggT=0;
		for (int i=0;i<vec.length;i++) if(vec[i]!=0){
			if (ggT==0) ggT=Math.abs(vec[i]);
			else
				ggT=ggT(ggT,Math.abs(vec[i]));
		}

		for (int i=0;i<vec.length;i++) vec[i]/=ggT;
	}

	/**
	 * Multiplies a Vector as long with 10 as it still contains entrys behind the comma
	 * @param vec Vector
	 * @throws NumericalException
	 */
	public static void makeInteger(double [] vec) throws NumericalException{
		boolean isDouble=true;
		while (isDouble){
			isDouble=false;
			for (int i=0;i<vec.length;i++) if (Math.round(vec[i])-vec[i]!=0) isDouble=true;

			if (isDouble)
				for (int i=0;i<vec.length;i++) vec[i]*=10;
		}

		//Simplify
		reduceVec(vec);
	}

	/**
	 * Vertauscht 2 Zeilen einer Matrix
	 * @param matrix Matrix
	 * @param pos1 auszutauschende Positionen
	 * @param pos2 auszutauschende Positionen
	 */
	public static void permRows(double [][] matrix, int pos1,int pos2){
		double [] temp=matrix[pos1];
		matrix[pos1]=matrix[pos2];
		matrix[pos2]=temp;
	}

	/**
	 *
	 * @param vec Vektor, der in ein Bitmap umgewandelt werden soll
	 * @param start Startpunkt, ab demdi Umwandlung beginnen soll
	 * @return Bit-Vektor von vec ab start (Einsen korrespondiern zu positiven Eintr�gen)
	 */
	public static int[] makeBits(double [] vec, int start){
		if (pows==null) initialize();

		int l=vec.length/32+1;
		int [] res=new int[l];

		for (int i=start;i<vec.length;i++)if(vec[i]>0){
			int intNum=i/32;
        	int intPos=31-i%32;
        	res[intNum]|=pows[intPos];
		}

		return res;
	}

	/**
	 *
	 * @param vec Integer-Vector
	 * @return Number of Bit-Entries in this vec
	 */
	public static int countBits(int [] vec){
		int res=0;

		for (int k=0;k<vec.length;k++)
			for (int i=0;i<32;i++){
				if ((vec[k]&pows[i])!=0) res++;
			}

		return res;
	}

	/**
	 *
	 * @param vec Vector
	 * @return if one position in this vector is true
	 */
	public static boolean hasEntry(boolean [] vec){
		int pos=0;
		while (pos<vec.length&&!vec[pos]) pos++;
		return (pos<vec.length);
	}

    /**
     *
     * @param vec Vector to check
     * @return Original vector with all entrys below eps set to zero
     */
    public static double[] checkNumerics(double[] vec){
        for (int i=0;i<vec.length;i++)if(Math.abs(vec[i])<eps){
            vec[i]=0;
        }

        return vec;

    }

	/**
	 * Initializes some Data-Structures and lists
	 *
	 */
	public static void initialize(){
        pows=new int[32]; pows[0]=1;
        for (int i=1;i<32;i++){
            pows[i]=2*pows[i-1];
        }
	}

    /**
     * Determines the rank of a matrix by computing its nullspace
     * @param matrix Matrix
     * @return Rank of matrix
     * @throws NumericalException
     */
    public static int rank(double[][] matrix){
        try{
            return matrix[0].length-computeNullspace(matrix)[0].length;
        }catch(NumericalException e){
            //Is mostly thrown, when matrix is of full rank
            return Math.min(matrix.length,matrix[0].length);
        }
    }

    /**
     * Removes rows in a matrix, such that it is of full rank
     * @param matrix input matrix
     * @return output matrix
     */
    public static double[][] reduce(double[][] matrix){
        if (matrix.length==0)
            return matrix;

        //Copy old matrix to avoid side-effects
        double[][] temp=new double[matrix.length][matrix[0].length];
        for (int i=0;i<matrix.length;i++) System.arraycopy(matrix[i],0,temp[i],0,matrix[0].length);
        matrix=temp;

        //Compute rank
        int rank=rank(matrix);

        //Determine rows
        int rows=matrix.length;

        for (int i=rank;i<rows;i++){
            //Assuming that more complex rows are at the ending
            boolean foundLesser=false;  //Found matrix with a row less and same rank
            int pos=matrix.length-1;

            while (pos>=0 && !foundLesser){
                //create new matrix without row pos
                double [][] tempMatrix=new double[matrix.length-1][];

                int entryPos=0;
                for (int j=0;j<matrix.length;j++)if(j!=pos){
                    tempMatrix[entryPos]=matrix[j];
                    entryPos++;
                }

                //Compare ranks
                if (rank==rank(tempMatrix)){
                    foundLesser=true;
                    matrix=tempMatrix;
                }
                pos--;
            }
        }

        return matrix;
    }

    public static void main (String[] args){
        double[][] matrix = {{1,0,0,1,1,0,0},{0,1,0,0,0,1,0},{0,0,1,0,0,0,1}};
        try {
            MathUtils.gaussianElimination(matrix,4);
        } catch (NumericalException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}


class GaussException extends Exception{

}
