/*
 * Copyright (C) 2005  Christoph Kaleta
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.binf.ema;

/**
 * Tableau for fast saving and comparison of binary vectors
 * @author Christoph Kaleta
 *
 */
public class IntVec {
    int capacityIncrement;
    int dataSize;
    int[][] fieldVec;		//field of fields where data is stored
    int size;
    IntVec oldVec;
    int fieldCount;
    int cPos;				//position in current field
    int metaCount;

    int maxFieldCount=1000;	//max. number of fields
    int skippedVecs;			//number of entrys that where rejected by S-condition

    boolean [][] skipEntry;		// number of entrys rejected by Condition 7


    /**
     *
     * @param capacityIncrement Number of entrys per field
     * @param dataSize length of an entry
     * @param oldVec previous IntVec
     * @param metaCount maximum number of entrys for an EM (needed for test for elementarity, |metabolites| + 1 )
     */
    public IntVec(int capacityIncrement, int dataSize,IntVec oldVec,int metaCount){

        this.capacityIncrement=capacityIncrement;
        this.dataSize=dataSize;
        this.oldVec=oldVec;

        //delete old oldVec to allow Garbage Collection
        if (oldVec!=null)
            oldVec.oldVec=null;

        //create large field of fields to avoid overrun
        fieldVec=new int[maxFieldCount][];

        fieldCount=0;

        //initialize first field
        fieldVec[0]=new int[capacityIncrement*dataSize];
        size=0;
        cPos=0;
        this.metaCount=metaCount;

        if (MathUtils.pows==null) MathUtils.initialize();
        skippedVecs=0;
    }

    /**
     * Transfers an entry directly from oldVec
     * @param fieldPos number of entry
     * @param pos position in field
     */
    public void directAdd(int fieldPos,int pos){
        size++;

        //Check if enough place in this field
        if (cPos==capacityIncrement){
            fieldCount++;
            fieldVec[fieldCount]=new int[capacityIncrement*dataSize];
            cPos=0;
        }

        //Transfer data
        System.arraycopy(oldVec.fieldVec[fieldPos],pos*dataSize,fieldVec[fieldCount],cPos*dataSize,dataSize);

        // size increased
        cPos++;
    }

    /**
     * Inserts a entry directly into the field
     * @param vec entry to insert
     */
    public void insert(int [] vec){
        size++;

        //Check if enough place in this field
        if (cPos==capacityIncrement){
            fieldCount++;
            fieldVec[fieldCount]=new int[capacityIncrement*dataSize];
            cPos=0;
        }

        //Transfer data
        System.arraycopy(vec,0,fieldVec[fieldCount],cPos*dataSize,dataSize);

        //size increased
        cPos++;
    }

    /**
     * Unites to entrys from oldVec and checks if union fullfills condition for elementary
     * mode
     * @param fieldPos1 number of the field of first entry
     * @param pos1	position in field
     * @param fieldPos2 number of the field of second entry
     * @param pos2 position in field
     * @return If condition for elementary modes is fullfilled
     */
    public boolean combineVecs(int fieldPos1,int pos1,int fieldPos2, int pos2){
        int [] vec=new int[dataSize];
        pos1=pos1*dataSize; pos2=pos2*dataSize;
        int [] vec1=oldVec.fieldVec[fieldPos1],vec2=oldVec.fieldVec[fieldPos2];
        for (int i=0;i<dataSize;i++){

            if ((vec1[pos1]!=0)||(vec2[pos2]!=0)){
               vec[i]= vec1[pos1]|vec2[pos2];
            }
            pos1++;
            pos2++;
        }

        //Check newly created entry

        //Check if number of positive bits in entry not to large
        int rCount=MathUtils.countBits(vec);

        if (rCount>metaCount+1)  {
        	skippedVecs++;
        	return false;
        }
        else return this.addIfUnique(vec);

    }



    /**
     * Inserts a new entry if it fullfills the condition for an elementary mode
     * @param vec new entry
     * @return if entry has been added and therefore represents an elementary mode
     */
    public boolean addIfUnique(int [] vec){
    	int maxPos=capacityIncrement*dataSize;
    	boolean unique =true;
        int [] tvec;
        for (int i=0;(i<=fieldCount)&&unique;i++){
            tvec= fieldVec[i];

            //check max position in last field
            if (i==fieldCount) maxPos=cPos*dataSize;

            int j=0,k=0,num;

            while (unique&&(j<maxPos)){
            	if (k==dataSize) unique=false;
            	else{
            		num=vec[k];
            		if (!((tvec[j]|num)==num)){
            			j+=dataSize-k-1;
            			k=-1;
            		}
            	}

            	j++;k++;
            }
        }

        if (unique){
            insert(vec);
            return true;
        }
        else
            return false;
    }

    /**
     * Codes a boolean vector into an integer-vector and adds it, if it fullfills the elementary mode condition
     * @param vec boolean vector
     * @return if vector has been added
     */
    public boolean addIfUnique(boolean [] vec){
    	int [] binVec=new int[dataSize];
    	for (int i=0;i<vec.length;i++)if(vec[i]){
    		int intNum=i/32;
        	int intPos=31-i%32;
        	binVec[intNum]|=MathUtils.pows[intPos];
    	}
    	return this.addIfUnique(binVec);
    }

    /**
     * Generates a identity matrix and adds it into the data structure
     * @param entrys Number of entrys of the identity matrix
     */
    public void generateUnityMatrix(int entrys){
        int [] vec=new int[entrys/32+1];vec[0]=MathUtils.pows[31];
        int pos;

        //Generat fields
        for (int i=0;i<entrys; i++){
            //transfer to intVec
            insert(vec);

            pos=i/32;
            if (i%32==31){
                vec[pos]=0;
                vec[pos+1]=MathUtils.pows[31];
            }
            else vec[pos]>>>=1;

        }
    }

    /**
     *
     * @param m Number of reactions
     * @return complete set of saved integer-vectors decoded into boolean vectors
     */
    public boolean [][] createMatrix(int m){
        boolean [][] res=new boolean[m][size];

        int [] vec;
        int maxPos=capacityIncrement;

        for (int i=0;i<=fieldCount;i++){
            vec= fieldVec[i];

            //just go till last entry in last field
            if (i==fieldCount) maxPos=cPos;

            //Just take entrys that should not be deleted
            for (int j=0;j<maxPos*dataSize;j+=dataSize)if (!skipEntry[i][j/dataSize]){

                int vPos=capacityIncrement*i+j/dataSize;
              	int maxIntPos=32;
               	int resInt;

               	//decode into boolean vector
               	for (int l=0;l<dataSize;l++){
                   	int hPos=l*32;
                   	resInt=fieldVec[i][j+l];

                   	if (resInt!= 0){
                       	if (l%dataSize==dataSize-1) maxIntPos=m%32;
                       	for (int k=0;k<maxIntPos;k++){
                           	res[hPos+k][vPos]=((resInt&MathUtils.pows[31-k])!=0);
                       	}
                   	}
               	}
            }
        }

        return res;
    }

	/**
	 * Test for condition seven
	 * @param performCleaning if false just returns a boolean vector with all entrys false and performs no test for condition seven
	 * @return Boolean vector with true if entry is not an elementary mode due to condition seven
	 */
	public boolean[][] cleanVec(boolean performCleaning){
	    //result matrix
	    boolean [][]res=new boolean[fieldCount+1][capacityIncrement];

	    //Check all entrys
	    int[] tVec;
	    int killCount=0;
	    int maxPos=capacityIncrement*dataSize;
	    for (int i=0;performCleaning & (i<=fieldCount);i++){
            tVec= fieldVec[i];
            int [] compVec;

            //just go till last entry in last field
            if (i==fieldCount) maxPos=cPos*dataSize;

            for (int j=0;j<maxPos;j+=dataSize){
                //compare entry with all other
                int maxPos2=capacityIncrement*dataSize;
                boolean unique=true;
                for (int i2=i;(i2<=fieldCount)&&unique;i2++){

                    compVec=fieldVec[i2];

                    if (i2==fieldCount) maxPos2=cPos*dataSize;

                    for (int j2=(i==i2? j+dataSize:0);(j2<maxPos2)&&unique;j2+=dataSize){
                        boolean isEqual=true;
                        for (int k=0;(k<dataSize)&&isEqual;k++){
                            isEqual= ((tVec[j+k]|compVec[j2+k])==tVec[j+k]);
                        }
                        unique=!isEqual;
                    }
                }

                //vector is now EM
                if (!unique) {
                    res[i][j/dataSize]=true;
                    killCount++;
                }
            }
        }
	    skippedVecs=killCount;
	    //System.out.println(killCount);
	    this.skipEntry=res;
	    return res;
	}

	/**
     * Sets a reaction/bit in a specific entry to true
     * @param reaction Number of reaction
     * @param pos index of entry
     */
    public void setReaction(int reaction,int pos){
    	int fieldPos=pos/capacityIncrement;
    	pos = (pos%capacityIncrement)*dataSize;
    	int intNum=reaction/32;
    	int intPos=31-reaction%32;
    	fieldVec[fieldPos][pos+intNum]|=MathUtils.pows[intPos];
    }

    /**
     *
     * @param reaction index of reaction
     * @return if reaction/bit is set in any entry
     */
    public boolean reactionExists(int reaction){
		boolean res=false;
		int intNum=reaction/32;
		int intPos=31-reaction%32;

		int maxPos=capacityIncrement*dataSize;
	    for (int i=0;(i<=fieldCount)&&!res;i++){
            int[] tVec= fieldVec[i];

            //just go till last entry in last field
            if (i==fieldCount) maxPos=cPos*dataSize;

            for (int j=0;(j<maxPos)&&!res;j+=dataSize)if((tVec[j+intNum]&MathUtils.pows[intPos])!=0){
            	res=true;
            }
	    }
		return res;
	}

    /**
     * Converts an integer vector to an boolean vector of specified length
     * @param vec integer vector
     * @return boolean-vector of the integer vector
     */
    public boolean [] toBoolVec(int[] vec, int length){
        boolean [] res=new boolean[length];

        //Umwandeln in Boolean-Array
        for (int i=0;i<vec.length;i++){
            for (int j=0;j<32;j++){
                int intPos=31-j%32;
                if ((i*32+j<length) && (vec[i]&MathUtils.pows[intPos]) !=0 ) res[32*i+j]=true;
            }
        }

        return res;
    }

    /**
     *
     * @param pos index of entry
     * @return If entry has been deleted due to condition seven
     */
    public boolean entryUsed(int pos){
        int fieldPos=pos/capacityIncrement;
    	pos = pos%capacityIncrement;

    	return (!skipEntry[fieldPos][pos]);
    }
}
