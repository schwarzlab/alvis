/*
 * Copyright (C) 2005  Christoph Kaleta
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.binf.ema;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Vector;



/**
 * <b>EMComputer Version 1.0.3</b><br><br>
 *
 * Computes elemenentary modes out of stoichiometric matrix, the list reversible reactions and the external metabolites
 * <br><br>
 * Algorith,:<br><br>
 * binary Schuster-algorithm, binary nullspace approach has also been implemented, but is not yet stable.
 * During computations only bit-pattern of EMs are used. Fluxes are being computed in a post-processing step
 * <br><br>
 * Handling of reversible reactions:<br><br>
 * During pre-processing split into forward and backward direction and reconfigured in post-processing step
 * @author Christoph Kaleta
 */
public class EMComputer {

    //DEBUGGING
    final boolean showDetails=false;        //shows details about computation

	boolean switchIt=false;
    int combinations;

    protected double [][] ems;			   //elementary modes
    protected double [][] cb;				//convex base

    protected double [][] matrix;           //stoichiometric matrix
    protected int n,m,metaCount,compLength; //m= number of reactions, n= number oof metabolites

    protected boolean [][] emBits;			//boolean representation of elementary modes

    protected int entrysPerField=512;		//number of entrys per field in data structures
    protected boolean [] reversible;			//reversible reactions
    protected int[] revReactionNums;			//indices of back directions of reversible reactions in expanded stoichiometric matrix
    protected int revCount;					//number of reversible reactions

    protected int [] perm;					//permutations of reactions needed for binary nullspace approach
    protected boolean stop;


	/**
	 *
	 * @param matrix Stoichiometric matrix
	 * @param externalMetabolites list of external metabolites
     *
	 * @throws NumericalException If numerical problems appeared
	 *
	 */
	public EMComputer(double[][] matrix,boolean [] reversible, boolean [] externalMetabolites) throws NumericalException{
		//Remove external metabolites from matrix
        Vector matrixVec=new Vector();
        for (int i=0;i<matrix.length;i++) if (!externalMetabolites[i]) matrixVec.add(matrix[i]);
        matrix=new double[matrixVec.size()][];
        for (int i=0;i<matrixVec.size();i++) matrix[i]=(double[]) matrixVec.get(i);

        //dimensions of matrix changed
		n=matrix.length;
		m=matrix[0].length;
		this.metaCount=n;
		compLength=n+m;
		this.reversible=reversible;
        this.switchIt=switchIt;

		//Initialize MathUtils
		MathUtils.initialize();

		this.matrix=matrix;

		//Uncomment binary approach for use of binary nullspace approach, elsewhise binary Schuster algorithm is used
        //binaryApproach(matrix,reversible);
        schusterApproach(matrix,reversible);
	}

    /**
     * Computes elementary modes with a binary Schuster-Algorithm
     * @param matrix Stoichiometrix matrix
     * @param reversible list of reversible reactions
     * @throws NumericalException
     */
    protected void schusterApproach(double[][] matrix,boolean [] reversible) throws NumericalException{
        long start = System.currentTimeMillis();
        revCount=0;
        for (int i=0;i<m;i++) if (reversible[i]) revCount++;
        matrix=new double[n][m+revCount];
        this.revReactionNums=new int[revCount];
        revCount=0;

        for (int i=0;i<n;i++) System.arraycopy(this.matrix[i],0,matrix[i],0,m);

        for (int i=0;i<m;i++) if (reversible[i]){
            this.revReactionNums[revCount]=i;
            for (int j=0;j<n;j++) matrix[j][m+revCount]=-1*matrix[j][i];

            revCount++;
        }
        this.matrix=matrix;
        //dimension changed
        n=this.matrix.length;
        m=this.matrix[0].length;
        compLength=n+m;

        // sort rows according to number of entrys to speed up computations
        sortMatrix(this.matrix);

        System.out.println("Computing elementary modes");
        this.computeEMsBySchuster();

        //Abort computation
        if (stop) return;

        if (showDetails)
            System.out.println("Computation of EMs took "+(System.currentTimeMillis()-start)+" ms.");

        ems=new double[emBits[0].length][];

        System.out.println("Performing back-transformation");

        //Compute flux for each bit pattern
        for (int i=0;i<this.emBits[0].length;i++){
            boolean[] em=new boolean[m];
            for (int j=0;j<m;j++) em[j]=this.emBits[j][i];

            //Check if EM uses a reaction
            int pos=0;
            while (pos<m&&!em[pos]) pos++;

            if (pos<m){
                double [] v=null;
                try {
                    v = this.decomposeElementaryMode(em);
                } catch (NumericalException e) {
                    System.out.println("Elementary Mode does not have correct rank, please check your system!");
                }
                ems[i]=v;
            }
        }

        //Rescale stoichiometric matrix
        m-=revCount;

        //check ems
        this.checkModes();

        //transfer list of elementary modes
        int emCount=0;
        for (int i=0;i<ems.length;i++) if (ems[i]!=null) emCount++;

        double[][] tempEMs=new double[emCount][];
        emCount=0;
        for (int i=0;i<ems.length;i++)if (ems[i]!=null){
            tempEMs[emCount]=ems[i];

            /*for (int j=0;j<m;j++)System.out.print(Float.toString(tempEMs[emCount][j])+" ");
            System.out.println();*/
            emCount++;
        }
        System.out.println("Found "+Integer.toString(emCount)+" elementary modes.");
        ems=tempEMs;

        // Compute convex base out of elementary modes
        System.out.println("Computing convex base.");
        this.determineConvexBase();
    }

    /**
     * Compute elementary modes using binary Schuster algorithm
     * @throws NumericalException
     */
    protected void computeEMsBySchuster() throws NumericalException{
        //data structures with bit patterns and stoichiometric matrix
        DoubleVec oldFloatVec,newFloatVec;
        oldFloatVec=new DoubleVec(entrysPerField,n,null);

        IntVec oldIntVec,newIntVec;
        oldIntVec=new IntVec(entrysPerField,m/32+1,null,n+1);

        // Generate initial DoubleVec trough transposition of stoichiometric matrix
        double [] vec;
        for (int j=0;j<m;j++){
            vec=new double[n];
            for (int i=0;i<n;i++){
                vec[i]=matrix[i][j];
            }
            oldFloatVec.insert(vec);
        }

        // initial IntVec is unity matrix
        oldIntVec.generateUnityMatrix(m);

        // Compute elementary modes
        int[][] positives,negatives;
        int res[];                      // number of positive and negative entrys in current collumn
        double startTime=System.currentTimeMillis();        //show information if computation takes to long

        boolean cleanVec = false;       //Apply condition seven (during computation set to true if number of entrys have changed)

        //need an initial check for Condition Seven to create some data structure
        oldFloatVec.skipEntry=oldIntVec.cleanVec(false);
        //for (int i=0;i<m;i++)if(skipReactions[i]) oldFloatVec.skipRow(i);

        for (int i=n;i>0;i--){

            if (showDetails)
                System.out.println("Rows to be computed: "+i+"; Number of entrys: "+oldFloatVec.size);
            //System.out.print(" "+oldFloatVec.size);

            newFloatVec=new DoubleVec(entrysPerField,i-1,oldFloatVec);
            newIntVec=new IntVec(entrysPerField,m/32+1,oldIntVec,n+1);
            positives=new int[oldFloatVec.size][2];
            negatives=new int[oldFloatVec.size][2];

            //determine entrys that need to be combined and those, that can directly be transfered to new tableau
            res=oldFloatVec.getNonzeroValues(positives,negatives,newFloatVec,newIntVec);
            int posCount=res[0];
            int negCount=res[1];

            if (showDetails){
                System.out.println("Positive entrys to combine: "+(posCount+1));
                System.out.println("Negative entrys to combine: "+(negCount+1));
            }
            //combine all entrys with positive first double with all entrys with negative first double
            for (int w=0;w<=posCount;w++){
                //If computation takes longer than 20s show addtional information
                //if (System.currentTimeMillis()-startTime>20000) showDetails=true;

                //Abort computation
                if (stop) return;

                if (showDetails){
                    if ((posCount*negCount>100000)&&(w%100==0)) {
                        System.out.println("Combined positive entrys: "+w+" Number of entrys: "+newFloatVec.size);
                        //newIntVec.cleanVec();
                    }
                }
                for (int z=0;z<=negCount;z++){
                    if (newIntVec.combineVecs(positives[w][0],positives[w][1],negatives[z][0],negatives[z][1]))
                        newFloatVec.add(positives[w][0],positives[w][1],negatives[z][0],negatives[z][1]);
                }
            }

            if ((posCount!=-1)&(negCount!=-1))cleanVec =true;
            else cleanVec=false;

            // new tableau is current tableau in next step
            oldFloatVec=newFloatVec;

            oldIntVec=newIntVec;

            //Check for condition Seven if new entrys have been computed
            if (posCount==-1||negCount==-1)
                oldFloatVec.skipEntry=oldIntVec.cleanVec(false);
            else oldFloatVec.skipEntry=oldIntVec.cleanVec(true);

        }
        //Check for Condition Seven
        oldIntVec.cleanVec(true);

        //retrieve bit patterns of ems
        this.emBits=oldIntVec.createMatrix(m);
    }

    /**
     * Computes elementary modes according to the binary approach of Klamt&Gagneur2005
     * @param matrix Stoichiometrix matrix
     * @param reversible list of reversible reactions
     * @throws NumericalException
     */
    protected void binaryApproach(double[][] matrix,boolean [] reversible) throws NumericalException{
        //Compute kernel
        this.perm=new int[m];

        System.out.println("Computing kernel");
        double [][] kernel=null;

        //Try to compute kernel, if matrix is of full rank the system has no solution
        try{
            kernel=this.computeKernel(this.perm);
        }catch(NumericalException e){
            if (e.type==NumericalException.FULL_RANK_MATRIX){
                System.out.println("The stoichiometric matrix is of full rank.");
                System.out.println("System has no elementary modes!");
                ems=new double[0][0];
                cb=new double[0][0];
                return;
            }
            else throw new NumericalException(e.type);
        }

        //expand matrix by adding back directions of reversible reactions
        revCount=0;
        for (int i=0;i<m;i++) if (reversible[i]) revCount++;
        matrix=new double[n][m+revCount];
        this.revReactionNums=new int[revCount];
        revCount=0;

        int[] backPos=new int[m];       //Remember position of back directions of reversible reactions

        for (int i=0;i<n;i++) System.arraycopy(this.matrix[i],0,matrix[i],0,m);

        for (int i=0;i<m;i++) if (reversible[i]){
            this.revReactionNums[revCount]=i;
            for (int j=0;j<n;j++) matrix[j][m+revCount]=-1*matrix[j][i];

            backPos[i]=m+revCount;
            revCount++;
        }

        this.matrix=matrix;
        //dimensions changed
        n=this.matrix.length;
        m=this.matrix[0].length;
        compLength=n+m;

        System.out.println("Computing elementary modes");
        long start=System.currentTimeMillis();
        this.computeEMsByWagner(kernel);

        if (stop)
            return;

        System.out.println("EM-computation took "+(System.currentTimeMillis()-start)+" ms.");

        ems=new double[emBits[0].length][];

        System.out.println("Performing back-transformation");

        //Compute fluxes of bit patterns
        for (int i=0;i<this.emBits[0].length;i++){
            boolean[] em=new boolean[m];
            //generate EM and re-do permutations through kernel-computation
            for (int j=0;j<m;j++)
                if (perm[j]>=0)
                    em[perm[j]]=this.emBits[j][i];
                else em[backPos[-perm[j]-1]]=this.emBits[j][i];
            double [] v=null;
            if (MathUtils.hasEntry(em)){
                try {
                    v = this.decomposeElementaryMode(em);
                } catch (NumericalException e) {
                System.out.println("Elementary Mode does not have correct rank, please check your system!");
                }
            }
            ems[i]=v;
        }

        //Obtain original size of stoichiometric matrix
        m-=revCount;

        //Check EMs
        this.checkModes();

        //transfer list of EMs
        int emCount=0;
        for (int i=0;i<ems.length;i++) if (ems[i]!=null) emCount++;

        double[][] tempEMs=new double[emCount][];
        emCount=0;
        for (int i=0;i<ems.length;i++)if (ems[i]!=null){
            tempEMs[emCount]=ems[i];

            /*for (int j=0;j<m;j++)System.out.print(Double.toString(tempEMs[emCount][j])+" ");
            System.out.println();*/
            emCount++;
        }
        System.out.println("Found "+Integer.toString(emCount)+" elementary modes.");
        ems=tempEMs;

        // Determine convec base
        this.determineConvexBase();

        // Delete temporary variables
        this.cleanUp();
    }

	/**
	 * Sorts rows of matrix ascending with number of entrys per row
	 * @param matrix Matrix (return value)
	 */
	protected void sortMatrix(double[][] matrix){
	    //Data structure for sorting
	    ArrayList L = new ArrayList();

	    //Insert into structure that counts number of entrys
	    for (int i=0;i<n;i++){
	        L.add(new lineComparator(matrix[i]));
	    }

	    //Sort
	    Collections.sort(L);

	    //transfer sorted list back to matrix
	    this.matrix=new double[L.size()][];
	    for (int i=0;i<L.size();i++) this.matrix[i]=((lineComparator)L.get(i)).flLine;
	}

	/**
	 * Check if reaction is used in any elementary mode
	 *
	 */
	protected void checkNullspace(){
	   for (int i=0;i<emBits[0].length;i++){
	       int j=0;
	       while ((j<emBits.length)&&(!emBits[j][i])) j++;

	       if (j==emBits.length) System.out.println("Reaction "+Integer.toString(i)+" participates in no EM.");
	   }
	}

	/**
	 * Computes the fluxes of an elementary mode out of an bit pattern (since determined by bit pattern)
	 *
	 * @param em EM
	 * @return flux distribution of elementary mode
	 * @throws NumericalException
	 */
	protected double[] decomposeElementaryMode(boolean [] em) throws NumericalException{
		//transfer part of stoichiometric matrix that is used by EM
		int rCount=0;		//number of reactions in EM
		for (int i=0;i<em.length;i++) if (em[i]) rCount++;

		//Matrix to determine flux of EM
		Vector solveVec=new Vector();		//Vector with columns from stoichiometric matrix that are used by EM

		double [] vec;

        for (int i=0;i<matrix.length;i++){
			//Check if non-zero element exists in row
			int pos=0;
			while ( pos<m && !( em[pos] && (matrix[i][pos]!=0) )) pos++;

			if (pos<m){
				vec = new double[rCount];

				int hPos=0;	//horizontal position
				for (int j=0;j<m;j++)if (em[j]){
					vec[hPos]=matrix[i][j];
					hPos++;
				}

				//Add to solveVec
				solveVec.add(vec);
			}

		}
		double[][] solveMatrix=new double[solveVec.size()][];
		for (int i=0;i<solveVec.size();i++) solveMatrix[i]=(double[]) solveVec.get(i);

		//Compute nullspace of solveVec*x=0, since it has only one row
		double [][] solve = MathUtils.computeNullspace(solveMatrix);

		//Expand solution to complete system
		double [] res=new double[m];
		int pos=0;

		for (int i=0;i<m;i++)if (em[i]){
			if (solve==null || solve[pos].length==0) throw new NumericalException(NumericalException.WRONG_EM);
            res[i]=solve[pos][0];
            //res[i]=solve[pos];
			pos++;
		}

		//remove back directions of reversible reactions
		for (int i=m-revCount;i<m;i++) if(res[i]>0){
			int secPos= this.revReactionNums[i-(m-revCount)];
			if (res[secPos]>0)
				return null;
			res[secPos]=-res[i];
		}

        //rescale EM to make it look nice
        double min = Double.MAX_VALUE;

        //deletet numerical inaccuracies
        res=MathUtils.checkNumerics(res);

        //Find minimal entry for a good scaling
        for (int i=0;i<res.length;i++){
            if (Math.abs(res[i])<min && Math.abs(res[i])>MathUtils.eps) min = Math.abs(res[i]);

        }
        if (min!=Double.MAX_VALUE){
            double fac = 1/min;
            for (int i=0;i<res.length;i++){
                res[i]*=fac;
            }
        }
        double[] resEM=new double[m-revCount];
		System.arraycopy(res,0,resEM,0,m-revCount);

        //deletet numerical inaccuracies .. again
        MathUtils.roundVec(resEM);

		return resEM;
	}



	/**
	 * Check if elementary modes fullfils steady state condition and deletes back directions of completely reversible EMs
	 * @throws NumericalException
	 *
	 */
	protected void checkModes() throws NumericalException{

		//Search elementary modes that are completely reversible
		boolean [] revEM = new boolean[ems.length];
		for (int i=0;i<ems.length;i++)if(ems[i]!=null){
			//check if completely reversible
			int pos=0;
			while ((pos<m)&&!(!reversible[pos]&&ems[i][pos]!=0)) pos++;

			//EM completely reversible
			if(pos==m) revEM[i]=true;
		}

		//Search for back direction and delete if existing
		for (int i=0;i<ems.length;i++)if(ems[i]!=null){
			for (int j=0;j<ems.length;j++) if (j!=i&&ems[j]!=null){
				int pos=0;
				while ((pos<m)&&(ems[i][pos]==-1*ems[j][pos])) pos++;
				if (pos==m) ems[j]=null;
			}
		}

		// Check if steady state condition is fullfilled
		for (int i=0;i<ems.length;i++)if(ems[i]!=null) checkEM(ems[i]);
	}

	/**
     * checks if an elementary mode satisfies the steady state condition
     * (and if the computations where numerical accurate)
     * Adjust the value 1e-4 for stricter accuracies
     * @param em Elementary mode to check
     * @throws NumericalException
	 */
    protected void checkEM(double [] em)throws NumericalException{
		for (int i=0;i< matrix.length;i++){
			double sum=0;
			for (int j=0;j<em.length;j++) sum+=em[j]*matrix[i][j];
			if (sum>=1e-4)
				throw new NumericalException(NumericalException.WRONG_EM);
		}
	}

	/**
	 * Determines those EMs that belong to the convex base
	 */
	protected void determineConvexBase(){
		if (ems.length==0){
            cb = new double[ems.length][];
            System.out.println(cb.length+" elementary modes belong to the convex base.");
        }
        else{

            IntVec conVec=new IntVec(entrysPerField,m/32+1,null,n+1);

    		//Check all EMs
    		int notCount=0;

    		//remember indices of EMs that belong to the convex base
    		int[] memIndex=new int[ems.length];

            //Vector for completely reversible reactions
            Vector revVec=new Vector();

    		for (int i=0;i<ems.length;i++){
    			memIndex[i]=-1;
    		    boolean[] vec=new boolean[m];

    			//Need at least one reversible reaction
    			boolean oneTrue=false;
    			for (int j=0;j<m;j++) if (!this.reversible[j]&&(Math.abs(ems[i][j])>MathUtils.eps)){
    				vec[j]=true;
    				oneTrue=true;
    			}

    			if (oneTrue){
    			    if (!conVec.addIfUnique(vec))
    			        notCount++;
    			    else
    			        //remember index of EM in conVec
    			        memIndex[i]=conVec.size-1;
                }
                else {
                    //Completely reverisble EM
                    revVec.add(ems[i]);
                    notCount++;
                }
    		}

    		//Need to bring set of completely reversible EMs to full rank
            double[][] revMat=new double[revVec.size()][m];
            for (int i=0;i<revMat.length;i++)revMat[i]=(double[])revVec.get(i);
            revMat=MathUtils.reduce(revMat);

    		conVec.cleanVec(true);

            //Check which EMs have been deleted by condition seven
    		for (int i=0;i<ems.length;i++)if (memIndex[i]!=-1){
    		    if (!conVec.entryUsed(memIndex[i])){
    		        memIndex[i]=-1;
    		        notCount++;
    		    }
    		}

    		//transfer convec base to own array
    		cb=new double[ems.length-notCount+revMat.length][];
    		int pos=0;

            //Completely reversible EMS
            for (int i=0;i<revMat.length;i++) {
                cb[pos]=revMat[i];
                notCount--;
                pos++;
            }

    		//EMs with irreversible reaction that belong to convex base
            for (int i=0;i<ems.length;i++)if (memIndex[i]!=-1){
    		    cb[pos]=ems[i];
    		    pos++;
    		}

    		System.out.println(Integer.toString(ems.length-notCount)+" elementary modes belong to the convex base.");
        }
	}

	/**
	 * Computes the kernel that is needed by the binary nullspace approach
	 * @param perm list with new indices of reactions after permutations
	 * @return Kernel of the stoichiometric matrix with the following properties<br>
	 * 1. Reversible reactions are at the beginning of the matrix <br>
	 * 2. Identy matrix at the end of the kernel <br>
	 * 3. Reactions that not do belong to identity matrix are ordered according to number of entrys <br>
	 * 4. back directions of reversible reactions have been added
	 * @throws NumericalException
	 */
	protected double [][] computeKernel(int[] perm) throws NumericalException{
		//Generate matrix with transposed stoichiometric matrix
		double [][] matrix=new double[m][n+m];

		int posCount=0;
		int revCount;

		//reversible reactions
		for (int i=0;i<m;i++)if (reversible[i]){
			perm[posCount]=i;
			for (int j=0;j<n;j++)
				matrix[posCount][j]=this.matrix[j][i];
			posCount++;
		}

		//remember number of reversible reactions
		revCount=posCount;

		//irreversible reactions
		for (int i=0;i<m;i++)if (!reversible[i]){
			perm[posCount]=i;
			for (int j=0;j<n;j++)
				matrix[posCount][j]=this.matrix[j][i];
			posCount++;
		}

		//append identity matrix
		for (int i=0;i<m;i++) matrix[i][n+i]=1;

		//Perform gaussian elemination
		int firstPos = MathUtils.gaussianElimination(matrix,n);

		//in the rows beginning with firstPos the Nullspace can be found

		//remember vectors for which we have already an entry in the identity matrix
		boolean[] idMem=new boolean[m];
		int [] memPos=new int[m];
		int memCount=firstPos;

		//remember rows that represent the identity matrix
		boolean [] idMatrix=new boolean[n+m];

		for (int i=m+n-1;i>=n;i--){
			int entryCount=0;
			for (int j=firstPos;j<m;j++) if (matrix[j][i]!=0)entryCount++;

			//Found entry of identity matrix

			if (entryCount==1){
				//Obtain vertical position of entry
				int pos=firstPos;
				while (matrix[pos][i]==0)pos++;

				if (!idMem[pos]) {

					//multiply with -1 if negative
					if (matrix[pos][i]<0) MathUtils.multVec(matrix[pos],-1);

					idMatrix[i]=true;
					idMem[pos]=true;
					//if (reversible[perm[i-n]]) System.err.println("Reversible Reaktion in der Identit�tsmatrix");
					memPos[memCount]=pos;
					memCount++;
				}
			}
		}

		//transfer all rows with entrys to the kernel and transpose

		//Count rows with entrys
		int entryCount=0;
		boolean [] hasEntry=new boolean[m];
		for (int i=firstPos;i<m;i++){
			int pos=n;
			while ((pos<n+m)&&(matrix[i][pos]==0)) pos++;
			if (pos<n+m){
				entryCount++;
				hasEntry[i]=true;
			}
		}

		//Kernel
		double [][] kernel = new double[m][entryCount];
		int[] newPerm = new int[m];

		//Entrys not from the identity matrix come first
		int hPos=0,vPos=0;	//horizontal and vertical position in kernel
		for (int i=0;i<m;i++)if(!idMatrix[i+n]){
			//remember permutations
			newPerm[hPos]=i;
			vPos=0;
			for (int j=firstPos;j<m;j++) if (hasEntry[j]){
				kernel[hPos][vPos]=matrix[j][i+n];
				vPos++;
			}
			hPos++;
		}

		//Entrys from identity matrix
		int idCount=0;
		for (int i=0;i<idMatrix.length;i++) if (idMatrix[i]) idCount++;

		for (int i=0;i<m;i++)if(idMatrix[i+n]){
			//remember permutations
			newPerm[hPos]=i;
			vPos=0;
			for (int j=firstPos;j<m;j++) if (hasEntry[j]){
				kernel[hPos][vPos]=matrix[j][i+n];
				vPos++;
			}
			hPos++;
		}

		//all permutations
		int[] absPerm=new int[m];
		for (int i=0;i<m;i++) absPerm[i]=perm[newPerm[i]];

		//place irreversible reactions first
		Vector kernelVec=new Vector();
		posCount=0;
		int irrevCount=0;
		for (int i=0;i<kernel.length-entryCount;i++) if (!reversible[absPerm[i]]){
			kernelVec.add(kernel[i]);
			perm[kernelVec.size()-1]=absPerm[i];
			irrevCount++;
		}

		//reversible reactions
		for (int i=0;i<kernel.length-entryCount;i++) if (reversible[absPerm[i]]){
			kernelVec.add(kernel[i]);
			perm[kernelVec.size()-1]=absPerm[i];
		}

		//identity matrix
		for (int i=kernel.length-entryCount;i<kernel.length;i++) {
			kernelVec.add(kernel[i]);
			perm[kernelVec.size()-1]=absPerm[i];
		}

		kernel=new double[kernelVec.size()][];
		for (int i=0;i<kernelVec.size();i++) kernel[i]=(double[])kernelVec.get(i);

		//Sort rows of irreversible reactions ascending with number of entrys
	    ArrayList L = new ArrayList();

	    for (int i=0;i<irrevCount;i++){
	        L.add(new lineComparator(kernel[i]));
	    }

	    //Sort
	    Collections.sort(L);

	    //Back into the kernel
	    newPerm  = new int[m];
	    for (int i=0;i<irrevCount;i++){
	    	kernel[i]=((lineComparator) L.get(i)).flLine;

	    	//Remember permutations
	    	newPerm[i]=perm[kernelVec.indexOf(kernel[i])];
	    }
	    for (int i=0;i<irrevCount;i++) perm[i]=newPerm[i];

	    //Check Kernel
	    double [][] temp= new double[n][m];
	    for (int i=0;i<n;i++)
	    	for (int j=0;j<m;j++)
	    		temp[i][j]=this.matrix[i][perm[j]];

	    MathUtils.checkKernel(temp,kernel);

	    // Add entrys for back directions of reversible reactions
		double [][] tempKernel=new double[kernel.length+revCount][kernel[0].length+revCount];
		int idStart=kernel.length-kernel[0].length;
		int tempRevCount=0;

		//Remember permutations
		this.perm=new int[kernel.length+revCount];
		System.arraycopy(perm,0,this.perm,0,kernel.length);

	    for(int i=0;i<kernel.length;i++){
	    	for (int j=0;j<kernel[0].length;j++) tempKernel[i][j]=kernel[i][j];

	    	//Add back directions
	    	if (reversible[perm[i]]){
	    		// 1. Reversible reaction within identity matrix
	    		if (i>=idStart){
	    			//Find position of entry
	    			int pos=0;
	    			while((pos<kernel[0].length) && kernel[i][pos]==0) pos++;

	    			//Add back direction
	    			for (int j=0;j<idStart;j++) tempKernel[j][kernel[0].length+tempRevCount]=-kernel[j][pos];
	    		}
	    		else {
	    			//Add "spurious two-cycle" to make kernel as sparse as possible
	    			tempKernel[i][kernel[0].length+tempRevCount]=1;
	    		}
	    		//entry in identity matrix
	    		tempKernel[kernel.length+tempRevCount][kernel[0].length+tempRevCount]=1;

	    		//remember permutations (back direction of reversible reaction with negative sign)
	    		this.perm[kernel.length+tempRevCount]=-perm[i]-1;

	    		tempRevCount++;
	    	}
	    }

	    //MathUtils.printMatrix(tempKernel);
        //reduce numerical inaccuracies
        for (int i=0;i<tempKernel.length;i++)
            MathUtils.roundVec(tempKernel[i]);

        //MathUtils.printMatrix(tempKernel);

		return tempKernel;
	}



	/**
	 * Computes the elementary modes out of a kernel
	 * @throws NumericalException
	 */
	protected void computeEMsByWagner(double[][] nullSpace) throws NumericalException{
		this.n=nullSpace[0].length;
		this.m=nullSpace.length;

		//Identity matrix can be found in last rows
		IntVec oldIntVec,newIntVec;
		oldIntVec=new IntVec(30000,m/32+1,null,metaCount+1);

		//transfer identity matrix to IntVec
		for (int i=0;i<n;i++){
			double[] vec=new double[m];
			double sum=0;
			for (int j=m-n;j<m;j++){
				vec[j]=nullSpace[j][i];
				sum +=vec[j];
			}

			oldIntVec.insert(MathUtils.makeBits(vec,0));
		}

		//transfer other entrys to DoubleVec
		DoubleVec oldFloatVec,newFloatVec;
	    oldFloatVec=new DoubleVec(30000,m-n,null);
	    for (int i=0;i<n;i++){
	    	double [] vec = new double[m-n];
	    	for (int j=0;j<m-n;j++) vec[j]=nullSpace[j][i];
	    	oldFloatVec.insert(vec);
	    }

	    // Compute elementary modes
	    int[][] positives,negatives;
	    int res[];						// Number of positive/negative entrys at current position

	    // For correct initialization of oldFloatVec
	    oldFloatVec.skipEntry=oldIntVec.cleanVec(false);

        int sum=0;

	    for (int i=0;i<m-n;i++){
            //restructure tableau
            oldFloatVec.flat();

            if (switchIt)
                oldFloatVec.switchToBest();

            //Some info
	    	if (showDetails){
                System.out.println();
		        System.out.println("Performing computations for row "+Integer.toString(i)+"; entrys: "+oldFloatVec.size);
		        System.out.println("Combinations to much: "+oldFloatVec.getDifToBestCombination());
            }

	    	newFloatVec=new DoubleVec(entrysPerField,(m-n)-i-1,oldFloatVec);
		    newIntVec=new IntVec(entrysPerField,m/32+1,oldIntVec,metaCount);
		    positives=new int[oldFloatVec.size][2];
		    negatives=new int[oldFloatVec.size][2];

		    //Find positive and negative entrys, zero entrys are directly transfered
		    res=oldFloatVec.getNonzeroValues(positives,negatives,newFloatVec,newIntVec);
		    int posCount=res[0];
		    int negCount=res[1];
            combinations+=(posCount+1)*(negCount+1);
		    if (showDetails){
		        System.out.println("Positive entrys to combine: "+posCount);
				System.out.println("Negative entrys to combine: "+negCount);
		    }

		    //Remember position till which positive entrys have been added to set reaction later on
		    int positivePos=newIntVec.size;

		    //combine positive and negative entrys

		    for (int w=0;w<=posCount;w++){
                //Abort computation
                if (stop) return;

		    	if (showDetails){
		    	    if ((posCount*negCount>100000)&&(w%100==0)) {
		    	        System.out.println("Combined positive entrys: "+w+"; entrys: "+newFloatVec.size);
		    	        //newIntVec.cleanVec();
		    	    }
		    	}
		    	for (int z=0;z<=negCount;z++){
		    		if (newIntVec.combineVecs(positives[w][0],positives[w][1],negatives[z][0],negatives[z][1]))
		                newFloatVec.add(positives[w][0],positives[w][1],negatives[z][0],negatives[z][1]);
		        }
		    }

		    //transfer fluxes of positive entrys
		    for (int w=0;w<=posCount;w++){
		    	newIntVec.directAdd(positives[w][0],positives[w][1]);
		    	newFloatVec.directAdd(positives[w][0],positives[w][1]);
		    	//newIntVec.setReaction(i,positivePos+w);
		    	newIntVec.setReaction(i,newIntVec.size-1);
		    }

		    // newFloatVec is source f                    if (vec[j*dataSize+k] < -MathUtils.eps) neg[k]++;or transfer in next step
		    oldFloatVec=newFloatVec;

		    oldIntVec=newIntVec;

		    // Condition 7
		    if (posCount!=-1&&negCount!=-1)
		    	oldFloatVec.skipEntry=oldIntVec.cleanVec(true);
		    else oldFloatVec.skipEntry=oldIntVec.cleanVec(true);
		    //System.out.println(oldIntVec.skippedVecs);

	    }
	    oldIntVec.cleanVec(true);

        if (showDetails)
	        System.out.println("Number of computed EMs: "+oldIntVec.size);

        System.out.println("Number of computed EMs: "+(oldIntVec.size-oldIntVec.skippedVecs));
	    //transfer bit patterns to boolean matrix
	    this.emBits=oldIntVec.createMatrix(m);
	}

	/**
	 * Performs some cleaning-Operations and starts the garbage-collector
	 */
	public void cleanUp(){
		this.emBits=null;
		this.matrix=null;
		this.perm=null;
		this.reversible=null;
		this.revReactionNums=null;
		//this.skipReactions=null;
		System.gc();
	}

    /**
     * @return The computed elementary modes
     */
    public double [][] getElementaryModes(){
        return ems;
    }

    /**
     * @return The computed convex base
     */
    public double [][] getConvexBase(){
        return cb;
    }

	public EMComputer(){}

	/**
	 * Writes the results of the EMA-Analysis to a file
	 * @param fname filename
	 */
	public void writeResults(String fname){
		BufferedWriter bw;

		try {
			bw=new BufferedWriter(new FileWriter(fname));
			bw.write("elementary modes"); bw.newLine();
			bw.write("dimensions "+Integer.toString(ems.length)+"x"+Integer.toString(ems[0].length));

			for (int i=0;i<ems.length;i++){
				for (int j=0;j<ems[0].length;j++) bw.write(ems[i][j]+" ");
				bw.newLine();
			}

			bw.newLine();
			bw.write("convex base"); bw.newLine();
			bw.write("dimensions "+Integer.toString(cb.length)+"x"+Integer.toString(cb[0].length));

			for (int i=0;i<cb.length;i++){
				for (int j=0;j<cb[0].length;j++) bw.write(cb[i][j]+" ");
				bw.newLine();
			}
		} catch (IOException e) {
			System.err.println("Could not write result-file!");
		}

	}

    /**
     * Aborts the computation of the elementary modes
     *
     */
    public void abort(){
        this.stop=true;
    }

	public static void main(String[] args) {
	    //MathUtils.initialize();
        /*
        ReaReader reader = new ReaReader("/home/christoph/workspace/SBW/test.rea");

        boolean [] rev=reader.getReversibleList();
        rev[0]=true;
        boolean [] ext=new boolean[reader.getMetaCount()];
        ext[3]=true;
        ext[4]=true;
        ext[5]=true;

        try {

            long start = System.currentTimeMillis();
            EMComputer N=new EMComputer(reader.stoech,rev,ext);
            long firstTime=System.currentTimeMillis()-start;

        } catch (NumericalException e) {
            System.err.println("Problems...");
            e.printStackTrace();
        }
        */
	}

}

class lineComparator implements Comparable{

    int [] line;
    double [] flLine;
    int entryCount;

    /**
     * counts the number of entrys in a vector and enables sorting
     */
    public lineComparator(int[] line){
        this.line=line;
        entryCount=0;
        for (int i=0;i<line.length;i++)if (line[i]!=0){
            entryCount++;
        }
    }

    public lineComparator(double[] line){
        this.flLine=line;
        entryCount=0;
        for (int i=0;i<line.length;i++)if (line[i]!=0){
            entryCount++;
        }
    }

    public int compareTo(Object o) {
        if (((lineComparator)o).entryCount<entryCount)return 1;
        if (((lineComparator)o).entryCount==entryCount)return 0;
        else return -1;
    }

}

