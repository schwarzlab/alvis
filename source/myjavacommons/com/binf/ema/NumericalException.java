/*
 * Copyright (C) 2005  Christoph Kaleta
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.binf.ema;

/**
 *
 *Exceptions for MathUtils and EMComputer
 * @author Christoph Kaleta
 */

public class NumericalException extends Exception{
	public static final int WRONG_KERNEL=0;
	public static final int NUMERICAL_PROBLEMS=1;
	public static final int WRONG_EM=2;
	public static final int NEGATIVE_GGT=3;
    public static final int FULL_RANK_MATRIX=4;

	public int type;

	public NumericalException(int type){
		super("Numerical problems!");
		this.type=type;
	}

	public String getMessage(){
        if (type==0) return "Computed kernel is not correct.";
        if (type==1) return "Numerical problems.";
        if (type==2) return "Computed EM does not fulfill steady-state condition.";
        if (type==3) return "Tried to compute gcd of negative number.";
        if (type==4) return "Matrix is of full rank.";
        return "";
    }
}
