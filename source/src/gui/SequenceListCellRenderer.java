/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui;

import com.general.utils.GUIutils;
import de.biozentrum.bioinformatik.sequence.Sequence;
import gui.sequencebundle.SequenceBundleColorModel;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.image.BufferedImage;
import java.util.Map;
import javax.swing.DefaultListCellRenderer;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
class SequenceListCellRenderer extends JLabel implements ListCellRenderer<Sequence> {
	private final Map<Sequence,Integer> sequenceGroups;
	private final SequenceBundleColorModel  colorModel;
	
	public SequenceListCellRenderer(Map<Sequence,Integer> groups, SequenceBundleColorModel colorModel){
		setOpaque(true);
		this.sequenceGroups=groups;
		if (colorModel==null) {
			this.colorModel=new SequenceBundleColorModel();
		} else {
			this.colorModel = colorModel;
		}
	}
	public SequenceListCellRenderer() {
		this(null, null);
	}
	

	@Override
	public Component getListCellRendererComponent(JList<? extends Sequence> list, Sequence value, int index, boolean isSelected, boolean cellHasFocus) {
		//Font font = getFont();
		setFont(list.getFont());
		setText(value.getName());
		Color background;
		Color foreground;
		// check if this cell represents the current DnD drop location
		JList.DropLocation dropLocation = list.getDropLocation();
		if (dropLocation != null && !dropLocation.isInsert() && dropLocation.getIndex() == index) {
			background = Color.BLUE;
			foreground = Color.WHITE;
			// check if this cell is selected
		} else if (isSelected) {
			background = colorModel.getSelectionMarkerColor();
			foreground = GUIConstants.FOREGROUND_COLOR;
			// unselected, and not the DnD drop location
		} else {
			background = Color.WHITE;
			foreground = GUIConstants.FOREGROUND_COLOR;
		}
		setBackground(background);
		setForeground(foreground);
		
		if (this.sequenceGroups==null) return this;
		
		Integer group = this.sequenceGroups.get(value); 
		int cellHeight;
		if (list.getFixedCellHeight()>0) {
			cellHeight = list.getFixedCellHeight();
		} else {
			cellHeight = this.getPreferredSize().height;
			if (cellHeight<=0) {
				cellHeight = 20;
			}
		}
		
		int squareWidth = (int)Math.round(0.8 * cellHeight);
		int squareHeight = squareWidth;
		BufferedImage img = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration().createCompatibleImage(squareWidth, squareHeight);
		Graphics2D g = img.createGraphics();
		GUIutils.setQualityRenderingHints(g);
		g.setColor(colorModel.getGroupColors().get(group));
		g.fillRect(0, 0, squareWidth, squareHeight);		
		g.dispose();
		this.setIcon(new ImageIcon(img));
		
		return this;
	}
	
}
