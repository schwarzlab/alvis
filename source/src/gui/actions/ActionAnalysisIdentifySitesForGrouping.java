/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.actions;

import alvis.AlvisDataModel;
import alvis.algorithm.SitewiseAnovaTask;
import com.general.gui.dialog.DialogFactory;
import com.general.gui.progress.ProgressView;
import gui.sequencebundle.ColumnAnnotation;
import gui.sequencebundle.DefaultColumnAnnotation;
import gui.sequencebundle.JSequenceBundle;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.concurrent.ExecutionException;
import javax.swing.AbstractAction;
import javax.swing.SwingWorker;

/**
 *
 * @author Roland Schwarz <rfs32@cam.ac.uk>
 */
public class ActionAnalysisIdentifySitesForGrouping extends AbstractAction implements PropertyChangeListener{
	private final AlvisDataModel dataModel;
	private final ProgressView progressView;
	private final JSequenceBundle bundle;
	public ActionAnalysisIdentifySitesForGrouping(AlvisDataModel dataModel, ProgressView progressView, JSequenceBundle bundle) {
		super("Identify sites for grouping");
		this.dataModel=dataModel;
		this.progressView=progressView;
		this.dataModel.addPropertyChangeListener(AlvisDataModel.PROPERTY_FISHER_SCORES, this);
		setEnabled(this.dataModel.getFisherScores()!=null);
		this.bundle=bundle;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		setEnabled(false);
		final SitewiseAnovaTask task = new SitewiseAnovaTask(dataModel.getFisherScores(), dataModel.getSequenceGroups(), dataModel.getAlignment());
		progressView.addModel(task.getProgressable());		
		task.addPropertyChangeListener(new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				if (evt.getPropertyName().equals("state") && evt.getNewValue().equals(SwingWorker.StateValue.DONE)){
					processResult(task);
				}
			}
		});
		task.execute();
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		setEnabled(this.dataModel.getFisherScores()!=null);
	}

	private void processResult(SitewiseAnovaTask task) {
		try {
			double[] anovaResult = task.get();
			ColumnAnnotation ca = new DefaultColumnAnnotation(anovaResult, true);
			bundle.setColumnAnnotation(ca);
		} catch (InterruptedException|ExecutionException ex){
			DialogFactory.showErrorMessage(ex.toString(), "Error building tree:");
		}
		progressView.removeModel(task.getProgressable());
		setEnabled(true);
	}
	
}
