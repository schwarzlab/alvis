/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.actions;

import alvis.Alvis;
import alvis.AlvisDataModel;
import com.general.gui.dialog.DialogFactory;
import com.general.gui.fileHistory.FileHistory;
import gui.GUIConstants;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import javax.swing.AbstractAction;
import static javax.swing.Action.ACCELERATOR_KEY;
import javax.swing.KeyStroke;
import javax.swing.filechooser.FileFilter;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public class ActionFileSave extends AbstractAction implements PropertyChangeListener{
	private Alvis alvis;
	public ActionFileSave(Alvis alvis) {
		super("Save");
		this.alvis=alvis;
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_DOWN_MASK));	
		alvis.getDataModel().addPropertyChangeListener(AlvisDataModel.PROPERTY_ALIGNMENT, this);
		setEnabled(alvis.getDataModel().getAlignment()!=null);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		File chosenFile=alvis.getDataModel().getFile();
		if (chosenFile==null) {
			chosenFile = DialogFactory.showFileSaveDialog(new FileFilter[] {GUIConstants.ALVIS_FILE_FILTER});
		} 
		if (chosenFile!=null) {
			try {
				alvis.getDataModel().save(chosenFile);
				FileHistory.getInstance().registerFile(chosenFile);
			} catch (IOException ex) {
				DialogFactory.showErrorMessage(ex.toString(), "File save error:");
			}
		}
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		setEnabled(alvis.getDataModel().getAlignment()!=null);
	}
	
	
}
