/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.actions;

import alvis.AlvisDataModel;
import alvis.algorithm.matrix.JBlasClusterableAdapter;
import alvis.algorithm.SpectralClustering;
import alvis.algorithm.SpectralClustering.SpectralClusteringTask;
import com.general.containerModel.Nameable;
import com.general.containerModel.impl.MapListenerDecorator;
import com.general.gui.dialog.DialogFactory;
import com.general.gui.progress.DefaultProgressable;
import com.general.gui.progress.ProgressView;
import com.general.gui.progress.Progressable;
import com.stromberglabs.cluster.Cluster;
import com.stromberglabs.cluster.Clusterable;
import de.biozentrum.bioinformatik.alignment.MultipleAlignment;
import de.biozentrum.bioinformatik.sequence.Sequence;
import com.general.gui.panels.SelectNameablePanel;
import gui.SequenceGroupsPanel;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;
import java.util.concurrent.ExecutionException;
import javax.swing.AbstractAction;
import javax.swing.SwingWorker;
import org.jblas.DoubleMatrix;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public class ActionAnalysisClusterSequences extends AbstractAction implements PropertyChangeListener{
	SpectralClustering specc=null;
	DoubleMatrix K=null;
	DefaultProgressable p;
	int nclusters=2;
	private final AlvisDataModel dataModel;
	private final ProgressView progressView;
	
	public ActionAnalysisClusterSequences(AlvisDataModel dataModel, ProgressView progressView) {
		super("Cluster sequences into groups...");
		this.dataModel=dataModel;
		this.progressView = progressView;
		dataModel.addPropertyChangeListener(AlvisDataModel.PROPERTY_KERNELS, this);
		setEnabled(!dataModel.getKernelMatrices().isEmpty());
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		SelectNameablePanel panel = new SelectNameablePanel(dataModel.getKernelMatrices().keySet(), "Choose Kernel:");
		if (!DialogFactory.showModalOKCancelDialog(panel)) { // cancelled
			return; 
		}
		Nameable id = panel.getSelectedObject();
		DoubleMatrix newK = dataModel.getKernelMatrix(id);
		if (newK!=null && K!=newK){
			K=newK;
			specc = new SpectralClustering(K);
		}
		Integer input=null;
		String text;
		boolean valid=false;
		while (!valid){
			text = DialogFactory.showInputDialog("Number of groups?", nclusters);
			if (text==null) {
				input=null;
				break;
			}
			int maxgroups = Math.min(dataModel.getAlignment().getSequenceCount(), SequenceGroupsPanel.MAX_GROUPS);			
			try {
				input=Integer.valueOf(text);
				if (input>=2 && input<=maxgroups) {
					valid=true;
				} else {
					valid=false;
				}
			} catch (NumberFormatException ex) {
				valid=false;
			}
			if (!valid){
				String errorMsg = "Number of groups must be between 2 and " + maxgroups + ".";
				DialogFactory.showInformationDialog(errorMsg, "Invalid input");
			}
		}
		
		if (input==null) {
			return;
		} else {
			nclusters=input;
		}
		
		specc.setNumberOfClusters(nclusters);
		p = new DefaultProgressable("Clustering...", true);
		progressView.addModel(p);
		p.setState(Progressable.STATE_RUNNING);
		final SpectralClusteringTask task = specc.createTask();
		task.addPropertyChangeListener(new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				if (evt.getPropertyName().equals("state")){
					if (evt.getNewValue()==SwingWorker.StateValue.DONE) {
						try {
							Cluster[] result=task.get();
							processResults(result);
						} catch (InterruptedException | ExecutionException ex) {
							DialogFactory.showErrorMessage(ex.getMessage(), "Error");
						}
						cleanUp();
					}
				}
			}
		});
		task.execute();
	}

	private void processResults(Cluster[] result){
		MapListenerDecorator<Sequence, Integer> groups = MapListenerDecorator.getInstance(dataModel.getSequenceGroups());
		MultipleAlignment alignment = dataModel.getAlignment();
		
		groups.setValueIsAdjusting(true);
		for (int group=0;group<result.length;group++){
			List<Clusterable> groupMembers = result[group].getItems();
			for (int i=0;i<groupMembers.size();i++){
				JBlasClusterableAdapter.DoubleClusterable member = (JBlasClusterableAdapter.DoubleClusterable)groupMembers.get(i);
				int seqid = member.getIndex();
				Sequence seq = alignment.getSequence(seqid);
				if (nclusters<SequenceGroupsPanel.MAX_GROUPS){ // if we have space avoid the default group
					groups.put(seq, group+1);
				} else {
					groups.put(seq, group);
				}


			}
		}
		groups.setValueIsAdjusting(false);
	}

	private void cleanUp() {
		p.setState(Progressable.STATE_IDLE);
		progressView.removeModel(p);
	}
	
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		setEnabled(!dataModel.getKernelMatrices().isEmpty());
	}
}
