/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.actions;

import com.general.containerModel.impl.MapListenerDecorator;
import com.general.resources.MyJavaResources;
import de.biozentrum.bioinformatik.alignment.AlignmentTools;
import de.biozentrum.bioinformatik.alignment.MultipleAlignment;
import de.biozentrum.bioinformatik.sequence.Sequence;
import gui.sequencebundle.JSequenceBundle;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.util.List;
import java.util.Map;
import javax.swing.AbstractAction;
import static javax.swing.Action.NAME;
import static javax.swing.Action.SMALL_ICON;
import javax.swing.JList;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *
 * @author Roland Schwarz <rfs32@cam.ac.uk>
 */
public class ActionSequencesSetAsReference extends AbstractAction implements ListSelectionListener{
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

	private final JList<Sequence> list;
	private final MultipleAlignment alignment;
	private final JSequenceBundle bundle;
	

	public ActionSequencesSetAsReference(JList<Sequence> list, MultipleAlignment alignment, JSequenceBundle bundle)  {
		this.list=list;
		this.alignment=alignment;
		this.bundle=bundle;
		this.list.getSelectionModel().addListSelectionListener(this);
		setEnabled(!this.list.getSelectionModel().isSelectionEmpty());
		putValue(NAME, "Set as reference sequence");
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		int[] selected = list.getSelectedIndices();
		if (selected.length>0) {
			Sequence ref = AlignmentTools.getConsensusSequence(alignment, selected);
			bundle.setReferenceSequence(ref);
		}
		
	}

	@Override
	public void valueChanged(ListSelectionEvent e) {
		setEnabled(!this.list.getSelectionModel().isSelectionEmpty());
	}
	
	
	
}
