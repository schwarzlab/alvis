 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.actions;

import alvis.AlvisDataModel;
import com.general.gui.dialog.DialogFactory;
import gui.GUIConstants;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import javax.swing.AbstractAction;
import javax.swing.filechooser.FileFilter;
import org.phylowidget.PhyloTree;
import org.phylowidget.alvis.PhyloWidgetTools;

/**
 *
 * @author Roland Schwarz <rfs32@cam.ac.uk>
 */
public class ActionFileExportTree extends AbstractAction implements PropertyChangeListener{
	private final AlvisDataModel dataModel;
	public ActionFileExportTree(AlvisDataModel dataModel) {
		super("Export tree...");
		this.dataModel = dataModel;
		this.dataModel.addPropertyChangeListener(AlvisDataModel.PROPERTY_TREE, this);
		setEnabled(this.dataModel.getTree()!=null);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		File file = DialogFactory.showFileSaveDialog(new FileFilter[]{GUIConstants.NEWICK_FILE_FILTER});
		if (file!=null){
			PhyloTree tree=dataModel.getTree();
			try {
				PhyloWidgetTools.saveTree(tree, file);
			} catch (Exception ex) {
				DialogFactory.showErrorMessage(ex.getMessage(), "Error:");
			}
		}
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		setEnabled(this.dataModel.getTree()!=null);
	}
}
