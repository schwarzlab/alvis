/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.actions;

import gui.MultipleAlignmentPanelExtension;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;

/**
 *
 * @author Roland Schwarz <rfs32@cam.ac.uk>
 */
public class ActionSequencesShowAll extends AbstractAction{
	private MultipleAlignmentPanelExtension alignmentPanel;
	public ActionSequencesShowAll(MultipleAlignmentPanelExtension alignmentPanel) {
		super("Show all");
		this.alignmentPanel = alignmentPanel;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (alignmentPanel.getVisibilityModel()!=null){
			alignmentPanel.getVisibilityModel().showAll();
		}
	}

}
