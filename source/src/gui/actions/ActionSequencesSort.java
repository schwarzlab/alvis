/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.actions;

import alvis.AlvisDataModel;
import alvis.algorithm.SortSequencesTask;
import alvis.algorithm.comparison.SequenceComparatorByGroup;
import alvis.algorithm.comparison.SequenceComparatorByIndex;
import alvis.algorithm.comparison.SequenceComparatorByName;
import com.general.gui.dialog.DialogFactory;
import com.general.gui.progress.ProgressView;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ExecutionException;
import javax.swing.AbstractAction;
import org.phylowidget.alvis.AlvisPhyloWidgetAPI;

/**
 *
 * @author Roland Schwarz <rfs32@cam.ac.uk>
 */
public class ActionSequencesSort extends AbstractAction implements PropertyChangeListener{
	private final AlvisDataModel dataModel;
	private AlvisPhyloWidgetAPI phyloWidget;
	private ProgressView progressView;
	public ActionSequencesSort(AlvisDataModel dataModel, ProgressView progressView, AlvisPhyloWidgetAPI phyloWidget) {
		super("Sort...");
		this.phyloWidget = phyloWidget;
		this.progressView = progressView;
		this.dataModel = dataModel;
		this.dataModel.addPropertyChangeListener(AlvisDataModel.PROPERTY_ALIGNMENT, this);
		setEnabled(this.dataModel.getAlignment()!=null);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		List<Comparator> comparators = new ArrayList<>();
		comparators.add(new SequenceComparatorByName());
		comparators.add(new SequenceComparatorByGroup(dataModel.getSequenceGroups()));
		if (dataModel.getTree()!=null){
			phyloWidget.setSequences(dataModel.getAlignment());
			phyloWidget.update();
			comparators.add(new SequenceComparatorByIndex(dataModel.getAlignment().getSequences(), phyloWidget.getSequenceNodeOrder(), "Tree position"));
		}
		Comparator chosenComparator = (Comparator) DialogFactory.showInputDialog("Select sequence ordering:", "Order sequences", comparators.toArray(), comparators.get(0));
		if (chosenComparator!=null) {
			final SortSequencesTask task = new SortSequencesTask(dataModel, chosenComparator);
			progressView.addModel(task.getProgressable());
			task.addPropertyChangeListener(new PropertyChangeListener() {
				@Override
				public void propertyChange(PropertyChangeEvent evt) {
					processResult(task);
				}
			});
			task.execute();
		}
	}

	private void processResult(SortSequencesTask task){
		try {
			task.get();
		} catch (InterruptedException | ExecutionException ex) {
			DialogFactory.showErrorMessage(ex.toString(), "Error:");
		}
		progressView.removeModel(task.getProgressable());
	}
	
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		setEnabled(this.dataModel.getAlignment()!=null);
	}
}
