/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.actions;

import alvis.AlvisDataModel;
import alvis.algorithm.ComputeAlignmentKernelTask;
import alvis.algorithm.matrix.NameableSubstitutionMatrix;
import alvis.resources.SubstitutionMatrixRepository;
import com.general.gui.dialog.DialogFactory;
import gui.MainFrame;
import com.general.gui.panels.SelectNameablePanel;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import javax.swing.AbstractAction;
import javax.swing.SwingWorker;
import org.jblas.DoubleMatrix;

/**
 *
 * @author Roland Schwarz <rfs32@cam.ac.uk>
 */
public class ActionAnalysisComputeAlignmentKernel extends AbstractAction implements PropertyChangeListener{
	private final AlvisDataModel dataModel;
	private final MainFrame frmMain;
	public ActionAnalysisComputeAlignmentKernel(AlvisDataModel dataModel, MainFrame frmMain) {
		super("Compute alignment kernel...");
		this.dataModel = dataModel;
		this.frmMain = frmMain;
		this.dataModel.addPropertyChangeListener(AlvisDataModel.PROPERTY_ALIGNMENT, this);
		setEnabled(this.dataModel.getAlignment()!=null);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		List<NameableSubstitutionMatrix> subsmats = SubstitutionMatrixRepository.getSubstitutionMatrices(dataModel.getAlignmentType());
		NameableSubstitutionMatrix defaultmat = SubstitutionMatrixRepository.getDefaultSubstitutionMatrix(dataModel.getAlignmentType());
		SelectNameablePanel panel = new SelectNameablePanel(subsmats, defaultmat, "Choose substitution matrix:");
		if (!DialogFactory.showModalOKCancelDialog(panel)) { // cancelled
			return; 
		}
		NameableSubstitutionMatrix subsmat = (NameableSubstitutionMatrix) panel.getSelectedObject();
		
		final ComputeAlignmentKernelTask task = new ComputeAlignmentKernelTask(dataModel.getAlignment(), subsmat, 5, 2);
		task.addPropertyChangeListener(new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				if (evt.getPropertyName().equals("state") && evt.getNewValue().equals(SwingWorker.StateValue.DONE)) {
					frmMain.removeProgressable(task.getProgressable());
					try {
						DoubleMatrix result = task.get();
						dataModel.addKernelMatrix(task.getKernelDescription(), result);
					} catch (ExecutionException | InterruptedException ex) {
						DialogFactory.showErrorMessage(ex.toString(), "Error");
				 	} catch (CancellationException ex) {}
				}
			}
		});
		frmMain.addProgressable(task.getProgressable());
		task.execute();
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		setEnabled(this.dataModel.getAlignment()!=null);
	}
	
}
