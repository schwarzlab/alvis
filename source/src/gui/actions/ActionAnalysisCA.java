/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.actions;

import alvis.AlvisDataModel;
import alvis.algorithm.ComputeFisherScoresTask;
import alvis.algorithm.CorrespondanceAnalysisTask;
import alvis.algorithm.ProfileHMMTools;
import cama.BioJavaHelper;
import cama.CAMAConfiguration;
import cama.CAMADataProvider;
import cama.CAMAFrame;
import com.general.gui.dialog.DialogFactory;
import de.biozentrum.bioinformatik.ca.CADataModel;
import de.biozentrum.bioinformatik.sequence.Sequence;
import gui.MainFrame;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.concurrent.ExecutionException;
import javax.swing.AbstractAction;
import javax.swing.SwingWorker;
import org.biojava.bio.BioException;
import org.biojava.bio.alignment.Alignment;
import org.biojava.bio.seq.NucleotideTools;
import org.biojava.bio.seq.ProteinTools;
import org.biojava.bio.symbol.FiniteAlphabet;

/**
 *
 * @author Roland Schwarz <rfs32@cam.ac.uk>
 */
public class ActionAnalysisCA extends AbstractAction implements PropertyChangeListener{
	private final AlvisDataModel dataModel;
	private final MainFrame frmMain;
	public ActionAnalysisCA(AlvisDataModel dataModel, MainFrame frmMain) {
		super("Correspondence analysis");
		this.dataModel = dataModel;
		this.frmMain = frmMain;
		this.dataModel.addPropertyChangeListener(AlvisDataModel.PROPERTY_PROFILE_HMM, this);
		this.dataModel.addPropertyChangeListener(AlvisDataModel.PROPERTY_ALIGNMENT, this);
		this.dataModel.addPropertyChangeListener(AlvisDataModel.PROPERTY_FISHER_SCORES, this);
		setEnabled(this.dataModel.getProfileHMM()!=null && this.dataModel.getAlignment()!=null && this.dataModel.getFisherScores()!=null);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		final CAMADataProvider dataProvider = new CAMADataProvider(this.dataModel.getFisherScores(), this.dataModel.getAlignment());
		final CorrespondanceAnalysisTask task = new CorrespondanceAnalysisTask(dataProvider);
		task.addPropertyChangeListener(new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				if (evt.getPropertyName().equals("state") && evt.getNewValue().equals(SwingWorker.StateValue.DONE)) {
					frmMain.removeProgressable(task.getProgressable());
					try {
						CADataModel results = task.get();
						results.setAnnotation(dataProvider);
						final CAMAConfiguration config = new CAMAConfiguration();
						config.setAlphabet(BioJavaHelper.convertAlphabet(dataModel.getAlignment().getAlphabet()));
						config.setAlphabetIndex(BioJavaHelper.createNonatomicAlphabetIndex(dataModel.getFisherScores().getAlphabet()));
						config.setDataModel(results);
						config.setDataProvider(dataProvider);
						config.setName(dataModel.getName());
						Alignment alignment = BioJavaHelper.convertAlignment(dataModel.getAlignment());
						boolean[] columnMask = ProfileHMMTools.profileColumnMask(alignment, 0.51);
						config.setColumnMask(columnMask);
						final CAMAFrame frame = new CAMAFrame(config, frmMain.getAlignmentPanel());
						frame.createGUI();
//						frame.pack();
						frame.setVisible(true);
						dataModel.addPropertyChangeListener(AlvisDataModel.PROPERTY_FILE, new PropertyChangeListener() {
							@Override
							public void propertyChange(PropertyChangeEvent evt) {
								config.setName(dataModel.getName());
								frame.setTitle(dataModel.getName());
							}
						});
					} catch (ExecutionException | InterruptedException | BioException ex) {
						DialogFactory.showErrorMessage(ex.toString(), "Error");
					}
				}
			}
		});
		frmMain.addProgressable(task.getProgressable());
		task.execute();
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		setEnabled(this.dataModel.getProfileHMM()!=null && this.dataModel.getAlignment()!=null && this.dataModel.getFisherScores()!=null);
	}
	
}
