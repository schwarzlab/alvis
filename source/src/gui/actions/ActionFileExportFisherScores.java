/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.actions;

import alvis.Alvis;
import alvis.AlvisDataModel;
import alvis.algorithm.ProfileHMMTools;
import alvis.algorithm.matrix.FisherScoresMatrix;
import com.general.gui.dialog.DialogFactory;
import de.biozentrum.bioinformatik.sequence.Sequence;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileWriter;
import javax.swing.AbstractAction;
import org.biojava.bio.seq.io.SymbolTokenization;
import org.biojava.bio.symbol.FiniteAlphabet;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public class ActionFileExportFisherScores extends AbstractAction implements PropertyChangeListener{
	private AlvisDataModel dataModel;
	public ActionFileExportFisherScores(AlvisDataModel dataModel) {
		super("Export Fisher scores...");
		this.dataModel = dataModel;
		dataModel.addPropertyChangeListener(AlvisDataModel.PROPERTY_FISHER_SCORES, this);
		setEnabled(dataModel.getFisherScores()!=null);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		FisherScoresMatrix dataMatrix=dataModel.getFisherScores();			
		if (dataMatrix==null || dataMatrix.rows==0 || dataMatrix.columns==0) {
			DialogFactory.showErrorMessage("No Fisher scores found to export!", "Error");
			return;
		}

		File file = DialogFactory.showFileSaveDialog();
		if (file==null) return;

		try {
			FileWriter writer=new FileWriter(file);
			FiniteAlphabet alphabet = dataMatrix.getAlphabet();
			SymbolTokenization tokenization = alphabet.getTokenization("token");

			StringBuilder header=new StringBuilder();
			
			boolean[] mask = ProfileHMMTools.profileColumnMask(dataModel.getAlignment(), AlvisDataModel.GAP_CHAR, AlvisDataModel.MATCH_CUTOFF);
			for (int j=0;j<dataMatrix.columns;j++) {
				header.append(",");
				header.append(tokenization.tokenizeSymbol(dataMatrix.getSymbolForColumn(j)));
				header.append("_");
				int profileColumn = j / alphabet.size();
				int alignmentColumn = ProfileHMMTools.getAlignmentColumnFromProfileColumn(profileColumn, mask);
				header.append(alignmentColumn+1);
			}
			//header.append(tokenization.tokenizeSymbol(index.symbolForIndex((dataMatrix.columns()-1) % symbolCount)));
			header.append("\n");
			writer.write(header.toString());
			// generate fisher scores comma separated values file
			for (int i=0; i < dataMatrix.rows; i++) {
				StringBuilder fisherScores=new StringBuilder();
				Sequence s = dataModel.getAlignment().getSequence(i);
				fisherScores.append(s.getName());
				fisherScores.append(",");
				for (int j=0; j<dataMatrix.columns-1; j++) {
					fisherScores.append(dataMatrix.get(i, j));
					fisherScores.append(",");
				}
				fisherScores.append(dataMatrix.get(i,dataMatrix.columns-1));
				fisherScores.append("\n");
				writer.write(fisherScores.toString());
			}

			writer.close();
		} catch (Exception ex) {
			DialogFactory.showErrorMessage(ex.getMessage(), "Error:");
		}
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		setEnabled(dataModel.getFisherScores()!=null);
	}
}

