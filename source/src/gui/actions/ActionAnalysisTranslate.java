/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.actions;

import alvis.AlvisDataModel;
import alvis.algorithm.TranslateAlignmentTask;
import com.general.gui.dialog.DialogFactory;
import de.biozentrum.bioinformatik.alignment.MultipleAlignment;
import gui.MainFrame;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.AbstractAction;
import javax.swing.SwingWorker;

/**
 *
 * @author Roland Schwarz <rfs32@cam.ac.uk>
 */
public class ActionAnalysisTranslate extends AbstractAction implements PropertyChangeListener{
	private final AlvisDataModel dataModel;
	private final MainFrame mainFrame;
	public ActionAnalysisTranslate(AlvisDataModel dataModel, MainFrame mainFrame) {
		super("Translate (DNA -> Prot)");
		this.dataModel=dataModel;
		this.mainFrame = mainFrame;
		this.dataModel.addPropertyChangeListener(AlvisDataModel.PROPERTY_ALIGNMENT, this);
		setEnabled(this.dataModel.getAlignment()!=null && this.dataModel.getAlignmentType().equals(AlvisDataModel.AlignmentType.DNA));
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		MultipleAlignment alignment = dataModel.getAlignment();
		final TranslateAlignmentTask task = new TranslateAlignmentTask(alignment);
		mainFrame.addProgressable(task.getProgressable());
		task.addPropertyChangeListener(new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				if (evt.getPropertyName().equals("state") && evt.getNewValue().equals(SwingWorker.StateValue.DONE)){
					processResult(task);
				}
			}
		});
		task.execute();
		
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		setEnabled(this.dataModel.getAlignment()!=null && this.dataModel.getAlignmentType().equals(AlvisDataModel.AlignmentType.DNA));
	}
	
	private void processResult(TranslateAlignmentTask task) {
		try {
			MultipleAlignment newAlignment = task.get();
			dataModel.setAlignment(newAlignment);
		} catch (Exception ex) {
			DialogFactory.showErrorMessage(ex, "Error translating alignment");
		} 
		mainFrame.removeProgressable(task.getProgressable());
	}

}
