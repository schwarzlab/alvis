/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.actions;

import com.general.containerModel.impl.MapListenerDecorator;
import com.general.resources.MyJavaResources;
import de.biozentrum.bioinformatik.sequence.Sequence;
import alvis.resources.AlvisResources;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.util.Map;
import javax.swing.AbstractAction;
import javax.swing.JList;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public class ActionSequencesAssignGroup extends AbstractAction implements ListSelectionListener{
	private final int group;
	private final JList<Sequence> list;
	private final Map<Sequence, Integer> sequenceGroups;
	

	public ActionSequencesAssignGroup(Map<Sequence, Integer> sequenceGroups, JList<Sequence> list, int group, Color color) {
		this.sequenceGroups = sequenceGroups;
		this.group=group;
		this.list=list;
		this.list.getSelectionModel().addListSelectionListener(this);
		setEnabled(!this.list.getSelectionModel().isSelectionEmpty());
		putValue(NAME, String.valueOf(group+1));
		putValue(SMALL_ICON, MyJavaResources.createColoredSquare(12, color));	
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		MapListenerDecorator<Sequence, Integer> map = MapListenerDecorator.getInstance(sequenceGroups);
		map.setValueIsAdjusting(true);
		for (Sequence seq:list.getSelectedValuesList()) {
			map.put(seq, group);
		}
		map.setValueIsAdjusting(false);
	}

	@Override
	public void valueChanged(ListSelectionEvent e) {
		setEnabled(!this.list.getSelectionModel().isSelectionEmpty());
	}
	
	
	
}
