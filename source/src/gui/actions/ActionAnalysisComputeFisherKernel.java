/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.actions;

import alvis.AlvisDataModel;
import alvis.algorithm.ComputeFisherScoresTask;
import com.general.gui.dialog.DialogFactory;
import gui.MainFrame;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import javax.swing.AbstractAction;
import javax.swing.SwingWorker;

/**
 *
 * @author Roland Schwarz <rfs32@cam.ac.uk>
 */
public class ActionAnalysisComputeFisherKernel extends AbstractAction implements PropertyChangeListener{
	private final AlvisDataModel dataModel;
	private final MainFrame frmMain;
	public ActionAnalysisComputeFisherKernel(AlvisDataModel dataModel, MainFrame frmMain) {
		super("Compute Fisher kernel");
		this.dataModel = dataModel;
		this.frmMain = frmMain;
		this.dataModel.addPropertyChangeListener(AlvisDataModel.PROPERTY_PROFILE_HMM, this);
		this.dataModel.addPropertyChangeListener(AlvisDataModel.PROPERTY_ALIGNMENT, this);
		setEnabled(this.dataModel.getProfileHMM()!=null && this.dataModel.getAlignment()!=null);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		final ComputeFisherScoresTask task = new ComputeFisherScoresTask(dataModel.getProfileHMM(), dataModel.getAlignment());
		task.addPropertyChangeListener(new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				if (evt.getPropertyName().equals("state") && evt.getNewValue().equals(SwingWorker.StateValue.DONE)) {
					frmMain.removeProgressable(task.getProgressable());
					try {
						ComputeFisherScoresTask.FisherScoresTaskResult results = task.get();
						dataModel.addKernelMatrix(ComputeFisherScoresTask.FISHER_KERNEL_DESCRIPTION, results.fisherKernel);
						dataModel.setFisherScores(results.fisherScores);
						dataModel.setFisherKernelSVD(results.svd);
					} catch (ExecutionException | InterruptedException ex) {
						DialogFactory.showErrorMessage(ex.toString(), "Error");
					} catch (CancellationException ce) {}
				}
			}
		});
		frmMain.addProgressable(task.getProgressable());
		task.execute();
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		setEnabled(this.dataModel.getProfileHMM()!=null && this.dataModel.getAlignment()!=null);
	}
	
}
