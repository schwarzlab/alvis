/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.actions;

import gui.MultipleAlignmentPanelExtension;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *
 * @author Roland Schwarz <rfs32@cam.ac.uk>
 */
public class ActionSequencesHideSelected extends AbstractAction implements ListSelectionListener{
	private MultipleAlignmentPanelExtension alignmentPanel;
	public ActionSequencesHideSelected(MultipleAlignmentPanelExtension alignmentPanel) {
		super("Hide selected");
		this.alignmentPanel = alignmentPanel;
		this.alignmentPanel.getAlignmentView().getSequenceSelectionModel().addListSelectionListener(this);
		setEnabled(!this.alignmentPanel.getAlignmentView().getSequenceSelectionModel().isSelectionEmpty());
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		alignmentPanel.hideSelectedSequences();
	}

	@Override
	public void valueChanged(ListSelectionEvent e) {
		setEnabled(!this.alignmentPanel.getAlignmentView().getSequenceSelectionModel().isSelectionEmpty());
	}
	
}
