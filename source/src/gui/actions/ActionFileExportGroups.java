 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.actions;

import alvis.AlvisDataModel;
import com.general.gui.dialog.DialogFactory;
import com.general.utils.GeneralUtils;
import com.general.utils.IOUtils;
import gui.GUIConstants;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import javax.swing.AbstractAction;
import javax.swing.filechooser.FileFilter;
import org.phylowidget.PhyloTree;
import org.phylowidget.alvis.PhyloWidgetTools;

/**
 *
 * @author Roland Schwarz <rfs32@cam.ac.uk>
 */
public class ActionFileExportGroups extends AbstractAction implements PropertyChangeListener{
	private final AlvisDataModel dataModel;
	public ActionFileExportGroups(AlvisDataModel dataModel) {
		super("Export groups...");
		this.dataModel = dataModel;
		this.dataModel.addPropertyChangeListener(AlvisDataModel.PROPERTY_ALIGNMENT, this);
		setEnabled(this.dataModel.getAlignment()!=null);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		File file = DialogFactory.showFileSaveDialog();
		if (file!=null){
			String text = dataModel.groupsToString();
			try {
				IOUtils.writeTextToFile(text, file);
			} catch (IOException ex) {
				DialogFactory.showErrorMessage(ex.toString(), "Error exporting groups:");
			}
		}
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		setEnabled(this.dataModel.getTree()!=null);
	}
}
