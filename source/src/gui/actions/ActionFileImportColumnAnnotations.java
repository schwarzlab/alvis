/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.actions;

import com.general.gui.dialog.DialogFactory;
import gui.sequencebundle.DefaultColumnAnnotation;
import gui.sequencebundle.JSequenceBundle;
import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.AbstractAction;
import org.apache.commons.lang3.ArrayUtils;

/**
 *
 * @author Roland Schwarz <rfs32@cam.ac.uk>
 */
public class ActionFileImportColumnAnnotations extends AbstractAction{
	private final JSequenceBundle bundle;
	public ActionFileImportColumnAnnotations(JSequenceBundle bundle) {
		super("Import column annotations...");
		this.bundle=bundle;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		File file = DialogFactory.showSingleFileOpenDialog();
		if (file!=null){
			// do your stuff
			ArrayList<Double> vals = new ArrayList<>();
			try {
				BufferedReader br = new BufferedReader(new FileReader(file));
					String line = null;
					while ((line = br.readLine()) != null) {
						try {
							vals.add(Double.parseDouble(line.trim()));
						} catch (NumberFormatException ex) {
							vals.add(Double.NaN);
						}
					}
				br.close();			
				bundle.setColumnAnnotation(new DefaultColumnAnnotation(ArrayUtils.toPrimitive(vals.toArray(new Double[0]))));
			} catch (IOException ex) {
				DialogFactory.showErrorMessage(ex, "Error reading file");
			}
			
		}
	}
}
