/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.actions;

import alvis.AlvisDataModel;
import alvis.algorithm.TrainHMMTask;
import com.general.gui.dialog.DialogFactory;
import gui.MainFrame;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.concurrent.ExecutionException;
import javax.swing.AbstractAction;
import javax.swing.SwingWorker;
import org.biojava.bio.dp.ProfileHMM;

/**
 *
 * @author Roland Schwarz <rfs32@cam.ac.uk>
 */
public class ActionAnalysisTrainHMM extends AbstractAction implements PropertyChangeListener{
	private final AlvisDataModel dataModel;
	private final MainFrame frmMain;
	public ActionAnalysisTrainHMM(AlvisDataModel dataModel, MainFrame frmMain) {
		super("Train HMM");
		this.dataModel = dataModel;
		this.frmMain = frmMain;
		this.dataModel.addPropertyChangeListener(AlvisDataModel.PROPERTY_ALIGNMENT, this);
		setEnabled(this.dataModel.getAlignment()!=null);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		final TrainHMMTask task = new TrainHMMTask(dataModel.getAlignment());
		task.addPropertyChangeListener(new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				if (evt.getPropertyName().equals("state") && evt.getNewValue().equals(SwingWorker.StateValue.DONE)) {
					frmMain.removeProgressable(task.getProgressable());
					try {
						ProfileHMM pHMM = task.get();
						dataModel.setProfileHMM(pHMM);
					} catch (ExecutionException | InterruptedException ex) {
						DialogFactory.showErrorMessage(ex.toString(), "Error");
					}
				}
			}
		});
		frmMain.addProgressable(task.getProgressable());
		task.execute();
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		setEnabled(this.dataModel.getAlignment()!=null);
	}
	
}
