/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.actions;

import gui.sequencebundle.JSequenceBundle;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import static javax.swing.Action.NAME;

/**
 *
 * @author Roland Schwarz <rfs32@cam.ac.uk>
 */
public class ActionSequencesUnsetReference extends AbstractAction { 

	private final JSequenceBundle bundle;
	

	public ActionSequencesUnsetReference(JSequenceBundle bundle)  {
		this.bundle=bundle;
		putValue(NAME, "Clear reference sequence");
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		bundle.setReferenceSequence(null);
	}

}
