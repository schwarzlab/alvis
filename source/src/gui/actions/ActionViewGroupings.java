/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.actions;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.JComponent;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public class ActionViewGroupings extends AbstractAction{
	private final JComponent container;
	private final JComponent component;
	public ActionViewGroupings(JComponent container, JComponent component) {
		super("Groups");
		this.container=container;
		this.component=component;
		putValue(SELECTED_KEY, component.getParent()==container);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		boolean show=true;
		if (e.getSource() instanceof AbstractButton) {
			AbstractButton mnu = (AbstractButton)e.getSource();
			show = mnu.isSelected();
		}
		if (show) {
			container.add(component, BorderLayout.EAST);
		} else {
			container.remove(component);
		}
		container.revalidate();
		container.repaint();
	}
	
}
