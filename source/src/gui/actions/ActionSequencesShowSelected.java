/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.actions;

import gui.MultipleAlignmentPanelExtension;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *
 * @author Roland Schwarz <rfs32@cam.ac.uk>
 */
public class ActionSequencesShowSelected extends AbstractAction implements ListSelectionListener{
	private MultipleAlignmentPanelExtension alignmentPanel;
	public ActionSequencesShowSelected(MultipleAlignmentPanelExtension alignmentPanel) {
		super("Show selected");
		this.alignmentPanel = alignmentPanel;
		this.alignmentPanel.getAlignmentView().getSequenceSelectionModel().addListSelectionListener(this);
		setEnabled(!this.alignmentPanel.getAlignmentView().getSequenceSelectionModel().isSelectionEmpty());
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		alignmentPanel.showSelectedSequences();
	}

	@Override
	public void valueChanged(ListSelectionEvent e) {
		setEnabled(!this.alignmentPanel.getAlignmentView().getSequenceSelectionModel().isSelectionEmpty());
	}
	
}
