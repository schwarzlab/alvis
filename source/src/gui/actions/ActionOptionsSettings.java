/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.actions;

import alvis.AlvisSettings;
import com.general.gui.dialog.DialogFactory;
import gui.SettingsPanel;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;

/**
 *
 * @author Roland Schwarz <rfs32@cam.ac.uk>
 */
public class ActionOptionsSettings extends AbstractAction{
	private final AlvisSettings settings;
	public ActionOptionsSettings(AlvisSettings settings){
		super("Settings...");
		this.settings=settings;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		SettingsPanel panel = new SettingsPanel(settings);
		if (DialogFactory.showModalOKCancelDialog(panel)){
			this.settings.setAll(panel.getSettings());
		}
	}
	
}
