/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.actions;

import alvis.Alvis;
import alvis.AlvisDataModel;
import com.general.gui.dialog.DialogFactory;
import com.general.gui.fileHistory.FileHistoryAction;
import java.awt.event.ActionEvent;
import java.io.IOException;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public class ActionFileOpenHistory extends FileHistoryAction{
	private Alvis alvis;

	public ActionFileOpenHistory(Alvis alvis, int historyIndex) {
		super(historyIndex);
		this.alvis = alvis;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (getFile()==null) return;
		
		try {
			AlvisDataModel loadedModel = AlvisDataModel.load(getFile());
			boolean confirmed=true;
			if (loadedModel.getAlignment()==null) {
				confirmed=DialogFactory.showConfirmDialog("Model file does not include a valid alignment. Continue?");
			}
			if (confirmed) {
				alvis.setDataModel(loadedModel);
			}
		} catch (IOException | ClassNotFoundException ex) {
			DialogFactory.showErrorMessage(ex.getMessage(), "File open error:");
		}
	}
	
}
