/*
 * Copyright (C) 2015 Roland Schwarz <rfs32@cam.ac.uk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package gui.actions;

import alvis.AlvisDataModel;
import alvis.algorithm.ProfileHMMTools;
import alvis.algorithm.matrix.FisherScoresMatrix;
import com.general.gui.dialog.DialogFactory;
import com.general.gui.panels.SelectNameablePanel;
import de.biozentrum.bioinformatik.sequence.Sequence;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileWriter;
import javax.swing.AbstractAction;
import org.biojava.bio.seq.io.SymbolTokenization;
import org.biojava.bio.symbol.FiniteAlphabet;
import org.jblas.DoubleMatrix;

/**
 *
 * @author Roland Schwarz <rfs32@cam.ac.uk>
 */
public class ActionFileExportKernel extends AbstractAction implements PropertyChangeListener{
	private AlvisDataModel dataModel;
	public ActionFileExportKernel(AlvisDataModel dataModel) {
		super("Export Kernel...");
		this.dataModel = dataModel;
		dataModel.addPropertyChangeListener(AlvisDataModel.PROPERTY_KERNELS, this);
		setEnabled(dataModel.getKernelMatrices()!=null && dataModel.getKernelMatrices().size() > 0);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		FisherScoresMatrix dataMatrix=dataModel.getFisherScores();			
		if (dataModel.getKernelMatrices()==null || dataModel.getKernelMatrices().isEmpty()) {
			DialogFactory.showErrorMessage("No Kernels found to export!", "Error");
			return;
		}

		SelectNameablePanel panel = new SelectNameablePanel(dataModel.getKernelMatrices().keySet());
		if (!DialogFactory.showModalOKCancelDialog(panel)) { // cancelled
			return; 
		}
		DoubleMatrix K = dataModel.getKernelMatrix(panel.getSelectedObject());
		if (K==null) return;
		
		File file = DialogFactory.showFileSaveDialog();
		if (file==null) return;

		try (FileWriter writer = new FileWriter(file)) {
			StringBuilder names=new StringBuilder();			
			for (int i=0; i < K.columns; i++) {
				Sequence s = dataModel.getAlignment().getSequence(i);
				names.append(s.getName());
				if (i<(K.columns-1)) {
					names.append(",");
				}
			}
			names.append("\n");
			writer.write(names.toString());
			String Ktxt = K.toString("%.6f", "", "", ",", "\n");
			writer.write(Ktxt);
		} catch (Exception ex) {
			DialogFactory.showErrorMessage(ex.getMessage(), "Error:");
		}
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		setEnabled(dataModel.getKernelMatrices()!=null && dataModel.getKernelMatrices().size() > 0);
	}
	
}
