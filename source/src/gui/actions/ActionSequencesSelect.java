/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.actions;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JList;

/**
 *
 * @author Roland Schwarz <rfs32@cam.ac.uk>
 */
public class ActionSequencesSelect extends AbstractAction{
	private final SelectionType op;
	private final JList list;
	public static enum SelectionType { ALL, NONE };
	
	public ActionSequencesSelect(JList list, SelectionType op) {
		if (op==SelectionType.ALL) {
			putValue(NAME, "Select all");
		} else {
			putValue(NAME, "Deselect all");
		}
		this.op = op;
		this.list = list;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (op==SelectionType.ALL){
			this.list.setSelectionInterval(0, list.getModel().getSize());
		} else {
			this.list.clearSelection();
		}
	}
	
}
