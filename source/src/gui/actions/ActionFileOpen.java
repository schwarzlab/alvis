/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.actions;

import alvis.Alvis;
import alvis.AlvisDataModel;
import com.general.gui.dialog.DialogFactory;
import com.general.gui.fileHistory.FileHistory;
import gui.GUIConstants;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import javax.swing.AbstractAction;
import static javax.swing.Action.ACCELERATOR_KEY;
import javax.swing.KeyStroke;
import javax.swing.filechooser.FileFilter;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public class ActionFileOpen extends AbstractAction{
	private Alvis alvis;
	public ActionFileOpen(Alvis alvis) {
		super("Open...");
		this.alvis=alvis;
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_DOWN_MASK));		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		File chosenFile = DialogFactory.showSingleFileOpenDialog(new FileFilter[] {GUIConstants.ALVIS_FILE_FILTER});
		if (chosenFile!=null) {
			try {
				AlvisDataModel loadedModel = AlvisDataModel.load(chosenFile);
				boolean confirmed=true;
				if (loadedModel.getAlignment()==null) {
					confirmed=DialogFactory.showConfirmDialog("Model file does not include a valid alignment. Continue?");
				}
				if (confirmed) {
					alvis.setDataModel(loadedModel);
				}
				FileHistory.getInstance().registerFile(chosenFile);
			} catch (IOException | ClassNotFoundException ex) {
				DialogFactory.showErrorMessage(ex.getMessage(), "File save error:");
			}
		}


	}
	
}
