/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.actions;

import alvis.AlvisDataModel;
import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.AbstractAction;
import javax.swing.JCheckBoxMenuItem;
import org.phylowidget.alvis.PhyloWidgetWrapper;
/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public class ActionViewTree extends AbstractAction implements WindowListener, PropertyChangeListener{
	private final PhyloWidgetWrapper phyloWidget;
	private final AlvisDataModel dataModel;
	
	public ActionViewTree(AlvisDataModel model, PhyloWidgetWrapper phyloWidget) {
		super("Tree");
		this.phyloWidget=phyloWidget;
		this.dataModel = model;
		putValue(SELECTED_KEY, phyloWidget!=null && phyloWidget.isVisible());
		this.dataModel.addPropertyChangeListener(AlvisDataModel.PROPERTY_TREE, this);
		setEnabled(this.dataModel.getTree()!=null);
		phyloWidget.addWindowListener(this);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		boolean show=true;
		if (e.getSource() instanceof JCheckBoxMenuItem) {
			JCheckBoxMenuItem mnu = (JCheckBoxMenuItem)e.getSource();
			show = mnu.isSelected();
		}
		phyloWidget.setVisible(show);
		phyloWidget.getUI().zoomToFull();
	}

	
	@Override
	public void windowOpened(WindowEvent e) {
		putValue(SELECTED_KEY, true);
	}

	@Override
	public void windowClosing(WindowEvent e) {
		putValue(SELECTED_KEY, false);		
	}

	@Override
	public void windowClosed(WindowEvent e) {
		putValue(SELECTED_KEY, false);
	}

	@Override
	public void windowIconified(WindowEvent e) {
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
	}

	@Override
	public void windowActivated(WindowEvent e) {
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		setEnabled(evt.getNewValue()!=null);		
		if (isEnabled() && (evt.getOldValue()==null || !evt.getOldValue().equals(evt.getNewValue()))){
			// then there's a new tree
			phyloWidget.setVisible(true);
			phyloWidget.getUI().zoomToFull();			
		}
		if (evt.getNewValue()==null) {
			phyloWidget.setVisible(false);
		}
	}
	
}
