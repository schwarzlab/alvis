/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui.actions;

import alvis.Alvis;
import alvis.AlvisDataModel;
import alvis.algorithm.ParseAlignmentTask;
import alvis.algorithm.AlignSequencesTask;
import com.general.gui.dialog.DialogFactory;
import gui.MainFrame;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.StringReader;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import javax.swing.AbstractAction;
import javax.swing.SwingWorker;

/**
 *
 * @author Roland Schwarz <rfs32@cam.ac.uk>
 */
public class ActionAnalysisAlign extends AbstractAction implements PropertyChangeListener{

	private final AlvisDataModel dataModel;
	private final MainFrame mainFrame;

	public ActionAnalysisAlign(AlvisDataModel dataModel, MainFrame mainFrame) {
		super("Align using WebPrank");
		this.dataModel = dataModel;
		this.mainFrame = mainFrame;
		dataModel.addPropertyChangeListener(AlvisDataModel.PROPERTY_ALIGNMENT, this);
		setEnabled(dataModel.getAlignment()!=null);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		final AlignSequencesTask aligner = new AlignSequencesTask(dataModel.getAlignment());
		mainFrame.addProgressable(aligner.getProgressable());
		aligner.addPropertyChangeListener(new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				if (evt.getPropertyName().equals("state") && evt.getNewValue().equals(SwingWorker.StateValue.DONE)) {
					mainFrame.removeProgressable(aligner.getProgressable());
					processResults(aligner);
				}
			}
		});
		aligner.execute();
	}

	private void processResults(AlignSequencesTask aligner) {
		String fastaString = null;
		try {
			fastaString = aligner.get();
		} catch (ExecutionException ex) {
			String message = "Error aligning sequences!\n";
			String title = "Alignment error:";
			message += "The aligner said:\n";
			message += ex.getCause().getMessage();
			DialogFactory.showErrorMessage(message, title);
		} catch (InterruptedException ex) {
			DialogFactory.showErrorMessage("Task canceled.", "Abort");
		}

		if (fastaString == null) {
			return;
		}

		final ParseAlignmentTask parser = new ParseAlignmentTask(new StringReader(fastaString), Alvis.AlignmentFormat.FASTA, dataModel.getAlignmentType(), null);
		parser.addPropertyChangeListener(new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				if (evt.getPropertyName().equals("state") && evt.getNewValue().equals(SwingWorker.StateValue.DONE)) {
					try {
						ParseAlignmentTask.AlignmentParserResults results = parser.get();
						dataModel.setAlignment(results.alignment);
					} catch (CancellationException ex) {
						DialogFactory.showErrorMessage("Task canceled", "Canceled:");
					} catch (ExecutionException | InterruptedException e) {
						DialogFactory.showErrorMessage(e.getCause().getMessage(), "Error parsing alignment:");
					} 
				}
			}
		});

		parser.execute();

	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		setEnabled(dataModel.getAlignment()!=null);
	}

}
