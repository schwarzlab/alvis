/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.actions;

import alvis.AlvisDataModel;
import alvis.algorithm.BuildTreeTask;
import alvis.algorithm.BuildTreeTask.BuildTreeResults;
import com.general.gui.dialog.DialogFactory;
import com.general.gui.panels.SelectNameablePanel;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.concurrent.ExecutionException;
import javax.swing.AbstractAction;
import javax.swing.SwingWorker;
import org.jblas.DoubleMatrix;

/**
 *
 * @author Roland Schwarz <rfs32@cam.ac.uk>
 */
public class ActionAnalysisBuildTree extends AbstractAction implements PropertyChangeListener{
	private final AlvisDataModel dataModel;
	public ActionAnalysisBuildTree(AlvisDataModel dataModel) {
		super("Build tree...");
		this.dataModel=dataModel;
		this.dataModel.addPropertyChangeListener(AlvisDataModel.PROPERTY_KERNELS, this);
		setEnabled(!this.dataModel.getKernelMatrices().isEmpty());
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		SelectNameablePanel panel = new SelectNameablePanel(dataModel.getKernelMatrices().keySet());
		if (!DialogFactory.showModalOKCancelDialog(panel)) { // cancelled
			return; 
		}
		DoubleMatrix K = dataModel.getKernelMatrix(panel.getSelectedObject());

		if (K!=null){
			final BuildTreeTask task = new BuildTreeTask(dataModel, K);
			task.addPropertyChangeListener(new PropertyChangeListener() {
				@Override
				public void propertyChange(PropertyChangeEvent evt) {
					if (evt.getPropertyName().equals("state") && evt.getNewValue().equals(SwingWorker.StateValue.DONE)){
						processResult(task);
					}
				}
			});
			task.execute();
		}
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		setEnabled(!this.dataModel.getKernelMatrices().isEmpty());
	}

	private void processResult(BuildTreeTask task) {
		try {
			BuildTreeResults results = task.get();
			dataModel.setTree(results.tree);
		} catch (InterruptedException|ExecutionException ex){
			DialogFactory.showErrorMessage(ex.toString(), "Error building tree:");
		}
	}
	
}
