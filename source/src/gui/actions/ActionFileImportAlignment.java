/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui.actions;

import alvis.Alvis.AlignmentFormat;
import alvis.AlvisDataModel;
import alvis.AlvisDataModel.AlignmentType;
import alvis.algorithm.ParseAlignmentTask;
import alvis.algorithm.ParseAlignmentTask.CustomAlphabetAbortedException;
import cama.BioJavaHelper;
import com.general.gui.dialog.DialogFactory;
import gui.GUIConstants;
import gui.MainFrame;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.ExecutionException;
import javax.swing.AbstractAction;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.SwingWorker;
import javax.swing.filechooser.FileFilter;
import org.biojava.bio.seq.DNATools;
import org.biojava.bio.seq.ProteinTools;
import org.biojava.bio.seq.RNATools;
import org.biojava.bio.symbol.FiniteAlphabet;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public class ActionFileImportAlignment extends AbstractAction {

	private final AlvisDataModel dataModel;
	private final MainFrame mainFrame;

	static File currentDirectory = new File("./");

	public ActionFileImportAlignment(AlvisDataModel dataModel, MainFrame mainFrame) {
		super("Import alignment...");
		this.dataModel = dataModel;
		this.mainFrame = mainFrame;
	}

	public void presentException(Exception e) {
		String msg = e.getLocalizedMessage();
		DialogFactory.showErrorMessage(msg, "Error");
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JFileChooser fileChooser = DialogFactory.createFileOpenDialog("Import sequences/alignment...", new FileFilter[]{GUIConstants.FASTA_FILE_FILTER, GUIConstants.CLUSTAL_FILE_FILTER});

		int returnVal = fileChooser.showOpenDialog(mainFrame);

		if (returnVal == JFileChooser.APPROVE_OPTION) {
                        DialogFactory.lastDir = fileChooser.getCurrentDirectory();
			AlignmentFormat format;
			if (fileChooser.getFileFilter().equals(GUIConstants.CLUSTAL_FILE_FILTER)) {
				format = AlignmentFormat.CLUSTAL;
			} else if (fileChooser.getFileFilter().equals(GUIConstants.FASTA_FILE_FILTER)) {
				format = AlignmentFormat.FASTA;
			} else { // for now assume FASTA as the default
				format = AlignmentFormat.FASTA;
			}				

			Object[] possibilities = {"DNA", "RNA", "Amino Acid", "From File"};
			Object option = possibilities[2];

			try {
				FiniteAlphabet alphabet = BioJavaHelper.guessAlphabet(fileChooser.getSelectedFile());
				if (alphabet != null) {
					if (alphabet == RNATools.getRNA()) {
						option = possibilities[1];
					} else if (alphabet == DNATools.getDNA()) {
						option = possibilities[0];
					} else if (alphabet == ProteinTools.getAlphabet()) {
						option = possibilities[2];
					}
				} else {
					option = possibilities[3];
				}
			} catch (IOException e1) {
				DialogFactory.showErrorMessage(e1.getMessage(), "Error!");
				return;
			}

			String s = (String) JOptionPane.showInputDialog(mainFrame, "Choose sequence type", "Sequence type", JOptionPane.PLAIN_MESSAGE, null, possibilities, option);

			//If a string was returned, say so.
			File customAlphabetFile = null;
			AlignmentType type = AlignmentType.DNA;
			if ((s != null) && (s.length() > 0)) {
				if (s.equals("DNA")) {
					type = AlignmentType.DNA;
				} else if (s.equals("RNA")) {
					type = AlignmentType.RNA;
				} else if (s.equals("Amino Acid")) {
					type = AlignmentType.AminoAcid;
				} else if (s.equals("From File")) {
					type = AlignmentType.Custom;
					customAlphabetFile = DialogFactory.showSingleFileOpenDialog();
					if (customAlphabetFile == null) {
						return;
					}
				}

				try {
					final ParseAlignmentTask parser = new ParseAlignmentTask(fileChooser.getSelectedFile(), format, type, customAlphabetFile);
					this.mainFrame.addProgressable(parser.getProgressable());
					parser.addPropertyChangeListener(new PropertyChangeListener() {
						@Override
						public void propertyChange(PropertyChangeEvent evt) {
							if (evt.getPropertyName().equals("state") && evt.getNewValue().equals(SwingWorker.StateValue.DONE)) {
								mainFrame.removeProgressable(parser.getProgressable());
								try {
									ParseAlignmentTask.AlignmentParserResults results = parser.get();
									dataModel.setAlignment(results.alignment);
								} catch (InterruptedException ex) {
									String title = "Invalid file format";
									String message = "Could not open Alignment file.";
									DialogFactory.showErrorMessage(message, title);

								} catch (ExecutionException e) {
									String title, message;
									if (e.getCause() instanceof CustomAlphabetAbortedException){
										title = "Error in custom alphabet file";
										message = "Could not read alphabet file.\nPlease provide the alphabet in the following format:\n"
												+ "A A-Annotation\n"
												+ "B B-Annotation\n"
												+ "....";								
									} else {
										title = "Reading Error";
										message = "Alignment could not be opened.\n Please check if you selected the right format\nand selected/provided the corresponding alphabet.\n";
										message += "The parser said:\n";
										message += e.getMessage();
									}
									DialogFactory.showErrorMessage(message, title);
								}
								
							}
						}
					});

					parser.execute();
				} catch (FileNotFoundException ex) {
					DialogFactory.showErrorMessage(ex.getMessage(), "File not found!");
				}
			} // end if
		} // end if
	} // end method

}
