/*
 * Copyright (C) 2020 rfs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package gui.actions;

import alvis.resources.AlvisResources;
import com.general.gui.SplashScreen;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.AbstractAction;

/**
 *
 * @author rfs
 */
public class ActionHelpAbout extends AbstractAction {
    public ActionHelpAbout() {
        super("About");
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        SplashScreen splash = new SplashScreen(AlvisResources.ALVIS_SPLASH_SMALL);
        splash.setVisible(true);
        splash.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                splash.dispose();
            }
        });
        //JOptionPane pane = new JOptionPane(splash,JOptionPane.PLAIN_MESSAGE,JOptionPane.OK_OPTION,null,new Object[]{"Close"});
        //JDialog dlg=pane.createDialog("About");
        //dlg.setResizable(true);
        //dlg.setModal(false);
        //dlg.setAlwaysOnTop(false);
        //dlg.setVisible(true);            
    }
    
}
