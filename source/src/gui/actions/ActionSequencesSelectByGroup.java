/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.actions;

import com.general.resources.MyJavaResources;
import de.biozentrum.bioinformatik.sequence.Sequence;
import alvis.resources.AlvisResources;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.util.Map;
import javax.swing.AbstractAction;
import javax.swing.ListSelectionModel;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public class ActionSequencesSelectByGroup extends AbstractAction {
	private final int group;
	private final ListSelectionModel list;
	private final Map<Sequence, Integer> sequenceGroups;
	private final Iterable<Sequence> allSequences;

	public ActionSequencesSelectByGroup(Iterable<Sequence> allSequences, Map<Sequence, Integer> sequenceGroups, ListSelectionModel list, int group, Color color) {
		this.sequenceGroups = sequenceGroups;
		this.allSequences = allSequences;
		this.group=group;
		this.list=list;
		putValue(NAME, String.valueOf(group+1));
		putValue(SMALL_ICON, MyJavaResources.createColoredSquare(12, color));	
		setEnabled(this.allSequences!=null);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		int index=0;
		list.setValueIsAdjusting(true);
		for (Sequence seq:allSequences) {
			if (sequenceGroups.get(seq)==group){
				list.addSelectionInterval(index, index);
			}
			index++;			
		}
		list.setValueIsAdjusting(false);
	}
}
