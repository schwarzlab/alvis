/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.actions;

import alvis.Alvis;
import com.general.gui.dialog.DialogFactory;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import javax.swing.AbstractAction;
import static javax.swing.Action.ACCELERATOR_KEY;
import javax.swing.KeyStroke;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public class ActionFileExit extends AbstractAction {
	private Alvis alvis;
    public ActionFileExit(Alvis alvis) {
        super("Exit");
		this.alvis = alvis;
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_X, InputEvent.CTRL_DOWN_MASK));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (DialogFactory.showConfirmDialog("Are you sure you want to quit?")) {
            alvis.exit();
        }
    }
    
}
