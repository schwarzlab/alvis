/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.actions;

import de.biozentrum.bioinformatik.sequence.Sequence;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JList;


/**
 *
 * @author Roland Schwarz <rfs32@cam.ac.uk>
 */
public class ActionSequencesInvertSelection extends AbstractAction{
	private final JList<Sequence> list;
	
	public ActionSequencesInvertSelection(JList<Sequence> list) {
		super("Invert selection");
		this.list = list;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		list.getSelectionModel().setValueIsAdjusting(true);
		for (int i=0;i<list.getModel().getSize();i++){
			if (list.isSelectedIndex(i)){
				list.getSelectionModel().removeSelectionInterval(i, i);
			} else {
				list.getSelectionModel().addSelectionInterval(i, i);
			}
		}
		list.getSelectionModel().setValueIsAdjusting(false);
	}
	
}
