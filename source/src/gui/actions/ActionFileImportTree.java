/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.actions;

import alvis.AlvisDataModel;
import com.general.gui.dialog.DialogFactory;
import gui.GUIConstants;
import java.awt.event.ActionEvent;
import java.io.File;
import javax.swing.AbstractAction;
import javax.swing.filechooser.FileFilter;
import org.phylowidget.PhyloTree;
import org.phylowidget.alvis.PhyloWidgetTools;

/**
 *
 * @author Roland Schwarz <rfs32@cam.ac.uk>
 */
public class ActionFileImportTree extends AbstractAction{
	private final AlvisDataModel dataModel;
	public ActionFileImportTree(AlvisDataModel dataModel) {
		super("Import tree...");
		this.dataModel = dataModel;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		File file = DialogFactory.showSingleFileOpenDialog(new FileFilter[]{GUIConstants.NEWICK_FILE_FILTER});
		if (file!=null){
			PhyloTree tree=null;
			try {
				tree = PhyloWidgetTools.loadTree(file);
			} catch (Exception ex) {
				DialogFactory.showErrorMessage(ex.getMessage(), "Error:");
			}
			dataModel.setTree(tree);			
		}
	}
}
