/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui;

import alvis.aql.ParseException;
import alvis.aql.TokenMgrError;
import alvis.resources.AlvisResources;
import com.general.Range;
import com.general.gui.layers.ValidationLayerUI;
import de.biozentrum.bioinformatik.alignment.FontSizeChooser;
import de.biozentrum.bioinformatik.alignment.MultipleAlignment;
import de.biozentrum.bioinformatik.alignment.MultipleAlignmentPanel;
import de.biozentrum.bioinformatik.alignment.MultipleAlignmentView;
import de.biozentrum.bioinformatik.alignment.events.AlignmentChangedEvent;
import de.biozentrum.bioinformatik.alignment.events.AlignmentListener;
import de.biozentrum.bioinformatik.alignment.events.AlignmentStructureChangedEvent;
import de.biozentrum.bioinformatik.alignment.selection.AlignmentSelectionModel;
import de.biozentrum.bioinformatik.color.ColorChangedEvent;
import de.biozentrum.bioinformatik.color.ColorModel;
import de.biozentrum.bioinformatik.color.ColorModelListener;
import de.biozentrum.bioinformatik.sequence.Sequence;
import de.biozentrum.bioinformatik.sequence.SequenceColorModel;
import gui.actions.ActionSequencesAssignGroup;
import gui.actions.ActionSequencesSetAsReference;
import gui.actions.ActionSequencesUnsetReference;
import gui.search.AQLSequenceSearcher;
import gui.search.RegexpSequenceSearcher;
import gui.search.SequenceSearcher;
import gui.sequencebundle.JSequenceBundle;
import gui.sequencebundle.SequenceBundleColorModel;
import gui.sequencebundle.SequenceBundleResources;
import gui.sequencebundle.event.GridPoint;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.datatransfer.Transferable;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.PatternSyntaxException;
import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JComponent;
import javax.swing.JLayer;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.TransferHandler;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public class MultipleAlignmentPanelExtension extends MultipleAlignmentPanel implements PropertyChangeListener, ColorModelListener{
	public static final int MIN_CHARBOX_SIZE=3;
	SearchPanel searchPanel;
	SequenceListModel listModel = new SequenceListModel();
	JList<Sequence> lstSequences = new JList<>(listModel);
	private Map<Sequence, Integer> sequenceGroups=null;
	private boolean alignmentLinkedToSequenceSelection=true;	
	private SequenceBundleColorModel colorModel;
	JPopupMenu listPopup;
	JMenu mnuAssignGroup;
	FontSizeChooser fontSizeChooser;
	JSequenceBundle sequenceBundle=null;
	
    public MultipleAlignmentPanelExtension() {
        super();
		this.searchPanel = new SearchPanel();
        getSpacerPanel().add(searchPanel, BorderLayout.CENTER);
		getSpacerPanel().setBorder(null);
		getSplitPane().setDividerSize(8);
		addColorChooserAndDoubleClickFunctionality();
		lstSequences.setDragEnabled(true);
		lstSequences.setTransferHandler(new SequenceTransferHandler());
		lstSequences.setCellRenderer(new SequenceListCellRenderer());
		lstSequences.setFixedCellHeight(getAlignmentView().getCharBoxSize().height + 3);
		listPopup = new JPopupMenu("test");
		lstSequences.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (  e.getButton()==MouseEvent.BUTTON3 ) {
					if (lstSequences.isSelectionEmpty()) {
						lstSequences.setSelectedIndex(lstSequences.locationToIndex(e.getPoint()));
					} 
					createContextMenu(e);
				}
			}
		});
		
		getAlignmentView().setSequenceSelectionModel(lstSequences.getSelectionModel());
		lstSequences.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if (!e.getValueIsAdjusting()){
					int minSel = e.getFirstIndex();
					Rectangle bounds = lstSequences.getCellBounds(minSel, minSel);
					bounds.width=1; // don't care about x axis
					if (!lstSequences.getVisibleRect().contains(bounds)){
						Point p = getAlignmentScrollPane().getViewport().getViewPosition();
						p.y  = bounds.y;
						getAlignmentScrollPane().getViewport().setViewPosition(p);
					}
				}
			}
		});
		
		final JScrollPane c = (JScrollPane) super.getNameList().getParent().getParent();
		c.setViewportView(lstSequences);
		this.setBackground(Color.WHITE);
		this.setBorder(null);
//		getAlignmentView().getLogoView().setFont(AlvisResources.DEFAULT_FONT.deriveFont(15.0f));

		this.addMouseWheelListener(new MouseWheelListener() {
			@Override
			public void mouseWheelMoved(MouseWheelEvent e) {
				if (e.isControlDown()) {
					int scrolled = e.getWheelRotation();
					Dimension newSize = new Dimension(getCharBoxSize());
					newSize.width = Math.max(MIN_CHARBOX_SIZE, getCharBoxSize().width + scrolled); 
					setCharBoxSize(newSize);
				} else {
					e.getComponent().getParent().dispatchEvent(e);
				}
			}
		});
    }
    
	private void createContextMenu(MouseEvent e) {
		listPopup.removeAll();
		
		mnuAssignGroup = new JMenu("Assign group...");
		for (int i=0;i<SequenceGroupsPanel.MAX_GROUPS;i++){
			mnuAssignGroup.add(new ActionSequencesAssignGroup(sequenceGroups, lstSequences, i, colorModel.getGroupColors().get(i)));
		}
		
		if (sequenceBundle!=null) {
			listPopup.add(new ActionSequencesUnsetReference(sequenceBundle));
			listPopup.add(new ActionSequencesSetAsReference(lstSequences, getAlignmentView().getAlignment(), sequenceBundle));
		}
		listPopup.add(mnuAssignGroup);
		listPopup.show(lstSequences, e.getX(), e.getY());
	}
	
    private void addColorChooserAndDoubleClickFunctionality() {
        this.getAlignmentView().addMouseListener( new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				MultipleAlignmentView view = (MultipleAlignmentView)e.getSource();
				int seqPos = view.yToSequence( e.getY());
				int sitePos = view.xToColumn( e.getX());
				if (seqPos < 0 || seqPos >= view.getAlignment().rows() || sitePos < 0 || sitePos >= view.getAlignment().length()) {
					return;
				}
				
				if (e.getButton() == MouseEvent.BUTTON1) {
					if (e.getClickCount()==2) {
						selectAllMatchesInSameColumn(seqPos, sitePos);
					} else {
						// make sure if any column in a row in the alignment is selected, the sequence gets selected too.
						if (alignmentLinkedToSequenceSelection) {
							if (!e.isShiftDown() && !e.isControlDown()) {
								lstSequences.getSelectionModel().clearSelection();
							}
							if (getAlignmentView().getSelectionModel().isAnySiteSelected(seqPos, false)) {
								lstSequences.getSelectionModel().addSelectionInterval(seqPos, seqPos);
							} else {
								lstSequences.getSelectionModel().removeSelectionInterval(seqPos, seqPos);
							}
						}
					} 
				} else if (e.getButton() == MouseEvent.BUTTON3) {
					char c = view.getAlignment().getSequence( seqPos).charAt( sitePos);
					SequenceColorModel colorModel = view.getSequenceColorModel();
					Color newColor = JColorChooser.showDialog(
					getParent(),
					"Choose Character Color",
					colorModel.getColorForChar( c));
					if (newColor != null)
						colorModel.setColorForChar( newColor, c);
				}
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				if (!alignmentLinkedToSequenceSelection) return;
				if (e.getButton()!=MouseEvent.BUTTON1) return;
				
				// after dragging a rectangle
				Point from = getAlignmentView().getStartDrag();
				Point to = getAlignmentView().getEndDrag();
				if (to==null) to=from;
				GridPoint gFrom = new GridPoint(getAlignmentView().xToColumn(from.x), getAlignmentView().yToSequence(from.y));
				GridPoint gTo = new GridPoint(getAlignmentView().xToColumn(to.x), getAlignmentView().yToSequence(to.y));
				GridPoint.GridRectangle r = GridPoint.enclosingRectangle(new GridPoint[]{gFrom, gTo});
				if (!e.isShiftDown() && !e.isControlDown()) {
					lstSequences.getSelectionModel().clearSelection();
				}
				for (int row=r.p1.y;row<=r.p2.y;row++) {
					if (row<0 || row>=getAlignmentView().getAlignment().rows()) continue;
					
					if (getAlignmentView().getSelectionModel().isAnySiteSelected(row, false)) {
						lstSequences.getSelectionModel().addSelectionInterval(row, row);
					} else {
						lstSequences.getSelectionModel().removeSelectionInterval(row, row);
					}
				}
			}
			
			
        });
    }

	public void setCharBoxSize(Dimension size) {
		getAlignmentView().setCharBoxSize(size);
		if (getAlignmentView().getAlignment()!=null) {
			getAlignmentView().sizeToFit();
		}
		if (this.lstSequences!=null) {
			this.lstSequences.setFixedCellHeight(size.height + 3);
		}
	}
	
	public Dimension getCharBoxSize() {
		return getAlignmentView().getCharBoxSize();
	}
	
	@Override
	public void setFont(Font font) {
		super.setFont(font);
		if (this.lstSequences!=null) {
			this.lstSequences.setFixedCellHeight(getAlignmentView().getCharBoxSize().height + 3);
			this.lstSequences.setFont(font);
		}
	}
	
	@Override
	public void setAlignment(MultipleAlignment alignment) {
//		getAlignmentView().getSelectionModel().deselectAll();		
		super.setAlignment(alignment); 
		searchPanel.setSequenceSearcher(new AQLSequenceSearcher(alignment.getSequences(), this.getVisibilityModel()));
		lstSequences.clearSelection();
		listModel.setModel(alignment);
	}

		/** If a cell is selected in the AlignmentSelection model all other cells that have
	 * the same character in the affected columns need to be selected as well. That's what
	 * this method does.
	 * 
	 * @param sel the rectangle in which the AlignmentSelectionModel changed
	 */
	private void selectAllMatchesInSameColumn(int i, int j) {
		AlignmentSelectionModel asm = getAlignmentView().getSelectionModel();
		asm.begin();
		lstSequences.getSelectionModel().setValueIsAdjusting(true);
		char letter = getAlignmentView().getAlignment().characterAt(i, j);
		boolean select = !asm.isSelected(i, j, false);
		for (int row=0;row<getAlignmentView().getAlignment().rows();row++){
			if (getAlignmentView().getAlignment().characterAt(row, j)==letter) {
				if (select) {
					asm.select(row, j,false);
					if (alignmentLinkedToSequenceSelection) {
						lstSequences.getSelectionModel().addSelectionInterval(row, row);
					}
				} else {
					asm.deselect(row, j);
					if (alignmentLinkedToSequenceSelection && !asm.isAnySiteSelected(row, false)) {
						lstSequences.getSelectionModel().removeSelectionInterval(row, row);
					}
				}
			}
		}
		lstSequences.getSelectionModel().setValueIsAdjusting(false);
		getAlignmentView().getSelectionModel().end();
	}

	@Override
	public JList getNameList(){
		return lstSequences;
	}

	/**
	 * @return the sequenceGroups
	 */
	public Map<Sequence, Integer> getSequenceGroups() {
		return sequenceGroups;
	}

	/**
	 * @param sequenceGroups the sequenceGroups to set
	 * @param groupColors
	 */
	public void setSequenceGroups(Map<Sequence, Integer> sequenceGroups) {
		this.sequenceGroups=sequenceGroups;
		lstSequences.setCellRenderer(new SequenceListCellRenderer(this.sequenceGroups, this.colorModel));
		lstSequences.revalidate();
		lstSequences.repaint();
	}
	
	public void setColorModel(SequenceBundleColorModel colorModel) {
		if (this.colorModel!=null) {
			this.colorModel.removePropertyChangeListener(this);
			this.colorModel.getGroupColors().removeColorModelListener(this);
		}
		this.colorModel = colorModel;
		if (this.colorModel!=null) {
			this.colorModel.addPropertyChangeListener(this);
			this.colorModel.getGroupColors().addColorModelListener(this);
		}
		lstSequences.setCellRenderer(new SequenceListCellRenderer(sequenceGroups, this.colorModel));		
		lstSequences.revalidate();
		lstSequences.repaint();
		getAlignmentView().setSequenceColorModel(colorModel.getAlphabetColors());
	}

	/**
	 * @return the alignmentLinkedToSequenceSelection
	 */
	public boolean isAlignmentLinkedToSequenceSelection() {
		return alignmentLinkedToSequenceSelection;
	}

	/**
	 * @param alignmentLinkedToSequenceSelection the alignmentLinkedToSequenceSelection to set
	 */
	public void setAlignmentLinkedToSequenceSelection(boolean alignmentLinkedToSequenceSelection) {
		this.alignmentLinkedToSequenceSelection = alignmentLinkedToSequenceSelection;
	}

	public JSequenceBundle getSequenceBundle() {
		return sequenceBundle;
	}

	public void setSequenceBundle(JSequenceBundle sequenceBundle) {
		this.sequenceBundle = sequenceBundle;
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if (evt.getPropertyName().equals(SequenceBundleColorModel.PROP_GROUPCOLORS)) {
			((ColorModel)evt.getOldValue()).removeColorModelListener(this);
			((ColorModel)evt.getNewValue()).addColorModelListener(this);
		}
		lstSequences.setCellRenderer(new SequenceListCellRenderer(this.sequenceGroups, this.colorModel));
		lstSequences.invalidate();
		lstSequences.repaint();
	}

	@Override
	public void colorModelChanged(ColorChangedEvent e) {
		lstSequences.setCellRenderer(new SequenceListCellRenderer(this.sequenceGroups, this.colorModel));
		lstSequences.invalidate();
		lstSequences.repaint();
	}
	
    private class SearchPanel extends JPanel implements KeyListener{
        private JTextField txtSearch = new JTextField("Enter search string");
		private SequenceSearcher sequenceSearcher=new RegexpSequenceSearcher(new ArrayList<Sequence>());
		private JPanel panOptions = new JPanel(new FlowLayout(FlowLayout.LEADING, 0, 0));
		private JCheckBox chkSubstring = new JCheckBox("Substring");
		private ValidationLayerUI layerUI = new ValidationLayerUI();
		
		//private JLabel lblSearch = new JLabel("Search:");
		
        SearchPanel(){
            super();
            this.setLayout(new BorderLayout(0, 0));
            this.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
			panOptions.setBorder(BorderFactory.createEmptyBorder(0,0,2,0));
			panOptions.setBackground(Color.WHITE);
			this.setBackground(Color.WHITE);
            this.add(new JLayer(panOptions, layerUI), BorderLayout.NORTH);			
			this.add(txtSearch, BorderLayout.CENTER);
			txtSearch.addFocusListener(new FocusAdapter() {
				@Override
				public void focusGained(FocusEvent e) {
					txtSearch.selectAll();
				}
			});
            txtSearch.requestFocus();			
			txtSearch.addKeyListener(this);
			txtSearch.setBorder(BorderFactory.createLineBorder(GUIConstants.BORDER_COLOR,1));
			panOptions.add(chkSubstring);			
			chkSubstring.setBorder(null);
			chkSubstring.setSelected(true);
			chkSubstring.addItemListener(new ItemListener() {
				@Override
				public void itemStateChanged(ItemEvent e) {
					sequenceSearcher.setMatchingSubstring(chkSubstring.isSelected());
				}
			});
        }

        @Override
        public void keyTyped(KeyEvent e) {
        }

        @Override
        public void keyPressed(KeyEvent e) {
            
        }

        @Override
        public void keyReleased(KeyEvent e) {
            String searchString = txtSearch.getText().trim();
			try {
				if (!"".equals(searchString)){
					getSequenceSearcher().setSearchString(searchString);
				}
				layerUI.setValid(true);
				txtSearch.setToolTipText(null);
				panOptions.revalidate();
				panOptions.repaint();
			} catch (ParseException | PatternSyntaxException | TokenMgrError ex) {
				layerUI.setValid(false);
				txtSearch.setToolTipText(ex.getMessage());
				panOptions.revalidate();
				panOptions.repaint();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			
			if (e.getKeyCode()==KeyEvent.VK_ENTER) {
				int[] result = getSequenceSearcher().search();
				// set selected sequences
				JList list = getNameList();
				ListSelectionModel listModel = list.getSelectionModel();
				listModel.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
				listModel.setValueIsAdjusting(true);
				listModel.clearSelection();
				
				AlignmentSelectionModel alignmentSelection = getAlignmentView().getSelectionModel();
				alignmentSelection.deselectAll();
				
				// select the results from the search
				for (int i=0; i<result.length; i++) {
					listModel.addSelectionInterval(result[i], result[i]);
					// set selected alignment areas
					List<Range<Integer>> sequenceHits = getSequenceSearcher().getSequenceHits().get(result[i]);
					for (Range<Integer> hit:sequenceHits) {
						alignmentSelection.select(new Rectangle(hit.getFrom(), result[i], hit.getTo()-hit.getFrom(), 1), true);
					}
				}
				listModel.setValueIsAdjusting(false);
			} else if (e.getKeyCode()== KeyEvent.VK_ESCAPE) { // clear selection
				getNameList().clearSelection();
				getAlignmentView().getSelectionModel().deselectAll();
			} 
        }

		/**
		 * @return the searcher
		 */
		public SequenceSearcher getSequenceSearcher() {
			return sequenceSearcher;
		}

		/**
		 * @param searcher the searcher to set
		 */
		public void setSequenceSearcher(SequenceSearcher searcher) {
			this.sequenceSearcher = searcher;
		}

    }
	private class SequenceListModel extends DefaultListModel<Sequence> implements AlignmentListener {
		private MultipleAlignment model;
		public SequenceListModel(MultipleAlignment model){
			super();
			this.model=model;
			model.addAlignmentListener(this);
		}
		public SequenceListModel() {
			super();
		}
		
		public void setModel( MultipleAlignment model ) {
			this.model = model;
			model.addAlignmentListener( this);
			fireContentsChanged( this, 0, model.getLength() );
		}
		
		public int getSize() {
			if (model == null)
				return 0;
			return model.rows();
		}
 
		public Sequence getElementAt(int index) {
			return model.getSequence(index);
		}

		public void alignmentChanged(AlignmentChangedEvent e) {
			// TODO Auto-generated method stub
			
		}

		public void alignmentStructureChanged(AlignmentStructureChangedEvent e) {
			fireContentsChanged( this, 0, model.getLength() );
		}

	}
	
	private class SequenceTransferHandler extends TransferHandler {
		int seqCount=0;
		
		@Override
		public int getSourceActions(JComponent c) {
			return COPY;
		}

		@Override
		public Transferable createTransferable(JComponent c) {
			JList<Sequence> list = (JList<Sequence>)c;
			List<Sequence> values = list.getSelectedValuesList();
			seqCount=values.size();
			return new TransferableSequences(values);
		}

		@Override
		public void exportDone(JComponent c, Transferable t, int action) {
			seqCount=0;
		}

		@Override
		public Image getDragImage() {
			return AlvisResources.createDnDIcon(seqCount);
		}
		
		
	}
}
