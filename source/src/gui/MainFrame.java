/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui;

import alvis.Alvis;
import alvis.AlvisDataModel;
import alvis.resources.AlvisResources;
import com.general.containerModel.CollectionEvent;
import com.general.containerModel.CollectionListener;
import com.general.containerModel.Nameable;
import com.general.containerModel.impl.MapListenerDecorator;
import com.general.gui.controls.JStatusBar;
import com.general.gui.controls.StayOpenCheckBoxMenuItem;
import com.general.gui.dialog.DialogFactory;
import com.general.gui.fileHistory.FileHistory;
import com.general.gui.progress.ProgressView;
import com.general.gui.progress.Progressable;
import com.general.resources.MyJavaResources;
import de.biozentrum.bioinformatik.alignment.MultipleAlignment;
import de.biozentrum.bioinformatik.alignment.MultipleAlignmentView;
import de.biozentrum.bioinformatik.sequence.Sequence;
import de.biozentrum.bioinformatik.sequence.SequenceAlphabet;
import de.biozentrum.bioinformatik.sequence.SequenceColorModel;
import gui.actions.ActionFileExit;
import gui.actions.ActionFileExportFisherScores;
import gui.actions.ActionFileExportGroups;
import gui.actions.ActionFileExportTree;
import gui.actions.ActionFileImportColumnAnnotations;
import gui.actions.ActionFileImportTree;
import gui.actions.ActionFileNew;
import gui.actions.ActionFileOpen;
import gui.actions.ActionFileOpenHistory;
import gui.actions.ActionFileSave;
import gui.actions.ActionFileSaveAs;
import gui.actions.ActionFileImportAlignment;
import gui.actions.ActionOptionsSettings;
import gui.actions.ActionAnalysisAlign;
import gui.actions.ActionSequencesAssignGroup;
import gui.actions.ActionAnalysisClusterSequences;
import gui.actions.ActionAnalysisComputeAlignmentKernel;
import gui.actions.ActionAnalysisComputeFisherKernel;
import gui.actions.ActionSequencesHideSelected;
import gui.actions.ActionSequencesInvertSelection;
import gui.actions.ActionSequencesSelect;
import gui.actions.ActionSequencesSelectByGroup;
import gui.actions.ActionSequencesShowAll;
import gui.actions.ActionSequencesShowSelected;
import gui.actions.ActionSequencesSort;
import gui.actions.ActionAnalysisBuildTree;
import gui.actions.ActionAnalysisCA;
import gui.actions.ActionAnalysisIdentifySitesForGrouping;
import gui.actions.ActionAnalysisTrainHMM;
import gui.actions.ActionAnalysisTranslate;
import gui.actions.ActionFileExportKernel;
import gui.actions.ActionHelpAbout;
import gui.actions.ActionViewGroupings;
import gui.actions.ActionViewTree;
import gui.sequencebundle.JSequenceBundle;
import gui.sequencebundle.SequenceBundleConfig;
import gui.sequencebundle.SequenceBundleToolBar;
import gui.sequencebundle.actions.ActionBundleAutoRedraw;
import gui.sequencebundle.actions.ActionBundleClearColumnAnnotations;
import gui.sequencebundle.actions.ActionBundleExport;
import gui.sequencebundle.actions.ActionBundleLegendAAIndex;
import gui.sequencebundle.actions.ActionBundleLegendDefault;
import gui.sequencebundle.actions.ActionBundleOptions;
import gui.sequencebundle.actions.ActionBundleRedraw;
import gui.sequencebundle.actions.ActionBundleShowAllGroups;
import gui.sequencebundle.actions.ActionBundleShowConsensus;
import gui.sequencebundle.actions.ActionBundleShowGroup;
import gui.sequencebundle.actions.ActionBundleShowSelection;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Map;
import java.util.Properties;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;
import org.phylowidget.PhyloTree;
import org.phylowidget.alvis.PhyloWidgetManager;
import org.phylowidget.alvis.PhyloWidgetWrapper;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public final class MainFrame extends javax.swing.JFrame {
	private static final String PROP_WIDTH = "mainFrame.width";
	private static final String PROP_HEIGHT = "mainFrame.height";
	private static final String PROP_X = "mainFrame.x";
	private static final String PROP_Y = "mainFrame.y";
    MultipleAlignmentPanelExtension alignmentPanel;
	SequenceGroupsPanel groupsPanel;
	
    JSequenceBundle bundleView;
	JScrollPane scrBundles;
    JSplitPane splitMain;
    JPanel panMain;
	JStatusBar stbStatus;
	ProgressView progressView;
	StatusBarManager barManager;
	AlignmentPropertyHandler alignmentPropertyHandler;
	SequenceGroupsPropertyHandler sequenceGroupsPropertyHandler;
	BundleConfigPropertyHandler groupColorsPropertyHandler;
	TreePropertyHandler treePropertyHandler;	
	private final JMenuItem[] mnuFileOpenHistory = new JMenuItem[8];
	private final Alvis alvis;
	private PhyloWidgetWrapper phyloWidget;
	
    /**
     * Creates new form MainFrame
     */
    public MainFrame(Alvis alvis) {
		this.alvis = alvis;
		DialogFactory.setDefaultComponent(this);
        initComponents(); // generated code		
		init();
	}

	public void init() {
		setIconImage(AlvisResources.ALVIS_LOGO_32x32.getImage());
		
		Dimension size = getSize();
		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent e) {
				alvis.exit();
			}
		});
		
		this.getContentPane().removeAll();
        myInitComponents(); // my custom code
//		if (isVisible()) {
//			setSize(size);
//		} else {
//			setSize(GUIConstants.DEFAULT_WINDOW_SIZE);
//		}
		repaint();
	}
	
    private void myInitComponents() {
		progressView=new ProgressView(this);
		progressView.getContentPane().setBackground(GUIConstants.BACKGROUND_COLOR);
		progressView.setBorder(BorderFactory.createLineBorder(GUIConstants.BORDER_COLOR));
		panMain = new JPanel(new BorderLayout());
		groupsPanel = new SequenceGroupsPanel();
		JPanel southPanel = new JPanel(new BorderLayout());
		stbStatus = new JStatusBar(10);
		stbStatus.addGlue(3);
		stbStatus.setPaneInfo(JStatusBar.ALL_PANES, JStatusBar.BORDER_STYLE_ETCHED, JStatusBar.AUTO_WIDTH);
		stbStatus.setTooltipBackground(GUIConstants.BACKGROUND_COLOR);
		stbStatus.setTooltipBorder(GUIConstants.DEFAULT_BORDER);
		southPanel.add(stbStatus, BorderLayout.CENTER);
		panMain.add(southPanel, BorderLayout.SOUTH);
        getContentPane().add(panMain);
		progressView.setAnchor(panMain, ProgressView.ANCHOR_BOTTOM | ProgressView.ANCHOR_INSIDE_X | ProgressView.ANCHOR_INSIDE_Y | ProgressView.ANCHOR_RIGHT);
		
		splitMain = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
		splitMain.setResizeWeight(0.8);
		splitMain.setDividerSize(8);
		alignmentPanel = new MultipleAlignmentPanelExtension();
		alignmentPanel.setFont(AlvisResources.DEFAULT_FONT);
		alignmentPanel.getAlignmentView().setLogoHeight(60);  
		alignmentPanel.getSplitPane().addPropertyChangeListener(JSplitPane.DIVIDER_LOCATION_PROPERTY, new PropertyChangeListener(){
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				adjustBundleIndent();
			}
		});

		bundleView = new JSequenceBundle();		
		alignmentPanel.getAlignmentView().addPropertyChangeListener(MultipleAlignmentView.PROP_CHARBOXSIZE, new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				// adjust bundle charBoxSize
				Dimension newval = ((Dimension)evt.getNewValue());
				if (bundleView.getBundleConfig().getCellWidth()-2 != newval.width) {
					bundleView.getBundleConfig().setCellWidth(newval.width + 2);
				}
			}
		});
		alignmentPanel.setSequenceBundle(bundleView);

		splitMain.setBottomComponent(alignmentPanel);

		scrBundles = new JScrollPane(bundleView, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrBundles.getHorizontalScrollBar().setModel(alignmentPanel.getAlignmentScrollPane().getHorizontalScrollBar().getModel());
		scrBundles.setBorder(null);
		splitMain.setTopComponent(scrBundles);
		mnuBundleAutoRedraw.setAction(new ActionBundleAutoRedraw(bundleView));
		mnuBundleShowConsensus.setAction(new ActionBundleShowConsensus(bundleView));
		mnuBundleShowSelection.setAction(new ActionBundleShowSelection(bundleView));
		mnuBundleExport.setAction(new ActionBundleExport(bundleView));
		mnuBundleRedraw.setAction(new ActionBundleRedraw(bundleView));
		mnuBundleLegendDefault.setAction(new ActionBundleLegendDefault(bundleView));
		mnuBundleLegendAAIndex.setAction(new ActionBundleLegendAAIndex(bundleView));
		mnuBundleClearColumnAnnotations.setAction(new ActionBundleClearColumnAnnotations(bundleView));
		mnuFileColumnAnnotations.setAction(new ActionFileImportColumnAnnotations(bundleView));
		mnuOptionsColors.setAction(new ActionBundleOptions(bundleView));
		
		progressView.addModel(bundleView.getProgressModel());		
		
		phyloWidget = PhyloWidgetManager.createPhyloWidgetWrapper();
		phyloWidget.setLocationRelativeTo(null);
		phyloWidget.setSize(800,600);
		
		mnuSequencesSelectAll.setAction(new ActionSequencesSelect(alignmentPanel.getNameList(), ActionSequencesSelect.SelectionType.ALL));
		mnuSequencesDeselectAll.setAction(new ActionSequencesSelect(alignmentPanel.getNameList(), ActionSequencesSelect.SelectionType.NONE));
		
		mnuSequencesAssignToGroup.addMenuListener(new MenuListener() {
			@Override
			public void menuSelected(MenuEvent e) {
				mnuSequencesAssignToGroup.removeAll();
				for (int i=0;i<SequenceGroupsPanel.MAX_GROUPS;i++) {
					mnuSequencesAssignToGroup.add(
							new ActionSequencesAssignGroup(
									alvis.getDataModel().getSequenceGroups(), 
									alignmentPanel.getNameList(), 
									i, 
									alvis.getDataModel().getSequenceBundleConfig().getColorModel().getGroupColors().get(i)));
				}
			}
			@Override
			public void menuDeselected(MenuEvent e) {
			}
			@Override
			public void menuCanceled(MenuEvent e) {
			}
		});

		mnuSequencesSelectByGroup.addMenuListener(new MenuListener() {
			@Override
			public void menuSelected(MenuEvent e) {
				mnuSequencesSelectByGroup.removeAll();				
				for (int i=0;i<SequenceGroupsPanel.MAX_GROUPS;i++) {
					mnuSequencesSelectByGroup.add(
							new ActionSequencesSelectByGroup(
									alvis.getDataModel().getAlignment(), 
									alvis.getDataModel().getSequenceGroups(), 
									alignmentPanel.getAlignmentView().getSequenceSelectionModel(), 
									i, 
									alvis.getDataModel().getSequenceBundleConfig().getColorModel().getGroupColors().get(i)));
				}
			}
			@Override
			public void menuDeselected(MenuEvent e) {
			}
			@Override
			public void menuCanceled(MenuEvent e) {
			}
		});
		
		mnuBundleShowGroup.addMenuListener(new MenuListener() {
			@Override
			public void menuSelected(MenuEvent e) {
				mnuBundleShowGroup.removeAll();	
				StayOpenCheckBoxMenuItem item = new StayOpenCheckBoxMenuItem();
				item.setAction(new ActionBundleShowAllGroups(bundleView));
				mnuBundleShowGroup.add(item);
				for (int i=0;i<SequenceGroupsPanel.MAX_GROUPS;i++) {
					item = new StayOpenCheckBoxMenuItem();
					item.setAction(new ActionBundleShowGroup(bundleView, i));
					mnuBundleShowGroup.add(item);
				}
			}
			@Override
			public void menuDeselected(MenuEvent e) {
			}
			@Override
			public void menuCanceled(MenuEvent e) {
			}
		});
		
		mnuOptionsSettings.setAction(new ActionOptionsSettings(alvis.getSettings()));
		mnuSequencesHideSelected.setAction(new ActionSequencesHideSelected(alignmentPanel));
		mnuSequencesShowSelected.setAction(new ActionSequencesShowSelected(alignmentPanel));
		mnuSequencesShowAll.setAction(new ActionSequencesShowAll(alignmentPanel));
		mnuSequencesInvertSelection.setAction(new ActionSequencesInvertSelection(alignmentPanel.getNameList()));

    }
    
	private void adjustBundleIndent() {
		JSplitPane split = alignmentPanel.getSplitPane();
		int offsetX = split.getDividerLocation() + split.getDividerSize();        
		bundleView.getBundleConfig().setBundleIndent(offsetX);
	}
	
	public void addProgressable(Progressable model) {
		progressView.addModel(model);
	}
	public void removeProgressable(Progressable model){
		progressView.removeModel(model);
	}
	
	private String makeTitle() {
		String title = String.format("%s %s", alvis.Info.PROGRAM_SHORT_NAME, alvis.Info.PROGRAM_VERSION);
		return title;
	}
	/**
	 * This method is called only if the whole data model changes. We cannot process that through normal events
	 * because that would lead to multiple redraws on the sequence bundle which is very expensive.
	 */
	public void dataModelChanged() {
		alvis.getDataModel().addPropertyChangeListener(AlvisDataModel.PROPERTY_FILE, new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				setTitle(makeTitle() + " - " + alvis.getDataModel().getName());
			}
		});	
		setTitle(makeTitle() + " - " + alvis.getDataModel().getName());
		barManager = new StatusBarManager();
		alignmentPropertyHandler = new AlignmentPropertyHandler();
		sequenceGroupsPropertyHandler = new SequenceGroupsPropertyHandler();
		groupColorsPropertyHandler = new BundleConfigPropertyHandler();
		treePropertyHandler = new TreePropertyHandler();
		
		// init menus
        mnuFileExit.setAction(new ActionFileExit(alvis));
        mnuImportAlignment.setAction(new ActionFileImportAlignment(alvis.getDataModel(), this));
		mnuFileOpen.setAction(new ActionFileOpen(alvis));
		mnuFileSaveAs.setAction(new ActionFileSaveAs(alvis));
		mnuFileSave.setAction(new ActionFileSave(alvis));
		mnuFileNew.setAction(new ActionFileNew(alvis));

		mnuFileHistory.removeAll();
		for (int i=0;i<FileHistory.getInstance().size();i++) {
			mnuFileOpenHistory[i]=new JMenuItem(new ActionFileOpenHistory(alvis, i));
			mnuFileHistory.add(mnuFileOpenHistory[i]);
		}
		mnuSequencesAutoAssign.setAction(new ActionAnalysisClusterSequences(alvis.getDataModel(), progressView));
		mnuFileExportFisherScores.setAction(new ActionFileExportFisherScores(alvis.getDataModel()));
		mnuFileExportKernel.setAction(new ActionFileExportKernel(alvis.getDataModel()));
		mnuFileImportTree.setAction(new ActionFileImportTree(alvis.getDataModel()));
		mnuFileExportTree.setAction(new ActionFileExportTree(alvis.getDataModel()));
		mnuFileExportGroups.setAction(new ActionFileExportGroups(alvis.getDataModel()));
		mnuViewTree.setAction(new ActionViewTree(alvis.getDataModel(), phyloWidget));
        mnuViewGroupings.setAction(new ActionViewGroupings(panMain, groupsPanel));		
		mnuToolsBuildTree.setAction(new ActionAnalysisBuildTree(alvis.getDataModel()));
		mnuSequencesAlign.setAction(new ActionAnalysisAlign(alvis.getDataModel(), this));
		mnuToolsRetrainHMM.setAction(new ActionAnalysisTrainHMM(alvis.getDataModel(), this));
		mnuToolsIdentifySitesForGrouping.setAction(new ActionAnalysisIdentifySitesForGrouping(alvis.getDataModel(), progressView, bundleView));
		mnuSequencesSort.setAction(new ActionSequencesSort(alvis.getDataModel(), progressView, phyloWidget));
		mnuSequencesComputeFisherKernel.setAction(new ActionAnalysisComputeFisherKernel(alvis.getDataModel(), this));
		mnuSequencesComputeAlignmentKernel.setAction(new ActionAnalysisComputeAlignmentKernel(alvis.getDataModel(), this));
		mnuAnalysisCA.setAction(new ActionAnalysisCA(alvis.getDataModel(), this));
		mnuAnalysisTranslate.setAction(new ActionAnalysisTranslate(alvis.getDataModel(), this));
                mnuHelpAbout.setAction(new ActionHelpAbout());
		groupsPanel.setSequenceGroups(alvis.getDataModel().getSequenceGroups());		
		groupsPanel.setSequenceBundleColorModel(alvis.getDataModel().getSequenceBundleConfig().getColorModel());		
		alignmentPanel.setSequenceGroups(alvis.getDataModel().getSequenceGroups()); 
		alignmentPanel.setColorModel(alvis.getDataModel().getSequenceBundleConfig().getColorModel());
		phyloWidget.setSequenceGroups(alvis.getDataModel().getSequenceGroups());
		phyloWidget.setColorModel(alvis.getDataModel().getSequenceBundleConfig().getColorModel());
		phyloWidget.setListSelectionModel(alignmentPanel.getNameList().getSelectionModel());
		bundleView.setSequenceGroups(alvis.getDataModel().getSequenceGroups());
		bundleView.setBundleConfig(alvis.getDataModel().getSequenceBundleConfig());
		
		alignmentPropertyHandler.propertyChange(null); // invoke changes specific to a changing alignment
		if (alvis.getDataModel().getAlignment()!=null) {
			panMain.add(splitMain, BorderLayout.CENTER);
		}
		
		if (alvis.getDataModel().getTree()!=null) {
			phyloWidget.setTree(alvis.getDataModel().getTree());
			phyloWidget.update();
			if (phyloWidget.isVisible()){
				phyloWidget.getUI().zoomToFull();
			}
		} 
		
		adjustBundleIndent();
		Dimension newSize = new Dimension(alignmentPanel.getCharBoxSize());
		newSize.width = bundleView.getBundleConfig().getCellWidth() - 2;
		alignmentPanel.setCharBoxSize(newSize);
		
	}
    
	public void toProperties(Properties p) {
		p.setProperty(PROP_WIDTH, String.valueOf(getWidth()));
		p.setProperty(PROP_HEIGHT, String.valueOf(getHeight()));
		p.setProperty(PROP_X, String.valueOf(getX()));
		p.setProperty(PROP_Y, String.valueOf(getY()));
	}
	
	public void fromProperties(Properties p){
		int width,height,x,y;
		try {
			width = Integer.parseInt(p.getProperty(PROP_WIDTH));
		} catch (NumberFormatException ex) {
			width = GUIConstants.DEFAULT_WINDOW_SIZE.width;
		}
		try {
			height = Integer.parseInt(p.getProperty(PROP_HEIGHT));
		} catch (NumberFormatException ex) {
			height = GUIConstants.DEFAULT_WINDOW_SIZE.height;
		}
		try {
			x = Integer.parseInt(p.getProperty(PROP_X));
		} catch (NumberFormatException ex) {
			x = 0;
		}
		try {
			y = Integer.parseInt(p.getProperty(PROP_Y));
		} catch (NumberFormatException ex){
			y = 0;
		}
		
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		setSize(width, height);		
		if (x<screenSize.width-50 && y<screenSize.height-50) {
			setLocation(x, y);			
		} else {
			setLocationRelativeTo(null);
		}
	}
    /**
     * This method is called from within the constructor to initialise the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mnuMain = new javax.swing.JMenuBar();
        mnuFile = new javax.swing.JMenu();
        mnuFileNew = new javax.swing.JMenuItem();
        mnuFileOpen = new javax.swing.JMenuItem();
        mnuFileHistory = new javax.swing.JMenu();
        mnuFileSave = new javax.swing.JMenuItem();
        mnuFileSaveAs = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        mnuImportAlignment = new javax.swing.JMenuItem();
        mnuFileImportTree = new javax.swing.JMenuItem();
        mnuFileColumnAnnotations = new javax.swing.JMenuItem();
        mnuFileExportTree = new javax.swing.JMenuItem();
        mnuFileExportFisherScores = new javax.swing.JMenuItem();
        mnuFileExportKernel = new javax.swing.JMenuItem();
        mnuFileExportGroups = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        mnuFileExit = new javax.swing.JMenuItem();
        mnuSequences = new javax.swing.JMenu();
        mnuSequencesSelectAll = new javax.swing.JMenuItem();
        mnuSequencesSelectByGroup = new javax.swing.JMenu();
        mnuSequencesDeselectAll = new javax.swing.JMenuItem();
        mnuSequencesInvertSelection = new javax.swing.JMenuItem();
        jSeparator4 = new javax.swing.JPopupMenu.Separator();
        mnuSequencesHideSelected = new javax.swing.JMenuItem();
        mnuSequencesShowSelected = new javax.swing.JMenuItem();
        mnuSequencesShowAll = new javax.swing.JMenuItem();
        jSeparator8 = new javax.swing.JPopupMenu.Separator();
        mnuSequencesAssignToGroup = new javax.swing.JMenu();
        jSeparator5 = new javax.swing.JPopupMenu.Separator();
        mnuSequencesSort = new javax.swing.JMenuItem();
        mnuBundle = new javax.swing.JMenu();
        mnuBundleShowGroup = new javax.swing.JMenu();
        mnuBundleShowConsensus = new javax.swing.JCheckBoxMenuItem();
        mnuBundleShowSelection = new javax.swing.JCheckBoxMenuItem();
        mnuBundleLegend = new javax.swing.JPopupMenu.Separator();
        mnuBundleClearColumnAnnotations = new javax.swing.JMenuItem();
        jSeparator10 = new javax.swing.JPopupMenu.Separator();
        jMenu1 = new javax.swing.JMenu();
        mnuBundleLegendDefault = new javax.swing.JMenuItem();
        mnuBundleLegendAAIndex = new javax.swing.JMenuItem();
        jSeparator9 = new javax.swing.JPopupMenu.Separator();
        mnuBundleAutoRedraw = new javax.swing.JCheckBoxMenuItem();
        mnuBundleRedraw = new javax.swing.JMenuItem();
        jSeparator3 = new javax.swing.JPopupMenu.Separator();
        mnuBundleExport = new javax.swing.JMenuItem();
        mnuTools = new javax.swing.JMenu();
        mnuAnalysisTranslate = new javax.swing.JMenuItem();
        mnuSequencesAlign = new javax.swing.JMenuItem();
        mnuToolsBuildTree = new javax.swing.JMenuItem();
        jSeparator7 = new javax.swing.JPopupMenu.Separator();
        mnuToolsRetrainHMM = new javax.swing.JMenuItem();
        mnuSequencesComputeFisherKernel = new javax.swing.JMenuItem();
        mnuSequencesComputeAlignmentKernel = new javax.swing.JMenuItem();
        mnuAnalysisCA = new javax.swing.JMenuItem();
        jSeparator11 = new javax.swing.JPopupMenu.Separator();
        mnuToolsIdentifySitesForGrouping = new javax.swing.JMenuItem();
        mnuSequencesAutoAssign = new javax.swing.JMenuItem();
        mnuView = new javax.swing.JMenu();
        mnuViewGroupings = new javax.swing.JCheckBoxMenuItem();
        mnuViewTree = new javax.swing.JCheckBoxMenuItem();
        mnuOptions = new javax.swing.JMenu();
        mnuOptionsColors = new javax.swing.JMenuItem();
        mnuOptionsSettings = new javax.swing.JMenuItem();
        mnuHelp = new javax.swing.JMenu();
        mnuHelpAbout = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        mnuFile.setText("File");

        mnuFileNew.setText("New");
        mnuFile.add(mnuFileNew);

        mnuFileOpen.setText("Open...");
        mnuFile.add(mnuFileOpen);

        mnuFileHistory.setText("Open recent file...");
        mnuFile.add(mnuFileHistory);

        mnuFileSave.setText("Save...");
        mnuFile.add(mnuFileSave);

        mnuFileSaveAs.setText("Save as...");
        mnuFile.add(mnuFileSaveAs);
        mnuFile.add(jSeparator1);

        mnuImportAlignment.setText("Import Alignment...");
        mnuFile.add(mnuImportAlignment);

        mnuFileImportTree.setText("Import Tree...");
        mnuFile.add(mnuFileImportTree);

        mnuFileColumnAnnotations.setText("Import column annotations...");
        mnuFile.add(mnuFileColumnAnnotations);

        mnuFileExportTree.setText("Export tree...");
        mnuFile.add(mnuFileExportTree);

        mnuFileExportFisherScores.setText("Export Fisher scores...");
        mnuFile.add(mnuFileExportFisherScores);

        mnuFileExportKernel.setText("Export Kernel...");
        mnuFile.add(mnuFileExportKernel);

        mnuFileExportGroups.setText("Export groups");
        mnuFile.add(mnuFileExportGroups);
        mnuFile.add(jSeparator2);

        mnuFileExit.setText("Exit");
        mnuFile.add(mnuFileExit);

        mnuMain.add(mnuFile);

        mnuSequences.setText("Sequences");

        mnuSequencesSelectAll.setText("Select all");
        mnuSequences.add(mnuSequencesSelectAll);

        mnuSequencesSelectByGroup.setText("Select by group");
        mnuSequences.add(mnuSequencesSelectByGroup);

        mnuSequencesDeselectAll.setText("Deselect all");
        mnuSequences.add(mnuSequencesDeselectAll);

        mnuSequencesInvertSelection.setText("Invert selection");
        mnuSequences.add(mnuSequencesInvertSelection);
        mnuSequences.add(jSeparator4);

        mnuSequencesHideSelected.setText("Hide selected sequences");
        mnuSequences.add(mnuSequencesHideSelected);

        mnuSequencesShowSelected.setText("Show selected sequences");
        mnuSequences.add(mnuSequencesShowSelected);

        mnuSequencesShowAll.setText("Show all");
        mnuSequences.add(mnuSequencesShowAll);
        mnuSequences.add(jSeparator8);

        mnuSequencesAssignToGroup.setText("Assign to group");
        mnuSequences.add(mnuSequencesAssignToGroup);
        mnuSequences.add(jSeparator5);

        mnuSequencesSort.setText("Sort...");
        mnuSequencesSort.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuSequencesSortActionPerformed(evt);
            }
        });
        mnuSequences.add(mnuSequencesSort);

        mnuMain.add(mnuSequences);

        mnuBundle.setText("Bundle");

        mnuBundleShowGroup.setText("Show group");
        mnuBundle.add(mnuBundleShowGroup);

        mnuBundleShowConsensus.setText("Show consensus");
        mnuBundle.add(mnuBundleShowConsensus);

        mnuBundleShowSelection.setSelected(true);
        mnuBundleShowSelection.setText("Show selection");
        mnuBundle.add(mnuBundleShowSelection);
        mnuBundle.add(mnuBundleLegend);

        mnuBundleClearColumnAnnotations.setText("Clear column annotations");
        mnuBundle.add(mnuBundleClearColumnAnnotations);
        mnuBundle.add(jSeparator10);

        jMenu1.setText("Legend");

        mnuBundleLegendDefault.setText("Default");
        jMenu1.add(mnuBundleLegendDefault);

        mnuBundleLegendAAIndex.setText("From AAIndex...");
        jMenu1.add(mnuBundleLegendAAIndex);

        mnuBundle.add(jMenu1);
        mnuBundle.add(jSeparator9);

        mnuBundleAutoRedraw.setSelected(true);
        mnuBundleAutoRedraw.setText("Auto redraw");
        mnuBundle.add(mnuBundleAutoRedraw);

        mnuBundleRedraw.setText("Redraw");
        mnuBundle.add(mnuBundleRedraw);
        mnuBundle.add(jSeparator3);

        mnuBundleExport.setText("Export as PNG...");
        mnuBundle.add(mnuBundleExport);

        mnuMain.add(mnuBundle);

        mnuTools.setText("Analysis");

        mnuAnalysisTranslate.setText("Translate");
        mnuTools.add(mnuAnalysisTranslate);

        mnuSequencesAlign.setText("Align...");
        mnuTools.add(mnuSequencesAlign);

        mnuToolsBuildTree.setText("Build tree...");
        mnuTools.add(mnuToolsBuildTree);
        mnuTools.add(jSeparator7);

        mnuToolsRetrainHMM.setText("Retrain HMM...");
        mnuTools.add(mnuToolsRetrainHMM);

        mnuSequencesComputeFisherKernel.setText("Compute Fisher kernel");
        mnuTools.add(mnuSequencesComputeFisherKernel);

        mnuSequencesComputeAlignmentKernel.setText("Compute alignment kernel");
        mnuTools.add(mnuSequencesComputeAlignmentKernel);

        mnuAnalysisCA.setText("Correspondence Analysis");
        mnuTools.add(mnuAnalysisCA);
        mnuTools.add(jSeparator11);

        mnuToolsIdentifySitesForGrouping.setText("Identify sites");
        mnuTools.add(mnuToolsIdentifySitesForGrouping);

        mnuSequencesAutoAssign.setText("Auto assign groups...");
        mnuTools.add(mnuSequencesAutoAssign);

        mnuMain.add(mnuTools);

        mnuView.setText("View");

        mnuViewGroupings.setText("Groupings");
        mnuView.add(mnuViewGroupings);

        mnuViewTree.setSelected(true);
        mnuViewTree.setText("Tree");
        mnuView.add(mnuViewTree);

        mnuMain.add(mnuView);

        mnuOptions.setText("Options");

        mnuOptionsColors.setText("Colors...");
        mnuOptions.add(mnuOptionsColors);

        mnuOptionsSettings.setText("Settings...");
        mnuOptions.add(mnuOptionsSettings);

        mnuMain.add(mnuOptions);

        mnuHelp.setText("Help");

        mnuHelpAbout.setText("About...");
        mnuHelpAbout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuHelpAboutActionPerformed(evt);
            }
        });
        mnuHelp.add(mnuHelpAbout);

        mnuMain.add(mnuHelp);

        setJMenuBar(mnuMain);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void mnuSequencesSortActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuSequencesSortActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_mnuSequencesSortActionPerformed

    private void mnuHelpAboutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuHelpAboutActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_mnuHelpAboutActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu jMenu1;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator10;
    private javax.swing.JPopupMenu.Separator jSeparator11;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JPopupMenu.Separator jSeparator3;
    private javax.swing.JPopupMenu.Separator jSeparator4;
    private javax.swing.JPopupMenu.Separator jSeparator5;
    private javax.swing.JPopupMenu.Separator jSeparator7;
    private javax.swing.JPopupMenu.Separator jSeparator8;
    private javax.swing.JPopupMenu.Separator jSeparator9;
    private javax.swing.JMenuItem mnuAnalysisCA;
    private javax.swing.JMenuItem mnuAnalysisTranslate;
    private javax.swing.JMenu mnuBundle;
    private javax.swing.JCheckBoxMenuItem mnuBundleAutoRedraw;
    private javax.swing.JMenuItem mnuBundleClearColumnAnnotations;
    private javax.swing.JMenuItem mnuBundleExport;
    private javax.swing.JPopupMenu.Separator mnuBundleLegend;
    private javax.swing.JMenuItem mnuBundleLegendAAIndex;
    private javax.swing.JMenuItem mnuBundleLegendDefault;
    private javax.swing.JMenuItem mnuBundleRedraw;
    private javax.swing.JCheckBoxMenuItem mnuBundleShowConsensus;
    private javax.swing.JMenu mnuBundleShowGroup;
    private javax.swing.JCheckBoxMenuItem mnuBundleShowSelection;
    private javax.swing.JMenu mnuFile;
    private javax.swing.JMenuItem mnuFileColumnAnnotations;
    private javax.swing.JMenuItem mnuFileExit;
    private javax.swing.JMenuItem mnuFileExportFisherScores;
    private javax.swing.JMenuItem mnuFileExportGroups;
    private javax.swing.JMenuItem mnuFileExportKernel;
    private javax.swing.JMenuItem mnuFileExportTree;
    private javax.swing.JMenu mnuFileHistory;
    private javax.swing.JMenuItem mnuFileImportTree;
    private javax.swing.JMenuItem mnuFileNew;
    private javax.swing.JMenuItem mnuFileOpen;
    private javax.swing.JMenuItem mnuFileSave;
    private javax.swing.JMenuItem mnuFileSaveAs;
    private javax.swing.JMenu mnuHelp;
    private javax.swing.JMenuItem mnuHelpAbout;
    private javax.swing.JMenuItem mnuImportAlignment;
    private javax.swing.JMenuBar mnuMain;
    private javax.swing.JMenu mnuOptions;
    private javax.swing.JMenuItem mnuOptionsColors;
    private javax.swing.JMenuItem mnuOptionsSettings;
    private javax.swing.JMenu mnuSequences;
    private javax.swing.JMenuItem mnuSequencesAlign;
    private javax.swing.JMenu mnuSequencesAssignToGroup;
    private javax.swing.JMenuItem mnuSequencesAutoAssign;
    private javax.swing.JMenuItem mnuSequencesComputeAlignmentKernel;
    private javax.swing.JMenuItem mnuSequencesComputeFisherKernel;
    private javax.swing.JMenuItem mnuSequencesDeselectAll;
    private javax.swing.JMenuItem mnuSequencesHideSelected;
    private javax.swing.JMenuItem mnuSequencesInvertSelection;
    private javax.swing.JMenuItem mnuSequencesSelectAll;
    private javax.swing.JMenu mnuSequencesSelectByGroup;
    private javax.swing.JMenuItem mnuSequencesShowAll;
    private javax.swing.JMenuItem mnuSequencesShowSelected;
    private javax.swing.JMenuItem mnuSequencesSort;
    private javax.swing.JMenu mnuTools;
    private javax.swing.JMenuItem mnuToolsBuildTree;
    private javax.swing.JMenuItem mnuToolsIdentifySitesForGrouping;
    private javax.swing.JMenuItem mnuToolsRetrainHMM;
    private javax.swing.JMenu mnuView;
    private javax.swing.JCheckBoxMenuItem mnuViewGroupings;
    private javax.swing.JCheckBoxMenuItem mnuViewTree;
    // End of variables declaration//GEN-END:variables

	public MultipleAlignmentPanelExtension getAlignmentPanel() {
		return alignmentPanel;
	}

	private class BundleConfigPropertyHandler implements PropertyChangeListener {

		BundleConfigPropertyHandler() {
			alvis.getDataModel().addPropertyChangeListener(AlvisDataModel.PROPERTY_SEQUENCE_BUNDLE_CONFIG, this);
			alvis.getDataModel().getSequenceBundleConfig().addPropertyChangeListener(new PropertyChangeListener() {
				@Override
				public void propertyChange(PropertyChangeEvent evt) {
					configChanged(evt);
				}
			});
			
		}
		
		@Override
		public void propertyChange(PropertyChangeEvent evt) {
			bundleView.setBundleConfig(alvis.getDataModel().getSequenceBundleConfig());
			groupsPanel.setSequenceBundleColorModel(alvis.getDataModel().getSequenceBundleConfig().getColorModel());
			alignmentPanel.setColorModel(alvis.getDataModel().getSequenceBundleConfig().getColorModel());
			phyloWidget.setColorModel(alvis.getDataModel().getSequenceBundleConfig().getColorModel());
			phyloWidget.update();
			alvis.getDataModel().getSequenceBundleConfig().addPropertyChangeListener(new PropertyChangeListener() {
				@Override
				public void propertyChange(PropertyChangeEvent evt) {
					configChanged(evt);
				}
			});
			
		}
		
		private void configChanged(PropertyChangeEvent evt) {
			switch (evt.getPropertyName()) {
				case SequenceBundleConfig.PROP_COLORMODEL:
					groupsPanel.setSequenceBundleColorModel(alvis.getDataModel().getSequenceBundleConfig().getColorModel());
					alignmentPanel.setColorModel(alvis.getDataModel().getSequenceBundleConfig().getColorModel());
					break;
				case SequenceBundleConfig.PROP_CELLWIDTH:
					int newval = ((Integer)evt.getNewValue());
					Dimension newSize = new Dimension(alignmentPanel.getCharBoxSize());
					newSize.width = newval - 2;
					alignmentPanel.setCharBoxSize(newSize);
					
			}
		}
	}
	
	private class SequenceGroupsPropertyHandler implements PropertyChangeListener, CollectionListener {

		SequenceGroupsPropertyHandler() {
			alvis.getDataModel().addPropertyChangeListener(AlvisDataModel.PROPERTY_SEQUENCE_GROUPS, this);
			MapListenerDecorator.getInstance(alvis.getDataModel().getSequenceGroups()).addCollectionListener(this);
		}

		@Override
		public void propertyChange(PropertyChangeEvent evt) {
			// sequence groups can also change on their own (currently never happens)
			bundleView.setSequenceGroups(alvis.getDataModel().getSequenceGroups());
			groupsPanel.setSequenceGroups(alvis.getDataModel().getSequenceGroups());
			alignmentPanel.setSequenceGroups(alvis.getDataModel().getSequenceGroups());
			phyloWidget.setSequenceGroups(alvis.getDataModel().getSequenceGroups());
			phyloWidget.update();
		}

		private void processEvent(CollectionEvent e) {
			if (!e.getValueIsAdjusting()) {
				Map<Sequence, Integer> groups = alvis.getDataModel().getSequenceGroups();
				bundleView.setSequenceGroups(groups);
				alignmentPanel.setSequenceGroups(groups);
				phyloWidget.setSequenceGroups(groups);
				phyloWidget.update();
				// don't update groupsPanel here, it listens for events by itself
			}
		}

		@Override
		public void itemAdded(CollectionEvent e) {
			processEvent(e);
		}

		@Override
		public void itemRemoved(CollectionEvent e) {
			processEvent(e);		
		}

		@Override
		public void itemChanged(CollectionEvent e) {
			processEvent(e);		
		}

		@Override
		public void collectionChanged(CollectionEvent e) {
			processEvent(e);		
		}
	}

	private class AlignmentPropertyHandler implements PropertyChangeListener{

		AlignmentPropertyHandler() {
			alvis.getDataModel().addPropertyChangeListener(AlvisDataModel.PROPERTY_ALIGNMENT, this);
		}
				
		@Override
		public void propertyChange(PropertyChangeEvent evt) {
			MultipleAlignment newvalue = alvis.getDataModel().getAlignment();
			if (newvalue==null) {
			} else {
				alignmentPanel.setAlignment(newvalue);
				SequenceBundleConfig bundleConfig = bundleView.getBundleConfig();
				
				bundleConfig.setAutomaticRedraw(false);
				bundleView.setAlignment(newvalue);
				bundleView.setReferenceSequence(null);

				// sync selection models
				bundleView.setSequenceSelectionModel(alignmentPanel.getNameList().getSelectionModel()); 
				bundleView.setAlignmentSelectionModel(alignmentPanel.getAlignmentView().getSelectionModel());
				bundleView.setColumnAnnotation(null);
				bundleConfig.setSequenceVisibilityModel(alignmentPanel.getVisibilityModel());
				
				// update color model
				if (newvalue.getAlphabet().equals( SequenceAlphabet.AMINOACIDS ) )
					bundleConfig.getColorModel().getAlphabetColors().putAll(SequenceColorModel.AMINOACID_MODEL);
				else if (newvalue.getAlphabet().equals( SequenceAlphabet.RNA_STRUCTURE ) )
					bundleConfig.getColorModel().getAlphabetColors().putAll(SequenceColorModel.RNA_STRUCTURE_MODEL);
				else 
					bundleConfig.getColorModel().getAlphabetColors().putAll(SequenceColorModel.NUCLEOTIDE_MODEL);
				
				
				panMain.add(splitMain, BorderLayout.CENTER);
				final SequenceBundleToolBar sbtb = bundleView.createSequenceBundleToolBar();
//				sbtb.getSpinModelCellWidth().addChangeListener(new ChangeListener() {
//					@Override
//					public void stateChanged(ChangeEvent e) {
//						int newval = sbtb.getSpinModelCellWidth().getNumber().intValue();
//						alignmentPanel.setCharBoxSize(newval - 2);
//					}
//				});
				BorderLayout l = (BorderLayout) panMain.getLayout();
				Component c = l.getLayoutComponent(BorderLayout.NORTH);
				if (c!=null) {
					panMain.remove(c);
				}
				panMain.add(sbtb, BorderLayout.NORTH);
				
				bundleConfig.setAutomaticRedraw(true);
				bundleView.redraw();
				
				phyloWidget.setSequences(newvalue);
				phyloWidget.update();
			}
			groupsPanel.setSequences(newvalue);			
			splitMain.setDividerLocation(bundleView.getPreferredSize().height + 1);
		}
	}
	
	private class TreePropertyHandler implements PropertyChangeListener {
		public TreePropertyHandler() {
			alvis.getDataModel().addPropertyChangeListener(AlvisDataModel.PROPERTY_TREE, this);
		}
		
		@Override
		public void propertyChange(PropertyChangeEvent evt) {
			phyloWidget.setTree((PhyloTree)evt.getNewValue());
			if (alvis.getDataModel().getAlignment()!=null){
				phyloWidget.setSequences(alvis.getDataModel().getAlignment());
			}
			phyloWidget.setSequenceGroups(alvis.getDataModel().getSequenceGroups());
			phyloWidget.update();
		}
		
	}

	private final class StatusBarManager implements PropertyChangeListener, ListSelectionListener{
		private final static int FIELD_ROW_COUNT=0;
		private final static int FIELD_COL_COUNT=1;
		private final static int FIELD_SEL_COUNT=2;
		private final static int FIELD_KERNEL_COUNT=3;
		private final static int FIELD_TREE=4;
		
		public StatusBarManager() {
			registerListeners();
			update();
		}
		
		public void registerListeners() {
	        alvis.getDataModel().addPropertyChangeListener(AlvisDataModel.PROPERTY_ALIGNMENT, this);
			if (alignmentPanel!=null) {
				alignmentPanel.getNameList().addListSelectionListener(this);			
			}
			alvis.getDataModel().addPropertyChangeListener(AlvisDataModel.PROPERTY_KERNELS, this);
			alvis.getDataModel().addPropertyChangeListener(AlvisDataModel.PROPERTY_TREE, this);			
		}
		
		
		@Override
		public void propertyChange(PropertyChangeEvent evt) {
			update();
		}
		
		@Override
		public void valueChanged(ListSelectionEvent e) {
			update();
		}
		
		public void update() {
			MultipleAlignment newvalue = alvis.getDataModel().getAlignment();
			if (newvalue!=null) {
				stbStatus.setPaneText(FIELD_ROW_COUNT, newvalue.getSequenceCount() + " sequences");
				stbStatus.setPaneText(FIELD_COL_COUNT, newvalue.getLength() + " sites");
				if (alignmentPanel!=null) {
					int selectionCount = alignmentPanel.getNameList().getSelectedIndices().length;
					if (selectionCount>0) {
						stbStatus.setPaneText(FIELD_SEL_COUNT, selectionCount + " sequence" + (selectionCount>1?"s":"") + " selected");
					} else {
						stbStatus.setPaneText(FIELD_SEL_COUNT, "");
					}
				}
			
				// kernel count
				String text;
				String tooltip="";
				Icon theicon;
				int nKernels = alvis.getDataModel().getKernelMatrices().size();
				if (nKernels>0) {
					text =  "kernel" + (nKernels==1?"":"s") + " ("+nKernels+")";
					for (Nameable id : alvis.getDataModel().getKernelMatrices().keySet()) {
						tooltip+="* ";
						tooltip+=id.getName();
						tooltip+="\n";
					}
					theicon = MyJavaResources.OK_16x16;
				} else {
					text = "no kernels";
					tooltip = "Construct a kernel matrix using the 'Analysis' menu!\nThis will allow you to build trees and automatically assign groups.";
					theicon = MyJavaResources.NOTOK_16x16;
				}
				stbStatus.setPaneText(FIELD_KERNEL_COUNT, text);
				stbStatus.setPaneToolTipText(FIELD_KERNEL_COUNT, tooltip.trim());
				stbStatus.setPaneIcon(FIELD_KERNEL_COUNT, theicon);
				
				// tree
				if (alvis.getDataModel().getTree()!=null){
					stbStatus.setPaneText(FIELD_TREE, "tree");
					stbStatus.setToolTipText(null);
					stbStatus.setPaneIcon(FIELD_TREE, MyJavaResources.OK_16x16);
				} else {
					stbStatus.setPaneText(FIELD_TREE, "no tree");
					stbStatus.setPaneToolTipText(FIELD_TREE, "Once you have computed a kernel matrix you can build a tree using the 'Analysis - build tree' menu item!");
					stbStatus.setPaneIcon(FIELD_TREE, MyJavaResources.NOTOK_16x16);
				}
			}			
		}
	}
}
