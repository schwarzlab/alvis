/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui;

import de.biozentrum.bioinformatik.sequence.Sequence;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public class TransferableSequences implements Transferable{
	DataFlavor sequenceFlavor = new DataFlavor(Sequence.class, "Sequence");
	List<Sequence> data;
	public TransferableSequences(Collection<Sequence> sequences){
		 data = new ArrayList<>(sequences.size());
		 data.addAll(sequences);
	}
	public TransferableSequences(Sequence[] sequences){
		data = Arrays.asList(sequences);
	}

	@Override
	public DataFlavor[] getTransferDataFlavors() {
		return new DataFlavor[]{sequenceFlavor, DataFlavor.stringFlavor};
	}

	@Override
	public boolean isDataFlavorSupported(DataFlavor flavor) {
		if (!(flavor.equals(sequenceFlavor)||flavor.equals(DataFlavor.stringFlavor))){
			return false;
		}
		return true;
	}

	@Override
	public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
		if (flavor.equals(sequenceFlavor)) {
			return data;
		} else if (flavor.equals(DataFlavor.stringFlavor)){
			StringBuilder buffer = new StringBuilder();
			for (Sequence s: data){
				buffer.append(s.toString());
				buffer.append("\n");
			}
			return buffer.toString();
		} else {
			throw new UnsupportedFlavorException(flavor);
		}
	}
	
}
