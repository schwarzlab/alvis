/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui;

import de.biozentrum.bioinformatik.sequence.Sequence;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.datatransfer.DataFlavor;
import javax.swing.BorderFactory;
import javax.swing.border.Border;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public class GUIConstants {
	// COLORS
	public static final Color BACKGROUND_COLOR = new Color(194, 224, 255, 200); //Color(44,127,184,100);
	public static final Color BORDER_COLOR = new Color(100, 110, 182, 255);
	public static final Color TEXT_COLOR = new Color(0,0,80,255);
	public static final Color FOREGROUND_COLOR = new Color(0,0,80,255);
	
	
	// DND support
	public static final DataFlavor SEQUENCE_FLAVOR = new DataFlavor(Sequence.class, "Sequence");
	
	public static final FileNameExtensionFilter ALVIS_FILE_FILTER = new FileNameExtensionFilter("ALVIS files", "alvis", "alv");
	public static final FileNameExtensionFilter PNG_FILE_FILTER = new FileNameExtensionFilter("PNG image", "png");
	public static final FileNameExtensionFilter SVG_FILE_FILTER = new FileNameExtensionFilter("SVG image", "svg");
	public static final FileNameExtensionFilter NEWICK_FILE_FILTER = new FileNameExtensionFilter("NEWICK format", "new", "tree");
	public static final FileNameExtensionFilter FASTA_FILE_FILTER = new FileNameExtensionFilter("FASTA", "fas", "fasta", "fa", "fsa");
	public static final FileNameExtensionFilter CLUSTAL_FILE_FILTER = new FileNameExtensionFilter("Clustal format", "aln", "clustal");	
	public static final FileNameExtensionFilter DLL_FILE_FILTER = new FileNameExtensionFilter("DLLs", "dll", "so");

	public static final Border DEFAULT_BORDER = BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(GUIConstants.BORDER_COLOR), BorderFactory.createEmptyBorder(2, 2, 2, 2));
	public static final Dimension DEFAULT_WINDOW_SIZE = new Dimension(1024, 768);
}
