/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui;

import alvis.resources.AlvisResources;
import alvis.algorithm.comparison.SequenceComparatorByName;
import com.general.containerModel.CollectionEvent;
import com.general.containerModel.CollectionListener;
import com.general.containerModel.impl.MapDefaultValueDecorator;
import com.general.containerModel.impl.MapListenerDecorator;
import com.general.gui.IndexedTreeSetListModel;
import com.general.gui.layers.ListButtonLayerUI;
import de.biozentrum.bioinformatik.color.ColorChangedEvent;
import de.biozentrum.bioinformatik.color.ColorModel;
import de.biozentrum.bioinformatik.color.ColorModelListener;
import de.biozentrum.bioinformatik.sequence.Sequence;
import gui.sequencebundle.SequenceBundleColorModel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.FlowLayout;
import java.util.ArrayList;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Point;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import javax.swing.DropMode;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLayer;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JSpinner.DefaultEditor;
import javax.swing.JTextArea;
import javax.swing.JToolTip;
import javax.swing.ListModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.TransferHandler;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import prefuse.util.ui.JCustomTooltip;
/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public class SequenceGroupsPanel extends JPanel implements CollectionListener<Sequence>, PropertyChangeListener, ColorModelListener{
	private static final int INITIAL_GROUPS=4;
	public static final int MAX_GROUPS=7;
	private JPanel panBoxes;
	MapListenerDecorator<Sequence, Integer> sequenceGroups;
	SequenceBundleColorModel colorModel;
	private List<IndexedTreeSetListModel<Sequence>> listModels;
	private List<JList<Sequence>> listComponents;
	private List<Border> listBorders;
	private JSpinner spnGroups;
	private JPanel panTop;
	private JButton cmdClearAll;
	Iterable<Sequence> sequences;
	SpinnerNumberModel numberModel;
	
	public SequenceGroupsPanel() {
		this(null, null, null);
	}
	
	public SequenceGroupsPanel(Iterable<Sequence> sequences, Map<Sequence, Integer> sequenceGroupMap, SequenceBundleColorModel groupColors){
		super(new BorderLayout());
		if (groupColors==null) {
			groupColors = new SequenceBundleColorModel();
		}
		if (sequenceGroupMap==null) {
			sequenceGroupMap = new MapDefaultValueDecorator<>(new HashMap<Sequence, Integer>(), 0);
		}
		if (sequences==null) {
			sequences = new ArrayList<>();
		}
		
		this.sequenceGroups = MapListenerDecorator.getInstance(sequenceGroupMap);
		this.sequenceGroups.addCollectionListener(this);				
		this.colorModel = groupColors;
		this.colorModel.addPropertyChangeListener(this);
		this.colorModel.getGroupColors().addColorModelListener(this);
		this.sequences = sequences;
		this.numberModel = new SpinnerNumberModel(1, 1, MAX_GROUPS, 1);		
		this.numberModel.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				updateNumberOfListBoxes();
			}
		});
		
		
		init();
	}
	
	private void init() {
		this.removeAll();
		this.listModels = new ArrayList<>(MAX_GROUPS);
		for (int i=0;i<MAX_GROUPS;i++) {
			IndexedTreeSetListModel<Sequence> listModel = new IndexedTreeSetListModel<>(new SequenceComparatorByName());
			listModels.add(listModel);
		}
		// now fill the list models
		updateListModels();
		
		this.listComponents = new ArrayList<>(MAX_GROUPS);
		this.listBorders = new ArrayList<>(MAX_GROUPS);
		Border border = BorderFactory.createTitledBorder(BorderFactory.createEmptyBorder(), "Sequence groups:");
		setBorder(border);
		this.spnGroups = new JSpinner(numberModel);		
		((DefaultEditor)spnGroups.getEditor()).getTextField().setEditable(false);
		spnGroups.setEnabled(sequenceGroups!=null);
		panBoxes = new JPanel();
		panBoxes.setLayout(new GridLayout(0,1,0,3));		
		try{
			numberModel.setValue(Collections.max(sequenceGroups.values())+1);
		} catch (NoSuchElementException ex) {
			numberModel.setValue(INITIAL_GROUPS);
		}
		updateNumberOfListBoxes();
		this.add(panBoxes, BorderLayout.CENTER);
		panBoxes.setBackground(Color.WHITE);
		panBoxes.setBorder(null);
		this.setBackground(Color.WHITE);
		panTop = new JPanel(new FlowLayout());
		panTop.add(new JLabel("# Groups:"));
		panTop.add(spnGroups);
		cmdClearAll = new JButton("Clear all");
		cmdClearAll.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				sequenceGroups.setValueIsAdjusting(true);
				for (Sequence s:sequenceGroups.keySet()) {
					sequenceGroups.put(s, 0);
				}
				sequenceGroups.setValueIsAdjusting(false);
			}
		});
		cmdClearAll.setEnabled(sequenceGroups!=null);
		panTop.add(cmdClearAll);
		panTop.setBackground(Color.WHITE);
		this.add(panTop, BorderLayout.NORTH);
		panBoxes.setPreferredSize(panTop.getPreferredSize());
		
	}

	public int getNumberOfGroups() {
		return (Integer)this.numberModel.getValue();
	}
	
	public void setNumberOfGroups(int groups){
		if (groups>(Integer)numberModel.getMaximum()){
			throw new IllegalArgumentException("Max number of groups=" + numberModel.getMaximum());
		}
		if (groups<(Integer)numberModel.getMinimum()){
			throw new IllegalArgumentException("Min number of groups=" + numberModel.getMinimum());
		}
		this.numberModel.setValue(groups);
	}
	
	public void updateNumberOfListBoxes(){
		int numberOfGroups = (Integer)numberModel.getValue();
		while (listComponents.size()<numberOfGroups) {
			addListBox();
		}
		while (listComponents.size()>numberOfGroups) {
			removeListBox();
		}				
	}
	/**
	 * We are maintaining an IndexedTreeSetListModel for every group to speed up index searches
	 * and allow for easy updating of the JLists and the JScrollpane.
	 * These list models are never modified but only change in response to events from the main
	 * group map.
	 */
	private void addListBox() {
		int groupIndex = listComponents.size();
		GridLayout layout = (GridLayout)panBoxes.getLayout();
		layout.setRows(layout.getRows()+1);
		ListModel listModel = listModels.get(groupIndex);
		final SequenceList list = new SequenceList(listModel, groupIndex);
		list.setBorder(null);
		list.setBackground(Color.WHITE);
		list.setTransferHandler(new SequenceTransferHandler());
		list.setDropMode(DropMode.INSERT);
		list.setDragEnabled(true);
		list.setCellRenderer(new SequenceListCellRenderer(sequenceGroups, colorModel));
		ListButtonLayerUI layerui = new ListButtonLayerUI(GUIConstants.BACKGROUND_COLOR);		
		if (listComponents.size()>0) { // default group not editable
			list.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode()==KeyEvent.VK_DELETE){
						removeSelectedItems((JList)e.getSource());
					}
				}
			});
			layerui.setDeleteButtonActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					removeSelectedItems((JList)((JLayer)e.getSource()).getView());
				}
			});
		}
		listComponents.add(list);
		JLayer<JList> layer = new JLayer<>(list, layerui);
		JScrollPane scroll = new JScrollPane(layer);
		scroll.setBorder(createBorder(groupIndex));
		scroll.setViewportBorder(null);
		panBoxes.add(scroll);
		this.revalidate();
	}

	private Border createBorder(int index) {
		String title = "Group " + (index+1) + " (" + listModels.get(index).getSize() + ")";
		Color col = colorModel.getGroupColors().get(index);
		if (col==null) col=Color.BLACK;
		Border border = BorderFactory.createTitledBorder(BorderFactory.createLineBorder(col, 1, false), title);
		return border;
	}
	
	private void updateColors() {
		for (int i=0;i<panBoxes.getComponentCount();i++) {
			Component comp = panBoxes.getComponent(i);
			JScrollPane pane;
			if (comp instanceof JScrollPane) {
				pane = (JScrollPane)comp;
				pane.setBorder(createBorder(i));
				listComponents.get(i).setCellRenderer(new SequenceListCellRenderer(sequenceGroups, colorModel));
				pane.invalidate();
				pane.repaint();
			}
		}
	}

	
	private void removeSelectedItems(JList<Sequence> list) {
		List<Sequence> todel = list.getSelectedValuesList();
		sequenceGroups.setValueIsAdjusting(true);
		for (Sequence s:todel) {
			sequenceGroups.put(s,0);
		}
		sequenceGroups.setValueIsAdjusting(false);
	}
	
	private void removeListBox() {
		if (listComponents.size()==1) { // can't remove default group
			throw new ArrayIndexOutOfBoundsException("Can't remove default group!");
		}
		GridLayout layout = (GridLayout)panBoxes.getLayout();
		layout.setRows(layout.getRows()-1);
		listComponents.remove(listComponents.size()-1);
		panBoxes.remove(panBoxes.getComponentCount()-1);
		this.revalidate();
	}

	// COLLECTION LISTENER EVENTS
	@Override
	public void itemAdded(CollectionEvent<Sequence> e) {
		itemChanged(e);
	}

	@Override
	public void itemRemoved(CollectionEvent<Sequence> e) {
		for (Sequence s:e.getChangedElements()){
			for (int i=0;i<listModels.size();i++){
				listModels.get(i).removeElement(s);
			}
		}
	}

	@Override
	public void itemChanged(CollectionEvent<Sequence> e) {
		for (Sequence s:e.getChangedElements()){
			int group = sequenceGroups.get(s);
			if (group>=(Integer)numberModel.getValue()) {
				numberModel.setValue(group+1);
			}
			for (int i=0;i<listModels.size();i++){
				listModels.get(i).removeElement(s);
			}
			listModels.get(group).addElement(s);
		}
	}

	@Override
	public void collectionChanged(CollectionEvent<Sequence> e) {
		updateListModels();
	}

	/**
	 * @return the sequenceGroups
	 */
	public Map<Sequence, Integer> getSequenceGroups() {
		return sequenceGroups;
	}

	/**
	 * @param sequenceGroups the sequenceGroups to set
	 * @param groupColors
	 */
	public void setSequenceGroups(Map<Sequence, Integer> sequenceGroups) {
		if (this.sequenceGroups!=null) {
			this.sequenceGroups.removeCollectionListener(this);
		}
		this.sequenceGroups = MapListenerDecorator.getInstance(sequenceGroups);
		this.sequenceGroups.addCollectionListener(this);				
		try{
			numberModel.setValue(Collections.max(sequenceGroups.values())+1);
		} catch (NoSuchElementException ex) {
			numberModel.setValue(INITIAL_GROUPS);
		}
		updateListModels();
		updateColors(); // because of the cell renderers which keep references to the groups
	}

	public void setSequenceBundleColorModel(SequenceBundleColorModel colorModel) {
		if (!this.colorModel.equals(colorModel)) {
			this.colorModel.removePropertyChangeListener(this);
			this.colorModel.getGroupColors().removeColorModelListener(this);
			this.colorModel = colorModel;
			this.colorModel.addPropertyChangeListener(this);
			this.colorModel.getGroupColors().addColorModelListener(this);
			updateColors();
		}
	}
	
	/**
	 * @return the colorModel
	 */
	public SequenceBundleColorModel getSequenceBundleColorModel() {
		return colorModel;
	}

	/**
	 * @return the sequences
	 */
	public Iterable<Sequence> getSequences() {
		return sequences;
	}

	/**
	 * @param sequences the sequences to set
	 */
	public void setSequences(Iterable<Sequence> sequences) {
		this.sequences = sequences;
		updateListModels();
	}

	private void updateListModels() {
		for (IndexedTreeSetListModel lm : listModels) {
			lm.clear();
		}
		if (sequences != null) {
			for (Sequence seq: sequences) {
				int group = sequenceGroups.get(seq);
				listModels.get(group).addElement(seq);
			}
		}
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if (evt.getPropertyName().equals(SequenceBundleColorModel.PROP_GROUPCOLORS)) {
			((ColorModel)evt.getOldValue()).removeColorModelListener(this);
			((ColorModel)evt.getNewValue()).addColorModelListener(this);
		}
		updateColors();
	}

	@Override
	public void colorModelChanged(ColorChangedEvent e) {
		updateColors();
	}

	// DRAG AND DROP SUPPORT
	private class SequenceTransferHandler extends TransferHandler {
		int seqCount=0;
		// EXPORT
		@Override
		public int getSourceActions(JComponent c) {
			return MOVE;
		}

		@Override
		public Transferable createTransferable(JComponent c) {
			JList<Sequence> list = (JList<Sequence>)c;
			List<Sequence> values = list.getSelectedValuesList();
			this.seqCount = values.size();
			return new TransferableSequences(values);			
		}

		@Override
		public void exportDone(JComponent c, Transferable t, int action) {
			this.seqCount=0;
		}
		
		// IMPORT
		@Override
		public boolean canImport(TransferSupport supp) {
			// Check for Sequence flavor
			if (!supp.isDataFlavorSupported(GUIConstants.SEQUENCE_FLAVOR)) {
				return false;
			}
			return true;
		}
		
		@Override
		public boolean importData(TransferSupport support) {
			if (!canImport(support)) {
				return false;
			}
			JList<Sequence> target = (JList) support.getComponent();
			IndexedTreeSetListModel<Sequence> targetModel = (IndexedTreeSetListModel) target.getModel();
			int group = listModels.indexOf(targetModel);
			try {
				List<Sequence> data = (List) support.getTransferable().getTransferData(GUIConstants.SEQUENCE_FLAVOR);
				Map<Sequence, Integer> tmpMap = new HashMap<>();
				for (Sequence s:data) {
					tmpMap.put(s, group); // if group is default the map will remove the key instead
				}
				sequenceGroups.putAll(tmpMap); // only raises a single event
			} catch (UnsupportedFlavorException | IOException ex) {
				ex.printStackTrace();
			} 
			return true;
		}

		@Override
		public Image getDragImage() {
			return AlvisResources.createDnDIcon(seqCount);
		}
		
	}
	
	private class SequenceList extends JList<Sequence> implements ListDataListener {
		int index;
		public SequenceList(ListModel<Sequence> model, int index){
			super(model);
			this.index=index;
			this.getModel().addListDataListener(this);
		}
		
		@Override
		public JToolTip createToolTip() {
			JPanel panel = new JPanel(new BorderLayout());
			panel.setOpaque(true);
			panel.setBackground(GUIConstants.BACKGROUND_COLOR);
		
			JTextArea label = new JTextArea(getToolTipText(null));
			label.setEditable(false);
			label.setBackground(new Color(0,0,0,0));
			label.setBorder(GUIConstants.DEFAULT_BORDER);
			
			panel.add(label, BorderLayout.CENTER);
			JCustomTooltip tt = new JCustomTooltip(this, panel, false);
			tt.setOpaque(false);
			return tt;
		}

		@Override
		public Point getToolTipLocation(MouseEvent event) {
			Point p = event.getPoint();
			p.translate(10, 10);
			return p;
		}

		@Override
		public String getToolTipText(MouseEvent event) {
			String text = "Group " + (index+1) + "\n" + getModel().getSize() + " members";
			return text;
		}	

		/**
		 * @return the index
		 */
		public int getIndex() {
			return index;
		}

		/**
		 * @param index the index to set
		 */
		public void setIndex(int index) {
			this.index = index;
		}

		@Override
		public void intervalAdded(ListDataEvent e) {
			updateBorder(e);
		}

		@Override
		public void intervalRemoved(ListDataEvent e) {
			updateBorder(e);
		}

		@Override
		public void contentsChanged(ListDataEvent e) {
			updateBorder(e);
		}
		
		private void updateBorder(ListDataEvent e) {
			JList<Sequence> list = listComponents.get(index);
			Container parent = list.getParent();
			if (!(parent instanceof JScrollPane)) { // try again
				parent = parent.getParent();
			}
			if (!(parent instanceof JScrollPane)) { // try again
				parent = parent.getParent();
			}
			
			if (parent instanceof JScrollPane) { // if still no scrollpane, give up
				JScrollPane pane = (JScrollPane)parent;
				pane.setBorder(createBorder(index));
			}
		}
	}
}
