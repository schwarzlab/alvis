/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.sequencebundle;

import com.general.utils.GUIutils;
import gui.GUIConstants;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import javax.swing.ImageIcon;

/**
 *
 * @author Roland Schwarz <rfs32@cam.ac.uk>
 */
public class SequenceBundleResources {
	public static Font SB_FONT_REGULAR;
	public static Font SB_FONT_ITALIC;
	public static Font SB_FONT_BOLD;
	public static Font SB_FONT_BOLDITALIC;
	public static ImageIcon DNA_128x128;
	public static ImageIcon DNA_64x64;
	public static ImageIcon DNA_48x48;	
	public static ImageIcon DNA_32x32;
	
    // load the resources, if that gets too time consuming remove and put explicitely on a worker thread
    static {
        loadResources();
    }
    
	public static void init(){} // to force loading if desired

	private static void loadResources() {
		SB_FONT_REGULAR = createFont("/res/fonts/notosans/NotoSans-Regular.ttf");
		SB_FONT_BOLD = createFont("/res/fonts/notosans/NotoSans-Bold.ttf");
		SB_FONT_ITALIC = createFont("/res/fonts/notosans/NotoSans-Italic.ttf");
		SB_FONT_BOLDITALIC = createFont("/res/fonts/notosans/NotoSans-BoldItalic.ttf");
		DNA_128x128 = createImageIcon("/res/images/dna-icon_128x128.png");
		DNA_64x64=createImageIcon("/res/images/dna-icon_64x64.png");
		DNA_48x48=createImageIcon("/res/images/dna-icon_48x48.png");
		DNA_32x32=createImageIcon("/res/images/dna-icon_32x32.png");
	}

    protected static ImageIcon createImageIcon(String resource) {
        ImageIcon icon = null;
        URL imageURL = SequenceBundleResources.class.getResource(resource);
        if (imageURL != null) {
            icon = new ImageIcon(imageURL);
        }        
        return icon;
    }
	
	private static Font createFont(String path) {
		Font font;
		try {
		  InputStream is = SequenceBundleResources.class.getResourceAsStream(path);
		  font = Font.createFont(Font.TRUETYPE_FONT, is);
		  font = font.deriveFont(12.0f);
		  GraphicsEnvironment.getLocalGraphicsEnvironment().registerFont(font);
		} catch (FontFormatException | IOException ex) {
		  font = new Font("serif", Font.PLAIN, 24);
		}
		return font;
	}

	public static Image createDnDIcon() {
		return createDnDIcon(0);
	}
	public static Image createDnDIcon(int n){
		int imageWidth = 48;
		int imageHeight = 48;
		BufferedImage img = new BufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g = img.createGraphics();
		GUIutils.setQualityRenderingHints(g);
		g.drawImage(DNA_48x48.getImage(), 0,0, (int)(imageWidth*0.9), (int)(imageHeight*0.9), null);
		if (n>0){
			Font font = SB_FONT_REGULAR.deriveFont(9.0f);
			g.setFont(font);
			g.setColor(GUIConstants.TEXT_COLOR);
			FontMetrics fm = g.getFontMetrics();
			String text = Integer.toString(n);
			text += " seq";
			g.drawString(text, imageWidth - fm.stringWidth(text), imageHeight-2);
		}
		g.dispose();
		return img;
	}
	
}
