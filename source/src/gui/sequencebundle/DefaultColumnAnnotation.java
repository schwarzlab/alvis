/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.sequencebundle;

import com.general.utils.GeneralUtils;
import org.apache.commons.lang3.ArrayUtils;

/**
 *
 * @author Roland Schwarz <rfs32@cam.ac.uk>
 */
public class DefaultColumnAnnotation implements ColumnAnnotation{
	private final double[] columnValues;
	private final int[] ranks;
	
	public DefaultColumnAnnotation(double[] values) {
		this.columnValues = values;
		this.ranks = GeneralUtils.order(this.columnValues);
	}
	public DefaultColumnAnnotation(double[] values, boolean reverseRanks) {
		this.columnValues = values;
		this.ranks = GeneralUtils.order(this.columnValues);
		if (reverseRanks){
			ArrayUtils.reverse(this.ranks);
		}
	}
	
	@Override
	public double[] getColumnValues() {
		return columnValues;
	}

	@Override
	public int[] getRanks() {
		return ranks;
	}
	
}
