/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.sequencebundle;

import java.util.EventObject;

/**
 *
 * @author Roland Schwarz <rfs32@cam.ac.uk>
 */
public class SequenceBundleRendererEvent extends EventObject{
	int startValue=-1;
	int currentValue=-1;
	int endValue=-1;
	
	public SequenceBundleRendererEvent(Object source) {
		super(source);
	}
	
	public SequenceBundleRendererEvent(Object source, int start, int current, int end) {
		super(source);
		this.startValue = start;
		this.endValue = end;
		this.currentValue = current;
	}

	public int getStartValue() {
		return startValue;
	}

	public int getCurrentValue() {
		return currentValue;
	}

	public int getEndValue() {
		return endValue;
	}
}
