/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.sequencebundle;

import com.general.gui.controls.NumericTextField;
import com.general.gui.noia.NoiaIconRepository;
import gui.sequencebundle.actions.ActionBundleShowAllGroups;
import gui.sequencebundle.actions.ActionBundleShowAlphabetOverlay;
import gui.sequencebundle.actions.ActionBundleShowGroup;
import gui.sequencebundle.actions.ActionBundleShowSelection;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * @author Roland Schwarz <rfs32@cam.ac.uk>
 */
public class SequenceBundleToolBar extends JToolBar implements PropertyChangeListener{
	public static final int MIN_COLUMN_WIDTH = 5;
	public static final int MAX_COLUMN_WIDTH = 1000;
	private JButton cmdAllLeft;
	private JButton cmdAllRight;
	private JButton cmdLeft;
	private JButton cmdRight;
	private JButton cmdGoto;
	private NumericTextField txtGoto;
	private final JSequenceBundle bundle;
	private JToggleButton[] cmdShowGroups;
	private JButton cmdColumnAnnotationAllLeft;
	private JButton cmdColumnAnnotationAllRight;
	private JButton cmdColumnAnnotationLeft;
	private JButton cmdColumnAnnotationRight;
	private int currentIndex=0;
	private final PropertyChangeListener bundleConfigListener = new ConfigChangeListener();
	JSpinner spinCellWidth;
	SpinnerNumberModel spinModelCellWidth;
	
	public SequenceBundleToolBar(JSequenceBundle bundle) {
		this.bundle=bundle;
		init();
		this.bundle.addPropertyChangeListener(this);
		this.bundle.getBundleConfig().addPropertyChangeListener(bundleConfigListener);		
	}

	public void update() {
		this.removeAll();
		init();
	}
	
	private void init() {
		cmdAllLeft = new JButton(NoiaIconRepository.createImageIcon(NoiaIconRepository.Images.IMG_16X16_ACTIONS_2LEFTARROW_PNG));
		cmdAllLeft.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				bundle.scrollRectToVisible(new Rectangle(0, 0, 1, 1));
			}
		});
		cmdAllLeft.setToolTipText("Go to beginning");
		cmdAllLeft.setFocusable(false);
		
		cmdAllRight = new JButton(NoiaIconRepository.createImageIcon(NoiaIconRepository.Images.IMG_16X16_ACTIONS_2RIGHTARROW_PNG));
		cmdAllRight.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				bundle.scrollRectToVisible(new Rectangle(bundle.getWidth()-1, 0, 1, 1));
			}
		});
		cmdAllRight.setToolTipText("Go to end");
		cmdAllRight.setFocusable(false);
		
		cmdLeft = new JButton(NoiaIconRepository.createImageIcon(NoiaIconRepository.Images.IMG_16X16_ACTIONS_1LEFTARROW_PNG));
		cmdLeft.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Rectangle rect = bundle.getVisibleRect();
				rect.x-=100;
				rect.width=1;
				bundle.scrollRectToVisible(rect);
			}
		});
		cmdLeft.setToolTipText("Go left");
		cmdLeft.setFocusable(false);
		
		cmdRight = new JButton(NoiaIconRepository.createImageIcon(NoiaIconRepository.Images.IMG_16X16_ACTIONS_1RIGHTARROW_PNG));
		cmdRight.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Rectangle rect = bundle.getVisibleRect();
				rect.x+=rect.width;
				rect.x+=100;
				rect.width=1;
				bundle.scrollRectToVisible(rect);
			}
		});
		cmdRight.setToolTipText("Go right");
		cmdRight.setFocusable(false);
		
		cmdGoto = new JButton("Goto:");
		cmdGoto.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try{
					int column = Integer.valueOf(txtGoto.getText())-1;
					bundle.scrollColumnToCenter(column);
					bundle.getGridSelectionModel().clearSelection();
					bundle.getGridSelectionModel().addColSelectionInterval(column, column);
				} catch (NumberFormatException ex){}
			}
		});
		cmdGoto.setFocusable(false);
		
		txtGoto = new NumericTextField(10);
		txtGoto.setMaximumValue(bundle.getAlignment().getLength());
		txtGoto.setMinimumValue(1);
		txtGoto.setValueType(NumericTextField.INTEGER);
		txtGoto.addKeyListener(new KeyAdapter() {

			@Override
			public void keyReleased(KeyEvent e) {
				if (e.getKeyCode()==KeyEvent.VK_ENTER) {
					cmdGoto.doClick();
				}
			}
			
		});
		
		this.add(cmdAllLeft);
		this.add(cmdLeft);
		this.add(cmdRight);
		this.add(cmdAllRight);
		this.add(new Separator());
		this.add(cmdGoto);
		this.add(txtGoto);
		this.add(new Separator());

		this.cmdShowGroups = new JToggleButton[bundle.bundleRenderer.groupIDs.size()];
		this.add(new JLabel("Show groups: "));
		JButton cmdShowAll = new JButton(new ActionBundleShowAllGroups(bundle));
		cmdShowAll.setFocusable(false);
		this.add(cmdShowAll);
		for (int group:bundle.bundleRenderer.groupIDs) {
			int groupidx = bundle.bundleRenderer.groupIDs.entryIndex(group);
			Action item = new ActionBundleShowGroup(bundle, group);
			cmdShowGroups[groupidx] = new JToggleButton(item);
			cmdShowGroups[groupidx].setFocusable(false);
			this.add(cmdShowGroups[groupidx]);
		}
		this.add(new Separator());
		JToggleButton tb = new JToggleButton(new ActionBundleShowSelection(bundle));
		tb.setFocusable(false);
		this.add(tb);
		JToggleButton tbAlpha = new JToggleButton(new ActionBundleShowAlphabetOverlay(bundle));
		tbAlpha.setFocusable(false);
		this.add(tbAlpha);
		this.add(new Separator());
		
		// zoom options
		JLabel lblCellWidth = new JLabel("Column width: ");
		this.add(lblCellWidth);
		spinModelCellWidth = new SpinnerNumberModel(bundle.getBundleConfig().getCellWidth(), MIN_COLUMN_WIDTH, MAX_COLUMN_WIDTH, 1);
		spinCellWidth = new JSpinner(spinModelCellWidth);
		spinCellWidth.setFocusable(false);
		spinCellWidth.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				bundle.getBundleConfig().setCellWidth(spinModelCellWidth.getNumber().intValue());
			}
		});
		this.add(spinCellWidth);
		// end zoom options
		
		this.add(new Separator());
		// column annotation navigation controls
		cmdColumnAnnotationAllLeft = new JButton(NoiaIconRepository.createImageIcon(NoiaIconRepository.Images.IMG_16X16_ACTIONS_2LEFTARROW_PNG));
		cmdColumnAnnotationAllLeft.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				currentIndex = 0;
				selectAndShowColumnByCurrentIndex();
			}
		});
		cmdColumnAnnotationAllLeft.setToolTipText("Go to lowest value");
		cmdColumnAnnotationAllLeft.setFocusable(false);
		
		cmdColumnAnnotationAllRight = new JButton(NoiaIconRepository.createImageIcon(NoiaIconRepository.Images.IMG_16X16_ACTIONS_2RIGHTARROW_PNG));
		cmdColumnAnnotationAllRight.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				currentIndex = bundle.columnAnnotation.getRanks().length;
				double value;
				do {
					currentIndex-=1;
					int column = bundle.columnAnnotation.getRanks()[currentIndex];
					value = bundle.columnAnnotation.getColumnValues()[column];
				} while(currentIndex>=0 && Double.isNaN(value));
				selectAndShowColumnByCurrentIndex();			
			}
		});
		cmdColumnAnnotationAllRight.setToolTipText("Go to highest value");
		cmdColumnAnnotationAllRight.setFocusable(false);
		
		cmdColumnAnnotationLeft = new JButton(NoiaIconRepository.createImageIcon(NoiaIconRepository.Images.IMG_16X16_ACTIONS_1LEFTARROW_PNG));
		cmdColumnAnnotationLeft.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (currentIndex>0) {
					currentIndex-=1;
				}
				selectAndShowColumnByCurrentIndex();
			}
		});
		cmdColumnAnnotationLeft.setToolTipText("Next lower value");
		cmdColumnAnnotationLeft.setFocusable(false);
		
		cmdColumnAnnotationRight = new JButton(NoiaIconRepository.createImageIcon(NoiaIconRepository.Images.IMG_16X16_ACTIONS_1RIGHTARROW_PNG));
		cmdColumnAnnotationRight.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (currentIndex<(bundle.columnAnnotation.getRanks().length-1)) {
					int lookaheadColumn = bundle.columnAnnotation.getRanks()[currentIndex+1];
					double lookaheadValue = bundle.columnAnnotation.getColumnValues()[lookaheadColumn];
					if (!Double.isNaN(lookaheadValue)) {
						currentIndex+=1;
					}
				}
				selectAndShowColumnByCurrentIndex();
			}
		});
		cmdColumnAnnotationRight.setToolTipText("Next higher value");
		cmdColumnAnnotationRight.setFocusable(false);

		cmdColumnAnnotationAllLeft.setVisible(bundle.columnAnnotation!=null);
		cmdColumnAnnotationAllRight.setVisible(bundle.columnAnnotation!=null);
		cmdColumnAnnotationLeft.setVisible(bundle.columnAnnotation!=null);
		cmdColumnAnnotationRight.setVisible(bundle.columnAnnotation!=null);

		this.add(cmdColumnAnnotationAllLeft);
		this.add(cmdColumnAnnotationLeft);
		this.add(cmdColumnAnnotationRight);
		this.add(cmdColumnAnnotationAllRight);
		this.add(new Separator());
		
	}

	private void selectAndShowColumnByCurrentIndex() {
		bundle.gridSelectionModel.getColSelectionModel().clearSelection();		
		int column = bundle.columnAnnotation.getRanks()[currentIndex];
		bundle.gridSelectionModel.addColSelectionInterval(column, column);
		bundle.scrollColumnToCenter(column);
	}
	
	
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if (evt.getPropertyName().equals(JSequenceBundle.PROP_COLUMN_ANNOTATION)) {
			if (evt.getNewValue()==null) {
				cmdColumnAnnotationAllLeft.setVisible(false);
				cmdColumnAnnotationAllRight.setVisible(false);
				cmdColumnAnnotationLeft.setVisible(false);
				cmdColumnAnnotationRight.setVisible(false);
			} else {
				if (evt.getOldValue()==null) {
					cmdColumnAnnotationAllLeft.setVisible(true);
					cmdColumnAnnotationAllRight.setVisible(true);
					cmdColumnAnnotationLeft.setVisible(true);
					cmdColumnAnnotationRight.setVisible(true);
				}
			}
		} else if (evt.getPropertyName().equals(JSequenceBundle.PROP_BUNDLECONFIG)) {
			if (evt.getOldValue()!=null) {
				((SequenceBundleConfig)evt.getOldValue()).removePropertyChangeListener(bundleConfigListener);
			}
			((SequenceBundleConfig)evt.getNewValue()).addPropertyChangeListener(bundleConfigListener);
		}
	}

	public JSpinner getSpinCellWidth() {
		return spinCellWidth;
	}

	public SpinnerNumberModel getSpinModelCellWidth() {
		return spinModelCellWidth;
	}
	
	private class ConfigChangeListener implements PropertyChangeListener {
		@Override
		public void propertyChange(PropertyChangeEvent evt) {
			 if (evt.getPropertyName().equals(SequenceBundleConfig.PROP_CELLWIDTH)) {
				 spinCellWidth.setValue(bundle.getBundleConfig().getCellWidth());
			 }
		}
	}
}
