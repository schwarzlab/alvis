/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.sequencebundle;

import com.general.utils.GUIutils;
import com.general.utils.GeneralUtils;
import de.biozentrum.bioinformatik.alignment.events.AlignmentSelectionEvent;
import de.biozentrum.bioinformatik.alignment.events.AlignmentSelectionListener;
import gui.sequencebundle.event.GridPoint;
import gui.sequencebundle.event.GridSelectionEvent;
import gui.sequencebundle.event.GridSelectionListener;
import gui.sequencebundle.event.SequenceBundleMouseEvent;
import gui.sequencebundle.event.SequenceBundlesMouseListener;
import java.awt.Point;
import java.awt.Rectangle;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public class SequenceBundleEventManager {
	private final JSequenceBundle view;
	private boolean noAlignmentSelectionEvents;
	protected GridSelectionListener gridSelectionListener;
	protected SequenceBundlesMouseListener mouseEventListener;
	protected ListSelectionListener sequenceSelectionListener;
	protected AlignmentSelectionListener alignmentSelectionListener;
	
	public SequenceBundleEventManager(JSequenceBundle view) {
		this.view = view;
		this.gridSelectionListener = new GridSelectionManager();
		this.mouseEventListener = new MouseEventManager();
		this.sequenceSelectionListener = new SequenceSelectionManager();
		this.alignmentSelectionListener = new AlignmentSelectionManager();
	}

	protected void disableAlignmentSelectionEvents() {
		this.noAlignmentSelectionEvents=true;
	}
	
	protected void enableAlignmentSelectionEvents() {
		this.noAlignmentSelectionEvents=false;
	}
	
	int[] gridPointToSequenceIndices(GridPoint p){
		if (!p.isWithinGrid()) return new int[0];
		
		boolean[] matches = new boolean[view.alignment.getSequenceCount()];
		
		char[] col = view.alignment.getColumn(p.x);		
		char testChar;
		if (p.y==view.alphabet.size()) { // gap char
			testChar=view.bundleConfig.gapChar;
		} else {
			testChar=view.alphabet.characterAt(p.y);
		}

		for (int i=0;i<col.length;i++) {
			// translate row index into actual char, this needs to be redone at some point
			if (col[i]==testChar) {
				matches[i]=true;
			}
		}
		return GeneralUtils.whichTrue(matches);
	}
	
	/**
	 * Translates an alignment position into a GridPoint in the sequence bundle
	 * 
	 * @param p The point in the alignment (y=sequence, x=column)
	 * @return 
	 */
	private GridPoint alignmentPointToSBGridPoint(Point p){
		int	sequenceIndex = p.y;
		int column = p.x;
		char letter = view.alignment.characterAt(sequenceIndex, column);
		int alphabetIndex;
		if (letter == view.bundleConfig.gapChar) {
			alphabetIndex = view.alphabet.size();
		} else {
			alphabetIndex = view.alphabet.indexOf(letter);
		}
		return new GridPoint(column, alphabetIndex);
	}
	
	// SB MOUSE EVENT LISTENER
	private class MouseEventManager implements SequenceBundlesMouseListener {
		private GridPoint lastPressedPoint=null;
		// deal with mouse events, translate them to selection events
		@Override
		public void sequenceBundleGridClicked(SequenceBundleMouseEvent e) {
//				updateSequenceSelection(e.getGridPoint());			
		}
		@Override
		public void sequenceBundleGridCellEntered(SequenceBundleMouseEvent e) {
			view.currentMouseGridPoint = e.getGridPoint();
			view.repaint();
			//System.out.println("Entered x="+e.getGridPoint().x + ", y="+e.getGridPoint().y);
		}

		@Override
		public void sequenceBundleGridCellLeft(SequenceBundleMouseEvent e) {
			view.currentMouseGridPoint=GridPoint.invalidGridPoint();
			view.repaint();
			//System.out.println("Left x="+e.getGridPoint().x + ", y="+e.getGridPoint().y);
		}
		// end mouse events

		@Override
		public void sequenceBundleGridPressed(SequenceBundleMouseEvent e) {
			if (view.bundleConfig.selectionEnabled) {
				lastPressedPoint = e.getGridPoint();
				updateGridSelection(e);
				updateSequenceSelection(e.getGridPoint());
				updateAlignmentSelection(e.getGridPoint());
			}
		}

		@Override
		public void sequenceBundleGridReleased(SequenceBundleMouseEvent e) {
			if (view.currentDragRectangle!=null){
				view.gridSelectionModel.addSelectionArea(view.currentDragRectangle.p1, view.currentDragRectangle.p2);
				view.sequenceSelectionModel.setValueIsAdjusting(true);
				for (GridPoint p:view.currentDragRectangle){
					updateSequenceSelection(p);
					updateAlignmentSelection(p);
				}
				view.sequenceSelectionModel.setValueIsAdjusting(false);
			}
			lastPressedPoint=null;
			view.currentDragRectangle=null;
		}

		@Override
		public void sequenceBundleGridDragged(SequenceBundleMouseEvent e) {
			if (lastPressedPoint==null) lastPressedPoint=e.getGridPoint(); // shouldn't happen
			GridPoint currentPoint = e.getGridPoint();
			view.currentDragRectangle = new GridPoint.GridRectangle(lastPressedPoint, currentPoint);
			view.repaint();
		}
		

		/**
		* Updates the grid selection at the point given by the SequencesBundles
		 * MouseEvent. Basically just flips between selected/unselected. Also clears
		 * all selections if the click was off grid.
		 * 
		 * @param e The SequenceBundlesMouseEvent generated from SequenceBundlesView
		 */
		private void updateGridSelection(SequenceBundleMouseEvent e) {
			GridPoint p = e.getGridPoint();
			if (!p.isValidGridPoint()){
				view.clearSelection();
			} else {
				boolean remove;

				// first update our own grid selection models
				if (!(e.isControlDown() || e.isShiftDown())) {
					view.clearSelection();
					remove=false;
				} else {
					if (view.gridSelectionModel.isSelectedCell(p)) {
						remove=true;
					} else {
						remove=false;
					}
				}

				if (remove) {
					view.gridSelectionModel.removeSelectionArea(p, p);
				} else {
					view.gridSelectionModel.addSelectionArea(p, p);
				}
			}
		}

		/**
		 * Updates the SequenceSelectionModel to reflect the click on 
		 * GridPoint p. All sequences that contain the character corresponding
		 * to the row that was clicked at the position that was clicked will
		 * be selected. 
		 * 
		 * @param p The GridPoint that was clicked.
		 */
		private void updateSequenceSelection(GridPoint p) {
			if (!p.isWithinGrid()) return;
		
			boolean prevVal = view.sequenceSelectionModel.getValueIsAdjusting();
			view.sequenceSelectionModel.setValueIsAdjusting(true);		
			boolean remove = !view.gridSelectionModel.isSelectedCell(p);
			int[] seqIdx = gridPointToSequenceIndices(p);
			for (int i=0;i<seqIdx.length;i++) {
				if (remove) {
					view.sequenceSelectionModel.removeSelectionInterval(seqIdx[i], seqIdx[i]);
				} else {
					view.sequenceSelectionModel.addSelectionInterval(seqIdx[i], seqIdx[i]);
				}
			}
			view.sequenceSelectionModel.setValueIsAdjusting(prevVal);		
		}

		/**
		 * Updates the AlignmentSelection to reflect the Bundles GridPoint that was clicked.
		 * All characters that match the clicked Cell are selected in the clicked column.
		 * 
		 * @param p The GridPoint that was clicked
		 */
		private void updateAlignmentSelection(GridPoint p){
			if (!p.isWithinGrid()) return;
			
			noAlignmentSelectionEvents = true;
			view.alignmentSelectionModel.begin();
			boolean remove = !view.gridSelectionModel.isSelectedCell(p);
			int[] seqIdx = gridPointToSequenceIndices(p);
			for (int i=0;i<seqIdx.length;i++) {
				if (remove) {
					if (view.alignmentSelectionModel!=null) {
						view.alignmentSelectionModel.deselect(seqIdx[i], p.x);
					}
				} else {
					if (view.alignmentSelectionModel!=null) {
						view.alignmentSelectionModel.select(seqIdx[i], p.x, false);
					}
				}
			}
			view.alignmentSelectionModel.end();
			noAlignmentSelectionEvents=false;
		}
		
	}
	
	// ALIGNMENT SELECTION LISTENER
	private class AlignmentSelectionManager implements AlignmentSelectionListener {
		private Rectangle alignmentSelectionCache = new Rectangle();
		
		@Override
		public void selectionChanged(AlignmentSelectionEvent e) {
			this.alignmentSelectionCache.add(e.getRectangle());
		}

		/** Updates the GridSelectionModel to reflect the changes in the AlignmentSelectionModel.
		 * 
		 * @param sel The rectangle in which the AlignmentSelectionModel changed
		 */
		private void updateGridSelection(Rectangle sel) {
			if (sel==null) {
//				sel = new Rectangle(0, 0, view.alignment.length(), view.alignment.rows());
				sel = alignmentSelectionCache.getBounds();
			}			
			view.gridSelectionModel.setValueIsAdjusting(true);
			for (int i=sel.y;i<sel.y+sel.height;i++){
				for (int j=sel.x;j<sel.x+sel.width;j++) {
					GridPoint gridPoint = alignmentPointToSBGridPoint(new Point(j,i));
					int index = gridPoint.y;
					if (view.alignmentSelectionModel.isSelected(i, j, false)) {
						view.gridSelectionModel.addSelectionArea(index, j, index, j);
					} else {
						view.gridSelectionModel.removeSelectionArea(index, j, index, j);
					}
				}
			}
			view.gridSelectionModel.setValueIsAdjusting(false);
		}

		@Override
		public void selectionWillBegin() {
			view.gridSelectionModel.setValueIsAdjusting(true);
		}

		@Override
		public void selectionDidEnd() {
			if (noAlignmentSelectionEvents) { // avoid looping of events
				return;
			}
			updateGridSelection(null);
			
			view.gridSelectionModel.setValueIsAdjusting(false);
			this.alignmentSelectionCache = new Rectangle();
		}

		private void updateSequenceSelection(Rectangle sel) {
			if (sel==null) {
				sel = new Rectangle(0, 0, view.alignment.length(), view.alignment.rows());
			}
			view.sequenceSelectionModel.setValueIsAdjusting(true);
			for (int i=sel.y;i<sel.y+sel.height;i++){
				boolean seqSelected = false;
				for (int j=sel.x;j<sel.x+sel.width;j++) {
					if (view.alignmentSelectionModel.isSelected(i, j, false)) {
						seqSelected = true;
					}
				}
				if (seqSelected){
					view.sequenceSelectionModel.addSelectionInterval(i, i);
				} else {
					view.sequenceSelectionModel.removeSelectionInterval(i, i);
				}
			}
			view.sequenceSelectionModel.setValueIsAdjusting(false);
		}

	}
	// SEQUENCE SELECTION LISTENER
	private class SequenceSelectionManager implements ListSelectionListener {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if (!e.getValueIsAdjusting()) {
					int[] selected = GUIutils.getSelectedIndices(view.sequenceSelectionModel);
					view.rendererCache.setSelectionIndices(selected);
					view.repaint();
				}
			}
		
	}
	// GRID SELECTION LISTENER
	private class GridSelectionManager implements GridSelectionListener {
		@Override
		public void valueChanged(GridSelectionEvent e) {
			if (!e.getValueIsAdjusting()) {
				view.repaint();
				//System.out.println("Grid selection changed [not adjusting]: " + (e.getFirstPoint()==null?"null":e.getFirstPoint().toString()) + " to " + (e.getLastPoint()==null?"null":e.getLastPoint().toString()));
			} else {
				//System.out.println("Grid selection changed [adjusting]");					
			}
		}
		
		
		
	}
}
