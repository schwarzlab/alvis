/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.sequencebundle.actions;

import gui.sequencebundle.JSequenceBundle;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.AbstractAction;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public class ActionBundleClearColumnAnnotations extends AbstractAction implements PropertyChangeListener{
	JSequenceBundle bundle;
	public ActionBundleClearColumnAnnotations(JSequenceBundle bundle) {
		super("Clear column annotations");
		this.bundle=bundle;
		this.bundle.addPropertyChangeListener(JSequenceBundle.PROP_COLUMN_ANNOTATION, this);
		setEnabled(this.bundle.getColumnAnnotation()!=null);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		bundle.setColumnAnnotation(null);
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		setEnabled(this.bundle.getColumnAnnotation()!=null);
	}
	
	
	
}
