/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.sequencebundle.actions;

import gui.sequencebundle.JSequenceBundle;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.AbstractAction;
import static javax.swing.Action.ACCELERATOR_KEY;
import javax.swing.KeyStroke;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public class ActionBundleRedraw extends AbstractAction implements PropertyChangeListener{
	JSequenceBundle bundle;
	public ActionBundleRedraw(JSequenceBundle bundle) {
		super("Redraw");
		this.bundle=bundle;
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_R, InputEvent.CTRL_DOWN_MASK));	
		this.bundle.addPropertyChangeListener(JSequenceBundle.PROP_ALIGNMENT, this);
		setEnabled(this.bundle.getAlignment()!=null);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		bundle.redraw();
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		setEnabled(this.bundle.getAlignment()!=null);
	}
	
	
	
}
