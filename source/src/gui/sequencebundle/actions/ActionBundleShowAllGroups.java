/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.sequencebundle.actions;

import gui.sequencebundle.JSequenceBundle;
import java.awt.event.ActionEvent;
import java.util.HashSet;
import java.util.Set;
import javax.swing.AbstractAction;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public class ActionBundleShowAllGroups extends AbstractAction{
	private final JSequenceBundle bundle;
	public ActionBundleShowAllGroups(JSequenceBundle bundle){
		super("All");
		this.bundle=bundle;
	}
			
	@Override
	public void actionPerformed(ActionEvent e) {
		Set<Integer> groups = new HashSet<>(bundle.getSequenceGroups().values());
		boolean allvisible=true;
		for (int group:groups) {
			allvisible &= bundle.getBundleConfig().isShowingGroup(group);
		}
		for (int group:groups){
			bundle.getBundleConfig().setShowingGroup(group, !allvisible);
		}
	}
	
}
