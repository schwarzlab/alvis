/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.sequencebundle.actions;

import gui.sequencebundle.JSequenceBundle;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.AbstractAction;
import javax.swing.JCheckBoxMenuItem;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public class ActionBundleAutoRedraw extends AbstractAction implements PropertyChangeListener{
	JSequenceBundle bundle;
	
	public ActionBundleAutoRedraw(JSequenceBundle bundle) {
		super("Auto redraw");
		this.bundle=bundle;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JCheckBoxMenuItem mnu = (JCheckBoxMenuItem) e.getSource();
		bundle.getBundleConfig().setAutomaticRedraw(mnu.isSelected());
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
	}
	
}
