/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.sequencebundle.actions;

import gui.sequencebundle.JSequenceBundle;
import gui.sequencebundle.legend.AbstractLegendRenderer;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.AbstractAction;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public class ActionBundleLegendDefault extends AbstractAction implements PropertyChangeListener{
	JSequenceBundle bundle;
	public ActionBundleLegendDefault(JSequenceBundle bundle) {
		super("Default");
		this.bundle=bundle;
		this.bundle.addPropertyChangeListener(JSequenceBundle.PROP_ALIGNMENT, this);
		setEnabled(this.bundle.getAlignment()!=null && this.bundle.getAlphabet()!=null);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		AbstractLegendRenderer lr = AbstractLegendRenderer.createDefaultLegendRenderer(bundle.getAlphabet(), bundle.getBundleConfig());
		bundle.setLegendRenderer(lr);
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		setEnabled(this.bundle.getAlignment()!=null && this.bundle.getAlphabet()!=null);
	}
	
	
	
}
