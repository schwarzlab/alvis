/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.sequencebundle.actions;

import com.general.gui.dialog.DialogFactory;
import gui.sequencebundle.JSequenceBundle;
import gui.sequencebundle.SequenceBundleConfigPanel;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public class ActionBundleOptions extends AbstractAction{
	private final JSequenceBundle bundle;
	public ActionBundleOptions(JSequenceBundle bundle) {
		super("Rendering options...");
		this.bundle = bundle;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		SequenceBundleConfigPanel cop = new SequenceBundleConfigPanel(bundle.getBundleConfig());
		DialogFactory.showOkCancelApplyDialog(cop, false);
	}
}
