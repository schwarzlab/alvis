/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.sequencebundle.actions;

import com.general.resources.MyJavaResources;
import gui.sequencebundle.JSequenceBundle;
import gui.sequencebundle.SequenceBundleConfig;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import static javax.swing.Action.SMALL_ICON;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public class ActionBundleShowGroup extends AbstractAction implements PropertyChangeListener{
	private final JSequenceBundle bundle;
	private final int group;
	public ActionBundleShowGroup(JSequenceBundle bundle, int group){
		super(String.valueOf(group+1));
		this.bundle=bundle;
		this.group = group;
		putValue(SMALL_ICON, MyJavaResources.createColoredSquare(12, bundle.getBundleConfig().getColorModel().getGroupColors().get(this.group)));
		this.bundle.getBundleConfig().addPropertyChangeListener(SequenceBundleConfig.PROP_GROUPVISIBILITY, this);
		putValue(SELECTED_KEY, bundle.getBundleConfig().isShowingGroup(group));
	}
			
	@Override
	public void actionPerformed(ActionEvent e) {
		boolean show=true;
		if (e.getSource() instanceof AbstractButton) {
			AbstractButton mnu = (AbstractButton)e.getSource();
			show = mnu.isSelected();
		}
		if (bundle!=null){
			bundle.getBundleConfig().setShowingGroup(group, show);
		}
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		putValue(SELECTED_KEY, bundle.getBundleConfig().isShowingGroup(group));
	}
	
}
