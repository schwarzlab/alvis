/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.sequencebundle.actions;

import gui.sequencebundle.JSequenceBundle;
import gui.sequencebundle.SequenceBundleConfig;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JCheckBoxMenuItem;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public class ActionBundleShowConsensus extends AbstractAction implements PropertyChangeListener{
	
	private final JSequenceBundle bundle;
	
	public ActionBundleShowConsensus(JSequenceBundle bundle){
		super("Show consensus");
		this.bundle=bundle;
		
		bundle.addPropertyChangeListener(new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				if (evt.getPropertyName().equals(JSequenceBundle.PROP_BUNDLECONFIG)) {
					((SequenceBundleConfig)evt.getOldValue()).removePropertyChangeListener(ActionBundleShowConsensus.this);
					((SequenceBundleConfig)evt.getNewValue()).addPropertyChangeListener(ActionBundleShowConsensus.this);
				}
			}
		});
		
		if (bundle!=null) {
			putValue(Action.SELECTED_KEY, bundle.getBundleConfig().isShowingConsensus());
		}
		bundle.getBundleConfig().addPropertyChangeListener(this);				
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (bundle!=null){
			bundle.getBundleConfig().setShowingConsensus((Boolean)getValue(Action.SELECTED_KEY));
		}
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if (evt.getPropertyName().equals(SequenceBundleConfig.PROP_SHOWINGCONSENSUS)) {
			putValue(Action.SELECTED_KEY, evt.getNewValue());
		} 
	}

	
}
