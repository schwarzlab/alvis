/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.sequencebundle.actions;

import com.general.gui.dialog.DialogFactory;
import com.general.gui.panels.SelectNameablePanel;
import de.biozentrum.bioinformatik.sequence.SequenceAlphabet;
import gui.sequencebundle.JSequenceBundle;
import gui.sequencebundle.aaindex.AAIndexEntry;
import gui.sequencebundle.aaindex.AAIndexRepository;
import gui.sequencebundle.legend.AbstractLegendRenderer;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.AbstractAction;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public class ActionBundleLegendAAIndex extends AbstractAction implements PropertyChangeListener{
	JSequenceBundle bundle;
	AAIndexEntry lastChoice;
	public ActionBundleLegendAAIndex(JSequenceBundle bundle) {
		super("From AAIndex");
		this.bundle=bundle;
		this.bundle.addPropertyChangeListener(JSequenceBundle.PROP_ALIGNMENT, this);
		setEnabled(this.bundle.getAlignment()!=null && this.bundle.getAlphabet()!=null && this.bundle.getAlphabet().equals(SequenceAlphabet.AMINOACIDS));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		SelectNameablePanel<AAIndexEntry> panel;
		if (lastChoice==null) {
			panel = new SelectNameablePanel<>(AAIndexRepository.AAIndices, "Choose AAIndex:");
		} else {
			panel = new SelectNameablePanel<>(AAIndexRepository.AAIndices, lastChoice, "Choose AAIndex:");
		}
		if (DialogFactory.showModalOKCancelDialog(panel)) { // cancelled
			AAIndexEntry entry = panel.getSelectedObject();
			bundle.setLegendRenderer(AbstractLegendRenderer.createAAIndexLegendRenderer(bundle.getAlphabet(), bundle.getBundleConfig(), entry));
			lastChoice = entry;
		}
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		setEnabled(this.bundle.getAlignment()!=null && this.bundle.getAlphabet()!=null && this.bundle.getAlphabet().equals(SequenceAlphabet.AMINOACIDS));
	}
	
	
	
}
