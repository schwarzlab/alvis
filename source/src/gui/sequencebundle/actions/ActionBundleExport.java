/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.sequencebundle.actions;

import com.general.gui.dialog.DialogFactory;
import static com.general.gui.dialog.DialogFactory.createFileSaveDialog;
import com.general.gui.noia.NoiaIconRepository;
import gui.GUIConstants;
import gui.sequencebundle.JSequenceBundle;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import javax.swing.AbstractAction;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileFilter;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public class ActionBundleExport extends AbstractAction implements PropertyChangeListener{
	JSequenceBundle sequenceBundle;
	
	public ActionBundleExport(JSequenceBundle jsb) {
		super("Export to file...");
		this.sequenceBundle=jsb;
		this.sequenceBundle.addPropertyChangeListener(JSequenceBundle.PROP_ALIGNMENT, this);
		setEnabled(this.sequenceBundle.getAlignment()!=null);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JPanel pan = new JPanel(new FlowLayout());
		SpinnerNumberModel spinmod = new SpinnerNumberModel(144, 20, 1200, 1);
		JSpinner spin = new JSpinner(spinmod);
		pan.add(new JLabel("DPI: "));
		pan.add(spin);
		
        File chosenFile=null;
		FileFilter chosenFilter=null;
		FileFilter[] filters = new FileFilter[] {GUIConstants.PNG_FILE_FILTER, GUIConstants.SVG_FILE_FILTER};
//		FileFilter[] filters = new FileFilter[] {GUIConstants.PNG_FILE_FILTER};
        JFileChooser fileChooser=createFileSaveDialog("Choose the file to save to:",  filters);
		fileChooser.setAccessory(pan);

        if (fileChooser.showSaveDialog(sequenceBundle)==JFileChooser.APPROVE_OPTION) {
            // user confirmed the dialog
            chosenFile=fileChooser.getSelectedFile();
			chosenFilter = fileChooser.getFileFilter();
			DialogFactory.setLastDir(fileChooser.getCurrentDirectory());
            if (chosenFile.exists()) {
                if (JOptionPane.showConfirmDialog(sequenceBundle,
                                              "File already exists! Overwrite?",
                                              "Confirm:",
                                              JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE,NoiaIconRepository.createImageIcon(NoiaIconRepository.Images.IMG_32X32_ACTIONS_HELP_PNG))==1) {
                    chosenFile=null;
                }
            }
        }
		
		if (chosenFile!=null) {
			int dpi = spinmod.getNumber().intValue();
			if (chosenFilter==GUIConstants.SVG_FILE_FILTER) {
				sequenceBundle.renderSVGToFile(chosenFile, 72); // fix dpi for SVG export, otherwise line widths might be off
			} else {
				sequenceBundle.renderPNGToFile(chosenFile, dpi);
			}
		}
	}

	/**
	 * @return the sequenceBundle
	 */
	public JSequenceBundle getSequenceBundle() {
		return sequenceBundle;
	}

	/**
	 * @param sequenceBundle the sequenceBundle to set
	 */
	public void setSequenceBundle(JSequenceBundle sequenceBundle) {
		this.sequenceBundle = sequenceBundle;
		setEnabled(this.sequenceBundle!=null);		
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		setEnabled(sequenceBundle.getAlignment()!=null);
	}
	
}
