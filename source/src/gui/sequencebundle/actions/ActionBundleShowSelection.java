/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.sequencebundle.actions;

import gui.sequencebundle.JSequenceBundle;
import gui.sequencebundle.SequenceBundleConfig;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.AbstractAction;
import javax.swing.Action;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public class ActionBundleShowSelection extends AbstractAction implements PropertyChangeListener{
	private final JSequenceBundle bundle;
	public ActionBundleShowSelection(JSequenceBundle bundle){
		super("Show selection");
		this.bundle=bundle;
		
		bundle.addPropertyChangeListener(new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				if (evt.getPropertyName().equals(JSequenceBundle.PROP_BUNDLECONFIG)) {
					((SequenceBundleConfig)evt.getOldValue()).removePropertyChangeListener(ActionBundleShowSelection.this);
					((SequenceBundleConfig)evt.getNewValue()).addPropertyChangeListener(ActionBundleShowSelection.this);
				}
			}
		});
		bundle.getBundleConfig().addPropertyChangeListener(this);				
		
		putValue(SELECTED_KEY, bundle.getBundleConfig().isShowingSelection());
	}
			
	@Override
	public void actionPerformed(ActionEvent e) {
		if (bundle!=null){
			bundle.getBundleConfig().setShowingSelection((Boolean)getValue(Action.SELECTED_KEY));
		}
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if (evt.getPropertyName().equals(SequenceBundleConfig.PROP_SHOWINGSELECTION)) {
			putValue(Action.SELECTED_KEY, evt.getNewValue());
		} 
	}
	
}
