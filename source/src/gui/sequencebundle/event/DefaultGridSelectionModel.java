/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.sequencebundle.event;

import java.util.Collection;
import java.util.TreeSet;
import javax.swing.DefaultListSelectionModel;
import javax.swing.ListSelectionModel;
import javax.swing.event.EventListenerList;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public class DefaultGridSelectionModel implements GridSelectionModel{
	private final EventListenerList listenerList = new EventListenerList();	
	private boolean valueIsAdjusting=false;
	private final DefaultListSelectionModelDelegate rowModel = new DefaultListSelectionModelDelegate(GridSelectionEvent.Type.ROW);
	private final DefaultListSelectionModelDelegate colModel = new DefaultListSelectionModelDelegate(GridSelectionEvent.Type.COLUMN);
	private final Collection<GridPoint> cellModel = new TreeSet<>();
	private GridPoint.GridRectangle modifiedArea = null;
	
	public DefaultGridSelectionModel() {
		
	}

	private void addToModifiedArea(GridPoint p) {
		if (modifiedArea==null) {
			modifiedArea=new GridPoint.GridRectangle(p, p);
		} else {
			modifiedArea=GridPoint.enclosingRectangle(new GridPoint[] {modifiedArea.p1, modifiedArea.p2, p});
		}
	}
	
	@Override
	public boolean isSelectionEmpty() {
		return rowModel.isSelectionEmpty() && colModel.isSelectionEmpty() && cellModel.isEmpty();
	}

	@Override
	public void clearSelection() {
		if (rowModel.isSelectionEmpty() && colModel.isSelectionEmpty() && cellModel.isEmpty()) {
			return;
		}
		
		if (!cellModel.isEmpty()) {
			GridPoint[] all = new GridPoint[cellModel.size()];
			cellModel.toArray(all);
			GridPoint.GridRectangle rect = GridPoint.enclosingRectangle(all);
			if (this.valueIsAdjusting){
				addToModifiedArea(rect.p1);
				addToModifiedArea(rect.p2);
			}
			cellModel.clear();
			fireValueChanged(new GridSelectionEvent(this, GridSelectionEvent.Type.CELL, rect.p1, rect.p2, false));
		}
		
		if (!rowModel.isSelectionEmpty()) {
			rowModel.clearSelection();			
		}
		
		if (!colModel.isSelectionEmpty()) {
			colModel.clearSelection();
		}
	}
	
	@Override
	public void addSelectionArea(int rowFrom, int colFrom, int rowTo, int colTo) {
		GridPoint from = new GridPoint(colFrom, rowFrom);
		GridPoint to = new GridPoint(colTo, rowTo);
		addSelectionArea(from, to);
	}

	@Override
	public void addSelectionArea(GridPoint from, GridPoint to) {
		for (GridPoint p:GridPoint.enclosingRectangle(from, to)) {
			if (p.isColHeader()){
				colModel.addSelectionInterval(p.x, p.x);
				if (valueIsAdjusting) addToModifiedArea(p);
			} else if (p.isRowHeader()){
				rowModel.addSelectionInterval(p.y, p.y);
				if (valueIsAdjusting) addToModifiedArea(p);
			} else {
				if (cellModel.add(p)) {
					fireValueChanged(new GridSelectionEvent(this, GridSelectionEvent.Type.CELL, p,p, true));
					if (this.valueIsAdjusting) {
						addToModifiedArea(p);
					}
				}
			}
		}
		fireValueChanged(new GridSelectionEvent(this, GridSelectionEvent.Type.CELL, from, to, false));
	}

	@Override
	public void addRowSelectionInterval(int from, int to) {
		rowModel.addSelectionInterval(from, to);
	}

	@Override
	public void addColSelectionInterval(int from, int to) {
		colModel.addSelectionInterval(from, to);
	}

	@Override
	public boolean isSelectedCell(GridPoint p) {
		return cellModel.contains(p);
	}
	
	@Override
	public boolean isSelectedCell(int row, int col) {
		return cellModel.contains(new GridPoint(col, row));
	}

	@Override
	public boolean isSelectedRow(int row) {
		return rowModel.isSelectedIndex(row);
	}

	@Override
	public boolean isSelectedCol(int col) {
		return colModel.isSelectedIndex(col);
	}

	@Override
	public void removeSelectionArea(int rowFrom, int colFrom, int rowTo, int colTo) {
		GridPoint from = new GridPoint(colFrom, rowFrom);
		GridPoint to = new GridPoint(colTo,rowTo);
		removeSelectionArea(from, to);
	}

	@Override
	public void removeSelectionArea(GridPoint from, GridPoint to) {
		for (GridPoint p : GridPoint.enclosingRectangle(from, to)) {
			if (cellModel.remove(p)) {
				fireValueChanged(new GridSelectionEvent(this, GridSelectionEvent.Type.CELL, p, p, true));
				if (this.valueIsAdjusting) {
					addToModifiedArea(p);
				}
			}
		}
		fireValueChanged(new GridSelectionEvent(this, GridSelectionEvent.Type.CELL, from, to, false));
	}

	@Override
	public void removeRowSelectionInterval(int from, int to) {
		rowModel.removeIndexInterval(from, to);
	}

	@Override
	public void removeColSelectionInterval(int from, int to) {
		colModel.removeSelectionInterval(from, to);
	}
	
	@Override
	public boolean getValueIsAdjusting() {
		return this.valueIsAdjusting;
	}

	@Override
	public void setValueIsAdjusting(boolean isAdjusting) {
		// currently only fires event if isAdjusting==False and it has changed
		// when isAdjusting==True it only saves the current grid selection status
		// for use upon isAdjusting==False
        if (isAdjusting != this.valueIsAdjusting) {
			this.valueIsAdjusting = isAdjusting;						
			if (!isAdjusting) {
				this.fireValueChanged(new GridSelectionEvent(this, GridSelectionEvent.Type.GLOBAL, modifiedArea, isAdjusting));
				modifiedArea=null;
			}
		}		
	}

	@Override
	public ListSelectionModel getRowSelectionModel() {
		return rowModel;
	}

	@Override
	public ListSelectionModel getColSelectionModel() {
		return colModel;
	}

	@Override
	public Iterable<GridPoint> getSelectedGridPoints() {
		return cellModel;
	}

	@Override
	public void addGridSelectionListener(GridSelectionListener listener) {
		listenerList.add(GridSelectionListener.class, listener);
		rowModel.addGridSelectionListener(listener);
		colModel.addGridSelectionListener(listener);
	}

	@Override
	public void removeGridSelectionListener(GridSelectionListener listener) {
		listenerList.remove(GridSelectionListener.class, listener);
		rowModel.removeGridSelectionListener(listener);
		colModel.removeGridSelectionListener(listener);
	}


	private void fireValueChanged(GridSelectionEvent evt) {
		// don't fire events if we are adjusting and another comes along
		if (this.valueIsAdjusting && !evt.getValueIsAdjusting()) {
			return;
		}
		
		Object[] listeners = listenerList.getListenerList();
		for (int i = 0; i < listeners.length; i = i+2) {
			if (listeners[i] == GridSelectionListener.class) {
				((GridSelectionListener) listeners[i+1]).valueChanged(evt);
			}
		}
	}	

	private class DefaultListSelectionModelDelegate extends DefaultListSelectionModel {
		private GridSelectionEvent.Type type;
		public DefaultListSelectionModelDelegate(GridSelectionEvent.Type type) {
			super();
			this.type = type;
		}

		@Override
		public boolean getValueIsAdjusting() {
			return valueIsAdjusting;
		}

		@Override
		protected void fireValueChanged(int firstIndex, int lastIndex, boolean isAdjusting) {
			Object[] listeners = listenerList.getListenerList();
			GridSelectionEvent e = null;

			for (int i = listeners.length - 2; i >= 0; i -= 2) {
				if (listeners[i] == GridSelectionListener.class) {
					if (e == null) {
						e = new GridSelectionEvent(this, type, firstIndex, lastIndex, isAdjusting);
					}
					((GridSelectionListener)listeners[i+1]).valueChanged(e);
				}
			}
		}

		public void addGridSelectionListener(GridSelectionListener l) {
			listenerList.add(GridSelectionListener.class, l);
		}
		
		public void removeGridSelectionListener(GridSelectionListener l) {
			listenerList.remove(GridSelectionListener.class, l);
		}
		
		

	}
}
