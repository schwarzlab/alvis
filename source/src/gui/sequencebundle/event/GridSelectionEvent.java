/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.sequencebundle.event;

import javax.swing.event.ListSelectionEvent;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public class GridSelectionEvent extends ListSelectionEvent {
	public static enum Type {ROW, COLUMN, CELL, GLOBAL};
	private final GridPoint firstPoint;
	private final GridPoint lastPoint;
	private final Type type;
	
	public GridSelectionEvent(Object source, Type type, int firstIndex, int lastIndex, boolean isAdjusting) {
		super(source, firstIndex, lastIndex, isAdjusting);
		this.type=type;
		this.firstPoint=null;
		this.lastPoint=null;
	}
	
	public GridSelectionEvent(Object source, Type type, GridPoint firstPoint, GridPoint lastPoint, boolean isAdjusting) {
		super(source,-1, -1, isAdjusting);
		this.type = type;
		this.firstPoint = firstPoint;
		this.lastPoint=lastPoint;
	}
	
	public GridSelectionEvent(Object source, Type type, GridPoint.GridRectangle rect, boolean isAdjusting) {
		super(source,-1,-1,isAdjusting);
		this.type=type;
		if (rect==null){
			this.firstPoint=null;
			this.lastPoint=null;
		} else {
			this.firstPoint = rect.p1;
			this.lastPoint = rect.p2;
		}
	}
	
	public Type getType(){
		return type;
	}

	
	/**
	 * @return the firstPoint
	 */
	public GridPoint getFirstPoint() {
		return firstPoint;
	}

	/**
	 * @return the lastPoint
	 */
	public GridPoint getLastPoint() {
		return lastPoint;
	}

}
