/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.sequencebundle.event;

import java.awt.Point;
import java.awt.event.MouseEvent;
import java.util.EventObject;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public class SequenceBundleMouseEvent extends EventObject{
	private final GridPoint gridPoint;
	private final MouseEvent sourceEvent;
	
	public SequenceBundleMouseEvent(MouseEvent mouseEvent, GridPoint gridPoint) {
		super(mouseEvent.getSource());
		this.gridPoint = gridPoint;
		this.sourceEvent=mouseEvent;
	}

	/**
	 * @return the gridPoint
	 */
	public GridPoint getGridPoint() {
		return gridPoint;
	}
	
	public Point getPoint() {
		return sourceEvent.getPoint();
	}
	
	public boolean isAltDown() {
		return sourceEvent.isAltDown();
	}
	
	public boolean isShiftDown() {
		return sourceEvent.isShiftDown();
	}

	public boolean isControlDown() {
		return sourceEvent.isControlDown();
	}
	
	/**
	 * @return the mouseEvent
	 */
	public MouseEvent getSourceEvent() {
		return sourceEvent;
	}
}
