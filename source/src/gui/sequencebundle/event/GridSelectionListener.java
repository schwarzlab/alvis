/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.sequencebundle.event;

import java.util.EventListener;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public interface GridSelectionListener extends EventListener {
	public void valueChanged(GridSelectionEvent e);
}
