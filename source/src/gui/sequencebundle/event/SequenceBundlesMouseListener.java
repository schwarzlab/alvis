/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.sequencebundle.event;

import java.util.EventListener;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public interface SequenceBundlesMouseListener extends EventListener{
	public void sequenceBundleGridClicked(SequenceBundleMouseEvent e);
	public void sequenceBundleGridCellEntered(SequenceBundleMouseEvent e);
	public void sequenceBundleGridCellLeft(SequenceBundleMouseEvent e);
	public void sequenceBundleGridPressed(SequenceBundleMouseEvent e);
	public void sequenceBundleGridReleased(SequenceBundleMouseEvent e);
	public void sequenceBundleGridDragged(SequenceBundleMouseEvent e);
}
