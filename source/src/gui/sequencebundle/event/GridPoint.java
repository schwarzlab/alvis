/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.sequencebundle.event;

import java.awt.Point;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public class GridPoint extends Point implements Comparable{
	public static class GridRectangle implements Iterable<GridPoint>{ // rectangle defined by two points
		public GridRectangle(GridPoint p1, GridPoint p2) {
			this.p1=p1;
			this.p2=p2;
		}
		public GridPoint p1;
		public GridPoint p2;
		public int area(){ // grid area, therefore including the start point (+1)
			int area = ((Math.abs(p1.y-p2.y)+1) * Math.abs((p1.x-p2.x)+1));
			return area;
		}
		@Override
		public String toString() {
			return "Rectangle: [" + this.p1.toString() + "; " + this.p2.toString() + "]";
		}

		@Override
		public Iterator<GridPoint> iterator() {
			return new GridPointIterator(this);
		}
		
		
	}

	private static class GridPointIterator implements Iterator<GridPoint> {
		private final GridPoint from;
		private final GridPoint to;
		private int curRow;
		private int curCol;
		GridPointIterator(GridPoint from, GridPoint to){
			GridRectangle gr = GridPoint.enclosingRectangle(new GridPoint[] {from, to});
			this.from = gr.p1;
			this.to = gr.p2;
			curRow = this.from.y;
			curCol = this.from.x;
		}
		GridPointIterator(GridRectangle rect){
			this(rect.p1, rect.p2);
		}
		
		@Override
		public boolean hasNext() {
			return (curRow<=to.y && curCol<=to.x);
		}

		@Override
		public GridPoint next() {
			GridPoint  gp = new GridPoint(curCol, curRow);
			if (hasNext()){
				if (curCol<to.x) {
					curCol+=1;
				} else {
					curCol=from.x;
					curRow+=1;
				}
			} else {
				throw new NoSuchElementException();
			}
			return gp;
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
		}
		
	}
	public static final int HEADER=-1;
	public static final int OUT_OF_RANGE=-5;
	private static final GridPoint INVALID_POINT = new GridPoint(OUT_OF_RANGE,OUT_OF_RANGE);
	
	public static GridPoint invalidGridPoint() {
		return new GridPoint(INVALID_POINT);
	}
	
	public GridPoint(){
		super();
	}
	
	public GridPoint(Point p) {
		super(p);
	}
	
	public GridPoint(int x, int y) {
		super(x,y);
	}
	
	public boolean isValidGridPoint() {
		return (isWithinGrid() || isRowHeader() || isColHeader() || isHeader());
	}
	
	public boolean isWithinGrid() {
		return (x>=0 && y>=0);
	}
	
	public boolean isRowHeader() {
		return (x==HEADER && y>=0);
	}
	
	public boolean isColHeader() {
		return (y==HEADER && x>=0);
	}
	
	public boolean isHeader() {
		return (x==HEADER && y==HEADER);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof GridPoint) {
			GridPoint other = (GridPoint) obj;
			return (this.x==other.x && this.y == other.y);
		}
		return super.equals(obj); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}
	
	/**
	 * Returns the enclosing rectangle in the grid, i.e. the two points that span all given points.
	 * p1 of the rectangle is guaranteed to be the top left, p2 the bottom right point.
	 * 
	 * @param gridPoints The grid points around which to construct the rectangle
	 * @return 
	 */
	public static GridRectangle enclosingRectangle(GridPoint[] gridPoints) {
		if (gridPoints.length<1) throw new IllegalArgumentException("Need at least one point for Rectangle");
		int xMax=Integer.MIN_VALUE;
		int yMax=Integer.MIN_VALUE;
		int yMin=Integer.MAX_VALUE;
		int xMin=Integer.MAX_VALUE;
		for (GridPoint g: gridPoints) {
			xMax = Math.max(xMax, g.x);
			yMax = Math.max(yMax, g.y);
			xMin = Math.min(xMin, g.x);
			yMin = Math.min(yMin, g.y);
		}
		return new GridRectangle(new GridPoint(xMin, yMin), new GridPoint(xMax, yMax));
	}
	public static GridRectangle enclosingRectangle(GridPoint from, GridPoint to){
		return enclosingRectangle(new GridPoint[] {from, to});
	}
	public static GridRectangle enclosingRectangle(GridRectangle gr){
		return enclosingRectangle(gr.p1, gr.p2);
	}
	@Override
	public int compareTo(Object o) {
		// x before y
		int retval;
		GridPoint other = (GridPoint)o;
		if (this.x < other.x){
			retval = -1;
		} else if (this.x> other.x){
			retval = 1;
		} else {
			if (this.y < other.y){
				retval = -1;
			} else if (this.y > other.y){
				retval = 1;
			} else {
				retval =0;
			}
		}
		return retval;
	}	
	
}
