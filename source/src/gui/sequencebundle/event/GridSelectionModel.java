/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.sequencebundle.event;

import javax.swing.ListSelectionModel;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public interface GridSelectionModel {
	public boolean isSelectedCell(GridPoint p);
	public boolean isSelectedCell(int row, int col);
	public boolean isSelectedRow(int row);
	public boolean isSelectedCol(int col);
	public void addGridSelectionListener(GridSelectionListener listener);
	public void removeGridSelectionListener(GridSelectionListener listener);
	public void clearSelection();
	public boolean getValueIsAdjusting();
	public void setValueIsAdjusting(boolean isAdjusting);
	public void addSelectionArea(int rowFrom, int colFrom, int rowTo, int colTo);
	public void addSelectionArea(GridPoint from, GridPoint to);
	public void addRowSelectionInterval(int from, int to);
	public void addColSelectionInterval(int from, int to);
	public void removeSelectionArea(int rowFrom, int colFrom, int rowTo, int colTo);
	public void removeSelectionArea(GridPoint from, GridPoint to);
	public void removeRowSelectionInterval(int from, int to);
	public void removeColSelectionInterval(int from, int to);
	public boolean isSelectionEmpty();
	public Iterable<GridPoint> getSelectedGridPoints();
	public ListSelectionModel getRowSelectionModel();
	public ListSelectionModel getColSelectionModel();
	
}
