/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.sequencebundle;

import de.biozentrum.bioinformatik.color.ColorModel;
import de.biozentrum.bioinformatik.sequence.SequenceColorModel;
import java.awt.Color;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public class SequenceBundleColorModel implements Serializable{
	private static final long serialVersionUID=1l;
	
	public static final int SELECTION_COLOR=-1;
	public static final int BACKGROUND_COLOR=-2;
	public static final int SELECTION_CARET_COLOR=-3;
	public static final int SELECTION_MARKER_COLOR=-4;
	public static final int FONT_COLOR=-5;
	public static final int CONSENSUS_COLOR=-6;
	public static final Color DEFAULT_GROUP_COLOR=new Color(0,0,80);
	public static final Color DEFAULT_ALPHABET_COLOR=Color.BLUE;
	public static final Color DEFAULT_SELECTION_MARKER_COLOR=new Color(255, 204, 51, 172);
	public static final Color DEFAULT_SELECTION_CARET_COLOR=new Color(255, 102, 0, 255);
	public static final Color DEFAULT_FONT_COLOR=new Color(0,0,80);
	public static final Color DEFAULT_CONSENSUS_COLOR=Color.ORANGE;
	public static final Color[] DEFAULT_GROUP_COLORS = new Color[] {
		DEFAULT_GROUP_COLOR,
		new Color(55,126,184),
		new Color(255,127,0),		
		new Color(77,175,74),
		new Color(152,78,163),
		new Color(247,129,191),
		new Color(228,26,28)		
		};
	public static final String PROP_SELECTIONCOLOR = "PROP_SELECTIONCOLOR";
	public static final String PROP_BACKGROUNDCOLOR = "PROP_BACKGROUNDCOLOR";
	public static final String PROP_SELECTIONMARKERCOLOR = "PROP_SELECTIONMARKERCOLOR";
	public static final String PROP_SELECTIONCARETCOLOR = "PROP_SELECTIONCARETCOLOR";
	public static final String PROP_FONTCOLOR = "PROP_FONTCOLOR";
	public static final String PROP_CONSENSUSCOLOR = "PROP_CONSENSUSCOLOR";
	public static final String PROP_GROUPCOLORS = "PROP_GROUPCOLORS";
	public static final String PROP_ALPHABETCOLORS = "PROP_ALPHABETCOLORS";
	
	
	Color selectionColor;
	Color backgroundColor;
	Color selectionMarkerColor;
	Color selectionCaretColor;
	Color fontColor;
	Color consensusColor;
	ColorModel<Integer> groupColors;
	SequenceColorModel alphabetColors;
	private transient PropertyChangeSupport propertyChangeSupport = new java.beans.PropertyChangeSupport(this);
	
	public SequenceBundleColorModel() {
		groupColors = new ColorModel<>();
		alphabetColors = new SequenceColorModel();
		setDefaultColors();
	}
	
	public SequenceBundleColorModel(ColorModel<Integer> groupColors, SequenceColorModel alphabetColors,
			Color selectionColor, 
			Color backgroundColor,
			Color selectionMarkerColor,
			Color selectionCaretColor,
			Color fontColor,
			Color consensusColor) {
		this();
		this.groupColors = groupColors;
		this.selectionColor=selectionColor;
		this.backgroundColor=backgroundColor;
		this.selectionMarkerColor=selectionMarkerColor;
		this.selectionCaretColor=selectionCaretColor;
		this.fontColor = fontColor;
		this.consensusColor = consensusColor;
		this.alphabetColors = alphabetColors;
	}
	
	/**
	 * copy constructor
	 * @param otherModel 
	 */
	public SequenceBundleColorModel(SequenceBundleColorModel otherModel){ // copy constructor
		this();
		this.groupColors.putAll(otherModel.getGroupColors());
		this.backgroundColor=otherModel.getBackgroundColor();
		this.consensusColor=otherModel.getConsensusColor();
		this.fontColor=otherModel.getFontColor();
		this.selectionCaretColor=otherModel.getSelectionCaretColor();
		this.selectionColor=otherModel.getSelectionColor();
		this.selectionMarkerColor=otherModel.getSelectionMarkerColor();
		this.alphabetColors.putAll(otherModel.getAlphabetColors());
//		this.alphabetColors = otherModel.getAlphabetColors();
	}
	
	public void setDefaultColors() {
		this.groupColors.setDefaultColor(DEFAULT_GROUP_COLOR);
		this.selectionColor=Color.RED;
		this.backgroundColor=Color.WHITE;
		this.selectionMarkerColor=DEFAULT_SELECTION_MARKER_COLOR;
		this.selectionCaretColor=DEFAULT_SELECTION_CARET_COLOR;
		this.fontColor = DEFAULT_FONT_COLOR;
		this.consensusColor = DEFAULT_CONSENSUS_COLOR;
		for (int i=0;i<DEFAULT_GROUP_COLORS.length;i++) {
			this.groupColors.put(i, DEFAULT_GROUP_COLORS[i]);
		}	
		this.alphabetColors.setDefaultColor(DEFAULT_ALPHABET_COLOR);
	}

	/**
	 * @return the selectionColor
	 */
	public Color getSelectionColor() {
		return selectionColor;
	}

	public Color getConsensusColor() {
		return consensusColor;
	}
	
	/**
	 * @return the backgroundColor
	 */
	public Color getBackgroundColor() {
		return backgroundColor;
	}

	/**
	 * @return the selectionMarkerColor
	 */
	public Color getSelectionMarkerColor() {
		return selectionMarkerColor;
	}

	/**
	 * @return the selectionCaretColor
	 */
	public Color getSelectionCaretColor() {
		return selectionCaretColor;
	}

	/**
	 * @return the fontColor
	 */
	public Color getFontColor() {
		return fontColor;
	}

	public ColorModel<Integer> getGroupColors() {
		return groupColors;
	}

	public SequenceColorModel getAlphabetColors() {
		return alphabetColors;
	}

	public void setGroupColors(ColorModel<Integer> groupColors) {
		java.util.Map<java.lang.Integer, java.awt.Color> oldGroupColors = this.groupColors;
		this.groupColors = groupColors;
		propertyChangeSupport.firePropertyChange(PROP_GROUPCOLORS, oldGroupColors, groupColors);
	}

	public void setAlphabetColors(SequenceColorModel alphabetColors) {
		de.biozentrum.bioinformatik.sequence.SequenceColorModel oldAlphabetColors = this.alphabetColors;
		this.alphabetColors = alphabetColors;
		propertyChangeSupport.firePropertyChange(PROP_ALPHABETCOLORS, oldAlphabetColors, alphabetColors);
	}
	
	@Override
	public int hashCode() {
		int hash = 5;
		hash = 97 * hash + Objects.hashCode(this.selectionColor);
		hash = 97 * hash + Objects.hashCode(this.backgroundColor);
		hash = 97 * hash + Objects.hashCode(this.selectionMarkerColor);
		hash = 97 * hash + Objects.hashCode(this.selectionCaretColor);
		hash = 97 * hash + Objects.hashCode(this.fontColor);
		hash = 97 * hash + Objects.hashCode(this.consensusColor);
		hash = 97 * hash + Objects.hashCode(this.groupColors);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final SequenceBundleColorModel other = (SequenceBundleColorModel) obj;
		if (!Objects.equals(this.selectionColor, other.selectionColor)) {
			return false;
		}
		if (!Objects.equals(this.backgroundColor, other.backgroundColor)) {
			return false;
		}
		if (!Objects.equals(this.selectionMarkerColor, other.selectionMarkerColor)) {
			return false;
		}
		if (!Objects.equals(this.selectionCaretColor, other.selectionCaretColor)) {
			return false;
		}
		if (!Objects.equals(this.fontColor, other.fontColor)) {
			return false;
		}
		if (!Objects.equals(this.consensusColor, other.consensusColor)) {
			return false;
		}
		if (!Objects.equals(this.groupColors, other.groupColors)) {
			return false;
		}
		return true;
	}

	public void setSelectionColor(Color selectionColor) {
		java.awt.Color oldSelectionColor = this.selectionColor;
		this.selectionColor = selectionColor;
		propertyChangeSupport.firePropertyChange(PROP_SELECTIONCOLOR, oldSelectionColor, selectionColor);
	}

	public void setBackgroundColor(Color backgroundColor) {
		java.awt.Color oldBackgroundColor = this.backgroundColor;
		this.backgroundColor = backgroundColor;
		propertyChangeSupport.firePropertyChange(PROP_BACKGROUNDCOLOR, oldBackgroundColor, backgroundColor);
	}

	public void setSelectionMarkerColor(Color selectionMarkerColor) {
		java.awt.Color oldSelectionMarkerColor = this.selectionMarkerColor;
		this.selectionMarkerColor = selectionMarkerColor;
		propertyChangeSupport.firePropertyChange(PROP_SELECTIONMARKERCOLOR, oldSelectionMarkerColor, selectionMarkerColor);
	}

	public void setSelectionCaretColor(Color selectionCaretColor) {
		java.awt.Color oldSelectionCaretColor = this.selectionCaretColor;
		this.selectionCaretColor = selectionCaretColor;
		propertyChangeSupport.firePropertyChange(PROP_SELECTIONCARETCOLOR, oldSelectionCaretColor, selectionCaretColor);
	}

	public void setFontColor(Color fontColor) {
		java.awt.Color oldFontColor = this.fontColor;
		this.fontColor = fontColor;
		propertyChangeSupport.firePropertyChange(PROP_FONTCOLOR, oldFontColor, fontColor);
	}

	public void setConsensusColor(Color consensusColor) {
		java.awt.Color oldConsensusColor = this.consensusColor;
		this.consensusColor = consensusColor;
		propertyChangeSupport.firePropertyChange(PROP_CONSENSUSCOLOR, oldConsensusColor, consensusColor);
	}

	public void addPropertyChangeListener(PropertyChangeListener listener) {
		propertyChangeSupport.addPropertyChangeListener(listener);
	}

	public void removePropertyChangeListener(PropertyChangeListener listener) {
		propertyChangeSupport.removePropertyChangeListener(listener);
	}

	public void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
		propertyChangeSupport.addPropertyChangeListener(propertyName, listener);
	}

	public void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
		propertyChangeSupport.removePropertyChangeListener(propertyName, listener);
	}

	private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();
		propertyChangeSupport = new PropertyChangeSupport(this);
    }	

	private void writeObject(ObjectOutputStream out) throws IOException {
		out.defaultWriteObject();
	}
	
}
