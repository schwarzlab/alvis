/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.sequencebundle;

import com.general.mvc.view.IView;
import java.awt.BorderLayout;
import java.awt.Component;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

/**
 *
 * @author Roland Schwarz <rfs32@cam.ac.uk>
 */
public class SequenceBundleConfigPanel extends JPanel implements IView{
	final SequenceBundleConfig bundleConfig;
	
	private SequenceBundleRenderOptionsPanel optionsPanel;
	private SequenceBundleColorPanel colorPanel;
	
	public SequenceBundleConfigPanel() { 
		this(new SequenceBundleConfig());
	}
	
	public SequenceBundleConfigPanel(SequenceBundleConfig bundleConfig) {
		this.bundleConfig = bundleConfig;
		this.optionsPanel = new  SequenceBundleRenderOptionsPanel(bundleConfig);
		this.colorPanel = new SequenceBundleColorPanel(bundleConfig.getColorModel());
		init();
	}

	private void init() {
		JTabbedPane pane = new JTabbedPane();
		pane.addTab("Rendering", optionsPanel);
		pane.addTab("Colors", colorPanel);
		this.setLayout(new BorderLayout());
		this.add(pane, BorderLayout.CENTER);
	}

	@Override
	public void save() {
		bundleConfig.setCellHeight(optionsPanel.modelCellHeight.getNumber().intValue());
		bundleConfig.setCellWidth(optionsPanel.modelCellWidth.getNumber().intValue());
		bundleConfig.setDpi(optionsPanel.modelDpi.getNumber().intValue());
		bundleConfig.setHorizontalExtent(optionsPanel.modelHorizontalExtent.getNumber().floatValue());
		bundleConfig.setMaxBundleWidth(optionsPanel.modelMaxBundleWidth.getNumber().intValue());
		bundleConfig.setOffsetY(optionsPanel.modelOffsetY.getNumber().intValue());
		bundleConfig.setTangL(optionsPanel.modelTangL.getNumber().intValue());
		bundleConfig.setTangR(optionsPanel.modelTangR.getNumber().intValue());
		bundleConfig.setAntiAliasing(optionsPanel.chkAntiAliasing.isSelected());
		bundleConfig.setSpeedOverQuality(!optionsPanel.chkSpeed.isSelected());
		bundleConfig.setShowingHorizontalLines(optionsPanel.chkHorizontalLines.isSelected());
		bundleConfig.setShowingVerticalLines(optionsPanel.chkVerticalLines.isSelected());
		bundleConfig.setShowingConsensus(optionsPanel.chkShowConsensus.isSelected());
		bundleConfig.setShowingSelection(optionsPanel.chkShowSelection.isSelected());
		bundleConfig.setShowingOverlay(optionsPanel.chkAlphabetOverlay.isSelected());
		bundleConfig.setGapRendering((SequenceBundleConfig.GapRenderingType)optionsPanel.modelGapRendering.getSelectedItem());
		bundleConfig.setGroupStacking((SequenceBundleConfig.GroupStackingType)optionsPanel.modelGroupStacking.getSelectedItem());
		bundleConfig.setMinAlphaPerThread(optionsPanel.modelAlphaMin.getValue() / 100.0);
		bundleConfig.setMaxAlphaTotal(optionsPanel.modelAlphaMax.getValue() / 100.0);
		bundleConfig.setConservationThreshold(optionsPanel.modelConservationThreshold.getNumber().doubleValue() / 100.0);
		
		bundleConfig.getColorModel().getAlphabetColors().putAll(colorPanel.getModel().getAlphabetColors());
		bundleConfig.getColorModel().getGroupColors().putAll(colorPanel.getModel().getGroupColors());
		bundleConfig.getColorModel().setBackgroundColor(colorPanel.getModel().getBackgroundColor());
		bundleConfig.getColorModel().setConsensusColor(colorPanel.getModel().getConsensusColor());
		bundleConfig.getColorModel().setFontColor(colorPanel.getModel().getFontColor());
		bundleConfig.getColorModel().setSelectionCaretColor(colorPanel.getModel().getSelectionCaretColor());
		bundleConfig.getColorModel().setSelectionColor(colorPanel.getModel().getSelectionColor());
		bundleConfig.getColorModel().setSelectionMarkerColor(colorPanel.getModel().getSelectionMarkerColor());

		
	}

	@Override
	public boolean verify() {
		return true;
	}

	@Override
	public Component getComponent() {
		return this;
	}

	@Override
	public String getTitle() {
		return "Sequence Bundle configuration";
	}

	@Override
	public void closing() {
	}

	public SequenceBundleConfig getBundleConfig() {
		return bundleConfig;
	}
	
	
}
