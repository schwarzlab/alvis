/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.sequencebundle;

import com.general.containerModel.Nameable;
import com.general.containerModel.impl.MapDefaultValueDecorator;
import de.biozentrum.bioinformatik.alignment.visibility.SequenceVisibilityModel;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 *
 * @author Roland Schwarz <rfs32@cam.ac.uk>
 */
public class SequenceBundleConfig implements Serializable {
	public enum GapRenderingType implements Nameable{
		STANDARD ("standard", "Gaps are treated like other residues and connected to the bottom of the bundle."),
        DISCONNECTED("disconnected", "Gap columns are disconnected from the bundle and placed at the bottom.");
        private final String name;
        private final String description;

        GapRenderingType(String name, String description) {
			this.name=name;
            this.description = description;
        }

        @Override
        public String getDescription() {
            return description;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public String toString() {
            return name;
        }
    }

    public enum GroupStackingType implements Nameable {
		SEPARATE ("separate", "Groups are kept separate from each other within stack in addition to having their own color."),
		OVERLAYED ("overlayed", "Groups are rendered on top of each other blending the colors.");
        private final String name;
        private final String description;

        GroupStackingType(String name, String description) {
			this.name=name;
            this.description = description;
        }

        @Override
        public String getDescription() {
            return description;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public String toString() {
            return name;
        }

    }

	private final static long serialVersionUID=1L;
    public static final String PROP_GAPRENDERING = "PROP_GAPRENDERING";
    public static final String PROP_GAPCHAR = "PROP_GAPCHAR";
    public static final String PROP_BUNDLEINDENT = "PROP_BUNDLEINDENT";
    public static final String PROP_CELLWIDTH = "PROP_CELLWIDTH";
    public static final String PROP_CELLHEIGHT = "PROP_CELLHEIGHT";
    public static final String PROP_MAXBUNDLEWIDTH = "PROP_MAXBUNDLEWIDTH";
    public static final String PROP_TANGL = "PROP_TANGL"; // bundle curvature left and right
    public static final String PROP_TANGR = "PROP_TANGR";
    public static final String PROP_HORIZONTALEXTENT = "PROP_HORIZONTALEXTENT"; // amount of horizontal "straightness" in the curve in pixel
	public static final String PROP_SELECTIONENABLED="PROP_SELECTIONENABLED";	
	public static final String PROP_AUTOMATICREDRAW="PROP_AUTOMATICREDRAW";
	public static final String PROP_SHOWINGCONSENSUS="PROP_SHOWCONSENSUS";
	public static final String PROP_SHOWINGSELECTION="PROP_SHOWSELECTION";
    public static final String PROP_COLORMODEL = "PROP_COLORMODEL";
    public static final String PROP_SEQUENCEVISIBILITYMODEL = "PROP_SEQUENCEVISIBILITYMODEL";
    public static final String PROP_GROUPVISIBILITY = "PROP_GROUPVISIBILITY";
    public static final String PROP_DPI = "PROP_DPI";
    public static final String PROP_OFFSETY = "PROP_OFFSETY";
    public static final String PROP_ANTIALIASING = "PROP_ANTIALIASING";
    public static final String PROP_SPEEDOVERQUALITY = "PROP_SPEEDOVERQUALITY";
    public static final String PROP_SHOWINGHORIZONTALLINES = "PROP_SHOWINGHORIZONTALLINES";
    public static final String PROP_SHOWINGVERTICALLINES = "PROP_SHOWINGVERTICALLINES";
    public static final String PROP_SHOWINGOVERLAY = "PROP_SHOWINGOVERLAY";
    public static final String PROP_COLUMNOFFSET = "PROP_COLUMNOFFSET";
    public static final String PROP_GROUPSTACKING = "PROP_GROUPSTACKING";
    public static final String PROP_MINALPHAPERTHREAD = "PROP_MINALPHAPERTHREAD";
    public static final String PROP_MAXALPHATOTAL = "PROP_MAXALPHATOTAL";
    public static final String PROP_CONSERVATIONTHRESHOLD = "PROP_CONSERVATIONTHRESHOLD";

    public static final char DEFAULT_GAPCHAR = '-';

    public static final int BUNDLEINDENT_MIN = 0;
    public static final int BUNDLEINDENT_MAX = 10000;
    public static final int CELLWIDTH_MIN = 5;
    public static final int CELLWIDTH_MAX = 1000;
    public static final int CELLHEIGHT_MIN = 5;
    public static final int CELLHEIGHT_MAX = 1000;
    public static final int MAXBUNDLEWIDTH_MIN = 5;
    public static final int MAXBUNDLEWIDTH_MAX = 500;
    public static final int TANG_MIN = 0;
    public static final int TANG_MAX = 10000;
    public static final double HORIZONTALEXTENT_MIN = 0.0;
    public static final double HORIZONTALEXTEND_MAX = 2.0;
    public static final int DPI_MIN = 36;
    public static final int DPI_MAX = 1600;
    public static final double DEFAULT_DPI = 72.0; // must be double
    public static final int OFFSETY_MIN = 0;
    public static final int OFFSETY_MAX = 10000;
    public static final double ALPHA_MIN = 0.01;
    public static final double ALPHA_MAX = 0.90;
    public static final double CONSERVATION_THRESHOLD_MIN = 0.00;
    public static final double CONSERVATION_THRESHOLD_MAX = 1.00;


    char gapChar = '-';
	int bundleIndent = 150;
    int cellWidth = 60;
    int maxBundleWidth = 16;
    int cellHeight = maxBundleWidth + 4;
	int dpi = (int)DEFAULT_DPI;
    int tangL = 18;
    int tangR = 18;
    float horizontalExtent = 0.33f;
    boolean selectionEnabled = true;
    boolean automaticRedraw = true;
    boolean showingConsensus = false;
    boolean showingSelection = true;
    SequenceBundleColorModel colorModel = new SequenceBundleColorModel();
    transient SequenceVisibilityModel sequenceVisibilityModel = new SequenceVisibilityModel();
    Map<Integer, Boolean> groupVisibility = new MapDefaultValueDecorator<>(new HashMap<Integer, Boolean>(), true);
    int offsetY = 40;
    boolean antiAliasing = true;
    boolean speedOverQuality = false;
    int[] columnOffset = new int[]{0};
    boolean showingHorizontalLines = false;
    boolean showingVerticalLines = true;
    boolean showingOverlay = false;
    GapRenderingType gapRendering = GapRenderingType.STANDARD;
    GroupStackingType groupStacking = GroupStackingType.SEPARATE;
    double minAlphaPerThread = 0.01;
    double maxAlphaTotal = 0.9;
    double conservationThreshold = 0.0;

    transient PropertyChangeSupport propertyChangeSupport = new java.beans.PropertyChangeSupport(this);

	public SequenceBundleConfig() {}
    public SequenceBundleConfig(SequenceBundleConfig other) {
		this.automaticRedraw=other.automaticRedraw;
		this.bundleIndent=other.bundleIndent;
		this.cellHeight=other.cellHeight;
		this.cellWidth=other.cellWidth;
		this.colorModel=new SequenceBundleColorModel(other.colorModel);
        this.dpi = other.dpi;
        this.gapChar = other.gapChar;
        this.groupVisibility.putAll(other.groupVisibility);
        this.gapRendering = other.gapRendering;
        this.horizontalExtent = other.horizontalExtent;
		this.maxBundleWidth =  other.maxBundleWidth;
        this.selectionEnabled = other.selectionEnabled;
        this.sequenceVisibilityModel = new SequenceVisibilityModel(); // sequence visibility currently not copied
        this.showingConsensus = other.showingConsensus;
        this.showingSelection = other.showingSelection;
        this.tangL = other.tangL;
        this.tangR = other.tangR;
		this.offsetY  = other.offsetY;
        this.antiAliasing = other.antiAliasing;
        this.speedOverQuality = other.speedOverQuality;
        this.showingHorizontalLines = other.showingHorizontalLines;
        this.showingVerticalLines = other.showingVerticalLines;
        this.showingOverlay = other.showingOverlay;
        this.columnOffset = Arrays.copyOf(other.columnOffset, other.columnOffset.length);
		this.gapRendering = other.gapRendering;
		this.conservationThreshold = other.conservationThreshold;
    }

    @Override
    public boolean equals(Object obj) {
		if (!(obj instanceof SequenceBundleConfig)) return false;
		SequenceBundleConfig other =  (SequenceBundleConfig)obj;

		boolean retval =
				this.automaticRedraw==other.automaticRedraw &&
				this.bundleIndent==other.bundleIndent &&
				this.cellHeight==other.cellHeight &&
				this.cellWidth==other.cellWidth &&
				this.colorModel.equals(other.colorModel) &&
				this.dpi == other.dpi &&
				this.gapChar == other.gapChar &&
				this.groupVisibility.equals(other.groupVisibility) &&
				this.horizontalExtent == other.horizontalExtent &&
				this.maxBundleWidth == other.maxBundleWidth &&
				this.selectionEnabled == other.selectionEnabled &&
				this.sequenceVisibilityModel.equals(other.sequenceVisibilityModel) &&
				this.showingConsensus == other.showingConsensus &&
				this.showingSelection == other.showingSelection &&
				this.tangL == other.tangL &&
				this.tangR == other.tangR &&
				this.offsetY  == other.offsetY &&
				this.antiAliasing == other.antiAliasing &&
				this.speedOverQuality == other.speedOverQuality &&
				this.showingHorizontalLines == other.showingHorizontalLines &&
				this.showingVerticalLines == other.showingVerticalLines &&
				this.showingOverlay == other.showingOverlay &&
				Arrays.equals(this.columnOffset, other.columnOffset) &&
				this.gapRendering.equals(other.gapRendering) &&
				this.conservationThreshold == other.conservationThreshold;
		
        return retval;

    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + this.gapChar;
        hash = 59 * hash + this.bundleIndent;
        hash = 59 * hash + this.cellWidth;
        hash = 59 * hash + this.maxBundleWidth;
        hash = 59 * hash + this.cellHeight;
        hash = 59 * hash + this.dpi;
        hash = 59 * hash + this.tangL;
        hash = 59 * hash + this.tangR;
        hash = 59 * hash + Float.floatToIntBits(this.horizontalExtent);
        hash = 59 * hash + (this.selectionEnabled ? 1 : 0);
        hash = 59 * hash + (this.automaticRedraw ? 1 : 0);
        hash = 59 * hash + (this.showingConsensus ? 1 : 0);
        hash = 59 * hash + (this.showingSelection ? 1 : 0);
        hash = 59 * hash + Objects.hashCode(this.colorModel);
        hash = 59 * hash + Objects.hashCode(this.sequenceVisibilityModel);
        hash = 59 * hash + Objects.hashCode(this.groupVisibility);
        hash = 59 * hash + this.offsetY;
        hash = 59 * hash + (this.antiAliasing ? 1 : 0);
        hash = 59 * hash + (this.speedOverQuality ? 1 : 0);
        hash = 59 * hash + (this.showingHorizontalLines ? 1 : 0);
        hash = 59 * hash + (this.showingVerticalLines ? 1 : 0);
        hash = 59 * hash + (this.showingOverlay ? 1 : 0);
        hash = 59 * hash + Objects.hashCode(this.columnOffset);
		hash = 59 * hash + Objects.hashCode(this.gapRendering);
		hash = 59 * hash + Double.valueOf(this.conservationThreshold).hashCode();
        return hash;
    }


	
    public GapRenderingType getGapRendering() {
        return gapRendering;
    }

    public void setGapRendering(GapRenderingType gapRendering) {
        gui.sequencebundle.SequenceBundleConfig.GapRenderingType oldGapRendering = this.gapRendering;
        this.gapRendering = gapRendering;
        propertyChangeSupport.firePropertyChange(PROP_GAPRENDERING, oldGapRendering, gapRendering);
    }

    public char getGapChar() {
        return gapChar;
    }

    public void setGapChar(char gapChar) {
        char oldGapChar = this.gapChar;
        this.gapChar = gapChar;
        propertyChangeSupport.firePropertyChange(PROP_GAPCHAR, oldGapChar, gapChar);
    }

    public int getBundleIndent() {
        return bundleIndent;
    }

    public void setBundleIndent(int bundleIndent) {
        int oldBundleIndent = this.bundleIndent;
//		this.bundleIndent = bundleIndent + cellWidth/2;
        this.bundleIndent = bundleIndent;
        propertyChangeSupport.firePropertyChange(PROP_BUNDLEINDENT, oldBundleIndent, bundleIndent);
    }

    public int getCellWidth() {
        return cellWidth;
    }

    public void setCellWidth(int cellWidth) {
        int oldCellWidth = this.cellWidth;
        this.cellWidth = cellWidth;
        propertyChangeSupport.firePropertyChange(PROP_CELLWIDTH, oldCellWidth, cellWidth);
    }

    public int getCellHeight() {
        return cellHeight;
    }

    public void setCellHeight(int cellHeight) {
        int oldCellHeight = this.cellHeight;
        this.cellHeight = cellHeight;
        propertyChangeSupport.firePropertyChange(PROP_CELLHEIGHT, oldCellHeight, cellHeight);
    }

    public int getMaxBundleWidth() {
        return maxBundleWidth;
    }

    public void setMaxBundleWidth(int maxBundleWidth) {
        int oldMaxBundleWidth = this.maxBundleWidth;
        this.maxBundleWidth = maxBundleWidth;
        propertyChangeSupport.firePropertyChange(PROP_MAXBUNDLEWIDTH, oldMaxBundleWidth, maxBundleWidth);
    }

    public int getTangL() {
        return tangL;
    }

    public void setTangL(int tangL) {
        int oldTangL = this.tangL;
        this.tangL = tangL;
        propertyChangeSupport.firePropertyChange(PROP_TANGL, oldTangL, tangL);
    }

    public int getTangR() {
        return tangR;
    }

    public void setTangR(int tangR) {
        int oldTangR = this.tangR;
        this.tangR = tangR;
        propertyChangeSupport.firePropertyChange(PROP_TANGR, oldTangR, tangR);
    }

    public float getHorizontalExtent() {
        return horizontalExtent;
    }

    public void setHorizontalExtent(float horizontalExtent) {
        // horizontal span of each curve in fraction of the cell width
        float oldHorizontalExtent = this.horizontalExtent;
        this.horizontalExtent = horizontalExtent;
        propertyChangeSupport.firePropertyChange(PROP_HORIZONTALEXTENT, oldHorizontalExtent, horizontalExtent);
    }

    public boolean isSelectionEnabled() {
        return selectionEnabled;
    }

    public void setSelectionEnabled(boolean selectionEnabled) {
        boolean oldSelectionEnabled = this.selectionEnabled;
        this.selectionEnabled = selectionEnabled;
        propertyChangeSupport.firePropertyChange(PROP_SELECTIONENABLED, oldSelectionEnabled, selectionEnabled);
    }

    public boolean isAutomaticRedraw() {
        return automaticRedraw;
    }

    public void setAutomaticRedraw(boolean automaticRedraw) {
        boolean oldAutomaticRedraw = this.automaticRedraw;
        this.automaticRedraw = automaticRedraw;
        propertyChangeSupport.firePropertyChange(PROP_AUTOMATICREDRAW, oldAutomaticRedraw, automaticRedraw);
    }

    public boolean isShowingConsensus() {
        return showingConsensus;
    }

    public void setShowingConsensus(boolean showingConsensus) {
        boolean oldShowConsensus = this.showingConsensus;
        this.showingConsensus = showingConsensus;
        propertyChangeSupport.firePropertyChange(PROP_SHOWINGCONSENSUS, oldShowConsensus, showingConsensus);
    }

    public boolean isShowingSelection() {
        return showingSelection;
    }

    public void setShowingSelection(boolean showingSelection) {
        boolean oldShowSelection = this.showingSelection;
        this.showingSelection = showingSelection;
        propertyChangeSupport.firePropertyChange(PROP_SHOWINGSELECTION, oldShowSelection, showingSelection);
    }

    public SequenceBundleColorModel getColorModel() {
        return colorModel;
    }

    public void setColorModel(SequenceBundleColorModel colorModel) {
        gui.sequencebundle.SequenceBundleColorModel oldColorModel = this.colorModel;
        this.colorModel = colorModel;
        propertyChangeSupport.firePropertyChange(PROP_COLORMODEL, oldColorModel, colorModel);
    }

    public SequenceVisibilityModel getSequenceVisibilityModel() {
        return sequenceVisibilityModel;
    }

    public void setSequenceVisibilityModel(SequenceVisibilityModel sequenceVisibilityModel) {
        de.biozentrum.bioinformatik.alignment.visibility.SequenceVisibilityModel oldSequenceVisibilityModel = this.sequenceVisibilityModel;
        this.sequenceVisibilityModel = sequenceVisibilityModel;
        propertyChangeSupport.firePropertyChange(PROP_SEQUENCEVISIBILITYMODEL, oldSequenceVisibilityModel, sequenceVisibilityModel);
    }

    public Map<Integer, Boolean> getGroupVisibility() {
        return groupVisibility;
    }

	public boolean isShowingGroup(int group){
        return groupVisibility.get(group);
    }

	public void setShowingGroup(int group, boolean value){
        Boolean oldval = groupVisibility.get(group);
        groupVisibility.put(group, value);
		propertyChangeSupport.firePropertyChange(PROP_GROUPVISIBILITY, oldval, (Boolean)value);
    }


    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }

    public void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(propertyName, listener);
    }

    public void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(propertyName, listener);
    }

    public int getDpi() {
        return dpi;
    }

    public void setDpi(int dpi) {
        int oldDpi = this.dpi;
        this.dpi = dpi;
        propertyChangeSupport.firePropertyChange(PROP_DPI, oldDpi, dpi);
    }

    public int getOffsetY() {
        return offsetY;
    }

    public void setOffsetY(int offsetY) {
        int oldOffsetY = this.offsetY;
        this.offsetY = offsetY;
        propertyChangeSupport.firePropertyChange(PROP_OFFSETY, oldOffsetY, offsetY);
    }

    public boolean getAntiAliasing() {
        return antiAliasing;
    }

    public void setAntiAliasing(boolean antiAliasing) {
        boolean oldAntiAliasing = this.antiAliasing;
        this.antiAliasing = antiAliasing;
        propertyChangeSupport.firePropertyChange(PROP_ANTIALIASING, oldAntiAliasing, antiAliasing);
    }

    public boolean getSpeedOverQuality() {
        return speedOverQuality;
    }

    public void setSpeedOverQuality(boolean speedOverQuality) {
        boolean oldSpeedOverQuality = this.speedOverQuality;
        this.speedOverQuality = speedOverQuality;
        propertyChangeSupport.firePropertyChange(PROP_SPEEDOVERQUALITY, oldSpeedOverQuality, speedOverQuality);
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        this.sequenceVisibilityModel = new SequenceVisibilityModel();
        this.propertyChangeSupport = new PropertyChangeSupport(this);
    }

    private void writeObject(ObjectOutputStream out) throws IOException {
        out.defaultWriteObject();
    }

    public boolean isShowingHorizontalLines() {
        return showingHorizontalLines;
    }

    public void setShowingHorizontalLines(boolean showingHorizontalLines) {
        boolean oldShowingHorizontalLines = this.showingHorizontalLines;
        this.showingHorizontalLines = showingHorizontalLines;
        propertyChangeSupport.firePropertyChange(PROP_SHOWINGHORIZONTALLINES, oldShowingHorizontalLines, showingHorizontalLines);
    }

    public boolean isShowingVerticalLines() {
        return showingVerticalLines;
    }

    public void setShowingVerticalLines(boolean showingVerticalLines) {
        boolean oldShowingVerticalLines = this.showingVerticalLines;
        this.showingVerticalLines = showingVerticalLines;
        propertyChangeSupport.firePropertyChange(PROP_SHOWINGVERTICALLINES, oldShowingVerticalLines, showingVerticalLines);
    }

    public boolean isShowingOverlay() {
        return showingOverlay;
    }

    public void setShowingOverlay(boolean showingOverlay) {
        boolean oldShowingOverlay = this.showingOverlay;
        this.showingOverlay = showingOverlay;
        propertyChangeSupport.firePropertyChange(PROP_SHOWINGOVERLAY, oldShowingOverlay, showingOverlay);
    }

    public int[] getColumnOffset() {
        return columnOffset;
    }

    public void setColumnOffset(int[] columnOffset) {
        int[] oldColumnOffset = this.columnOffset;
        if (columnOffset == null) {
            columnOffset = new int[]{0};
        }
        this.columnOffset = columnOffset;
        propertyChangeSupport.firePropertyChange(PROP_COLUMNOFFSET, oldColumnOffset, columnOffset);
    }

    public GroupStackingType getGroupStacking() {
        return groupStacking;
    }

    public void setGroupStacking(GroupStackingType groupStacking) {
        gui.sequencebundle.SequenceBundleConfig.GroupStackingType oldGroupStacking = this.groupStacking;
        this.groupStacking = groupStacking;
        propertyChangeSupport.firePropertyChange(PROP_GROUPSTACKING, oldGroupStacking, groupStacking);
    }

    public double getMinAlphaPerThread() {
        return minAlphaPerThread;
    }

    public void setMinAlphaPerThread(double minAlphaPerThread) {
        double oldMinAlphaPerThread = this.minAlphaPerThread;
        this.minAlphaPerThread = minAlphaPerThread;
        propertyChangeSupport.firePropertyChange(PROP_MINALPHAPERTHREAD, oldMinAlphaPerThread, minAlphaPerThread);
    }

    public double getMaxAlphaTotal() {
        return maxAlphaTotal;
    }

    public void setMaxAlphaTotal(double maxAlphaTotal) {
        double oldMaxAlphaTotal = this.maxAlphaTotal;
        this.maxAlphaTotal = maxAlphaTotal;
        propertyChangeSupport.firePropertyChange(PROP_MAXALPHATOTAL, oldMaxAlphaTotal, maxAlphaTotal);
    }

    public double getConservationThreshold() {
        return conservationThreshold;
    }

    public void setConservationThreshold(double conservationThreshold) {
        double oldConservationThreshold = this.conservationThreshold;
        this.conservationThreshold = conservationThreshold;
        propertyChangeSupport.firePropertyChange(PROP_CONSERVATIONTHRESHOLD, oldConservationThreshold, conservationThreshold);
    }

}
