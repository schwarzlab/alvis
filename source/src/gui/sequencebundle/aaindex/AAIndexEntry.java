/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.sequencebundle.aaindex;

import com.dictiography.collections.IndexedNavigableMap;
import com.dictiography.collections.IndexedTreeMap;
import com.general.containerModel.Nameable;
import java.util.Map;
import java.util.Objects;

/**
 *
 ************************************************************************
 *                                                                      *
 * Each entry has the following format.                                 *
 *                                                                      *
 * H Accession number                                                   *
 * D Data description                                                   *
 * R LITDB entry number                                                 *
 * A Author(s)                                                          *
 * T Title of the article                                               *
 * J Journal reference                                                  *
 * * Comment or missing                                                 *
 * C Accession numbers of similar entries with the correlation          *
 *   coefficients of 0.8 (-0.8) or more (less).                         *
 *   Notice: The correlation coefficient is calculated with zeros       *
 *   filled for missing values.                                         *
 * I Amino acid index data in the following order                       *
 *   Ala    Arg    Asn    Asp    Cys    Gln    Glu    Gly    His    Ile *
 *   Leu    Lys    Met    Phe    Pro    Ser    Thr    Trp    Tyr    Val *
 * //                                                                   *
 ************************************************************************
 * @author Roland Schwarz <rfs32@cam.ac.uk>
 */
public class AAIndexEntry implements Nameable, Comparable{
	private String accessionNumber;
	private String dataDescription;
	private String litdbEntryNumber;
	private String author;
	private String articleTitle;
	private String journalReference;
	private String relatedEntries;
	private final IndexedNavigableMap<Character, Double> aminoAcidIndices;

	public AAIndexEntry(String accessionNumber, String dataDescription, String litdbEntryNumber, String author, String articleTitle, String journalReference, String relatedEntries, Map<Character, Double> aminoAcidIndices) {
		this.accessionNumber = accessionNumber;
		this.dataDescription = dataDescription;
		this.litdbEntryNumber = litdbEntryNumber;
		this.author = author;
		this.articleTitle = articleTitle;
		this.journalReference = journalReference;
		this.relatedEntries = relatedEntries;
		this.aminoAcidIndices = new IndexedTreeMap<>();
		this.aminoAcidIndices.putAll(aminoAcidIndices);
	}

	public AAIndexEntry(String accessionNumber, String dataDescription, String litdbEntryNumber, String author, String articleTitle, String journalReference, String relatedEntries) {
		this.accessionNumber = accessionNumber;
		this.dataDescription = dataDescription;
		this.litdbEntryNumber = litdbEntryNumber;
		this.author = author;
		this.articleTitle = articleTitle;
		this.journalReference = journalReference;
		this.relatedEntries = relatedEntries;
		this.aminoAcidIndices = new IndexedTreeMap<>();
	}
	
	public AAIndexEntry(String accessionNumber){
		this.accessionNumber = accessionNumber;
		this.dataDescription="";
		this.litdbEntryNumber="";
		this.author="";
		this.articleTitle="";
		this.journalReference="";
		this.relatedEntries ="";
		this.aminoAcidIndices = new IndexedTreeMap<>();
	}
	
	/**
	 * @return the accessionNumber
	 */
	public String getAccessionNumber() {
		return accessionNumber;
	}

	/**
	 * @param accessionNumber the accessionNumber to set
	 */
	public void setAccessionNumber(String accessionNumber) {
		this.accessionNumber = accessionNumber;
	}

	/**
	 * @return the dataDescription
	 */
	public String getDataDescription() {
		return dataDescription;
	}

	/**
	 * @param dataDescription the dataDescription to set
	 */
	public void setDataDescription(String dataDescription) {
		this.dataDescription = dataDescription;
	}

	/**
	 * @return the litdbEntryNumber
	 */
	public String getLitdbEntryNumber() {
		return litdbEntryNumber;
	}

	/**
	 * @param litdbEntryNumber the litdbEntryNumber to set
	 */
	public void setLitdbEntryNumber(String litdbEntryNumber) {
		this.litdbEntryNumber = litdbEntryNumber;
	}

	/**
	 * @return the author
	 */
	public String getAuthor() {
		return author;
	}

	/**
	 * @param author the author to set
	 */
	public void setAuthor(String author) {
		this.author = author;
	}

	/**
	 * @return the articleTitle
	 */
	public String getArticleTitle() {
		return articleTitle;
	}

	/**
	 * @param articleTitle the articleTitle to set
	 */
	public void setArticleTitle(String articleTitle) {
		this.articleTitle = articleTitle;
	}

	/**
	 * @return the journalReference
	 */
	public String getJournalReference() {
		return journalReference;
	}

	/**
	 * @param journalReference the journalReference to set
	 */
	public void setJournalReference(String journalReference) {
		this.journalReference = journalReference;
	}

	/**
	 * @return the relatedEntries
	 */
	public String getRelatedEntries() {
		return relatedEntries;
	}

	/**
	 * @param relatedEntries the relatedEntries to set
	 */
	public void setRelatedEntries(String relatedEntries) {
		this.relatedEntries = relatedEntries;
	}

	/**
	 * @return the aminoAcidIndices
	 */
	public IndexedNavigableMap<Character, Double> getAminoAcidIndices() {
		return aminoAcidIndices;
	}

	@Override
	public String getName() {
		return this.accessionNumber;
	}

	@Override
	public String getDescription() {
		return this.dataDescription;
	}

	@Override
	public int compareTo(Object o) {
		return this.accessionNumber.compareTo(((AAIndexEntry)o).accessionNumber);
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 31 * hash + Objects.hashCode(this.accessionNumber);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final AAIndexEntry other = (AAIndexEntry) obj;
		if (!Objects.equals(this.accessionNumber, other.accessionNumber)) {
			return false;
		}
		return true;
	}

	
}
