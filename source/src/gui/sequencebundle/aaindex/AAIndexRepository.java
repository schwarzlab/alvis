/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.sequencebundle.aaindex;

import com.dictiography.collections.IndexedTreeSet;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Roland Schwarz <rfs32@cam.ac.uk>
 */
public class AAIndexRepository {
	private static final String DATABASE_FILE = "/res/aaindex1/aaindex1";
	private static final Character[] AA_ORDER = new Character[] {'A', 'R', 'N', 'D', 'C', 'Q', 'E', 'G', 'H', 'I', 'L', 'K', 'M', 'F', 'P', 'S', 'T', 'W', 'Y', 'V'};
	public static final IndexedTreeSet<AAIndexEntry> AAIndices = new IndexedTreeSet<>();
	
	private AAIndexRepository() {}
	
	static {
		try {
			loadAAIndices();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
	
	public static void init(){} // to force loading if desired
	private static void loadAAIndices() throws IOException{
		URL dbURL = AAIndexRepository.class.getResource(DATABASE_FILE);
		loadAAIndices(dbURL);
	}
	
	private static void loadAAIndices(URL dbURL) throws IOException{
		BufferedReader input = new BufferedReader(new InputStreamReader(dbURL.openStream()));		
		String line;
		List<String> block=new ArrayList<>();
		while((line=input.readLine())!=null){
			if (!line.trim().equals("//")){ // collect data for block
				block.add(line);
			} else { // parse block and reset
				AAIndexEntry entry = parseBlock(block);
				AAIndices.add(entry);
				block.clear();
			}
		}
	}

	private static AAIndexEntry parseBlock(List<String> block) {
		StringBuilder accessionNumber = new StringBuilder();
		StringBuilder dataDescription = new StringBuilder();
		StringBuilder litdbEntryNumber = new StringBuilder();
		StringBuilder author = new StringBuilder();
		StringBuilder articleTitle = new StringBuilder();
		StringBuilder journalReference = new StringBuilder();
		StringBuilder relatedEntries = new StringBuilder();
		StringBuilder aminoAcidIndices = new StringBuilder();
		
		String previousSection="";
		boolean firstLineAAI=true;
		for (String line:block){
			String section = line.substring(0, 1);
			String data = line.substring(1).trim();			
			if (section.equals(" ")) {
				section=previousSection;
				data = " " + data;
			}
			
			if (section.equals("H")){
				accessionNumber.append(data);
			} else if (section.equals("D")) {
				dataDescription.append(data);
			} else if (section.equals("R")) {
				litdbEntryNumber.append(data);
			} else if (section.equals("A")) {
				author.append(data);
			} else if (section.equals("T")) {
				articleTitle.append(data);
			} else if (section.equals("J")) {
				journalReference.append(data);
			} else if (section.equals("C")) {
				relatedEntries.append(data);
			} else if (section.equals("I")){
				if (!firstLineAAI) {
					aminoAcidIndices.append(data);
				}
				firstLineAAI = false;				
			}
			previousSection=section;
		}
		
		Map<Character, Double> aaindices = parseAAindices(aminoAcidIndices.toString());
		AAIndexEntry entry = new AAIndexEntry(accessionNumber.toString());
		entry.setArticleTitle(articleTitle.toString());
		entry.setAuthor(author.toString());
		entry.setDataDescription(dataDescription.toString());
		entry.setJournalReference(journalReference.toString());
		entry.setLitdbEntryNumber(litdbEntryNumber.toString());
		entry.setRelatedEntries(relatedEntries.toString());
		entry.getAminoAcidIndices().putAll(aaindices);
		return entry;
	}

	private static Map<Character, Double> parseAAindices(String aminoAcidIndices) {
		String[] values = aminoAcidIndices.trim().split("\\s+");
		Map<Character, Double> aaindices = new HashMap<>();
		int count=0;
		for (String value:values){
			Double dblval;
			try {
				dblval = Double.valueOf(value);
			} catch (NumberFormatException ex){
				dblval = Double.NaN;
			}
			aaindices.put(AA_ORDER[count++], dblval);
		}
		return aaindices;
	}

	
}
