/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui.sequencebundle;

import java.awt.Rectangle;
import java.awt.geom.Area;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRenderedImage;
import org.jaitools.tiledimage.DiskMemImage;

/**
 *
 * @author Roland Schwarz <rfs32@cam.ac.uk>
 */
final public class SequenceBundleRendererCache {
	private final SequenceBundleRenderer renderer;
	DiskMemImage[] sequenceImage;
	DiskMemImage selectionImage;
	private Area sequenceCachedArea;
	private Area selectionCachedArea;
	private final JSequenceBundle bundle;
	private BufferedImage[] sequenceScreenCache;
	private BufferedImage selectionScreenCache;
	private Rectangle sequenceLastRect;
	private Rectangle selectionLastRect;
	int[] selectionIndices;
	private boolean okToStartSequenceRenderer = true;
	private boolean okToStartSelectionRenderer = true;
	
	public SequenceBundleRendererCache(JSequenceBundle bundle) {
		this.renderer = bundle.bundleRenderer;
		this.bundle = bundle;
		initCache();
	}
	
	public void initCache() {
		sequenceImage = new DiskMemImage[renderer.numberOfGroups];
		sequenceScreenCache = new BufferedImage[renderer.numberOfGroups];
		for (int i=0;i<renderer.numberOfGroups;i++) {
			sequenceImage[i] = renderer.createDiskMemImage(renderer.getDrawingAreaWidth(), renderer.getDrawingAreaHeight());
		}
		selectionImage = renderer.createDiskMemImage(renderer.getDrawingAreaWidth(), renderer.getDrawingAreaHeight());
		sequenceCachedArea = new Area();
		selectionCachedArea = new Area();
		sequenceLastRect = new Rectangle();
		selectionLastRect = new Rectangle();
	}

	private Rectangle adjustVisibleRect(Rectangle rect) {
		// first adjust the requested rect so it starts and ends on a column and is wider than the requested rect
		rect.y = 0;
		rect.height = renderer.getDrawingAreaHeight(); // caching only width atm
		
		Rectangle adjustedRect = new Rectangle(rect);
		int colFrom = renderer.xToColumn(adjustedRect.x);
		int colTo = renderer.xToColumn(adjustedRect.x + adjustedRect.width);
		adjustedRect.x = renderer.columnToXLeft(colFrom);
		adjustedRect.width = renderer.columnToXRight(colTo) - 1 - adjustedRect.x;
		return adjustedRect;
	}
	
	public BufferedImage getSelectedSequenceImage(final Rectangle rect) {
		if (this.selectionIndices==null || this.selectionIndices.length==0) return null;
		
		Rectangle adjustedRect = adjustVisibleRect(rect);
		
		if (!selectionCachedArea.contains(adjustedRect)) {
			final Area requestedArea = new Area(adjustedRect);
			requestedArea.subtract(selectionCachedArea);
			final Rectangle bounds = requestedArea.getBounds();
			int colFrom = renderer.xToColumn(bounds.x+1);
			int colTo = renderer.xToColumn(bounds.x + bounds.width);
			
			if (okToStartSelectionRenderer) {
				okToStartSelectionRenderer = false;
				// render sequences in area
				SequenceBundleRendererListener sbrl = new SequenceBundleRendererAdapter() {
					@Override
					public void renderingFinished(SequenceBundleRendererEvent e) {
						renderer.removeSequenceBundleRendererListener(this);
						selectionCachedArea.add(requestedArea);
						selectionLastRect = new Rectangle(); // clear screen cache
						bundle.repaint();
						okToStartSelectionRenderer=true;
					}

					@Override
					public void renderingCancelled(SequenceBundleRendererEvent e) {
						okToStartSelectionRenderer=true;
					}
				};
				renderer.addSequenceBundleRendererListener(sbrl);
				renderer.renderSelectedSequences(selectionImage, selectionIndices, colFrom, colTo);
			}
		}
		
		BufferedImage result;//= subImage(sequenceImage[group], rect);
		if (!selectionLastRect.equals(rect)) {
			selectionScreenCache = subImage(selectionImage, rect);
		} 
		selectionLastRect = rect;
		result = selectionScreenCache;
		return result;
		
	}
	
	public BufferedImage[] getSequenceImage(final Rectangle rect) {
//		System.out.println("Requested from " + colFrom + " to " + colTo);
		Rectangle adjustedRect = adjustVisibleRect(rect);
		
		if (!sequenceCachedArea.contains(adjustedRect)) {
			final Area requestedArea = new Area(adjustedRect);
			requestedArea.subtract(sequenceCachedArea);
			final Rectangle bounds = requestedArea.getBounds();
			int colFrom = renderer.xToColumn(bounds.x+1);
			int colTo = renderer.xToColumn(bounds.x + bounds.width);
			
			if (okToStartSequenceRenderer) {
				okToStartSequenceRenderer = false;
				// render sequences in area
				SequenceBundleRendererListener sbrl = new SequenceBundleRendererAdapter() {
					@Override
					public void renderingFinished(SequenceBundleRendererEvent e) {
						renderer.removeSequenceBundleRendererListener(this);
						sequenceCachedArea.add(requestedArea);
						sequenceLastRect = new Rectangle(); // clear screen cache
						bundle.repaint();
						okToStartSequenceRenderer = true;
					}

					@Override
					public void renderingCancelled(SequenceBundleRendererEvent e) {
						okToStartSequenceRenderer = true;
					}
				};
				renderer.addSequenceBundleRendererListener(sbrl);
				renderer.renderSequences(sequenceImage, colFrom, colTo, SequenceBundleRenderer.RenderingStrategy.FRAGMENT_RENDERER);
			}
		}
		BufferedImage[] result;//= subImage(sequenceImage[group], rect);
		if (!sequenceLastRect.equals(rect)) {
			for (int group=0;group<renderer.numberOfGroups;group++) {
				sequenceScreenCache[group] = subImage(sequenceImage[group], rect);
			}
		} 
		sequenceLastRect = rect;
		result = sequenceScreenCache;
		return result;
	}

	private BufferedImage subImage(WritableRenderedImage img, Rectangle visibleRect) {
		if (img==null) return null;
		
		if (img instanceof BufferedImage) {
			int x = visibleRect.x;
			int subImgWidth = Math.min(visibleRect.width, img.getWidth()-visibleRect.x);
			int subImgHeight = Math.min(renderer.getDrawingAreaHeight(), img.getHeight());			
			return ((BufferedImage)img).getSubimage(x,0,subImgWidth, subImgHeight);
		} else if (img instanceof DiskMemImage){
			DiskMemImage dmimg = (DiskMemImage)img;
			if (dmimg.getBounds().intersects(visibleRect)) { // DiskMemImage doesn't check for that and causes an exception if not the case
				return dmimg.getAsBufferedImage(visibleRect, null);
			} else {
				return null;
			}
		} else {
			throw new IllegalArgumentException("Image must be BufferedImage or DiskMemImage!");
		}
	}

	public int[] getSelectionIndices() {
		return selectionIndices;
	}

	public void setSelectionIndices(int[] selectionIndices) {
		this.selectionIndices = selectionIndices;
		this.selectionLastRect = new Rectangle();
		this.selectionCachedArea = new Area();
		this.selectionImage = renderer.createDiskMemImage(renderer.getDrawingAreaWidth(), renderer.getDrawingAreaHeight());
	}
}
