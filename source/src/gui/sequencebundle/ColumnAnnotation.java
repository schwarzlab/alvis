/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.sequencebundle;

/**
 *
 * @author Roland Schwarz <rfs32@cam.ac.uk>
 */
public interface ColumnAnnotation {
	public double[] getColumnValues();
	public int[] getRanks();
}
