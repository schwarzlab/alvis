/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.sequencebundle.tooltip;

import gui.sequencebundle.JSequenceBundle;
import java.awt.BorderLayout;
import java.awt.Color;
import javax.swing.JPanel;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public abstract class SBToolTipPanel extends JPanel {
	protected final JSequenceBundle view;

	public SBToolTipPanel(JSequenceBundle view) {
		this.view = view;
		this.setOpaque(true);
		this.setLayout(new BorderLayout());
		this.setBackground(new Color(0,0,0,0));
		this.setBorder(null);
	}

	public abstract void updateView();

}
