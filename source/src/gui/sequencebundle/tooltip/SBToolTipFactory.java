/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.sequencebundle.tooltip;

import gui.sequencebundle.JSequenceBundle;
import gui.sequencebundle.event.GridPoint;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public class SBToolTipFactory {

	private SBToolTipFactory(){}
	
	public static SBToolTipPanel createToolTipPanel(JSequenceBundle view, GridPoint position){
		if (position.isWithinGrid()) {
			return createGridCellToolTipPanel(view);
		} else if (position.isColHeader()) {
			return createColHeaderToolTipPanel(view);
		} else {
			return null;
		}
	}
	
	public static SBToolTipPanel createGridCellToolTipPanel(JSequenceBundle view) {
		return new GridCellToolTipPanel(view);
	}

	public static SBToolTipPanel createColHeaderToolTipPanel(JSequenceBundle view) {
		return new ColHeaderToolTipPanel(view);
	}
	
	public static SBToolTipPanel createEmptyPanel(){
		return new SBToolTipPanel(null) {	
			@Override
			public void updateView() {}
		};
	}
}
