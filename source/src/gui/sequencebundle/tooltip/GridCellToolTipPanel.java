/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.sequencebundle.tooltip;

import com.general.utils.StringUtils;
import de.biozentrum.bioinformatik.sequence.SequenceAlphabet;
import gui.GUIConstants;
import gui.sequencebundle.JSequenceBundle;
import gui.sequencebundle.event.GridPoint;
import java.awt.BorderLayout;
import java.text.DecimalFormat;
import java.util.Map;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import org.biojava.bio.BioException;
import org.biojava.bio.seq.DNATools;
import org.biojava.bio.seq.ProteinTools;
import org.biojava.bio.symbol.Symbol;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public class GridCellToolTipPanel extends SBToolTipPanel{
	private final JLabel lblText = new JLabel("bla");
	private GridPoint gridPosition=GridPoint.invalidGridPoint();
	
	public GridCellToolTipPanel(JSequenceBundle view) {
		super(view);
		initComponents();
	}

	private void initComponents() {
		lblText.setOpaque(true);
		lblText.setBorder(BorderFactory.createLineBorder(GUIConstants.BORDER_COLOR));
		lblText.setBackground(GUIConstants.BACKGROUND_COLOR);
		this.add(lblText, BorderLayout.CENTER);		
		updateView();
	}

	@Override
	public void updateView() {
		gridPosition = view.getCurrentMouseGridPoint();
		if (gridPosition.isWithinGrid()) {
			this.lblText.setText(getText());
		} else {
			this.lblText.setText("");
		}
	}
	
	public String getText() { 
		if (!gridPosition.isWithinGrid() || gridPosition.y>view.getAlphabet().size()) {
			return "";
		} 
		
		Character currentLetter;
		String threeLetter="";
		if (gridPosition.y == view.getAlphabet().size()) {
			currentLetter = view.getBundleConfig().getGapChar();
			threeLetter = "<GAP>";
		} else {
			currentLetter = view.getAlphabet().characterAt(gridPosition.y);
			try {
				Symbol symbol;
				if (view.getAlignment().getAlphabet().equals(SequenceAlphabet.AMINOACIDS)) {
					symbol = ProteinTools.getAlphabet().getTokenization("token").parseToken(currentLetter.toString());
					threeLetter=symbol.getName();					
				} else if (view.getAlignment().getAlphabet().equals(SequenceAlphabet.NUCLEOTIDES)) {
					symbol = DNATools.getDNA().getAlphabets().get(0).getTokenization("token").parseToken(currentLetter.toString());					
					threeLetter=StringUtils.firstCharToCapital(symbol.getName());
				} 
			} catch (BioException ex) {
				threeLetter = "<unknown>";
			}
		}
		Map<Character, Integer> map = view.getAlignment().getSymbolCounts(gridPosition.x);
		Integer counts = map.get(currentLetter);
		String countText = "";
		if (counts== null || counts==0){
			countText = "No occurences";
		} else {
			countText = counts + " occurence";
			if (counts>1) countText+= "s";	
			double pc = (double)counts / view.getAlignment().getSequenceCount();
			DecimalFormat df = new DecimalFormat("#.##");			
			countText = countText + " (" + df.format(pc*100) + "%)";
		}
		
		String text = "<html><b>" + currentLetter + " - " + threeLetter + "</b><br>" 
				+ "Position " + (gridPosition.x+1) + "<br>"
				+ countText + "</html>";
		
		return text;
	}

}
