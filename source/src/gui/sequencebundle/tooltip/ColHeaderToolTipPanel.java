/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.sequencebundle.tooltip;

import com.general.utils.GUIutils;
import gui.GUIConstants;
import gui.sequencebundle.JSequenceBundle;
import gui.sequencebundle.event.GridPoint;
import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public class ColHeaderToolTipPanel extends SBToolTipPanel{
	List<Map<Character, Integer>> datasets=null;
	JLabel lblImage = new JLabel();
	JLabel lblHeader = new JLabel();
	JLabel lblFooter = new JLabel();
	public ColHeaderToolTipPanel(JSequenceBundle view) {
		super(view);
		this.setBorder(BorderFactory.createLineBorder(GUIConstants.BORDER_COLOR));
		lblImage.setBorder(null);
		this.add(lblImage);
		lblHeader.setOpaque(true);
		lblHeader.setBackground(GUIConstants.BACKGROUND_COLOR);
		lblHeader.setForeground(GUIConstants.TEXT_COLOR);
		this.add(lblHeader, BorderLayout.NORTH);
		lblFooter.setOpaque(true);
		lblFooter.setBackground(GUIConstants.BACKGROUND_COLOR);
		lblFooter.setForeground(GUIConstants.TEXT_COLOR);
		this.add(lblFooter, BorderLayout.SOUTH);		
	}
	
	@Override
	public void updateView() {
		GridPoint pos = view.getCurrentMouseGridPoint();
		if (!pos.isColHeader()) {
			return;
		}
		
		if (datasets==null){
			datasets = new ArrayList<>(view.getAlignment().getLength());			
			fillDatasets();
		}
		
		String text = "Column " + (pos.x+1) + " frequencies:";
		lblHeader.setText(text);
		BufferedImage img =	renderBarChart();
		lblImage.setIcon(new ImageIcon(img));
		if (view.getColumnAnnotation()!=null){
			double val;
			try {
				val = view.getColumnAnnotation().getColumnValues()[pos.x];
			} catch (ArrayIndexOutOfBoundsException ex){
				val = Double.NaN;
			}
			if (!Double.isNaN(val)) {
				lblFooter.setText("Value: " + new DecimalFormat("##.####").format(val));
			} else {
				lblFooter.setText("Value: NaN");
			}
		} else {
			lblFooter.setText(null);
		}
	}

	private void fillDatasets() {
		for (int col=0;col<view.getAlignment().getLength();col++) {
//			Map<Character, Integer> counts = view.getAlignment().getSymbolCounts(col);
//			datasets.add(counts);
			datasets.add(null);
		}	
	}

	private BufferedImage renderBarChart() {
		int xMargin = 3;
		int yMargin = 3;
		int xLegendHeight = 10;
		int yLegendWidth = 20;
		int barWidth=8;
		int cellWidth=barWidth+2;
		int maxBarHeight=100;
		int plotWidth = 2*xMargin + yLegendWidth + (view.getAlphabet().size()+1) * cellWidth;
		int plotHeight = 2*yMargin + maxBarHeight + xLegendHeight;
		FontMetrics metrics = lblHeader.getFontMetrics(lblHeader.getFont());
		int imgWidth= Math.max(plotWidth, metrics.stringWidth(lblHeader.getText()));
		int imgHeight = plotHeight;
		plotWidth=imgWidth;
		BufferedImage img = new BufferedImage(imgWidth, imgHeight, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g = img.createGraphics();
		GUIutils.setQualityRenderingHints(g, view.getBundleConfig().getAntiAliasing(), view.getBundleConfig().getSpeedOverQuality());
		g.setColor(GUIConstants.BACKGROUND_COLOR);
		g.fillRect(0, 0, imgWidth, imgHeight);
		Font legendFont = new Font(Font.SANS_SERIF, Font.BOLD, 10);
		// draw y legend
		g.setColor(GUIConstants.BACKGROUND_COLOR.darker());
		//g.drawLine(0, 0, plotWidth, 0);
		g.setColor(GUIConstants.BORDER_COLOR);
		g.setFont(legendFont);
		for (int i=0;i<=80;i+=20){
			DecimalFormat df = new DecimalFormat("#.#");
			int ypos = plotHeight - yMargin - xLegendHeight - (i*maxBarHeight/100);
			g.drawString(df.format((double)i / 100), xMargin, ypos);
			float dash1[] = {3.0f};
			g.setStroke(new BasicStroke(1.0f,
                        BasicStroke.CAP_BUTT,
                        BasicStroke.JOIN_MITER,
                        10.0f, dash1, 0.0f));
			g.drawLine(xMargin + yLegendWidth, ypos, plotWidth-xMargin, ypos);
		}
		Map<Character, Integer> data = datasets.get(view.getCurrentMouseGridPoint().x);
		if (data==null) {
			data = view.getAlignment().getSymbolCounts(view.getCurrentMouseGridPoint().x);
			datasets.set(view.getCurrentMouseGridPoint().x, data);
		}
		for (int i=0;i<=view.getAlphabet().size();i++){
			Character c;
			if (i==view.getAlphabet().size()) {
				c=view.getBundleConfig().getGapChar();
			} else {
				c = view.getAlphabet().characterAt(i);
			}
			Integer counts = data.get(c);
			if (counts==null) counts=0;
			int barHeight = maxBarHeight * counts / view.getAlignment().getSequenceCount();
			int yposBar = plotHeight - barHeight - yMargin - xLegendHeight;
			int yposLegend = plotHeight - yMargin;
			int xpos = xMargin + yLegendWidth + i * cellWidth;
			g.drawString(c.toString(), xpos+1, yposLegend);
			g.fillRect(xpos, yposBar, barWidth, barHeight);
		}
		g.dispose();
		return img;
	}
}
