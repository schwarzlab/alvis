/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.sequencebundle;

import com.general.containerModel.impl.MapDefaultValueDecorator;
import com.general.gui.dialog.DialogFactory;
import com.general.gui.progress.DefaultProgressable;
import com.general.gui.progress.Progressable;
import com.general.utils.GUIutils;
import com.general.utils.GeneralUtils;
import de.biozentrum.bioinformatik.alignment.MultipleAlignment;
import de.biozentrum.bioinformatik.alignment.selection.AlignmentSelectionModel;
import de.biozentrum.bioinformatik.alignment.selection.DefaultAlignmentSelectionModel;
import de.biozentrum.bioinformatik.alignment.visibility.AlignmentVisibilityChangedEvent;
import de.biozentrum.bioinformatik.alignment.visibility.AlignmentVisibilityListener;
import de.biozentrum.bioinformatik.alignment.visibility.SequenceVisibilityModel;
import de.biozentrum.bioinformatik.color.ColorChangedEvent;
import de.biozentrum.bioinformatik.color.ColorModelListener;
import de.biozentrum.bioinformatik.sequence.Sequence;
import de.biozentrum.bioinformatik.sequence.SequenceAlphabet;
import gui.TransferableSequences;
import gui.sequencebundle.event.DefaultGridSelectionModel;
import gui.sequencebundle.event.GridPoint;
import gui.sequencebundle.event.GridSelectionModel;
import gui.sequencebundle.event.SequenceBundleMouseEvent;
import gui.sequencebundle.event.SequenceBundlesMouseListener;
import gui.sequencebundle.legend.AbstractLegendRenderer;
import gui.sequencebundle.tooltip.SBToolTipFactory;
import gui.sequencebundle.tooltip.SBToolTipPanel;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.datatransfer.Transferable;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionAdapter;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.DefaultListSelectionModel;
import javax.swing.JComponent;
import javax.swing.JToolTip;
import javax.swing.ListSelectionModel;
import javax.swing.ToolTipManager;
import javax.swing.TransferHandler;
import static javax.swing.TransferHandler.COPY;
import org.apache.batik.dom.GenericDOMImplementation;
import org.apache.batik.svggen.SVGGraphics2D;
import org.jaitools.tiledimage.DiskMemImage;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import prefuse.util.ui.JCustomTooltip;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public class JSequenceBundle extends JComponent implements AlignmentVisibilityListener, SequenceBundleRendererListener{
	public static final String PROP_ALIGNMENT="alignment";
	public static final String PROP_ALIGNMENT_SELECTION_MODEL="alignmentSelectionModel";	
	public static final String PROP_COLUMN_ANNOTATION="columnAnnotation";
	public static final String PROP_GRID_SELECTION_MODEL="gridSelectionModel";
	public static final String PROP_LEGEND_RENDERER="legendRenderer";
	public static final String PROP_SEQUENCE_GROUPS="sequenceGroups";
	public static final String PROP_SEQUENCE_SELECTION_MODEL="sequenceSelectionModel";
	public static final String PROP_BUNDLECONFIG = "bundleConfig";
	public static final String PROP_REFERENCESEQUENCE = "PROP_REFERENCESEQUENCE";
	
	SequenceBundleConfig bundleConfig;
	Map<Sequence, Integer> sequenceGroups;
	MultipleAlignment alignment;
	final SequenceBundleRenderer bundleRenderer;
	private AbstractLegendRenderer legendRenderer;
	GridPoint lastMouseGridPoint=GridPoint.invalidGridPoint();
	GridPoint currentMouseGridPoint=GridPoint.invalidGridPoint();
	GridPoint.GridRectangle currentDragRectangle=null;
	GridSelectionModel gridSelectionModel = new DefaultGridSelectionModel();
	ListSelectionModel sequenceSelectionModel = new DefaultListSelectionModel();
	AlignmentSelectionModel alignmentSelectionModel;
	SequenceAlphabet alphabet;
	private SBToolTipPanel ttColHeader = SBToolTipFactory.createColHeaderToolTipPanel(this);
	private final SBToolTipPanel ttGridCell = SBToolTipFactory.createGridCellToolTipPanel(this);
	private SBToolTipPanel tt;
	private BufferedImage legendImage;
	DefaultProgressable progressModel = new DefaultProgressable();
	SequenceBundleToolBar sequenceBundleToolBar;
	ColumnAnnotation columnAnnotation;
	private Color[] columnAnnotationColorGradient;
	Sequence referenceSequence;
	
	// images
	BufferedImage selectionCaret;
	BufferedImage selectionMarker;	
	BufferedImage columnMarker;	
	BufferedImage backgroundImageLegend;
	BufferedImage backgroundImageHorizontalLines;
	BufferedImage backgroundImageVerticalLines;
	DiskMemImage backgroundImageColumnHeader;
	BufferedImage[] alphabetOverlayFragments;
	// end images
	
	// handlers and listeners
	SequenceBundleEventManager eventManager;
	ConfigPropertyChangeHandler configPropertyChangeHandler = new ConfigPropertyChangeHandler();
	ColorPropertyChangeHandler colorPropertyChangeHandler = new ColorPropertyChangeHandler();
	ColorModelChangeHandler colorModelHandlerAlphabet = new ColorModelChangeHandler(SequenceBundleColorModel.PROP_ALPHABETCOLORS);
	ColorModelChangeHandler colorModelHandlerGroups = new ColorModelChangeHandler(SequenceBundleColorModel.PROP_GROUPCOLORS);
	// end handlers and listeners
	private final transient PropertyChangeSupport propertyChangeSupport = new java.beans.PropertyChangeSupport(this);
	private BufferedImage[] cellShadingFragments;
	final SequenceBundleRendererCache rendererCache;
	
	public JSequenceBundle() {
		this(null, null, new SequenceBundleConfig());
	}
	
	public JSequenceBundle(MultipleAlignment alignment, Map<Sequence,Integer> sequenceGroups, SequenceBundleConfig bundleConfig) {
		setBundleConfig(bundleConfig);
		
		// setup groups
		if (sequenceGroups==null) {
			sequenceGroups = new MapDefaultValueDecorator<>(new HashMap<Sequence, Integer>(), 0);
		} 
		this.sequenceGroups = sequenceGroups;
		// and colors
		
		if (alignment!=null) {
			this.alignment = alignment;
			this.alphabet = alignment.getAlphabet();
		} else {
			this.alignment = new MultipleAlignment();
			this.alphabet = new SequenceAlphabet();
		}
		alignmentSelectionModel = new DefaultAlignmentSelectionModel(this.alignment);

		bundleConfig.sequenceVisibilityModel.addVisibilityListener(this);
		
		bundleRenderer =  new SequenceBundleRenderer(this);
		bundleRenderer.addSequenceBundleRendererListener(this);
		rendererCache = new SequenceBundleRendererCache(this);

		legendRenderer = AbstractLegendRenderer.createDefaultLegendRenderer(this.alphabet, bundleConfig);
		this.alphabet = legendRenderer.getAlphabet(); // might have changed
		bundleRenderer.init();
		legendImage = legendRenderer.renderLegend();
		
		ToolTipManager.sharedInstance().registerComponent(this);
		ToolTipManager.sharedInstance().setInitialDelay(100);
		ToolTipManager.sharedInstance().setDismissDelay(5000);
		ToolTipManager.sharedInstance().setReshowDelay(0);
		this.setBackground(Color.WHITE);
		this.addMouseListener(new MouseListener() {
			@Override
			public void mouseClicked(MouseEvent e) {
				GridPoint p = pixelToGrid(e.getPoint());	
				fireSequenceBundlesGridClickedEvent(new SequenceBundleMouseEvent(e, p));
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				if (lastMouseGridPoint.isValidGridPoint()) {
					fireSequenceBundlesGridCellLeftEvent(new SequenceBundleMouseEvent(e, lastMouseGridPoint));
				}
				currentMouseGridPoint=GridPoint.invalidGridPoint();
//				System.out.println("exited, current: " + currentMouseGridPoint + ", last: " + lastMouseGridPoint);
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				lastMouseGridPoint=currentMouseGridPoint;
				currentMouseGridPoint=pixelToGrid(e.getPoint());
//				System.out.println("entered, current: " + currentMouseGridPoint + ", last: " + lastMouseGridPoint);
			}

			@Override
			public void mousePressed(MouseEvent e) {
				GridPoint p = pixelToGrid(e.getPoint());	
				fireSequenceBundlesGridPressedEvent(new SequenceBundleMouseEvent(e, p));
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				GridPoint p = pixelToGrid(e.getPoint());	
				fireSequenceBundlesGridReleasedEvent(new SequenceBundleMouseEvent(e, p));
			}
			
			
			
		});
		
		this.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseMoved(MouseEvent e) {
				GridPoint p = pixelToGrid(e.getPoint());
				if (!p.equals(lastMouseGridPoint)) { // 
					if (lastMouseGridPoint.isValidGridPoint()) {
						fireSequenceBundlesGridCellLeftEvent(new SequenceBundleMouseEvent(e, lastMouseGridPoint));
					}
					if (p.isValidGridPoint()) { // grid position valid and has changed
						fireSequenceBundlesGridCellEnteredEvent(new SequenceBundleMouseEvent(e, p));
					}
				}
				lastMouseGridPoint=p;
			}

			@Override
			public void mouseDragged(MouseEvent e) {
				GridPoint p = pixelToGrid(e.getPoint());	
				fireSequenceBundlesGridDraggedEvent(new SequenceBundleMouseEvent(e, p));
				if (currentMouseGridPoint.isWithinGrid() && !e.isControlDown() && !e.isShiftDown()) {
					currentDragRectangle=null;
					JComponent c = (JComponent)e.getSource();
					TransferHandler h = c.getTransferHandler();
					h.exportAsDrag(c, e, TransferHandler.COPY);
				}
			}
		});

		eventManager = new SequenceBundleEventManager(this);
		this.sequenceSelectionModel.addListSelectionListener(eventManager.sequenceSelectionListener);
		this.gridSelectionModel.addGridSelectionListener(eventManager.gridSelectionListener);
		this.addSequenceBundlesMouseListener(eventManager.mouseEventListener); // fires selection events
		this.setTransferHandler(new SequenceTransferHandler());
		
	}	

	@Override
	public Dimension getPreferredSize() {
		if (bundleRenderer.isInitialised()) {
			int width = bundleRenderer.getDrawingAreaWidth()+bundleConfig.bundleIndent;
			int height = bundleRenderer.getDrawingAreaHeight();
			width *= bundleConfig.dpi / SequenceBundleConfig.DEFAULT_DPI;
			height *= bundleConfig.dpi / SequenceBundleConfig.DEFAULT_DPI;
			return new Dimension(width, height);
		} else {
			return super.getPreferredSize();
		}
	}

	
	@Override
	protected void paintComponent(Graphics g) {
//		long time = System.currentTimeMillis();
		List<GridPoint> visGridPoints = getVisibleGridPoints();
		GridPoint.GridRectangle visGridRect = getVisibleGridColumns();
		Rectangle visScreenRect = getVisibleRect();
		
		Graphics2D gc = (Graphics2D)g;
		GUIutils.setQualityRenderingHints(gc);
		gc.setClip(visScreenRect);
		gc.setColor(bundleConfig.colorModel.getBackgroundColor());
		gc.fillRect(0, 0, getWidth(), getHeight());

		gc.drawImage(legendImage, getVisibleRect().x,0,null);				

		// from here on extend clipping area to precent drawing onto legend
		Rectangle r = getVisibleRect();
		r.x+=bundleConfig.bundleIndent;
		r.width-=bundleConfig.bundleIndent;
		gc.setClip(r);
		
		if (bundleConfig.isShowingHorizontalLines()) {
			for (int col=visGridRect.p1.x;col<=visGridRect.p2.x;col++) {
				Point p = gridToPixel(new GridPoint(col, 0));
				gc.drawImage(backgroundImageHorizontalLines, p.x, p.y, null);
			}
		}
		
		if (bundleConfig.isShowingVerticalLines()) {
			for (int col=visGridRect.p1.x;col<=visGridRect.p2.x;col++) {
				Point p = gridToPixel(new GridPoint(col, GridPoint.HEADER));
				gc.drawImage(backgroundImageVerticalLines, p.x, p.y + bundleConfig.cellHeight, null);
			}
		}

		// draw sequences
		AffineTransform at = new AffineTransform();
		at.setToTranslation(bundleConfig.bundleIndent + visScreenRect.x, 0);
		Rectangle adjustedRect = new Rectangle(visScreenRect);
		adjustedRect.width -= bundleConfig.bundleIndent;
		
		BufferedImage[] seqImgs = rendererCache.getSequenceImage(adjustedRect);
		for (int group:bundleRenderer.groupIDs) {
			if (bundleConfig.groupVisibility.get(group)){
				BufferedImage si = seqImgs[bundleRenderer.groupIDs.entryIndex(group)];
				gc.drawImage(si, at, null);
				
			}
		}

		if (bundleConfig.showingSelection) {
			BufferedImage si = rendererCache.getSelectedSequenceImage(adjustedRect);
			gc.drawImage(si, at, null);
		}
		
		
		// draw consensus
		if (bundleConfig.showingConsensus) {
			BufferedImage consensus = bundleRenderer.renderConsensusSequence(Math.max(0, visGridRect.p1.x-1), visGridRect.p2.x);
			int consensusOffset;
			if (getVisibleRect().x >= (1.5*bundleConfig.cellWidth)) {
				consensusOffset = (getVisibleRect().x ) % (bundleConfig.cellWidth);
				consensusOffset = consensusOffset  + (int)(bundleConfig.cellWidth);
			} else {
				consensusOffset = (getVisibleRect().x) % (int)(1.5 * bundleConfig.cellWidth);
			}
			consensus = consensus.getSubimage(consensusOffset, 0, consensus.getWidth()-consensusOffset, consensus.getHeight());
			gc.drawImage(consensus, bundleConfig.bundleIndent + getVisibleRect().x, 0, null);
		}
		
		// draw selection markers
		for (GridPoint gp:gridSelectionModel.getSelectedGridPoints()) {
			Point p = gridToPixel(gp);
			gc.drawImage(selectionMarker, p.x, p.y, null);
		}

		// draw column values
		if (columnAnnotation!=null && columnAnnotation.getColumnValues()!=null){
			GridPoint colLeft = new GridPoint(visGridRect.p1.x, GridPoint.HEADER);
			GridPoint colRight = new GridPoint(visGridRect.p2.x, GridPoint.HEADER);
			GridPoint.GridRectangle visHeader = GridPoint.enclosingRectangle(colLeft, colRight);

			for (GridPoint gp:visHeader){
				Point pixel = gridToPixel(gp);
				gc.setStroke(new BasicStroke(1));
				if (gp.x<this.columnAnnotationColorGradient.length) {
					gc.setColor(this.columnAnnotationColorGradient[gp.x]);
					gc.fillRect(pixel.x, pixel.y, bundleConfig.cellWidth, bundleConfig.cellHeight);
				}
				
			}
		}
		
		// draw column header
//		gc.drawImage(rendererCache.getColumnHeaderImage(visScreenRect), bundleConfig.bundleIndent+getVisibleRect().x, 0, null);
		gc.translate(bundleConfig.bundleIndent, 0);
		bundleRenderer.renderColumnHeaders(gc, visGridRect.p1.x, visGridRect.p2.x);
		gc.translate(-bundleConfig.bundleIndent, 0);
		
		// draw selection caret
		GridPoint.GridRectangle markers;
		if (currentDragRectangle==null){			
			markers = GridPoint.enclosingRectangle(currentMouseGridPoint, currentMouseGridPoint);
		} else {
			markers = GridPoint.enclosingRectangle(currentDragRectangle);
		}
		if (markers.p1.isValidGridPoint()){
			Point p1 = gridToPixel(markers.p1);
			gc.drawImage(selectionCaret, p1.x, p1.y,null);
		}
		
		// draw column markers
		int[] selectedCols = GUIutils.getSelectedIndices(gridSelectionModel.getColSelectionModel());
		for (int i:selectedCols){
			Point pixel = gridToPixel(new GridPoint(i,GridPoint.HEADER));
			pixel.translate(bundleConfig.cellWidth / 2 - columnMarker.getWidth() / 2, -bundleConfig.cellHeight - 2);
//			pixel.y = pixel.y - bundleConfig.cellHeight;
			gc.drawImage(columnMarker, pixel.x, pixel.y, null);
		}
		
		// draw alphabet overlay
		if (bundleConfig.showingOverlay) {
			for (GridPoint p:visGridPoints) {
				if (p.isWithinGrid()) {
					Point pixel = gridToPixel(p);
					gc.drawImage(alphabetOverlayFragments[p.y], pixel.x, pixel.y, null);
				}
			}
		}

		// draw cell shading
		if (!bundleConfig.showingOverlay) {
			for (GridPoint p:visGridPoints) {
				if (p.isWithinGrid()) {
					Point pixel = gridToPixel(p);
					int idx = p.y % 2;
					gc.drawImage(cellShadingFragments[idx], pixel.x, pixel.y, null);
				}
			}
		}
		
		gc.dispose();
//		time = System.currentTimeMillis()-time;
//		System.out.println("Paint component: " + new DecimalFormat("##.####").format(time/1000.0) + " ms");
	}
	
	public void redraw() {
		bundleRenderer.init();
		rendererCache.initCache();
		this.backgroundImageHorizontalLines=bundleRenderer.renderBackgroundHorizontalLineFragment();
		this.backgroundImageVerticalLines=bundleRenderer.renderBackgroundVerticalLineFragment();
		this.cellShadingFragments = bundleRenderer.renderCellShadingFragments();
		this.columnMarker=bundleRenderer.renderColumnMarker();
		this.selectionCaret=bundleRenderer.renderSelectionCaret();
		this.selectionMarker=bundleRenderer.renderSelectionMarker();
		this.alphabetOverlayFragments = bundleRenderer.renderAlphabetOverlayFragments();
		this.legendImage = legendRenderer.renderLegend();
		repaint();
	}
	
	public void scrollColumnToCenter(int column) {
		Rectangle visRect = getVisibleRect();
		int bundleWidth = visRect.width-bundleConfig.bundleIndent;
		int offsetX = bundleConfig.bundleIndent + bundleWidth/2;
		Point pixel = gridToPixel(new GridPoint(column+1, GridPoint.HEADER));
		Rectangle target1 = new Rectangle(pixel.x-offsetX, 0, visRect.width, 1);
		scrollRectToVisible(target1);
	}
	
	public void renderSVGToFile(final File file, int dpi) {
		// Get a DOMImplementation.
		DOMImplementation domImpl = GenericDOMImplementation.getDOMImplementation();

		// Create an instance of org.w3c.dom.Document.
		String svgNS = "http://www.w3.org/2000/svg";
		Document document = domImpl.createDocument(svgNS, "svg", null);

		// Create an instance of the SVG Generator.
		final SVGGraphics2D svgGenerator = new SVGGraphics2D(document);

		SequenceBundleConfig config = new SequenceBundleConfig(bundleConfig); // copy the config
		config.setDpi(dpi);
		final SequenceBundleRenderer imageRenderer = new SequenceBundleRenderer(this, config);
		final AbstractLegendRenderer legendRenderer;
		try {
			legendRenderer = this.legendRenderer.getClass().newInstance();
		} catch (InstantiationException ex) {
			Logger.getLogger(JSequenceBundle.class.getName()).log(Level.SEVERE, null, ex);
			return;
		} catch (IllegalAccessException ex) {
			Logger.getLogger(JSequenceBundle.class.getName()).log(Level.SEVERE, null, ex);
			return;
		}
		
		legendRenderer.setBundleConfig(config);	
		legendRenderer.setAlphabet(alphabet);
		final int legendWidth = (int)legendRenderer.getLegendSize().getWidth();
		config.setBundleIndent(legendWidth);
		
		final int width = imageRenderer.getDrawingAreaWidth() + legendWidth;
		final int height = imageRenderer.getDrawingAreaHeight();

		// draw everything apart from the bundles
		final Graphics2D gc = svgGenerator;
		imageRenderer.setGlobalGraphicsParameters(gc);
		gc.setColor(getBackground());
		gc.fillRect(0, 0, width, height);

		legendRenderer.renderLegend(gc);
		gc.translate(config.bundleIndent, 0);
		
		if (bundleConfig.isShowingHorizontalLines()) {
			imageRenderer.renderBackgroundHorizontalLines(gc);
		}
		
		imageRenderer.renderColumnHeaders(gc);
		if (bundleConfig.isShowingVerticalLines()) {
			imageRenderer.renderBackgroundVerticalLines(gc);
		}
		
		if (bundleConfig.isShowingOverlay()) {
			imageRenderer.renderAlphabetOverlay(gc);
		} else {
			imageRenderer.renderCellShading(gc);
		}
		
		imageRenderer.addSequenceBundleRendererListener(new SequenceBundleRendererListener() {
			// react to renderer events
			@Override
			public void renderingStarted(SequenceBundleRendererEvent e) {
				progressModel.setState(Progressable.STATE_RUNNING);
				progressModel.setMinimum(e.getStartValue());
				progressModel.setMaximum(e.getEndValue());
				progressModel.setValue(e.getCurrentValue());
				progressModel.setIndeterminate(false);
				progressModel.setStatusText("Exporting...");
			}

			@Override
			public void renderingFinished(SequenceBundleRendererEvent e) {

				gc.dispose();
				// Finally, stream out SVG to the standard output using
				// UTF-8 encoding.
				boolean useCSS = true; // we want to use CSS style attributes
				Writer out;
				try {
					out = new OutputStreamWriter(new FileOutputStream(file), "UTF-8");
					svgGenerator.stream(out, useCSS);	
					out.close();
				} catch ( IOException ex) {
					DialogFactory.showErrorMessage(ex.getMessage(), "Error saving SVG:");
				} 
				
				progressModel.setState(Progressable.STATE_IDLE);		
			}

			@Override
			public void renderingCancelled(SequenceBundleRendererEvent e) {
				progressModel.setState(Progressable.STATE_IDLE);
			}

			@Override
			public void renderingProgressed(SequenceBundleRendererEvent e) {
				progressModel.setMinimum(e.getStartValue());
				progressModel.setMaximum(e.getEndValue());
				progressModel.setValue(e.getCurrentValue());
			}
		});
		
		imageRenderer.renderSequences(gc, SequenceBundleRenderer.RenderingStrategy.DIRECT_RENDERER);
		
	}
	
	public void renderPNGToFile(final File file, int dpi) {
		SequenceBundleConfig config = new SequenceBundleConfig(bundleConfig); // copy the config
		config.setDpi(dpi);
		
		final SequenceBundleRenderer imageRenderer = new SequenceBundleRenderer(this, config);
		final AbstractLegendRenderer legendRenderer;
		try {
			legendRenderer = this.legendRenderer.getClass().newInstance();
		} catch (InstantiationException ex) {
			Logger.getLogger(JSequenceBundle.class.getName()).log(Level.SEVERE, null, ex);
			return;
		} catch (IllegalAccessException ex) {
			Logger.getLogger(JSequenceBundle.class.getName()).log(Level.SEVERE, null, ex);
			return;
		}
		
		legendRenderer.setBundleConfig(config);	
		legendRenderer.setAlphabet(alphabet);
		final int legendWidth = (int)legendRenderer.getLegendSize().getWidth();
		config.setBundleIndent(legendWidth);
		
		final int width = imageRenderer.getDrawingAreaWidth() + legendWidth;
		final int height = imageRenderer.getDrawingAreaHeight();
		final DiskMemImage theImage = imageRenderer.createDiskMemImage(width, height);

		// draw everything apart from the bundles
		final Graphics2D gc = theImage.createGraphics();
		imageRenderer.setGlobalGraphicsParameters(gc);
		gc.setColor(getBackground());
		gc.fillRect(0, 0, width, height);
		legendRenderer.renderLegend(gc);
		gc.translate(config.bundleIndent, 0);
		
		if (bundleConfig.isShowingHorizontalLines()) {
			imageRenderer.renderBackgroundHorizontalLines(gc);
		}
		
		imageRenderer.renderColumnHeaders(gc);
		if (bundleConfig.isShowingVerticalLines()) {
			imageRenderer.renderBackgroundVerticalLines(gc);
		}
		
		if (bundleConfig.isShowingOverlay()) {
			imageRenderer.renderAlphabetOverlay(gc);
		} else {
			imageRenderer.renderCellShading(gc);
		}
		
		imageRenderer.addSequenceBundleRendererListener(new SequenceBundleRendererListener() {
			// react to renderer events
			@Override
			public void renderingStarted(SequenceBundleRendererEvent e) {
				progressModel.setState(Progressable.STATE_RUNNING);
				progressModel.setMinimum(e.getStartValue());
				progressModel.setMaximum(e.getEndValue());
				progressModel.setValue(e.getCurrentValue());
				progressModel.setIndeterminate(false);
				progressModel.setStatusText("Exporting...");
			}

			@Override
			public void renderingFinished(SequenceBundleRendererEvent e) {
				gc.dispose();
				try {
					ImageIO.write(theImage, "png", file);
				} catch (IOException ex) {
					DialogFactory.showErrorMessage(ex.getMessage(), "Error saving PNG:");
				}
				
				progressModel.setState(Progressable.STATE_IDLE);		
			}

			@Override
			public void renderingCancelled(SequenceBundleRendererEvent e) {
				progressModel.setState(Progressable.STATE_IDLE);
			}

			@Override
			public void renderingProgressed(SequenceBundleRendererEvent e) {
				progressModel.setMinimum(e.getStartValue());
				progressModel.setMaximum(e.getEndValue());
				progressModel.setValue(e.getCurrentValue());
			}
		});
		
		imageRenderer.renderSequences(gc, SequenceBundleRenderer.RenderingStrategy.TILE_RENDERER);
		
	}
	
	private void fillToRight(Graphics2D g, BufferedImage img, int targetWidth, int offset, int threshold) {
		for (int i=0;i<Math.ceil((float)targetWidth/img.getWidth());i++) {
			int xpos = offset + i * img.getWidth();
			if (xpos>=threshold) {
				g.drawImage(img, xpos,0, null);
			}
		}
	}
	
	/**
	 * Returns the pixel coordinates of the current grid point. Pixel coordinates
	 * include the legend image, i.e. they are component coordinates.
	 * @param p
	 * @return 
	 */
	Point gridToPixel(GridPoint p) {
		Point retval = bundleRenderer.gridToPixel(p);
		retval.x+=bundleConfig.bundleIndent;
		
		return retval;
	}
	
	/**
	 * Returns the grid point for the given pixel coordinate, i.e. the row and column
	 * of the grid cell that is behind the given pixel, if any. Pixel coordinates are
	 * assumed to be in component coordinates, i.e. including the legend image.
	 * @param p
	 * @return 
	 */
	GridPoint pixelToGrid(Point p) {
		int xTranslated = p.x - bundleConfig.bundleIndent;
		int column;
		int row;
		
//		if (xTranslated<(bundleConfig.cellWidth/2) || (p.x-getVisibleRect().x < bundleIndent)) {
		if ((xTranslated - getVisibleRect().x < 0)) {			
			column=GridPoint.HEADER;
		} else if (xTranslated>=bundleRenderer.columnToXRight(alignment.getLength()-1)) {
			column=GridPoint.OUT_OF_RANGE;
		} else {
//			int tmp = xTranslated - ((bundleConfig.cellWidth-2) / 2);
			column = (int) Math.floor(xTranslated / bundleConfig.cellWidth);
		}

		int idx = column % bundleConfig.columnOffset.length;
		int yTranslated = p.y - bundleConfig.offsetY;
		if (idx>=0) {
			yTranslated -=  bundleConfig.columnOffset[idx] * bundleConfig.cellHeight;
		}
		
		if (yTranslated<0) {
			row = GridPoint.HEADER;
		} else if (yTranslated>=(alphabet.size()+1)*bundleConfig.cellHeight) {
			row = GridPoint.OUT_OF_RANGE;
		} else {
			row = (int)Math.floor((yTranslated)/bundleConfig.cellHeight);
		}
//		System.out.println("row:" + row  + ",  column:" + column);
		return new GridPoint(column, row);
	}
	
	/**
	 * returns the Grid coordinates of the parts of the sequence bundle that 
	 * are currently visible. Will always return a rect (0,0)->(0,0) even if 
	 * the bundle isn't visible at all.
	 * @return 
	 */
	GridPoint.GridRectangle getVisibleGridColumns() {
		Rectangle visRect = getVisibleRect();
		Point origin = visRect.getLocation();
		origin.x += bundleConfig.bundleIndent;
		
		GridPoint topLeft = pixelToGrid(origin);
		GridPoint bottomRight = pixelToGrid(new GridPoint(
				(int)(origin.getX() + visRect.getWidth() - bundleConfig.bundleIndent), 
				(int)(origin.getY() + visRect.getHeight())
		));
		if (topLeft.x==GridPoint.HEADER) topLeft.x=0;
		if (topLeft.y==GridPoint.HEADER) topLeft.y=0;
		if (bottomRight.x==GridPoint.OUT_OF_RANGE) bottomRight.x=getAlignment().getLength()-1;
		if (bottomRight.y==GridPoint.OUT_OF_RANGE) bottomRight.y=getAlphabet().size() + bundleRenderer.maxColumnOffset;
		GridPoint.GridRectangle gr = new GridPoint.GridRectangle(topLeft, bottomRight);				
//		System.out.println(gr);				
		return gr ;
	}
	
	List<GridPoint> getVisibleGridPoints() {
		List<GridPoint> list = new ArrayList<>();
		Rectangle visRect = getVisibleRect();
		Point origin = visRect.getLocation();
		origin.x += bundleConfig.bundleIndent;
		visRect.setLocation(origin);
		visRect.setSize(visRect.width - bundleConfig.bundleIndent, visRect.height);
		
		int columnFrom = pixelToGrid(origin).x;
		int columnTo = pixelToGrid(new GridPoint(origin.x + visRect.width, 0)).x;
		if (columnTo == GridPoint.OUT_OF_RANGE) {
			columnTo = alignment.getLength()-1;
		}
		
		for (int col=columnFrom;col<=columnTo;col++) {
			for (int alpha=0;alpha<=alphabet.size();alpha++) {
				GridPoint testPoint = new GridPoint(col+1, alpha+1);
				GridPoint gridPoint = new GridPoint(col, alpha);
				Point testPointScreen = gridToPixel(testPoint);
				Point gridPointScreen = gridToPixel(gridPoint);
				if (visRect.contains(testPointScreen) || visRect.contains(gridPointScreen)) { // make sure half visible grid points are included on both left and right screen borders
					list.add(gridPoint);
				}
			}
		}
		return list;
	}
	
	
	int[] gridPointToSequenceIndices(GridPoint p){
		if (!p.isWithinGrid()) return new int[0];
		
		boolean[] matches = new boolean[alignment.getSequenceCount()];
		
		char[] col = alignment.getColumn(p.x);		
		char testChar;
		if (p.y==alphabet.size()) { // gap char
			testChar=bundleConfig.gapChar;
		} else {
			testChar=alphabet.characterAt(p.y);
		}

		for (int i=0;i<col.length;i++) {
			// translate row index into actual char, this needs to be redone at some point
			if (col[i]==testChar) {
				matches[i]=true;
			}
		}
		return GeneralUtils.whichTrue(matches);
	}
	
	// event support
	public void addSequenceBundlesMouseListener(SequenceBundlesMouseListener listener) {
	  listenerList.add(SequenceBundlesMouseListener.class, listener);
	}
	public void removeSequenceBundlesMouseListener(SequenceBundlesMouseListener listener) {
	  listenerList.remove(SequenceBundlesMouseListener.class, listener);
	}
  
	void fireSequenceBundlesGridClickedEvent(SequenceBundleMouseEvent evt) {
		Object[] listeners = listenerList.getListenerList();
		for (int i = 0; i < listeners.length; i = i+2) {
			if (listeners[i] == SequenceBundlesMouseListener.class) {
				((SequenceBundlesMouseListener) listeners[i+1]).sequenceBundleGridClicked(evt);
			}
		}
	}	

	void fireSequenceBundlesGridCellEnteredEvent(SequenceBundleMouseEvent evt) {
		Object[] listeners = listenerList.getListenerList();
		for (int i = 0; i < listeners.length; i = i+2) {
			if (listeners[i] == SequenceBundlesMouseListener.class) {
				((SequenceBundlesMouseListener) listeners[i+1]).sequenceBundleGridCellEntered(evt);
			}
		}
	}	

	void fireSequenceBundlesGridCellLeftEvent(SequenceBundleMouseEvent evt) {
		Object[] listeners = listenerList.getListenerList();
		for (int i = 0; i < listeners.length; i = i+2) {
			if (listeners[i] == SequenceBundlesMouseListener.class) {
				((SequenceBundlesMouseListener) listeners[i+1]).sequenceBundleGridCellLeft(evt);
			}
		}
	}	

	void fireSequenceBundlesGridPressedEvent(SequenceBundleMouseEvent evt) {
		Object[] listeners = listenerList.getListenerList();
		for (int i = 0; i < listeners.length; i = i+2) {
			if (listeners[i] == SequenceBundlesMouseListener.class) {
				((SequenceBundlesMouseListener) listeners[i+1]).sequenceBundleGridPressed(evt);
			}
		}
	}	
	void fireSequenceBundlesGridReleasedEvent(SequenceBundleMouseEvent evt) {
		Object[] listeners = listenerList.getListenerList();
		for (int i = 0; i < listeners.length; i = i+2) {
			if (listeners[i] == SequenceBundlesMouseListener.class) {
				((SequenceBundlesMouseListener) listeners[i+1]).sequenceBundleGridReleased(evt);
			}
		}
	}	
	void fireSequenceBundlesGridDraggedEvent(SequenceBundleMouseEvent evt) {
		Object[] listeners = listenerList.getListenerList();
		for (int i = 0; i < listeners.length; i = i+2) {
			if (listeners[i] == SequenceBundlesMouseListener.class) {
				((SequenceBundlesMouseListener) listeners[i+1]).sequenceBundleGridDragged(evt);
			}
		}
	}	
	
	public void clearSelection() {
		alignmentSelectionModel.begin();
		alignmentSelectionModel.deselectAll();
		alignmentSelectionModel.end();
		gridSelectionModel.clearSelection();
		sequenceSelectionModel.clearSelection();
	}


	/**
	 * @return the sequenceSelectionModel
	 */
	public ListSelectionModel getSequenceSelectionModel() {
		return sequenceSelectionModel;
	}

	/**
	 * @param lsm
	 */
	public void setSequenceSelectionModel(ListSelectionModel lsm) {
		ListSelectionModel oldval = this.sequenceSelectionModel;
		oldval.removeListSelectionListener(eventManager.sequenceSelectionListener);
		this.sequenceSelectionModel = lsm;
		sequenceSelectionModel.addListSelectionListener(eventManager.sequenceSelectionListener);
		firePropertyChange(PROP_SEQUENCE_SELECTION_MODEL, oldval, this.sequenceSelectionModel);
	}

	/**
	 * @return the gridSelectionModel
	 */
	public GridSelectionModel getGridSelectionModel() {
		return gridSelectionModel;
	}

	/**
	 * @param gridSelectionModel the gridSelectionModel to set
	 */
	public void setGridSelectionModel(GridSelectionModel gridSelectionModel) {
		GridSelectionModel oldval = this.gridSelectionModel;
		this.gridSelectionModel = gridSelectionModel;
		gridSelectionModel.addGridSelectionListener(eventManager.gridSelectionListener);
		firePropertyChange(PROP_GRID_SELECTION_MODEL, oldval, this.gridSelectionModel);
	}

	public void setAlignmentSelectionModel(AlignmentSelectionModel asm) {
		AlignmentSelectionModel oldval = this.alignmentSelectionModel;
		oldval.removeAlignmentSelectionListener(eventManager.alignmentSelectionListener);
		this.alignmentSelectionModel = asm;
		this.alignmentSelectionModel.addAlignmentSelectionListener(eventManager.alignmentSelectionListener);
		firePropertyChange(PROP_ALIGNMENT_SELECTION_MODEL, oldval, this.alignmentSelectionModel);
	}
	
	/**
	 * @return the alignment
	 */
	public MultipleAlignment getAlignment() {
		return alignment;
	}

	/**
	 * @param alignment the alignment to set
	 */
	public void setAlignment(MultipleAlignment alignment) {
		this.clearSelection();
		MultipleAlignment oldval = this.alignment;
		this.alignment = alignment;
		if (alignment!=null) {
			legendRenderer = AbstractLegendRenderer.createDefaultLegendRenderer(this.alignment.getAlphabet(), bundleConfig);
		} else {
			legendRenderer = AbstractLegendRenderer.createDefaultLegendRenderer(new SequenceAlphabet(), bundleConfig);
		}
		legendImage = legendRenderer.renderLegend();		
		// renderer can modify the alphabet, therefore
		this.alphabet = legendRenderer.getAlphabet();
		ttColHeader = SBToolTipFactory.createColHeaderToolTipPanel(this);
		tt=null;
		if (this.sequenceBundleToolBar!=null) {
			this.sequenceBundleToolBar.update();
		}
		if (bundleConfig.automaticRedraw) {
			redraw();
		}
		firePropertyChange(PROP_ALIGNMENT, oldval, this.alignment);
	}

	/**
	 * @return the sequenceGroups
	 */
	public Map<Sequence, Integer> getSequenceGroups() {
		return sequenceGroups;
	}

	/**
	 * @param sequenceGroups the sequenceGroups to set
	 */
	public void setSequenceGroups(Map<Sequence, Integer> sequenceGroups) {
		Map<Sequence, Integer> oldval = this.sequenceGroups;
		this.sequenceGroups=sequenceGroups;
		if (bundleConfig.automaticRedraw) {
			redraw();
		}
		if (this.sequenceBundleToolBar!=null) {		
			this.sequenceBundleToolBar.update();
		}
		firePropertyChange(PROP_SEQUENCE_GROUPS, oldval, this.sequenceGroups);
	}

	@Override
	public JToolTip createToolTip() {
		if (lastMouseGridPoint!=currentMouseGridPoint || tt==null) { // cell changed
			if (currentMouseGridPoint.isColHeader()) {
				tt = ttColHeader;
			} else if (currentMouseGridPoint.isWithinGrid()){
				tt = ttGridCell;
			} else{
				tt = ttGridCell; // fill in sth sensible
			} 
		}
		tt.updateView();
		JCustomTooltip ctt = new JCustomTooltip(this, tt, false);
		ctt.setOpaque(false);
		return ctt;
	}

	@Override
	public Point getToolTipLocation(MouseEvent event) {
		Point p = event.getPoint();
		p.translate(10, 10);
		return p;
	}

	@Override
	public String getToolTipText(MouseEvent event) {
		if (!currentMouseGridPoint.isValidGridPoint() || currentMouseGridPoint.isRowHeader()) {
			return null;
		}
		return ""; // important otherwise createToolTip doesn't get called
	}	

	/**
	 * @return the currentMouseGridPoint
	 */
	public GridPoint getCurrentMouseGridPoint() {
		return currentMouseGridPoint;
	}

	/**
	 * @return the alphabet
	 */
	public SequenceAlphabet getAlphabet() {
		return alphabet;
	}

	/**
	 * @return the progressModel
	 */
	public Progressable getProgressModel() {
		return progressModel;
	}

	@Override
	public void visibilityChanged(AlignmentVisibilityChangedEvent e) {
		if (bundleConfig.automaticRedraw && !e.getValueIsAdjusting()) {
			redraw();
		}
	}
	
	private void handleColorEvent(String colorPropertyName) {
		switch (colorPropertyName) {
				case SequenceBundleColorModel.PROP_BACKGROUNDCOLOR:
					JSequenceBundle.this.setBackground(bundleConfig.colorModel.getBackgroundColor());
					repaint();
					break;
				case SequenceBundleColorModel.PROP_GROUPCOLORS:
					if (sequenceBundleToolBar!=null) {
						sequenceBundleToolBar.update();
					}
					redraw();
					break;
				case SequenceBundleColorModel.PROP_ALPHABETCOLORS:
					alphabetOverlayFragments = bundleRenderer.renderAlphabetOverlayFragments();
					repaint();
					break;
				default:
					redraw();
			}
	}
	
	/**
	 * @return the sequenceBundleToolBar
	 */
	public SequenceBundleToolBar createSequenceBundleToolBar() {
		this.sequenceBundleToolBar=new SequenceBundleToolBar(this);
		return sequenceBundleToolBar;
	}

	/**
	 * @return the columnAnnotation
	 */
	public ColumnAnnotation getColumnAnnotation() {
		return columnAnnotation;
	}

	/**
	 * @param columnAnnotation the columnAnnotation to set
	 */
	public void setColumnAnnotation(ColumnAnnotation columnAnnotation) {
		ColumnAnnotation oldval = this.columnAnnotation;
		this.columnAnnotation = columnAnnotation;
		firePropertyChange(PROP_COLUMN_ANNOTATION, oldval, this.columnAnnotation);
		createColumnAnnotationColorGradient();
		repaint();
	}

	/**
	 * @return the legendRenderer
	 */
	public AbstractLegendRenderer getLegendRenderer() {
		return legendRenderer;
	}

	/**
	 * @param legendRenderer the legendRenderer to set
	 */
	public void setLegendRenderer(AbstractLegendRenderer legendRenderer) {
		AbstractLegendRenderer oldval = this.legendRenderer;
		this.legendRenderer = legendRenderer;
		if (this.legendRenderer!=null) {
			this.alphabet = this.legendRenderer.getAlphabet();
			legendImage = legendRenderer.renderLegend();					
		}
		setReferenceSequence(getReferenceSequence()); // refresh column offsets
		firePropertyChange(PROP_LEGEND_RENDERER, oldval, this.legendRenderer);
		if (bundleConfig.automaticRedraw) {
			redraw();
		}
	}

	private void createColumnAnnotationColorGradient() {
		if (columnAnnotation==null || columnAnnotation.getColumnValues()==null || columnAnnotation.getRanks()==null){
			columnAnnotationColorGradient=null;
			return;
		}
		
		double[] values = columnAnnotation.getColumnValues();
		int[] ranks = columnAnnotation.getRanks();
		int firstNonNan=0;
		int lastNonNan=ranks.length-1;
		for (int i=0;i<ranks.length;i++){
			if (Double.isNaN(values[ranks[i]])){
				firstNonNan++;
			} else {
				break;
			}
		}
		for (int i=ranks.length-1;i>=0;i--){
			if (Double.isNaN(values[ranks[i]])){
				lastNonNan--;
			} else {
				break;
			}
		}
		columnAnnotationColorGradient = GUIutils.createColorGradient(Color.WHITE, Color.GREEN, values, values[ranks[firstNonNan]], values[ranks[lastNonNan]], 10, Color.WHITE);
	}

	public SequenceBundleConfig getBundleConfig() {
		return bundleConfig;
	}

	public final void setBundleConfig(SequenceBundleConfig bundleConfig) {
		SequenceBundleConfig oldconfig = this.bundleConfig;
		this.bundleConfig = bundleConfig;
		
		if (oldconfig!=null) {
			SequenceBundleColorModel oldColorModel = oldconfig.getColorModel();
			oldColorModel.getAlphabetColors().removeColorModelListener(colorModelHandlerAlphabet);
			oldColorModel.getGroupColors().removeColorModelListener(colorModelHandlerGroups);
			oldColorModel.removePropertyChangeListener(colorPropertyChangeHandler);
			oldconfig.removePropertyChangeListener(configPropertyChangeHandler);					
		}
		
		if (this.bundleConfig != null) {
			SequenceBundleColorModel newColorModel = this.bundleConfig.getColorModel();
			newColorModel.getAlphabetColors().addColorModelListener(colorModelHandlerAlphabet);
			newColorModel.getGroupColors().addColorModelListener(colorModelHandlerGroups);
			newColorModel.addPropertyChangeListener(colorPropertyChangeHandler);
			this.bundleConfig.addPropertyChangeListener(configPropertyChangeHandler);			
		}
		
		firePropertyChange(PROP_BUNDLECONFIG, oldconfig, bundleConfig);
	}

	// react to renderer events
	@Override
	public void renderingStarted(SequenceBundleRendererEvent e) {
//		progressModel.setState(Progressable.STATE_RUNNING);
//		progressModel.setMinimum(e.getStartValue());
//		progressModel.setMaximum(e.getEndValue());
//		progressModel.setValue(e.getCurrentValue());
//		progressModel.setIndeterminate(false);
//		progressModel.setStatusText("Rendering...");
	}

	@Override
	public void renderingFinished(SequenceBundleRendererEvent e) {
//		progressModel.setState(Progressable.STATE_IDLE);
//		repaint();
	}

	@Override
	public void renderingCancelled(SequenceBundleRendererEvent e) {
//		progressModel.setState(Progressable.STATE_IDLE);
//		repaint();
	}
	
	@Override
	public void renderingProgressed(SequenceBundleRendererEvent e) {
//		progressModel.setMinimum(e.getStartValue());
//		progressModel.setMaximum(e.getEndValue());
//		progressModel.setValue(e.getCurrentValue());
//		repaint();
	}

	public Sequence getReferenceSequence() {
		return referenceSequence;
	}

	public void setReferenceSequence(Sequence referenceSequence) {
		de.biozentrum.bioinformatik.sequence.Sequence oldReferenceSequence = this.referenceSequence;
		this.referenceSequence = referenceSequence;
		
		int[] columnOffset = computeColumnOffsetRelativeTo(referenceSequence);
		if (!Arrays.equals(columnOffset, bundleConfig.getColumnOffset())) {
			bundleConfig.setColumnOffset(columnOffset);
		}
		
		if (this.referenceSequence==null) {
			bundleConfig.setShowingOverlay(false);
		} else {
			bundleConfig.setShowingOverlay(true);
		}
		
		propertyChangeSupport.firePropertyChange(PROP_REFERENCESEQUENCE, oldReferenceSequence, referenceSequence);
	}

	private int[] computeColumnOffsetRelativeTo(Sequence referenceSequence) {
		if (referenceSequence==null) {
			return new int[]{0};
		}
		
		int[] offset = new int[referenceSequence.length()];
		int minOffset = 0;
		int target = alphabet.size() / 2;
		
		// compute offset
		for (int i=0;i<referenceSequence.length();i++) {
			char c = referenceSequence.charAt(i);
			int alphaidx = alphabet.indexOf(c);
			if (alphaidx == -1) {
				if (c==bundleConfig.gapChar) {
					alphaidx = alphabet.size();
				} else {
					offset[i] = 0;
					break;
				}
			}
			offset[i] = - alphaidx + target;
			if (offset[i] < minOffset) {
				minOffset = offset[i];
			}
		}
		
		// make positive
		for (int i=0;i<offset.length;i++) {
			offset[i] =  offset[i] - minOffset;
		}
		
		return offset;
	}
	
	private class ColorPropertyChangeHandler implements PropertyChangeListener {
		@Override
		public void propertyChange(PropertyChangeEvent evt) {
			handleColorEvent(evt.getPropertyName());
		}
	}
	
	private class ColorModelChangeHandler implements ColorModelListener {
		private final String tag;
		ColorModelChangeHandler(String tag) {
			this.tag = tag;
		}
		@Override
		public void colorModelChanged(ColorChangedEvent e) {
			handleColorEvent(tag);
		}
		
	}
	
	private class ConfigPropertyChangeHandler implements PropertyChangeListener {
		@Override
		public void propertyChange(PropertyChangeEvent evt) {
			switch (evt.getPropertyName()) {
				case SequenceBundleConfig.PROP_SELECTIONENABLED:
					JSequenceBundle.this.clearSelection();						
					break;
				case SequenceBundleConfig.PROP_COLORMODEL:
					SequenceBundleColorModel newModel = (SequenceBundleColorModel) evt.getNewValue();
					SequenceBundleColorModel oldModel = (SequenceBundleColorModel) evt.getOldValue();
					
					// listen to new color model
					if (oldModel!=null) {
						oldModel.removePropertyChangeListener(colorPropertyChangeHandler);
						oldModel.getAlphabetColors().removeColorModelListener(colorModelHandlerAlphabet);
						oldModel.getGroupColors().removeColorModelListener(colorModelHandlerGroups);
					}
					
					if (newModel!=null) {
						newModel.addPropertyChangeListener(colorPropertyChangeHandler);
						newModel.getAlphabetColors().addColorModelListener(colorModelHandlerAlphabet);
						newModel.getGroupColors().addColorModelListener(colorModelHandlerGroups);
					}
					
					JSequenceBundle.this.setBackground(bundleConfig.colorModel.getBackgroundColor());				
					if (bundleConfig.automaticRedraw) {
						legendImage = legendRenderer.renderLegend();
						redraw();
					}
					if (sequenceBundleToolBar!=null) {		
						sequenceBundleToolBar.update();
					}
					break;
				case SequenceBundleConfig.PROP_GAPCHAR: // gap char, only render legend
					if (bundleConfig.automaticRedraw) {
						legendImage = legendRenderer.renderLegend();						
					}
					break;
				case SequenceBundleConfig.PROP_DPI: // dpi, cellHeight, anti-aliasing and offsetY, render legend and redraw
				case SequenceBundleConfig.PROP_CELLHEIGHT:
				case SequenceBundleConfig.PROP_OFFSETY:							
				case SequenceBundleConfig.PROP_ANTIALIASING:
				case SequenceBundleConfig.PROP_SPEEDOVERQUALITY:
				case SequenceBundleConfig.PROP_COLUMNOFFSET:
					if (bundleConfig.automaticRedraw) {
						legendImage = legendRenderer.renderLegend();						
					}
				case SequenceBundleConfig.PROP_CELLWIDTH: // all these only redraw					
				case SequenceBundleConfig.PROP_HORIZONTALEXTENT:
				case SequenceBundleConfig.PROP_MAXBUNDLEWIDTH:
				case SequenceBundleConfig.PROP_TANGL:
				case SequenceBundleConfig.PROP_TANGR:
				case SequenceBundleConfig.PROP_GAPRENDERING:
				case SequenceBundleConfig.PROP_GROUPSTACKING:
				case SequenceBundleConfig.PROP_MINALPHAPERTHREAD:
				case SequenceBundleConfig.PROP_MAXALPHATOTAL:
				case SequenceBundleConfig.PROP_CONSERVATIONTHRESHOLD:
					if (bundleConfig.automaticRedraw) {
						redraw();
					}
					break;
				case SequenceBundleConfig.PROP_SEQUENCEVISIBILITYMODEL:
					((SequenceVisibilityModel)evt.getOldValue()).removeVisibilityListener(JSequenceBundle.this);
					if (evt.getNewValue()!=null) {
						bundleConfig.sequenceVisibilityModel.addVisibilityListener(JSequenceBundle.this);
					}
					if (bundleConfig.automaticRedraw) {
						redraw();
					}
					break;
			}
			repaint(); // repaint in any case
		}
	}
	
	private class SequenceTransferHandler extends TransferHandler {
		int seqCount=0;
		
		@Override
		public int getSourceActions(JComponent c) {
			return COPY;
		}
		
		@Override
		public Transferable createTransferable(JComponent c) {
//			int[] selectedIndices = gridPointToSequenceIndices(getCurrentMouseGridPoint());
			int[] selectedIndices = GUIutils.getSelectedIndices(sequenceSelectionModel);
			seqCount=selectedIndices.length;
			Sequence[] selectedSequences = new Sequence[selectedIndices.length];
			for (int i=0;i<selectedIndices.length;i++){
				selectedSequences[i]=alignment.getSequence(selectedIndices[i]);
			}
			return new TransferableSequences(selectedSequences);
		}

		@Override
		public void exportDone(JComponent c, Transferable t, int action) {
			seqCount=0;
		}

		@Override
		public Image getDragImage() {
			return SequenceBundleResources.createDnDIcon(seqCount);
		}
		
		
	}
	
}
