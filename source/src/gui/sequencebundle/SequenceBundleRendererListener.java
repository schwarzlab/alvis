/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.sequencebundle;

import java.util.EventListener;

/**
 *
 * @author Roland Schwarz <rfs32@cam.ac.uk>
 */
public interface SequenceBundleRendererListener extends EventListener{
	public void renderingStarted(SequenceBundleRendererEvent e);
	public void renderingFinished(SequenceBundleRendererEvent e);
	public void renderingCancelled(SequenceBundleRendererEvent e);
	public void renderingProgressed(SequenceBundleRendererEvent e);
	
}
