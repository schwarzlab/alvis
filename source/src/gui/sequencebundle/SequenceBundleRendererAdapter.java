/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.sequencebundle;

/**
 *
 * @author Roland Schwarz <rfs32@cam.ac.uk>
 */
public class SequenceBundleRendererAdapter implements SequenceBundleRendererListener{
	@Override
	public void renderingStarted(SequenceBundleRendererEvent e) {
	}

	@Override
	public void renderingFinished(SequenceBundleRendererEvent e) {
	}

	@Override
	public void renderingCancelled(SequenceBundleRendererEvent e) {
	}

	@Override
	public void renderingProgressed(SequenceBundleRendererEvent e) {
	}
}
