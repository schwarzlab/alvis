/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.sequencebundle;

import com.general.containerModel.Nameable;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Dictionary;
import javax.swing.BoundedRangeModel;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultBoundedRangeModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.SpinnerNumberModel;

/**
 *
 * @author Roland Schwarz <rfs32@cam.ac.uk>
 */
public class SequenceBundleRenderOptionsPanel extends javax.swing.JPanel {
	private SequenceBundleConfig bundleConfig;
	SpinnerNumberModel modelCellWidth;
	SpinnerNumberModel modelCellHeight;
	SpinnerNumberModel modelMaxBundleWidth;
	SpinnerNumberModel modelTangL;
	SpinnerNumberModel modelTangR;
	SpinnerNumberModel modelHorizontalExtent;
	SpinnerNumberModel modelDpi;
	SpinnerNumberModel modelOffsetY;
	ComboBoxModel<SequenceBundleConfig.GapRenderingType> modelGapRendering;
	ComboBoxModel<SequenceBundleConfig.GroupStackingType> modelGroupStacking;
	BoundedRangeModel modelAlphaMin;
	BoundedRangeModel modelAlphaMax;
	SpinnerNumberModel modelConservationThreshold;
	Dictionary alphaLabelTable;
	
	/**
	 * Creates new form SequenceBundleOptionsPanel
	 */
	public SequenceBundleRenderOptionsPanel() {
		this(new SequenceBundleConfig());
	}
	
	public SequenceBundleRenderOptionsPanel(SequenceBundleConfig bundleConfig) {
		this.bundleConfig = bundleConfig;
		this.modelCellWidth = new SpinnerNumberModel(bundleConfig.cellWidth, SequenceBundleConfig.CELLWIDTH_MIN, SequenceBundleConfig.CELLWIDTH_MAX, 1);		
		this.modelCellHeight = new SpinnerNumberModel(bundleConfig.cellHeight, SequenceBundleConfig.CELLHEIGHT_MIN, SequenceBundleConfig.CELLHEIGHT_MAX, 1);
		this.modelMaxBundleWidth = new SpinnerNumberModel(bundleConfig.maxBundleWidth, SequenceBundleConfig.MAXBUNDLEWIDTH_MIN, SequenceBundleConfig.MAXBUNDLEWIDTH_MAX, 1);
		this.modelTangL = new SpinnerNumberModel(bundleConfig.tangL, SequenceBundleConfig.TANG_MIN, SequenceBundleConfig.TANG_MAX, 1);
		this.modelTangR = new SpinnerNumberModel(bundleConfig.tangR, SequenceBundleConfig.TANG_MIN, SequenceBundleConfig.TANG_MAX, 1);
		this.modelHorizontalExtent = new SpinnerNumberModel(bundleConfig.horizontalExtent, SequenceBundleConfig.HORIZONTALEXTENT_MIN, SequenceBundleConfig.HORIZONTALEXTEND_MAX, 0.01);
		this.modelDpi = new SpinnerNumberModel(bundleConfig.dpi, SequenceBundleConfig.DPI_MIN, SequenceBundleConfig.DPI_MAX, 1);		
		this.modelOffsetY = new SpinnerNumberModel(bundleConfig.offsetY, SequenceBundleConfig.OFFSETY_MIN, SequenceBundleConfig.OFFSETY_MAX, 1);				
		this.modelGapRendering = new DefaultComboBoxModel<>(
				new SequenceBundleConfig.GapRenderingType[]{
					SequenceBundleConfig.GapRenderingType.STANDARD, 
					SequenceBundleConfig.GapRenderingType.DISCONNECTED}
		);
		this.modelGroupStacking = new DefaultComboBoxModel<>(
				new SequenceBundleConfig.GroupStackingType[]{
					SequenceBundleConfig.GroupStackingType.SEPARATE,
					SequenceBundleConfig.GroupStackingType.OVERLAYED}
		);
		this.modelAlphaMin = new DefaultBoundedRangeModel((int)(bundleConfig.minAlphaPerThread * 100), 0, (int)(SequenceBundleConfig.ALPHA_MIN * 100), (int)(SequenceBundleConfig.ALPHA_MAX * 100));
		this.modelAlphaMax = new DefaultBoundedRangeModel((int)(bundleConfig.maxAlphaTotal * 100), 0, (int)(SequenceBundleConfig.ALPHA_MIN * 100), (int)(SequenceBundleConfig.ALPHA_MAX * 100));
		this.modelConservationThreshold = new SpinnerNumberModel(bundleConfig.conservationThreshold * 100, SequenceBundleConfig.CONSERVATION_THRESHOLD_MIN * 100, SequenceBundleConfig.CONSERVATION_THRESHOLD_MAX * 100, 0.1);
		
		initComponents();
		slideMinimumAlpha.setModel(modelAlphaMin);
		slideMaximumAlpha.setModel(modelAlphaMax);
		alphaLabelTable = slideMinimumAlpha.getLabelTable();
		alphaLabelTable.put((int)(SequenceBundleConfig.ALPHA_MIN * 100), new JLabel(String.valueOf((int)(SequenceBundleConfig.ALPHA_MIN * 100))));
		alphaLabelTable.put((int)(SequenceBundleConfig.ALPHA_MAX * 100), new JLabel(String.valueOf((int)(SequenceBundleConfig.ALPHA_MAX * 100))));
		slideMinimumAlpha.setLabelTable(alphaLabelTable);
		slideMaximumAlpha.setLabelTable(alphaLabelTable);
		
		updateValues(null, this.bundleConfig);
		this.bundleConfig.addPropertyChangeListener(new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				updateValues(evt, SequenceBundleRenderOptionsPanel.this.bundleConfig);
			}
		});
		cmdDefaults.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				updateValues(null, new SequenceBundleConfig()); // set to default values
			}
		});
	}
	
	private void updateValues(PropertyChangeEvent evt, SequenceBundleConfig bundleConfig) {
		if (evt==null) {
			this.modelCellWidth.setValue(bundleConfig.cellWidth);
			this.modelCellHeight.setValue(bundleConfig.cellHeight);
			this.modelMaxBundleWidth.setValue(bundleConfig.maxBundleWidth);
			this.modelTangL.setValue(bundleConfig.tangL);
			this.modelTangR.setValue(bundleConfig.tangR);
			this.modelHorizontalExtent.setValue(bundleConfig.horizontalExtent);
			this.modelDpi.setValue(bundleConfig.dpi);
			this.modelOffsetY.setValue(bundleConfig.offsetY);
			this.chkAntiAliasing.setSelected(bundleConfig.antiAliasing);
			this.chkSpeed.setSelected(!bundleConfig.speedOverQuality);
			this.chkHorizontalLines.setSelected(bundleConfig.showingHorizontalLines);
			this.chkVerticalLines.setSelected(bundleConfig.showingVerticalLines);
			this.chkShowSelection.setSelected(bundleConfig.showingSelection);
			this.chkShowConsensus.setSelected(bundleConfig.showingConsensus);
			this.chkAlphabetOverlay.setSelected(bundleConfig.showingOverlay);
			this.modelGapRendering.setSelectedItem(bundleConfig.gapRendering);
			this.modelGroupStacking.setSelectedItem(bundleConfig.groupStacking);
			this.modelAlphaMin.setValue((int)(bundleConfig.minAlphaPerThread * 100));
			this.modelAlphaMax.setValue((int)(bundleConfig.maxAlphaTotal * 100));
			this.modelConservationThreshold.setValue(bundleConfig.conservationThreshold * 100);
		} else {
			switch (evt.getPropertyName()) {
				case SequenceBundleConfig.PROP_CELLWIDTH:
					this.modelCellWidth.setValue(bundleConfig.cellWidth);
					break;
				case SequenceBundleConfig.PROP_CELLHEIGHT:
					this.modelCellHeight.setValue(bundleConfig.cellHeight);
					break;
				case SequenceBundleConfig.PROP_MAXBUNDLEWIDTH:
					this.modelMaxBundleWidth.setValue(bundleConfig.maxBundleWidth);
					break;
				case SequenceBundleConfig.PROP_TANGL:
					this.modelTangL.setValue(bundleConfig.tangL);
					break;
				case SequenceBundleConfig.PROP_TANGR:
					this.modelTangR.setValue(bundleConfig.tangR);
					break;
				case SequenceBundleConfig.PROP_HORIZONTALEXTENT:
					this.modelHorizontalExtent.setValue(bundleConfig.horizontalExtent);
					break;
				case SequenceBundleConfig.PROP_DPI:
					this.modelDpi.setValue(bundleConfig.dpi);
					break;
				case SequenceBundleConfig.PROP_OFFSETY:
					this.modelOffsetY.setValue(bundleConfig.offsetY);
					break;
				case SequenceBundleConfig.PROP_ANTIALIASING:
					this.chkAntiAliasing.setSelected(bundleConfig.antiAliasing);
					break;
				case SequenceBundleConfig.PROP_SPEEDOVERQUALITY:
					this.chkSpeed.setSelected(!bundleConfig.speedOverQuality);
					break;
				case SequenceBundleConfig.PROP_SHOWINGHORIZONTALLINES:
					this.chkHorizontalLines.setSelected(bundleConfig.showingHorizontalLines);
					break;
				case SequenceBundleConfig.PROP_SHOWINGVERTICALLINES:
					this.chkVerticalLines.setSelected(bundleConfig.showingVerticalLines);
					break;
				case SequenceBundleConfig.PROP_SHOWINGSELECTION:
					this.chkShowSelection.setSelected(bundleConfig.showingSelection);
					break;
				case SequenceBundleConfig.PROP_SHOWINGCONSENSUS:
					this.chkShowConsensus.setSelected(bundleConfig.showingConsensus);
					break;
				case SequenceBundleConfig.PROP_SHOWINGOVERLAY:
					this.chkAlphabetOverlay.setSelected(bundleConfig.showingOverlay);
					break;
				case SequenceBundleConfig.PROP_GAPRENDERING:
					this.modelGapRendering.setSelectedItem(bundleConfig.gapRendering);
					break;
				case SequenceBundleConfig.PROP_GROUPSTACKING:
					this.modelGroupStacking.setSelectedItem(bundleConfig.groupStacking);
					break;
				case SequenceBundleConfig.PROP_MINALPHAPERTHREAD:
					this.modelAlphaMin.setValue((int)(bundleConfig.minAlphaPerThread * 100));
					break;
				case SequenceBundleConfig.PROP_MAXALPHATOTAL:
					this.modelAlphaMax.setValue((int)(bundleConfig.maxAlphaTotal * 100));
					break;
				case SequenceBundleConfig.PROP_CONSERVATIONTHRESHOLD:
					this.modelConservationThreshold.setValue(bundleConfig.conservationThreshold * 100);
					break;					

			}
		}
	}

	/**
	 * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always regenerated by the Form Editor.
	 */
	@SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanel1 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jSpinner2 = new javax.swing.JSpinner();
        jSpinner3 = new javax.swing.JSpinner();
        jSpinner4 = new javax.swing.JSpinner();
        jSpinner5 = new javax.swing.JSpinner();
        jSpinner6 = new javax.swing.JSpinner();
        jSpinner7 = new javax.swing.JSpinner();
        jSpinner8 = new javax.swing.JSpinner();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        cmdDefaults = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        chkSpeed = new javax.swing.JCheckBox();
        chkAntiAliasing = new javax.swing.JCheckBox();
        jPanel3 = new javax.swing.JPanel();
        chkHorizontalLines = new javax.swing.JCheckBox();
        chkVerticalLines = new javax.swing.JCheckBox();
        chkAlphabetOverlay = new javax.swing.JCheckBox();
        jPanel4 = new javax.swing.JPanel();
        chkShowSelection = new javax.swing.JCheckBox();
        chkShowConsensus = new javax.swing.JCheckBox();
        jLabel1 = new javax.swing.JLabel();
        cmbGapRendering = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        slideMinimumAlpha = new javax.swing.JSlider();
        jLabel5 = new javax.swing.JLabel();
        slideMaximumAlpha = new javax.swing.JSlider();
        jLabel18 = new javax.swing.JLabel();
        cmbGroupStacking = new javax.swing.JComboBox();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jSpinner1 = new javax.swing.JSpinner();

        setLayout(new java.awt.BorderLayout());

        jPanel1.setLayout(new java.awt.GridBagLayout());

        jLabel3.setText("Cell width:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(jLabel3, gridBagConstraints);

        jLabel4.setText("Cell height:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(jLabel4, gridBagConstraints);

        jLabel6.setText("Maximum stack height:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(jLabel6, gridBagConstraints);

        jLabel7.setText("Global Y-Axis offset:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(jLabel7, gridBagConstraints);

        jLabel8.setText("Bundle curvature left:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(jLabel8, gridBagConstraints);

        jLabel9.setText("Bundle curvature right:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(jLabel9, gridBagConstraints);

        jLabel10.setText("Base width:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(jLabel10, gridBagConstraints);

        jSpinner2.setModel(modelCellWidth);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(jSpinner2, gridBagConstraints);

        jSpinner3.setModel(modelCellHeight);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(jSpinner3, gridBagConstraints);

        jSpinner4.setModel(modelMaxBundleWidth);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(jSpinner4, gridBagConstraints);

        jSpinner5.setModel(modelTangL);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(jSpinner5, gridBagConstraints);

        jSpinner6.setModel(modelTangR);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(jSpinner6, gridBagConstraints);

        jSpinner7.setModel(modelHorizontalExtent);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(jSpinner7, gridBagConstraints);

        jSpinner8.setModel(modelOffsetY);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(jSpinner8, gridBagConstraints);

        jLabel11.setText("px");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(jLabel11, gridBagConstraints);

        jLabel12.setText("px");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(jLabel12, gridBagConstraints);

        jLabel13.setText("px");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(jLabel13, gridBagConstraints);

        jLabel14.setText("px");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(jLabel14, gridBagConstraints);

        jLabel15.setText("px");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(jLabel15, gridBagConstraints);

        jLabel16.setText("* cell width");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(jLabel16, gridBagConstraints);

        jLabel17.setText("px");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(jLabel17, gridBagConstraints);

        cmdDefaults.setText("Restore default values");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 15;
        gridBagConstraints.gridwidth = 3;
        jPanel1.add(cmdDefaults, gridBagConstraints);

        chkSpeed.setText("High quality rendering");
        jPanel2.add(chkSpeed);

        chkAntiAliasing.setText("Anti-aliasing");
        jPanel2.add(chkAntiAliasing);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 14;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(jPanel2, gridBagConstraints);

        chkHorizontalLines.setText("Show horizontal grid lines");
        jPanel3.add(chkHorizontalLines);

        chkVerticalLines.setText("Show vertical grid lines");
        jPanel3.add(chkVerticalLines);

        chkAlphabetOverlay.setText("Show alphabet overlay");
        jPanel3.add(chkAlphabetOverlay);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 13;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weighty = 1.0;
        jPanel1.add(jPanel3, gridBagConstraints);

        chkShowSelection.setText("Show selected sequences");
        jPanel4.add(chkShowSelection);

        chkShowConsensus.setText("Show consensus sequence");
        jPanel4.add(chkShowConsensus);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 12;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weighty = 1.0;
        jPanel1.add(jPanel4, gridBagConstraints);

        jLabel1.setText("Gap rendering:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 11;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(jLabel1, gridBagConstraints);

        cmbGapRendering.setModel(modelGapRendering);
        cmbGapRendering.setRenderer(new NameableCellRenderer());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 11;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(cmbGapRendering, gridBagConstraints);

        jLabel2.setText("Minimum transparency per thread");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(jLabel2, gridBagConstraints);

        slideMinimumAlpha.setMajorTickSpacing(25);
        slideMinimumAlpha.setMinorTickSpacing(5);
        slideMinimumAlpha.setPaintLabels(true);
        slideMinimumAlpha.setPaintTicks(true);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(slideMinimumAlpha, gridBagConstraints);

        jLabel5.setText("Maximum total transparency");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 9;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(jLabel5, gridBagConstraints);

        slideMaximumAlpha.setMajorTickSpacing(25);
        slideMaximumAlpha.setMinorTickSpacing(5);
        slideMaximumAlpha.setPaintLabels(true);
        slideMaximumAlpha.setPaintTicks(true);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 9;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(slideMaximumAlpha, gridBagConstraints);

        jLabel18.setText("Group stacking");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 10;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(jLabel18, gridBagConstraints);

        cmbGroupStacking.setModel(modelGroupStacking);
        cmbGroupStacking.setRenderer(new NameableCellRenderer());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 10;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(cmbGroupStacking, gridBagConstraints);

        jLabel19.setText("Conservation threshold:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(jLabel19, gridBagConstraints);

        jLabel20.setText("%");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(jLabel20, gridBagConstraints);

        jLabel21.setText("%");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(jLabel21, gridBagConstraints);

        jLabel22.setText("%");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 9;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(jLabel22, gridBagConstraints);

        jSpinner1.setModel(modelConservationThreshold);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanel1.add(jSpinner1, gridBagConstraints);

        add(jPanel1, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    javax.swing.JCheckBox chkAlphabetOverlay;
    javax.swing.JCheckBox chkAntiAliasing;
    javax.swing.JCheckBox chkHorizontalLines;
    javax.swing.JCheckBox chkShowConsensus;
    javax.swing.JCheckBox chkShowSelection;
    javax.swing.JCheckBox chkSpeed;
    javax.swing.JCheckBox chkVerticalLines;
    javax.swing.JComboBox cmbGapRendering;
    javax.swing.JComboBox cmbGroupStacking;
    private javax.swing.JButton cmdDefaults;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JSpinner jSpinner1;
    private javax.swing.JSpinner jSpinner2;
    private javax.swing.JSpinner jSpinner3;
    private javax.swing.JSpinner jSpinner4;
    private javax.swing.JSpinner jSpinner5;
    private javax.swing.JSpinner jSpinner6;
    private javax.swing.JSpinner jSpinner7;
    private javax.swing.JSpinner jSpinner8;
    javax.swing.JSlider slideMaximumAlpha;
    javax.swing.JSlider slideMinimumAlpha;
    // End of variables declaration//GEN-END:variables

	public SpinnerNumberModel getModelCellWidth() {
		return modelCellWidth;
	}

	public SpinnerNumberModel getModelCellHeight() {
		return modelCellHeight;
	}

	public SpinnerNumberModel getModelMaxBundleWidth() {
		return modelMaxBundleWidth;
	}

	public SpinnerNumberModel getModelTangL() {
		return modelTangL;
	}

	public SpinnerNumberModel getModelTangR() {
		return modelTangR;
	}

	public SpinnerNumberModel getModelHorizontalExtent() {
		return modelHorizontalExtent;
	}

	public SpinnerNumberModel getModelDpi() {
		return modelDpi;
	}

	public SpinnerNumberModel getModelOffsetY() {
		return modelOffsetY;
	}

	private class NameableCellRenderer extends DefaultListCellRenderer {

		@Override
		public Component getListCellRendererComponent(JList list, Object value,
							int index, boolean isSelected, boolean cellHasFocus) {

			JComponent comp = (JComponent) super.getListCellRendererComponent(list,
					value, index, isSelected, cellHasFocus);

			if (index>=0 && value != null) {
				Nameable type = (Nameable) list.getSelectedValue();
				list.setToolTipText(type.getDescription());
			}
			return comp;
		}

	}	
}
