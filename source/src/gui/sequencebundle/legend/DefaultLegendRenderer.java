/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.sequencebundle.legend;

import com.general.utils.GeneralUtils;
import de.biozentrum.bioinformatik.sequence.SequenceAlphabet;
import gui.sequencebundle.SequenceBundleConfig;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Collections;


public final class DefaultLegendRenderer extends AbstractLegendRenderer {	
	private Dimension legendSize;
	
	public DefaultLegendRenderer(SequenceAlphabet alphabet) {
		this(alphabet, new SequenceBundleConfig());
		initSize();
	}
	
	public DefaultLegendRenderer(SequenceAlphabet alphabet, SequenceBundleConfig bundleConfig) {
		this.alphabet = new ReorderedImmutableSequenceAlphabet(requestedAlphabetOrder(alphabet), alphabet);	
		this.bundleConfig = bundleConfig;
	
		initSize();
	}

	@Override
	public void renderLegend(Graphics2D g) {
		initSize();
		g = (Graphics2D) g.create();
		g.setTransform(new AffineTransform());
		setGlobalGraphicsParameters(g);
		
		int cellHeight = bundleConfig.getCellHeight();
		int offsetY = bundleConfig.getOffsetY();
		// begin draw legend and horizontal lines
		Font f = new Font(g.getFont().getName(), g.getFont().getStyle(), DEFAULT_TEXT_SIZE_ALPHABET);
		g.setFont(f);
		g.setColor(bundleConfig.getColorModel().getFontColor());

		int fontSize = cellHeight - 2;

        for (int i=0; i<=getAlphabet().size(); i++) {
			int pos = offsetY + (i+1) * cellHeight - (cellHeight-(fontSize-5))/2; 
            if (i<getAlphabet().size()) {
                g.drawString(getAlphabet().characterAt(i).toString().toUpperCase(), DEFAULT_OFFSET_X, pos);
            } else {
                g.drawString(Character.toString(DEFAULT_GAP_CHAR), DEFAULT_OFFSET_X, pos);
            }
        }
        // end draw legend
		g.dispose();
	}
	
	@Override
	public BufferedImage renderLegend() {
		initSize();
		BufferedImage img = createBufferedImage(legendSize.width, legendSize.height);
		Graphics2D g = createBufferedImageGraphics(img);
		renderLegend(g);
		g.dispose();
		return img;        		
		
	}

	@Override
	protected void initSize() {
		legendSize = new Dimension(DEFAULT_OFFSET_X + 50, 2 * bundleConfig.getOffsetY() + (alphabet.size()+1) * bundleConfig.getCellHeight());
	}

	@Override
	public Dimension getLegendSize() {
		return legendSize;
	}

	protected int[] requestedAlphabetOrder(SequenceAlphabet alphabet) {
		int[] retval = new int[alphabet.size()];
		ArrayList<Character> newOrder = new ArrayList<>(alphabet);
		Collections.sort(newOrder);
		//Collections.reverse(newOrder);
		for (int i=0;i<alphabet.size();i++) {
			int index = newOrder.indexOf(alphabet.characterAt(i));
			retval[i] = index;
		}
		retval = GeneralUtils.order(retval);
		return retval;
	}
	
}
