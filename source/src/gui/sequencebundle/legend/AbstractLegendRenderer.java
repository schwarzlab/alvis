/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.sequencebundle.legend;

import com.general.utils.GUIutils;
import de.biozentrum.bioinformatik.sequence.SequenceAlphabet;
import gui.sequencebundle.SequenceBundleConfig;
import gui.sequencebundle.SequenceBundleResources;
import gui.sequencebundle.aaindex.AAIndexEntry;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsEnvironment;
import java.awt.Transparency;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public abstract class AbstractLegendRenderer {
	public final static int DEFAULT_TEXT_SIZE_ALPHABET=15;
	public final static int DEFAULT_OFFSET_X = 13;
	public final static char DEFAULT_GAP_CHAR='-';
	
	protected SequenceAlphabet alphabet;
	protected SequenceBundleConfig bundleConfig;
	protected GraphicsConfiguration graphicsConfig = null;
	
	public AbstractLegendRenderer(){
		this(null, null);
	}
	
	public AbstractLegendRenderer(SequenceAlphabet alphabet, SequenceBundleConfig bundleConfig) {
		this.alphabet = alphabet;
		this.bundleConfig = bundleConfig;
		if (!GraphicsEnvironment.getLocalGraphicsEnvironment().isHeadlessInstance()) {		
			graphicsConfig = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration();
		}
	}

	public abstract BufferedImage renderLegend();
	public abstract void renderLegend(Graphics2D g);
	
	public static AbstractLegendRenderer createDefaultLegendRenderer(SequenceAlphabet alpha, SequenceBundleConfig bundleConfig) {
		if (alpha.equals(SequenceAlphabet.AMINOACIDS)) {
			return new DefaultAminoAcidLegendRenderer(alpha, bundleConfig);
		} else if (alpha.equals(SequenceAlphabet.NUCLEOTIDES) || alpha.equals(SequenceAlphabet.RNA)) {
			return new DefaultNucleotideLegendRenderer(alpha, bundleConfig);
		} else {
			return new DefaultLegendRenderer(alpha, bundleConfig);
		}
	}
	
	public static AbstractLegendRenderer createAAIndexLegendRenderer(SequenceAlphabet alpha, SequenceBundleConfig bundleConfig, AAIndexEntry aaindex){
		return new AAIndexAminoAcidLegendRenderer(alpha, aaindex, bundleConfig);
	}
	
	/**
	 * @return the alphabet
	 */
	public SequenceAlphabet getAlphabet() {
		return alphabet;
	}

	public void setBundleConfig(SequenceBundleConfig bundleConfig) {
		this.bundleConfig = bundleConfig;
		initSize();
	}
	
	public SequenceBundleConfig getBundleConfig(){
		return this.bundleConfig;
	}

	protected abstract void initSize();
	
	/**
	 * @param alphabet the alphabet to set
	 */
	public void setAlphabet(SequenceAlphabet alphabet) {
		this.alphabet = alphabet;
		initSize();
	}

	Graphics2D setGlobalGraphicsParameters(Graphics2D g) {
		// scale
		double scale = bundleConfig.getDpi() / SequenceBundleConfig.DEFAULT_DPI;
		AffineTransform at = (AffineTransform) g.getTransform().clone(); // DiskMemImageGraphics doesn't return a copy on getTransform as it should
		at.setTransform(
				scale, 
				at.getShearY(), 
				at.getShearX(), 
				scale, 
				at.getTranslateX(), 
				at.getTranslateY());
		g.setTransform(at);
		
		// set font
		g.setFont(SequenceBundleResources.SB_FONT_REGULAR);
		
		return g;
	}
	
	public abstract Dimension getLegendSize();
	public Dimension getScaledLegendSize() {
		double scale = bundleConfig.getDpi() / SequenceBundleConfig.DEFAULT_DPI;
		Dimension d = new Dimension((int)Math.ceil(getLegendSize().width * scale), (int)Math.ceil(getLegendSize().height * scale));
		return d;
	}
	
	BufferedImage createBufferedImage(int width, int height){
		int scaledWidth = (int)Math.ceil(width * bundleConfig.getDpi() / SequenceBundleConfig.DEFAULT_DPI);
		int scaledHeight = (int)Math.ceil(height * bundleConfig.getDpi() / SequenceBundleConfig.DEFAULT_DPI);
		if (graphicsConfig!=null) {
			return graphicsConfig.createCompatibleImage(scaledWidth, scaledHeight, Transparency.TRANSLUCENT);
		} else {
			return new BufferedImage(scaledWidth, scaledHeight, BufferedImage.TYPE_INT_ARGB);
		}
	}
	
	Graphics2D createBufferedImageGraphics(BufferedImage img) {
		Graphics2D g = img.createGraphics();
		GUIutils.setQualityRenderingHints(g, bundleConfig.getAntiAliasing(), bundleConfig.getSpeedOverQuality());
		g.scale(bundleConfig.getDpi() / SequenceBundleConfig.DEFAULT_DPI, bundleConfig.getDpi() / SequenceBundleConfig.DEFAULT_DPI);
		return g;
	}
	
	
}
