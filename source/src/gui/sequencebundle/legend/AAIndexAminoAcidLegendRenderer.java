/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.sequencebundle.legend;

import com.dictiography.collections.IndexedNavigableMap;
import com.general.utils.GUIutils;
import com.general.utils.GeneralUtils;
import com.general.utils.StringUtils;
import de.biozentrum.bioinformatik.sequence.SequenceAlphabet;
import gui.sequencebundle.SequenceBundleColorModel;
import gui.sequencebundle.SequenceBundleConfig;
import gui.sequencebundle.aaindex.AAIndexEntry;
import static gui.sequencebundle.legend.AbstractLegendRenderer.DEFAULT_GAP_CHAR;
import static gui.sequencebundle.legend.AbstractLegendRenderer.DEFAULT_OFFSET_X;
import static gui.sequencebundle.legend.AbstractLegendRenderer.DEFAULT_TEXT_SIZE_ALPHABET;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.font.TextLayout;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import org.biojava.bio.BioException;
import org.biojava.bio.seq.ProteinTools;
import org.biojava.bio.symbol.AtomicSymbol;
import org.biojava.bio.symbol.Symbol;

/**
 *
 * @author Roland Schwarz <rfs32@cam.ac.uk>
 */
public class AAIndexAminoAcidLegendRenderer extends AbstractLegendRenderer{
	private final AAIndexEntry aaindex;
	private Map<Character, Color> aaIndexColorMap;
	private Dimension legendSize; 
	
	public AAIndexAminoAcidLegendRenderer(SequenceAlphabet alphabet, AAIndexEntry aaindex) {
		this.aaindex = aaindex;
		this.alphabet = new ReorderedImmutableSequenceAlphabet(requestedAlphabetOrder(alphabet), alphabet);
		this.bundleConfig=new SequenceBundleConfig();
		initGradient();
		initSize();
	}

	public AAIndexAminoAcidLegendRenderer(SequenceAlphabet alphabet, AAIndexEntry aaindex, SequenceBundleConfig bundleConfig) {
		this.aaindex = aaindex;
		this.alphabet = new ReorderedImmutableSequenceAlphabet(requestedAlphabetOrder(alphabet), alphabet);
		this.bundleConfig = bundleConfig;
		initGradient();
		initSize();
	}

	protected  void initSize() {
		legendSize = new Dimension(DEFAULT_OFFSET_X + 170, 2*bundleConfig.getOffsetY() + (alphabet.size()+1)* bundleConfig.getCellHeight());		
	}
	
	
	private void initGradient() {
		double[] values = GeneralUtils.doubleArrayToDouble(aaindex.getAminoAcidIndices().values().toArray(new Double[]{}));
		Color[] colors = GUIutils.createColorGradient(Color.WHITE, Color.MAGENTA, values, 20);
		Character[] keys = aaindex.getAminoAcidIndices().navigableKeySet().toArray(new Character[]{});
		aaIndexColorMap = new HashMap<>(keys.length);
		for (int i=0;i<keys.length;i++){
			aaIndexColorMap.put(keys[i], colors[i]);
		}
	}

	@Override
	public void renderLegend(Graphics2D g) {
		initSize();
		g = (Graphics2D) g.create();
		g.setTransform(new AffineTransform());
		setGlobalGraphicsParameters(g);
		
		int offsetY = bundleConfig.getOffsetY();
		int cellHeight = bundleConfig.getCellHeight();
		AffineTransform trans = g.getTransform();		
		g.setColor(bundleConfig.getColorModel().getFontColor());				
		String text = aaindex.getDataDescription();

		TextLayout tl = new TextLayout(text, g.getFont(), g.getFontRenderContext());
		int from = offsetY;
		int to = offsetY + (alphabet.size()+1) * cellHeight;
		int textpos = (to+from)/2 + (int)tl.getBounds().getWidth()/2;
		
		g.translate(DEFAULT_OFFSET_X + 160, textpos);
		g.rotate((float) Math.toRadians(-90));		
		g.drawString(text, 0, 0);
		g.setTransform(trans);
		
        for (int i=0; i<=getAlphabet().size(); i++) {
			int centerY = offsetY + (i+1) * cellHeight - (cellHeight/2);			
			FontMetrics fontMetrics = g.getFontMetrics();
			int pos = centerY + fontMetrics.getAscent()/2;			

            if (i<getAlphabet().size()) {
				Character letter = getAlphabet().characterAt(i);
				String threeLetter;
				try {
					Symbol symbol = ProteinTools.getAlphabet().getTokenization("token").parseToken(letter.toString());
					if (symbol instanceof AtomicSymbol) { // not ambiguous
						threeLetter = symbol.getName();
					} else {
						threeLetter = "*";
					}
					threeLetter = StringUtils.firstCharToCapital(threeLetter);
				} catch (BioException ex) {
					threeLetter="<unk>";
				}
				
				Color chemPropColor= aaIndexColorMap.get(letter);
				if (chemPropColor==null) {
					chemPropColor=DefaultAminoAcidLegendRenderer.COLOR_ELSE;
				}

				Font f = new Font(g.getFont().getName(), g.getFont().getStyle(), DEFAULT_TEXT_SIZE_ALPHABET);
				g.setFont(f);
				g.setColor(bundleConfig.getColorModel().getFontColor());				
				g.drawString(Character.toString(letter), DEFAULT_OFFSET_X, pos);
				g.drawString(threeLetter, DEFAULT_OFFSET_X + 30, pos);

				Double value = aaindex.getAminoAcidIndices().get(letter);
				if (value!=null && !value.isNaN()) {
					f = new Font(g.getFont().getName(), g.getFont().getStyle(), DEFAULT_TEXT_SIZE_ALPHABET-4);
					g.setFont(f);
					FontMetrics fm = g.getFontMetrics();
					g.drawString(new DecimalFormat("##.##").format(value), DEFAULT_OFFSET_X + 110, centerY + fm.getAscent()/2);
				}
				g.setColor(chemPropColor);
				g.fillRect(DEFAULT_OFFSET_X + 70, offsetY + i*cellHeight, 25, cellHeight);
				
            } else {
				Font f = new Font(g.getFont().getName(), g.getFont().getStyle(), DEFAULT_TEXT_SIZE_ALPHABET);
				g.setFont(f);
				g.setColor(bundleConfig.getColorModel().getFontColor());
                g.drawString(Character.toString(DEFAULT_GAP_CHAR), DEFAULT_OFFSET_X, pos);
				g.drawString("Gap", DEFAULT_OFFSET_X + 30, pos);				
            }
        }
		
        // end draw legend
		g.dispose();
	}
	
	@Override
	public BufferedImage renderLegend() {
		initSize();
		BufferedImage img = createBufferedImage(legendSize.width, legendSize.height);
		Graphics2D g = createBufferedImageGraphics(img);
		renderLegend(g);
		g.dispose();
		return img;       
	}
	
	protected int[] requestedAlphabetOrder(SequenceAlphabet alphabet) {
		IndexedNavigableMap<Character, Double> map = aaindex.getAminoAcidIndices();
		
		int[] orderIndex = GeneralUtils.order(new ArrayList<>(alphabet), new Comparator<Character>() {
			@Override
			public int compare(Character o1, Character o2) {
				Double d1 = aaindex.getAminoAcidIndices().get(o1);
				Double d2 = aaindex.getAminoAcidIndices().get(o2);
				if (d1==null) {
					d1 = 0.0;
				} 
				if (d2==null) {
					d2 = 0.0;
				}
				return -d1.compareTo(d2);
			}
		});
		int[] order = new int[alphabet.size()];
		for (int i=0;i<orderIndex.length;i++) {
			order[i]=orderIndex[i];
		}
		for (int i=orderIndex.length;i<alphabet.size();i++) {
			order[i]=i;
		}
		return order;
	}
	
	protected int getTextPositionForCentering(Graphics2D g, String text, char charFrom, char charTo, int offsetY, int cellHeight) {
			TextLayout tl = new TextLayout(text, g.getFont(), g.getFontRenderContext());
			int from = offsetY + alphabet.indexOf(charFrom) * cellHeight;
			int to = offsetY + (alphabet.indexOf(charTo)+1) * cellHeight;
			int textpos = (to+from)/2 + (int)tl.getBounds().getWidth()/2;
			return textpos;
	}

	@Override
	public Dimension getLegendSize() {
		return legendSize;
	}

}
