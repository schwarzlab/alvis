/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.sequencebundle.legend;

import com.general.utils.GeneralUtils;
import com.general.utils.StringUtils;
import de.biozentrum.bioinformatik.sequence.SequenceAlphabet;
import gui.sequencebundle.SequenceBundleConfig;
import static gui.sequencebundle.legend.AbstractLegendRenderer.DEFAULT_OFFSET_X;
import static gui.sequencebundle.legend.AbstractLegendRenderer.DEFAULT_TEXT_SIZE_ALPHABET;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.font.TextLayout;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import org.biojava.bio.BioException;
import org.biojava.bio.seq.ProteinTools;
import org.biojava.bio.symbol.AtomicSymbol;
import org.biojava.bio.symbol.Symbol;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public final class DefaultAminoAcidLegendRenderer extends AbstractLegendRenderer{
	static final int CATEGORY_TEXT_SIZE=18;
	
	static final Color COLOR_CHARGED = new Color(166,206,227);
	static final Color COLOR_POLAR = new Color(31,120,180);
	static final Color COLOR_HYDROPHOBIC = new Color(178,223,138);
	static final Color COLOR_ELSE = Color.GRAY;

	static enum Category {CHARGED, POLAR, HYDROPHOBIC};
	static final Map<Category, Color> colorMap = new HashMap<>();
	static{
		colorMap.put(Category.CHARGED, COLOR_CHARGED);
		colorMap.put(Category.HYDROPHOBIC, COLOR_HYDROPHOBIC);
		colorMap.put(Category.POLAR, COLOR_POLAR);
	}
	
	static final Map<Category, String> legendMap = new HashMap<>();
	static{
		legendMap.put(Category.CHARGED, "charged");
		legendMap.put(Category.HYDROPHOBIC, "hydrophobic");
		legendMap.put(Category.POLAR, "polar");
	}
	
	static final Map<Character, Category> aaMap = new LinkedHashMap<>();
	static{
		aaMap.put('R', Category.CHARGED);
		aaMap.put('K', Category.CHARGED);
		aaMap.put('D', Category.CHARGED);
		aaMap.put('E', Category.CHARGED);
		
		aaMap.put('Q', Category.POLAR);
		aaMap.put('N', Category.POLAR);
		aaMap.put('H', Category.POLAR);
		aaMap.put('S', Category.POLAR);
		aaMap.put('T', Category.POLAR);
		aaMap.put('Y', Category.POLAR);
		aaMap.put('C', Category.POLAR);
		aaMap.put('M', Category.POLAR);
		aaMap.put('W', Category.POLAR);
		
		aaMap.put('A', Category.HYDROPHOBIC);
		aaMap.put('I', Category.HYDROPHOBIC);
		aaMap.put('L', Category.HYDROPHOBIC);
		aaMap.put('F', Category.HYDROPHOBIC);
		aaMap.put('V', Category.HYDROPHOBIC);
		aaMap.put('P', Category.HYDROPHOBIC);
		aaMap.put('G', Category.HYDROPHOBIC);
	}
	
	private Dimension legendSize;
	
	public DefaultAminoAcidLegendRenderer() {
		this(new SequenceAlphabet(), new SequenceBundleConfig());
		initSize();
	}
	
	public DefaultAminoAcidLegendRenderer(SequenceAlphabet alphabet) {
		this(alphabet, new SequenceBundleConfig());
		initSize();
	}
	
	public DefaultAminoAcidLegendRenderer(SequenceAlphabet alphabet, SequenceBundleConfig bundleConfig){
		this.alphabet = new ReorderedImmutableSequenceAlphabet(requestedAlphabetOrder(alphabet), alphabet);
		this.bundleConfig = bundleConfig;
		initSize();
	}

	@Override
	protected void initSize() {
		legendSize = new  Dimension(DEFAULT_OFFSET_X + 150, 2 * bundleConfig.getOffsetY() + (alphabet.size()+1)*bundleConfig.getCellHeight());
	}

	@Override
	public void renderLegend(Graphics2D g) {
		initSize();
		
		g = (Graphics2D) g.create();
		setGlobalGraphicsParameters(g);
		
		// begin draw legend and horizontal lines
		Font f = new Font(g.getFont().getName(), g.getFont().getStyle(), DEFAULT_TEXT_SIZE_ALPHABET);
		g.setFont(f);

		int offsetY = bundleConfig.getOffsetY();
		int cellHeight = bundleConfig.getCellHeight();
        for (int i=0; i<=getAlphabet().size(); i++) {
			int centerY = offsetY + (i+1) * cellHeight - (cellHeight/2);			
			FontMetrics fontMetrics = g.getFontMetrics();
			int pos = centerY + fontMetrics.getAscent()/2;			
				
            if (i<getAlphabet().size()) {
				Character letter = getAlphabet().characterAt(i);
				String threeLetter;
				try {
					Symbol symbol = ProteinTools.getAlphabet().getTokenization("token").parseToken(letter.toString());
					if (symbol instanceof AtomicSymbol) { // not ambiguous
						threeLetter = symbol.getName();
					} else {
						threeLetter = "*";
					}
					threeLetter = StringUtils.firstCharToCapital(threeLetter);
				} catch (BioException ex) {
					threeLetter="<unk>";
				}

			//int pos = offsetY + (i+1) * cellHeight - (cellHeight-(fontSize-5))/2; 
				Color chemPropColor=chemPropColor = colorMap.get(aaMap.get(letter));
				if (chemPropColor==null) {
					chemPropColor=COLOR_ELSE;
				}

				g.setColor(bundleConfig.getColorModel().getFontColor());				
				g.drawString(Character.toString(letter), DEFAULT_OFFSET_X, pos);
				g.drawString(threeLetter, DEFAULT_OFFSET_X + 30, pos);
				g.setColor(chemPropColor);
				g.fillRect(DEFAULT_OFFSET_X + 90, offsetY + i*cellHeight, 25, cellHeight);
            } else {
				g.setColor(bundleConfig.getColorModel().getFontColor());
                g.drawString(Character.toString(DEFAULT_GAP_CHAR), DEFAULT_OFFSET_X, pos);
				g.drawString("Gap", DEFAULT_OFFSET_X + 30, pos);				
            }
        }
		
		AffineTransform trans = g.getTransform();
		
		f = new Font(g.getFont().getName(), g.getFont().getStyle(), CATEGORY_TEXT_SIZE);
		g.setFont(f);
		
		// draw legend for charged
		g.setTransform((AffineTransform)trans.clone());
		g.setColor(COLOR_CHARGED);
		String text = legendMap.get(Category.CHARGED);
		g.translate(DEFAULT_OFFSET_X + 140, getTextPositionForCentering(g, text, 'Q', 'R', offsetY, cellHeight));
		g.rotate((float) Math.toRadians(-90));		
		g.drawString(text, 0,0);
		

		// draw legend for polar
		g.setTransform((AffineTransform)trans.clone());
		g.setColor(COLOR_POLAR);
		text = legendMap.get(Category.POLAR);
		g.translate(DEFAULT_OFFSET_X + 140, getTextPositionForCentering(g, text, 'A', 'Q', offsetY, cellHeight));
		g.rotate((float) Math.toRadians(-90));		
		g.drawString(text, 0,0);
		

		// draw legend for hydrophobic
		g.setTransform((AffineTransform)trans.clone());
		g.setColor(COLOR_HYDROPHOBIC);
		text = legendMap.get(Category.HYDROPHOBIC);
		g.translate(DEFAULT_OFFSET_X + 140, getTextPositionForCentering(g, text, 'U', 'A', offsetY, cellHeight));
		g.rotate((float) Math.toRadians(-90));		
		g.drawString(text, 0,0);
		
		
		g.dispose();
	}
	
	@Override
	public BufferedImage renderLegend() {
		initSize();
		BufferedImage img = createBufferedImage(legendSize.width, legendSize.height);
		Graphics2D g = createBufferedImageGraphics(img);
		renderLegend(g);

        // end draw legend
		g.dispose();
		return img;        		
	}	
	
	protected int getTextPositionForCentering(Graphics2D g, String text, char charFrom, char charTo, int offsetY, int cellHeight) {
			TextLayout tl = new TextLayout(text, g.getFont(), g.getFontRenderContext());
			int from = offsetY + alphabet.indexOf(charFrom) * cellHeight;
			int to = offsetY + alphabet.indexOf(charTo) * cellHeight;
			int textpos = (to+from)/2 + (int)tl.getBounds().getWidth()/2;
			return textpos;
	}
	
	protected int[] requestedAlphabetOrder(SequenceAlphabet alphabet) {
		int[] retval = new int[alphabet.size()];
		ArrayList<Character> newOrder = new ArrayList<>(aaMap.keySet());		
		assert(alphabet.size()>=newOrder.size());		
		int overflow = newOrder.size();
		for (int i=0;i<alphabet.size();i++) {
			int index = newOrder.indexOf(alphabet.characterAt(i));
			if (index<0) {
				index = overflow++;
			}
			retval[i] = index;
		}
		retval = GeneralUtils.order(retval);
		return retval;
	}

	@Override
	public Dimension getLegendSize() {
		return legendSize;
	}
	
}
