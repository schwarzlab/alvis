/*
 * Copyright (C) 2015 Roland Schwarz <rfs32@cam.ac.uk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package gui.sequencebundle.legend;

import com.general.utils.GeneralUtils;
import com.general.utils.StringUtils;
import de.biozentrum.bioinformatik.sequence.SequenceAlphabet;
import gui.sequencebundle.SequenceBundleConfig;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import org.biojava.bio.BioException;
import org.biojava.bio.seq.DNATools;
import org.biojava.bio.seq.NucleotideTools;
import org.biojava.bio.seq.RNATools;
import org.biojava.bio.seq.io.SymbolTokenization;
import org.biojava.bio.symbol.Alphabet;
import org.biojava.bio.symbol.AtomicSymbol;
import org.biojava.bio.symbol.FiniteAlphabet;
import org.biojava.bio.symbol.Symbol;

/**
 *
 * @author Roland Schwarz <rfs32@cam.ac.uk>
 */
public class DefaultNucleotideLegendRenderer extends AbstractLegendRenderer{
	private static final Color COLOR_ELSE = Color.LIGHT_GRAY;
	private Dimension legendSize;
	
	static final Map<Character, Color> nucleotideMap = new LinkedHashMap<>();
	static{
		nucleotideMap.put('A', Color.RED);
		nucleotideMap.put('C', Color.BLUE);
		nucleotideMap.put('G', Color.GREEN);
		nucleotideMap.put('T', Color.ORANGE);
		nucleotideMap.put('U', Color.ORANGE);
	}
	
	public DefaultNucleotideLegendRenderer() {
		this(new SequenceAlphabet(), new SequenceBundleConfig());
		initSize();
	}
	
	public DefaultNucleotideLegendRenderer(SequenceAlphabet alphabet) {
		this(alphabet, new SequenceBundleConfig());
		initSize();
	}
	
	public DefaultNucleotideLegendRenderer(SequenceAlphabet alphabet, SequenceBundleConfig bundleConfig){
		this.alphabet = new ReorderedImmutableSequenceAlphabet(requestedAlphabetOrder(alphabet), alphabet);
		this.bundleConfig = bundleConfig;
		initSize();
	}

	@Override
	public BufferedImage renderLegend() {
		initSize();
		BufferedImage img = createBufferedImage(legendSize.width, legendSize.height);
		Graphics2D g = createBufferedImageGraphics(img);
		renderLegend(g);

        // end draw legend
		g.dispose();
		return img;        		
	}

	@Override
	public void renderLegend(Graphics2D g) {
		SymbolTokenization st=null;
		try {
			if (this.alphabet.equals(SequenceAlphabet.NUCLEOTIDES)) {
				st = DNATools.getDNA().getTokenization("token");
			} else if (this.alphabet.equals(SequenceAlphabet.RNA)) {
				st = RNATools.getRNA().getTokenization("token");
			}
		} catch (BioException ex) {
			st = null;
		}
		
		initSize();
		
		g = (Graphics2D) g.create();
		setGlobalGraphicsParameters(g);
		
		// begin draw legend and horizontal lines
		Font f = new Font(g.getFont().getName(), g.getFont().getStyle(), DEFAULT_TEXT_SIZE_ALPHABET);
		g.setFont(f);

		int offsetY = bundleConfig.getOffsetY();
		int cellHeight = bundleConfig.getCellHeight();
        for (int i=0; i<=getAlphabet().size(); i++) {
			int centerY = offsetY + (i+1) * cellHeight - (cellHeight/2);			
			FontMetrics fontMetrics = g.getFontMetrics();
			int pos = centerY + fontMetrics.getAscent()/2;			
				
			
            if (i<getAlphabet().size()) {
				Character letter = getAlphabet().characterAt(i);
				String name="";
				try {
					Symbol symbol = st.parseToken(letter.toString());
					if (symbol instanceof AtomicSymbol) { // not ambiguous
						name = StringUtils.firstCharToCapital(symbol.getName());
					} else {
						if (symbol.getMatches() instanceof FiniteAlphabet) {
							FiniteAlphabet matches = (FiniteAlphabet) symbol.getMatches();
							Iterator<Symbol> it = matches.iterator();
							name = "{";
							while (it.hasNext()) {
								Symbol s = it.next();
								name = name + st.tokenizeSymbol(s).toUpperCase();
								if (it.hasNext()) {
									name += ", ";
								}
							}
							name += "}";
						}
					}
				} catch (BioException ex) {
					name="<unknown>";
				}

			//int pos = offsetY + (i+1) * cellHeight - (cellHeight-(fontSize-5))/2; 
				Color color= nucleotideMap.get(letter);
				if (color==null) {
					color=COLOR_ELSE;
				}

				g.setColor(bundleConfig.getColorModel().getFontColor());				
				g.drawString(Character.toString(letter), DEFAULT_OFFSET_X, pos);
				g.drawString(name, DEFAULT_OFFSET_X + 30, pos);
				g.setColor(color);
				g.fillRect(DEFAULT_OFFSET_X + 120, offsetY + i*cellHeight, 25, cellHeight);
            } else {
				g.setColor(bundleConfig.getColorModel().getFontColor());
                g.drawString(Character.toString(DEFAULT_GAP_CHAR), DEFAULT_OFFSET_X, pos);
				g.drawString("Gap", DEFAULT_OFFSET_X + 30, pos);				
            }
        }
		
//		AffineTransform trans = g.getTransform();
		
		// draw legend for charged
//		g.setTransform((AffineTransform)trans.clone());
//		g.setColor(COLOR_CHARGED);
//		String text = legendMap.get(Category.CHARGED);
//		g.translate(DEFAULT_OFFSET_X + 130, getTextPositionForCentering(g, text, 'Q', 'R', offsetY, cellHeight));
//		g.rotate((float) Math.toRadians(-90));		
//		g.drawString(text, 0,0);
//		
//
//		// draw legend for polar
//		g.setTransform((AffineTransform)trans.clone());
//		g.setColor(COLOR_POLAR);
//		text = legendMap.get(Category.POLAR);
//		g.translate(DEFAULT_OFFSET_X + 130, getTextPositionForCentering(g, text, 'A', 'Q', offsetY, cellHeight));
//		g.rotate((float) Math.toRadians(-90));		
//		g.drawString(text, 0,0);
//		
//
//		// draw legend for hydrophobic
//		g.setTransform((AffineTransform)trans.clone());
//		g.setColor(COLOR_HYDROPHOBIC);
//		text = legendMap.get(Category.HYDROPHOBIC);
//		g.translate(DEFAULT_OFFSET_X + 130, getTextPositionForCentering(g, text, 'U', 'A', offsetY, cellHeight));
//		g.rotate((float) Math.toRadians(-90));		
//		g.drawString(text, 0,0);
		
		
		g.dispose();

	}

	@Override
	protected void initSize() {
		legendSize = new  Dimension(DEFAULT_OFFSET_X + 150, 2 * bundleConfig.getOffsetY() + (alphabet.size()+1)*bundleConfig.getCellHeight());
	}

	@Override
	public Dimension getLegendSize() {
		return legendSize;
	}

	private int[] requestedAlphabetOrder(SequenceAlphabet alphabet) {
		int[] retval = new int[alphabet.size()];
		ArrayList<Character> newOrder = new ArrayList<>(nucleotideMap.keySet());		
		assert(alphabet.size()>=newOrder.size());		
		int overflow = newOrder.size();
		for (int i=0;i<alphabet.size();i++) {
			int index = newOrder.indexOf(alphabet.characterAt(i));
			if (index<0) {
				index = overflow++;
			}
			retval[i] = index;
		}
		retval = GeneralUtils.order(retval);
		return retval;
		
	}
	
}
