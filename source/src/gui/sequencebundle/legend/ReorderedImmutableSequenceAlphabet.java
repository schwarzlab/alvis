/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.sequencebundle.legend;

import de.biozentrum.bioinformatik.sequence.SequenceAlphabet;
import java.util.Collection;
import java.util.Iterator;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public class ReorderedImmutableSequenceAlphabet extends SequenceAlphabet{
	int[] order;
	private SequenceAlphabet originalAlphabet;
	private static final String IMMUTABLE_MSG = "Alphabet is immutable.";
	
	public ReorderedImmutableSequenceAlphabet(int[] order, SequenceAlphabet originalAlphabet) {
		this.order=order;
		this.originalAlphabet=originalAlphabet;
	}

	@Override
	public Character characterAt(int index) {
//		int translatedIndex=-1;
//		for (int i=0;i<order.length;i++) {
//			if (order[i]==index) {
//				translatedIndex=i;
//			}
//		}
//		return originalAlphabet.characterAt(translatedIndex); //To change body of generated methods, choose Tools | Templates.
		return originalAlphabet.characterAt(order[index]);
	}

	@Override
	public int indexOf(Character o) {
		int originalIndex = originalAlphabet.indexOf(o); //To change body of generated methods, choose Tools | Templates.
		int index=-1;
		for (int i=0;i<order.length;i++) {
			if (order[i]==originalIndex) {
				index = i;
			}
		}
		return index;
	}

	@Override
	public Iterator<Character> iterator() {
		return new Iterator<Character>() {
			private int currentIndex = 0;
			
			@Override
			public boolean hasNext() {
				return currentIndex < size();
			}

			@Override
			public Character next() {
				return characterAt(currentIndex++);
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException(IMMUTABLE_MSG);
			}
		};
	}

	@Override
	public Object[] toArray() {
		Character[] retval = new Character[size()];
		for (int i=0;i<size();i++) {
			retval[i]=characterAt(i);
		}
		return retval;
	}

	@Override
	public <T> T[] toArray(T[] a) {
		throw new UnsupportedOperationException("niy");
	}

	@Override
	public String toString() {
		return originalAlphabet.toString(); //To change body of generated methods, choose Tools | Templates.
	}	

	@Override
	public boolean contains(Object o) {
		return originalAlphabet.contains(o); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		return originalAlphabet.containsAll(c); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public Collection<Character> getImpl() {
		return originalAlphabet.getImpl(); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public boolean equals(Object other) {
		return originalAlphabet.equals(other);
	}

	@Override
	public boolean isEmpty() {
		return originalAlphabet.isEmpty(); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public int size() {
		return originalAlphabet.size(); //To change body of generated methods, choose Tools | Templates.
	}

	
	@Override
	public boolean add(Character e) {
		throw new UnsupportedOperationException(IMMUTABLE_MSG);
	}

	@Override
	public void addCharacter(Character o) {
		throw new UnsupportedOperationException(IMMUTABLE_MSG);
	}
	
	@Override
	public boolean addAll(Collection<? extends Character> c) {
		throw new UnsupportedOperationException(IMMUTABLE_MSG);
	}

	@Override
	public void clear() {
		throw new UnsupportedOperationException(IMMUTABLE_MSG);
	}

	@Override
	public boolean remove(Object o) {
		throw new UnsupportedOperationException(IMMUTABLE_MSG);
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		throw new UnsupportedOperationException(IMMUTABLE_MSG);
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		throw new UnsupportedOperationException(IMMUTABLE_MSG);
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		throw new CloneNotSupportedException();
	}

	/**
	 * @return the originalAlphabet
	 */
	public SequenceAlphabet getOriginalAlphabet() {
		return originalAlphabet;
	}

	
	
}
