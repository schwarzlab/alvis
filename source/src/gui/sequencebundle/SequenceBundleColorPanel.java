/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.sequencebundle;

import com.general.mvc.view.IView;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EtchedBorder;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public class SequenceBundleColorPanel extends JPanel implements IView, ActionListener{
	JPanel groupColorPanel;
	JColorChooser colorChooser;
	
	private JPanel otherColorPanel;
	SequenceBundleColorModel model;
	
	public SequenceBundleColorPanel(SequenceBundleColorModel colorModel) {
		setColorsFromColorModel(colorModel);
		init();
	}
	
	private void setColorsFromColorModel(SequenceBundleColorModel colorModel){
		this.model = new SequenceBundleColorModel(colorModel); // copy construct
	}
	
	private void init() {
		this.removeAll();
		this.setLayout(new BorderLayout());
		JPanel colorPanel = new JPanel(new GridLayout(1,2));
		
		groupColorPanel = new JPanel(new GridLayout(0,2));
		groupColorPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED),"Group colors"));
		for (Integer group:model.getGroupColors().keySet()) {
			((GridLayout)groupColorPanel.getLayout()).setRows(((GridLayout)groupColorPanel.getLayout()).getRows()+1);
			JButton cmd = new JButton("...");
			cmd.setActionCommand(String.valueOf(group));
			cmd.addActionListener(this);
			cmd.setBackground(model.getGroupColors().get(group));
			groupColorPanel.add(new JLabel("Group " + (group+1) + ":"));
			groupColorPanel.add(cmd);
		}
		colorPanel.add(groupColorPanel);
		
		JButton cmd;
		otherColorPanel = new JPanel(new GridLayout(6,2));
		otherColorPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED),"Other colors"));
		
		// selection color
		otherColorPanel.add(new JLabel("Selected thread: "));
		cmd = new JButton("...");
		cmd.setActionCommand(String.valueOf(SequenceBundleColorModel.SELECTION_COLOR));
		cmd.addActionListener(this);
		cmd.setBackground(model.getSelectionColor());
		otherColorPanel.add(cmd);

		// consensus color
		otherColorPanel.add(new JLabel("Consensus thread: "));
		cmd = new JButton("...");
		cmd.setActionCommand(String.valueOf(SequenceBundleColorModel.CONSENSUS_COLOR));
		cmd.addActionListener(this);
		cmd.setBackground(model.getConsensusColor());
		otherColorPanel.add(cmd);
		
		// background color
		otherColorPanel.add(new JLabel("Background: "));
		cmd = new JButton("...");
		cmd.addActionListener(this);
		cmd.setActionCommand(String.valueOf(SequenceBundleColorModel.BACKGROUND_COLOR));
		cmd.setBackground(model.getBackgroundColor());
		otherColorPanel.add(cmd);

		// selection marker color
		otherColorPanel.add(new JLabel("Selection marker: "));
		cmd = new JButton("...");
		cmd.addActionListener(this);
		cmd.setActionCommand(String.valueOf(SequenceBundleColorModel.SELECTION_MARKER_COLOR));
		cmd.setBackground(model.getSelectionMarkerColor());
		otherColorPanel.add(cmd);

		// selection caret color
		otherColorPanel.add(new JLabel("Selection caret: "));
		cmd = new JButton("...");
		cmd.addActionListener(this);
		cmd.setActionCommand(String.valueOf(SequenceBundleColorModel.SELECTION_CARET_COLOR));
		cmd.setBackground(model.getSelectionCaretColor());
		otherColorPanel.add(cmd);

		// font color
		otherColorPanel.add(new JLabel("Bundle text: "));
		cmd = new JButton("...");
		cmd.addActionListener(this);
		cmd.setActionCommand(String.valueOf(SequenceBundleColorModel.FONT_COLOR));
		cmd.setBackground(model.getFontColor());
		otherColorPanel.add(cmd);
		
		colorPanel.add(otherColorPanel);
		
		JPanel buttonPanel = new JPanel(new FlowLayout());
		JButton cmdDefaults = new JButton("Reset");
		cmdDefaults.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				setColorsFromColorModel(new SequenceBundleColorModel());
				init();
				revalidate();
				repaint();
			}
		});
		buttonPanel.add(cmdDefaults);
		
		this.add(colorPanel, BorderLayout.CENTER);
		this.add(buttonPanel, BorderLayout.SOUTH);
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JButton cmd = (JButton)e.getSource();
		Integer group = Integer.valueOf(e.getActionCommand());		
		String title;
		switch(group) {
			case SequenceBundleColorModel.BACKGROUND_COLOR:
				title="Choose background color:";
				break;
			case SequenceBundleColorModel.SELECTION_COLOR:
				title="Choose color for selected sequence thread:";
				break;
			case SequenceBundleColorModel.SELECTION_CARET_COLOR:
				title="Choose color for selection caret:";
				break;
			case SequenceBundleColorModel.SELECTION_MARKER_COLOR:
				title="Choose color for selection marker:";
				break;
			case SequenceBundleColorModel.FONT_COLOR:
				title="Choose color for sequence bundle text:";
				break;
			case SequenceBundleColorModel.CONSENSUS_COLOR:
				title="Choose color for consensus thread:";
				break;
			default:
				title = "Choose color for group " + group + ":";
		}
		Color selectedColor = JColorChooser.showDialog(this, title, cmd.getBackground());
		if (selectedColor!=null) {
			cmd.setBackground(selectedColor);
			switch(group) {
				case SequenceBundleColorModel.BACKGROUND_COLOR:
					model.setBackgroundColor(selectedColor);
					break;
				case SequenceBundleColorModel.SELECTION_COLOR:
					model.setSelectionColor(selectedColor);
					break;
				case SequenceBundleColorModel.SELECTION_CARET_COLOR:
					model.setSelectionCaretColor(selectedColor);
					break;
				case SequenceBundleColorModel.SELECTION_MARKER_COLOR:
					model.setSelectionMarkerColor(selectedColor);
					break;
				case SequenceBundleColorModel.FONT_COLOR:
					model.setFontColor(selectedColor);
					break;
				case SequenceBundleColorModel.CONSENSUS_COLOR:
					model.setConsensusColor(selectedColor);
					break;
				default:
					model.getGroupColors().put(group, selectedColor);
			}
		}
	}

	@Override
	public void save() {
	}

	@Override
	public boolean verify() {
		return true;
	}

	@Override
	public Component getComponent() {
		return this;
	}

	@Override
	public String getTitle() {
		return "Color options:";
	}

	@Override
	public void closing() {
		
	}

	/**
	 * @return the model
	 */
	public SequenceBundleColorModel getModel() {
		return model;
	}

}
