/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.search;

import com.general.Range;
import java.util.List;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public interface SequenceSearcher {
	public boolean isMatchingSubstring();
	public void setMatchingSubstring(boolean ms);
	public List<List<Range<Integer>>> getSequenceHits();
	public String getSearchString();
	public void setSearchString(String expression) throws Exception;
	public int[] search();
}
