/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.search;

import com.general.Range;
import com.general.utils.GeneralUtils;
import de.biozentrum.bioinformatik.sequence.Sequence;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

public class RegexpSequenceSearcher implements SequenceSearcher{
	private Pattern pattern=null;
	private final List<Sequence> sequences;
	private String regexp="";
	private final List<List<Range<Integer>>> sequenceHits; // list of lists of regions
	private boolean matchingSubstring=true;
	public final String TARGET_ALL="Name&Seq";
	public final String TARGET_NAME="Name only";
	public final String TARGET_SEQ="Seq only";
	public final String[] TARGETS={TARGET_ALL, TARGET_NAME, TARGET_SEQ};
	private String target=TARGET_ALL;

	public RegexpSequenceSearcher(Sequence[] sequences) {
		this(Arrays.asList(sequences));
	}
	public RegexpSequenceSearcher(List<Sequence> sequences){
		this.sequences = sequences;
		this.sequenceHits = new ArrayList<>(sequences.size());
	}

	private void compilePattern() throws PatternSyntaxException {
		this.pattern = Pattern.compile(regexp, Pattern.CASE_INSENSITIVE);
	}

	@Override
	public int[] search() {
		boolean[] matches = new boolean[sequences.size()];
		int i = 0;
		this.sequenceHits.clear();
		for (Sequence s:sequences) {
			Matcher matcherName = pattern.matcher(s.getName());
			Matcher matcherSequence = pattern.matcher(s.getSequence());
			boolean nameMatches=false;
			boolean sequenceMatches=false;
			if (isMatchingSubstring()) {
				if (target.equals(TARGET_ALL) || target.equals(TARGET_NAME)) {
					nameMatches = matcherName.find();
				}
				if (target.equals(TARGET_ALL) || target.equals(TARGET_SEQ)) {
					sequenceMatches = matcherSequence.find();
				}
			} else {
				if (target.equals(TARGET_ALL) || target.equals(TARGET_NAME)) {
					nameMatches = matcherName.matches();
				}
				if (target.equals(TARGET_ALL) || target.equals(TARGET_SEQ)) {
					sequenceMatches = matcherSequence.matches();
				}
			}
			matches[i] = nameMatches || sequenceMatches;
			this.getSequenceHits().add(i, new ArrayList<Range<Integer>>());				

			// now look where it matches
			if (sequenceMatches) { // can only happen if sequence was targeted
				Matcher matcherSequenceArea = pattern.matcher(s.getAlignedSequence());
				while(matcherSequenceArea.find()) {
					int start = matcherSequenceArea.start();
					int end = matcherSequenceArea.end();
					this.getSequenceHits().get(i).add(new Range(start,end));
				}
			}

			i++;
		}

		return GeneralUtils.whichTrue(matches);
	}

	/**
	 * @return the regexp
	 */
	public String getSearchString() {
		return regexp;
	}

	/**
	 * @param regexp the regexp to set
	 */
	public void setSearchString(String regexp) throws PatternSyntaxException {
		this.regexp = regexp;
		compilePattern();
	}

	/**
	 * @return the sequenceHits
	 */
	public List<List<Range<Integer>>> getSequenceHits() {
		return sequenceHits;
	}

	/**
	 * @return the matchingSubstring
	 */
	public boolean isMatchingSubstring() {
		return matchingSubstring;
	}

	/**
	 * @param matchingSubstring the matchingSubstring to set
	 */
	public void setMatchingSubstring(boolean matchingSubstring) {
		this.matchingSubstring = matchingSubstring;
	}

	/**
	 * @return the target
	 */
	public String getTarget() {
		return target;
	}

	/**
	 * @param target the target to set
	 */
	public void setTarget(String target) {
		this.target = target;
	}

}
