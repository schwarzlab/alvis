/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.search;

import com.general.Range;
import de.biozentrum.bioinformatik.sequence.Sequence;
import alvis.aql.AQLParser;
import alvis.aql.AQLSelectionVisitor;
import alvis.aql.ASTStart;
import alvis.aql.ParseException;
import alvis.aql.TokenMgrError;
import com.general.utils.GeneralUtils;
import de.biozentrum.bioinformatik.alignment.visibility.SequenceVisibilityModel;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public class AQLSequenceSearcher implements SequenceSearcher{
	List<Sequence> sequences;
	boolean matchingSubstring=true;
	private List<List<Range<Integer>>> sequenceHits;
	private String searchString;
	private ASTStart root;
	private final SequenceVisibilityModel visibilityModel;

	public AQLSequenceSearcher(List<Sequence> sequences, SequenceVisibilityModel visibilityModel) {
		this.sequences = sequences;
		this.visibilityModel = visibilityModel;
		this.sequenceHits = new ArrayList<>(sequences.size());
	}
	

	@Override
	public boolean isMatchingSubstring() {
		return this.matchingSubstring;
	}

	@Override
	public void setMatchingSubstring(boolean ms) {
		this.matchingSubstring = ms;
	}

	@Override
	public List<List<Range<Integer>>> getSequenceHits() {
		return this.sequenceHits;
	}

	@Override
	public String getSearchString() {
		return this.searchString;
	}

	@Override
	public void setSearchString(String expression) throws ParseException, TokenMgrError {
		this.searchString = expression;
		this.root=null;
		
		AQLParser parser = new AQLParser(new java.io.StringReader(this.searchString));
		root = (ASTStart) parser.Start();
		//root.dump(">");
	}

	@Override
	public int[] search() {
		if (root==null) {
			return new int[0];
		}
		this.sequenceHits.clear();
		boolean[] matches = new boolean[sequences.size()];
		
		for (int i=0;i<sequences.size();i++) {
			Sequence s = sequences.get(i);
			if (this.visibilityModel.isVisible(i, 0)) {
				AQLSelectionVisitor visitor = new AQLSelectionVisitor(matchingSubstring);
				boolean testval = (boolean) visitor.visit(root, s);
				matches[i] = testval;
				if (testval) {
					this.sequenceHits.add(visitor.getSequenceHits());				
				} else {
					this.sequenceHits.add(new ArrayList<Range<Integer>>());
				}
			} else {
				matches[i] = false;
				this.sequenceHits.add(new ArrayList<Range<Integer>>());
			}
		}
		
		// translate array of boolean into array of matching indices
		return GeneralUtils.whichTrue(matches);
	}
	
}
