/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package alvis.algorithm.comparison;

import de.biozentrum.bioinformatik.sequence.Sequence;
import java.util.Comparator;
import java.util.Map;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public class SequenceComparatorByGroup implements Comparator {
	private final Map<Sequence, Integer> sequenceGroups;
	private final Comparator sequenceComparatorByName = new SequenceComparatorByName();

	public SequenceComparatorByGroup(Map<Sequence, Integer> sequenceGroups) {
		this.sequenceGroups=sequenceGroups;
	}
	
	@Override
	public int compare(Object o1, Object o2) {
		Sequence s1 = (Sequence)o1;
		Sequence s2 = (Sequence)o2;
		int compareGroup = sequenceGroups.get(s1).compareTo(sequenceGroups.get(s2));
		if (compareGroup==0) {
			compareGroup = sequenceComparatorByName.compare(s1, s2);
		}
		return compareGroup;
	}

	@Override
	public String toString() {
		return "Group, " + sequenceComparatorByName.toString().toLowerCase();
	}
	
	
}
