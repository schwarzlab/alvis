/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package alvis.algorithm.comparison;

import de.biozentrum.bioinformatik.sequence.Sequence;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public class SequenceComparatorByIndex implements Comparator {
	private final int[] order;
	private final List<Sequence> sequenceList;
	private final String description;
	private final SequenceComparatorByName nameComparator;

	public SequenceComparatorByIndex(List<Sequence> sequenceList, int[] order, String description) {
		this.order = order;
		this.sequenceList = sequenceList;
		this.description = description;
		this.nameComparator = new SequenceComparatorByName();
	}
	
	@Override
	public int compare(Object o1, Object o2) {
		Sequence s1 = (Sequence)o1;
		Sequence s2 = (Sequence)o2;
		int index1 = sequenceList.indexOf(s1);
		int index2 = sequenceList.indexOf(s2);
		int compare = ((Integer)order[index1]).compareTo((Integer)order[index2]);
		if (compare==0){
			compare = nameComparator.compare(s1, s2);
		}
		return compare;
	}

	@Override
	public String toString() {
		return description;
	}
	
	
}
