/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package alvis.algorithm.comparison;

import de.biozentrum.bioinformatik.sequence.Sequence;
import java.util.Comparator;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public class SequenceComparatorByName implements Comparator {

	@Override
	public int compare(Object o1, Object o2) {
		Sequence s1 = (Sequence)o1;
		Sequence s2 = (Sequence)o2;
		int compareName = s1.getName().compareTo(s2.getName());
		if (compareName==0) {
			return s1.getSequence().compareTo(s2.getSequence());
		}
		return compareName;
	}

	@Override
	public String toString() {
		return "Name, actual sequence";
	}
	
}
