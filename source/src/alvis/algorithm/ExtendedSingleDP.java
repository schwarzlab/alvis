package alvis.algorithm;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.biojava.bio.BioException;
import org.biojava.bio.dp.DPMatrix;
import org.biojava.bio.dp.EmissionState;
import org.biojava.bio.dp.IllegalTransitionException;
import org.biojava.bio.dp.MarkovModel;
import org.biojava.bio.dp.ProfileHMM;
import org.biojava.bio.dp.ScoreType;
import org.biojava.bio.dp.State;
import org.biojava.bio.dp.onehead.SingleDP;
import org.biojava.bio.dp.onehead.SingleDPMatrix;
import org.biojava.bio.symbol.AlphabetIndex;
import org.biojava.bio.symbol.FiniteAlphabet;
import org.biojava.bio.symbol.IllegalAlphabetException;
import org.biojava.bio.symbol.IllegalSymbolException;
import org.biojava.bio.symbol.Symbol;
import org.biojava.bio.symbol.SymbolList;
import org.jblas.DoubleMatrix;

public class ExtendedSingleDP extends SingleDP {

	public ExtendedSingleDP(MarkovModel model) throws IllegalSymbolException,
			IllegalTransitionException, BioException {
		super(model);
	}
	
	public DPMatrix posteriorMatrix( SymbolList[] symbolList, ScoreType scoreType) throws IllegalSymbolException, IllegalAlphabetException, IllegalArgumentException {
		return this.posteriorMatrix(symbolList, this.forwardMatrix(symbolList, scoreType), this.backwardMatrix(symbolList, scoreType), scoreType);
	}
	
	public DPMatrix posteriorMatrix( SymbolList[] symbolList, DPMatrix forwardMatrix, DPMatrix backwardMatrix, ScoreType scoreType) throws IllegalSymbolException, IllegalAlphabetException, IllegalArgumentException {
	
		SingleDPMatrix matrix = new SingleDPMatrix( this, symbolList[0]);
				
		double score = forwardMatrix.getScore();
		
		for ( int i = 1; i <= symbolList[0].length(); i++) {
			double[] emmissions = this.getEmission( symbolList[0].symbolAt(i), scoreType);
			
			for ( int s = 1; s < getDotStatesIndex(); s++) {
				double forwardValue = forwardMatrix.getCell(new int[]{s,i});
				double backwardValue = backwardMatrix.getCell(new int[]{s,i});

				//forwardValue -= emmissions[s-1];
				matrix.scores[i][s] = (forwardValue + backwardValue) - score;

			}
		}
		
		return matrix;
	}

	public DoubleMatrix calculateFisherScores( SymbolList[] symbolList, DPMatrix forwardMatrix, DPMatrix backwardMatrix, ScoreType score ) {
		
		if(symbolList.length != 1) {
			throw new IllegalArgumentException("seq must be 1 long, not " + symbolList.length);
		}
		
		List<State> stList =  Arrays.asList(getStates());
		
		SymbolList sequence = symbolList[0];
		
		ProfileHMM profileHMM = (ProfileHMM)getModel();
		
		double alpha=forwardMatrix.getScore();
		FiniteAlphabet alph = (FiniteAlphabet)profileHMM.getMatch(1).getDistribution().getAlphabet();
		
		
		
		Iterator<Symbol> alphabetIterator = alph.iterator();
		
		
		DoubleMatrix matrix = new DoubleMatrix(profileHMM.columns(),alph.size());
		
		int alphPos = 0;
		
		while ( alphabetIterator.hasNext()) {
			Symbol currentSymbol = alphabetIterator.next();
			
			for (int j=1;j<=profileHMM.columns();j++) {
				double lhs=Double.NEGATIVE_INFINITY;//LogSpaceRoland.ZERO;
				double rhs=Double.NEGATIVE_INFINITY;//LogSpaceRoland.ZERO;
				
				EmissionState matchState = profileHMM.getMatch(j);
				int stateIndex = stList.indexOf(matchState);
				
				
				for (int l=1;l<=sequence.length();l++) {
					double Alpha=forwardMatrix.getCell(new int[]{stateIndex,l});//getForwardProbability(l,j,ProfileState.MATCH_STATE);
					double Beta=backwardMatrix.getCell(new int[]{stateIndex,l});//getBackwardProbability(l,j,ProfileState.MATCH_STATE);
					
					
					if (currentSymbol.equals(sequence.symbolAt(l))) {
						lhs = Math.log(Math.exp(lhs) + Math.exp(Alpha+Beta));
					}
					rhs = Math.log(Math.exp(rhs) + Math.exp(Alpha+Beta));// LogSpaceRoland.sum(rhs, Alpha+Beta);
				}
				
				
				double emissionProb=Double.NEGATIVE_INFINITY;
				 // start und endzustand sind in modelLenght mit drin, emittieren aber nicht
				try {
					emissionProb=getEmission(currentSymbol, score)[stateIndex];//Math.log(matchState.getDistribution().getWeight(currentSymbol));
				} catch (IllegalSymbolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				double p1 = -1* alpha /*- emissionProb*/ + lhs;
				double p2 = -1* alpha + rhs;
				
				matrix.put(j-1, alphPos,  (Math.exp(p1)-Math.exp(p2)));
				
				//matrix[j-1][alphPos] =);
			}
			alphPos++;
		}
		
		return matrix;
	}
	
	public DoubleMatrix matchFisherVector( SymbolList[] symbolListArray, AlphabetIndex alphabetIndex, DPMatrix forwardMatrix, DPMatrix backwardMatrix, ScoreType scoreType ) {
		ProfileHMM profileHMM = (ProfileHMM)getModel();
		List<State> states = Arrays.asList( getStates());
		
		DoubleMatrix fisherVector = new DoubleMatrix(1, profileHMM.columns() * alphabetIndex.getAlphabet().size());
		double seqScore = forwardMatrix.getScore();
		//double alpha=camaAlg.getSequenceProbability();
		for (int i=0;i<alphabetIndex.getAlphabet().size();i++) {
			for (int j=1;j<=profileHMM.columns();j++) {
				EmissionState matchState = profileHMM.getMatch(j);
				double lhs = Math.log(0);
				//double camaLhs=LogSpaceRoland.ZERO;
				double rhs = Math.log(0);
				//double camaRhs=LogSpaceRoland.ZERO;
				for (int l=1;l<=symbolListArray[0].length();l++) {
					
					int stateIndex = states.indexOf(matchState);
					double Alpha = forwardMatrix.getCell(new int[]{stateIndex,l});
					//double camaAlpha=camaAlg.getForwardProbability(l,j,ProfileState.MATCH_STATE);
					double Beta = backwardMatrix.getCell(new int[]{stateIndex,l});
					//double camaBeta=camaAlg.getBackwardProbability(l,j,ProfileState.MATCH_STATE);
					
					
					if (alphabetIndex.symbolForIndex(i).equals(symbolListArray[0].symbolAt(l))) {
						lhs = Math.log(Math.exp(lhs) + Math.exp(Alpha+Beta));
						//camaLhs = LogSpaceRoland.sum(camaLhs, camaAlpha+camaBeta);
					}
					
					
					
					rhs = Math.log(Math.exp(rhs) + Math.exp(Alpha+Beta));
					//camaRhs = LogSpaceRoland.sum(camaRhs, camaAlpha+camaBeta);
					
					
				}
				//ProfileColumn column = camaProfileHMM.getColumnAt( j );
				//ProfileState state = column.getStateByType( ProfileState.MATCH_STATE );
				
				double emmisionProb = Math.log(0);
				//double camaEmmisionProb=LogSpaceRoland.ZERO;
				//if ( state.getEmissionProbabilities() != null ) { // start und endzustand sind in modelLenght mit drin, emittieren aber nicht
					
				try {
					emmisionProb = Math.log(matchState.getDistribution().getWeight(alphabetIndex.symbolForIndex(i)));
				} catch (IllegalSymbolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
					//camaEmmisionProb=LogSpaceRoland.log(state.getEmissionProbabilities().getProbability( camaHMMAlphabet.objectAt(i)) );
				//} 

				double p1 = -1 * seqScore - emmisionProb + lhs;
				//double camaP1 = -1* alpha - camaEmmisionProb + camaLhs;
				double p2 = -1 * seqScore + rhs;
				//double camaP2 = -1* alpha + camaRhs;
				
				
				
				
				double f = Math.exp(p1)-Math.exp(p2);
				//double camaFisher = LogSpaceRoland.exp(camaP1)-LogSpaceRoland.exp(camaP2);
				fisherVector.put(((j-1) * alphabetIndex.getAlphabet().size()) + i, f);
				//System.out.println( "p1:" + f );
			}
		}
		return fisherVector;
	}
	
	public DoubleMatrix matchFisherVector2( SymbolList[] symbolListArray, AlphabetIndex alphabetIndex, DPMatrix forwardMatrix, DPMatrix backwardMatrix, ScoreType scoreType ) {
		ProfileHMM profileHMM = (ProfileHMM)getModel();
		List<State> states = Arrays.asList( getStates());
		int alphabetSize = alphabetIndex.getAlphabet().size();
		
		DoubleMatrix score = new DoubleMatrix(1, profileHMM.columns() * alphabetSize );
		double seqScore = Math.exp(forwardMatrix.getScore());
		//double alpha=camaAlg.getSequenceProbability();
		
		for (int l=1;l<=symbolListArray[0].length();l++) {
			for (int j=1;j<=profileHMM.columns();j++) {
				EmissionState matchState = profileHMM.getMatch(j);
				int stateIndex = states.indexOf(matchState);
				int i = -1;
				try {
					i = alphabetIndex.indexForSymbol(symbolListArray[0].symbolAt(l));
				} catch (IllegalSymbolException e) {
					e.printStackTrace();
				}
				double f = score.get(((j-1) * alphabetSize) + i);
				double alpha = Math.exp(forwardMatrix.getCell(new int[]{stateIndex,l}));
				double beta = Math.exp(backwardMatrix.getCell(new int[]{stateIndex,l}));
				double emissionProb = 0;
				try {
					emissionProb = matchState.getDistribution().getWeight(alphabetIndex.symbolForIndex(i));
				} catch (IllegalSymbolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				f = f + (alpha * beta / emissionProb / seqScore);
				score.put(((j-1) * alphabetSize) + i, f);
				
				for (int sigma=0;sigma<alphabetSize;sigma++) {
					double f2 = score.get((j-1) * alphabetSize + sigma);
					f2 = f2 - (alpha * beta / seqScore);
					score.put((j-1) * alphabetSize + sigma, f2);
				}
			}
		}
		
		return score;
	}
}

