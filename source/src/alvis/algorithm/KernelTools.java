/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package alvis.algorithm;

import org.jblas.DoubleMatrix;
import org.jblas.MatrixFunctions;


/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public class KernelTools {
	
	public static DoubleMatrix normalize(DoubleMatrix K) {
		DoubleMatrix diag = K.diag();
		MatrixFunctions.sqrti(diag);
		JBlasTools.apply(diag, JBlasTools.fInv);
		DoubleMatrix D = DoubleMatrix.diag(diag);
		DoubleMatrix result = D.mmul(K).mmul(D);
		return result;
	}
	
	/**
	 * computes pairwise distances from a kernel matrix
	 * RCode:
	 * M=matrix(data=diag(K),nrow=nrow(K),ncol=ncol(K))
	 * D= M-2*K+t(M)
     * if (correctForNumErrs){
     *		D[which(D<0)]=0
     * }
	 * if (takeSquareRoot) {
	 *		D = sqrt(D)
	 * } 
	 * if (toDist) {
	 *		D = as.dist(D)
	 * }
	 * @param K
	 * @return 
	 */
	public static DoubleMatrix kernelToDist(DoubleMatrix K, boolean takeSqrt) {
		DoubleMatrix tmp = K.mul(-2.0);
		tmp.addiColumnVector(K.diag());
		tmp.addiRowVector(K.diag());
		tmp.maxi(0.0);
		if (takeSqrt) {
			MatrixFunctions.sqrti(tmp);
		}
		return tmp;
	}
}
