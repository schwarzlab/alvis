/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alvis.algorithm;

import alvis.Alvis;
import alvis.AlvisDataModel;
import alvis.algorithm.ParseAlignmentTask.AlignmentParserResults;
import cama.BioJavaHelper;
import com.general.gui.progress.DefaultProgressable;
import com.general.gui.progress.Progressable;
import de.biozentrum.bioinformatik.alignment.MultipleAlignment;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.swing.SwingWorker;
import org.biojava.bio.alignment.Alignment;
import org.biojava.bio.program.sax.ClustalWAlignmentSAXParser;
import org.biojava.bio.program.sax.FastaSequenceSAXParser;
import org.biojava.bio.seq.DNATools;
import org.biojava.bio.seq.ProteinTools;
import org.biojava.bio.seq.RNATools;
import org.biojava.bio.seq.Sequence;
import org.biojava.bio.seq.io.CharacterTokenization;
import org.biojava.bio.symbol.AlphabetManager;
import org.biojava.bio.symbol.FiniteAlphabet;
import org.biojava.bio.symbol.SimpleAlphabet;
import org.biojava.bio.symbol.Symbol;
import org.xml.sax.XMLReader;

/**
 *
 * @author Roland Schwarz <rfs32@cam.ac.uk>
 */
public class ParseAlignmentTask extends SwingWorker<AlignmentParserResults, Void> {

	public DefaultProgressable getProgressable() {
		return progressable;
	}

	public class AlignmentParserResults {
		public MultipleAlignment alignment;
		public Alignment biojavaAlignment;
	}

	private final Alvis.AlignmentFormat format;
	private final AlvisDataModel.AlignmentType type;

	private final Reader inputReader;
	private final Reader customAlphabetReader;
	private FiniteAlphabet alphabet;
	private Sequence[] sequenceOrder;
	private MultipleAlignment camaAlignment;
	Alignment biojavaAlignment;
	DefaultProgressable progressable = new DefaultProgressable("Importing alignment...", true);

	public ParseAlignmentTask(File file, Alvis.AlignmentFormat format, AlvisDataModel.AlignmentType type, File customAlphabetFile) throws FileNotFoundException {
		this.inputReader = new FileReader(file);
		if (customAlphabetFile!=null){
			this.customAlphabetReader = new FileReader(customAlphabetFile);
		} else {
			this.customAlphabetReader = null;
		}
		this.format = format;
		this.type=type;
	}
	
	public ParseAlignmentTask( Reader input, Alvis.AlignmentFormat format, AlvisDataModel.AlignmentType type, Reader customAlphabetReader) {	
		this.format = format;
		this.type = type;
		this.inputReader=input;
		this.customAlphabetReader = customAlphabetReader;
	}
	
	public AlignmentParserResults parse() throws Exception {
		return doInBackground();
	}
	
	@Override
	protected AlignmentParserResults doInBackground() throws Exception {
		progressable.setState(Progressable.STATE_RUNNING);
		switch (this.type) {
			case RNA: {
				alphabet = RNATools.getRNA();
			}
			break;
			case DNA: {
				alphabet = DNATools.getDNA();
			}
			break;
			case AminoAcid: {
				alphabet = ProteinTools.getAlphabet();
			}
			break;
			case Custom: {
				if (customAlphabetReader != null) {
					BufferedReader breader = new BufferedReader(customAlphabetReader);
					String line = breader.readLine();
					Map<Character, Symbol> map = new HashMap<>();
					Set<Symbol> symbols = new HashSet<>();
					Symbol gap = AlphabetManager.getGapSymbol();
					map.put(AlvisDataModel.GAP_CHAR, gap);

					while (line != null) {
						if (line.length() > 0) {
							String s = line.substring(0, 1);
							String s2 = null;
							if (line.length() > 2) {
								s2 = line.substring(2);
							}
							Symbol sym;
							if (s2 != null) {
								sym = AlphabetManager.createSymbol(s2);
							} else {
								sym = AlphabetManager.createSymbol(s);
							}
							map.put(s.charAt(0), sym);
							symbols.add(sym);
						}
						line = breader.readLine();
					}
					alphabet = new SimpleAlphabet(symbols);
					CharacterTokenization tokenization = new CharacterTokenization(alphabet, true);
					((SimpleAlphabet) alphabet).putTokenization("token", tokenization);
					for (Map.Entry<Character, Symbol> entry : map.entrySet()) {
						tokenization.bindSymbol(entry.getValue(), entry.getKey());
					}
				} else {
					throw new CustomAlphabetAbortedException();
				}
			}
			break;

			default:
				break;
		}

		XMLReader parser = null;

		switch (format) {
			case FASTA:
				parser = new FastaSequenceSAXParser();
				break;
			case CLUSTAL:
				parser = new ClustalWAlignmentSAXParser();
				break;
			default:
				break;
		}

		ParseBiojavaAlignmentTask reader = new ParseBiojavaAlignmentTask(this.inputReader, alphabet, parser);
		Alignment bjAlignment = reader.readAlignment();
		this.sequenceOrder = reader.getSequenceOrder();

		String[] labelOrder = new String[sequenceOrder.length];
		for (int i = 0; i < sequenceOrder.length; i++) {
			labelOrder[i] = sequenceOrder[i].getName();
		}

		AlignmentParserResults results = new AlignmentParserResults();
//		results.alignment = BioJavaHelper.convertAlignment(bjAlignment, labelOrder, true);
		results.alignment = BioJavaHelper.convertAlignment(bjAlignment, labelOrder, true);
		results.biojavaAlignment = bjAlignment;
		
		return results;
	}

	public Sequence[] getSequencOrder() {
		return sequenceOrder;
	}

	public MultipleAlignment getMultipleAlignment() {
		return this.camaAlignment;
	}

	@Override
	public void done() {
		progressable.setState(Progressable.STATE_IDLE);
	}

	/**
	 * @return the biojavaAlignment
	 */
	public Alignment getBiojavaAlignment() {
		return biojavaAlignment;
	}


	public class CustomAlphabetAbortedException extends Exception {
	}
}
