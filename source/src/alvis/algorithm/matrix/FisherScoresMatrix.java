/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package alvis.algorithm.matrix;

import java.io.Serializable;
import org.biojava.bio.symbol.FiniteAlphabet;
import org.biojava.bio.symbol.Symbol;
import org.jblas.DoubleMatrix;

/**
 *
 * @author Roland Schwarz <rfs32@cam.ac.uk>
 */
public class FisherScoresMatrix extends DoubleMatrix implements Serializable {
	Symbol[] columnHeader;
	FiniteAlphabet alphabet;
	private static final long serialVersionUID = 1l;

	public FisherScoresMatrix(int rows, int columns, Symbol[] columnHeader, FiniteAlphabet alphabet) {
		super(rows, columns);
		this.columnHeader = columnHeader;
		this.alphabet = alphabet;
	}

	public FisherScoresMatrix(DoubleMatrix matrix, Symbol[] columnHeader, FiniteAlphabet alphabet) {
		super(matrix.toArray2());
		this.columnHeader = columnHeader;
		this.alphabet = alphabet;
	}

	public FisherScoresMatrix(FisherScoresMatrix matrix) {
		this(matrix, matrix.getColumnHeader(), matrix.getAlphabet());
	}

	public FiniteAlphabet getAlphabet() {
		return this.alphabet;
	}

	/**
	 * @param index
	 * @return the columnHeader
	 */
	public Symbol getSymbolForColumn(int index) {
		return columnHeader[index];
	}

	public Symbol[] getColumnHeader() {
		return columnHeader;
	}
	
	public int columnToHMMPosition(int index) {
		return index / alphabet.size();
	}
	
	public int hmmPositionToColumn(int index, Symbol sym) {
		int from = index * alphabet.size();
		int to = (index+1) * alphabet.size();
		if (from<0 || to>columnHeader.length) {
			return -1;
		}
		
		for (int i=from;i<to;i++) {
			if (columnHeader[i].equals(sym)) {
				return i;
			}
		}
		
		return -1;
	}
}
