/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package alvis.algorithm.matrix;

import com.general.containerModel.Nameable;
import java.io.File;
import java.io.IOException;
import java.util.NoSuchElementException;
import org.biojava.bio.BioException;
import org.biojava.bio.alignment.SubstitutionMatrix;
import org.biojava.bio.symbol.FiniteAlphabet;

/**
 *
 * @author Roland Schwarz <rfs32@cam.ac.uk>
 */
public class NameableSubstitutionMatrix extends SubstitutionMatrix implements Nameable {

	public NameableSubstitutionMatrix(FiniteAlphabet alpha, File matrixFile) throws BioException, NumberFormatException, IOException {
		super(alpha, matrixFile);
	}

	public NameableSubstitutionMatrix(FiniteAlphabet alpha, String matrixString, String name) throws BioException, NumberFormatException, IOException {
		super(alpha, matrixString, name);
	}

	public NameableSubstitutionMatrix(FiniteAlphabet alpha, short match, short replace) {
		super(alpha, match, replace);
	}

	public NameableSubstitutionMatrix(File file) throws NumberFormatException, NoSuchElementException, BioException, IOException {
		super(file);
	}
	
}
