/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package alvis.algorithm;

import alvis.algorithm.prank.soap.InputParameters;
import alvis.algorithm.prank.soap.JDispatcherService;
import alvis.algorithm.prank.soap.JDispatcherService_Service;
import alvis.algorithm.prank.soap.ObjectFactory;
import alvis.algorithm.prank.soap.WsParameterDetails;
import alvis.algorithm.prank.soap.WsParameters;
import alvis.algorithm.prank.soap.WsRawOutputParameters;
import alvis.algorithm.prank.soap.WsResultTypes;
import com.general.gui.progress.DefaultProgressable;
import com.general.gui.progress.Progressable;
import de.biozentrum.bioinformatik.sequence.Sequence;
import javax.swing.SwingWorker;

/**
 *
 * @author Roland Schwarz <rfs32@cam.ac.uk>
 */
public class AlignSequencesTask extends SwingWorker<String, Void>{
	private final Iterable<Sequence> sequences;
	private final DefaultProgressable progressable;
	
	public AlignSequencesTask(Iterable<Sequence> sequences){
		this.sequences = sequences;
		this.progressable = new DefaultProgressable(this, "Aligning sequences...", true);
	}
	
	@Override
	public String doInBackground() throws Exception{
		this.progressable.setState(Progressable.STATE_RUNNING);
		ObjectFactory of = new ObjectFactory();		
		String fasta = sequencesToFASTA(sequences);
		InputParameters input = of.createInputParameters();
		
		input.setSequence(of.createInputParametersSequence(fasta));
		input.setOutputFormat(of.createInputParametersOutputFormat("8")); // fasta
		input.setUseLogSpace(of.createInputParametersUseLogSpace(Boolean.TRUE));
		WsRawOutputParameters out = of.createWsRawOutputParameters();
		
		String jobid = run("rfs@ebi.ac.uk", "test2", input);
		progressable.setStatusDescription(jobid);
		while (!getStatus(jobid).equals("FINISHED")){
			Thread.sleep(500);
		}
//		WsResultTypes resultTypes = getResultTypes(jobid);
		byte[] result = getResult(jobid, "aln-fasta", out);
		String resultString=null;
		if (result!=null) {
			resultString = new String(result, "UTF-8");
		}
		return resultString;
	}

	@Override
	protected void done() {
		this.progressable.setState(Progressable.STATE_IDLE);
	}

	 
	
	private String sequencesToFASTA(Iterable<Sequence> sequences) {
		StringBuilder fasta = new StringBuilder();
		for (Sequence s:sequences){
			fasta.append(">");
			fasta.append(s.getName());
			fasta.append("\n");
			fasta.append(s.getSequence());
			fasta.append("\n");
		}
		return fasta.toString();
	}
	
	private static String run(String email, String title, InputParameters parameters) {
		JDispatcherService_Service service = new JDispatcherService_Service();
		JDispatcherService port = service.getJDispatcherServiceHttpPort();
		return port.run(email, title, parameters);
	}

	private static String getStatus(String jobId) {
		JDispatcherService_Service service = new JDispatcherService_Service();
		JDispatcherService port = service.getJDispatcherServiceHttpPort();
		return port.getStatus(jobId);
	}

	private static WsResultTypes getResultTypes(java.lang.String jobId) {
		JDispatcherService_Service service = new JDispatcherService_Service();
		JDispatcherService port = service.getJDispatcherServiceHttpPort();
		return port.getResultTypes(jobId);
	}

	private static byte[] getResult(String jobId, String type, WsRawOutputParameters parameters) {
		JDispatcherService_Service service = new JDispatcherService_Service();
		JDispatcherService port = service.getJDispatcherServiceHttpPort();
		return port.getResult(jobId, type, parameters);
	}

	private static WsParameters getParameters() {
		JDispatcherService_Service service = new JDispatcherService_Service();
		JDispatcherService port = service.getJDispatcherServiceHttpPort();
		return port.getParameters();
	}

	private static WsParameterDetails getParameterDetails(java.lang.String parameterId) {
		JDispatcherService_Service service = new JDispatcherService_Service();
		JDispatcherService port = service.getJDispatcherServiceHttpPort();
		return port.getParameterDetails(parameterId);
	}

	/**
	 * @return the progressable
	 */
	public DefaultProgressable getProgressable() {
		return progressable;
	}


	
}
