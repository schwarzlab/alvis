package alvis.algorithm;

import org.biojava.bio.dist.Distribution;
import org.biojava.bio.dist.DistributionFactory;
import org.biojava.bio.dist.DistributionTools;
import org.biojava.bio.dp.DP;
import org.biojava.bio.dp.DPFactory;
import org.biojava.bio.dp.ProfileHMM;
import org.biojava.bio.dp.ScoreType;
import org.biojava.bio.dp.StatePath;
import org.biojava.bio.seq.DNATools;
import org.biojava.bio.seq.Sequence;
import org.biojava.bio.seq.SequenceIterator;
import org.biojava.bio.seq.db.SequenceDB;
import org.biojava.bio.seq.io.SymbolTokenization;
import org.biojava.bio.alignment.Alignment;
import org.biojava.bio.symbol.Alphabet;
import org.biojava.bio.symbol.SymbolList;


/**
 * 
 * @author Eva Segeritz
 * A class for Training a Profile HMM with the given alignment.
 */
public class SeqEngine {

	private Alignment       		baseAlignment	= null;
	private Alphabet     			baseAlphabet	= null;
	private SequenceDB				baseDB 			= null;
	private ProfileHMM				baseProfile		= null;
	private DP						baseDP			= null;
	private int						matchColumns	= 0;
	private double					matchThreshold  = 0.5;

	/**
	 * 
	 * @param verbosity the higher, the more output is generated
	 * @param alignmentFile contains the alignment used for training the profileHMM
	 * @param name the name of the Model
	 * @param matchThreshold the threshold which is used for detecting model-columns within the alingment
	 * @throws Exception
	 */
	public SeqEngine(Alignment alignment, SequenceDB db, double matchThreshold) throws Exception {
		this.matchThreshold = matchThreshold;
		this.baseAlignment = alignment;
		this.baseDB = db;
		
		/* Create Alphabet */
		baseAlphabet 		= baseAlignment.getAlphabet();

		
		/* Read alignment file into Sequence database and create Alignment*/
		this.matchColumns 	= getMatchColumns(baseAlignment, matchThreshold);


		/* Create and configure ProfileHMM */
		baseProfile 		= createProfile(baseAlphabet, matchColumns);
		configureProfile(baseProfile, baseDB, baseAlignment);
//		BioHelper.printProfileHMM(baseProfile);

		/* Create dynamic programming object DP */
		//println(2, "Creating dynamic programming object from ProfileHMM [" + baseProfile.toString() + "]");
		baseDP 				= DPFactory.DEFAULT.createDP(baseProfile);
//		BioHelper.printDP(baseDP);

		/* done. */
	}

	
	/**
	 * Successively aligns the sequences from the initial base database to the corresponding model.
	 * This might be useful for checking the individual quality of each sequence and its conformity to the other sequences or to the model, respectively. 
	 */
	
	public void alignBaseSequences() throws Exception {
		
		SequenceIterator it = baseDB.sequenceIterator();
		while (it.hasNext()) {
			Sequence 			s 			= it.nextSequence();
			
			alignSequence(s);
		}
	}

	public void alignSequence( SymbolList s) throws Exception {
		

			SymbolList[] 		sl 			= {s};
			StatePath 			sp 			= baseDP.viterbi(sl, ScoreType.ODDS);
			double 				fwScore 	= baseDP.forward(sl,  ScoreType.ODDS);
			double 				bwScore 	= baseDP.backward(sl, ScoreType.ODDS);

//			String				test 		= "aaaaa";
			String 				seqString 	= "";
			String 				stateString = "";   
			
			Alphabet alphabet = s.getAlphabet();
			SymbolTokenization 	token 		= alphabet.getTokenization("token");
			
			for (int i = 1 ; i <= sp.length() ; i++) {
				seqString 	+= token.tokenizeSymbol(sp.symbolAt((String) StatePath.SEQUENCE, i));
				stateString += sp.symbolAt((String) StatePath.STATES, i).getName().charAt(0);
			}
			System.out.println( "Alignment-Sequence [" + seqString + "] [" + stateString + "] alias [" + "name" + "]: viterbi[" + sp.getScore() + "] fw[" + fwScore + "] bw[" + bwScore + "]");

	}
	/**
	 * Aligns a testsequence to a model
	 * @throws Exception
	 */
	public void alignTestSequence() throws Exception {
	
		String 				test 		= "ag-ag-a";
		Sequence 			testSeq 	= DNATools.createDNASequence(test, "testSeq");
		SymbolList [] 		sl 			= {testSeq};
		StatePath			sp 			= baseDP.viterbi(sl, ScoreType.PROBABILITY); 
		String 				seqString 	= "";
		String 				stateString = "";   
		SymbolTokenization 	token 		= DNATools.getDNA().getTokenization("token");

		for (int i = 1 ; i <= sp.length() ; i++) {
			seqString 	+= token.tokenizeSymbol(sp.symbolAt((String) StatePath.SEQUENCE, i));
			stateString += sp.symbolAt((String) StatePath.STATES, i).getName().charAt(0);
		}
		//printl "Test-Sequence      [" + seqString + "] [" + stateString + "] alias [" + testSeq.getName() + "]: viterbi[" + sp.getScore() + "] fw[" + "" + "] bw[" + "" + "]");

	
	}
	
	/**
	 * Configures the model with the data from the given alignment an the correspondign daatabase
	 * @param p the ProfileHMM to be configured
	 * @param db the sequence database
	 * @param al the alignment used for training
	 * @throws Exception
	 */
	private void configureProfile(ProfileHMM p, SequenceDB db, Alignment al) throws Exception {
		//println(1, "Configuring ProfileHMM [" + p.toString() + "] rom DB [" + db.getName() + "] and the corresponding alignment.");
		calcTransitionWeight(p, baseAlignment, baseDB, 1);
		setEmissionWeight(p, baseAlignment);
	}
	

	
	
	/** Creates a {@link ProfileHMM} from the give database and alignment. 
	 * 
	 * @param alpha the alphabet for the emission symbols
	 * @param cols the amount of model-columns within the ProfileHMM
	 * @param name the name of the model
	 * @throws Exception
	 */
	private ProfileHMM createProfile(Alphabet alpha, int cols) throws Exception {
		ProfileHMM profile = new ProfileHMM(alpha, cols, DistributionFactory.DEFAULT, DistributionFactory.DEFAULT, null);
		return profile;
	}


	/**
	 *  Calc Columns which are seen as profile columns
	 */
	private int getMatchColumns(Alignment alignment, double threshold) throws Exception{
		int 			matchCols 	= 0;
		double 			colWeight 	= 0.0;
		Distribution[] 	dis 		= DistributionTools.distOverAlignment(alignment, true, 1.0);
		
		for (int i = 0; i < dis.length; i++){
			colWeight = dis[i].getWeight(DNATools.n());
			if (colWeight >= threshold) matchCols++;
		}
		return matchCols;
	}
	/**
	 * Checks if an alingment column is a model column
	 */
	private boolean isMatchColumn(Alignment alignment, int alignmentCol, double threshold) throws Exception {
		Distribution[] 	dis 		= DistributionTools.distOverAlignment(alignment, true, 1.0);
		double 			colWeight 	= dis[alignmentCol].getWeight(DNATools.n());
		if (colWeight >= threshold) {
			return true;
		}
		return false;
	}

	/**
	 * Checks if an alignment column is a non-model (insert) column
	 */
	/*@SuppressWarnings("unused")
	private boolean isInsertColumn(Alignment alignment, int alignmentCol, double threshold) throws Exception {
		return !isMatchColumn(alignment, alignmentCol, threshold);
	}
	
	
	@SuppressWarnings("unused")
	private int countGaps(Alignment alignment, int alignmentCol){
		if (alignmentCol <= 0 || alignmentCol >= alignment.length()) return 0;
		int gaps = 0;
		
		SymbolListIterator 	si 		= new Alignment.SymbolListIterator(alignment);
		SimpleSymbolList 	sym		= null;
		
		while(si.hasNext()){
			sym = (SimpleSymbolList) si.next();
			if (sym.seqString().charAt(alignmentCol-1) == '-') gaps++;
			//println(10, "----- Symbol at col [ "+(col - 1)+" ]: [ "+sym.seqString().charAt(col-1)+" ]" );
		}
		
		return gaps;
	}
	
	@SuppressWarnings("unused")
	private int countChars(Alignment alignment, int alignmentCol){
		if (alignmentCol <= 0 || alignmentCol >= alignment.length()) return 0;
		int chars = 0;

		SymbolListIterator 	si 		= new Alignment.SymbolListIterator(alignment);
		SimpleSymbolList 	sym		= null;
		
		while(si.hasNext()){
			sym = (SimpleSymbolList) si.next();
			
			
			
			if (sym.symbolAt(alignmentCol-1) != alignment.getAlphabet().getGapSymbol()) chars++;
			//println(10, "----- Symbol at col [ "+(alignmentCol - 1)+" ]: [ "+sym.seqString().charAt(alignmentCol-1)+" ]" );
		}
		return chars;
	}
	

	@SuppressWarnings("unused")
	private int isInsertState(ProfileHMM p, State s) {
		for (int i = 0; i <= p.columns(); i++){
			if (p.getInsert(i) == s) return i;
		}
		return -1;
	}
	

	@SuppressWarnings("unused")
	private int isMatchState(ProfileHMM p, State s){
		for (int i = 0; i <= p.columns(); i++){
			if (p.getMatch(i) == s) return i;
		}
		return -1;
	}

@SuppressWarnings("unused")
	private int isDeleteState(ProfileHMM p, State s){
		for (int i = 1; i <= p.columns(); i++){
			if (p.getDelete(i) == s) return i;
		}
		return -1;
	}*/
	
	/**
	 * Sets the emission probabilites calculated from an alignmetn to the model
	 * @param p the ProfileHMM
	 * @param alignment the alignment
	 * @throws Exception
	 */
	private void setEmissionWeight(ProfileHMM p, Alignment alignment) throws Exception {
		int pCol = 0;	
		Distribution[] d = DistributionTools.distOverAlignment(alignment, true, 1.0);
		for (int i = 0; i < d.length; i++){						// iterate over alignment cols
			if (isMatchColumn(alignment, i, this.matchThreshold)) {
				pCol++; 
				p.getMatch(pCol).setDistribution(d[i]);
				p.getInsert(pCol).setDistribution(d[i]);

				/*println(10, "Setting pCol " + pCol + " from aCol " + i);
				println(10, "Match-Emissions-WS von a an Pos "+pCol+": "+p.getMatch(pCol).getDistribution().getWeight(DNATools.a()));
				println(10, "Match-Emissions-WS von c an Pos "+pCol+": "+p.getMatch(pCol).getDistribution().getWeight(DNATools.c()));
				println(10, "Match-Emissions-WS von g an Pos "+pCol+": "+p.getMatch(pCol).getDistribution().getWeight(DNATools.g()));
				println(10, "Match-Emissions-WS von t an Pos "+pCol+": "+p.getMatch(pCol).getDistribution().getWeight(DNATools.t()));
				println(10, "--");
				println(10, "Insert-Emissions-WS von a an Pos "+pCol+": "+p.getInsert(pCol).getDistribution().getWeight(DNATools.a()));
				println(10, "Insert-Emissions-WS von c an Pos "+pCol+": "+p.getInsert(pCol).getDistribution().getWeight(DNATools.c()));
				println(10, "Insert-Emissions-WS von g an Pos "+pCol+": "+p.getInsert(pCol).getDistribution().getWeight(DNATools.g()));
				println(10, "Insert-Emissions-WS von t an Pos "+pCol+": "+p.getInsert(pCol).getDistribution().getWeight(DNATools.t()));
				println(10, "");*/
				
			}
		}
		
	}
	
	
	/**
	 *  Returns the alignment column from a given ProfileHMM column or -1 if no corresponding column was found. */
	private int getAColFromPCol(ProfileHMM p, Alignment a, int pCol, double threshold) throws Exception {

		Distribution[] 	dis = DistributionTools.distOverAlignment(a, true, 0.0);
		if (pCol <= 0 || pCol > p.columns()) return -1;
		for (int i = 0 ; i < dis.length ; i++) {
			if (isMatchColumn(a, i, threshold)) pCol--;
			if (pCol == 0) return i;
		}
		return -1;
	}

	/**
	 * Calculates the transition probabilties from a givan alignment and a sequence db
	 * pseudoCounts can be also used
	 * @param p the ProfileHMM
	 * @param a the alignment
	 * @param db the sequence db
	 * @param pseudoCnt the additional pseudoCounts
	 * @throws Exception
	 */
	private void calcTransitionWeight(ProfileHMM p, Alignment a, SequenceDB db, int pseudoCnt) throws Exception {
		boolean insertBlock = false;
		for (int pCol = 0 ; pCol <= p.columns() ; pCol++) {			// iterate over all ProfileHPP Columns including start-column 0

			//println(10, "Configuring profileHMM column " + pCol);
			
			/* die beiden fuer pCol relevanten aCols ermitteln */
			int aCol1 = getAColFromPCol(p, a, pCol, this.matchThreshold);
			int aCol2 = getAColFromPCol(p, a, pCol+1, this.matchThreshold);
			//println(10, " using aCol " + aCol1 + " and aCol " + aCol2);
			if (aCol1 == aCol2) throw new Exception("Invalid column numbers!");
			insertBlock = (aCol2 - aCol1 > 1); 
			//if (insertBlock) println(10, " insert block found!");

						
			// init Transition-Probs with Pseudo-Counts
			int M2M = pseudoCnt, 
				M2I = pseudoCnt, 
				M2D = pseudoCnt, 
				I2M = pseudoCnt, 
				I2I = pseudoCnt, 
				I2D = pseudoCnt, 
				D2M = pseudoCnt, 
				D2I = pseudoCnt, 
				D2D = pseudoCnt;
			
			SequenceIterator it = db.sequenceIterator();
			while (it.hasNext()){
				Sequence s = it.nextSequence();
				
				if (aCol1 == -1) {			// we are in the ProfileHMM's start column 0
					/* Magical->M */
					if (isSymbol(s, aCol2) && !SymbolBetween(s, aCol1, aCol2)) 							M2M++;
					/* Magical->D */
					if (!isSymbol(s, aCol2) && !SymbolBetween(s, aCol1, aCol2))							M2D++;
					/* Magical->I */
					if (SymbolBetween(s, aCol1, aCol2))													M2I++;
					
					/* I->M */
					if (SymbolBetween(s, aCol1, aCol2) && isSymbol(s, aCol2))							I2M++;
					/* I->D */
					if (SymbolBetween(s, aCol1, aCol2) && !isSymbol(s, aCol2))							I2D++;
					/* I->I */
					if (countSymbolsBetween(s, aCol1, aCol2) > 1 ) I2I += countSymbolsBetween(s, aCol1, aCol2) - 1;
					
					/* in column 0 does delete state not exist */
					//M2D = 0;
					D2M = 0;
					D2D = 0;
					D2I = 0;
					
					
				} else if (aCol2 == -1) {	// we are in the ProfileHMM's end column 
					/* M->Magical */
					if (isSymbol(s, aCol1) && !SymbolBetween(s, aCol1, aCol2))							M2M++;
					/* D->Magical */
					if (!isSymbol(s, aCol1) && !SymbolBetween(s, aCol1, aCol2))							D2M++;
					/* I->Magical */
					if (countSymbolsBetween(s, aCol1, aCol2) > 1) I2M += countSymbolsBetween(s, aCol1, aCol2) - 1;
					
				} else {					// we are somewhere inbetween
					/* M->M */
					if (isSymbol(s, aCol1) && isSymbol(s, aCol2) && !SymbolBetween(s, aCol1, aCol2)) 	M2M++;
					/* M->D*/ 
					if (isSymbol(s, aCol1) && !isSymbol(s, aCol2) && !SymbolBetween(s, aCol1, aCol2)) 	M2D++;
					/* M->I */
					if (isSymbol(s, aCol1) && SymbolBetween(s, aCol1, aCol2)) 							M2I++;
					
					
					/* D->M */
					if (!isSymbol(s, aCol1) && isSymbol(s, aCol2) && !SymbolBetween(s, aCol1, aCol2))	D2M++;
					/* D->D */
					if (!isSymbol(s, aCol1) && !isSymbol(s, aCol2) && !SymbolBetween (s, aCol1, aCol2))	D2D++;
					/* D->I */
					if (!isSymbol(s, aCol1) && SymbolBetween (s, aCol1, aCol2))							D2I++;
					
					
					/* I->M */
					if(SymbolBetween(s, aCol1, aCol2) && isSymbol(s, aCol2))				 			I2M++;
					/* I->D */ 
					if (SymbolBetween(s, aCol1, aCol2) && !isSymbol(s, aCol2))							I2D++;
					/* I->I */ 
					if ( countSymbolsBetween(s, aCol1, aCol2) > 1 )	I2I += countSymbolsBetween(s, aCol1, aCol2) - 1;
					
					
				}
			}
			
			
			/* add all counts of a col*/
			int matchSum 	= M2M + M2D + M2I;
			int insertSum 	= I2M + I2D + I2I;
			int deleteSum 	= D2M + D2D + D2I;

			double M2M_Prob = (double) M2M / matchSum;
			double M2D_Prob = (double) M2D / matchSum;
			double M2I_Prob = (double) M2I / matchSum;
			
			double D2M_Prob = (double) D2M / deleteSum;
			double D2D_Prob = (double) D2D / deleteSum;
			double D2I_Prob = (double) D2I / deleteSum;
			
			double I2M_Prob = (double) I2M / insertSum;
			double I2D_Prob = (double) I2D / insertSum;
			double I2I_Prob = (double) I2I / insertSum;
			
			/*println(10, "M2M: "+M2M);
			println(10, "M2D: "+M2D);
			println(10, "M2I: "+M2I);
			println(10, "match-Sum: "+matchSum);
			println(10, "M2M_Prob: "+M2M_Prob);
			println(10, "M2D_Prob: "+M2D_Prob);
			println(10, "M2I_Prob: "+M2I_Prob);
			println(10, "");
			println(10, "D2M: "+D2M);
			println(10, "D2D: "+D2D);
			println(10, "D2I: "+D2I);
			println(10, "delete-Sum: "+deleteSum);
			println(10, "D2M_Prob: "+D2M_Prob);
			println(10, "D2D_Prob: "+D2D_Prob);
			println(10, "D2I_Prob: "+D2I_Prob);
			println(10, "");
			println(10, "I2M: "+I2M);
			println(10, "I2D: "+I2D);
			println(10, "I2I: "+I2I);
			println(10, "insert-Sum: "+insertSum);
			println(10, "I2M_Prob: "+I2M_Prob);
			println(10, "I2D_Prob: "+I2D_Prob);
			println(10, "I2I_Prob: "+I2I_Prob);
			println(10, "");*/
			
			/* Set the Transition-Probs */
			/* Magical ->x */
			if (pCol == 0){
				Distribution m_di = p.getWeights(p.getMatch(pCol));
				Distribution i_di = p.getWeights(p.getInsert(pCol));

				m_di.setWeight(p.getMatch(pCol+1),  M2M_Prob);  
				m_di.setWeight(p.getDelete(pCol+1), M2D_Prob);  
				m_di.setWeight(p.getInsert(pCol),  	M2I_Prob);
				
				i_di.setWeight(p.getMatch(pCol+1),  I2M_Prob);
				i_di.setWeight(p.getDelete(pCol+1), I2D_Prob);
				i_di.setWeight(p.getInsert(pCol),   I2I_Prob);
				
				/*println(10, "Magical->  Match  Prob  :"+m_di.getWeight(p.getMatch(pCol+1)));
				println(10, "Magical->  Delete Prob  :"+m_di.getWeight(p.getDelete(pCol+1)));
				println(10, "Magical->  Insert Prob  :"+m_di.getWeight(p.getInsert(pCol)));
				println(10, "");
				println(10, "Insert-0-> Match-1 Prob :"+i_di.getWeight(p.getMatch(pCol+1)));
				println(10, "Insert-0-> Delete-0 Prob:"+i_di.getWeight(p.getDelete(pCol+1)));
				println(10, "Insert-0-> Insert-0 Prob:"+i_di.getWeight(p.getInsert(pCol)));*/
				
			}
			/* x->Magical */
			else if (pCol >= p.columns()){

				matchSum 	= M2M + M2I;
				insertSum 	= I2M + I2I;
				deleteSum 	= D2M + D2I;

				M2M_Prob = (double) M2M / matchSum;
				M2I_Prob = (double) M2I / matchSum;
				
				D2M_Prob = (double) D2M / deleteSum;
				D2I_Prob = (double) D2I / deleteSum;
				
				I2M_Prob = (double) I2M / insertSum;
				I2I_Prob = (double) I2I / insertSum;


				Distribution m_di = p.getWeights(p.getMatch(pCol));
				Distribution i_di = p.getWeights(p.getInsert(pCol));
				Distribution d_di = p.getWeights(p.getDelete(pCol)); 
				
				m_di.setWeight(p.getMatch(pCol+1),  M2M_Prob); 
				m_di.setWeight(p.getInsert(pCol),   M2I_Prob);
				
				d_di.setWeight(p.getMatch(pCol+1), 	D2M_Prob);
				d_di.setWeight(p.getInsert(pCol),   D2I_Prob);
				
				i_di.setWeight(p.getMatch(pCol+1), 	I2M_Prob);
				i_di.setWeight(p.getInsert(pCol),   I2I_Prob);
				
				/*println(10, "Match->  Magical Prob :"+m_di.getWeight(p.getMatch(pCol+1)));
				println(10, "Delete-> Magical Prob :"+d_di.getWeight(p.getMatch(pCol+1)));
				println(10, "Insert-> Magical Prob :"+i_di.getWeight(p.getMatch(pCol+1)));
				println(10, "");
				println(10, "Match->  Insert Prob :"+m_di.getWeight(p.getInsert(pCol)));
				println(10, "Delete-> Insert Prob :"+d_di.getWeight(p.getInsert(pCol)));
				println(10, "Insert-> Insert Prob :"+i_di.getWeight(p.getInsert(pCol)));*/

			}
			/* all other states */
			else{
				Distribution m_di = p.getWeights(p.getMatch(pCol));
				Distribution i_di = p.getWeights(p.getInsert(pCol));
				Distribution d_di = p.getWeights(p.getDelete(pCol)); 
				
				m_di.setWeight(p.getMatch(pCol+1),  M2M_Prob);  // mit welcher WS von match(j) nach match(j+1) ???
				m_di.setWeight(p.getDelete(pCol+1), M2D_Prob);
				m_di.setWeight(p.getInsert(pCol),   M2I_Prob);
				
				i_di.setWeight(p.getMatch(pCol+1),  I2M_Prob);
				i_di.setWeight(p.getDelete(pCol+1), I2D_Prob);
				i_di.setWeight(p.getInsert(pCol),   I2I_Prob);
				
				d_di.setWeight(p.getMatch(pCol+1),  D2M_Prob);
				d_di.setWeight(p.getDelete(pCol+1), D2D_Prob);
				d_di.setWeight(p.getInsert(pCol),   D2I_Prob);

				/*println(10, "Match-> Match  Prob :"+m_di.getWeight(p.getMatch(pCol+1)));
				println(10, "Match-> Delete Prob :"+m_di.getWeight(p.getDelete(pCol+1)));
				println(10, "Match-> Insert Prob :"+m_di.getWeight(p.getInsert(pCol)));
				println(10, "");
				println(10, "Insert-> Match  Prob :"+i_di.getWeight(p.getMatch(pCol+1)));
				println(10, "Insert-> Delete Prob :"+i_di.getWeight(p.getDelete(pCol+1)));
				println(10, "Insert-> Insert Prob :"+i_di.getWeight(p.getInsert(pCol)));
				println(10, "");
				println(10, "Delete-> Match  Prob :"+d_di.getWeight(p.getMatch(pCol+1)));
				println(10, "Delete-> Delete Prob :"+d_di.getWeight(p.getDelete(pCol+1)));
				println(10, "Delete-> Insert Prob :"+d_di.getWeight(p.getInsert(pCol)));*/

			}
		}
	}		 
	
	/**
	 * Returns the amount of symbols within two columns
	 * @param s the sequence to check
	 * @param col1 
	 * @param col2 
	 * @return
	 */
	private int countSymbolsBetween(Sequence s, int col1, int col2) {
		if (col2 - col1 <= 1) return 0;
		int cnt = 0;
		for (int i = col1+1; i < col2;  i++){
			if (isSymbol(s, i)) cnt++;
		}
		return cnt;
	}

	/**
	 * Retrurns true, if between two columns within a sequence is a symbol detected
	 * @param s the sequence to check
	 * @param col1 
	 * @param col2
	 * @return
	 */
	private boolean SymbolBetween(Sequence s, int col1, int col2) {
		if (col2 - col1 <= 1) return false;
		for (int i = col1+1; i < col2;  i++){
			if (isSymbol(s, i)) {
				return true;
			}
			else return false;
		}
		return false;
	}

	/**
	 * Checks, is the actual symbol is a non-gab symbol
	 * @param s the sequence to check
	 * @param col1 the symbol to check
	 * @return
	 */
	private boolean isSymbol(Sequence s, int col1) {
		System.out.println("isSymbol:" + (s.symbolAt(col1 + 1) != s.getAlphabet().getGapSymbol()) );
		return s.symbolAt(col1 + 1) != s.getAlphabet().getGapSymbol();
	}

	/**
	 * Returns a ProfileHMM
	 * @throws Exception
	 */
	public ProfileHMM getProfile() throws Exception {
		return baseProfile;
	}
}
