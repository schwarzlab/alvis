/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alvis.algorithm;

import cama.BioJavaHelper;
import com.general.gui.progress.DefaultProgressable;
import com.general.gui.progress.Progressable;
import de.biozentrum.bioinformatik.MAFactory;
import de.biozentrum.bioinformatik.alignment.MultipleAlignment;
import de.biozentrum.bioinformatik.sequence.Sequence;
import de.biozentrum.bioinformatik.sequence.SequenceAlphabet;
import java.util.ArrayList;
import java.util.List;
import javax.swing.SwingWorker;
import org.biojava.bio.seq.DNATools;
import org.biojava.bio.seq.ProteinTools;
import org.biojava.bio.seq.RNATools;
import org.biojava.bio.symbol.SimpleSymbolList;
import org.biojava.bio.symbol.Symbol;
import org.biojava.bio.symbol.SymbolList;

/**
 *
 * @author Roland Schwarz <rfs32@cam.ac.uk>
 */
public class TranslateAlignmentTask extends SwingWorker<MultipleAlignment, Integer> {

	final DefaultProgressable progressable;
	final MultipleAlignment alignment;
	final boolean reverseComplement;
	
	public TranslateAlignmentTask(MultipleAlignment alignment, boolean reverseComplement) {
		this.alignment = alignment;
		this.reverseComplement = reverseComplement;
		progressable = new DefaultProgressable(this, "Translating alignment...", false);
		progressable.setMinimum(0);
		progressable.setMaximum(alignment.getSequenceCount());
	}

	public TranslateAlignmentTask(MultipleAlignment alignment) {
		this(alignment, false);
	}

	@Override
	protected MultipleAlignment doInBackground() throws Exception {
		progressable.setState(Progressable.STATE_RUNNING);
		ArrayList<Sequence> translatedSequences = new ArrayList<>();
		
		int progress=1;
		for (Sequence seq:alignment) {
			System.out.println(seq.getSequence().length());
			SymbolList sl = BioJavaHelper.convertSequence(seq);
			ArrayList<Symbol> slList = new ArrayList<>(sl.toList());
			int i=0;
			int ngaps=0;
			while (i<slList.size()) {
				Symbol s = slList.get(i);
				if (s.equals(DNATools.getDNA().getGapSymbol())) {
					slList.remove(i);
					ngaps++;
				} else {
					i++;
				}
			}
			sl = new SimpleSymbolList(slList.toArray(new Symbol[]{}), slList.size(), sl.getAlphabet());
			SymbolList slRNA;
			if (this.reverseComplement){
				slRNA = DNATools.transcribeToRNA(sl);
			} else {
				slRNA = DNATools.toRNA(sl);
			}
			
			SymbolList slTranslated = RNATools.translate(slRNA);
			List<Symbol> slProt = new ArrayList<>(slTranslated.toList());
			int j=0;
			while (j<slProt.size()) {
				Symbol s = slProt.get(j);
				if (s.equals(ProteinTools.ter())) {
					slProt.remove(j);
				} else {
					j++;
				}
			}
			SymbolList slProt2 = new SimpleSymbolList(slProt.toArray(new Symbol[]{}), slProt.size(), slTranslated.getAlphabet());
			Sequence seqProt = BioJavaHelper.convertSequence(slProt2, seq.getName(), true);
			translatedSequences.add(seqProt);
			publish(progress++);
		}
		BioJavaHelper.makeEqualLength(translatedSequences);
		MultipleAlignment newAlignment = MAFactory.createMultipleAlignment(translatedSequences);
		newAlignment.setAlphabet(SequenceAlphabet.AMINOACIDS);
		return newAlignment;
		
	}

	@Override
	protected void process(List<Integer> chunks) {
		if (chunks.size() > 0) {
			int last_i = chunks.get(chunks.size() - 1);
			progressable.setValue(last_i);
		}
	}

	@Override
	protected void done() {
		progressable.setState(Progressable.STATE_IDLE);
	}

	/**
	 * @return the progressable
	 */
	public DefaultProgressable getProgressable() {
		return progressable;
	}

}
