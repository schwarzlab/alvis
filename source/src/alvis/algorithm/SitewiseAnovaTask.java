/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package alvis.algorithm;

import alvis.AlvisDataModel;
import alvis.AlvisDataTools;
import alvis.algorithm.matrix.FisherScoresMatrix;
import alvis.algorithm.rengine.REngineNotFoundException;
import cama.BioJavaHelper;
import com.general.gui.progress.DefaultProgressable;
import com.general.gui.progress.Progressable;
import de.biozentrum.bioinformatik.alignment.MultipleAlignment;
import de.biozentrum.bioinformatik.sequence.Sequence;
import java.util.Map;
import javax.swing.SwingWorker;
import org.biojava.bio.symbol.FiniteAlphabet;
import org.rosuda.JRI.REXP;
import org.rosuda.JRI.Rengine;

/**
 *
 * @author Roland Schwarz <rfs32@cam.ac.uk>
 */
public class SitewiseAnovaTask extends SwingWorker<double[], Object>{
	private final FisherScoresMatrix fisherScores;
	private final Map<Sequence, Integer> sequenceGroups;
	private final MultipleAlignment alignment;
	private final static String[] MODULE_NAMES = new String[] {"sitewise_anova.R", "bumPlots.R", "bumStatistics.R"};
	DefaultProgressable progressable = new DefaultProgressable(this, "Searching for sites...", true);
	private static final double FDR=0.05;
	
	public SitewiseAnovaTask(FisherScoresMatrix fisherScores, Map<Sequence, Integer> sequenceGroups, MultipleAlignment alignment) {
		this.fisherScores=fisherScores;
		this.sequenceGroups = sequenceGroups;
		this.alignment=alignment;
	}
	
	@Override
	protected double[] doInBackground() throws Exception {
		progressable.setState(Progressable.STATE_RUNNING);
		Rengine R = Rengine.getMainEngine();
		if (R==null) throw new REngineNotFoundException("REngine not found!");
		
		// load module
		for (String module:MODULE_NAMES) {
			R.eval("source(\"Rmodules/"+ module +"\")");
		}
		
		// transfer data matrix
		R.eval("fisherScores = matrix(data=0, nrow="+fisherScores.rows+", ncol="+fisherScores.columns+")");
		for (int i = 0; i<fisherScores.rows; i++) {
			R.assign("i", new int[] {i+1});
			R.assign("currentRow", fisherScores.getRow(i).toArray());
			R.eval("fisherScores[i,] = currentRow");
		}
		boolean[] mask = ProfileHMMTools.profileColumnMask(alignment, AlvisDataModel.GAP_CHAR, AlvisDataModel.MATCH_CUTOFF);
		int[] alignmentColumns = new int[fisherScores.columns];
		FiniteAlphabet bjalpha = BioJavaHelper.convertAlphabet(alignment.getAlphabet());
		for (int j=0;j<fisherScores.columns;j++) {
			int profileColumn = j / bjalpha.size();
			alignmentColumns[j] = ProfileHMMTools.getAlignmentColumnFromProfileColumn(profileColumn, mask);
		}

		R.assign("alignmentColumns", alignmentColumns);
		
		// transfer groups
		int[] groups = AlvisDataTools.sequenceGroupsToIntArray(sequenceGroups, alignment.getSequences());
		R.assign("groups", groups);
		R.eval("groups = as.factor(groups)");

		R.eval("save.image()");		
		// run analysis
		REXP rexp = R.eval("sitewise_anova(fisherScores, groups, alignmentColumns, "+FDR+")");
		double[] anovaResult = rexp.asDoubleArray();
		
		double[] finalResult = new double[alignment.length()];
		int j=0;
		for (int i=0;i<alignment.length();i++) {
			if (mask[i]) {
				finalResult[i]=anovaResult[j++];
			} else {
				finalResult[i]=Double.NaN;
			}
		}
		return finalResult;
	}

	@Override
	protected void done() {
		progressable.setState(Progressable.STATE_IDLE);
	}

	
	/**
	 * @return the progressable
	 */
	public DefaultProgressable getProgressable() {
		return progressable;
	}
	
	
}
