package alvis.algorithm;

import com.general.utils.GeneralUtils;
import de.biozentrum.bioinformatik.alignment.MultipleAlignment;
import java.util.Iterator;

import org.biojava.bio.BioException;
import org.biojava.bio.dist.Distribution;
import org.biojava.bio.dist.DistributionFactory;
import org.biojava.bio.dist.DistributionTools;
import org.biojava.bio.dp.DP;
import org.biojava.bio.dp.EmissionState;
import org.biojava.bio.dp.ProfileHMM;
import org.biojava.bio.dp.ScoreType;
import org.biojava.bio.dp.State;
import org.biojava.bio.dp.StatePath;
import org.biojava.bio.seq.io.SymbolTokenization;
import org.biojava.bio.symbol.Alphabet;
import org.biojava.bio.symbol.FiniteAlphabet;
import org.biojava.bio.symbol.IllegalAlphabetException;
import org.biojava.bio.symbol.IllegalSymbolException;
import org.biojava.bio.symbol.Symbol;
import org.biojava.bio.symbol.SymbolList;

import java.util.List;
import java.util.Map;
import org.biojava.bio.alignment.Alignment;
import org.jblas.DoubleMatrix;

public class ProfileHMMTools {
	
	public static final int StateTypeMatch = 0;
	public static final int StateTypeInsert = 1;
	public static final int StateTypeDelete = 2;
	
	
	/*private static void copyEmissions( EmissionState source, ProfileState target) {
		FiniteAlphabet alphabet = (FiniteAlphabet)source.getDistribution().getAlphabet();
		Iterator<Symbol> iterator = (alphabet).iterator();
		SymbolTokenization tokenization = null;
		try {
			tokenization = (SymbolTokenization)alphabet.getTokenization("token");
		} catch (BioException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Distribution em = source.getDistribution();
		de.biozentrum.bioinformatik.distributions.Distribution dist = new DiscreteDistribution();
		
		Map<Character, Integer> map = new HashMap<Character, Integer>();
		
		try {
			while ( iterator.hasNext()) {
				Symbol s = iterator.next();
			
				double weight = em.getWeight(s);
				Character c = tokenization.tokenizeSymbol(s).toUpperCase().charAt(0);
				
				map.put(c, (int)(10000 * weight));
			}
		} catch (IllegalSymbolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Character[] chars = map.keySet().toArray( new Character[1]);
		int[] occurrences = new int[ chars.length];
		
		for ( int i = 0; i < chars.length; i++) {
			occurrences[i] = map.get(chars[i]);
		}
		
		dist = new DiscreteDistribution(chars, occurrences);
		target.setEmissionProbabilities(dist);
	}
	
	public static de.biozentrum.bioinformatik.hmm.profile.ProfileHMM convertProfileHMM( ProfileHMM profileHMM, de.biozentrum.bioinformatik.hmm.Alphabet a) {
		de.biozentrum.bioinformatik.hmm.profile.ProfileHMM newHMM = new DefaultProfileHMM(profileHMM.columns()+2);
		System.out.println("total columns "+ profileHMM.columns());
		newHMM.setAlphabet(a);
		
		try {
			
			EmissionState start = profileHMM.getMatch(0);
			Distribution startWeights = profileHMM.getWeights( start);
			
			EmissionState firstInsert = profileHMM.getInsert(0);
			Distribution firstInsertWeights = profileHMM.getWeights( firstInsert);
			
			EmissionState nMatch = profileHMM.getMatch(1);
			DotState nDelete = profileHMM.getDelete(1);
			
			ProfileColumn startColumn = newHMM.getColumnAt(0);
			
			copyEmissions( firstInsert, startColumn.getStateByType(ProfileState.INSERT_STATE));
			
			startColumn.getTransition(ProfileState.BEGIN_STATE, ProfileState.INSERT_STATE).setProbability( startWeights.getWeight( firstInsert));
			startColumn.getTransition(ProfileState.BEGIN_STATE, ProfileState.DELETE_STATE).setProbability( startWeights.getWeight( nDelete));
			startColumn.getTransition(ProfileState.BEGIN_STATE, ProfileState.MATCH_STATE).setProbability( startWeights.getWeight( nMatch));
			
			startColumn.getTransition(ProfileState.INSERT_STATE, ProfileState.INSERT_STATE).setProbability( firstInsertWeights.getWeight( firstInsert));
			startColumn.getTransition(ProfileState.INSERT_STATE, ProfileState.DELETE_STATE).setProbability( firstInsertWeights.getWeight( nDelete));
			startColumn.getTransition(ProfileState.INSERT_STATE, ProfileState.MATCH_STATE).setProbability( firstInsertWeights.getWeight( nMatch));
			
			
			for ( int i = 1; i < profileHMM.columns(); i++) {
				System.out.println( "column: "+ i);
				
				EmissionState match = profileHMM.getMatch(i);
				EmissionState insert = profileHMM.getInsert(i);
				DotState delete = profileHMM.getDelete(i);
				
				EmissionState nextMatch = profileHMM.getMatch(i+1);
				DotState nextDelete = profileHMM.getDelete(i+1);
				
				Distribution matchWeights = profileHMM.getWeights( match);
				Distribution insertWeights = profileHMM.getWeights( insert);
				Distribution deleteWeights = profileHMM.getWeights(delete);
				
				ProfileColumn column = newHMM.getColumnAt(i);
				
				column.getTransition(ProfileState.MATCH_STATE, ProfileState.INSERT_STATE).setProbability( matchWeights.getWeight( insert));
				column.getTransition(ProfileState.MATCH_STATE, ProfileState.DELETE_STATE).setProbability( matchWeights.getWeight( nextDelete));
				column.getTransition(ProfileState.MATCH_STATE, ProfileState.MATCH_STATE).setProbability( matchWeights.getWeight( nextMatch));
				
				column.getTransition(ProfileState.DELETE_STATE, ProfileState.INSERT_STATE).setProbability( deleteWeights.getWeight( insert));
				column.getTransition(ProfileState.DELETE_STATE, ProfileState.DELETE_STATE).setProbability( deleteWeights.getWeight( nextDelete));
				column.getTransition(ProfileState.DELETE_STATE, ProfileState.MATCH_STATE).setProbability( deleteWeights.getWeight( nextMatch));
				
				column.getTransition(ProfileState.INSERT_STATE, ProfileState.INSERT_STATE).setProbability( insertWeights.getWeight( insert));
				column.getTransition(ProfileState.INSERT_STATE, ProfileState.DELETE_STATE).setProbability( insertWeights.getWeight( nextDelete));
				column.getTransition(ProfileState.INSERT_STATE, ProfileState.MATCH_STATE).setProbability( insertWeights.getWeight( nextMatch));
				
				copyEmissions(match, column.getStateByType( ProfileState.MATCH_STATE));
				copyEmissions(insert, column.getStateByType( ProfileState.INSERT_STATE));
			}
			
			EmissionState lastMatch = profileHMM.getMatch(profileHMM.columns());
			EmissionState lastInsert = profileHMM.getInsert(profileHMM.columns());
			DotState lastDelete = profileHMM.getDelete(profileHMM.columns());
			
			;
			
			Distribution lastMatchWeights = profileHMM.getWeights( lastMatch);
			Distribution lastInsertWeights = profileHMM.getWeights( lastInsert);
			Distribution lastDeleteWeights = profileHMM.getWeights(lastDelete);
			
			EmissionState endState = profileHMM.getMatch(0);
			
			ProfileColumn col = newHMM.getColumnAt(profileHMM.columns());
			
			copyEmissions(lastMatch, col.getStateByType( ProfileState.MATCH_STATE));
			copyEmissions(lastInsert, col.getStateByType( ProfileState.INSERT_STATE));
			col.getTransition(ProfileState.MATCH_STATE, ProfileState.END_STATE).setProbability(lastMatchWeights.getWeight(endState));
			col.getTransition(ProfileState.MATCH_STATE, ProfileState.INSERT_STATE).setProbability(lastMatchWeights.getWeight(lastInsert));
			
			col.getTransition(ProfileState.INSERT_STATE, ProfileState.END_STATE).setProbability(lastInsertWeights.getWeight(endState));
			col.getTransition(ProfileState.INSERT_STATE, ProfileState.INSERT_STATE).setProbability(lastInsertWeights.getWeight(lastInsert));
			
			col.getTransition(ProfileState.DELETE_STATE, ProfileState.END_STATE).setProbability(lastDeleteWeights.getWeight(endState));
			col.getTransition(ProfileState.DELETE_STATE, ProfileState.INSERT_STATE).setProbability(lastDeleteWeights.getWeight(lastInsert));
			
		} catch (IllegalSymbolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IndexOutOfBoundsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return newHMM;
		
	}*/
	
	public static State getState( ProfileHMM profileHMM, int column, int stateType ) {
		switch (stateType) {
		case StateTypeDelete:
			return profileHMM.getDelete( column);
		case StateTypeInsert:
			return profileHMM.getInsert( column);
		case StateTypeMatch:
			return profileHMM.getMatch( column);
			
		default:
			break;
		}
		return null;
	}
	
	public static DoubleMatrix getEmissionMatrix( ProfileHMM profileHMM, int stateType, List<Symbol> index) {
		int columns = profileHMM.columns() + 1;
		
		
		
		int rows = index.size();
		
		DoubleMatrix matrix = new DoubleMatrix( rows, columns );
		
		if ( stateType != StateTypeDelete) {
			for ( int modelPos = 0; modelPos < columns; modelPos++ ) {
				if ( modelPos == 0 && stateType == StateTypeMatch) continue;
				
				EmissionState state = (EmissionState)getState(profileHMM, modelPos, stateType);
				for ( int alphIndex = 0; alphIndex < rows; alphIndex++) {
					try {
						double emm = 0;
						int count = 0;
						
						Iterator<Symbol> iterator = ((FiniteAlphabet)index.get(alphIndex).getMatches()).iterator();
						while ( iterator.hasNext()) {
							emm += state.getDistribution().getWeight(iterator.next());
							count++;
						}
						
						emm = emm / (double)count;
						matrix.put(alphIndex, modelPos, emm);
					} catch (IllegalSymbolException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IndexOutOfBoundsException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
		
		return matrix;
	}

	public static DoubleMatrix getTransitionMatrix( ProfileHMM profileHMM, int stateType ) {
		
		int columns = profileHMM.columns() + 1;
		
		DoubleMatrix matrix = new DoubleMatrix( 3, columns );
		
		
		for ( int modelPos = 0; modelPos < columns; modelPos++ ) {
			// No Deletestate in first column
			if ( modelPos == 0 && stateType == StateTypeDelete) continue;
			
			State state = getState(profileHMM, modelPos, stateType);
			try {
				Distribution d = profileHMM.getWeights(state);
				if ( modelPos < columns - 1) {
					matrix.put(StateTypeDelete, modelPos, d.getWeight(profileHMM.getDelete( modelPos+1)));
				}
				matrix.put(StateTypeInsert, modelPos, d.getWeight(profileHMM.getInsert( modelPos)));
				matrix.put(StateTypeMatch, modelPos, d.getWeight(profileHMM.getMatch( modelPos+1)));
			}
			catch ( Exception e) {
				System.out.println(e);
			}
		}
		return matrix;
	}

	public static void printProfileHMM(ProfileHMM p) throws Exception {

		for (int i = 0; i <= p.columns(); i++) {
			for (int j = 0; j < 1; j++) {
				if (i == 0 && j == 1) continue; // Delete0 ex. nicht



				State 	m = null;
				switch (j) {
					case 0: m = p.getMatch(i);  break;
					case 1: m = p.getDelete(i); break;
					case 2: m = p.getInsert(i); break;
				}
				Distribution 	 d 		= p.getWeights(m);
				FiniteAlphabet 	 alpha 	= (FiniteAlphabet) d.getAlphabet();
				
				if ( j == 0 || j == 2) {
					EmissionState state = (EmissionState)m;
					FiniteAlphabet al = (FiniteAlphabet)state.getDistribution().getAlphabet();

					System.out.println(al.getClass());
					
					Iterator<Symbol> it = ((FiniteAlphabet)al).iterator();
					
					while ( it.hasNext()) {
						Symbol sym = it.next();
						try {
						System.out.println( al.getTokenization("token").tokenizeSymbol(sym) + ": " + state.getDistribution().getWeight(sym));
						}
						catch ( Exception e) {
							
						}
					}


					
				}
				

				Iterator<Symbol> itt 	= alpha.iterator();
				while (itt.hasNext()) {
					Symbol s = itt.next();
					double w = d.getWeight(s);
					System.out.println(m.getName() + " to " + s.getName() + " Gewicht: " + w);
				}

				System.out.println();
			}
		}
	}
	
	
	public static ProfileHMM createProfile(Alignment alignment, double matchThreshold, int pseudoCounts) throws Exception {
		boolean[] columnMask = profileColumnMask(alignment, matchThreshold);
		
		int nProfileColumns = countProfileColumns(columnMask);
		
		if (nProfileColumns==0) {
			throw new AlvisAlgorithmException("Warning! The alignment only contains gap columns. No ProfileHMM was trained.");
		}
		
		ProfileHMM profileHMM = new ProfileHMM(((SymbolList)alignment.symbolListIterator().next()).getAlphabet(), nProfileColumns, DistributionFactory.DEFAULT, DistributionFactory.DEFAULT, null);
		
		configureProfile(profileHMM, alignment, columnMask, pseudoCounts);
		
		return profileHMM;
	}
	
	
	
	
	public static void alignSequence( DP baseDP, SymbolList s, ScoreType scoreType) throws Exception {
		

		SymbolList[] 		sl 			= {s};
		StatePath 			sp 			= baseDP.viterbi(sl, scoreType);
		double 				fwScore 	= baseDP.forward(sl,  scoreType);
		double 				bwScore 	= baseDP.backward(sl, scoreType);

//		String				test 		= "aaaaa";
		String 				seqString 	= "";
		String 				stateString = "";   
		
		Alphabet alphabet = s.getAlphabet();
		SymbolTokenization 	token 		= alphabet.getTokenization("token");
		
		for (int i = 1 ; i <= sp.length() ; i++) {
			seqString 	+= token.tokenizeSymbol(sp.symbolAt((String) StatePath.SEQUENCE, i));
			stateString += sp.symbolAt((String) StatePath.STATES, i).getName().charAt(0);
		}
		System.out.println( "Alignment-Sequence [" + seqString + "] [" + stateString + "] alias [" + "name" + "]: viterbi[" + sp.getScore() + "] fw[" + fwScore + "] bw[" + bwScore + "]");
		System.out.println( ((ExtendedSingleDP)baseDP).posteriorMatrix(sl, scoreType));
}
	
	public static ExtendedSingleDP createDP( ProfileHMM profileHMM) throws IllegalArgumentException, BioException {
		return new ExtendedSingleDP(profileHMM);
	}
	
	public static int countProfileColumns( Alignment alignment, double threshold) {
		return countProfileColumns( profileColumnMask(alignment, threshold));
	}
	
	public static void configureProfile(ProfileHMM p, Alignment al, boolean[] profileColumnMask, int pseudoCounts) throws Exception {

		//println(1, "Configuring ProfileHMM [" + p.toString() + "] rom DB [" + db.getName() + "] and the corresponding alignment.");
		initializeTransitionWeight(p, al, profileColumnMask, pseudoCounts);
		initializeEmissions(p, al, profileColumnMask);
	}
	
	public static int countProfileColumns( boolean[] profileColumnMask) {
		int count = 0;
		for ( boolean b : profileColumnMask) {
			if ( b) count++;
		}
		return count;
	}
	
	public static boolean[] profileColumnMask( MultipleAlignment alignment, char gapChar, double threshold) {
		boolean[] retval = new boolean[alignment.length()];
		for (int i=0;i<alignment.length();i++) {
			Map<Character, Integer> map = alignment.getSymbolCounts(i);
			Integer gapCount = map.get(gapChar);
			if (gapCount==null) gapCount=0;
			double gapWeight = (double)gapCount / alignment.rows();
			retval[i] = gapWeight < threshold;
		}
		return retval;
	}
	
	public static boolean[] profileColumnMask( Alignment alignment, double threshold) {
		Symbol gapSymbol = ((SymbolList)alignment.symbolListIterator().next()).getAlphabet().getGapSymbol();
		
		boolean[] mask = new boolean[alignment.length()];
		Distribution[] dis = null;
		try {
			dis = DistributionTools.distOverAlignment(alignment, true, 1.0);
		
		
			for ( int i = 0; i < dis.length; i++) {
				double gapWeight = dis[i].getWeight( gapSymbol);
				mask[i] = gapWeight < threshold;
			}
		} catch (IllegalAlphabetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalSymbolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return mask;
	}
	
	public static void initializeEmissions(ProfileHMM p, Alignment alignment, boolean[] profileColumnMask) throws Exception {

		int pCol = 0;
		
		SymbolList list = (SymbolList) alignment.symbolListIterator().next();
		int alphabetSize = ((FiniteAlphabet)list.getAlphabet()).size();
		
		Distribution[] d = DistributionTools.distOverAlignment(alignment, false, alphabetSize);
		
		int[] cols = new int[alignment.length()];
		for ( int i = 0; i < cols.length; i++) {
			cols[i] = i+1;
		} 
		
		p.getInsert(0).setDistribution( DistributionTools.average(d));
		
		for (int i = 0; i < d.length; i++){						// iterate over alignment cols
			if (profileColumnMask[i]) {
				pCol++; 
				p.getMatch(pCol).setDistribution(d[i]);
				p.getInsert(pCol).setDistribution(d[i]);				
			}
		}
		
	}
	
	private static int getAlignmentColumnFromProfileColumn(ProfileHMM p, Alignment a, int pCol, boolean[] profileColumnMask) throws Exception {
		if (pCol <= 0 || pCol > p.columns()) return -1;
		for (int i = 0 ; i < a.length() ; i++) {
			if (profileColumnMask[i]) pCol--;
			if (pCol == 0) return i;
		}
		return -1;
	}

	/**
	 * Maps profile Columns to alignment columns given a column mask. The column mask must have as many entries as the alignment has columns.
	 * @param pCol
	 * @param profileColumnMask
	 * @return
	 * @throws Exception 
	 */
	public static int getAlignmentColumnFromProfileColumn(int pCol, boolean[] profileColumnMask) throws Exception {
		return GeneralUtils.whichTrue(profileColumnMask)[pCol];
	}
	
	private static boolean isSymbol(SymbolList s, int col1) {
		return s.symbolAt(col1 + 1) != s.getAlphabet().getGapSymbol();
	}
	
	private static boolean SymbolBetween(SymbolList s, int col1, int col2) throws Exception {
		//if (col2 < col1) throw new Exception("Invalid columns specified");
		if (col2 - col1 <= 1) return false;
		for (int i = (col1 + 1) ; i < col2 ;  i++){
			if (isSymbol(s, i)) return true;
		}
		return false;
	}
	
	private static int countSymbolsBetween(SymbolList s, int col1, int col2) {
		if (col2 - col1 <= 1) return 0;
		int cnt = 0;
		for (int i = col1+1; i < col2;  i++){
			if (isSymbol(s, i)) cnt++;
		}
		return cnt;
	}
	
	public static void initializeTransitionWeight(ProfileHMM p, Alignment a, boolean[] profileColumnMask, int pseudoCnt) throws Exception {
		boolean insertBlock = false;
		for (int pCol = 0 ; pCol <= p.columns() ; pCol++) {			// iterate over all ProfileHPP Columns including start-column 0

			//println(10, "Configuring profileHMM column " + pCol);
			
			/* die beiden f�r pCol relevanten aCols ermitteln */
			int aCol1 = getAlignmentColumnFromProfileColumn(p, a, pCol, profileColumnMask);
			int aCol2 = getAlignmentColumnFromProfileColumn(p, a, pCol+1, profileColumnMask);
			//println(10, " using aCol " + aCol1 + " and aCol " + aCol2);
			if (aCol1 == aCol2) throw new Exception("Invalid column numbers!");
			insertBlock = (aCol2 - aCol1 > 1); 
			//if (insertBlock) println(10, " insert block found!");

						
			// init Transition-Probs with Pseudo-Counts
			int M2M = pseudoCnt, 
				M2I = pseudoCnt, 
				M2D = pseudoCnt, 
				I2M = pseudoCnt, 
				I2I = pseudoCnt, 
				I2D = pseudoCnt, 
				D2M = pseudoCnt, 
				D2I = pseudoCnt, 
				D2D = pseudoCnt;
			
			Iterator<SymbolList> it = a.symbolListIterator();
			while (it.hasNext()){
				SymbolList s = it.next();
				
				if (aCol1 == -1) {			// we are in the ProfileHMM's start column 0
					/* Magical->M */
					if (isSymbol(s, aCol2) && !SymbolBetween(s, aCol1, aCol2)) 							M2M++;
					/* Magical->D */
					if (!isSymbol(s, aCol2) && !SymbolBetween(s, aCol1, aCol2))							M2D++;
					/* Magical->I */
					if (SymbolBetween(s, aCol1, aCol2))													M2I++;
					
					/* I->M */
					if (SymbolBetween(s, aCol1, aCol2) && isSymbol(s, aCol2))							I2M++;
					/* I->D */
					if (SymbolBetween(s, aCol1, aCol2) && !isSymbol(s, aCol2))							I2D++;
					/* I->I */
					if (countSymbolsBetween(s, aCol1, aCol2) > 1 ) I2I += countSymbolsBetween(s, aCol1, aCol2) - 1;
					
					/* in column 0 does delete state not exist */
					//M2D = 0;
					D2M = 0;
					D2D = 0;
					D2I = 0;
					
					
				} else if (aCol2 == -1) {	// we are in the ProfileHMM's end column 
					/* M->Magical */
					if (isSymbol(s, aCol1) && !SymbolBetween(s, aCol1, aCol2))							M2M++;
					/* D->Magical */
					if (!isSymbol(s, aCol1) && !SymbolBetween(s, aCol1, aCol2))							D2M++;
					/* I->Magical */
					if (countSymbolsBetween(s, aCol1, aCol2) > 1) I2M += countSymbolsBetween(s, aCol1, aCol2) - 1;
					
				} else {					// we are somewhere inbetween
					/* M->M */
					if (isSymbol(s, aCol1) && isSymbol(s, aCol2) && !SymbolBetween(s, aCol1, aCol2)) 	M2M++;
					/* M->D*/ 
					if (isSymbol(s, aCol1) && !isSymbol(s, aCol2) && !SymbolBetween(s, aCol1, aCol2)) 	M2D++;
					/* M->I */
					if (isSymbol(s, aCol1) && SymbolBetween(s, aCol1, aCol2)) 							M2I++;
					
					
					/* D->M */
					if (!isSymbol(s, aCol1) && isSymbol(s, aCol2) && !SymbolBetween(s, aCol1, aCol2))	D2M++;
					/* D->D */
					if (!isSymbol(s, aCol1) && !isSymbol(s, aCol2) && !SymbolBetween (s, aCol1, aCol2))	D2D++;
					/* D->I */
					if (!isSymbol(s, aCol1) && SymbolBetween (s, aCol1, aCol2))							D2I++;
					
					
					/* I->M */
					if(SymbolBetween(s, aCol1, aCol2) && isSymbol(s, aCol2))				 			I2M++;
					/* I->D */ 
					if (SymbolBetween(s, aCol1, aCol2) && !isSymbol(s, aCol2))							I2D++;
					/* I->I */ 
					if ( countSymbolsBetween(s, aCol1, aCol2) > 1 )	I2I += countSymbolsBetween(s, aCol1, aCol2) - 1;
					
					
				}
			}
			
			
			/* add all counts of a col*/
			int matchSum 	= M2M + M2D + M2I;
			int insertSum 	= I2M + I2D + I2I;
			int deleteSum 	= D2M + D2D + D2I;

			double M2M_Prob = (double) M2M / matchSum;
			double M2D_Prob = (double) M2D / matchSum;
			double M2I_Prob = (double) M2I / matchSum;
			
			double D2M_Prob = (double) D2M / deleteSum;
			double D2D_Prob = (double) D2D / deleteSum;
			double D2I_Prob = (double) D2I / deleteSum;
			
			double I2M_Prob = (double) I2M / insertSum;
			double I2D_Prob = (double) I2D / insertSum;
			double I2I_Prob = (double) I2I / insertSum;
			
			/*println(10, "M2M: "+M2M);
			println(10, "M2D: "+M2D);
			println(10, "M2I: "+M2I);
			println(10, "match-Sum: "+matchSum);
			println(10, "M2M_Prob: "+M2M_Prob);
			println(10, "M2D_Prob: "+M2D_Prob);
			println(10, "M2I_Prob: "+M2I_Prob);
			println(10, "");
			println(10, "D2M: "+D2M);
			println(10, "D2D: "+D2D);
			println(10, "D2I: "+D2I);
			println(10, "delete-Sum: "+deleteSum);
			println(10, "D2M_Prob: "+D2M_Prob);
			println(10, "D2D_Prob: "+D2D_Prob);
			println(10, "D2I_Prob: "+D2I_Prob);
			println(10, "");
			println(10, "I2M: "+I2M);
			println(10, "I2D: "+I2D);
			println(10, "I2I: "+I2I);
			println(10, "insert-Sum: "+insertSum);
			println(10, "I2M_Prob: "+I2M_Prob);
			println(10, "I2D_Prob: "+I2D_Prob);
			println(10, "I2I_Prob: "+I2I_Prob);
			println(10, "");*/
			
			/* Set the Transition-Probs */
			/* Magical ->x */
			if (pCol == 0){
				Distribution m_di = p.getWeights(p.getMatch(pCol));
				Distribution i_di = p.getWeights(p.getInsert(pCol));

				m_di.setWeight(p.getMatch(pCol+1),  M2M_Prob);  
				m_di.setWeight(p.getDelete(pCol+1), M2D_Prob);  
				m_di.setWeight(p.getInsert(pCol),  	M2I_Prob);
				
				i_di.setWeight(p.getMatch(pCol+1),  I2M_Prob);
				i_di.setWeight(p.getDelete(pCol+1), I2D_Prob);
				i_di.setWeight(p.getInsert(pCol),   I2I_Prob);
				
				/*println(10, "Magical->  Match  Prob  :"+m_di.getWeight(p.getMatch(pCol+1)));
				println(10, "Magical->  Delete Prob  :"+m_di.getWeight(p.getDelete(pCol+1)));
				println(10, "Magical->  Insert Prob  :"+m_di.getWeight(p.getInsert(pCol)));
				println(10, "");
				println(10, "Insert-0-> Match-1 Prob :"+i_di.getWeight(p.getMatch(pCol+1)));
				println(10, "Insert-0-> Delete-0 Prob:"+i_di.getWeight(p.getDelete(pCol+1)));
				println(10, "Insert-0-> Insert-0 Prob:"+i_di.getWeight(p.getInsert(pCol)));*/
				
			}
			/* x->Magical */
			else if (pCol >= p.columns()){

				matchSum 	= M2M + M2I;
				insertSum 	= I2M + I2I;
				deleteSum 	= D2M + D2I;

				M2M_Prob = (double) M2M / matchSum;
				M2I_Prob = (double) M2I / matchSum;
				
				D2M_Prob = (double) D2M / deleteSum;
				D2I_Prob = (double) D2I / deleteSum;
				
				I2M_Prob = (double) I2M / insertSum;
				I2I_Prob = (double) I2I / insertSum;


				Distribution m_di = p.getWeights(p.getMatch(pCol));
				Distribution i_di = p.getWeights(p.getInsert(pCol));
				Distribution d_di = p.getWeights(p.getDelete(pCol)); 
				

				
				m_di.setWeight(p.getMatch(pCol+1),  M2M_Prob); 
				m_di.setWeight(p.getInsert(pCol),   M2I_Prob);
				
				d_di.setWeight(p.getMatch(pCol+1), 	D2M_Prob);
				d_di.setWeight(p.getInsert(pCol),   D2I_Prob);
				
				i_di.setWeight(p.getMatch(pCol+1), 	I2M_Prob);
				i_di.setWeight(p.getInsert(pCol),   I2I_Prob);
				
				/*println(10, "Match->  Magical Prob :"+m_di.getWeight(p.getMatch(pCol+1)));
				println(10, "Delete-> Magical Prob :"+d_di.getWeight(p.getMatch(pCol+1)));
				println(10, "Insert-> Magical Prob :"+i_di.getWeight(p.getMatch(pCol+1)));
				println(10, "");
				println(10, "Match->  Insert Prob :"+m_di.getWeight(p.getInsert(pCol)));
				println(10, "Delete-> Insert Prob :"+d_di.getWeight(p.getInsert(pCol)));
				println(10, "Insert-> Insert Prob :"+i_di.getWeight(p.getInsert(pCol)));*/

			}
			/* all other states */
			else{
				Distribution m_di = p.getWeights(p.getMatch(pCol));
				Distribution i_di = p.getWeights(p.getInsert(pCol));
				Distribution d_di = p.getWeights(p.getDelete(pCol)); 
				
				m_di.setWeight(p.getMatch(pCol+1),  M2M_Prob);  // mit welcher WS von match(j) nach match(j+1) ???
				m_di.setWeight(p.getDelete(pCol+1), M2D_Prob);
				m_di.setWeight(p.getInsert(pCol),   M2I_Prob);
				
				i_di.setWeight(p.getMatch(pCol+1),  I2M_Prob);
				i_di.setWeight(p.getDelete(pCol+1), I2D_Prob);
				i_di.setWeight(p.getInsert(pCol),   I2I_Prob);
				
				d_di.setWeight(p.getMatch(pCol+1),  D2M_Prob);
				d_di.setWeight(p.getDelete(pCol+1), D2D_Prob);
				d_di.setWeight(p.getInsert(pCol),   D2I_Prob);

				/*println(10, "Match-> Match  Prob :"+m_di.getWeight(p.getMatch(pCol+1)));
				println(10, "Match-> Delete Prob :"+m_di.getWeight(p.getDelete(pCol+1)));
				println(10, "Match-> Insert Prob :"+m_di.getWeight(p.getInsert(pCol)));
				println(10, "");
				println(10, "Insert-> Match  Prob :"+i_di.getWeight(p.getMatch(pCol+1)));
				println(10, "Insert-> Delete Prob :"+i_di.getWeight(p.getDelete(pCol+1)));
				println(10, "Insert-> Insert Prob :"+i_di.getWeight(p.getInsert(pCol)));
				println(10, "");
				println(10, "Delete-> Match  Prob :"+d_di.getWeight(p.getMatch(pCol+1)));
				println(10, "Delete-> Delete Prob :"+d_di.getWeight(p.getDelete(pCol+1)));
				println(10, "Delete-> Insert Prob :"+d_di.getWeight(p.getInsert(pCol)));*/

			}
		}
	}

	
}
