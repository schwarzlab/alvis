/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package alvis.algorithm.rengine;

/**
 *
 * @author Roland Schwarz <rfs32@cam.ac.uk>
 */
public class REngineNotFoundException extends Exception{

	public REngineNotFoundException() {
	}

	public REngineNotFoundException(String message) {
		super(message);
	}

	public REngineNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public REngineNotFoundException(Throwable cause) {
		super(cause);
	}

	public REngineNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
	
}
