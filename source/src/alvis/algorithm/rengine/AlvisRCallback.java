/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package alvis.algorithm.rengine;

import com.general.gui.dialog.DialogFactory;
import gui.MainFrame;
import org.rosuda.JRI.RMainLoopCallbacks;
import org.rosuda.JRI.Rengine;

/**
 *
 * @author Roland Schwarz <rfs32@cam.ac.uk>
 */
public class AlvisRCallback implements RMainLoopCallbacks{
	private final MainFrame mainFrame;
	public AlvisRCallback(MainFrame mainFrame){
		this.mainFrame=mainFrame;
	}

	@Override
	public void rWriteConsole(Rengine rngn, String string, int i) {
		System.out.println(string);
	}

	@Override
	public void rBusy(Rengine rngn, int i) {
	}

	@Override
	public String rReadConsole(Rengine rngn, String string, int i) {
		return "";
	}

	@Override
	public void rShowMessage(Rengine rngn, String string) {
		DialogFactory.showErrorMessage(string, "Error in REngine:");
	}

	@Override
	public String rChooseFile(Rengine rngn, int i) {
		return "";
	}

	@Override
	public void rFlushConsole(Rengine rngn) {
	}

	@Override
	public void rSaveHistory(Rengine rngn, String string) {
	}

	@Override
	public void rLoadHistory(Rengine rngn, String string) {
	}
	
}
