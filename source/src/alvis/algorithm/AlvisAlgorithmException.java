/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package alvis.algorithm;

/**
 *
 * @author Roland Schwarz <rfs32@cam.ac.uk>
 */
public class AlvisAlgorithmException extends Exception{
	public AlvisAlgorithmException() {
	}

	public AlvisAlgorithmException(String message) {
		super(message);
	}

	public AlvisAlgorithmException(String message, Throwable cause) {
		super(message, cause);
	}

	public AlvisAlgorithmException(Throwable cause) {
		super(cause);
	}

	public AlvisAlgorithmException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
	
	
}
