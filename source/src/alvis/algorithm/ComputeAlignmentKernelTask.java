/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alvis.algorithm;

import alvis.AlvisDataModel;
import com.general.containerModel.DefaultNameable;
import com.general.containerModel.Nameable;
import com.general.gui.progress.DefaultProgressable;
import com.general.gui.progress.Progressable;
import de.biozentrum.bioinformatik.alignment.MultipleAlignment;
import java.util.List;
import javax.swing.SwingWorker;
import org.biojava.bio.BioException;
import org.biojava.bio.alignment.SubstitutionMatrix;
import org.biojava.bio.seq.io.SymbolTokenization;
import org.biojava.bio.symbol.Symbol;
import org.jblas.DoubleMatrix;

/**
 *
 * @author Roland Schwarz <rfs32@cam.ac.uk>
 */
public class ComputeAlignmentKernelTask extends SwingWorker<DoubleMatrix, Integer> {
	private final static String ALIGNMENT_KERNEL_TEXT = "Evolutionary Kernel based on the %s matrix.\n"+
			"This kernel was directly derived from the multiple alignment.";
	private final MultipleAlignment alignment;
	private final SubstitutionMatrix subsMat;
	final DefaultProgressable progress;
	private final int gapExtend;
	private final int gapOpen;
	Nameable kernelDescription;
	
	public ComputeAlignmentKernelTask(MultipleAlignment alignment, SubstitutionMatrix subsMatrix, int gapOpen, int gapExtend) {
		this.alignment = alignment;
		this.subsMat = subsMatrix;
		this.gapOpen = gapOpen;
		this.gapExtend = gapExtend;
		this.progress = new DefaultProgressable(this, "Computing alignment kernel...", false);
		String title = "Evolutionary kernel [" + subsMat.getName() + "]";
		String desc = String.format(ALIGNMENT_KERNEL_TEXT, subsMat.getName());
		this.kernelDescription=new DefaultNameable(title,desc);
	}

	@Override
	protected DoubleMatrix doInBackground() throws Exception{
		progress.setMaximum(alignment.rows()*(alignment.rows()-1)/2);
		progress.setState(Progressable.STATE_RUNNING);
		DoubleMatrix K = new DoubleMatrix(alignment.rows(), alignment.rows());
		int count=0;
		for (int i = 0; i < alignment.rows(); i++) {
			for (int j = 0; j <= i; j++) {
				publish(count++);
				int score = alignmentScore(i, j);
				K.put(i, j, score);
				K.put(j, i, score);
			}
		}
		return K;
	}

	@Override
	protected void process(List<Integer> chunks) {
		if (!chunks.isEmpty()){
			int i = chunks.get(chunks.size()-1);
			progress.setValue(i);
		}
	}

	@Override
	protected void done() {
		progress.setState(Progressable.STATE_IDLE);
	}

	
	/**
	 * Computes the pairwise alignment score between sequences i and j
	 *
	 * @param i
	 * @param j
	 * @return
	 */
	private int alignmentScore(int index1, int index2) throws BioException{
		int score=0;
		String seq1 = alignment.getSequence(index1).getAlignedSequence();
		String seq2 = alignment.getSequence(index2).getAlignedSequence();
		
		SymbolTokenization token = subsMat.getAlphabet().getTokenization("token");
		Symbol gap = token.parseToken(String.valueOf(AlvisDataModel.GAP_CHAR));
		boolean isGapOpen=false;
		for (int i=0;i<seq1.length();i++){
			Symbol s1 = token.parseToken(seq1.substring(i, i+1));
			Symbol s2 = token.parseToken(seq2.substring(i, i+1));
			if (s1.equals(gap) && s2.equals(gap)) continue; // just ignore
			if (s1.equals(gap) || s2.equals(gap)) {
				if (!isGapOpen) { // open gap
					isGapOpen=true;
					score+=gapOpen;
				} else { // extend gap
					score+=gapExtend;
				}
			} else { // only other options is both are not gaps
				score += subsMat.getValueAt(s1, s2);
				isGapOpen=false;
			}
		}
		
		return score;
	}

	/**
	 * @return the progress
	 */
	public Progressable getProgressable() {
		return progress;
	}

	/**
	 * @return the kernelDescription
	 */
	public Nameable getKernelDescription() {
		return kernelDescription;
	}

}
