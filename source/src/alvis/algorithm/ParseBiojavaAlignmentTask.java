package alvis.algorithm;

import alvis.AlvisDataModel;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.biojava.bio.BioException;
import org.biojava.bio.alignment.Alignment;
import org.biojava.bio.alignment.SimpleAlignment;
import org.biojava.bio.seq.Sequence;
import org.biojava.bio.seq.impl.SimpleSequence;
import org.biojava.bio.symbol.FiniteAlphabet;
import org.biojava.bio.symbol.SimpleSymbolList;
import org.biojava.bio.symbol.Symbol;
import org.biojava.bio.symbol.SymbolList;
import org.biojava.utils.ChangeVetoException;
import org.biojavax.Namespace;
import org.biojavax.SimpleNamespace;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

public class ParseBiojavaAlignmentTask {
	
	private List<Sequence> order;
	private FiniteAlphabet alphabet;
	private File file;
	private Map<String, SymbolList> map;
	private XMLReader xmlReader;
	private Reader inputReader;
	
	public ParseBiojavaAlignmentTask( Reader input, FiniteAlphabet alphabet, XMLReader reader){
		this.alphabet = alphabet;
		this.inputReader = input;
		this.map = new HashMap<>();
		this.order = new ArrayList<>();
		this.xmlReader = reader;
		
	}
	public ParseBiojavaAlignmentTask( File file, FiniteAlphabet alphabet, XMLReader reader) throws FileNotFoundException {
		this(new FileReader(file), alphabet, reader);
	}
	
	public Sequence[] getSequenceOrder() {
		return order.toArray(new Sequence[1]);
	}
	
	public Alignment readAlignment() throws IOException, SAXException {
		
		ClustalHandler h = new ClustalHandler();
		xmlReader.setContentHandler( h );
		xmlReader.parse( new InputSource( this.inputReader ));

		// if sequences are of unequal length, fill up with gaps
		// first determine length
		int maxlength=0;
		for (SymbolList sl:map.values()){
			if (sl.length()>maxlength) maxlength=sl.length();
		}
		
		Symbol gapSymbol=null;
		try {
			gapSymbol = alphabet.getTokenization("token").parseToken(String.valueOf(AlvisDataModel.GAP_CHAR));
		} catch (BioException ex) {
			gapSymbol = null;
		}
		
		if (gapSymbol!=null) {
			for (String label :map.keySet()){
				SymbolList seq = map.get(label);
				if (seq.length()<maxlength) {
					SimpleSymbolList sl = new SimpleSymbolList(seq);
					while (sl.length()<maxlength){
						try {
							sl.addSymbol(gapSymbol);
						} catch (ChangeVetoException | BioException ex) {
							break;
						}
					}
					map.put(label, sl);
				}
			}
		}
		Alignment a = new SimpleAlignment(map);
		return a;
	}
	
	class ClustalHandler extends DefaultHandler {
        boolean startParse = false;
        String name = "";
        Namespace ns = null;
       // RichSequenceBuilder builder = new SimpleRichSequenceBuilder();
        
        public void startElement(String namespaceURI, String localName,
                String qName, Attributes atts) throws SAXException {
            if(localName.equals("Sequence") && !atts.getValue("sequenceName").equals("CLUSTAL")) {
                startParse = true;
    			name = atts.getValue("sequenceName");
				ns = new SimpleNamespace(namespaceURI);
	
               
            }

        }

        public void endElement(String namespaceURI, String localName,
                String qName) throws SAXException {
            if(localName.equals("Sequence")) {
                
                startParse = false;
            }

        }

        public void characters(char[] ch, int start, int length)
        throws SAXException {
            if(startParse) {
                StringBuffer c = new StringBuffer();

                c.append(ch, start, length);
                
                try {
					SymbolList list = new SimpleSymbolList(alphabet.getTokenization("token"), c.toString());
					Sequence s = new SimpleSequence(list,name,name,null);
					//(ns, name, name, 0, list, new Double(0));
                	map.put(s.getName(), s);
                	order.add(s);

				} catch (Exception e) {
					System.out.println( c);
					throw new SAXException( e.getMessage());
				}
            }
        }
    }
}
