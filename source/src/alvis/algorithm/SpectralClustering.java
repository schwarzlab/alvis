/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package alvis.algorithm;

import alvis.algorithm.matrix.JBlasClusterableAdapter;
import com.general.utils.GeneralUtils;
import com.stromberglabs.cluster.Cluster;
import com.stromberglabs.cluster.Clusterable;
import com.stromberglabs.cluster.KMeansClusterer;
import java.util.List;
import javax.swing.SwingWorker;
import org.jblas.DoubleMatrix;
import org.jblas.Eigen;
import org.jblas.MatrixFunctions;

/**
 * 		rcode for specc:
		d <- 1/sqrt(rowSums(x))
	   l <- d * x %*% diag(d)
	   xi <- eigen(l)$vectors[,1:nc]
	   yi <- xi/sqrt(rowSums(xi^2))
	   res <- kmeans(yi, centers, iterations)

 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public class SpectralClustering {
	private final DoubleMatrix matrix;
	int nclusters;
	private DoubleMatrix V;
	boolean isPrepared=false;

	public SpectralClustering(DoubleMatrix matrix){
		this(matrix,2);
	}
	
	public SpectralClustering(DoubleMatrix matrix, int nclusters){
		this.matrix=matrix;
		this.nclusters=nclusters;
	}
	
	public Cluster[] cluster() {
		if (!isPrepared){
			prepareClustering();
		}
		return doClustering();
	}
	private void prepareClustering() {
		DoubleMatrix d = matrix.rowSums();
		// some row sum are < 0, shouldn't be afaik
		d.maxi(0.1);
		MatrixFunctions.sqrti(d);
		JBlasTools.apply(d, JBlasTools.fInv);
		DoubleMatrix D = DoubleMatrix.diag(d);
		DoubleMatrix L = D.mmul(matrix.mmul(D));
		DoubleMatrix[] eig = Eigen.symmetricEigenvectors(L);
		V = eig[0]; // jblas evs are in ascending order
//		isPrepared=true;
	}

	private Cluster[] doClustering() {
		int[] selcols = GeneralUtils.range(V.columns-1, V.columns-nclusters-1, -1);
		DoubleMatrix Xi = V.getColumns(selcols);
		DoubleMatrix Xisq = MatrixFunctions.pow(Xi, 2);
		DoubleMatrix tmp = Xisq.rowSums();
		MatrixFunctions.sqrti(tmp);
		DoubleMatrix Yi = Xi.divColumnVector(tmp);
		List<Clusterable> c = new JBlasClusterableAdapter(Yi);
		KMeansClusterer kmeans = new KMeansClusterer();
		Cluster[] result = kmeans.cluster(c, nclusters);
		return result;
	}

	/**
	 * @return the nclusters
	 */
	public int getNumberOfClusters() {
		return nclusters;
	}

	/**
	 * @param nclusters the nclusters to set
	 */
	public void setNumberOfClusters(int nclusters) {
		this.nclusters = nclusters;
	}
	
	public SpectralClusteringTask createTask() {
		return new SpectralClusteringTask();
	}
	
	public class SpectralClusteringTask extends SwingWorker<Cluster[], Integer> {

		@Override
		protected Cluster[] doInBackground() throws Exception {
			return cluster();
		}

		@Override
		protected void process(List<Integer> chunks) {
			super.process(chunks); //To change body of generated methods, choose Tools | Templates.
		}

		@Override
		protected void done() {
			super.done();
		}
		
		
	}
}
