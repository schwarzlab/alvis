/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alvis.algorithm;

import alvis.AlvisDataModel;
import alvis.algorithm.BuildTreeTask.BuildTreeResults;
import com.general.gui.dialog.DialogFactory;
import com.general.gui.progress.DefaultProgressable;
import com.general.gui.progress.Progressable;
import com.traviswheeler.libs.DefaultLogger;
import com.traviswheeler.ninja.TreeBuilderManager;
import de.biozentrum.bioinformatik.sequence.Sequence;
import java.io.BufferedReader;
import java.io.StringReader;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import javax.swing.SwingWorker;
import org.jblas.DoubleMatrix;
import org.phylowidget.PhyloTree;
import org.phylowidget.tree.TreeIO;

/**
 *
 * @author Roland Schwarz <rfs32@cam.ac.uk>
 */
public class BuildTreeTask extends SwingWorker<BuildTreeResults, Integer> {

	private final AlvisDataModel dataModel;
	private DoubleMatrix K;

	public class BuildTreeResults {

		public String treeString;
		public PhyloTree tree;
	}

	final DefaultProgressable progressable;

	public BuildTreeTask(AlvisDataModel dataModel, DoubleMatrix K) {
		this.dataModel = dataModel;
		this.K = K;
		progressable = new DefaultProgressable(this, "Building tree...", true);
		progressable.setState(Progressable.STATE_RUNNING);
	}

	@Override
	protected BuildTreeResults doInBackground() throws Exception {
		BuildTreeResults results = new BuildTreeResults();
		int nseq = dataModel.getAlignment().getSequenceCount();
		String[] names = new String[nseq];
		for (int i = 0; i < nseq; i++) {
			Sequence s = dataModel.getAlignment().getSequence(i);
			String label = s.getName();
			label = label.replaceAll("'", Matcher.quoteReplacement("\'"));
			names[i] = "'" + label + "'";
		}
		float[][] distances = KernelTools.kernelToDist(K, true).toFloat().toArray2();
		TreeBuilderManager manager = new TreeBuilderManager("inmem", null, distances, names, new DefaultLogger());
		try {
			String treeString = manager.doJob();
			PhyloTree tree = new PhyloTree();
			TreeIO.parseReader(tree, new BufferedReader(new StringReader(treeString)));
			results.treeString = treeString;
			results.tree = tree;
		} catch (Exception ex) {
			DialogFactory.showErrorMessage(ex.getMessage(), "Error building tree!");
		}

		return results;
	}

	@Override
	protected void process(List<Integer> chunks) {
		super.process(chunks);
	}

	@Override
	protected void done() {
		progressable.setState(Progressable.STATE_IDLE);
	}

	/**
	 * @return the progressable
	 */
	public DefaultProgressable getProgressable() {
		return progressable;
	}

}
