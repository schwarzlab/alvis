/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alvis.algorithm;

import alvis.algorithm.matrix.FisherScoresMatrix;
import alvis.algorithm.ComputeFisherScoresTask.FisherScoresTaskResult;
import cama.BioJavaHelper;
import com.general.containerModel.DefaultNameable;
import com.general.containerModel.Nameable;
import com.general.gui.progress.DefaultProgressable;
import com.general.gui.progress.Progressable;
import com.general.utils.GeneralUtils;
import de.biozentrum.bioinformatik.alignment.MultipleAlignment;
import de.biozentrum.bioinformatik.sequence.Sequence;
import java.util.Arrays;
import java.util.List;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import org.biojava.bio.dp.ProfileHMM;
import org.biojava.bio.symbol.FiniteAlphabet;
import org.biojava.bio.symbol.SimpleSymbolList;
import org.biojava.bio.symbol.SymbolList;
import org.jblas.DoubleMatrix;
import org.jblas.MatrixFunctions;
import org.jblas.Singular;

/**
 *
 * @author Roland Schwarz <rfs32@cam.ac.uk>
 */
public class ComputeFisherScoresTask extends SwingWorker<FisherScoresTaskResult, Integer> {
	public static final Nameable FISHER_KERNEL_DESCRIPTION = new DefaultNameable("Fisher kernel", 
			"Kernel computed on the fisher scores of the profileHMM trained on the multiple alignment.\n"+
			"The kernel matrix is normalized.");

	private final ProfileHMM profileHMM;
	private final MultipleAlignment alignment;

	public class FisherScoresTaskResult {
		public FisherScoresMatrix fisherScores;
		public DoubleMatrix fisherKernel;
		public DoubleMatrix[] svd;
	}

	final DefaultProgressable progressable;

	public ComputeFisherScoresTask(ProfileHMM profileHMM, MultipleAlignment alignment) {
		this.profileHMM = profileHMM;
		this.alignment = alignment;
		progressable = new DefaultProgressable(this, "Computing Fisher scores...", false);
		progressable.setState(Progressable.STATE_RUNNING);
		progressable.setMinimum(0);
		progressable.setMaximum(alignment.getSequenceCount() - 1);
	}

	@Override
	protected FisherScoresTaskResult doInBackground() throws Exception {
		FisherScoresTaskResult results = new FisherScoresTaskResult();
		int nSequences = alignment.getSequenceCount();
		if (nSequences == 0) {
			return null;
		}
		FiniteAlphabet alphabet = BioJavaHelper.convertAlphabet(alignment.getAlphabet());
		FisherScoring fs = new FisherScoring(profileHMM, alphabet, FisherScoring.createFisherScoringParameters(new FisherScoring.LogStrategyFast()));
		FisherScoresMatrix fisherScores = fs.createFisherScoresMatrix(nSequences);

		for (int i = 0; i < nSequences; i++) {
			publish(i);
			Sequence sequence = alignment.getSequence(i);
			SymbolList ungappedSequence = new SimpleSymbolList(alphabet.getTokenization("token"), sequence.getSequence());
			fs.matchFisherVector(new SymbolList[]{ungappedSequence}, fisherScores, i);
		}
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				progressable.setStatusText("Computing Fisher kernel...");
				progressable.setIndeterminate(true);
			}
		});
		results.fisherScores = fisherScores;

		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				progressable.setStatusText("SVD...");
			}
		});
		results.svd = Singular.sparseSVD(results.fisherScores);
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				progressable.setStatusText("Computing Fisher kernel...");
			}
		});
		double thresh = 0.1;
		double[] search = new double[results.svd[1].length];
		for (int i = 0; i < search.length; i++) {
			search[i] = results.svd[1].get(search.length - i - 1);
		}
		int index = Arrays.binarySearch(search, thresh);
		index = search.length - Math.abs(index);

		int[] selectedColumns = GeneralUtils.range(0, index + 1);
		DoubleMatrix U = results.svd[0].getColumns(selectedColumns);
		DoubleMatrix S = DoubleMatrix.diag(results.svd[1].get(selectedColumns));
		MatrixFunctions.powi(S, 2);
		results.fisherKernel = U.mmul(S).mmul(U.transpose());
		results.fisherKernel = KernelTools.normalize(results.fisherKernel);
		return results;
	}

	@Override
	protected void process(List<Integer> chunks) {
		if (chunks.size() > 0) {
			int last_i = chunks.get(chunks.size() - 1);
			int first_i = chunks.get(0);
			if (first_i == 0) {
				progressable.setStatusText("Computing Fisher scores...");
			}
			progressable.setValue(last_i);
		}
	}

	@Override
	protected void done() {
		progressable.setState(Progressable.STATE_IDLE);
	}

	/**
	 * @return the progressable
	 */
	public DefaultProgressable getProgressable() {
		return progressable;
	}

}
