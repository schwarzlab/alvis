
package alvis.algorithm.prank.soap;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * Input parameters for the tool
 * 
 * <p>Java class for InputParameters complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InputParameters">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="sequence" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="data_file" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="tree_file" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="do_njtree" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="do_clustalw_tree" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="model_file" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="output_format" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="trust_insertions" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="show_insertions_with_dots" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="use_log_space" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="use_codon_model" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="translate_DNA" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="mt_translate_DNA" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="gap_rate" type="{http://www.w3.org/2001/XMLSchema}float" minOccurs="0"/>
 *         &lt;element name="gap_extension" type="{http://www.w3.org/2001/XMLSchema}float" minOccurs="0"/>
 *         &lt;element name="tn93_kappa" type="{http://www.w3.org/2001/XMLSchema}float" minOccurs="0"/>
 *         &lt;element name="tn93_rho" type="{http://www.w3.org/2001/XMLSchema}float" minOccurs="0"/>
 *         &lt;element name="guide_pairwise_distance" type="{http://www.w3.org/2001/XMLSchema}float" minOccurs="0"/>
 *         &lt;element name="max_pairwise_distance" type="{http://www.w3.org/2001/XMLSchema}float" minOccurs="0"/>
 *         &lt;element name="branch_length_scaling" type="{http://www.w3.org/2001/XMLSchema}float" minOccurs="0"/>
 *         &lt;element name="branch_length_fixed" type="{http://www.w3.org/2001/XMLSchema}float" minOccurs="0"/>
 *         &lt;element name="branch_length_maximum" type="{http://www.w3.org/2001/XMLSchema}float" minOccurs="0"/>
 *         &lt;element name="use_real_branch_lengths" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="do_no_posterior" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="run_once" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="run_twice" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="penalise_terminal_gaps" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="do_posterior_only" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="use_chaos_anchors" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="minimum_anchor_distance" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="maximum_anchor_distance" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="skip_anchor_distance" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="drop_anchor_distance" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="output_ancestors" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="noise_level" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="stay_quiet" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="random_seed" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InputParameters", propOrder = {
    "sequence",
    "dataFile",
    "treeFile",
    "doNjtree",
    "doClustalwTree",
    "modelFile",
    "outputFormat",
    "trustInsertions",
    "showInsertionsWithDots",
    "useLogSpace",
    "useCodonModel",
    "translateDNA",
    "mtTranslateDNA",
    "gapRate",
    "gapExtension",
    "tn93Kappa",
    "tn93Rho",
    "guidePairwiseDistance",
    "maxPairwiseDistance",
    "branchLengthScaling",
    "branchLengthFixed",
    "branchLengthMaximum",
    "useRealBranchLengths",
    "doNoPosterior",
    "runOnce",
    "runTwice",
    "penaliseTerminalGaps",
    "doPosteriorOnly",
    "useChaosAnchors",
    "minimumAnchorDistance",
    "maximumAnchorDistance",
    "skipAnchorDistance",
    "dropAnchorDistance",
    "outputAncestors",
    "noiseLevel",
    "stayQuiet",
    "randomSeed"
})
public class InputParameters {

    @XmlElementRef(name = "sequence", type = JAXBElement.class, required = false)
    protected JAXBElement<String> sequence;
    @XmlElementRef(name = "data_file", type = JAXBElement.class, required = false)
    protected JAXBElement<byte[]> dataFile;
    @XmlElementRef(name = "tree_file", type = JAXBElement.class, required = false)
    protected JAXBElement<byte[]> treeFile;
    @XmlElementRef(name = "do_njtree", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> doNjtree;
    @XmlElementRef(name = "do_clustalw_tree", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> doClustalwTree;
    @XmlElementRef(name = "model_file", type = JAXBElement.class, required = false)
    protected JAXBElement<byte[]> modelFile;
    @XmlElementRef(name = "output_format", type = JAXBElement.class, required = false)
    protected JAXBElement<String> outputFormat;
    @XmlElementRef(name = "trust_insertions", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> trustInsertions;
    @XmlElementRef(name = "show_insertions_with_dots", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> showInsertionsWithDots;
    @XmlElementRef(name = "use_log_space", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> useLogSpace;
    @XmlElementRef(name = "use_codon_model", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> useCodonModel;
    @XmlElementRef(name = "translate_DNA", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> translateDNA;
    @XmlElementRef(name = "mt_translate_DNA", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> mtTranslateDNA;
    @XmlElementRef(name = "gap_rate", type = JAXBElement.class, required = false)
    protected JAXBElement<Float> gapRate;
    @XmlElementRef(name = "gap_extension", type = JAXBElement.class, required = false)
    protected JAXBElement<Float> gapExtension;
    @XmlElementRef(name = "tn93_kappa", type = JAXBElement.class, required = false)
    protected JAXBElement<Float> tn93Kappa;
    @XmlElementRef(name = "tn93_rho", type = JAXBElement.class, required = false)
    protected JAXBElement<Float> tn93Rho;
    @XmlElementRef(name = "guide_pairwise_distance", type = JAXBElement.class, required = false)
    protected JAXBElement<Float> guidePairwiseDistance;
    @XmlElementRef(name = "max_pairwise_distance", type = JAXBElement.class, required = false)
    protected JAXBElement<Float> maxPairwiseDistance;
    @XmlElementRef(name = "branch_length_scaling", type = JAXBElement.class, required = false)
    protected JAXBElement<Float> branchLengthScaling;
    @XmlElementRef(name = "branch_length_fixed", type = JAXBElement.class, required = false)
    protected JAXBElement<Float> branchLengthFixed;
    @XmlElementRef(name = "branch_length_maximum", type = JAXBElement.class, required = false)
    protected JAXBElement<Float> branchLengthMaximum;
    @XmlElementRef(name = "use_real_branch_lengths", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> useRealBranchLengths;
    @XmlElementRef(name = "do_no_posterior", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> doNoPosterior;
    @XmlElementRef(name = "run_once", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> runOnce;
    @XmlElementRef(name = "run_twice", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> runTwice;
    @XmlElementRef(name = "penalise_terminal_gaps", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> penaliseTerminalGaps;
    @XmlElementRef(name = "do_posterior_only", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> doPosteriorOnly;
    @XmlElementRef(name = "use_chaos_anchors", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> useChaosAnchors;
    @XmlElementRef(name = "minimum_anchor_distance", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> minimumAnchorDistance;
    @XmlElementRef(name = "maximum_anchor_distance", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> maximumAnchorDistance;
    @XmlElementRef(name = "skip_anchor_distance", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> skipAnchorDistance;
    @XmlElementRef(name = "drop_anchor_distance", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> dropAnchorDistance;
    @XmlElementRef(name = "output_ancestors", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> outputAncestors;
    @XmlElementRef(name = "noise_level", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> noiseLevel;
    @XmlElementRef(name = "stay_quiet", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> stayQuiet;
    @XmlElementRef(name = "random_seed", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> randomSeed;

    /**
     * Gets the value of the sequence property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSequence() {
        return sequence;
    }

    /**
     * Sets the value of the sequence property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSequence(JAXBElement<String> value) {
        this.sequence = value;
    }

    /**
     * Gets the value of the dataFile property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link byte[]}{@code >}
     *     
     */
    public JAXBElement<byte[]> getDataFile() {
        return dataFile;
    }

    /**
     * Sets the value of the dataFile property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link byte[]}{@code >}
     *     
     */
    public void setDataFile(JAXBElement<byte[]> value) {
        this.dataFile = value;
    }

    /**
     * Gets the value of the treeFile property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link byte[]}{@code >}
     *     
     */
    public JAXBElement<byte[]> getTreeFile() {
        return treeFile;
    }

    /**
     * Sets the value of the treeFile property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link byte[]}{@code >}
     *     
     */
    public void setTreeFile(JAXBElement<byte[]> value) {
        this.treeFile = value;
    }

    /**
     * Gets the value of the doNjtree property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getDoNjtree() {
        return doNjtree;
    }

    /**
     * Sets the value of the doNjtree property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setDoNjtree(JAXBElement<Boolean> value) {
        this.doNjtree = value;
    }

    /**
     * Gets the value of the doClustalwTree property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getDoClustalwTree() {
        return doClustalwTree;
    }

    /**
     * Sets the value of the doClustalwTree property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setDoClustalwTree(JAXBElement<Boolean> value) {
        this.doClustalwTree = value;
    }

    /**
     * Gets the value of the modelFile property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link byte[]}{@code >}
     *     
     */
    public JAXBElement<byte[]> getModelFile() {
        return modelFile;
    }

    /**
     * Sets the value of the modelFile property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link byte[]}{@code >}
     *     
     */
    public void setModelFile(JAXBElement<byte[]> value) {
        this.modelFile = value;
    }

    /**
     * Gets the value of the outputFormat property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOutputFormat() {
        return outputFormat;
    }

    /**
     * Sets the value of the outputFormat property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOutputFormat(JAXBElement<String> value) {
        this.outputFormat = value;
    }

    /**
     * Gets the value of the trustInsertions property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getTrustInsertions() {
        return trustInsertions;
    }

    /**
     * Sets the value of the trustInsertions property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setTrustInsertions(JAXBElement<Boolean> value) {
        this.trustInsertions = value;
    }

    /**
     * Gets the value of the showInsertionsWithDots property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getShowInsertionsWithDots() {
        return showInsertionsWithDots;
    }

    /**
     * Sets the value of the showInsertionsWithDots property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setShowInsertionsWithDots(JAXBElement<Boolean> value) {
        this.showInsertionsWithDots = value;
    }

    /**
     * Gets the value of the useLogSpace property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getUseLogSpace() {
        return useLogSpace;
    }

    /**
     * Sets the value of the useLogSpace property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setUseLogSpace(JAXBElement<Boolean> value) {
        this.useLogSpace = value;
    }

    /**
     * Gets the value of the useCodonModel property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getUseCodonModel() {
        return useCodonModel;
    }

    /**
     * Sets the value of the useCodonModel property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setUseCodonModel(JAXBElement<Boolean> value) {
        this.useCodonModel = value;
    }

    /**
     * Gets the value of the translateDNA property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getTranslateDNA() {
        return translateDNA;
    }

    /**
     * Sets the value of the translateDNA property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setTranslateDNA(JAXBElement<Boolean> value) {
        this.translateDNA = value;
    }

    /**
     * Gets the value of the mtTranslateDNA property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getMtTranslateDNA() {
        return mtTranslateDNA;
    }

    /**
     * Sets the value of the mtTranslateDNA property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setMtTranslateDNA(JAXBElement<Boolean> value) {
        this.mtTranslateDNA = value;
    }

    /**
     * Gets the value of the gapRate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Float }{@code >}
     *     
     */
    public JAXBElement<Float> getGapRate() {
        return gapRate;
    }

    /**
     * Sets the value of the gapRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Float }{@code >}
     *     
     */
    public void setGapRate(JAXBElement<Float> value) {
        this.gapRate = value;
    }

    /**
     * Gets the value of the gapExtension property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Float }{@code >}
     *     
     */
    public JAXBElement<Float> getGapExtension() {
        return gapExtension;
    }

    /**
     * Sets the value of the gapExtension property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Float }{@code >}
     *     
     */
    public void setGapExtension(JAXBElement<Float> value) {
        this.gapExtension = value;
    }

    /**
     * Gets the value of the tn93Kappa property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Float }{@code >}
     *     
     */
    public JAXBElement<Float> getTn93Kappa() {
        return tn93Kappa;
    }

    /**
     * Sets the value of the tn93Kappa property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Float }{@code >}
     *     
     */
    public void setTn93Kappa(JAXBElement<Float> value) {
        this.tn93Kappa = value;
    }

    /**
     * Gets the value of the tn93Rho property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Float }{@code >}
     *     
     */
    public JAXBElement<Float> getTn93Rho() {
        return tn93Rho;
    }

    /**
     * Sets the value of the tn93Rho property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Float }{@code >}
     *     
     */
    public void setTn93Rho(JAXBElement<Float> value) {
        this.tn93Rho = value;
    }

    /**
     * Gets the value of the guidePairwiseDistance property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Float }{@code >}
     *     
     */
    public JAXBElement<Float> getGuidePairwiseDistance() {
        return guidePairwiseDistance;
    }

    /**
     * Sets the value of the guidePairwiseDistance property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Float }{@code >}
     *     
     */
    public void setGuidePairwiseDistance(JAXBElement<Float> value) {
        this.guidePairwiseDistance = value;
    }

    /**
     * Gets the value of the maxPairwiseDistance property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Float }{@code >}
     *     
     */
    public JAXBElement<Float> getMaxPairwiseDistance() {
        return maxPairwiseDistance;
    }

    /**
     * Sets the value of the maxPairwiseDistance property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Float }{@code >}
     *     
     */
    public void setMaxPairwiseDistance(JAXBElement<Float> value) {
        this.maxPairwiseDistance = value;
    }

    /**
     * Gets the value of the branchLengthScaling property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Float }{@code >}
     *     
     */
    public JAXBElement<Float> getBranchLengthScaling() {
        return branchLengthScaling;
    }

    /**
     * Sets the value of the branchLengthScaling property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Float }{@code >}
     *     
     */
    public void setBranchLengthScaling(JAXBElement<Float> value) {
        this.branchLengthScaling = value;
    }

    /**
     * Gets the value of the branchLengthFixed property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Float }{@code >}
     *     
     */
    public JAXBElement<Float> getBranchLengthFixed() {
        return branchLengthFixed;
    }

    /**
     * Sets the value of the branchLengthFixed property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Float }{@code >}
     *     
     */
    public void setBranchLengthFixed(JAXBElement<Float> value) {
        this.branchLengthFixed = value;
    }

    /**
     * Gets the value of the branchLengthMaximum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Float }{@code >}
     *     
     */
    public JAXBElement<Float> getBranchLengthMaximum() {
        return branchLengthMaximum;
    }

    /**
     * Sets the value of the branchLengthMaximum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Float }{@code >}
     *     
     */
    public void setBranchLengthMaximum(JAXBElement<Float> value) {
        this.branchLengthMaximum = value;
    }

    /**
     * Gets the value of the useRealBranchLengths property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getUseRealBranchLengths() {
        return useRealBranchLengths;
    }

    /**
     * Sets the value of the useRealBranchLengths property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setUseRealBranchLengths(JAXBElement<Boolean> value) {
        this.useRealBranchLengths = value;
    }

    /**
     * Gets the value of the doNoPosterior property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getDoNoPosterior() {
        return doNoPosterior;
    }

    /**
     * Sets the value of the doNoPosterior property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setDoNoPosterior(JAXBElement<Boolean> value) {
        this.doNoPosterior = value;
    }

    /**
     * Gets the value of the runOnce property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getRunOnce() {
        return runOnce;
    }

    /**
     * Sets the value of the runOnce property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setRunOnce(JAXBElement<Boolean> value) {
        this.runOnce = value;
    }

    /**
     * Gets the value of the runTwice property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getRunTwice() {
        return runTwice;
    }

    /**
     * Sets the value of the runTwice property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setRunTwice(JAXBElement<Boolean> value) {
        this.runTwice = value;
    }

    /**
     * Gets the value of the penaliseTerminalGaps property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getPenaliseTerminalGaps() {
        return penaliseTerminalGaps;
    }

    /**
     * Sets the value of the penaliseTerminalGaps property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setPenaliseTerminalGaps(JAXBElement<Boolean> value) {
        this.penaliseTerminalGaps = value;
    }

    /**
     * Gets the value of the doPosteriorOnly property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getDoPosteriorOnly() {
        return doPosteriorOnly;
    }

    /**
     * Sets the value of the doPosteriorOnly property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setDoPosteriorOnly(JAXBElement<Boolean> value) {
        this.doPosteriorOnly = value;
    }

    /**
     * Gets the value of the useChaosAnchors property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getUseChaosAnchors() {
        return useChaosAnchors;
    }

    /**
     * Sets the value of the useChaosAnchors property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setUseChaosAnchors(JAXBElement<Boolean> value) {
        this.useChaosAnchors = value;
    }

    /**
     * Gets the value of the minimumAnchorDistance property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getMinimumAnchorDistance() {
        return minimumAnchorDistance;
    }

    /**
     * Sets the value of the minimumAnchorDistance property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setMinimumAnchorDistance(JAXBElement<Integer> value) {
        this.minimumAnchorDistance = value;
    }

    /**
     * Gets the value of the maximumAnchorDistance property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getMaximumAnchorDistance() {
        return maximumAnchorDistance;
    }

    /**
     * Sets the value of the maximumAnchorDistance property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setMaximumAnchorDistance(JAXBElement<Integer> value) {
        this.maximumAnchorDistance = value;
    }

    /**
     * Gets the value of the skipAnchorDistance property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getSkipAnchorDistance() {
        return skipAnchorDistance;
    }

    /**
     * Sets the value of the skipAnchorDistance property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setSkipAnchorDistance(JAXBElement<Integer> value) {
        this.skipAnchorDistance = value;
    }

    /**
     * Gets the value of the dropAnchorDistance property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getDropAnchorDistance() {
        return dropAnchorDistance;
    }

    /**
     * Sets the value of the dropAnchorDistance property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setDropAnchorDistance(JAXBElement<Integer> value) {
        this.dropAnchorDistance = value;
    }

    /**
     * Gets the value of the outputAncestors property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getOutputAncestors() {
        return outputAncestors;
    }

    /**
     * Sets the value of the outputAncestors property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setOutputAncestors(JAXBElement<Boolean> value) {
        this.outputAncestors = value;
    }

    /**
     * Gets the value of the noiseLevel property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getNoiseLevel() {
        return noiseLevel;
    }

    /**
     * Sets the value of the noiseLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setNoiseLevel(JAXBElement<Integer> value) {
        this.noiseLevel = value;
    }

    /**
     * Gets the value of the stayQuiet property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getStayQuiet() {
        return stayQuiet;
    }

    /**
     * Sets the value of the stayQuiet property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setStayQuiet(JAXBElement<Boolean> value) {
        this.stayQuiet = value;
    }

    /**
     * Gets the value of the randomSeed property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getRandomSeed() {
        return randomSeed;
    }

    /**
     * Sets the value of the randomSeed property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setRandomSeed(JAXBElement<Integer> value) {
        this.randomSeed = value;
    }

}
