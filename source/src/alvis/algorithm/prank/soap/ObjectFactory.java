
package alvis.algorithm.prank.soap;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the alvis.algorithms.prank.soap package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _RunTitle_QNAME = new QName("", "title");
    private final static QName _WsResultTypeDescription_QNAME = new QName("", "description");
    private final static QName _WsResultTypeLabel_QNAME = new QName("", "label");
    private final static QName _GetResultParameters_QNAME = new QName("", "parameters");
    private final static QName _InputParametersDoNjtree_QNAME = new QName("", "do_njtree");
    private final static QName _InputParametersBranchLengthMaximum_QNAME = new QName("", "branch_length_maximum");
    private final static QName _InputParametersMaxPairwiseDistance_QNAME = new QName("", "max_pairwise_distance");
    private final static QName _InputParametersDoNoPosterior_QNAME = new QName("", "do_no_posterior");
    private final static QName _InputParametersStayQuiet_QNAME = new QName("", "stay_quiet");
    private final static QName _InputParametersUseCodonModel_QNAME = new QName("", "use_codon_model");
    private final static QName _InputParametersDataFile_QNAME = new QName("", "data_file");
    private final static QName _InputParametersGapRate_QNAME = new QName("", "gap_rate");
    private final static QName _InputParametersOutputFormat_QNAME = new QName("", "output_format");
    private final static QName _InputParametersOutputAncestors_QNAME = new QName("", "output_ancestors");
    private final static QName _InputParametersModelFile_QNAME = new QName("", "model_file");
    private final static QName _InputParametersSkipAnchorDistance_QNAME = new QName("", "skip_anchor_distance");
    private final static QName _InputParametersTn93Rho_QNAME = new QName("", "tn93_rho");
    private final static QName _InputParametersBranchLengthScaling_QNAME = new QName("", "branch_length_scaling");
    private final static QName _InputParametersBranchLengthFixed_QNAME = new QName("", "branch_length_fixed");
    private final static QName _InputParametersRunOnce_QNAME = new QName("", "run_once");
    private final static QName _InputParametersRandomSeed_QNAME = new QName("", "random_seed");
    private final static QName _InputParametersTranslateDNA_QNAME = new QName("", "translate_DNA");
    private final static QName _InputParametersRunTwice_QNAME = new QName("", "run_twice");
    private final static QName _InputParametersGapExtension_QNAME = new QName("", "gap_extension");
    private final static QName _InputParametersPenaliseTerminalGaps_QNAME = new QName("", "penalise_terminal_gaps");
    private final static QName _InputParametersNoiseLevel_QNAME = new QName("", "noise_level");
    private final static QName _InputParametersDropAnchorDistance_QNAME = new QName("", "drop_anchor_distance");
    private final static QName _InputParametersTreeFile_QNAME = new QName("", "tree_file");
    private final static QName _InputParametersShowInsertionsWithDots_QNAME = new QName("", "show_insertions_with_dots");
    private final static QName _InputParametersTn93Kappa_QNAME = new QName("", "tn93_kappa");
    private final static QName _InputParametersMinimumAnchorDistance_QNAME = new QName("", "minimum_anchor_distance");
    private final static QName _InputParametersGuidePairwiseDistance_QNAME = new QName("", "guide_pairwise_distance");
    private final static QName _InputParametersDoPosteriorOnly_QNAME = new QName("", "do_posterior_only");
    private final static QName _InputParametersMtTranslateDNA_QNAME = new QName("", "mt_translate_DNA");
    private final static QName _InputParametersMaximumAnchorDistance_QNAME = new QName("", "maximum_anchor_distance");
    private final static QName _InputParametersSequence_QNAME = new QName("", "sequence");
    private final static QName _InputParametersTrustInsertions_QNAME = new QName("", "trust_insertions");
    private final static QName _InputParametersUseRealBranchLengths_QNAME = new QName("", "use_real_branch_lengths");
    private final static QName _InputParametersUseLogSpace_QNAME = new QName("", "use_log_space");
    private final static QName _InputParametersUseChaosAnchors_QNAME = new QName("", "use_chaos_anchors");
    private final static QName _InputParametersDoClustalwTree_QNAME = new QName("", "do_clustalw_tree");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: alvis.algorithms.prank.soap
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetParameters }
     * 
     */
    public GetParameters createGetParameters() {
        return new GetParameters();
    }

    /**
     * Create an instance of {@link GetStatus }
     * 
     */
    public GetStatus createGetStatus() {
        return new GetStatus();
    }

    /**
     * Create an instance of {@link GetParametersResponse }
     * 
     */
    public GetParametersResponse createGetParametersResponse() {
        return new GetParametersResponse();
    }

    /**
     * Create an instance of {@link WsParameters }
     * 
     */
    public WsParameters createWsParameters() {
        return new WsParameters();
    }

    /**
     * Create an instance of {@link GetResultTypes }
     * 
     */
    public GetResultTypes createGetResultTypes() {
        return new GetResultTypes();
    }

    /**
     * Create an instance of {@link GetStatusResponse }
     * 
     */
    public GetStatusResponse createGetStatusResponse() {
        return new GetStatusResponse();
    }

    /**
     * Create an instance of {@link GetResultResponse }
     * 
     */
    public GetResultResponse createGetResultResponse() {
        return new GetResultResponse();
    }

    /**
     * Create an instance of {@link GetResultTypesResponse }
     * 
     */
    public GetResultTypesResponse createGetResultTypesResponse() {
        return new GetResultTypesResponse();
    }

    /**
     * Create an instance of {@link WsResultTypes }
     * 
     */
    public WsResultTypes createWsResultTypes() {
        return new WsResultTypes();
    }

    /**
     * Create an instance of {@link GetParameterDetailsResponse }
     * 
     */
    public GetParameterDetailsResponse createGetParameterDetailsResponse() {
        return new GetParameterDetailsResponse();
    }

    /**
     * Create an instance of {@link WsParameterDetails }
     * 
     */
    public WsParameterDetails createWsParameterDetails() {
        return new WsParameterDetails();
    }

    /**
     * Create an instance of {@link RunResponse }
     * 
     */
    public RunResponse createRunResponse() {
        return new RunResponse();
    }

    /**
     * Create an instance of {@link GetResult }
     * 
     */
    public GetResult createGetResult() {
        return new GetResult();
    }

    /**
     * Create an instance of {@link WsRawOutputParameters }
     * 
     */
    public WsRawOutputParameters createWsRawOutputParameters() {
        return new WsRawOutputParameters();
    }

    /**
     * Create an instance of {@link Run }
     * 
     */
    public Run createRun() {
        return new Run();
    }

    /**
     * Create an instance of {@link InputParameters }
     * 
     */
    public InputParameters createInputParameters() {
        return new InputParameters();
    }

    /**
     * Create an instance of {@link GetParameterDetails }
     * 
     */
    public GetParameterDetails createGetParameterDetails() {
        return new GetParameterDetails();
    }

    /**
     * Create an instance of {@link WsProperty }
     * 
     */
    public WsProperty createWsProperty() {
        return new WsProperty();
    }

    /**
     * Create an instance of {@link WsProperties }
     * 
     */
    public WsProperties createWsProperties() {
        return new WsProperties();
    }

    /**
     * Create an instance of {@link WsRawOutputParameter }
     * 
     */
    public WsRawOutputParameter createWsRawOutputParameter() {
        return new WsRawOutputParameter();
    }

    /**
     * Create an instance of {@link WsParameterValues }
     * 
     */
    public WsParameterValues createWsParameterValues() {
        return new WsParameterValues();
    }

    /**
     * Create an instance of {@link WsParameterValue }
     * 
     */
    public WsParameterValue createWsParameterValue() {
        return new WsParameterValue();
    }

    /**
     * Create an instance of {@link ArrayOfString }
     * 
     */
    public ArrayOfString createArrayOfString() {
        return new ArrayOfString();
    }

    /**
     * Create an instance of {@link WsResultType }
     * 
     */
    public WsResultType createWsResultType() {
        return new WsResultType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "title", scope = Run.class)
    public JAXBElement<String> createRunTitle(String value) {
        return new JAXBElement<String>(_RunTitle_QNAME, String.class, Run.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "description", scope = WsResultType.class)
    public JAXBElement<String> createWsResultTypeDescription(String value) {
        return new JAXBElement<String>(_WsResultTypeDescription_QNAME, String.class, WsResultType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "label", scope = WsResultType.class)
    public JAXBElement<String> createWsResultTypeLabel(String value) {
        return new JAXBElement<String>(_WsResultTypeLabel_QNAME, String.class, WsResultType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WsRawOutputParameters }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "parameters", scope = GetResult.class)
    public JAXBElement<WsRawOutputParameters> createGetResultParameters(WsRawOutputParameters value) {
        return new JAXBElement<WsRawOutputParameters>(_GetResultParameters_QNAME, WsRawOutputParameters.class, GetResult.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "do_njtree", scope = InputParameters.class)
    public JAXBElement<Boolean> createInputParametersDoNjtree(Boolean value) {
        return new JAXBElement<Boolean>(_InputParametersDoNjtree_QNAME, Boolean.class, InputParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Float }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "branch_length_maximum", scope = InputParameters.class)
    public JAXBElement<Float> createInputParametersBranchLengthMaximum(Float value) {
        return new JAXBElement<Float>(_InputParametersBranchLengthMaximum_QNAME, Float.class, InputParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Float }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "max_pairwise_distance", scope = InputParameters.class)
    public JAXBElement<Float> createInputParametersMaxPairwiseDistance(Float value) {
        return new JAXBElement<Float>(_InputParametersMaxPairwiseDistance_QNAME, Float.class, InputParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "do_no_posterior", scope = InputParameters.class)
    public JAXBElement<Boolean> createInputParametersDoNoPosterior(Boolean value) {
        return new JAXBElement<Boolean>(_InputParametersDoNoPosterior_QNAME, Boolean.class, InputParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "stay_quiet", scope = InputParameters.class)
    public JAXBElement<Boolean> createInputParametersStayQuiet(Boolean value) {
        return new JAXBElement<Boolean>(_InputParametersStayQuiet_QNAME, Boolean.class, InputParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "use_codon_model", scope = InputParameters.class)
    public JAXBElement<Boolean> createInputParametersUseCodonModel(Boolean value) {
        return new JAXBElement<Boolean>(_InputParametersUseCodonModel_QNAME, Boolean.class, InputParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "data_file", scope = InputParameters.class)
    public JAXBElement<byte[]> createInputParametersDataFile(byte[] value) {
        return new JAXBElement<byte[]>(_InputParametersDataFile_QNAME, byte[].class, InputParameters.class, ((byte[]) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Float }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "gap_rate", scope = InputParameters.class)
    public JAXBElement<Float> createInputParametersGapRate(Float value) {
        return new JAXBElement<Float>(_InputParametersGapRate_QNAME, Float.class, InputParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "output_format", scope = InputParameters.class)
    public JAXBElement<String> createInputParametersOutputFormat(String value) {
        return new JAXBElement<String>(_InputParametersOutputFormat_QNAME, String.class, InputParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "output_ancestors", scope = InputParameters.class)
    public JAXBElement<Boolean> createInputParametersOutputAncestors(Boolean value) {
        return new JAXBElement<Boolean>(_InputParametersOutputAncestors_QNAME, Boolean.class, InputParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "model_file", scope = InputParameters.class)
    public JAXBElement<byte[]> createInputParametersModelFile(byte[] value) {
        return new JAXBElement<byte[]>(_InputParametersModelFile_QNAME, byte[].class, InputParameters.class, ((byte[]) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "skip_anchor_distance", scope = InputParameters.class)
    public JAXBElement<Integer> createInputParametersSkipAnchorDistance(Integer value) {
        return new JAXBElement<Integer>(_InputParametersSkipAnchorDistance_QNAME, Integer.class, InputParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Float }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tn93_rho", scope = InputParameters.class)
    public JAXBElement<Float> createInputParametersTn93Rho(Float value) {
        return new JAXBElement<Float>(_InputParametersTn93Rho_QNAME, Float.class, InputParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Float }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "branch_length_scaling", scope = InputParameters.class)
    public JAXBElement<Float> createInputParametersBranchLengthScaling(Float value) {
        return new JAXBElement<Float>(_InputParametersBranchLengthScaling_QNAME, Float.class, InputParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Float }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "branch_length_fixed", scope = InputParameters.class)
    public JAXBElement<Float> createInputParametersBranchLengthFixed(Float value) {
        return new JAXBElement<Float>(_InputParametersBranchLengthFixed_QNAME, Float.class, InputParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "run_once", scope = InputParameters.class)
    public JAXBElement<Boolean> createInputParametersRunOnce(Boolean value) {
        return new JAXBElement<Boolean>(_InputParametersRunOnce_QNAME, Boolean.class, InputParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "random_seed", scope = InputParameters.class)
    public JAXBElement<Integer> createInputParametersRandomSeed(Integer value) {
        return new JAXBElement<Integer>(_InputParametersRandomSeed_QNAME, Integer.class, InputParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "translate_DNA", scope = InputParameters.class)
    public JAXBElement<Boolean> createInputParametersTranslateDNA(Boolean value) {
        return new JAXBElement<Boolean>(_InputParametersTranslateDNA_QNAME, Boolean.class, InputParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "run_twice", scope = InputParameters.class)
    public JAXBElement<Boolean> createInputParametersRunTwice(Boolean value) {
        return new JAXBElement<Boolean>(_InputParametersRunTwice_QNAME, Boolean.class, InputParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Float }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "gap_extension", scope = InputParameters.class)
    public JAXBElement<Float> createInputParametersGapExtension(Float value) {
        return new JAXBElement<Float>(_InputParametersGapExtension_QNAME, Float.class, InputParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "penalise_terminal_gaps", scope = InputParameters.class)
    public JAXBElement<Boolean> createInputParametersPenaliseTerminalGaps(Boolean value) {
        return new JAXBElement<Boolean>(_InputParametersPenaliseTerminalGaps_QNAME, Boolean.class, InputParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "noise_level", scope = InputParameters.class)
    public JAXBElement<Integer> createInputParametersNoiseLevel(Integer value) {
        return new JAXBElement<Integer>(_InputParametersNoiseLevel_QNAME, Integer.class, InputParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "drop_anchor_distance", scope = InputParameters.class)
    public JAXBElement<Integer> createInputParametersDropAnchorDistance(Integer value) {
        return new JAXBElement<Integer>(_InputParametersDropAnchorDistance_QNAME, Integer.class, InputParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tree_file", scope = InputParameters.class)
    public JAXBElement<byte[]> createInputParametersTreeFile(byte[] value) {
        return new JAXBElement<byte[]>(_InputParametersTreeFile_QNAME, byte[].class, InputParameters.class, ((byte[]) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "show_insertions_with_dots", scope = InputParameters.class)
    public JAXBElement<Boolean> createInputParametersShowInsertionsWithDots(Boolean value) {
        return new JAXBElement<Boolean>(_InputParametersShowInsertionsWithDots_QNAME, Boolean.class, InputParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Float }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tn93_kappa", scope = InputParameters.class)
    public JAXBElement<Float> createInputParametersTn93Kappa(Float value) {
        return new JAXBElement<Float>(_InputParametersTn93Kappa_QNAME, Float.class, InputParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "minimum_anchor_distance", scope = InputParameters.class)
    public JAXBElement<Integer> createInputParametersMinimumAnchorDistance(Integer value) {
        return new JAXBElement<Integer>(_InputParametersMinimumAnchorDistance_QNAME, Integer.class, InputParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Float }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "guide_pairwise_distance", scope = InputParameters.class)
    public JAXBElement<Float> createInputParametersGuidePairwiseDistance(Float value) {
        return new JAXBElement<Float>(_InputParametersGuidePairwiseDistance_QNAME, Float.class, InputParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "do_posterior_only", scope = InputParameters.class)
    public JAXBElement<Boolean> createInputParametersDoPosteriorOnly(Boolean value) {
        return new JAXBElement<Boolean>(_InputParametersDoPosteriorOnly_QNAME, Boolean.class, InputParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "mt_translate_DNA", scope = InputParameters.class)
    public JAXBElement<Boolean> createInputParametersMtTranslateDNA(Boolean value) {
        return new JAXBElement<Boolean>(_InputParametersMtTranslateDNA_QNAME, Boolean.class, InputParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "maximum_anchor_distance", scope = InputParameters.class)
    public JAXBElement<Integer> createInputParametersMaximumAnchorDistance(Integer value) {
        return new JAXBElement<Integer>(_InputParametersMaximumAnchorDistance_QNAME, Integer.class, InputParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "sequence", scope = InputParameters.class)
    public JAXBElement<String> createInputParametersSequence(String value) {
        return new JAXBElement<String>(_InputParametersSequence_QNAME, String.class, InputParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "trust_insertions", scope = InputParameters.class)
    public JAXBElement<Boolean> createInputParametersTrustInsertions(Boolean value) {
        return new JAXBElement<Boolean>(_InputParametersTrustInsertions_QNAME, Boolean.class, InputParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "use_real_branch_lengths", scope = InputParameters.class)
    public JAXBElement<Boolean> createInputParametersUseRealBranchLengths(Boolean value) {
        return new JAXBElement<Boolean>(_InputParametersUseRealBranchLengths_QNAME, Boolean.class, InputParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "use_log_space", scope = InputParameters.class)
    public JAXBElement<Boolean> createInputParametersUseLogSpace(Boolean value) {
        return new JAXBElement<Boolean>(_InputParametersUseLogSpace_QNAME, Boolean.class, InputParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "use_chaos_anchors", scope = InputParameters.class)
    public JAXBElement<Boolean> createInputParametersUseChaosAnchors(Boolean value) {
        return new JAXBElement<Boolean>(_InputParametersUseChaosAnchors_QNAME, Boolean.class, InputParameters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "do_clustalw_tree", scope = InputParameters.class)
    public JAXBElement<Boolean> createInputParametersDoClustalwTree(Boolean value) {
        return new JAXBElement<Boolean>(_InputParametersDoClustalwTree_QNAME, Boolean.class, InputParameters.class, value);
    }

}
