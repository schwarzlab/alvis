/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alvis.algorithm;

import cama.CAMADataProvider;
import com.general.gui.progress.DefaultProgressable;
import com.general.gui.progress.Progressable;
import de.biozentrum.bioinformatik.ca.CADataModel;
import de.biozentrum.bioinformatik.ca.CorrespondenceAnalysis;
import java.util.List;
import javax.swing.SwingWorker;

/**
 *
 * @author Roland Schwarz <rfs32@cam.ac.uk>
 */
public class CorrespondanceAnalysisTask extends SwingWorker<CADataModel, Integer> {

	final DefaultProgressable progressable;
	private final CAMADataProvider dataProvider;
	private final CorrespondenceAnalysis ca;

	public CorrespondanceAnalysisTask(CAMADataProvider dataProvider) {
		this.dataProvider = dataProvider;
		this.ca = new CorrespondenceAnalysis(this.dataProvider);
		progressable = new DefaultProgressable(this, "Correspondence Analysis...", true);
	}

	@Override
	protected CADataModel doInBackground() throws Exception {
		progressable.setState(Progressable.STATE_RUNNING);
		return ca.performCA();
	}

	@Override
	protected void process(List<Integer> chunks) {
		if (chunks.size() > 0) {
			int last_i = chunks.get(chunks.size() - 1);
			int first_i = chunks.get(0);
			if (first_i == 0) {
				progressable.setStatusText("Correspondance analysis...");
			}
			progressable.setValue(last_i);
		}
	}

	@Override
	protected void done() {
		progressable.setState(Progressable.STATE_IDLE);
	}

	/**
	 * @return the progressable
	 */
	public DefaultProgressable getProgressable() {
		return progressable;
	}

}
