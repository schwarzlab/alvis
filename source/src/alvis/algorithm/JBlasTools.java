/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package alvis.algorithm;

import org.jblas.DoubleFunction;
import org.jblas.DoubleMatrix;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public class JBlasTools {
	public static final DoubleFunction fInv = new fInv();
	/**
	 * Applies function f to each element in M. M is changed in place.
	 * 
	 * @param M
	 * @param f
	 * @return  This for convenience only
	 */
	public static DoubleMatrix apply(DoubleMatrix M, DoubleFunction f){
		for (int i=0;i<M.rows;i++){
			for (int j=0;j<M.columns;j++){
				M.put(i, j, f.compute(M.get(i,j)));
			}
		}
	
		return M;
	}
	
	public static class fInv implements DoubleFunction {
		@Override
		public double compute(double x) {
			return 1.0/x;
		}
		
	}
}
