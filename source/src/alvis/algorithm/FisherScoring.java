package alvis.algorithm;

import alvis.algorithm.matrix.FisherScoresMatrix;
import cama.BioJavaHelper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.biojava.bio.dp.ProfileHMM;
import org.biojava.bio.symbol.AlphabetIndex;
import org.biojava.bio.symbol.AlphabetManager;
import org.biojava.bio.symbol.FiniteAlphabet;
import org.biojava.bio.symbol.IllegalSymbolException;
import org.biojava.bio.symbol.Symbol;
import org.biojava.bio.symbol.SymbolList;
import org.jblas.DoubleFunction;
import org.jblas.DoubleMatrix;

public class FisherScoring {
	public interface LogStrategy {
		public double log(double val);
		public double exp(double val);
	}
	
	public interface FisherScoringParameters { 
		public LogStrategy getLogStrategy();
	}
	
	public static class LogStrategyExact implements LogStrategy {
		@Override
		public double exp(double val) {
			return Math.exp(val);
		}

		@Override
		public double log(double val) {
			return Math.log(val);
		}
	}
	
	public static class LogStrategyFast implements LogStrategy {
		@Override
		public double exp(double val) {
			if (val < -700) { return 0; }
			final long tmp = (long) (1512775 * val + 1072632447);
		    return Double.longBitsToDouble(tmp << 32);
		}

		@Override
		public double log(double val) {
		    return 6 * (val - 1) / (val + 1 + 4 * (Math.sqrt(val)));			
		}
	}
	
	public static FisherScoringParameters createFisherScoringParameters(final LogStrategy logStrategy) {
		return new FisherScoringParameters() {
			@Override
			public LogStrategy getLogStrategy() {
				return logStrategy;
			}
		};
	}
	
	public static FisherScoringParameters createDefaultFisherScoringParameters() {
		return createFisherScoringParameters(new LogStrategyExact());
	}

	
	private class LogSpace {
		private class LogFunction implements DoubleFunction {
			@Override
			public double compute(double p1) {
				return toLog(p1);
			}
		}
		
		private class RealFunction implements DoubleFunction {
			@Override
			public double compute(double p1) {
				return toReal(p1);
			}
			
		}
		
		public double ZERO = Double.MAX_VALUE;
		public LogFunction LOG = new LogFunction();
		public RealFunction REAL = new RealFunction();
		public LogStrategy logStrategy = null;
		
		public LogSpace(LogStrategy strategy) {
			this.logStrategy = strategy;
		}
		
		public double log(double val) {
			//return Math.log(val);
			return logStrategy.log(val);
		}
		
		public double exp(double val) {
			//return Math.exp(val);
			return logStrategy.exp(val);
		}
		
		public double sum(double p1, double p2) {
			// Fink 2003, Mustererkennung mit Markovmodellen, p. 121
			//assert(p1 > 0 && p2 > 0);
			//double larger = Math.max(p1, p2);
			//double smaller = Math.min(p1, p2);
			double larger;
			double smaller;
			if (p1 > p2) {
				larger = p1;
				smaller = p2;
			} else {
				larger = p2;
				smaller = p1;
			}
			
			return smaller - log(1 + exp( - (larger - smaller)));
			//return smaller - Math.log1p(fast_exp( - (larger - smaller)));
//			return smaller - Math.log1p(exp( smaller - larger ));
		}
		
		public double toLog(double p1) {
			assert(p1 >= 0);
			if (p1 == 0) {
				return ZERO;
			} else {
				return -log(p1);
			}
		}
		
		public double toReal(double p1) {
			return exp(-p1);
		}
		
	}

	// parameters
	FisherScoringParameters parameters = null;			// parameter object
	LogSpace logSpace = null;							// LogSpace object
	
	ProfileHMM model = null; 							// the pHMM
	AlphabetIndex alphabetIndex = null;					// the alphabetIndex, BioJava class, given to the constructor
	FiniteAlphabet alphabet;
	int alphabetSize = 0;								// derived attribute, size of the alphabet, read from alphabetIndex
	int columnCount = 0;
	int stateCount = 0;

	double[][][] incomingTransitionsCache = null;		// cache for incoming transitions for a given state
	double[][][] outgoingTransitionsCache = null;		// cache for outgoing transitions for a given state
	HashMap<Symbol, Integer> alphabetIndexCache = null;	// cache for the symbol indices in the alphabet manager
	HashMap<Symbol, int[]> matchingSymbolsCache = null; // cache containing the matching symbol indices for a given symbol
	double[][] emissionCache = null; 					// cache for the emission probabilities
	
	public FisherScoring(ProfileHMM model, FiniteAlphabet alphabet) {
		this(model, alphabet, createDefaultFisherScoringParameters());
	}
	
	public FisherScoring(ProfileHMM model, FiniteAlphabet alphabet, FisherScoringParameters parameters) {
		this.model = model;
		this.alphabet = alphabet;
		this.alphabetIndex = AlphabetManager.getAlphabetIndex(alphabet);
		this.alphabetSize = alphabetIndex.getAlphabet().size();
		this.parameters = parameters;
		this.logSpace = new LogSpace(parameters.getLogStrategy());
		
		// this is a regular pHMM (Durbin, p. 105). We model the first delete state in column
		// 0 (which does not exist) as a state with zero transitions and zero emissions.
		// Therefore the total number of states is:
		this.columnCount = this.model.columns() + 1; // BioJava doesn't count the first and last column, we ignore the last
		this.stateCount = columnCount * 3;

		initializeMatrices();
	}

	private void initializeMatrices() {
		// create matrices from the ProfileHMMTools
		DoubleMatrix[] transitionMatrices = new DoubleMatrix[] {
				JBlasTools.apply(ProfileHMMTools.getTransitionMatrix(this.model, ProfileHMMTools.StateTypeMatch),logSpace.LOG),
				JBlasTools.apply(ProfileHMMTools.getTransitionMatrix(this.model, ProfileHMMTools.StateTypeInsert),logSpace.LOG),
				JBlasTools.apply(ProfileHMMTools.getTransitionMatrix(this.model, ProfileHMMTools.StateTypeDelete),logSpace.LOG)
		};
				
		// setup alphabet index cache for fast access to the index of a symbol in the alphabet
		// includes indices for ambiguities
		Set<Symbol> allSymbols = BioJavaHelper.getAllSymbols(alphabetIndex.getAlphabet()); 
		this.alphabetIndexCache = new HashMap<>(this.alphabetSize, 1.0F);
		// first add atomic symbols
		for (Iterator<Symbol>it=alphabetIndex.getAlphabet().iterator();it.hasNext();){
			Symbol s = it.next();
			int index = -1;
			try {
				index = this.alphabetIndex.indexForSymbol(s);
			} catch (IllegalSymbolException e) {
				e.printStackTrace();
			}			
			this.alphabetIndexCache.put(s, index);
			allSymbols.remove(s);
		}
		// now all symbols only contains the ambiguity symbols, add those at the end
		int index = alphabetIndex.getAlphabet().size();
		for (Symbol s:allSymbols) {
			this.alphabetIndexCache.put(s, index++);
		}
			
		// setup matching symbols cache for fast access to the symbols behind ambiguities
		// normal symbols just match themselves
		this.matchingSymbolsCache = new HashMap<>(this.alphabetIndexCache.size(), 1.0F);
		for (Symbol s: alphabetIndexCache.keySet()) {
			FiniteAlphabet matchAlphabet = (FiniteAlphabet) s.getMatches();
			int[] matches = new int[matchAlphabet.size()];
			int i = 0;
			Iterator<Symbol> itmatch = matchAlphabet.iterator(); 
			while (itmatch.hasNext()) {
				matches[i] = this.alphabetIndexCache.get(itmatch.next());
				i++;
			}
			this.matchingSymbolsCache.put(s, matches);
		}

		List<Symbol> orderedSymbolList = new ArrayList<>();
		for (int j=0;j<alphabetIndexCache.size();j++){
			for (Symbol s:alphabetIndexCache.keySet()){
				int testindex = alphabetIndexCache.get(s);
				if (testindex==j){
					orderedSymbolList.add(s);
					break;
				}
			}
		}
		
		DoubleMatrix[] emissionMatrices = new DoubleMatrix[] {
				JBlasTools.apply(ProfileHMMTools.getEmissionMatrix(this.model, ProfileHMMTools.StateTypeMatch, orderedSymbolList),logSpace.LOG),
				JBlasTools.apply(ProfileHMMTools.getEmissionMatrix(this.model, ProfileHMMTools.StateTypeInsert, orderedSymbolList),logSpace.LOG)
		};

		// setup transition and emission caches for efficient access to the right probabilities and destinations/origins
		this.outgoingTransitionsCache = new double[stateCount][][];
		this.incomingTransitionsCache = new double[stateCount][][];
		this.emissionCache = new double[stateCount + 3][this.alphabetIndexCache.values().size()]; 
		// the + 3 are zero emissions for a virtual "end" column. This is needed because the outgoing
		// transitions of the last state still give increasing indices pointing to the next (non-existant) column
		
		for (int a = 0; a < stateCount; a++) {
			this.outgoingTransitionsCache[a] = getOutgoingTransitionTriplet(a, transitionMatrices);
			this.incomingTransitionsCache[a] = getIncomingTransitionTriplet(a + 1, transitionMatrices);
		}

		for (int a = 0; a < (stateCount + 3); a++) {
			for (int symbolIndex : this.alphabetIndexCache.values()) {
				this.emissionCache[a][symbolIndex] = getEmission(a, symbolIndex, emissionMatrices);
			}
		}
	}

	private double[][] getOutgoingTransitionTriplet(int stateIndex, DoubleMatrix[] transitionMatrices) {
		// Returns two triplets with the transition probabilities and
		// the respective indices of the destination states
		double[][] triplet = new double[][]{{logSpace.ZERO, logSpace.ZERO, logSpace.ZERO}, {0, 0, 0}};
		int column = stateIndex / 3;
		int stateType = stateIndex % 3;
		
		DoubleMatrix transMatMatch = transitionMatrices[ProfileHMMTools.StateTypeMatch];		// matrix with transitions for matches
		DoubleMatrix transMatDelete = transitionMatrices[ProfileHMMTools.StateTypeDelete];	// matrix with transitions for deletes
		DoubleMatrix transMatInsert = transitionMatrices[ProfileHMMTools.StateTypeInsert];	// matrix with transitions for inserts
		
		if (column < transMatDelete.columns) { 
			switch(stateType) {
				case ProfileHMMTools.StateTypeDelete: // delete
					//triplet[0] = this.transMatDelete.viewColumn(column).toArray();
					triplet[0][ProfileHMMTools.StateTypeMatch] = transMatDelete.get(ProfileHMMTools.StateTypeMatch, column);
					triplet[0][ProfileHMMTools.StateTypeInsert] = transMatDelete.get(ProfileHMMTools.StateTypeInsert, column);
					triplet[0][ProfileHMMTools.StateTypeDelete] = transMatDelete.get(ProfileHMMTools.StateTypeDelete, column);
					triplet[1][ProfileHMMTools.StateTypeMatch] = (column + 1) * 3; // transition to match in next column
					triplet[1][ProfileHMMTools.StateTypeInsert] = column * 3 + 1; // transition to insert in same column
					triplet[1][ProfileHMMTools.StateTypeDelete] = (column + 1) * 3 + 2; // transition to delete in next column
					break;
				case ProfileHMMTools.StateTypeInsert: // insert
					//triplet[0] = this.transMatInsert.viewColumn(column).toArray();
					triplet[0][ProfileHMMTools.StateTypeMatch] = transMatInsert.get(ProfileHMMTools.StateTypeMatch, column);
					triplet[0][ProfileHMMTools.StateTypeInsert] = transMatInsert.get(ProfileHMMTools.StateTypeInsert, column);
					triplet[0][ProfileHMMTools.StateTypeDelete] = transMatInsert.get(ProfileHMMTools.StateTypeDelete, column);
					triplet[1][ProfileHMMTools.StateTypeMatch] = (column + 1) * 3; // transition to match in next column
					triplet[1][ProfileHMMTools.StateTypeInsert] = stateIndex; // transition to insert in same column
					triplet[1][ProfileHMMTools.StateTypeDelete] = (column + 1) * 3 + 2; // transition to delete in next column
					break;
				case ProfileHMMTools.StateTypeMatch: // match
					//triplet[0] = this.transMatMatch.viewColumn(column).toArray();
					triplet[0][ProfileHMMTools.StateTypeMatch] = transMatMatch.get(ProfileHMMTools.StateTypeMatch, column);
					triplet[0][ProfileHMMTools.StateTypeInsert] = transMatMatch.get(ProfileHMMTools.StateTypeInsert, column);
					triplet[0][ProfileHMMTools.StateTypeDelete] = transMatMatch.get(ProfileHMMTools.StateTypeDelete, column);
					triplet[1][ProfileHMMTools.StateTypeMatch] = (column + 1) * 3; // transition to match in next column
					triplet[1][ProfileHMMTools.StateTypeInsert] = column * 3 + 1; // transition to insert in same column
					triplet[1][ProfileHMMTools.StateTypeDelete] = (column + 1) * 3 + 2; // transition to delete in next column
					break;
			}
		} else {
			triplet[0] = new double[3];
			triplet[1] = new double[] {column * 3, column * 3, column * 3};
		}
		return triplet;
	}

	private double[][] getIncomingTransitionTriplet(int stateIndex, DoubleMatrix[] transitionMatrices) {
		// Returns two triplets with the transition probabilities and
		// the respective indices of the origin states
		double[][] triplet = new double[][]{{logSpace.ZERO, logSpace.ZERO, logSpace.ZERO}, {0, 0, 0}};
		int column = stateIndex / 3;
		int stateType = stateIndex % 3;

		DoubleMatrix transMatMatch = transitionMatrices[ProfileHMMTools.StateTypeMatch];		// matrix with transitions for matches
		DoubleMatrix transMatDelete = transitionMatrices[ProfileHMMTools.StateTypeDelete];	// matrix with transitions for deletes
		DoubleMatrix transMatInsert = transitionMatrices[ProfileHMMTools.StateTypeInsert];	// matrix with transitions for inserts

		switch(stateType) {
			case ProfileHMMTools.StateTypeDelete: // delete
				if (column == 0) break;  // no delete state in column 0
				triplet[0][ProfileHMMTools.StateTypeMatch] = transMatMatch.get(ProfileHMMTools.StateTypeDelete, column - 1);
				triplet[0][ProfileHMMTools.StateTypeInsert] = transMatInsert.get(ProfileHMMTools.StateTypeDelete, column - 1);
				triplet[0][ProfileHMMTools.StateTypeDelete] = transMatDelete.get(ProfileHMMTools.StateTypeDelete, column - 1);
				triplet[1][ProfileHMMTools.StateTypeMatch] = (column - 1) * 3; // transition from match in previous column
				triplet[1][ProfileHMMTools.StateTypeInsert] = (column - 1) * 3 + 1; // transition from insert in previous column
				triplet[1][ProfileHMMTools.StateTypeDelete] = (column - 1) * 3 + 2; // transition from delete in previous column
				break;
			case ProfileHMMTools.StateTypeInsert: // insert
				triplet[0][ProfileHMMTools.StateTypeMatch] = transMatMatch.get(ProfileHMMTools.StateTypeInsert, column);
				triplet[0][ProfileHMMTools.StateTypeInsert] = transMatInsert.get(ProfileHMMTools.StateTypeInsert, column);
				triplet[0][ProfileHMMTools.StateTypeDelete] = transMatDelete.get(ProfileHMMTools.StateTypeInsert, column);
				triplet[1][ProfileHMMTools.StateTypeMatch] = column * 3; // transition from match in same column
				triplet[1][ProfileHMMTools.StateTypeInsert] = column * 3 + 1; // transition from insert in same column
				triplet[1][ProfileHMMTools.StateTypeDelete] = column * 3 + 2; // transition from delete in same column
				break;
			case ProfileHMMTools.StateTypeMatch: // match
				triplet[0][ProfileHMMTools.StateTypeMatch] = transMatMatch.get(ProfileHMMTools.StateTypeMatch, column - 1);
				triplet[0][ProfileHMMTools.StateTypeInsert] = transMatInsert.get(ProfileHMMTools.StateTypeMatch, column - 1);
				triplet[0][ProfileHMMTools.StateTypeDelete] = transMatDelete.get(ProfileHMMTools.StateTypeMatch, column - 1);
				triplet[1][ProfileHMMTools.StateTypeMatch] = (column - 1) * 3; // transition from match in previous column
				triplet[1][ProfileHMMTools.StateTypeInsert] = (column - 1) * 3 + 1; // transition from insert in previous column
				triplet[1][ProfileHMMTools.StateTypeDelete] = (column - 1) * 3 + 2; // transition from delete in previous column
				break;
		}
		return triplet;
	}

	private double getEmission(int stateIndex, int symbolIndex, DoubleMatrix[] emissionMatrices) {
		// return the emission prob for a given stateIndex and symbolIndex
		// the symbolIndex can include indices for ambiguities as they are
		// contained in the emission matrices
		double emission = logSpace.ZERO;
		int column = stateIndex / 3;
		int stateType = stateIndex % 3;
		
		DoubleMatrix emitMatMatch = emissionMatrices[ProfileHMMTools.StateTypeMatch];
		DoubleMatrix emitMatInsert = emissionMatrices[ProfileHMMTools.StateTypeInsert];
		
		if (column < emitMatMatch.columns) {
			switch(stateType) {
				case ProfileHMMTools.StateTypeDelete:
					emission = logSpace.ZERO;
					break;
				case ProfileHMMTools.StateTypeInsert:
					emission = emitMatInsert.get(symbolIndex, column);
					break;
				case ProfileHMMTools.StateTypeMatch:
					emission = emitMatMatch.get(symbolIndex, column);
					break;
			}
		}
		
		return emission;
	}

	public int getFisherScoreDimensions() {
		return  this.model.columns() * this.alphabetSize;
	}
	
	public DoubleMatrix matchFisherVector( SymbolList[] symbolListArray) {
		DoubleMatrix scoreMat = new DoubleMatrix(1, this.model.columns() * this.alphabetSize);
		matchFisherVector(symbolListArray, scoreMat, 0);
		return scoreMat.getRow(0);
	}
	
	public void matchFisherVector( SymbolList[] symbolListArray, DoubleMatrix scoreMat, int scoreIndex) {
		SymbolList sequence = symbolListArray[0];
		int sequenceLength = sequence.length();
		
		DoubleMatrix forw = new DoubleMatrix(stateCount, sequenceLength + 1);
		DoubleMatrix back = new DoubleMatrix(stateCount + 3, sequenceLength + 1);
		
		// init forward matrix
		// first row
		forw.put(0, 0, logSpace.toLog(1));
		for (int i = 1; i <= sequenceLength; i++) {
			forw.put(0, i, logSpace.ZERO);
		}
		// first column
		for (int a = 1; a < stateCount; a++) {
			double value = logSpace.ZERO;
			if (a % 3 == 2) { // delete state
				double[][] triplet = this.incomingTransitionsCache[a - 1];
				for (int b = 0; b < 3; b++) {
					value = logSpace.sum(value, triplet[0][b] + forw.get( (int) triplet[1][b], 0)); 
				}
			}
			forw.put(a, 0, value);
		}
		
		// recurse
		for (int i = 1; i <= sequenceLength; i++) { // i iterates over sequence from 1..sequenceLength
			int symbolIndex = this.alphabetIndexCache.get(sequence.symbolAt(i));
			for (int a = 1; a < stateCount; a++) { // a iterates over states from 1..stateCount-1
				double[][] triplet = this.incomingTransitionsCache[a - 1];
				double value = logSpace.ZERO;

				if (a % 3 != 2) { // real state				
					for (int b = 0; b < 3; b++) {
						value = logSpace.sum(value, triplet[0][b] + forw.get( (int) triplet[1][b], i - 1));
					}
					value = value + this.emissionCache[a][symbolIndex];
				} else { // silent state
					for (int b = 0; b < 3; b++) { 
						value = logSpace.sum(value, triplet[0][b] + forw.get( (int) triplet[1][b], i));
					}
				}
				forw.put(a, i, value);
			}
		}
		
		// calculate total sequence probability, start at 1, index 0 has value zero anyway
		double seqScore = logSpace.ZERO;
		double[][] finalTriplet = this.incomingTransitionsCache[stateCount - 1];
		for (int b = 0; b < 3; b++) {
			seqScore = logSpace.sum(seqScore, forw.get((int) finalTriplet[1][b], sequenceLength) + finalTriplet[0][b]); 
		}

		// init backward matrix
		back.put(stateCount, sequenceLength, logSpace.toLog(1)); // final state
		for (int a = stateCount - 1; a >= 0; a--) {
			double[][] triplet = this.outgoingTransitionsCache[a];
			double value = logSpace.ZERO;
			for (int b = 0; b < 3; b++) {
				int sid = (int) triplet[1][b];
				if (sid % 3 == 2 || sid == stateCount) { // silent state
					value = logSpace.sum(value, triplet[0][b] + back.get(sid, sequenceLength));
				}
			}
			back.put(a, sequenceLength, value);
		}
		
		// now backward and fisher scores
		for (int i = sequenceLength - 1; i >= 0; i--) {
			int symbolIndex = this.alphabetIndexCache.get(sequence.symbolAt(i + 1));
			int[] matches = this.matchingSymbolsCache.get(sequence.symbolAt(i + 1));
			for (int a = stateCount-1; a >= 0; a--) {
				double value = logSpace.ZERO;
				double[][] triplet = this.outgoingTransitionsCache[a];
				
				for (int b = 0; b < 3; b++) {
					int sid = (int) triplet[1][b];
					if (sid % 3 != 2) { // real state
						double emit = this.emissionCache[sid][symbolIndex];
						value = logSpace.sum(value, triplet[0][b] + emit + back.get( sid, i + 1));
					} else { // silent state
						value = logSpace.sum(value, triplet[0][b] + back.get( sid, i));
					}
				}
				back.put(a, i, value);

				int columnIndex = a / 3;
				int rowIndex = a % 3;

				if (rowIndex == 0 && columnIndex > 0) { // match state
					
					double emit = this.emissionCache[a][symbolIndex];
					
					double backVal = back.get(a, i + 1);
					double forwVal = forw.get(a, i + 1);
					for (int fisherIndex : matches) {
						try {
							double f = scoreMat.get(scoreIndex, ((columnIndex - 1) * this.alphabetSize) + fisherIndex);
							f = f + (logSpace.toReal(backVal + forwVal - emit - seqScore));
							scoreMat.put(scoreIndex, ((columnIndex - 1) * this.alphabetSize) + fisherIndex, f);
						}
						catch (Exception e) {
							e.printStackTrace();
						}
					}
					
					for (int sigma = 0; sigma < this.alphabetSize; sigma++) {
						double f2 = scoreMat.get(scoreIndex, (columnIndex - 1) * this.alphabetSize + sigma);
						f2 = f2 - (logSpace.toReal(backVal + forwVal - seqScore));
						scoreMat.put(scoreIndex, (columnIndex - 1) * this.alphabetSize + sigma, f2);
					} 
				}
			}
		}
	}

	public Symbol symbolForFisherColumn(int column){
		int max = getFisherScoreDimensions();
		if (column>max-1){
			throw new ArrayIndexOutOfBoundsException("Column " + column + " > " + max);
		}
		Symbol s = this.alphabetIndex.symbolForIndex(column % this.alphabetSize);
		return s;
	}
	
	public int[] fisherColumnsForSymbol(int alignmentColumn, Symbol symbol) throws IllegalSymbolException {
		int[] matches = this.matchingSymbolsCache.get(symbol);
		// matches are now guaranteed to be [0,alphabetSize-1]
		for (int i=0;i<matches.length;i++){
			matches[i]+= (alignmentColumn * this.alphabetSize);
		}
		return matches;
	}
	
	public FisherScoresMatrix createFisherScoresMatrix(int nrows) {
		Symbol[] columnHeader = new Symbol[getFisherScoreDimensions()];
		for (int i=0;i<getFisherScoreDimensions();i++){
			columnHeader[i]=symbolForFisherColumn(i);
		}
		return new FisherScoresMatrix(nrows, getFisherScoreDimensions(), columnHeader, alphabet);
	}
}
