/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package alvis.algorithm;


import alvis.AlvisDataModel;
import cama.BioJavaHelper;
import com.general.gui.progress.DefaultProgressable;
import com.general.gui.progress.Progressable;
import de.biozentrum.bioinformatik.alignment.MultipleAlignment;
import javax.swing.SwingWorker;
import org.biojava.bio.alignment.Alignment;
import org.biojava.bio.dp.ProfileHMM;
import org.biojava.bio.symbol.FiniteAlphabet;

/**
 *
 * @author Roland Schwarz <rfs32@cam.ac.uk>
 */
	public class TrainHMMTask extends SwingWorker<ProfileHMM, Integer> {
		private final MultipleAlignment alignment;
		final DefaultProgressable progressable;
		
		public TrainHMMTask(MultipleAlignment alignment) {
			this.alignment=alignment;
			progressable = new DefaultProgressable(this, "Training ProfileHMM", true);
			progressable.setState(Progressable.STATE_RUNNING);			
			progressable.setMinimum(0);
			progressable.setMaximum(alignment.getSequenceCount()-1);
		}
		
		@Override
		protected ProfileHMM doInBackground() throws Exception {
			int nSequences = alignment.getSequenceCount();
			if (nSequences==0) return null;
//			FiniteAlphabet alphabet = BioJavaHelper.convertAlphabet(alignment.getAlphabet());
			Alignment bjAlignment = BioJavaHelper.convertAlignment(alignment);
			ProfileHMM pHMM = ProfileHMMTools.createProfile(bjAlignment, AlvisDataModel.MATCH_CUTOFF, 1);
			return pHMM;
		}

		@Override
		protected void done() {
			progressable.setState(Progressable.STATE_IDLE);
		}

		/**
		 * @return the progressable
		 */
		public DefaultProgressable getProgressable() {
			return progressable;
		}
		
	}
