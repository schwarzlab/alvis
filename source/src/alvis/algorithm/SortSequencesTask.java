/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package alvis.algorithm;

import alvis.AlvisDataModel;
import alvis.algorithm.matrix.FisherScoresMatrix;
import com.general.containerModel.Nameable;
import com.general.gui.progress.DefaultProgressable;
import com.general.gui.progress.Progressable;
import com.general.utils.GeneralUtils;
import de.biozentrum.bioinformatik.alignment.MultipleAlignment;
import de.biozentrum.bioinformatik.alignment.events.AlignmentChangedEvent;
import de.biozentrum.bioinformatik.alignment.events.AlignmentStructureChangedEvent;
import de.biozentrum.bioinformatik.alignment.events.MAEvent;
import de.biozentrum.bioinformatik.sequence.Sequence;
import java.awt.Rectangle;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import org.jblas.DoubleMatrix;

/**
 *
 * @author Roland Schwarz <rfs32@cam.ac.uk>
 */
public class SortSequencesTask extends SwingWorker<Object,Integer>{
	private final AlvisDataModel dataModel;
	private final Comparator<Sequence> comparator;
	private FisherScoresMatrix newFisher;
	private DoubleMatrix[] newSVD;
	private final Map<Nameable, DoubleMatrix> newKernelMatrices;
	DefaultProgressable progress=new DefaultProgressable("Sorting sequences...", true);
	public SortSequencesTask(AlvisDataModel dataModel, Comparator<Sequence> comparator){
		this.dataModel = dataModel;
		this.comparator = comparator;
		this.newKernelMatrices = new HashMap<>();
	}
	
	@Override
	protected Object doInBackground() throws Exception {
		progress.setState(Progressable.STATE_RUNNING);
		// modify alignment in place
		final MultipleAlignment alignment = dataModel.getAlignment();
		int[] order = GeneralUtils.order(alignment.getSequences(), comparator);
		Collections.sort(alignment.getSequences(), comparator);
		FisherScoresMatrix oldFisher = dataModel.getFisherScores();
		if (oldFisher!=null){
			newFisher = new FisherScoresMatrix(oldFisher.getRows(order), oldFisher.getColumnHeader(), oldFisher.getAlphabet());
		} else {
			newFisher = null;
		}
		
		for (Nameable id:dataModel.getKernelMatrices().keySet()){
			DoubleMatrix oldK = dataModel.getKernelMatrix(id);
			DoubleMatrix newK = oldK.get(order, order);
			newKernelMatrices.put(id, newK);
		}
		
		if (dataModel.getFisherKernelSVD()!=null) {
			newSVD = new DoubleMatrix[3];
			newSVD[0]=dataModel.getFisherKernelSVD()[0].getRows(order);
			newSVD[1]=dataModel.getFisherKernelSVD()[1];
			newSVD[2]=dataModel.getFisherKernelSVD()[2];
		} else {
			newSVD = null;
		}
		return null;
	}

	@Override
	protected void done() {
		progress.setState(Progressable.STATE_IDLE);
		dataModel.getAlignment().fireAlignmentModelEvent(new AlignmentStructureChangedEvent(this, 0, dataModel.getAlignment().rows(), AlignmentStructureChangedEvent.ModifiedStructure.ROWS, MAEvent.MAEventType.UPDATE));
		dataModel.getAlignment().fireAlignmentModelEvent(new AlignmentChangedEvent(dataModel.getAlignment(), new Rectangle(0,0,dataModel.getAlignment().length(), dataModel.getAlignment().rows()), AlignmentChangedEvent.MAEventType.UPDATE, false));
		dataModel.setFisherScores(newFisher);
		dataModel.setFisherKernelSVD(newSVD);
		for (Entry<Nameable,DoubleMatrix> e:newKernelMatrices.entrySet()){
			dataModel.addKernelMatrix(e.getKey(), e.getValue());
		}
	}

	/**
	 * @return the progress
	 */
	public DefaultProgressable getProgressable() {
		return progress;
	}
	
	
	
}
