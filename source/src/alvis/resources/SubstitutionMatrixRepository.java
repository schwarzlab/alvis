/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alvis.resources;

import alvis.AlvisDataModel;
import alvis.algorithm.matrix.NameableSubstitutionMatrix;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import org.biojava.bio.BioException;
import org.biojava.bio.seq.DNATools;
import org.biojava.bio.seq.ProteinTools;
import org.biojava.bio.seq.RNATools;
import org.biojava.bio.symbol.FiniteAlphabet;

/**
 *
 * @author Roland Schwarz <rfs32@cam.ac.uk>
 */
public class SubstitutionMatrixRepository {
	private static final String PREFIX = "/res/subsmat/";
	private static final String NAME_LIST = "matrix_ids";
	private static final List<NameableSubstitutionMatrix> matrices = new ArrayList<>();
	private static NameableSubstitutionMatrix defaultAmino;
	private static NameableSubstitutionMatrix defaultNucl;

	static {
		try {
			loadMatrices();
		} catch (IOException | BioException ex) {
			ex.printStackTrace();
		}
	}

	private SubstitutionMatrixRepository() {}
	public static void init(){} // to force loading if desired
	
	private static void loadMatrices() throws IOException, BioException {
		URL listURL = SubstitutionMatrixRepository.class.getResource(PREFIX + NAME_LIST);
		BufferedReader input = new BufferedReader(new InputStreamReader(listURL.openStream()));
		String inputLine;
		while ((inputLine = input.readLine()) != null) {
			URL matrixURL = SubstitutionMatrixRepository.class.getResource(PREFIX + inputLine);
			BufferedReader matrixReader = new BufferedReader(new InputStreamReader(matrixURL.openStream()));
			StringBuilder stringMatrix = new StringBuilder("");
			while (matrixReader.ready()) {
				stringMatrix.append(matrixReader.readLine());
				stringMatrix.append("\n");
			}
			matrixReader.close();
			String mat = stringMatrix.toString();
			FiniteAlphabet alpha;
			if (inputLine.startsWith("NUC")) {
				alpha = DNATools.getDNA();
			} else {
				alpha = ProteinTools.getAlphabet();
			}
			String name = inputLine;
			if (name.endsWith(".mod")){
				name = name.substring(0, name.length()-4);
			}
			NameableSubstitutionMatrix matrix = new NameableSubstitutionMatrix(alpha, mat, name);
			matrix.setDescription(matrix.toString());
			if (matrix.getName().startsWith("BLOSUM62")) {
				defaultAmino = matrix;
			} else if (matrix.getName().startsWith("NUC.4.4")) {
				defaultNucl = matrix;
			}
			matrices.add(matrix);
		}
	}

	public static List<NameableSubstitutionMatrix> getSubstitutionMatrices(AlvisDataModel.AlignmentType type) {
		List<NameableSubstitutionMatrix> subList = new ArrayList<>();
		FiniteAlphabet testAlpha = null;
		if (type.equals(AlvisDataModel.AlignmentType.AminoAcid)) {
			testAlpha = ProteinTools.getAlphabet();
		} else if (type.equals(AlvisDataModel.AlignmentType.DNA)) {
			testAlpha = DNATools.getDNA();
		} else if (type.equals(AlvisDataModel.AlignmentType.RNA)) {
			testAlpha = RNATools.getRNA();
		}

		if (testAlpha != null) {
			for (NameableSubstitutionMatrix sm : matrices) {
				if (sm.getAlphabet().equals(testAlpha)) {
					subList.add(sm);
				}
			}
		}
		return subList;
	}

	public static NameableSubstitutionMatrix getDefaultSubstitutionMatrix(AlvisDataModel.AlignmentType type) {
		if (type.equals(AlvisDataModel.AlignmentType.AminoAcid)) {
			return defaultAmino;
		} else if (type.equals(AlvisDataModel.AlignmentType.DNA)) {
			return defaultNucl;
		} else {
			return null;
		}
	}
}
