package alvis.resources;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



import gui.sequencebundle.SequenceBundleResources;
import java.awt.Font;
import javax.swing.ImageIcon;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public class AlvisResources extends SequenceBundleResources{
    public static ImageIcon SANDCLOCK_BLACK_64x64;
    public static ImageIcon SANDCLOCK_BLACK_128x128;
    public static ImageIcon SANDCLOCK_PURPLE_64x64;
    public static ImageIcon SANDCLOCK_PURPLE_128x128;
    public static ImageIcon BACKGROUND_TILE;
	public static ImageIcon ALVIS_LOGO_32x32;
	public static ImageIcon ALVIS_LOGO_48x48;
	public static ImageIcon ALVIS_SPLASH_SMALL;
	public static ImageIcon ALVIS_SPLASH_LARGE;
	public static Font DEFAULT_FONT = SequenceBundleResources.SB_FONT_REGULAR.deriveFont(12.0f);
	
    
    // load the resources, if that gets too time consuming remove and put explicitely on a worker thread
    static {
        loadResources();
    }
    
	public static void init(){} // to force loading if desired
	
	
	private AlvisResources() {} // static only
    
    private static void loadResources() {
        SANDCLOCK_BLACK_64x64 = createImageIcon("/res/images/sandclock_black_64x64.gif");
        SANDCLOCK_BLACK_128x128 = createImageIcon("/res/images/sandclock_black_128x128.gif");
        SANDCLOCK_PURPLE_64x64 = createImageIcon("/res/images/sandclock_purple_64x64.gif");
        SANDCLOCK_PURPLE_128x128 = createImageIcon("/res/images/sandclock_purple_128x128.gif");
        BACKGROUND_TILE = createImageIcon("/res/images/background_tile.png");
		ALVIS_LOGO_32x32=createImageIcon("/res/images/alvis_icon_32x32.png");
		ALVIS_LOGO_48x48=createImageIcon("/res/images/alvis_icon_48x48.png");
		ALVIS_SPLASH_SMALL=createImageIcon("/res/images/splash-screen_900.png");
		ALVIS_SPLASH_LARGE=createImageIcon("/res/images/splash-screen_1300.png");
	}
    

}
