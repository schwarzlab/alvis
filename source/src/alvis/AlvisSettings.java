/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package alvis;

import java.util.Properties;

/**
 *
 * @author Roland Schwarz <rfs32@cam.ac.uk>
 */
public final class AlvisSettings {
	public static final String PROP_PATH_JRI="pathToJRI";
	String pathToJRI="";

	public AlvisSettings() {
	}

	public AlvisSettings(AlvisSettings settings){
		setAll(settings);
	}
	
	public Properties toProperties(Properties p){
		p.setProperty(PROP_PATH_JRI, pathToJRI);
		return p;
	}
	public Properties toProperties(){
		Properties p = new Properties();
		return toProperties(p);
	}
	
	public void fromProperties(Properties p){
		setPathToJRI(p.getProperty(PROP_PATH_JRI, ""));
	}
	
	public void setAll(AlvisSettings settings){
		this.pathToJRI = settings.getPathToJRI();
	}
	/**
	 * @return the pathToJRI
	 */
	public String getPathToJRI() {
		return pathToJRI;
	}

	/**
	 * @param pathToJRI the pathToJRI to set
	 */
	public void setPathToJRI(String pathToJRI) {
		this.pathToJRI = pathToJRI;
	}
	
}
