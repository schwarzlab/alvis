/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package alvis;

import alvis.algorithm.TrainHMMTask;
import com.general.gui.dialog.DialogFactory;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.SwingWorker;
import org.biojava.bio.dp.ProfileHMM;
import org.phylowidget.tree.TreeIO;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public final class AlvisDataLogic implements PropertyChangeListener{
	private Alvis alvis;
	
	/**
	 *
	 * @param alvis
	 */
	public AlvisDataLogic(Alvis alvis) {
		this.alvis = alvis;
		alvis.getDataModel().addPropertyChangeListener(this);
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if (evt.getPropertyName().equals(AlvisDataModel.PROPERTY_ALIGNMENT)) {
			alignmentChanged();
		} else if (evt.getPropertyName().equals(AlvisDataModel.PROPERTY_TREE)) {
			treeChanged();
		} 
	}

	private void alignmentChanged() {
		if (alvis.getDataModel().getAlignment()!=null) {
//			trainHMM();
		}
		alvis.getDataModel().getSequenceGroups().clear();
		alvis.getDataModel().setFisherKernelSVD(null);
		alvis.getDataModel().setFisherScores(null);
		alvis.getDataModel().setProfileHMM(null);
		alvis.getDataModel().setTree(null);
		alvis.getDataModel().removeAllKernelMatrices();
	}
	
	private void treeChanged() {
		String treeString;
		if (alvis.getDataModel().getTree()!=null) {
			TreeIO.TreeOutputConfig config = new TreeIO.TreeOutputConfig();
			config.outputNHX=false;
			config.scrapeNaughtyChars=false;
			treeString = TreeIO.createNewickString(alvis.getDataModel().getTree(), config);
		} else {
			treeString=null;
		}
		alvis.getDataModel().setTreeString(treeString);
	}

	public void trainHMM() {
		final TrainHMMTask task = new TrainHMMTask(alvis.getDataModel().getAlignment());
		alvis.getMainFrame().addProgressable(task.getProgressable());
		task.addPropertyChangeListener(new PropertyChangeListener() {

			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				if (evt.getPropertyName().equals("state") && evt.getNewValue().equals(SwingWorker.StateValue.DONE)){
					try {
						ProfileHMM pHMM = task.get();
						alvis.getDataModel().setProfileHMM(pHMM);
					} catch (Exception ex){
						DialogFactory.showErrorMessage(ex.toString(), "Error");
					}
				}
			}
		});
		task.execute();
	}
}
