/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package alvis;

import alvis.algorithm.matrix.FisherScoresMatrix;
import com.general.containerModel.Nameable;
import com.general.containerModel.impl.MapDefaultValueDecorator;
import de.biozentrum.bioinformatik.alignment.MultipleAlignment;
import de.biozentrum.bioinformatik.sequence.Sequence;
import de.biozentrum.bioinformatik.sequence.SequenceAlphabet;
import gui.sequencebundle.SequenceBundleColorModel;
import gui.sequencebundle.SequenceBundleConfig;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.Map;
import javax.swing.SwingUtilities;
import org.biojava.bio.dp.ProfileHMM;
import org.jblas.DoubleMatrix;
import org.phylowidget.PhyloTree;
import org.phylowidget.tree.TreeIO;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public final class AlvisDataModel implements Serializable {
    public enum AlignmentType {
            DNA,
            RNA,
            AminoAcid,
            Custom
    };
	
	private final static long serialVersionUID=1L;
	private transient PropertyChangeSupport pcs = new PropertyChangeSupport(this);
    public static String PROPERTY_ALIGNMENT="alignment";
	public static String PROPERTY_SEQUENCE_GROUPS="sequenceGroups";
	public static String PROPERTY_SEQUENCE_BUNDLE_CONFIG="groupColors";
	public static String PROPERTY_PROFILE_HMM="profileHMM";
	public static String PROPERTY_FISHER_SCORES="fisherScores";
	public static String PROPERTY_KERNELS="kernels";
	public static String PROPERTY_FISHER_SVD="fisherSVD";
	public static String PROPERTY_FILE="file";
	public static String PROPERTY_TREE="tree";
	public static String PROPERTY_TREE_STRING="treeString";
    public static final char GAP_CHAR = '-';
	public static final double MATCH_CUTOFF = 0.51;
	
//	private transient Alignment bjAlignment;
    private MultipleAlignment alignment;
	private final Map<Sequence, Integer> sequenceGroups;
	private final Map<Nameable, DoubleMatrix> kernelMatrices;
	private SequenceBundleConfig sequenceBundleConfig;
	private ProfileHMM profileHMM;
	private FisherScoresMatrix fisherScores;
	private DoubleMatrix[] fisherKernelSVD;
	private transient PhyloTree tree;
	private String treeString;
	private transient File file=null;
	
    public AlvisDataModel() {
		sequenceBundleConfig = new SequenceBundleConfig();
		sequenceGroups = new MapDefaultValueDecorator<>(new HashMap<Sequence, Integer>(), 0);
		this.kernelMatrices = new HashMap<>();
    }
	
	
    public void addPropertyChangeListener(PropertyChangeListener listener) {
		this.pcs.addPropertyChangeListener(listener);
    }
	
    public void addPropertyChangeListener(String property, PropertyChangeListener listener) {
         this.pcs.addPropertyChangeListener(property, listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
         this.pcs.removePropertyChangeListener(listener);
    }    

	public AlignmentType getAlignmentType() {
		AlignmentType type;
		if (this.alignment.getAlphabet().equals(SequenceAlphabet.NUCLEOTIDES)){
			type = AlignmentType.DNA;
		} else if (this.alignment.getAlphabet().equals(SequenceAlphabet.AMINOACIDS)){
			type = AlignmentType.AminoAcid;
		} else if (this.alignment.getAlphabet().equals(SequenceAlphabet.RNA)){
			type = AlignmentType.RNA;
		} else {
			type = AlignmentType.Custom;
		}
		return type;
	}

    /**
     * @return the bzAlignment
     */
    public MultipleAlignment getAlignment() {
        return alignment;
    }

	private void checkEDT() {
		if (!SwingUtilities.isEventDispatchThread()) {
			throw new ConcurrentModificationException("not on EDT!");
		}
	}
    /**
     * @param newAlignment the alignment to set
     */
    public void setAlignment(MultipleAlignment newAlignment) {
		checkEDT();
		MultipleAlignment oldAlignment = this.alignment;
		this.alignment=newAlignment;
        this.pcs.firePropertyChange(PROPERTY_ALIGNMENT, oldAlignment, newAlignment);
    }

	/**
	 * @return the sequenceGroupMap
	 */
	public Map<Sequence, Integer> getSequenceGroups() {
		return sequenceGroups;
	}

	private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();
		pcs = new PropertyChangeSupport(this);
		if (this.treeString!=null) {
			PhyloTree tree = new PhyloTree();
			TreeIO.parseNewickString(tree, treeString);
			this.tree = tree;
		} else {
			this.tree = null;
		}
    }	

	private void writeObject(ObjectOutputStream out) throws IOException {
		out.defaultWriteObject();
	}
	
	/**
	 * @return the groupColors
	 */
	public SequenceBundleConfig getSequenceBundleConfig() {
		return sequenceBundleConfig;
	}

	/**
	 * @param sequenceBundleConfig
	 */
	public void setSequenceBundleConfig(SequenceBundleConfig sequenceBundleConfig) {
		checkEDT();
		SequenceBundleConfig oldval = this.sequenceBundleConfig;
		this.sequenceBundleConfig = sequenceBundleConfig;
		this.pcs.firePropertyChange(PROPERTY_SEQUENCE_BUNDLE_CONFIG, oldval, this.sequenceBundleConfig);
	}
	
	public static AlvisDataModel load(File file) throws IOException, ClassNotFoundException {
		FileInputStream fis = new FileInputStream(file);
		ObjectInputStream ois = new ObjectInputStream(fis);
		AlvisDataModel loadedModel = (AlvisDataModel)ois.readObject();
		ois.close();
		fis.close();
		loadedModel.setFile(file);
		return loadedModel;
	}
	
	public void save(File file) throws IOException {
		FileOutputStream fos = new FileOutputStream(file);
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		oos.writeObject(this);				
		oos.close();
		fos.close();
		setFile(file);
	}

	public String getName() {
		String name;
		if (file==null) {
			name = "<New project>";
		} else {
			name = file.getName();
		}
		return name;
	}
	/**
	 * @return the profileHMM
	 */
	public ProfileHMM getProfileHMM() {
		return profileHMM;
	}

	/**
	 * @param profileHMM the profileHMM to set
	 */
	public void setProfileHMM(ProfileHMM profileHMM) {
		checkEDT();
		ProfileHMM oldval = this.profileHMM;
		if (oldval==null || !oldval.equals(profileHMM)) {
			this.profileHMM = profileHMM;			
			this.pcs.firePropertyChange(PROPERTY_PROFILE_HMM, oldval, this.profileHMM);
		}
	}

	/**
	 * @return the fisherScores
	 */
	public FisherScoresMatrix getFisherScores() {
		return fisherScores;
	}

	/**
	 * @param fisherScores the fisherScores to set
	 */
	public void setFisherScores(FisherScoresMatrix fisherScores) {
		checkEDT();
		DoubleMatrix oldval=this.fisherScores;
		this.fisherScores = fisherScores;
		this.pcs.firePropertyChange(PROPERTY_FISHER_SCORES, oldval, this.fisherScores);
	}

	/**
	 * @return the fisherKernelSVD
	 */
	public DoubleMatrix[] getFisherKernelSVD() {
		return fisherKernelSVD;
	}

	/**
	 * @param fisherKernelSVD the fisherKernelSVD to set
	 */
	public void setFisherKernelSVD(DoubleMatrix[] fisherKernelSVD) {
		checkEDT();
		DoubleMatrix[] oldval = this.fisherKernelSVD;
		this.fisherKernelSVD = fisherKernelSVD;
		this.pcs.firePropertyChange(PROPERTY_FISHER_SVD, oldval, this.fisherKernelSVD);
	}

	/**
	 * @return the file
	 */
	public File getFile() {
		return file;
	}

	void setFile(File file){
		checkEDT();
		File oldval=this.file;
		if (oldval==null || !oldval.equals(file)){
			this.file=file;
			this.pcs.firePropertyChange(PROPERTY_FILE,oldval, this.file);
		} else {
		}
	}

	/**
	 * @return the tree
	 */
	public PhyloTree getTree() {
		return tree;
	}

	/**
	 * @param tree the tree to set
	 */
	public void setTree(PhyloTree tree) {
		checkEDT();
		PhyloTree oldTree = this.tree;
		if (oldTree==null || !oldTree.equals(tree)) {
			this.tree = tree;
			pcs.firePropertyChange(PROPERTY_TREE, oldTree, tree);
		}
	}

	/**
	 * @return the treeString
	 */
	public String getTreeString() {
		return treeString;
	}

	/**
	 * @param treeString the treeString to set
	 */
	public void setTreeString(String treeString) {
		String oldString = this.treeString;
		if (oldString==null || !oldString.equals(treeString)) {
			this.treeString = treeString;
			pcs.firePropertyChange(PROPERTY_TREE_STRING, oldString, this.treeString);
		}
	}
	
	public void addKernelMatrix(Nameable description, DoubleMatrix K){
		kernelMatrices.put(description,K);
		pcs.firePropertyChange(PROPERTY_KERNELS, null, K);
	}
	
	public void removeKernelMatrix(Nameable description){
		DoubleMatrix K;
		if((K=kernelMatrices.remove(description))!=null){
			pcs.firePropertyChange(PROPERTY_KERNELS, K, null);
		}
	}
	
	public void removeAllKernelMatrices() {
		boolean fire = kernelMatrices.size() > 0;
		kernelMatrices.clear();
		if (fire) {
			pcs.firePropertyChange(PROPERTY_KERNELS, null, null);
		}
	}
	
	public Map<Nameable,DoubleMatrix> getKernelMatrices() {
		return Collections.unmodifiableMap(kernelMatrices);
	}

	public DoubleMatrix getKernelMatrix(Nameable id){
		return kernelMatrices.get(id);
	}
	
	public String groupsToString() {
		if (alignment==null) {
			return "";
		}
		StringBuilder sb = new StringBuilder();
		for (Sequence s:alignment) {
			sb.append(s.getName());
			sb.append(",");
			sb.append(sequenceGroups.get(s)+1);
			sb.append("\n");
		}
		return sb.toString().trim();
	}
}
