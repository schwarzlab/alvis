/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package alvis;

import de.biozentrum.bioinformatik.sequence.Sequence;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Roland Schwarz <rfs32@cam.ac.uk>
 */
public class AlvisDataTools {

	public static int[] sequenceGroupsToIntArray(Map<Sequence, Integer> sequenceGroups, List<Sequence> sequences){
		int[]  groups = new int[sequences.size()];
		for (int i=0;i<sequences.size();i++){
			groups[i]=sequenceGroups.get(sequences.get(i));
		}
		return groups;
	}
}
