/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package alvis;

import alvis.resources.SubstitutionMatrixRepository;
import alvis.algorithm.rengine.AlvisRCallback;
import gui.sequencebundle.aaindex.AAIndexRepository;
import com.general.gui.SplashScreen;
import com.general.gui.fileHistory.FileHistory;
import com.general.resources.MyJavaResources;
import com.general.utils.GeneralUtils;
import alvis.resources.AlvisResources;
import com.general.ProgramInfo;
import gui.MainFrame;
import gui.sequencebundle.SequenceBundleResources;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Properties;
import javax.swing.ImageIcon;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.UnsupportedLookAndFeelException;
import org.rosuda.JRI.Rengine;


/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public class Alvis {
	public static final ProgramInfo Info = new ProgramInfo();
	static {
		Info.AUTHOR_CONTACT="roland.schwarz@mdc-berlin.de";
		Info.AUTHOR_DEPARTMENT="Max Delbrueck Center for Molecular Medicine, Berlin, Germany";
		Info.AUTHOR_NAME="Roland Schwarz & Asif Tamuri";
		Info.PROGRAM_LONG_NAME="Alvis - Visualisation of sequential datasets using Sequence Bundles";
		Info.PROGRAM_SHORT_NAME="Alvis";
		Info.PROGRAM_VERSION="0.2 beta";
	}
	
	public static final String ALVIS_CONFIG_FILE=".alvis";
	AlvisDataLogic dataLogic;
    private static Alvis _INSTANCE=null;
    private MainFrame frmMain;
    private AlvisDataModel dataModel;
	AlvisSettings settings;
	Rengine rengine;
	AlvisRCallback callback;
	private Properties globalProperties;

	private Alvis() {
		this.settings = new AlvisSettings();
		this.globalProperties = new Properties();
	}
	/**
	 * @return the settings
	 */
	public AlvisSettings getSettings() {
		return settings;
	}

    public enum AlignmentFormat {
            FASTA,
            CLUSTAL
    }

    private static Alvis getInstance() {
        if (_INSTANCE == null) {
            _INSTANCE = new Alvis();
        }
        return _INSTANCE;
    }
	
    /**
     * @return the dataModel
     */
    public AlvisDataModel getDataModel() {
        return dataModel;
    }
	public void setDataModel(AlvisDataModel model) {
		this.dataModel=model;
		this.dataLogic = new AlvisDataLogic(this);
		frmMain.init();		
		frmMain.dataModelChanged();
	}

	/**
	 * @return the dataLogic
	 */
	public AlvisDataLogic getDataLogic() {
		return dataLogic;
	}
	
    
    public MainFrame getMainFrame() {
        return frmMain;
    }
    
    public void run() {
		SplashScreen splash = new SplashScreen(AlvisResources.ALVIS_SPLASH_SMALL);
		splash.setVisible(true);
		splash.setStatusText("Setting look and feel...");
		setLookAndFeel();		
		splash.setStatusText("Loading settings...");
		loadSettings();
		// check if JRI is configured and chosen file is loadable
		if (this.settings.getPathToJRI()!=null && !this.settings.getPathToJRI().equals("")){
			splash.setStatusText("Starting R Engine...");
			startREngine();
		} 
		splash.setStatusText("Loading resources...");
		AlvisResources.init();
		MyJavaResources.init();
		SubstitutionMatrixRepository.init();
		AAIndexRepository.init();
		SequenceBundleResources.init();
		
		splash.setStatusText("Creating UI...");
        frmMain = new MainFrame(this);  
		frmMain.fromProperties(globalProperties);
        setDataModel(new AlvisDataModel());
//		splash.setStatusText("Waiting for you to read the splash screen...");
//		try {
//			Thread.sleep(5000);
//		} catch (InterruptedException ex) {}
		
        frmMain.setVisible(true);
		splash.dispose();
    }

	private void startREngine() {
		System.setProperty("jri.ignore.ule", "yes");
		try {
			GeneralUtils.addLibraryPath(Paths.get(settings.getPathToJRI()).getParent().toString());
			callback = new AlvisRCallback(frmMain);
			rengine = new Rengine(new String[] {"--vanilla"}, false, callback);
		} catch (UnsatisfiedLinkError | IOException | NoSuchFieldException | IllegalAccessException error){
			System.out.println("Couldn't load JRI library at: " + Paths.get(settings.getPathToJRI()).getParent().toString());
		}
	}

	private void setLookAndFeel() {
		try {
			for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException | IllegalAccessException | InstantiationException | UnsupportedLookAndFeelException e) {
			// If Nimbus is not available, you can set the GUI to another look and feel.
		}
	}
    
	private void loadSettings() {
		// load properties
		String home = System.getProperty("user.home");
		try {
			globalProperties.load(new FileInputStream(home+"/"+ALVIS_CONFIG_FILE));
			FileHistory.getInstance().fromProperties(globalProperties);
			this.settings.fromProperties(globalProperties);
			// don't set mainframe settings here, set at the end of run
		} catch (IOException ex) {}
	}
	
	private void saveSettings() {
		// save properties
		String home = System.getProperty("user.home");
		FileHistory.getInstance().toProperties(globalProperties);
		this.settings.toProperties(globalProperties);
		this.frmMain.toProperties(globalProperties);
		try {
			globalProperties.store(new FileOutputStream(home+"/"+ALVIS_CONFIG_FILE), "Alvis settings");
		} catch (IOException ex) {}
	}
	
    public void exit() {
		saveSettings();
		System.exit(0);
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
		System.setProperty("sun.java2d.dscale", "true");
		System.setProperty("sun.java2d.trace", "count");
		System.setProperty("sun.java2d.d3d", "false");
		System.setProperty("sun.java2d.noddraw", "true");
		System.setProperty("sun.java2d.opengl","false");
		System.setProperty("sun.java2d.translaccel","true");
		
        // TODO code application logic here
        Alvis myAlvis = getInstance();
        myAlvis.run();
    }
    
}
