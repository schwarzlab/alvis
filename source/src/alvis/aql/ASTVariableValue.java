package alvis.aql;

public class ASTVariableValue {
	private boolean name;
	private int fromIndex;
	private int toIndex;

	public ASTVariableValue(boolean isname, int fromIndex, int toIndex) {
		this.name=isname;
		this.fromIndex = fromIndex;
		this.toIndex = toIndex;
	}
			
	/**
	 * @return the name
	 */
	public boolean isName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(boolean name) {
		this.name = name;
	}

	/**
	 * @return the fromIndex
	 */
	public int getFromIndex() {
		return fromIndex;
	}

	/**
	 * @param fromIndex the fromIndex to set
	 */
	public void setFromIndex(int fromIndex) {
		this.fromIndex = fromIndex;
	}

	/**
	 * @return the toIndex
	 */
	public int getToIndex() {
		return toIndex;
	}

	/**
	 * @param toIndex the toIndex to set
	 */
	public void setToIndex(int toIndex) {
		this.toIndex = toIndex;
	}
	
}