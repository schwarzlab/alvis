package alvis.aql;

public class ASTQueryValue {
	private int type;
	private String expression;
	
	public ASTQueryValue(String expression, int type) {
		this.type=type;
		this.expression=expression;
	}

	/**
	 * @return the type
	 */
	public int getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(int type) {
		this.type = type;
	}

	/**
	 * @return the expression
	 */
	public String getExpression() {
		return expression;
	}

	/**
	 * @param expression the expression to set
	 */
	public void setExpression(String expression) {
		this.expression = expression;
	}
}