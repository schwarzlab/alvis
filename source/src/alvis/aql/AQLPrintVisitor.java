/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package alvis.aql;

import de.biozentrum.bioinformatik.sequence.Sequence;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public class AQLPrintVisitor implements AQLParserVisitor {

	@Override
	public Object visit(SimpleNode node, Sequence data) {
		throw new RuntimeException("Visit SimpleNode");
	}

	@Override
	public Object visit(ASTStart node, Sequence data) {
		node.jjtGetChild(0).jjtAccept(this, data);
		System.out.println(";");
		return(false);
	}

	@Override
	public Object visit(ASTTerm node, Sequence data) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public Object visit(ASTPrime node, Sequence data) {
		node.jjtGetChild(0).jjtAccept(this, data);
		System.out.print(" && ");
		node.jjtGetChild(1).jjtAccept(this, data);
		return false;
	}

	@Override
	public Object visit(ASTOpNot node, Sequence data) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public Object visit(ASTQuery node, Sequence data) {
		node.jjtGetChild(0).jjtAccept(this, data);
		ASTQueryValue value = (ASTQueryValue) node.value;
		boolean wasRegexp=false;
		switch (value.getType()) {
			case AQLParserConstants.OP_EQUALS:
				System.out.print(" == ");
				break;
			case AQLParserConstants.OP_NOTEQUALS:
				System.out.print(" != ");
				break;
			case AQLParserConstants.OP_MATCHES:
				System.out.print(".matches(");
				wasRegexp=true;
				break;
		}
		System.out.print(value.getExpression());
		if (wasRegexp) System.out.print(")");
		return false;
	}

	@Override
	public Object visit(ASTVariable node, Sequence data) {
		ASTVariableValue value = (ASTVariableValue) node.value;
		return value;
	}
	
}
