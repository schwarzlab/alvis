/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package alvis.aql;

import com.general.Range;
import de.biozentrum.bioinformatik.sequence.Sequence;
import gui.search.RegexpSequenceSearcher;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public class AQLSelectionVisitor implements AQLParserVisitor {
	private final List<Range<Integer>> sequenceHits = new ArrayList<>(); // list of regions	
	private boolean matchSubstring=true;
	private int currentFromIndex;
	private int currentToIndex;
	public AQLSelectionVisitor() {
		
	}
	public AQLSelectionVisitor(boolean matchSubstring){
		this.matchSubstring = matchSubstring;
	}
	
	@Override
	public Object visit(SimpleNode node, Sequence data) {
		throw new RuntimeException("Visit SimpleNode");
	}

	@Override
	public Object visit(ASTStart node, Sequence data) {
		boolean child = (Boolean) node.jjtGetChild(0).jjtAccept(this, data);
		return(child);
	}

	@Override
	public Object visit(ASTTerm node, Sequence data) {
		// OR ||
		boolean left = (Boolean) node.jjtGetChild(0).jjtAccept(this, data);
		boolean right = (Boolean) node.jjtGetChild(1).jjtAccept(this, data);
		return left || right;
	}

	@Override
	public Object visit(ASTPrime node, Sequence data) {
		// AND &&
		boolean left = (Boolean) node.jjtGetChild(0).jjtAccept(this, data);
		boolean right = (Boolean) node.jjtGetChild(1).jjtAccept(this, data);
		return left && right;
	}

	@Override
	public Object visit(ASTOpNot node, Sequence data) {
		return !(Boolean)node.jjtGetChild(0).jjtAccept(this, data);
	}

	@Override
	public Object visit(ASTQuery node, Sequence data) {
		boolean retval;
		ASTVariableValue variable = (ASTVariableValue) node.jjtGetChild(0).jjtAccept(this, data);
		String varValue;
		if (variable.isName()){
			varValue=data.getName();
		} else{
			varValue=data.getAlignedSequence();
		}

		int fromIndex=0, toIndex=varValue.length();
		if (variable.getFromIndex()>0){
			fromIndex=variable.getFromIndex()-1;
			if (variable.getToIndex()>0){
				toIndex=variable.getToIndex()-1 +1;
			} else {
				toIndex=fromIndex+1;
			}
		}
		varValue = varValue.substring(fromIndex, toIndex);
		
		ASTQueryValue value = (ASTQueryValue) node.value;
		String expr = value.getExpression().substring(1, value.getExpression().length()-1); // remove quotation marks
		switch (value.getType()) {
			case AQLParserConstants.OP_EQUALS:
				retval = varValue.equalsIgnoreCase(expr);
				if (retval){
					this.sequenceHits.add(new Range(currentFromIndex, currentToIndex));
				}
				break;
			case AQLParserConstants.OP_NOTEQUALS:
				retval = !varValue.equalsIgnoreCase(expr);
				if (retval){
					this.sequenceHits.add(new Range(currentFromIndex, currentToIndex));
				}
				break;
			case AQLParserConstants.OP_MATCHES:
				Pattern pattern = Pattern.compile(expr, Pattern.CASE_INSENSITIVE);
				Matcher matcher = pattern.matcher(varValue);
				if (matchSubstring) {
					retval = matcher.find();
				} else {
					retval = matcher.matches();
				}
				if (!variable.isName() && retval){ // find sequence hits
					matcher.reset();
					while(matcher.find()) {
						int start = matcher.start();
						int end = matcher.end();
						this.getSequenceHits().add(new Range(start,end));
					}
				}
				break;
			default:
				retval = false;
		}
		
		return retval;
	}

	@Override
	public Object visit(ASTVariable node, Sequence data) {
		return node.value;
	}

	/**
	 * @return the sequenceHits
	 */
	public List<Range<Integer>> getSequenceHits() {
		return sequenceHits;
	}
	
}
