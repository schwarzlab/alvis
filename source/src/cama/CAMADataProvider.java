package cama;

import alvis.algorithm.matrix.FisherScoresMatrix;
import com.general.utils.StringUtils;
import de.biozentrum.bioinformatik.alignment.MultipleAlignment;
import de.biozentrum.bioinformatik.ca.CAData;
import de.biozentrum.bioinformatik.ca.CADataModel;
import de.biozentrum.bioinformatik.ca.CADataProvider;
import de.biozentrum.bioinformatik.ca.annotation.CAAnnotation;
import de.biozentrum.bioinformatik.ca.annotation.CAAnnotationProvider;
import de.biozentrum.bioinformatik.ca.ext.alignment.MAHMMPositionMap;
import org.biojava.bio.BioException;
import org.biojava.bio.symbol.Symbol;
import org.jblas.DoubleMatrix;

public class CAMADataProvider implements CAAnnotationProvider, CADataProvider {

	FisherScoresMatrix fisherScores;
	MultipleAlignment alignment;
	private MAHMMPositionMap positionMap;
	
	public CAMADataProvider( FisherScoresMatrix fisherScores, MultipleAlignment multipleAlignment) {
		this.fisherScores = fisherScores;
		this.alignment = multipleAlignment;
		
	}
	
	public void setPositionMap( MAHMMPositionMap positionMap) {
		this.positionMap = positionMap;
	}
	
	public DoubleMatrix getDataMatrix() {
		return fisherScores;
	}

	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.ca.annotation.CAAnnotationProvider#getAnnotation(de.biozentrum.bioinformatik.ca.CAData, java.lang.String)
	 */
	public Object getAnnotation(CAData data, String key) {
		if ( key.equals( CAAnnotation.NAME ) ) {
			switch ( data.getDataSetIdentifier() ) {
				case CADataModel.FIRST_DATA_SET:
					return alignment.getSequence( data.getPosition() ).getName();
				case CADataModel.SECOND_DATA_SET:
					int column = data.getPosition() / fisherScores.getAlphabet().size() + 1;
					Symbol sym = fisherScores.getColumnHeader()[data.getPosition()];
					String uppercase;
					try {
						uppercase = fisherScores.getAlphabet().getTokenization("token").tokenizeSymbol(sym).toUpperCase();
					} catch (BioException ex) {
						uppercase = "<error>";
					}
				
				return uppercase + " (" + StringUtils.firstCharToCapital(sym.getName()) + ") " + ((this.positionMap != null) ? (this.positionMap.getMAColumn(column) + 1) : (column + 1));
			}
		}
		else if ( key.equals( CAAnnotation.IDENTIFIER ) ) {
			return data.getDataSetIdentifier() + "-" + data.getPosition();
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.ca.annotation.CAAnnotationProvider#getClass(java.lang.String)
	 */
	public Class getClass(String key) {
		if ( key.equals( CAAnnotation.NAME ) )
			return String.class;
		if ( key.equals( CAAnnotation.IDENTIFIER ) )
			return String.class;
		return null;
	}

	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.ca.annotation.CAAnnotationProvider#getLabel(java.lang.String)
	 */
	public String getLabel(String key) {
		return key;
	}

	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.ca.annotation.CAAnnotationProvider#getLabel(int)
	 */
	public String getLabel( int dataSetIdentifier ) {
		switch ( dataSetIdentifier ) {
			case CADataModel.FIRST_DATA_SET:
				return "Sequences";
			case CADataModel.SECOND_DATA_SET:
				return "Aminoacids on HMM position";
		}
		return "";
	}

	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.ca.annotation.CAAnnotationProvider#getKeys(int)
	 */
	public String[] getKeys( int dataSetIdentifier ) {
		return new String[] { CAAnnotation.NAME, CAAnnotation.IDENTIFIER };
	}

	public MultipleAlignment getAlignment() {
		return alignment;
	}

	public FisherScoresMatrix getFisherScores() {
		return fisherScores;
	}

}
