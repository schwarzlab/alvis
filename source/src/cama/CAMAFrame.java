package cama;

import alvis.Alvis.AlignmentFormat;
import alvis.AlvisDataModel.AlignmentType;
import alvis.algorithm.matrix.FisherScoresMatrix;
import de.biozentrum.bioinformatik.alignment.MultipleAlignment;
import de.biozentrum.bioinformatik.ca.CADataModel;
import de.biozentrum.bioinformatik.ca.CorrespondenceAnalysis;
import de.biozentrum.bioinformatik.ca.ext.CAExtensionRegistry;
import de.biozentrum.bioinformatik.ca.ext.alignment.AlignmentToCAPositionMapper;
import de.biozentrum.bioinformatik.ca.ext.plot.CAChartPanel;
import de.biozentrum.bioinformatik.ca.ext.plot.CAPlotToolbar;
import de.biozentrum.bioinformatik.ca.ext.plot.CAXYPlot;
import de.biozentrum.bioinformatik.ca.ext.selection.SelectionExtension;
import gui.MultipleAlignmentPanelExtension;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Event;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.KeyStroke;
import org.biojava.bio.seq.Sequence;
import org.biojava.bio.seq.io.SymbolTokenization;
import org.biojava.bio.symbol.AlphabetIndex;
import org.jfree.chart.JFreeChart;


public class CAMAFrame extends JFrame {

	AlignmentType type;
	AlignmentFormat format;
	
	double caProgress;
	
	//File file;
	
	JPanel view;
	AlignmentToCAPositionMapper extension;
	SelectionExtension selectionExtension;
	SelectionStorageExtension selectionStorageExtension;
	JCheckBox annotate;
	CAExtensionRegistry extRegistry;
	CAMADataProvider provider;
	CorrespondenceAnalysis ca;
	MultipleAlignment alnmodel;
	CAChartPanel panel;
	
	AlphabetIndex alphabetIndex;
		
	Action action;
	
	Action svgExportAction;
	Action fisherExportAction;
	private final MultipleAlignmentPanelExtension alignmentPanel;

	public Action getSVGExportAction() {
		return svgExportAction;
	}
	
	public Action getFisherExportAction() {
		return fisherExportAction;
	}
	
	public Action getAction() { return action; }
	
	CAMAConfiguration configuration;
	
	public CAMAConfiguration getConfiguration() {
		return configuration;
	}
	
	public CAMAFrame( CAMAConfiguration configuration, MultipleAlignmentPanelExtension alignmentPanel) {
		super(configuration.getName());
		
		this.configuration = configuration;
		this.alignmentPanel = alignmentPanel;
		
		
		
		
		
		svgExportAction = new AbstractAction( "Plot") {

			public void actionPerformed(ActionEvent e) {
				try {
					panel.doSaveAs();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			
		};
		
		fisherExportAction = new AbstractAction("FisherScores") {

			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser();
				int retval=chooser.showSaveDialog(CAMAFrame.this);
				if (retval == JFileChooser.APPROVE_OPTION) {
					try {
						File file=chooser.getSelectedFile();
						FileWriter writer=new FileWriter(file);
						
						FisherScoresMatrix dataMatrix=CAMAFrame.this.configuration.getFisherScores();

						AlphabetIndex index = CAMAFrame.this.configuration.getAlphabetIndex();
						//int symbolCount = BioJavaHelper.getAllSymbols(index.getAlphabet()).size();
						int symbolCount = index.getAlphabet().size();
						SymbolTokenization tokenization = index.getAlphabet().getTokenization("token");
						
						StringBuffer header=new StringBuffer();

						for (int j=0;j<dataMatrix.columns;j++) {
							header.append(",");
							header.append(tokenization.tokenizeSymbol(index.symbolForIndex(j % symbolCount)));
							header.append("_");
							header.append(j / symbolCount);
						}
						//header.append(tokenization.tokenizeSymbol(index.symbolForIndex((dataMatrix.columns()-1) % symbolCount)));
						header.append("\n");
						writer.write(header.toString());
						// generate fisher scores comma separated values file
						for (int i=0; i < dataMatrix.rows; i++) {
							StringBuffer fisherScores=new StringBuffer();
							Sequence s = CAMAFrame.this.configuration.getSequenceOrder()[i];
							fisherScores.append(s.getName());
							fisherScores.append(",");
							for (int j=0; j<dataMatrix.columns-1; j++) {
								fisherScores.append(dataMatrix.get(i, j));
								fisherScores.append(",");
							}
							fisherScores.append(dataMatrix.get(i,dataMatrix.columns-1));
							fisherScores.append("\n");
							writer.write(fisherScores.toString());
						}
						
						writer.close();
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				}
				
			}};
		
		action = new AbstractAction( configuration.getName()) {
			
			public void actionPerformed(ActionEvent e) {
//				try {
//					CAMAFrame.this.setSelected(true);
//				} catch (PropertyVetoException ex) {
//					ex.printStackTrace();
//				}
			}
		};

			
		this.setSize(200, 100);
		
	}
	
	public void createGUI() {
		extension = new AlignmentToCAPositionMapper(configuration.getMultipleAlignment(), configuration.getFisherScores(), alignmentPanel.getNameList().getSelectionModel(), alignmentPanel.getAlignmentView().getSelectionModel());
		extension.setColumnMask(configuration.getColumnMask());
		
		configuration.getDataProvider().setPositionMap(extension);
		
		selectionExtension = new SelectionExtension();
		selectionStorageExtension = new SelectionStorageExtension(configuration);
		
		JPanel c = new JPanel();

		c.setLayout(new BorderLayout());

		view = new JPanel();
		CADataModel model = configuration.getDataModel();
		
		CAXYPlot plot = new CAXYPlot( model);
		JFreeChart freeChart = new JFreeChart(null, JFreeChart.DEFAULT_TITLE_FONT, plot, false);
		
		freeChart.setAntiAlias(true);
		
	    panel = new CAChartPanel(freeChart);
		panel.setMouseWheelEnabled(true);
		panel.setMouseZoomable(true,true);
		panel.setDataModel(model);
		panel.restoreAutoBounds();
		
		CAPlotToolbar bar = new CAPlotToolbar( panel );
		bar.setModel(model);
		
		
		view.setLayout( new BorderLayout() );
		view.add( bar, BorderLayout.NORTH);
		view.add( panel, BorderLayout.CENTER);

		extRegistry = new CAExtensionRegistry();		
		extRegistry.add( selectionExtension);
		extRegistry.add( extension );
		extRegistry.add( selectionStorageExtension);
		
		
		
		JSplitPane sPane = new JSplitPane(  JSplitPane.VERTICAL_SPLIT );
		//sPane.putClientProperty( Options.EMBEDDED_TABS_KEY, Boolean.TRUE );
		sPane.setResizeWeight(0.5);
		sPane.setMinimumSize(new Dimension( 200, 200));
		
		selectionExtension.getComponent().setSize(new Dimension(100, 200));
		selectionStorageExtension.getComponent().setSize(new Dimension(100, 200));
		
		sPane.add(selectionExtension.getComponent() );
		sPane.add( selectionStorageExtension.getComponent() );
		
		
		
		JSplitPane horizontalSplit = new JSplitPane( JSplitPane.HORIZONTAL_SPLIT, sPane, view );
//		JSplitPane verticalSplit = new JSplitPane(  JSplitPane.VERTICAL_SPLIT, horizontalSplit, extension.getComponent() );
		
//		verticalSplit.setResizeWeight(0.6);
//		horizontalSplit.setResizeWeight(0.0);
		
		c.add(horizontalSplit, BorderLayout.CENTER);
		
		Container contentPane = getContentPane();
		contentPane.setLayout( new BorderLayout() );
		contentPane.add( c,BorderLayout.CENTER );
		
		extRegistry.setDataModel( configuration.getDataModel());
		
//		panel.restoreAutoBounds();		
//		panel.setFocusable(true);
		
		String chars = "0123456789qwertzuiopasdfghjklyxcvbnm";
		String upperCase = chars.toUpperCase();
		
		for ( int i = 0; i < chars.length(); i++) {
			char cc = chars.charAt(i);
			char uc = upperCase.charAt(i);
			String s = String.valueOf(cc) + "_CMD";
			
			((JComponent)getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(cc), s);
			((JComponent)getContentPane()).getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(uc, Event.CTRL_MASK), s);
			((JComponent)getContentPane()).getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke(uc, Event.CTRL_MASK), s);
			((JComponent)getContentPane()).getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke(cc), s);
			((JComponent)getContentPane()).getActionMap().put(s, selectionStorageExtension.createSelectionAction(cc));
		}
		
		//panel.getInputMap().put(KeyStroke.getKeyStroke('s'), "toggleLabels");
		//panel.getActionMap().put("toggleLabels", panel.getToggleLabelsAction());
		
		//extension.getAlignmentView().getInputMap().put(KeyStroke.getKeyStroke('s'), "toggleLabels");
		//extension.getAlignmentView().getActionMap().put("toggleLabels", panel.getToggleLabelsAction());
		
		
//		verticalSplit.setDividerLocation(0.6);
//		sPane.setDividerLocation( 0.5);
		this.pack();
		horizontalSplit.setDividerLocation(0.2);		
	}
	
	public void setShowsSelectedItemLabels( boolean flag) {
		((CAXYPlot)panel.getChart().getXYPlot()).showLabels(flag);
	}
	
	public boolean showsSelectedItemLabels() {
		return ((CAXYPlot)panel.getChart().getXYPlot()).showsLabels();
	}
	
}
