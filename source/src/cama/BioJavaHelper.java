package cama;

import de.biozentrum.bioinformatik.MAFactory;
import de.biozentrum.bioinformatik.alignment.MultipleAlignment;
import de.biozentrum.bioinformatik.sequence.BiojavaSequenceAdapter;
import de.biozentrum.bioinformatik.sequence.Sequence;
import de.biozentrum.bioinformatik.sequence.SequenceAlphabet;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.biojava.bio.BioException;
import org.biojava.bio.alignment.Alignment;
import org.biojava.bio.alignment.SimpleAlignment;
import org.biojava.bio.seq.DNATools;
import org.biojava.bio.seq.ProteinTools;
import org.biojava.bio.seq.RNATools;
import org.biojava.bio.seq.io.CharacterTokenization;
import org.biojava.bio.seq.io.SymbolTokenization;
import org.biojava.bio.symbol.AlphabetIndex;
import org.biojava.bio.symbol.AlphabetManager;
import org.biojava.bio.symbol.AtomicSymbol;
import org.biojava.bio.symbol.FiniteAlphabet;
import org.biojava.bio.symbol.IllegalSymbolException;
import org.biojava.bio.symbol.SimpleAlphabet;
import org.biojava.bio.symbol.SimpleSymbolList;
import org.biojava.bio.symbol.Symbol;
import org.biojava.bio.symbol.SymbolList;
import org.biojava.utils.ChangeListener;
import org.biojava.utils.ChangeType;
import org.biojava.utils.ChangeVetoException;
import org.biojavax.bio.seq.RichSequence;
import org.biojavax.bio.seq.RichSequenceIterator;

public class BioJavaHelper {

	public static void makeEqualLength(ArrayList<Sequence> sequences) {
		int maxLength = 0;
		for (Sequence sequence : sequences) {
			maxLength = Math.max(maxLength, sequence.length());
		}
		
		for (Sequence sequence : sequences) {
			sequence.fillUpWithGaps( maxLength );
		}
	}
	
	
	public static FiniteAlphabet guessAlphabet(File f) throws IOException {
		Reader r = new FileReader(f);
		Set<Character> sample = new HashSet<>();
		int chars = 0;
		int c = r.read();

		while (c != -1 && chars < 2048) {

			// ignore lines starting with '>'
			Character character = (char) c;
			if (character == '>') {
				while (c!= -1 && character != '\n') {
					c = r.read();
					character = (char) c;
				}
			}
			
			if (character == ' '
					|| character == '\t'
					|| character == '\n'
					|| character == '\r') {
				c = r.read();
				continue;
			}

			character = ((String.valueOf(character)).toUpperCase()).charAt(0);
			
			sample.add(character);

			chars++;

			c = r.read();
		}

		if (testAlphabet(sample, RNATools.getRNA())) {
			return RNATools.getRNA();
		} else if (testAlphabet(sample, DNATools.getDNA())) {
			return DNATools.getDNA();
		} else if (testAlphabet(sample, ProteinTools.getAlphabet())) {
			return ProteinTools.getAlphabet();
		} else {
			return null;
		}

	}
	
	private static boolean testAlphabet(Set<Character> sample, FiniteAlphabet alphabet) {
		boolean retval = true;
		SymbolTokenization st;
		try {
			st = alphabet.getTokenization("token");
		} catch (BioException ex) {
			return false;
		}
		for (Character c: sample) {
			try {
				Symbol s = st.parseToken(c.toString());
			} catch (IllegalSymbolException ex) {
				retval = false;
				break;
			}
		}
		return retval;
	}

	public static Alignment createAlignment(RichSequenceIterator iterator, List<RichSequence> sequenceOrder) throws NoSuchElementException, BioException {
		Map<String, SymbolList> map = new HashMap<>();
		while (iterator.hasNext()) {
			RichSequence seq = iterator.nextRichSequence();
			map.put(seq.getName(), seq);
			sequenceOrder.add(seq);
		}

		return new SimpleAlignment(map);
	}

	private static class SymbolComparator implements Comparator<Symbol> {

		private SymbolTokenization tokenizer;

		public SymbolComparator(SymbolTokenization tokenizer) {
			this.tokenizer = tokenizer;
		}

		@Override
		public int compare(Symbol o1, Symbol o2) {
			try {
				String s1 = tokenizer.tokenizeSymbol(o1);
				String s2 = tokenizer.tokenizeSymbol(o2);
				return s1.compareTo(s2);
			} catch (IllegalSymbolException ex) {
				throw new ClassCastException();
			}
		}
	}

	private static class NonatomicAlphabetIndex implements AlphabetIndex {

		List<Symbol> symbols;
		private FiniteAlphabet alphabet;

		public NonatomicAlphabetIndex(FiniteAlphabet alphabet) {
			this.alphabet = alphabet;
			this.symbols = new ArrayList<>(alphabet.size());
			SymbolTokenization tokenization;
			try {
				tokenization = alphabet.getTokenization("token");
			} catch (BioException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			Iterator<Symbol> iterator = alphabet.iterator();

			while (iterator.hasNext()) {
				symbols.add(iterator.next());
			}

			Set<Symbol> allSymbols = getAllSymbols(alphabet);
			for (Symbol s : allSymbols) {
				if (!symbols.contains(s)) {
					symbols.add(s);
				}

			}


			/*if ( this.tokenization != null) {
			 Collections.sort(this.symbols, new Comparator<Symbol>() {
					
			 public int compare(Symbol o1, Symbol o2) {
			 try {
			 return tokenization.tokenizeSymbol(o1).compareToIgnoreCase(tokenization.tokenizeSymbol(o2));
			 } catch (IllegalSymbolException e) {
			 //passiert nicht!! hoffentlich :-)
			 }
			 return 0;
			 }
			 });
			 }*/
		}

		@Override
		public FiniteAlphabet getAlphabet() {
			return alphabet;
		}

		@Override
		public int indexForSymbol(Symbol arg0) throws IllegalSymbolException {
			return symbols.indexOf(arg0);
		}

		@Override
		public Symbol symbolForIndex(int arg0) throws IndexOutOfBoundsException {
			return symbols.get(arg0);
		}

		@Override
		public void addChangeListener(ChangeListener arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void addChangeListener(ChangeListener arg0, ChangeType arg1) {
			// TODO Auto-generated method stub

		}

		@Override
		public boolean isUnchanging(ChangeType arg0) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public void removeChangeListener(ChangeListener arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void removeChangeListener(ChangeListener arg0, ChangeType arg1) {
			// TODO Auto-generated method stub

		}

	}

	public static Set<Symbol> getAllSymbols(FiniteAlphabet alphabet) {
		Set<Symbol> set = new HashSet<>();

		String possibleSymbols = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		try {
			SymbolTokenization tokenization = alphabet.getTokenization("token");
			for (int i = 0; i < possibleSymbols.length(); i++) {
				String token = possibleSymbols.substring(i, i + 1);
				try {
					Symbol s = tokenization.parseToken(token);
					set.add(s);
				} catch (Exception e) {

				}
			}
		} catch (BioException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return set;
	}

	public static AlphabetIndex createNonatomicAlphabetIndex(FiniteAlphabet alphabet) {
		return new BioJavaHelper.NonatomicAlphabetIndex(alphabet);
	}

	public static de.biozentrum.bioinformatik.sequence.Sequence convertSequence(SymbolList sequence, String name) throws BioException {
		return convertSequence(sequence, name, true);
	}

	public static de.biozentrum.bioinformatik.sequence.Sequence convertSequence(SymbolList sequence, String name, boolean enforceUpperCase) throws BioException {
		return new BiojavaSequenceAdapter(sequence, name, enforceUpperCase);
	}

	public static SymbolList convertSequence(Sequence sequence) {
		if (sequence instanceof BiojavaSequenceAdapter) {
			return ((BiojavaSequenceAdapter)sequence).toSymbolList();
		} else {
			throw new UnsupportedOperationException("NIY");
		}
	}
	
	
	/** converts a BZ alignment to a BJ alignment with no guarantee about the ordering of the sequences
	 * 
	 * @param alignment
	 * @param type
	 * @return 
	 * @throws org.biojava.bio.BioException 
	 */
	public static Alignment convertAlignment(MultipleAlignment alignment) throws BioException{
		FiniteAlphabet alpha = convertAlphabet(alignment.getAlphabet());
		Map<String, SymbolList> almap = new HashMap<>();
		for (de.biozentrum.bioinformatik.sequence.Sequence s: alignment){
			String name = s.getName();
			SymbolList sl;
			if (s instanceof BiojavaSequenceAdapter) {
				sl = ((BiojavaSequenceAdapter)s).toSymbolList();
			} else {
				sl = new SimpleSymbolList(alpha.getTokenization("token"), s.getAlignedSequence());
			}
			almap.put(name, sl);
		}
		SimpleAlignment bjAlignment = new SimpleAlignment(almap);
		return bjAlignment;
	}
	
	public static MultipleAlignment convertAlignment(Alignment alignment, String[] labelOrder, boolean toUpperCase) throws BioException {
		ArrayList<de.biozentrum.bioinformatik.sequence.Sequence> sequences = new ArrayList<>();

		FiniteAlphabet alph = null;
		for (String label : labelOrder) {
			SymbolList seq = alignment.symbolListForLabel(label);
			if (alph == null) {
				alph = (FiniteAlphabet) seq.getAlphabet();
			}
			de.biozentrum.bioinformatik.sequence.Sequence bzSequence = new BiojavaSequenceAdapter(seq, label, toUpperCase);
			sequences.add(bzSequence);				
		}

		MultipleAlignment mul = MAFactory.createMultipleAlignment(sequences);
		mul.setAlphabet(convertAlphabet(alph, toUpperCase));
		return mul;
	}
	

	public static de.biozentrum.bioinformatik.sequence.SequenceAlphabet convertAlphabet(FiniteAlphabet alphabet) {
		return convertAlphabet(alphabet, false);
	}

	public static de.biozentrum.bioinformatik.sequence.SequenceAlphabet convertAlphabet(FiniteAlphabet alphabet, boolean enforceUpperCase) {
		de.biozentrum.bioinformatik.sequence.SequenceAlphabet al = new de.biozentrum.bioinformatik.sequence.SequenceAlphabet();
		if (alphabet.equals(DNATools.getDNA())) { // DNA
			al.addAll(de.biozentrum.bioinformatik.sequence.SequenceAlphabet.NUCLEOTIDES);
		} else if (alphabet.equals(ProteinTools.getAlphabet())) { // AA
			al.addAll(de.biozentrum.bioinformatik.sequence.SequenceAlphabet.AMINOACIDS);
		} else if (alphabet.equals(RNATools.getRNA())) { // RNA
			al.addAll(de.biozentrum.bioinformatik.sequence.SequenceAlphabet.RNA);
		} else { // CUSTOM
			try {
				SymbolTokenization tokenization = alphabet.getTokenization("token");
				Iterator<Symbol> it = alphabet.iterator();
				while (it.hasNext()) {
					Symbol next = it.next();
					if (enforceUpperCase) {
						al.addCharacter(new Character(tokenization.tokenizeSymbol(next).toUpperCase().charAt(0)));
					} else {
						al.addCharacter(new Character(tokenization.tokenizeSymbol(next).charAt(0)));
					}
				}
			} catch (BioException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return al;
	}

	public static FiniteAlphabet convertAlphabet(SequenceAlphabet alphabet) {
		if (alphabet.equals(SequenceAlphabet.AMINOACIDS)) {
			return ProteinTools.getAlphabet();
		} else if (alphabet.equals(SequenceAlphabet.NUCLEOTIDES)) {
			return DNATools.getDNA();
		} else if (alphabet.equals(SequenceAlphabet.RNA)) {
			return RNATools.getRNA();
		} else {
			SimpleAlphabet newalphabet = new SimpleAlphabet();
			CharacterTokenization tokenizer = new CharacterTokenization(newalphabet, false);
			newalphabet.putTokenization("token", tokenizer);
			for (Character c : alphabet) {
				AtomicSymbol symbol = AlphabetManager.createSymbol(String.valueOf(c).toUpperCase(), null);
				try {
					newalphabet.addSymbol(symbol);
					tokenizer.bindSymbol(symbol, c);
				} catch (IllegalSymbolException | ChangeVetoException ex) {
					ex.printStackTrace();
				}
			}
			return newalphabet;
		}

	}

	static class CharComparator implements Comparator {

		private Map<Character, Integer> counts;

		public CharComparator(Map<Character, Integer> counts) {
			this.counts = counts;
		}

		@Override
		public int compare(Object o1, Object o2) {
			if (o1 instanceof Character
					&& o2 instanceof Character) {
				Character c1 = (Character) o1;
				Character c2 = (Character) o2;

				return counts.get(c1) > counts.get(c2) ? -1 : 1;
			}
			return 0;
		}

	}

}
