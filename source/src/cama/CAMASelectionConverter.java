/*
 * Created on 20.05.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package cama;

import de.biozentrum.bioinformatik.alignment.MultipleAlignment;
import de.biozentrum.bioinformatik.alignment.events.AlignmentSelectionEvent;
import de.biozentrum.bioinformatik.alignment.events.AlignmentSelectionListener;
import de.biozentrum.bioinformatik.alignment.events.MAEvent.MAEventType;
import de.biozentrum.bioinformatik.alignment.selection.AlignmentSelectionModel;
import de.biozentrum.bioinformatik.ca.CAData;
import de.biozentrum.bioinformatik.ca.CADataChangedEvent;
import de.biozentrum.bioinformatik.ca.CADataListener;
import de.biozentrum.bioinformatik.ca.CADataModel;
import de.biozentrum.bioinformatik.ca.ext.alignment.MAHMMPositionMap;
import java.awt.Rectangle;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javax.swing.ListSelectionModel;
import org.biojava.bio.BioException;
import org.biojava.bio.symbol.Alphabet;
import org.biojava.bio.symbol.AlphabetIndex;
import org.biojava.bio.symbol.FiniteAlphabet;
import org.biojava.bio.symbol.IllegalSymbolException;
import org.biojava.bio.symbol.Symbol;

/**
 * @author pseibel
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class CAMASelectionConverter implements CADataListener, AlignmentSelectionListener {

	private CADataModel caModel;
	private AlignmentSelectionModel alignmentSelectionModel;
	private MultipleAlignment alignment;
	private AlphabetIndex alphabetIndex;
	private MAHMMPositionMap positionMap;
	private Alphabet alphabet;
	
	private Map<Character, Symbol> charToSym;
	
	private boolean blockedByCA = false;
	private boolean blockedByAlignment = false;
	
	private boolean multipleAlignmentSelections;
	private boolean multipleCASelections;
	
	public CAMASelectionConverter( CADataModel caModel, AlignmentSelectionModel alignmentSelectionModel, ListSelectionModel sequenceSelectionModel, ListSelectionModel columnSelectionModel, AlphabetIndex alphabetIndex, Alphabet alphabet, MAHMMPositionMap positionMap ) {
		this.caModel = caModel;
		this.alignmentSelectionModel = alignmentSelectionModel;
		this.alphabetIndex = alphabetIndex;
		this.alignment = alignmentSelectionModel.getAlignment();
		this.positionMap = positionMap;
		this.alphabet = alphabet;
		
		this.charToSym = new HashMap<Character, Symbol>();
		
//		caModel.addCADataListener( this );
//		alignmentSelectionModel.addAlignmentSelectionListener( this );
	}
	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.ca.CADataListener#selectionChanged(de.biozentrum.bioinformatik.ca.CADataChangedEvent)
	 */
	public void selectionChanged(CADataChangedEvent e) {
		if ( !blockedByAlignment) {
			boolean wasBlocked = blockedByCA;
			blockedByCA = true;
		
			CAData[] data = e.getData();
		
			for ( int i = 0; i < data.length; i++ ) {
				if ( data[ i].getDataSetIdentifier() == CADataModel.SECOND_DATA_SET ) {
					int hmmCol = (data[i].getPosition() / alphabetIndex.getAlphabet().size()) + 1;
					int col = positionMap.getMAColumn( hmmCol );
					if ( col == -1 )
						continue;
					
					Symbol selectedSymbol = alphabetIndex.symbolForIndex( data[i].getPosition() % alphabetIndex.getAlphabet().size() );
					
					
					for ( int j = 0; j < alignment.getSequenceCount(); j++ ) {
						Character c = alignment.characterAt(j, col);
						
	
							Symbol s = charToSym.get(c);
							if ( s == null) {
								try {
									s = alphabet.getTokenization("token").parseToken(String.valueOf(c));
									charToSym.put(c, s);
								} catch (IllegalSymbolException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								} catch (BioException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}
							}
							if ( s.getMatches().contains( selectedSymbol) 
									&& !selectedSymbol.getMatches().getGapSymbol().equals(s)) {
								if ( e.getType() == CADataChangedEvent.INSERTED ) {
									alignmentSelectionModel.select( j, col, false);
								}
								else {
									alignmentSelectionModel.deselect(j, col);
								}
							}	
		
						
						
					}
				}
			}
		blockedByCA = wasBlocked;
		}
	}

	public void updateWithEvent( AlignmentSelectionEvent e) {
		Rectangle rect = e.getRectangle();
		for ( int seq = (int) rect.getMinY(); seq < rect.getMaxY(); seq++ ) {
			for ( int column = (int) rect.getMinX(); column < rect.getMaxX(); column++ ) {
				Character character = alignmentSelectionModel.getAlignment().characterAt(seq,column);
				Symbol symbol = charToSym.get( character);
				if ( symbol == null) {
					
					try {
						symbol = alphabet.getTokenization("token").parseToken(String.valueOf(character));
						charToSym.put(character, symbol);
					} catch (IllegalSymbolException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (BioException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
				
				
				int maColumn = column;
				
//					for ( int i = 0; i < alignment.getSequenceCount(); i++ ) {
//						if ( alignment.characterAt( i, maColumn ).equals(character)  ) {
//							if ( e.getType() == MAEventType.INSERT 
//									&&  !alignmentSelectionModel.isSelected( i, maColumn, false )) {
//								alignmentSelectionModel.select( i, maColumn, false );
//							}
//							else if (e.getType() == MAEventType.DELETE 
//									&&  alignmentSelectionModel.isSelected( i, maColumn, false )) {
//								alignmentSelectionModel.deselect( i, maColumn);
//							}
//						}
//					}
					if ( character.charValue() != '-' ) {
						int hmmColumn = positionMap.getHMMPosition( maColumn );
						if ( hmmColumn == -1 )
							continue;
						
						Iterator<Symbol> iterator = ((FiniteAlphabet) symbol.getMatches()).iterator();
						while ( iterator.hasNext() ) {
							Symbol s = iterator.next();
							if ( s.equals( alphabet.getGapSymbol()))
								continue;
							
							
							int fisherPos = - 1;
							try {
								fisherPos = (hmmColumn-1) * alphabetIndex.getAlphabet().size() + alphabetIndex.indexForSymbol( s );
							} catch (IllegalSymbolException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							
							if ( fisherPos == -1) {
								//GAP
								continue;
							}
							if ( e.getType() == MAEventType.INSERT ) {
								
								caModel.select( CADataModel.SECOND_DATA_SET, fisherPos );
							}
							else {
								caModel.removeSelection( CADataModel.SECOND_DATA_SET, fisherPos );
							}
						}
					}
				
				
			}
		}
	}
	
	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.ca.ext.alignment.MASelectionListener#selectionChanged(de.biozentrum.bioinformatik.ca.ext.alignment.MASelectionEvent)
	 */
	
	
	@Override
	public void selectionChanged( AlignmentSelectionEvent e ) {
		if ( !blockedByCA) {
			boolean wasBlocked = blockedByAlignment;
			blockedByAlignment = true;
		
			updateWithEvent(e);
		
			blockedByAlignment = wasBlocked;
		}
	}
	@Override
	public void selectionDidEnd() {
		if ( blockedByAlignment) {
			caModel.commitSelectionTransaction();
			blockedByAlignment = false;
			multipleAlignmentSelections = false;
		}
		
	}
	@Override
	public void selectionWillBegin() {
		if ( !blockedByCA) {
			blockedByAlignment = true;
			multipleAlignmentSelections = true;
			caModel.beginSelectionTransaction();
		}
		
	}
	public void selectionTransactionDidCommit() {
		if ( blockedByCA) {
			alignmentSelectionModel.end();
			blockedByCA = false;
			multipleCASelections = false;
		}
	}
	
	public void selectionTransactionWillBegin() {
		if ( !blockedByAlignment) {
			blockedByCA = true;
			multipleCASelections = true;
			alignmentSelectionModel.begin();
		}
		
	}
}
