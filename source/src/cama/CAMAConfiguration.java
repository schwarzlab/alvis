package cama;

import alvis.AlvisDataModel.AlignmentType;
import alvis.algorithm.matrix.FisherScoresMatrix;
import de.biozentrum.bioinformatik.alignment.MultipleAlignment;
import de.biozentrum.bioinformatik.ca.CADataModel;
import de.biozentrum.bioinformatik.ca.CADefaultModel;
import de.biozentrum.bioinformatik.ca.CorrespondenceAnalysis;
import de.biozentrum.bioinformatik.ca.ext.selection.SelectionStore;
import de.biozentrum.bioinformatik.sequence.SequenceColorModel;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import org.biojava.bio.alignment.Alignment;
import org.biojava.bio.alignment.SimpleAlignment;
import org.biojava.bio.seq.Sequence;
import org.biojava.bio.symbol.Alphabet;
import org.biojava.bio.symbol.AlphabetIndex;
import org.biojava.bio.symbol.FiniteAlphabet;
import org.biojava.bio.symbol.SymbolList;

public class CAMAConfiguration implements Serializable {
	


	/**
	 * 
	 */
	private static final long serialVersionUID = 7363718080340490835L;

	
	private SequenceColorModel sequenceColorModel;
	
	private String name;
	
	/*
	 * @serial
	 */
	private Alphabet alphabet;
	
	/*
	 * @serial
	 */
	private transient AlphabetIndex alphabetIndex;
	
	/*
	 * @serial
	 */
	private transient Alignment alignment;
	
	
	private CorrespondenceAnalysis corresponenceAnalysis;
	
	private transient CADataModel dataModel;
	private transient CAMADataProvider dataProvider;
	
	//public String[] sequenceNames;
	
	/*
	 * @serial
	 */
	private Sequence[] sequenceOrder;
	/*
	 * @serial
	 */
	private boolean[] columnMask;
	
	
	private SelectionStore selectionStore;
	
	public CAMAConfiguration() {
		this.selectionStore = new SelectionStore();
	}
	/**
	   * Always treat de-serialization as a full-blown constructor, by
	   * validating the final state of the de-serialized object.
	   */
	   private void readObject(
	     ObjectInputStream aInputStream
	   ) throws ClassNotFoundException, IOException {
	     //always perform the default de-serialization first
	     aInputStream.defaultReadObject();
	     
	     String[] sequenceNames = new String[sequenceOrder.length];
	     int index = 0;
	     
	     Map<String, SymbolList> map = new HashMap<String,SymbolList>();
	    for ( Sequence seq : sequenceOrder) {
	    	sequenceNames[index] = seq.getName();
	    	index++;
	    	map.put(seq.getName(), seq);
	    }
	    
	    alignment = new SimpleAlignment( map);
	    
//	    setAlphabetIndex(BioJavaHelper.createNonatomicAlphabetIndex(getAlphabet()));
		
	    
//	     setMultipleAlignment(BioJavaHelper.convertAlignment(getAlignment(), sequenceNames));
	     
	     setDataProvider(new CAMADataProvider( getFisherScores(), getMultipleAlignment()));
	     setDataModel(new CADefaultModel( getCorresponenceAnalysis()));
	     getDataModel().setAnnotation(getDataProvider());
	  }

	    /**
	    * This is the default implementation of writeObject.
	    * Customise if necessary.
	    */
	    private void writeObject(
	      ObjectOutputStream aOutputStream
	    ) throws IOException {
	      //perform the default serialization for all non-transient, non-static fields
	      aOutputStream.defaultWriteObject();
	    }

		public void setName(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}

		public void setAlphabet(Alphabet alphabet) {
			this.alphabet = alphabet;
		}

		public Alphabet getAlphabet() {
			return alphabet;
		}

		public void setAlphabetIndex(AlphabetIndex alphabetIndex) {
			this.alphabetIndex = alphabetIndex;
		}

		public AlphabetIndex getAlphabetIndex() {
			return alphabetIndex;
		}

		public MultipleAlignment getMultipleAlignment() {
			return dataProvider.getAlignment();
		}

		public void setAlignment(Alignment alignment) {
			this.alignment = alignment;
		}

		public Alignment getAlignment() {
			return alignment;
		}

		public FisherScoresMatrix getFisherScores() {
			return dataProvider.getFisherScores();
		}

		public void setCorresponenceAnalysis(CorrespondenceAnalysis corresponenceAnalysis) {
			this.corresponenceAnalysis = corresponenceAnalysis;
		}

		public CorrespondenceAnalysis getCorresponenceAnalysis() {
			return corresponenceAnalysis;
		}

		public void setDataModel(CADataModel dataModel) {
			this.dataModel = dataModel;
			this.selectionStore.setModel(dataModel);
		}

		public CADataModel getDataModel() {
			return dataModel;
		}

		public void setDataProvider(CAMADataProvider dataProvider) {
			this.dataProvider = dataProvider;
		}

		public CAMADataProvider getDataProvider() {
			return dataProvider;
		}

		public void setColumnMask(boolean[] columnMask) {
			this.columnMask = columnMask;
		}

		public boolean[] getColumnMask() {
			return columnMask;
		}

		public void setSequenceOrder(Sequence[] sequenceOrder) {
			this.sequenceOrder = sequenceOrder;
		}

		public Sequence[] getSequenceOrder() {
			return sequenceOrder;
		}

		public SelectionStore getSelectionStore() {
			return selectionStore;
		}
}
