package cama;

import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;

import de.biozentrum.bioinformatik.ca.CAData;
import de.biozentrum.bioinformatik.ca.CADataChangedEvent;
import de.biozentrum.bioinformatik.ca.CADataListener;
import de.biozentrum.bioinformatik.ca.CADataModel;
import de.biozentrum.bioinformatik.ca.ext.CAExtension;
import de.biozentrum.bioinformatik.ca.ext.selection.SelectionStore.Selection;


public class SelectionStorageExtension implements CAExtension {

	private CADataModel model;
	private JScrollPane component;
	private JTable table;
	
	private CAMAConfiguration configuration;
	private transient boolean initiatedSelection;
	private transient boolean doNotClear;
	
	public SelectionStorageExtension( CAMAConfiguration configuration) {
		this.configuration = configuration;
				
		table = new JTable( configuration.getSelectionStore().getTableModel());
		table.addKeyListener( new KeyListener() {

			public void keyPressed(KeyEvent e) {
				if ( e.getKeyCode() == KeyEvent.VK_BACK_SPACE
						|| e.getKeyCode() == KeyEvent.VK_DELETE) {
					int selectedRow = table.getSelectedRow();
					if ( selectedRow > -1) 
						SelectionStorageExtension.this.configuration.getSelectionStore().remove(selectedRow);
				}
				
			}

			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}

			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}});
		
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.getSelectionModel().addListSelectionListener( new ListSelectionListener() {
			
			public void valueChanged(ListSelectionEvent e) {
				int selectedRow = table.getSelectedRow();
				if ( selectedRow >= 0) {
					model.clearSelection();
					model.select(SelectionStorageExtension.this.configuration.getSelectionStore().getSelection(selectedRow).getData()[0]);
					model.select(SelectionStorageExtension.this.configuration.getSelectionStore().getSelection(selectedRow).getData()[1]);
				}
			}
		});
		
		component = new JScrollPane( table);	
	}
	
	public JComponent getComponent() {
		return component;
	}

	public String getName() {
		return "Selection Store";
	}

	public void setModel(CADataModel model) {
		this.model = model;
		this.model.addCADataListener( new CADataListener() {
			
			
			
			public void selectionChanged(CADataChangedEvent e) {
				if ( !doNotClear) {
					table.clearSelection();
				}
				
			}

			public void selectionTransactionDidCommit() {
				// TODO Auto-generated method stub
				
			}

			public void selectionTransactionWillBegin() {
				// TODO Auto-generated method stub
				
			}
		});
	}

	
	public Action createSelectionAction( char key) {
		return new SelectionAction(key);
	}

	private class SelectionAction extends AbstractAction {

		private char key;
		
		public SelectionAction( char key) {
			this.key = key;
		}
		
		public void actionPerformed(ActionEvent e) {
			//char key = e.getActionCommand().charAt(0);
			if ( (e.getModifiers() & InputEvent.CTRL_MASK) != 0 
					|| (e.getModifiers() & InputEvent.CTRL_DOWN_MASK) != 0 ) {
				configuration.getSelectionStore().addSelection(  "Selection", key);
				doNotClear = true;
				table.clearSelection();
				table.setRowSelectionInterval(configuration.getSelectionStore().size()-1,configuration.getSelectionStore().size()-1);
				doNotClear = false;
				initiatedSelection = true;
			}
			else {
				int index = configuration.getSelectionStore().indexForKey(key);
				if ( index >= 0) { 
					doNotClear = true;
					table.clearSelection();
					table.setRowSelectionInterval(index, index);
					doNotClear = false;
					initiatedSelection = true;
				}
			}
		}
	}
}
