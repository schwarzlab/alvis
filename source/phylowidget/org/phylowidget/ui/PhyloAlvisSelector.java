package org.phylowidget.ui;

import org.andrewberman.ui.UIEvent;
import org.andrewberman.ui.ifaces.UIListener;
import org.phylowidget.PWContext;
import org.phylowidget.PWPlatform;
import org.phylowidget.render.NodeRange;
import org.phylowidget.tree.PhyloNode;
import processing.core.PApplet;

import java.awt.event.MouseEvent;
import org.phylowidget.alvis.PhyloWidgetExtension;

public class PhyloAlvisSelector extends PhyloContextMenu implements UIListener
{
	public PhyloAlvisSelector(PApplet p)
	{
		super(p);
		addListener(this);
	}

    public void open(NodeRange r, MouseEvent e) {
        super.open(r);

        PWContext context = PWPlatform.getInstance().getThisAppContext();
        PhyloWidgetExtension pw = (PhyloWidgetExtension) context.getPW();

        if (r.node == null) {
            curNodeRange = null;
            pw.setSelectedNode(null, e.isControlDown());
        } else {
            PhyloNode selected = curNodeRange.node;
            pw.setSelectedNode(selected, e.isControlDown());
        }

        close();
    }

    public void uiEvent(UIEvent e)
    {

    }

}
