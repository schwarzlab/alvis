package org.phylowidget.alvis;

import de.biozentrum.bioinformatik.sequence.Sequence;
import gui.sequencebundle.SequenceBundleColorModel;
import org.phylowidget.PhyloTree;

import java.awt.*;
import java.util.*;
import javax.swing.*;

import org.phylowidget.ui.PhyloUI;

/**
 * Author: Asif Tamuri (tamuri@ebi.ac.uk) and a bit of Roland (for the space station)
 * Date: 14/02/2014 18:28
 */
public class PhyloWidgetWrapper extends JFrame implements AlvisPhyloWidgetAPI{

	private final PhyloWidgetExtension phyloWidgetApplet;

	public PhyloWidgetWrapper(PhyloWidgetExtension phyloWidgetApplet){
		super("Alvis - Tree view");
		this.phyloWidgetApplet=phyloWidgetApplet;
		this.getContentPane().setLayout(new BorderLayout());
		this.getContentPane().add(phyloWidgetApplet, BorderLayout.CENTER);
	}

	@Override
	public Dimension getPreferredSize() {
		return new Dimension(800,600);
	}

	
	@Override
	public void setSequences(Iterable<Sequence> sequences) {
		phyloWidgetApplet.setSequences(sequences);
	}

	@Override
	public void setSequenceGroups(Map<Sequence, Integer> groups) {
		phyloWidgetApplet.setSequenceGroups(groups);
	}

	@Override
	public void setColorModel(SequenceBundleColorModel colorModel) {
		phyloWidgetApplet.setColorModel(colorModel);
	}

	@Override
	public void setTree(PhyloTree tree) {
		phyloWidgetApplet.setTree(tree);
	}

	@Override
	public void update() {
		phyloWidgetApplet.update();
	}

	@Override
	public int[] getSelectedIndices() {
		return phyloWidgetApplet.getSelectedIndices();
	}

    @Override
    public ListSelectionModel getListSelectionModel() {
        return phyloWidgetApplet.getListSelectionModel();
    }

    @Override
    public void setListSelectionModel(ListSelectionModel model) {
        phyloWidgetApplet.setListSelectionModel(model);
    }

    @Override
	public void setSelectedIndices(int[] selectedIndices) {
		phyloWidgetApplet.setSelectedIndices(selectedIndices);
	}

    @Override
    public int[] getSequenceNodeOrder() {
        return phyloWidgetApplet.getSequenceNodeOrder();
    }

    @Override
	public PhyloUI getUI() {
		return phyloWidgetApplet.getUI();
	}
}
