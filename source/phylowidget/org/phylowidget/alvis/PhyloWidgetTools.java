/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.phylowidget.alvis;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import org.phylowidget.PhyloTree;
import org.phylowidget.tree.TreeIO;

/**
 *
 * @author Roland Schwarz <rfs32@cam.ac.uk>
 */
public class PhyloWidgetTools {
	public static PhyloTree loadTree(File file) throws Exception{ // seriously? great exception handling phylowidget...
		PhyloTree tree = new PhyloTree();
		TreeIO.parseReader(tree, new BufferedReader(new FileReader(file)));
		return tree;
	}
	
	public static void saveTree(PhyloTree tree, File file) throws IOException{ // seriously? great exception handling phylowidget...
		String newick = TreeIO.createNewickString(tree);
		BufferedWriter writer = new BufferedWriter(new FileWriter(file));
		writer.write(newick);
		writer.flush();
		writer.close();
	}
	
}
