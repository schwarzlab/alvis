package org.phylowidget.alvis;

import java.awt.Dimension;
import javax.swing.JFrame;
import org.phylowidget.PWContext;
import org.phylowidget.PWPlatform;

/**
 * Author: Asif Tamuri (tamuri@ebi.ac.uk)
 * Date: 14/02/2014 18:29
 */
public class PhyloWidgetManager {

    private PhyloWidgetManager(){}

    private final static PhyloWidgetWrapper WRAPPER;
    static{
        PhyloWidgetExtension pApplet = new PhyloWidgetExtension();

        WRAPPER = new PhyloWidgetWrapper(pApplet);
        WRAPPER.setSize(new Dimension(1000, 1000));
        WRAPPER.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);

        pApplet.init();
        PWContext context = (PWContext) PWPlatform.getInstance().registerApp(pApplet);
        context.config().setMenus("context-alvis.xml;dock.xml;toolbar-alvis.xml");

    }

    public static PhyloWidgetWrapper createPhyloWidgetWrapper() {
		return WRAPPER;
    }
}
