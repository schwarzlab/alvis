package org.phylowidget.alvis;

import de.biozentrum.bioinformatik.sequence.Sequence;
import gui.sequencebundle.SequenceBundleColorModel;
import org.phylowidget.PhyloTree;
import org.phylowidget.tree.PhyloNode;
import org.phylowidget.tree.TreeIO;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Author: Asif Tamuri (tamuri@ebi.ac.uk)
 * Date: 14/02/2014 18:15
 */
public class Test {
    private static final String TEST_TREE_1 = "((A, (B, C)), (D, (E, F)));";
    private static final String TEST_TREE_2 = "((One,((Two,Three),(Four,five))),('Six 6',(('Seven 7',Eight_8),('9,Nine',10))));";

    private void run() {
        final PhyloWidgetWrapper pw = PhyloWidgetManager.createPhyloWidgetWrapper();

        // Example: setting the tree
        PhyloTree tree = (PhyloTree) TreeIO.parseNewickString(new PhyloTree(), TEST_TREE_2);
        pw.setTree(tree);

        // Example: setting the sequences. NOTE: tree leaf names are expected to equal sequence names!
        List<Sequence> sequences = new ArrayList<>();
        Sequence A = new Sequence("One", "TCAGTCAGTCAGTCAGTCAG");
        Sequence B = new Sequence("Two", "TCAGTCAGTCAGTCAGTCAG");
        Sequence C = new Sequence("Three", "TCAGTCAGTCAGTCAGTCAG");
        Sequence D = new Sequence("Four", "TCAGTCAGTCAGTCAGTCAG");
        Sequence E = new Sequence("five", "TCAGTCAGTCAGTCAGTCAG");
        Sequence F = new Sequence("Six 6", "TCAGTCAGTCAGTCAGTCAG");
        Sequence G = new Sequence("Seven 7", "TCAGTCAGTCAGTCAGTCAG");
        Sequence H = new Sequence("Eight_8", "TCAGTCAGTCAGTCAGTCAG");
        Sequence I = new Sequence("9,Nine", "TCAGTCAGTCAGTCAGTCAG");
        Sequence J = new Sequence("10", "TCAGTCAGTCAGTCAGTCAG");
        sequences.add(A);
        sequences.add(B);
        sequences.add(C);
        sequences.add(D);
        sequences.add(E);
        sequences.add(F);
        sequences.add(G);
        sequences.add(H);
        sequences.add(I);
        sequences.add(J);
        pw.setSequences(sequences);

        Map<Sequence, Integer> groups = new HashMap<>();
        groups.put(A, 0);
        groups.put(B, 1);
        groups.put(C, 1);
        groups.put(D, 2);
        groups.put(E, 3);
        groups.put(F, 3);
        groups.put(G, 2);
        groups.put(H, 2);
        groups.put(I, 1);
        groups.put(J, 0);
        pw.setSequenceGroups(groups);

        SequenceBundleColorModel colorModel = new SequenceBundleColorModel();
        colorModel.getGroupColors().put(0, Color.BLUE);
        colorModel.getGroupColors().put(1, Color.CYAN);
        colorModel.getGroupColors().put(2, Color.GREEN);
        colorModel.getGroupColors().put(3, Color.DARK_GRAY);
        pw.setColorModel(colorModel);


        ListSelectionModel mySelection = new DefaultListSelectionModel();
        mySelection.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (!e.getValueIsAdjusting()){
                    System.out.println(pw.getSelectedIndices().length + " selected.");
                    for (int i : pw.getSequenceNodeOrder()) {
                        System.out.printf("%s ", i);
                    }
                    System.out.println();
                }


            }
        });
        pw.setListSelectionModel(mySelection);


        pw.setVisible(true);
        pw.getUI().zoomToFull();
        pw.update();


        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        int[] sel = new int[]{0, 2, 4};
        pw.setSelectedIndices(sel);

    }

    public static void main(String[] args) {
        Test t = new Test();
        t.run();
    }
}
