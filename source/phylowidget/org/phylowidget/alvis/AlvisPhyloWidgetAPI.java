/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.phylowidget.alvis;

import de.biozentrum.bioinformatik.sequence.Sequence;
import gui.sequencebundle.SequenceBundleColorModel;
import java.util.Map;
import org.phylowidget.PhyloTree;
import org.phylowidget.ui.PhyloUI;

import javax.swing.*;

/**
 *
 * @author Roland Schwarz <rfs32@cam.ac.uk>
 */
public interface AlvisPhyloWidgetAPI {
    public void setSequences(Iterable<Sequence> sequences);
    public void setSequenceGroups(Map<Sequence, Integer> groups);
    public void setColorModel(SequenceBundleColorModel colorModel);
    public void setTree(PhyloTree tree);
    public void update();
    public int[] getSelectedIndices();
    public void setSelectedIndices(int[] selectedIndices);
    public ListSelectionModel getListSelectionModel();
    public void setListSelectionModel(ListSelectionModel model);
    public int[] getSequenceNodeOrder();
    public PhyloUI getUI();
}
