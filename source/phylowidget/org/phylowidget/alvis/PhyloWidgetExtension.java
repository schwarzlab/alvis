/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.phylowidget.alvis;

import com.general.utils.GUIutils;
import de.biozentrum.bioinformatik.sequence.Sequence;
import gui.sequencebundle.SequenceBundleColorModel;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.phylowidget.PWContext;
import org.phylowidget.PhyloTree;
import org.phylowidget.PhyloWidget;
import org.phylowidget.tree.PhyloNode;
import org.phylowidget.ui.PhyloUI;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.phylowidget.ui.PhyloConfig;

/**
 *
 * @author Roland Schwarz <rfs32@cam.ac.uk>
 */
public class PhyloWidgetExtension extends PhyloWidget implements AlvisPhyloWidgetAPI {

    // Alvis stuff
    private Iterable<Sequence> sequences;
    private Map<Sequence, Integer> sequenceGroups;
    private SequenceBundleColorModel colorModel;
    private ListSelectionModel listSelectionModel;

    /* These are supplied Alvis */
    public void setTree(PhyloTree tree) {
		if (tree==null) {
			getPWContext().trees().setTree(PhyloConfig.DEFAULT_TREE);
		} else {
			getPWContext().trees().setTree(tree);
		}
    }

    public void setSequences(Iterable<Sequence> sequences) {
        this.sequences = sequences;
    }

    public void setSequenceGroups(Map<Sequence, Integer> groups) {
        this.sequenceGroups = groups;
    }

    public void setColorModel(SequenceBundleColorModel colorModel) {
        this.colorModel = colorModel;
    }

    public ListSelectionModel getListSelectionModel() {
        return listSelectionModel;
    }

    public void setListSelectionModel(ListSelectionModel listSelectionModel) {
        this.listSelectionModel = listSelectionModel;
        this.listSelectionModel.addListSelectionListener(new SelectionListener());
    }

    private PWContext getPWContext() {
        return this.pwc;
    }

    private String getHighlightColorRGB() {
        Color c = colorModel.getSelectionColor();
        return c.getRed() + "," + c.getGreen() + "," + c.getBlue();
    }

    public void update() {
        if (sequences == null) return;

        PhyloTree tree = (PhyloTree) getPWContext().trees().getTree();
        List<PhyloNode> leaves = tree.getAllLeaves();
        for (PhyloNode n : leaves) {

            int offset = 0;
            for (Sequence s : sequences) {
                if (s.getName().equals(n.getLabel())) {
                    int g = sequenceGroups.get(s);
                    Color c = colorModel.getGroupColors().get(g);
                    String rgb = c.getRed() + "," + c.getGreen() + "," + c.getBlue();
                    n.setAnnotation("alvisgrpcol", rgb); // alvis sequence colour
                    if (listSelectionModel.isSelectedIndex(offset)) {
                        n.setAnnotation("ncol", getHighlightColorRGB()); // selected node colour
                        n.setAnnotation("lcol", rgb);
                        n.setAnnotation("nsh", "triangle"); // selected node shape
                    } else {
                        n.setAnnotation("ncol", rgb);
                        n.setAnnotation("lcol", rgb);
                        n.setAnnotation("bcol", null);
                        n.setAnnotation("nsh", null);
                    }
                }
                offset++;
            }
        }
    }

    public int[] getSelectedIndices() {
        return GUIutils.getSelectedIndices(this.listSelectionModel);
    }

    public void setSelectedIndices(int[] indices) {
        int[] currentIndices = getSelectedIndices();

        // always update if size of selection has changed
        boolean change = !(indices.length == currentIndices.length);

        // if the size hasn't changed
        if (!change) {
            // check whether selected indices have change
            for (int i : indices) {
                if (!listSelectionModel.isSelectedIndex(i)) {
                    change = true;
                    break;
                }
            }
        }

        if (change) {
            listSelectionModel.setValueIsAdjusting(true);
            listSelectionModel.clearSelection();
            for (int selected : indices) {
                listSelectionModel.addSelectionInterval(selected, selected);
            }
            listSelectionModel.setValueIsAdjusting(false);
        }
    }

    public void setSelectedNode(PhyloNode node, boolean add) {
        if (this.sequences == null) return;

        // if no node selected
        if (node == null) {
            // if we're not adding nodes
            if (!add) {
                // clear selection
                setSelectedIndices(new int[0]);
            }
        } else {
            // get the sequence number of the selected nodes
            PhyloTree tree = node.getTree();
            List<PhyloNode> leaves = tree.getAllLeaves(node);
            List<String> labels = new ArrayList<>();
            for (PhyloNode n : leaves) labels.add(n.getLabel());

            // collect the sequence numbers
            Set<Integer> newSelection = new HashSet<>();

            if (add) {
                for (int i : GUIutils.getSelectedIndices(listSelectionModel)) {
                    newSelection.add(i);
                }
            }

            Iterator<Sequence> it = sequences.iterator();
            int offset = 0;
            while (it.hasNext()) {
                String sequenceName = it.next().getName();
                if (labels.contains(sequenceName)) {
                    newSelection.add(offset);
                }
                offset++;
            }

            int[] selected = new int[newSelection.size()];
            offset = 0;
            for (int s : newSelection) {
                selected[offset++] = s;
            }

            Arrays.sort(selected);
            setSelectedIndices(selected);
        }
    }

    public PhyloWidgetExtension() {
        super();
        setListSelectionModel(new DefaultListSelectionModel());
    }

    private class SelectionListener implements ListSelectionListener {
        @Override
        public void valueChanged(ListSelectionEvent e) {
            if (!e.getValueIsAdjusting()) update();
        }
    }

    @Override
	public PhyloUI getUI() {
		return this.getPWContext().ui();
	}

    @Override
    public int[] getSequenceNodeOrder() {
        if (sequences == null) return null;

        List<PhyloNode> leaves = getPWContext().trees().getTree().getAllLeaves();
        List<String> nodeLabels = new ArrayList<>();
        for (PhyloNode n : leaves) nodeLabels.add(n.getLabel());

        List<Integer> out = new ArrayList<>();
        for (Sequence s : sequences) out.add(nodeLabels.indexOf(s.getName()));

        return convertIntegers(out);
    }

    private static int[] convertIntegers(List<Integer> integers)
    {
        int[] out = new int[integers.size()];
        Iterator<Integer> it = integers.iterator();
        for (int i = 0; i < out.length; i++)
        {
            out[i] = it.next();
        }
        return out;
    }
}
