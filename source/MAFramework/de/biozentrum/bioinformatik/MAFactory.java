package de.biozentrum.bioinformatik;

import de.biozentrum.bioinformatik.alignment.MultipleAlignment;
import de.biozentrum.bioinformatik.alignment.MultipleAlignmentCached;
import de.biozentrum.bioinformatik.alignment.SimpleAlignmentCache;
import de.biozentrum.bioinformatik.sequence.Sequence;
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public class MAFactory {
    private MAFactory(){} // non-instantiable
    
    public static MultipleAlignment createMultipleAlignment() {
        return new MultipleAlignmentCached(new SimpleAlignmentCache());
//		return new MultipleAlignment();
    }
    
    public static MultipleAlignment createMultipleAlignment(ArrayList<Sequence> sequences) {
        return new MultipleAlignmentCached(new SimpleAlignmentCache(),sequences);
//		return new MultipleAlignment(sequences);
    }
    
    
}
