/*
 * Created on 16.12.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package de.biozentrum.bioinformatik.color;

import java.awt.Color;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;


/**
 * @author pseibel
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 * @param <E>
 */
public class ColorModel<E> implements Serializable, Map<E, Color> {
	protected Map<E,Color> colorMap;
	protected transient ArrayList<ColorModelListener> listeners;
	private static final long serialVersionUID=1L;
	protected Color defaultColor;
	
	public ColorModel() {
		colorMap = new HashMap<>();
		listeners = new ArrayList<>();
	}
	
	public void addColorModelListener( ColorModelListener listener ) {
		listeners.add( listener );
	}
	
	public void removeColorModelListener( ColorModelListener listener ) {
		listeners.remove( listener );
	}
	
	protected void fireColorModelChanged( ColorChangedEvent e ) {
		Iterator<ColorModelListener> iterator = listeners.iterator();
		while ( iterator.hasNext() ) {
			iterator.next().colorModelChanged( e );
		}
	}

	protected void fireColorModelChanged(E object, Color oldColor, Color newColor, int type){
		ColorChangedEvent<E> event = new ColorChangedEvent<>(this, object, oldColor, newColor, type);
		fireColorModelChanged(event);
	}
	

	private void readObject(ObjectInputStream aInputStream) throws ClassNotFoundException, IOException {
		     //always perform the default de-serialization first
		     aInputStream.defaultReadObject();
		     listeners = new ArrayList<ColorModelListener>();
	}

	// now implementing the map interface
	@Override
	public int size() {
		return colorMap.size();
	}

	@Override
	public boolean isEmpty() {
		return colorMap.isEmpty();
	}

	@Override
	public boolean containsKey(Object key) {
		return colorMap.containsKey(key);
	}

	@Override
	public boolean containsValue(Object value) {
		return colorMap.containsValue(value);
	}

	@Override
	public Color get(Object key) {
		Color col = colorMap.get(key);
		if (col==null) {
			col=defaultColor;
		}
		return col;
	}

	@Override
	public Color put(E key, Color value) {
		Color oldval = colorMap.put(key, value);
		int type;
		if (oldval==null) {
			type = ColorChangedEvent.INSERT;
			fireColorModelChanged(key, oldval, value, type);
		} else {
			if (!oldval.equals(value)) {
				type = ColorChangedEvent.UPDATE;
				fireColorModelChanged(key, oldval, value, type);
			}
		}
		return oldval;
	}

	@Override
	public Color remove(Object key) {
		Color val = colorMap.remove(key);
		if (val!=null) {
			fireColorModelChanged((E)key, val, null, ColorChangedEvent.DELETE);
		}
		return val;
	}

	@Override
	public void putAll(Map<? extends E, ? extends Color> m) {
//		colorMap.putAll(m);
		for (E key:m.keySet()) {
			put(key, m.get(key));
		}
//		ColorChangedEvent<E> event = new ColorChangedEvent<>(this);
//		fireColorModelChanged(event);
	}

	@Override
	public void clear() {
		colorMap.clear();
		ColorChangedEvent<E> event = new ColorChangedEvent<>(this);
		fireColorModelChanged(event);
	}

	@Override
	public Set<E> keySet() {
		return colorMap.keySet();
	}

	@Override
	public Collection<Color> values() {
		return colorMap.values();
	}

	@Override
	public Set<Entry<E, Color>> entrySet() {
		return colorMap.entrySet();
	}

	public Color getDefaultColor() {
		return defaultColor;
	}

	public void setDefaultColor(Color color) {
		defaultColor = color;
		fireColorModelChanged(new ColorChangedEvent(this));
	}
}
