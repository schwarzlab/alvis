/*
 * Created on 21.11.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package de.biozentrum.bioinformatik.color;

import java.awt.Color;
import java.util.EventObject;


/**
 * @author pseibel
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ColorChangedEvent<E> extends EventObject {

	/**
	 * Comment for <code>serialVersionUID</code>
	 */
	private static final long serialVersionUID = 3256443594867815732L;

	public static final int INSERT = 0;
	public static final int UPDATE = 1;
	public static final int DELETE = 2;

	/**
	 * 
	 * @uml.property name="type" 
	 */
	protected int type;

	/**
	 * 
	 * @uml.property name="oldColor" 
	 */
	protected Color oldColor;

	protected Color newColor;

	/**
	 * 
	 * @uml.property name="object" 
	 */
	protected E object;

	
	/**
	 * @param source
	 * @param arg0
	 */
	public ColorChangedEvent( ColorModel<E> source ) {
		super( source );
		// TODO Auto-generated constructor stub
	}
	
	public ColorChangedEvent( ColorModel<E> source, E object, Color oldColor, Color newColor, int type ) {
		super( source );
		this.object = object;
		this.oldColor = oldColor;
		this.newColor = newColor;
		this.type = type;
	}
	
	public Color getNewColor() {
		return newColor;
	}

	/**
	 * 
	 * @uml.property name="oldColor"
	 */
	public Color getOldColor() {
		return oldColor;
	}

	/**
	 * 
	 * @uml.property name="object"
	 */
	public E getObject() {
		return object;
	}

	/**
	 * 
	 * @uml.property name="type"
	 */
	public int getType() {
		return type;
	}

}
