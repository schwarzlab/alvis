/*
 * Created on 21.11.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package de.biozentrum.bioinformatik.color;

import java.util.EventListener;




/**
 * @author pseibel
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface ColorModelListener extends EventListener {
	public void colorModelChanged( ColorChangedEvent e );
}
