package de.biozentrum.bioinformatik.alignment;

import java.awt.Rectangle;

import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoableEdit;

public class AlignmentEdit implements UndoableEdit {

	public static final int INSERT = 0;
	public static final int DELETE = 1;
	
	private int type;
	private Rectangle rect;
	private MultipleAlignment alignment;
	
	public AlignmentEdit( Rectangle rect, int type, MultipleAlignment alignment) {
		this.alignment = alignment;
		this.rect = rect;
		this.type = type;
	}

	public void undo() throws CannotUndoException {
		if ( type == DELETE ) {
			alignment.insertChar( '-', rect );
		}
		else if ( type == INSERT ) {
			alignment.delete( rect );
		}
	}

	public boolean canUndo() {
		return true;
	}

	public void redo() throws CannotRedoException {
		if ( type == INSERT ) {
			alignment.insertChar( '-', rect );
		}
		else if ( type == DELETE ) {
			alignment.delete( rect );
		}
	}

	public boolean canRedo() {
		return true;
	}

	public void die() {
		// TODO Auto-generated method stub

	}

	public boolean addEdit(UndoableEdit arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean replaceEdit(UndoableEdit arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean isSignificant() {
		return true;
	}

	public String getPresentationName() {
		return "edit operation";
	}

	public String getUndoPresentationName() {
		return "edit operation";
	}

	public String getRedoPresentationName() {
		return "edit operation";
	}

}
