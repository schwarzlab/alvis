package de.biozentrum.bioinformatik.alignment.policy;

import java.awt.Rectangle;

import de.biozentrum.bioinformatik.alignment.MultipleAlignment;

public interface AlignmentEditPolicy {
	public void setAlignment( MultipleAlignment alignment );
	public boolean canInsert( char c, Rectangle rect );
	public boolean canDelete( Rectangle rect );
}
