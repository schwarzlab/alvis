package de.biozentrum.bioinformatik.alignment.cursor;

import java.awt.Rectangle;

public class LeftSideCursorMode implements CursorMode {

	public String getName() {
		return "left side";
	}

	public boolean insertChar(char c, Cursor cursor) {
		Rectangle rect = translateCursorRect( cursor.getRect() );
		
		if ( cursor.getEditPolicy().canInsert( c ,rect ) ) {
			cursor.getAlignment().insertChar( c, rect );
			return true;
		}
		else {
			return false;
		}
	}

	public boolean delete(Cursor cursor) {
		Rectangle rect = translateCursorRect( cursor.getRect() );
		
		if ( cursor.getEditPolicy().canDelete( rect ) ) {
			cursor.getAlignment().delete( rect );
			return true;
		}
		else {
			return false;
		}
	}

	public boolean performBackSpace(Cursor cursor) {
		Rectangle rect = translateCursorRect( cursor.getRect() );
		if ( cursor.getEditPolicy().canDelete( rect ) ) {
			cursor.getAlignment().delete( rect );
			return true;
		}
		else {
			return false;
		}
	}
	
	private Rectangle translateCursorRect( Rectangle rect ) {
		return new Rectangle( 0, (int)rect.getMinY(), (int)rect.getWidth(), (int)rect.getHeight() );
	}

}
