package de.biozentrum.bioinformatik.alignment.cursor;

import java.awt.Rectangle;

import de.biozentrum.bioinformatik.alignment.MultipleAlignment;
import de.biozentrum.bioinformatik.alignment.policy.AlignmentEditPolicy;

public class Cursor {
	protected Rectangle rect;
	protected int cursorMode;
	protected CursorMode currentCursorMode;
	protected AlignmentEditPolicy editPolicy;
	protected MultipleAlignment alignment;
	protected boolean isActive;
	
	protected static CursorMode standardCursorMode;
	protected static CursorMode exclusiveCursorMode;
	protected static CursorMode leftSideCursorMode;
	
	static {
		standardCursorMode = new StandardCursorMode();
		exclusiveCursorMode = new ExclusiveCursorMode();
		leftSideCursorMode = new LeftSideCursorMode();
	}
	
	public Cursor( AlignmentEditPolicy editPolicy ) {
		this.editPolicy = editPolicy;
		
		currentCursorMode = standardCursorMode;

		isActive = false;
	}
	
	public void setAlignment( MultipleAlignment alignment ) {
		this.alignment = alignment;
		if ( editPolicy != null )
			editPolicy.setAlignment( alignment );
	}
	
	public MultipleAlignment getAlignment() {
		return alignment;
	}
	
	public void setEditPolicy( AlignmentEditPolicy editPolicy ) {
		this.editPolicy = editPolicy;
		if ( alignment != null )
			editPolicy.setAlignment( alignment );
	}
	
	public AlignmentEditPolicy getEditPolicy() {
		return editPolicy;
	}
	
	public boolean isActive() {
		return isActive;
	}
	
	public void setActive( boolean flag ) {
		isActive = flag;
	}
	
	public boolean insertChar( char c ) {
		return currentCursorMode.insertChar( c, this );
	}
	
	public boolean delete() {
		return currentCursorMode.delete( this );
	}
	
	public boolean performBackSpace() {
		return currentCursorMode.performBackSpace( this );
	}
	
	public void setRect( Rectangle rect ) {
		this.rect = rect;
	}
	
	public Rectangle getRect() {
		return rect;
	}
	
	public void setCursorMode( int cursorMode ) {
		this.cursorMode = cursorMode;
		switch ( cursorMode ) {
		case CursorMode.STANDARD:
			currentCursorMode = standardCursorMode;
			break;
		case CursorMode.EXCLUSIVE:
			currentCursorMode = exclusiveCursorMode;
			break;
		case CursorMode.LEFTSIDE:
			currentCursorMode = leftSideCursorMode;
		}
	}
	
	public int getCursorMode() {
		return cursorMode;
	}
}


