package de.biozentrum.bioinformatik.alignment.cursor;

import java.awt.Rectangle;

import de.biozentrum.bioinformatik.alignment.MultipleAlignment;

public class ExclusiveCursorMode implements CursorMode {

	public String getName() {
		return "exclusive cursor";
	}

	public boolean insertChar(char c, Cursor cursor) {
		Rectangle[] rects = getExclusiveRects( cursor.getRect(), cursor.getAlignment() );
		if ( cursor.getEditPolicy().canInsert( c ,rects[0] )
		  && cursor.getEditPolicy().canInsert( c ,rects[1] )) {
			
			Rectangle r = cursor.getRect();
			r.setLocation( (int)(r.getMinX() + r.getWidth()), (int)r.getMinY());
			cursor.setRect( r );
			
			cursor.getAlignment().insertChar( c, rects[0] );
			cursor.getAlignment().insertChar( c, rects[1] );	
				
			return true;
		}
		else {
			return false;
		}
	
	}

	public boolean delete(Cursor cursor) {
		Rectangle[] rects = getExclusiveRects( cursor.getRect(), cursor.getAlignment() );
		if ( cursor.getEditPolicy().canDelete( rects[0] )
		  && cursor.getEditPolicy().canDelete( rects[1] )) {
			cursor.getAlignment().delete( rects[0] );
			cursor.getAlignment().delete( rects[1] );	
			return true;
		}
		else {
			return false;
		}
	}

	public boolean performBackSpace(Cursor cursor) {
		Rectangle r = cursor.getRect();
		Rectangle rect = new Rectangle( (int)(r.getMinX() - r.getWidth()), (int)r.getMinY(), (int)r.getWidth(), (int)r.getHeight() );	
		Rectangle[] rects = getExclusiveRects( rect, cursor.getAlignment() );
		if ( cursor.getEditPolicy().canDelete( rects[0] )
		  && cursor.getEditPolicy().canDelete( rects[1] )) {
			
			r.setLocation( (int)(r.getMinX() - r.getWidth()), (int)r.getMinY());
			cursor.setRect( r );
			
			cursor.getAlignment().delete( rects[0] );
			cursor.getAlignment().delete( rects[1] );	
	
			return true;
		}
		else {
			return false;
		}
	}
	
	private Rectangle[] getExclusiveRects( Rectangle r, MultipleAlignment alignment ) {
		
		Rectangle[] rects = new Rectangle[2];
		rects[0] = new Rectangle( (int)r.getMinX(), 0, (int)r.getWidth(), (int)r.getMinY());
		rects[1] = new Rectangle( (int)r.getMinX(), (int)r.getMaxY(), (int)r.getWidth(), (int)(alignment.getSequenceCount() - r.getMaxY()));
		
		return rects;
	}

}
