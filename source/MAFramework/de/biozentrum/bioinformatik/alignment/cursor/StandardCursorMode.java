package de.biozentrum.bioinformatik.alignment.cursor;

import java.awt.Rectangle;

public class StandardCursorMode implements CursorMode {

	public String getName() {
		return "default cursor";
	}

	public boolean insertChar(char c, Cursor cursor) {
		if ( cursor.getEditPolicy().canInsert( c, cursor.getRect() ) ) {
			cursor.getAlignment().insertChar( c, cursor.getRect() );
			return true;
		}
		else {
			return false;
		}
	}

	public boolean delete(Cursor cursor) {
		if ( cursor.getEditPolicy().canDelete( cursor.getRect() ) ) {
			cursor.getAlignment().delete( cursor.getRect() );
			return true;
		}
		else {
			return false;
		}
	}

	public boolean performBackSpace( Cursor cursor ) {
		Rectangle r = cursor.getRect();
		Rectangle rect = new Rectangle( (int)(r.getMinX() - r.getWidth()), (int)r.getMinY(), (int)r.getWidth(), (int)r.getHeight() );
		if ( rect.getMinX() >= 0 && cursor.getEditPolicy().canDelete( rect ) ) {
			cursor.getAlignment().delete( rect );
			return true;
		}
		else {
			return false;
		}
	}

}
