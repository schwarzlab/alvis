package de.biozentrum.bioinformatik.alignment.cursor;


public interface CursorMode {
	
	public static final int STANDARD = 0;
	public static final int LEFTSIDE = 1;
	public static final int EXCLUSIVE = 2;
	
	public String getName();
	public boolean insertChar( char c, Cursor cursor );
	public boolean delete( Cursor cursor );
	public boolean performBackSpace( Cursor cursor );
}
