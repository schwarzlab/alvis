package de.biozentrum.bioinformatik.alignment;

import java.awt.Font;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import javax.swing.JComponent;
import javax.swing.JTextField;

public class FontSizeChooser implements MouseWheelListener {
	
	private JComponent editor;
	JTextField sizeField;
	int minSize;
	int maxSize;
	int stepSize;
	
	public FontSizeChooser( JComponent editor ) {
		this(editor, 2, 24, 1);
	}
	
	public FontSizeChooser(JComponent editor, int minSize, int maxSize, int stepSize) {
		this.minSize = minSize;
		this.maxSize = maxSize;
		this.stepSize = stepSize;
		
		sizeField = new JTextField();
		sizeField.setEditable(false);
		this.editor = editor;
		
		editor.addMouseWheelListener( FontSizeChooser.this );
		
		sizeField.setText( String.valueOf( editor.getFont().getSize() ));
	}
	
	
	private void increaseFont( int steps ) {
		Font f = editor.getFont();
		int size = f.getSize();
		size = Math.min( this.maxSize, size + steps );
		editor.setFont( new Font( f.getFontName(), f.getStyle(), size ));
		sizeField.setText( String.valueOf( size ));
	} 
	
	private void decreaseFont( int steps ) {
		Font f = editor.getFont();
		int size = f.getSize();
		size = Math.max( this.minSize, size - steps );
		editor.setFont( new Font( f.getFontName(), f.getStyle(), size ));
		sizeField.setText( String.valueOf( size ));
	}
	
	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		if (e.isControlDown()) {
			int scrolled = e.getWheelRotation();
			
			if ( scrolled > 0 )
				increaseFont( scrolled * stepSize);
			else 
				decreaseFont( Math.abs( scrolled * stepSize) );
		} else {
			e.getComponent().getParent().dispatchEvent(e);
		}
	}
	
	
}