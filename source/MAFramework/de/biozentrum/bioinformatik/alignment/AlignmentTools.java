/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package de.biozentrum.bioinformatik.alignment;

import com.general.utils.GeneralUtils;
import de.biozentrum.bioinformatik.sequence.Sequence;
import java.util.Map;

/**
 *
 * @author Roland Schwarz <rfs32@cam.ac.uk>
 */
public class AlignmentTools {
	public static Sequence getConsensusSequence(MultipleAlignment alignment) {
		return getConsensusSequence(alignment, GeneralUtils.range(0, alignment.getSequenceCount()));
	}
	
	
	public static Sequence getConsensusSequence(MultipleAlignment alignment, int[] selection) {
		Sequence consensus;
		StringBuilder conString = new StringBuilder();
		
		if (selection.length>0) {
			for (int col=0;col<alignment.length();col++) {
				Character position=null;
				Map<Character, Integer> counts = alignment.getSymbolCounts(col, selection);
				Integer max=0;
				for (Map.Entry<Character,Integer> e:counts.entrySet()){
					if (e.getValue()>max){
						max=e.getValue();
						position = e.getKey();
					}
				}
				conString.append(position);
			}
		}
		consensus = new Sequence("Consensus", conString.toString());
		return consensus;
	}
}
