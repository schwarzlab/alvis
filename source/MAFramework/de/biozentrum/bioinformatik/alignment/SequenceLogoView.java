package de.biozentrum.bioinformatik.alignment;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.font.TextLayout;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JPanel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import de.biozentrum.bioinformatik.alignment.events.AlignmentSelectionEvent;
import de.biozentrum.bioinformatik.alignment.events.AlignmentSelectionListener;
import de.biozentrum.bioinformatik.sequence.SequenceAlphabet;
import de.biozentrum.bioinformatik.sequence.SequenceColorModel;

public class SequenceLogoView extends JPanel implements MouseListener, AlignmentSelectionListener, ListSelectionListener {
		private boolean needsRedraw;
		private BufferedImage buffer;
		private double currentX;
		private double currentY;
		private boolean showLogo = true;
		private MultipleAlignmentView alignmentView;
		private MultipleAlignment alignment;
		private SequenceColorModel colorModel;
		
		private boolean inSelectionProcess;
		
		public SequenceLogoView( MultipleAlignmentView alignmentView ) {
			this.colorModel = alignmentView.getSequenceColorModel();
			alignmentView.addPropertyChangeListener( new PropertyChangeListener() {

				public void propertyChange(PropertyChangeEvent e) {
					needsRedraw = true;
					repaint();
				}
				
			});
			this.alignmentView = alignmentView;
			alignment = alignmentView.getAlignment();
			addMouseListener( this );
			setBackground( Color.WHITE );
		}
		
		
		public void paintComponent(Graphics g){
			alignment = alignmentView.getAlignment();
			setPreferredSize( new Dimension( (int)alignmentView.getPreferredSize().getWidth(), 30 ) );
			
			Rectangle2D visibleRect = getVisibleRect();
			if ( this.buffer == null || visibleRect.getHeight() != buffer.getHeight() || visibleRect.getWidth() != buffer.getWidth() ) {
				this.buffer = new BufferedImage( (int)visibleRect.getWidth(), (int)visibleRect.getHeight(), BufferedImage.TYPE_INT_ARGB );
				this.needsRedraw = true;
			}
			
			if ( visibleRect.getX() != currentX || visibleRect.getY() != currentY ) {
				this.currentX = visibleRect.getX();
				this.currentY = visibleRect.getY();
				this.needsRedraw = true;
				
			}
			
			if ( alignment != null && this.needsRedraw || true ) {
				
				Graphics2D g2 = (Graphics2D)buffer.getGraphics();
				draw( g2 );
				this.needsRedraw = false;
			}
			g.drawImage( buffer, (int)visibleRect.getMinX(), (int)visibleRect.getMinY(), null);
		}
		
		public void draw( Graphics2D g2 ) {
			setFont( alignmentView.getFont() );
			int columnWidth = RenderUtils.pixelSizeForFont( alignmentView.getFont() ) + 2;
			
			Rectangle2D visibleRect = alignmentView.getVisibleRect();
			
			g2.setColor( Color.WHITE );
			g2.fillRect(0,0,(int)visibleRect.getWidth(),(int)visibleRect.getHeight());
			//g2.setRenderingHint( RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON );
			
			int colFrom = alignmentView.xToColumn( visibleRect.getX() );
			int colTo = Math.min( alignmentView.xToColumn( visibleRect.getMaxX() ), alignment.getLength() - 1);
			
			int seqFrom = alignmentView.yToSequence( visibleRect.getY() );
			
			g2.translate( alignmentView.columnToX(colFrom) - visibleRect.getX(), alignmentView.sequenceToY(seqFrom) - visibleRect.getY() );
			if ( colFrom == 0 ) {
				g2.translate( columnWidth / 2 , 0 );
			}
			
			for ( int i = colFrom; i <= colTo; i++ ) {
				if ( !showLogo ) {
					double conservation = getConservationAt( i );
					double height = (26) * conservation;
					Rectangle2D cRect = new Rectangle2D.Double( 2, 30 - height - 2 , columnWidth, height );
					
					Color fillColor = null;
					
					if ( conservation > 0.50 ) {
						fillColor = new Color( (int) (255 * (1 - ((conservation - 0.5) * 2))) , 255,0);			
					}
					else {
						fillColor = new Color( 255, (int) (255 * conservation * 2),0);	
					}

					g2.setColor( fillColor );
					g2.fill( cRect );
				}
				else {
					
					drawLogo( g2, i );
				}
				g2.translate( columnWidth+2, 0 );
			}
		}
		
		public void drawLogo( Graphics2D g2, int column ) {
			AffineTransform old = g2.getTransform();
			g2.setFont( getFont() );
			int columnWidth = RenderUtils.pixelSizeForFont( alignmentView.getFont() ) + 2;
			
			SequenceAlphabet alphabet = alignment.getAlphabet();
			ArrayList<Character> characters = new ArrayList<Character>();
			HashMap<Character,Double> charToValue = new HashMap<Character,Double>();
			double h = 0;
			for ( int i = 0; i < alphabet.size(); i++ ) {
				Character c = alphabet.characterAt( i );
				
				double value = (double)getCharCount( column, c ) / (double)alignment.getSequenceCount();			
				if ( c.charValue() == '-' || value == 0 )
					continue;
				
				h += value * log2( value );
				int j = 0;
				for ( j= 0; j < characters.size(); j++ ) {
					if ( charToValue.get( characters.get(j) ) > value )
						break;
				}
				characters.add( j, c );
				charToValue.put( c, new Double(value) );
			}
			h = -h;
			double r = log2( alphabet.size()-1 ) - h;
			g2.translate( 0, 30 );
			for ( int i = 0; i < characters.size(); i++ ) {
				Character c = characters.get( i );
				String s = String.valueOf( c.charValue() );
				double height = 30 / log2(alphabet.size()-1) * r * ((Double)charToValue.get( c )).doubleValue();
				TextLayout layout = new TextLayout( s, g2.getFont(), g2.getFontRenderContext() );
				g2.setColor( colorModel.getColorForChar( c.charValue() ));
				
				int xOffset = (int) (( columnWidth - layout.getBounds().getWidth() ) / 2);
				g2.translate( xOffset, 0 );
				
				g2.scale( 1, height / layout.getBounds().getHeight() );
				g2.drawString( s, (int) ((17 - layout.getBounds().getWidth()) / 2), 0 );
				g2.scale( 1, layout.getBounds().getHeight() / height );
				g2.translate( -xOffset, -height );
			}
			g2.setTransform( old );
		}
		
		public double log2( double d ) {
			return Math.log( d ) / Math.log( 2 );
		}
		
		public int getCharCount( int column, Character character ) {
			int count = 0;
			for ( int i = 0; i < alignment.getSequenceCount(); i++ ) {
				if ( alignment.characterAt( i, column ).equals( character ) )
					count++;
			}
			return count;
		}
		
		public double getConservationAt( int column ) {
			int max = 0;
			HashMap<Character,Integer> map = new HashMap<Character,Integer>();
			for ( int i = 0; i < alignment.getSequenceCount(); i++ ) {
				Character character = alignment.characterAt( i, column );
				if ( character.charValue() == '-')
					continue;
				Integer integer = map.get( character );
				if ( integer != null )
					integer = new Integer( integer.intValue() + 1 );
				else {
					integer = new Integer( 1 );				
				}
				if ( max < integer.intValue() )
					max = integer.intValue();
				map.put( character, integer );
			}
			return (double)max / (double)alignment.getSequenceCount();
		}
		
		public void selectionChanged(AlignmentSelectionEvent e) {
			if ( !inSelectionProcess) {
				this.needsRedraw = true;
				repaint();	
			}
		}

		/* (non-Javadoc)
		 * @see javax.swing.event.ListSelectionListener#valueChanged(javax.swing.event.ListSelectionEvent)
		 */
		public void valueChanged(ListSelectionEvent arg0) {
			this.needsRedraw = true;
			repaint();
		}
		
		/* (non-Javadoc)
		 * @see java.awt.event.MouseListener#mouseClicked(java.awt.event.MouseEvent)
		 */
		public void mouseClicked(MouseEvent e) {
			showLogo = !showLogo;
			this.needsRedraw = true;
			repaint();
		}

		/* (non-Javadoc)
		 * @see java.awt.event.MouseListener#mousePressed(java.awt.event.MouseEvent)
		 */
		public void mousePressed(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		/* (non-Javadoc)
		 * @see java.awt.event.MouseListener#mouseReleased(java.awt.event.MouseEvent)
		 */
		public void mouseReleased(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		/* (non-Javadoc)
		 * @see java.awt.event.MouseListener#mouseEntered(java.awt.event.MouseEvent)
		 */
		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		/* (non-Javadoc)
		 * @see java.awt.event.MouseListener#mouseExited(java.awt.event.MouseEvent)
		 */
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}


		public void selectionDidEnd() {
			inSelectionProcess = false;
			this.needsRedraw = true;
			repaint();
		}


		public void selectionWillBegin() {
			inSelectionProcess = true;
			
		}
		
		
		
	}
