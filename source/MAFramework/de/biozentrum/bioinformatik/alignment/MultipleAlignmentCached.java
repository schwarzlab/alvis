/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package de.biozentrum.bioinformatik.alignment;

import de.biozentrum.bioinformatik.sequence.Sequence;
import java.util.ArrayList;
import java.util.Map;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public class MultipleAlignmentCached extends MultipleAlignment {
    private final IAlignmentCachingStrategy cachingStrategy;
    public MultipleAlignmentCached(IAlignmentCachingStrategy cachingStrategy) {
        super();
        this.cachingStrategy = cachingStrategy;
        init();
    }
    public MultipleAlignmentCached(IAlignmentCachingStrategy cachingStrategy, ArrayList<Sequence> sequences ) {
        super(sequences);
        this.cachingStrategy = cachingStrategy;
        init();
    }
    
    private void init() {
        cachingStrategy.register(this);
    }
    
    @Override
    public char[] getColumn(int position) {
		if (position>=0 && position<this.length()) {
			return cachingStrategy.columnAt(position);
		} else {
			return new char[this.rows()];
		}
    }

	@Override
	public Map<Character, Integer> getSymbolCounts(int position) {
		return super.getSymbolCounts(position);
	}

	@Override
	public Map<Character, Integer> getSymbolCounts(int position, int[] rows) {
		if (rows != null) {
			return super.getSymbolCounts(position, rows); //To change body of generated methods, choose Tools | Templates.
		} else {
			return cachingStrategy.getSymbolCounts(position);
		}
	}
	
	
    
}
