/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package de.biozentrum.bioinformatik.alignment;

import java.util.Map;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public interface IAlignmentCachingStrategy {
    public void register(MultipleAlignment  alignment);
    public char[] columnAt( int position );    
    public Map<Character, Integer> getSymbolCounts( int position);
	
}
