package de.biozentrum.bioinformatik.alignment;

import java.awt.Font;

import de.biozentrum.bioinformatik.alignment.selection.AlignmentSelectionModel;

public interface AlignmentEditor {
	public static enum EditorMode {
		SELECT,
		EDIT,
		NONE
	};
	
	public AlignmentSelectionModel getSelectionModel();
	
	public void setEditorMode( EditorMode editMode );
	public EditorMode getEditorMode();
	
	public void setEditable( boolean editable );
	public boolean isEditable();
	
	public void setCursorMode( int cursorMode );
	public int getCursorMode();
	
	public void setFont( Font font );
	public Font getFont();	
}
