/*
 * Created on 17.05.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package de.biozentrum.bioinformatik.alignment;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.Shape;
import java.awt.font.FontRenderContext;
import java.awt.font.TextLayout;
import java.awt.geom.Area;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RoundRectangle2D;
import java.awt.image.BufferedImage;
import java.util.HashMap;

/**
 * @author binf024
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class MACharacterRenderer {
	
	public static final int START = 0;
	public static final int MIDDLE = 1;
	public static final int END = 2;
	
	private Shape[] shapes;
	private HashMap<Color,BufferedImage> cache;
	private Dimension size=new Dimension(0,0);
	private Font font;
	
	double offsetX;
	double offsetY;
	private int chosenFontSize;
	
	public MACharacterRenderer() {
		setFont( new Font("Curier",Font.BOLD, 12) );
	}
	
	void setSize( Dimension s ) {
		size = s;
	
		// if enough space, set back to chosen font size (this is in case we shrunk the font earlier)
		Font testfont = new Font(font.getName(), font.getStyle(), chosenFontSize);
		// shrink font if necessary
		while (RenderUtils.pixelSizeForFont(testfont) + 2 > Math.min(size.height, size.width)) {
			testfont = new Font(font.getName(), font.getStyle(), testfont.getSize()-1);
		}
		font = testfont;
		
		cache = new HashMap<>();
		
		shapes = new Shape[ 3 ];
		
		Area start = new Area( new RoundRectangle2D.Double( 0, 0, 2*s.width, s.height, s.width, s.height  ) );
		start.subtract( new Area( new Rectangle2D.Double( 1.5*s.width, 0, s.width, s.height ) ) );
		
		shapes[ START ] = start;
		shapes[ MIDDLE ] = new Rectangle2D.Double(0,0,s.width,s.height);
		
		Area end = new Area( new RoundRectangle2D.Double( -s.width/2, 0, 2*s.width, s.height, s.width, s.height  ) );
		end.subtract( new Area( new Rectangle2D.Double( -s.width/2, 0, s.width/2, s.height ) ) );
		
		shapes[ END ] = end;
		
		recalculateOffsets();
	}
	
	private void recalculateOffsets() {
		BufferedImage img = new BufferedImage( 10,10, BufferedImage.TYPE_INT_ARGB );		
		FontRenderContext frc = ((Graphics2D)img.getGraphics()).getFontRenderContext();		
		TextLayout layout = new TextLayout( new Character('U').toString(), font, frc);
		Rectangle2D bounds = layout.getBounds();
		offsetX = (size.width - bounds.getWidth()) / 2;
		offsetY = (size.height - bounds.getHeight()) / 2;
	}
	
	public void setFont( Font font ) {
		this.font = font;
		this.chosenFontSize = font.getSize();
		int x = RenderUtils.pixelSizeForFont( font ) + 2;
		setSize( new Dimension(Math.max(size.width, x), Math.max(size.height, x)));
	}
	
	public void renderSymbol( Graphics2D g, 
			Character character, 
			Color color, 
			int state,
			boolean selected,
			boolean highlighted, 
			boolean doCaching ) {
		
		
		if ( selected ) {
			
			g.setColor( Color.BLACK );
			g.fill( shapes[ state ] );
			
		} else {	
			BufferedImage buffer = (BufferedImage) cache.get( color );
			if ( buffer == null ) {
				buffer = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration().createCompatibleImage(size.width, size.height);
				Graphics2D g2 = (Graphics2D)buffer.getGraphics();
				g2.setColor(Color.WHITE);
				g2.fillRect(0, 0, size.width, size.height);
//					g2.setPaint( paint );
				Color paint = new Color(color.getRed(), color.getGreen(), color.getBlue(), 125);
				g2.setPaint(paint);
				g2.fill( shapes[state] );
				cache.put( color, buffer );
			}
			g.drawImage(buffer, 0, 0, null);
		}

//		if ( selected) {
//			g.setStroke( new BasicStroke(2));
//			g.setColor( Color.BLACK);
//			
//		}
//		else {
//			g.setStroke( new BasicStroke(1));
//			g.setColor( color );
//		}
//		g.draw( shapes[state] );
		
		
		
		if ( font.getSize() >= 6 ) {
			if ( selected )
				g.setColor( Color.WHITE );
			else 
				g.setColor( Color.BLACK );
			
			g.setFont( font );
			
//			if ( state == START )
//				g.drawString( character.toString(),(int)(offsetX+size/2), (int)(size-offsetY) );
//			else {
				
			g.drawString( character.toString(),(int)offsetX, (int)(size.height-offsetY) );
//			}
		}
	}
}
