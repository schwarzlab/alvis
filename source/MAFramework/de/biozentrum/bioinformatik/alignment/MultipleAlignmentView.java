/*
 * Created on 18.05.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package de.biozentrum.bioinformatik.alignment;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.font.TextLayout;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.DefaultListSelectionModel;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;
import javax.swing.ToolTipManager;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import de.biozentrum.bioinformatik.alignment.cursor.Cursor;
import de.biozentrum.bioinformatik.alignment.events.AlignmentChangedEvent;
import de.biozentrum.bioinformatik.alignment.events.AlignmentListener;
import de.biozentrum.bioinformatik.alignment.events.AlignmentSelectionEvent;
import de.biozentrum.bioinformatik.alignment.events.AlignmentSelectionListener;
import de.biozentrum.bioinformatik.alignment.events.AlignmentStructureChangedEvent;
import de.biozentrum.bioinformatik.alignment.events.MAEvent.MAEventType;
import de.biozentrum.bioinformatik.alignment.policy.AlignmentEditPolicy;
import de.biozentrum.bioinformatik.alignment.selection.AlignmentSelectionModel;
import de.biozentrum.bioinformatik.alignment.selection.DefaultAlignmentSelectionModel;
import de.biozentrum.bioinformatik.alignment.visibility.AlignmentVisibilityListener;
import de.biozentrum.bioinformatik.alignment.visibility.AlignmentVisibilityModel;
import de.biozentrum.bioinformatik.alignment.visibility.AlignmentVisibilityChangedEvent;
import de.biozentrum.bioinformatik.alignment.visibility.DefaultAlignmentVisibilityModel;
import de.biozentrum.bioinformatik.color.ColorChangedEvent;
import de.biozentrum.bioinformatik.color.ColorModelListener;
import de.biozentrum.bioinformatik.sequence.SequenceAlphabet;
import de.biozentrum.bioinformatik.sequence.SequenceColorModel;
import java.awt.FontMetrics;
import java.awt.font.FontRenderContext;
import java.awt.font.GlyphVector;
import java.beans.PropertyChangeSupport;

/**
 * @author binf024
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class MultipleAlignmentView extends JPanel implements AlignmentEditor, KeyListener, MouseListener, MouseMotionListener, AlignmentSelectionListener, AlignmentListener, AlignmentVisibilityListener, ColorModelListener {
	public static final String PROP_CHARBOXSIZE = "PROP_CHARBOXSIZE";

	private int logoHeight = 50;
    private boolean needsRedraw = true;
	private boolean bufferedRedrawX;
	private boolean bufferedRedrawY;
	
	private EditorMode editMode;
	private boolean editable = true;
	
	Dimension charBoxSize;
	
	private Rectangle2D currentViewRect;
	
	protected MultipleAlignment alignment;
	
	private BufferedImage buffer;
	private BufferedImage tmpBuffer;
	
	
	private MACharacterRenderer renderer;
	
	private SequenceColorModel colorModel;
	
	AlignmentVisibilityModel visibilityModel;
	
	private ListSelectionModel columnSelectionModel;
	private ListSelectionModel sequenceSelectionModel;
	private AlignmentSelectionModel maSelectionModel;
	
	// Cursor
	private Point startDrag;
	private Point endDrag;
	
	protected Cursor cursor;
	private Rectangle2D draggedCursorRect;
	
	// Cursor Event Handling
	
	private Rectangle2D drawSelectionRect;
	
	//private MAHeaderView headerView;
	private ConservationView conservationView;
	private LogoView logoView;
	
	private double currentX;
	private double currentY;
	
	
	private boolean ignoreGaps;
	private Rectangle alignmentScreenRect;
	private boolean mouseWasDragged;
	
	//private final transient PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);
	
	public MultipleAlignmentView() {
		currentX = 0;
		currentY = 0;

		renderer = new MACharacterRenderer();
		cursor = new Cursor( new DefaultEditPolicy() );
		setEditorMode( EditorMode.SELECT );
		setEditable( true );
		Font font = new Font("Curier",Font.BOLD,12);
        setFont(font);
		int tmp = RenderUtils.pixelSizeForFont( font ) + 2;
		charBoxSize = new Dimension(tmp, tmp);
                
		ToolTipManager.sharedInstance().registerComponent(this);
		
		this.addMouseListener( this );
		this.addMouseMotionListener( this );
		this.addKeyListener( this );
		this.setFocusTraversalKeysEnabled(false);
		
		
		this.setVisibilityModel( new DefaultAlignmentVisibilityModel() );
		
		sequenceSelectionModel = new DefaultListSelectionModel();
		columnSelectionModel = new DefaultListSelectionModel();
		
		colorModel = SequenceColorModel.NUCLEOTIDE_MODEL;
		
		//this.headerView = new MAHeaderView();
		this.conservationView = new ConservationView();
		this.logoView = new LogoView();
	}
	
	@Override
	public void setFont( Font font ) {
		super.setFont( font );
		
//		double centerX = 0;
//		double centerY = 0;
		
//		if ( alignment != null ) {
//			Rectangle rect = convertToRectangle( translateFromScreenRect( getVisibleRect() )  );
//			centerX = rect.getCenterX();
//			centerY = rect.getCenterY();
//		}
		

		if (renderer != null) {
			renderer.setFont( font );
        }
		
		if (logoView != null) {
			logoView.setFont(font);
        }
		
		int tmp = RenderUtils.pixelSizeForFont( font ) + 2;
		setCharBoxSize(new Dimension(tmp, tmp));		
		needsRedraw = true;
//		if ( alignment != null ) {
//			sizeToFit();
//			Rectangle rect = convertToRectangle( translateFromScreenRect( getVisibleRect() )  );
//			
//			double targetWidth =  rect.getWidth();
//			double targetHeight = rect.getHeight();
//			
//			double distX = (targetWidth / 2);
//			double distY = (targetHeight / 2);
//			
//			
//			
//			double xAdjust = (( distX + centerX ) - alignment.length()); 
//			double yAdjust = (( distY + centerY ) - alignment.getSequenceCount()); 
//			
//			if ( xAdjust > 0 )
//				targetWidth -= xAdjust;
//			if ( yAdjust > 0 )
//				targetHeight -= yAdjust;
//			
//			double minX = (centerX - distX);
//			double minY = (centerY - distY);
//			
//			if ( minX < 0 ) {
//				targetWidth += minX;
//				minX = 0;
//			}
//				
//			if ( minY < 0 ) {
//				targetHeight += minY;
//				minY = 0;
//			}
//			Rectangle newRect =  new Rectangle( (int)minX, (int)minY, (int)targetWidth, (int)targetHeight );	
//			showRect( newRect );
//		}
		
		if (alignment!=null) {
			sizeToFit();
		}
//		repaint();
	}
		
	/*public JComponent getHeader() {
		return headerView;
	}*/
	
	
	public JComponent getConservationView() {
		return conservationView;
	}
	
	public JComponent getLogoView() {
		return logoView;
	}
	
	public void setAlignment( MultipleAlignment model ) {
		this.setAlignment( model, new DefaultAlignmentSelectionModel(model) );
	}
	
	public void setAlignment( MultipleAlignment alignment, DefaultAlignmentSelectionModel model ) {
		this.alignment = alignment;
		this.alignment.addAlignmentListener( this );
		this.alignment.addAlignmentListener( conservationView );
		this.alignment.addAlignmentListener( logoView );
			
		sizeToFit();
		//this.headerView.setPreferredSize( new Dimension( targetWidth, (int)headerView.getPreferredSize().getHeight() ) );
//		if (alignment.getAlphabet().equals( SequenceAlphabet.AMINOACIDS ) )
//			setSequenceColorModel( SequenceColorModel.AMINOACID_MODEL);
//		else if (alignment.getAlphabet().equals( SequenceAlphabet.RNA_STRUCTURE ) )
//			setSequenceColorModel(SequenceColorModel.RNA_STRUCTURE_MODEL);
//		else
//			setSequenceColorModel(SequenceColorModel.NUCLEOTIDE_MODEL);
		
		
				
		maSelectionModel = model;
		maSelectionModel.addAlignmentSelectionListener( this );
		//maSelectionModel.addAlignmentSelectionListener( headerView );
		cursor.setAlignment( alignment );
		
                logoView.needsRecalculate=true;
		logoView.setActive(true);
		//columnSelectionModel.addListSelectionListener( headerView );
	}
	
	public void setSequenceColorModel(SequenceColorModel model) {
		colorModel = model;
		colorModel.addColorModelListener( this);
		needsRedraw = true;
		repaint();
	}
	
	public SequenceColorModel getSequenceColorModel() {
		return colorModel;
	}
	
	public MultipleAlignment getAlignment() {
		return alignment;
	}
	
	public void setEditPolicy( AlignmentEditPolicy editPolicy ) {
		cursor.setEditPolicy( editPolicy );
	}
	
	public AlignmentEditPolicy getEditPolicy() {
		return cursor.getEditPolicy();
	}
	
	public void setVisibilityModel( AlignmentVisibilityModel visibilityModel ) {
		this.visibilityModel = visibilityModel;
		visibilityModel.addVisibilityListener( this );
	}
	
	public void setSelectionModel( AlignmentSelectionModel model ) {
		maSelectionModel = model;
		maSelectionModel.addAlignmentSelectionListener( this );
		//maSelectionModel.addAlignmentSelectionListener( headerView );
		
	}
	
	public void setIgnoreGaps( boolean flag ) {
		ignoreGaps = flag;
	}
	
	public boolean ignoresGaps() {
		return ignoreGaps;
	}
	
	public AlignmentSelectionModel getSelectionModel() {
		return maSelectionModel;
	}
	
	public ListSelectionModel getColumnSelectionModel() {
		return columnSelectionModel;
	}
	
	public void setSequenceSelectionModel( ListSelectionModel model ) {
		sequenceSelectionModel = model;
	}
	
	public ListSelectionModel getSequenceSelectionModel() {
		return sequenceSelectionModel;
	}
	
	public void showRect( Rectangle rect ) {
		Rectangle r = convertToRectangle( translateToScreenRect( rect ) );
		scrollRectToVisible( r );
	}
	
/*	public void writeSVG( OutputStream outStream ) throws IOException {
		GenericDocument doc = new GenericDocument( null, new SVGDOMImplementation() );
		SVGGraphics2D g = new SVGGraphics2D( doc );
		drawAlignment( g );
		Writer outWriter = new OutputStreamWriter(outStream, "UTF-8");
		g.stream( outWriter );
	}*/
	
	public void drawAlignment( Graphics2D g2, int seqFrom, int seqTo, int colFrom, int colTo, boolean doCaching ) {
		g2.setRenderingHint( RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON );
		
		g2.translate(2, 0);
		for ( int i = colFrom; i <= colTo; i++ ) {
			AffineTransform old = g2.getTransform();
			int state = MACharacterRenderer.MIDDLE;
			AffineTransform right = new AffineTransform();
//			if ( i == 0) {
//				state = MACharacterRenderer.START;
//				right.translate( ( getCharBoxSize() * 1.5 ) + 2, 0 );
//			}
//			else if ( i == (alignment.getLength() - 1) ) {
//				state = MACharacterRenderer.END;		
//			}
//			else {
				right.translate( getCharBoxSize().width + 2, 0 );
//			}
							
			for ( int j = seqFrom; j <= seqTo; j++ ) {		
				Character character = alignment.characterAt( j, i );
				boolean isSelected = maSelectionModel.isSelected( j, i, false );
				boolean isVisible = visibilityModel.isVisible( j, i );
				Color color = colorModel.getColorForChar( character.charValue() );	
				if ( isSelected )
					color = colorModel.getSelectionColor();	
				if ( isVisible )
					renderer.renderSymbol( g2, character, color, state, isSelected, true, doCaching );			
				g2.translate( 0, getCharBoxSize().height + 3 );
			}
			old.concatenate( right );
			g2.setTransform( old );
		}
	}
	
	@Override
	public boolean isFocusable() {
		return true;
	}
	
	private void drawAlignment( Graphics2D g2 ) {
		Rectangle2D visibleRect = getVisibleRect();
		
		g2.setColor( Color.WHITE );
		g2.fillRect(0,0,(int)visibleRect.getWidth(),(int)visibleRect.getHeight());
		
		int colFrom = xToColumn( visibleRect.getX() );
		int colTo = Math.min( xToColumn( visibleRect.getMaxX() ), alignment.getLength() - 1);
		
		int seqFrom = yToSequence( visibleRect.getY() );
		int seqTo = Math.min( yToSequence( visibleRect.getMaxY() ), alignment.getSequenceCount() - 1);
		
		g2.translate( columnToX(colFrom) - visibleRect.getX(), sequenceToY(seqFrom) - visibleRect.getY() );
		
		
		drawAlignment( g2, seqFrom, seqTo, colFrom, colTo, true );
	}

	@Override
	public void paintComponent( Graphics g ) {
		Rectangle2D visibleRect = this.getVisibleRect();
		((Graphics2D)g).setRenderingHint( RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON );
		if ( buffer == null || visibleRect.getHeight() != buffer.getHeight() || visibleRect.getWidth() != buffer.getWidth() ) {
			buffer = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration().createCompatibleImage((int)visibleRect.getWidth(), (int)visibleRect.getHeight());
			tmpBuffer = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration().createCompatibleImage((int)visibleRect.getWidth(), (int)visibleRect.getHeight());
			this.needsRedraw = true;
		}
		
		if ( visibleRect.getX() != currentX ) {
			currentX = visibleRect.getX();
			currentY = visibleRect.getY();
			bufferedRedrawX = true;
			
		}
		
		if( visibleRect.getY() != currentY ) {
			currentX = visibleRect.getX();
			currentY = visibleRect.getY();
			bufferedRedrawY = true;
		}
		
		if ( alignment != null && needsRedraw ) {
			
			Graphics2D g2 = (Graphics2D)buffer.getGraphics();
			//g2.setRenderingHint( RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON );
			
			drawAlignment( g2 );
			needsRedraw = false;
		}
		else if ( alignment != null && ( bufferedRedrawX || bufferedRedrawY ) ) {
			Graphics2D g2 = (Graphics2D)tmpBuffer.getGraphics();
			
			g2.setColor( Color.WHITE );
			g2.fillRect(0,0,(int)visibleRect.getWidth(),(int)visibleRect.getHeight());
			
			
			g2.setRenderingHint( RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON );
			
			//double x = visibleRect.getX() + (visibleRect.getX() - currentViewRect.getX());
			//double y = visibleRect.getY() + (visibleRect.getY() - currentViewRect.getY());
			
			AffineTransform prev = g2.getTransform();
			g2.translate( currentViewRect.getX() - visibleRect.getX(),currentViewRect.getY() - visibleRect.getY() );
			g2.drawImage( buffer, (int)0, (int)0, null);
			g2.setTransform( prev );
			
			if ( bufferedRedrawX ) {
				if ( visibleRect.getX() < currentViewRect.getX() ) {
					int colFrom = xToColumn( visibleRect.getX() );
					int colTo = Math.min( xToColumn( currentViewRect.getX() ), alignment.getLength() - 1);
					
					int seqFrom = yToSequence( visibleRect.getY() );
					int seqTo = Math.min( yToSequence( visibleRect.getMaxY() ), alignment.getSequenceCount() - 1);
					
					g2.translate( columnToX(colFrom) - visibleRect.getX(), sequenceToY(seqFrom) - visibleRect.getY() );
					
					drawAlignment( g2, seqFrom, seqTo, colFrom, colTo, true );
				}
				else {
					
					int colFrom = xToColumn( currentViewRect.getMaxX() );
					int colTo = Math.min( xToColumn( visibleRect.getMaxX() ), alignment.getLength() - 1);
					
					int seqFrom = yToSequence( visibleRect.getY() );
					int seqTo = Math.min( yToSequence( visibleRect.getMaxY() ), alignment.getSequenceCount() - 1);
					
					g2.translate( columnToX(colFrom) - visibleRect.getX(), sequenceToY(seqFrom) - visibleRect.getY() );
					
					drawAlignment( g2, seqFrom, seqTo, colFrom, colTo, true );
				}
				bufferedRedrawX = false;
				
			}
			
			
			if ( bufferedRedrawY ) {
				if ( visibleRect.getY() < currentViewRect.getY() ) {
					int colFrom = xToColumn( visibleRect.getMinX() );
					int colTo = Math.min( xToColumn( visibleRect.getMaxX() ), alignment.length() - 1);
						
					int seqFrom = yToSequence( visibleRect.getMinY() );
					int seqTo = Math.min( yToSequence( currentViewRect.getY() ), alignment.getSequenceCount() - 1);
					
					g2.translate( columnToX(colFrom) - visibleRect.getX(), sequenceToY(seqFrom) - visibleRect.getY() );
					
					drawAlignment( g2, seqFrom, seqTo, colFrom, colTo, true );
				}
				else {
					int colFrom = xToColumn( visibleRect.getMinX() );
					int colTo = Math.min( xToColumn( visibleRect.getMaxX() ), alignment.length() - 1);
										
					int seqFrom = yToSequence( currentViewRect.getMaxY() );
					int seqTo = Math.min( yToSequence( visibleRect.getMaxY() ), alignment.getSequenceCount() - 1);
					
					g2.translate( columnToX(colFrom) - visibleRect.getX(), sequenceToY(seqFrom) - visibleRect.getY() );
					
					drawAlignment( g2, seqFrom, seqTo, colFrom, colTo, true );
				}
				bufferedRedrawY = false;	
			}
			
			Graphics gb = buffer.getGraphics();
			gb.drawImage( tmpBuffer, (int)0, (int)0, null);
		}
		g.drawImage( buffer, (int)visibleRect.getX(), (int)visibleRect.getY(), null);
		
		if ( editable && ( cursor.isActive() || draggedCursorRect != null ) ) {
			
			Rectangle2D drawCursorRect = draggedCursorRect;
			if ( drawCursorRect == null ) {
				drawCursorRect = translateToScreenRect( cursor.getRect() );
			}
			if ( drawCursorRect != null ) {
				Color c = new Color( 0f,0f,0f,0.2f);
				g.setColor( c );
				((Graphics2D) g).fill( drawCursorRect );
				
				if( isFocusOwner()) {
					g.setColor( Color.BLACK );
				}
				else 
				{
					g.setColor( Color.GRAY );
				}
				((Graphics2D) g).setStroke( new BasicStroke( 2 ) );
				((Graphics2D) g).draw( drawCursorRect );
			}
		}
		
		if (  editMode == EditorMode.SELECT  && drawSelectionRect != null ) {
			Color c = new Color( 1f,0f,0f,0.2f);
			g.setColor( c );
			((Graphics2D) g).fill( drawSelectionRect );
			
			g.setColor( Color.RED );
			((Graphics2D) g).setStroke( new BasicStroke( 2 ) );
			((Graphics2D) g).draw( drawSelectionRect );
			
		}
		
		currentViewRect = visibleRect;
	}

	public double columnToX( int column ) {
		if ( column == 0 )
			return 0;
//		return column * (getCharBoxSize() + 2) + getCharBoxSize()/2;
		return column * (getCharBoxSize().width + 2);
	}
	
	public double sequenceToY( int sequence ) {
		return sequence * (getCharBoxSize().height + 3);
	}
	
	public int xToColumn( double x ) {
		//double tmp = x - (getCharBoxSize() / 2);
		double tmp = x;
		if ( tmp > 0 ) {
			return (int) (tmp / (getCharBoxSize().width + 2));
		}
		else {
			return 0;
		}
	}
	
	public int yToSequence( double y ) {
		return (int) (y / (getCharBoxSize().height + 3));
	}

	public String getToolTipText( MouseEvent e ) {
		int seq = yToSequence( e.getY());
		int x = xToColumn( e.getX() );
		
		if ( (seq > alignment.getSequenceCount()-1) 
			|| (x > alignment.getLength()-1)) {
			return "";
		}
		
		return "site:" + (x+1 ) + " | " + alignment.getSequence(seq).getName();
	}
	
	/* (non-Javadoc)
	 * @see java.awt.event.MouseListener#mouseClicked(java.awt.event.MouseEvent)
	 */
	@Override
	public void mouseClicked(MouseEvent e) {
		int row = yToSequence(e.getY());
		int col = xToColumn(e.getX());
		
		if (row<0 || row>=alignment.rows() || col<0 || col>=alignment.length()) {
			maSelectionModel.deselectAll();
			return;
		}
		
		maSelectionModel.begin();
		if ( maSelectionModel.isSelected(row, col, ignoreGaps ) ) {
			if ( !e.isShiftDown() && !e.isControlDown()) {
				maSelectionModel.deselectAll();
			}		
			maSelectionModel.deselect(row, col);
		}
		else {
			if ( !e.isShiftDown() && !e.isControlDown()) {
				maSelectionModel.deselectAll();
			}		
			maSelectionModel.select(row, col, ignoreGaps );
		}
		maSelectionModel.end();
	}

	/* (non-Javadoc)
	 * @see java.awt.event.MouseListener#mousePressed(java.awt.event.MouseEvent)
	 */
	public void mousePressed(MouseEvent e) {
		requestFocusInWindow();
		
		startDrag = e.getPoint();
		startDrag = new Point( (int)columnToX( xToColumn( startDrag.getX() ) ), (int)sequenceToY( yToSequence( startDrag.getY() ) ) );
	}

	/* (non-Javadoc)
	 * @see java.awt.event.MouseListener#mouseReleased(java.awt.event.MouseEvent)
	 */
	public void mouseReleased(MouseEvent e) {
		if (!mouseWasDragged) return;
		
		if ( e.getButton() != MouseEvent.BUTTON1)
			return;
		
		endDrag = e.getPoint();
		endDrag = new Point( (int)columnToX( xToColumn( endDrag.getX() ) ), (int)sequenceToY( yToSequence( endDrag.getY() ) ) );
		
		double minXd = Math.min( startDrag.getX(), endDrag.getX() );
		double minYd = Math.min( startDrag.getY(), endDrag.getY() );
		double maxXd = Math.max( startDrag.getX(), endDrag.getX() );
		double maxYd = Math.max( startDrag.getY(), endDrag.getY() );
		
		minXd = Math.min( minXd, alignmentScreenRect.getMaxX() );
		minYd = Math.min( minYd, alignmentScreenRect.getMaxY() );
		maxXd = Math.min( maxXd, alignmentScreenRect.getMaxX() );
		maxYd = Math.min( maxYd, alignmentScreenRect.getMaxY() );
		
		int cols = alignment.getLength();
		int seqs = alignment.getSequenceCount();
		
		int minX = xToColumn( minXd );
		int minY = yToSequence( minYd );
		
		
		if ( minX > cols-1 || minY > seqs) {
			return;
		}
		
		minX = Math.min(minX, cols-1);
		minY = Math.min(minY, seqs-1);
		int maxX = Math.min(xToColumn( maxXd ), cols-1);
		int maxY = Math.min(yToSequence( maxYd ), seqs-1);
		
		if ( editMode == EditorMode.SELECT ) {
			maSelectionModel.begin();
			Rectangle sel = new Rectangle( minX, minY, maxX-minX + 1, maxY-minY + 1 );
			if ( maSelectionModel.isSelected(sel, ignoreGaps ) ) {
				if ( !e.isShiftDown() && !e.isControlDown()) {
					maSelectionModel.deselectAll();
				}
				else {
					maSelectionModel.deselect(sel);
				}
			}
			else {
				if ( !e.isShiftDown() && !e.isControlDown()) {
					maSelectionModel.deselectAll();
				}
				maSelectionModel.select(sel, ignoreGaps );
			}
			drawSelectionRect = null;
			maSelectionModel.end();
		}
		else if ( editMode == EditorMode.EDIT ) {
			draggedCursorRect = null;
			setCursorRect( new Rectangle( minX, minY, maxX-minX + 1, maxY-minY + 1 ));
		}
		//repaint();
		this.mouseWasDragged=false;
	}

	public void mouseDragged(MouseEvent e) {
		this.mouseWasDragged=true;
		
		endDrag = e.getPoint();
		endDrag = new Point( (int)columnToX( xToColumn( endDrag.getX() ) ), (int)sequenceToY( yToSequence( endDrag.getY() ) ) );
		
		double minXd = Math.min( startDrag.getX(), endDrag.getX() );
		double minYd = Math.min( startDrag.getY(), endDrag.getY() );
		double maxXd = Math.max( startDrag.getX(), endDrag.getX() );
		double maxYd = Math.max( startDrag.getY(), endDrag.getY() );
		
		if ( editMode == EditorMode.SELECT  ) {
			drawSelectionRect = new Rectangle2D.Double( minXd, minYd, maxXd-minXd+getCharBoxSize().width, maxYd-minYd+getCharBoxSize().height );
			drawSelectionRect = drawSelectionRect.createIntersection( alignmentScreenRect );
			
		}
		else if ( editMode == EditorMode.EDIT ) {
			draggedCursorRect = new Rectangle2D.Double( minXd, minYd, maxXd-minXd+getCharBoxSize().width, maxYd-minYd+getCharBoxSize().height );
			draggedCursorRect = draggedCursorRect.createIntersection( alignmentScreenRect );
			
		}
		
		Rectangle r = new Rectangle(e.getX(), e.getY(), 1, 1);
	    scrollRectToVisible(r);
		
		repaint();
	}

	public void setCursorRect( Rectangle cursorRect ) {
		if ( cursor.getRect() != null && cursor.getRect().equals(cursorRect) )
			return;
		
		if ( cursorRect.getMaxX() > alignment.length() || cursorRect.getMaxY() > alignment.getSequenceCount()
				|| cursorRect.getMinX() < 0 || cursorRect.getMinY() < 0 )
			return;
			
		
		cursor.setRect( cursorRect);
		cursor.setActive( true );
		showRect( cursorRect );
		
		repaint();
	}
	
	protected Rectangle2D translateToScreenRect( Rectangle rect ) {
		if ( rect == null )
			return null;
		
		double minXd = columnToX( (int) rect.getMinX() );
		double minYd = sequenceToY( (int) rect.getMinY() );
		double maxXd = columnToX( (int) rect.getMaxX() );
		double maxYd = sequenceToY( (int) rect.getMaxY() );
		
		return new Rectangle2D.Double( minXd, minYd, maxXd-minXd/*-2*/, maxYd-minYd/*-2*/ );			
	}
	
	protected Rectangle2D translateFromScreenRect( Rectangle screenRect ) {
		if ( screenRect == null )
			return null;
		
		double minXd = xToColumn( (int) screenRect.getMinX() );
		double minYd = yToSequence( (int) screenRect.getMinY() );
		double maxXd = xToColumn( (int) screenRect.getMaxX() );
		double maxYd = yToSequence( (int) screenRect.getMaxY() );
		
		return new Rectangle2D.Double( minXd, minYd, maxXd-minXd/*-2*/, maxYd-minYd/*-2*/ );			
	}
	
	protected Rectangle convertToRectangle( Rectangle2D rectangle ) {
		return new Rectangle( (int) rectangle.getMinX(), (int) rectangle.getMinY(), (int) rectangle.getWidth(), (int) rectangle.getHeight() );
	} 
	
	public void removeCursor() {
		cursor.setActive( false );
	}
	
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	/* (non-Javadoc)
	 * @see java.awt.event.MouseListener#mouseEntered(java.awt.event.MouseEvent)
	 */
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see java.awt.event.MouseListener#mouseExited(java.awt.event.MouseEvent)
	 */
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	boolean inSelectionProcess;

	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.ca.ext.alignment.MASelectionListener#selectionChanged(de.biozentrum.bioinformatik.ca.ext.alignment.MASelectionEvent)
	 */
	public void selectionChanged(AlignmentSelectionEvent e) {
		if ( !inSelectionProcess) {
			this.needsRedraw = true;
			repaint();		
		}
	}
	
	public void selectionDidEnd() {
		inSelectionProcess = false;
		this.needsRedraw = true;
		repaint();
	}


	public void selectionWillBegin() {
		inSelectionProcess = true;
		
	}
	
	public void sizeToFit() {
		alignment.pack();
		
		int height = alignment.getSequenceCount() * (getCharBoxSize().height + 3);
		//int targetWidth = (int) ((alignment.getLength() - 2) * (getCharBoxSize() + 2) + 2 + 2 * ((1.5 *getCharBoxSize()) + 2));
		int width = alignment.getLength() * (getCharBoxSize().width + 2);
		this.setPreferredSize( new Dimension( width, height ) );
		this.setMaximumSize( new Dimension( width, height ) );
		this.alignmentScreenRect = new Rectangle( 0,0,width,height-3 );
		
		this.needsRedraw = true;
		
		conservationView.setPreferredSize( new Dimension( width, getLogoHeight()) );
		conservationView.needsRedraw = true;
		conservationView.revalidate();
		conservationView.repaint();
		
		logoView.setPreferredSize( new Dimension( width, getLogoHeight()) );
		logoView.needsRedraw = true;
		logoView.revalidate();
		logoView.repaint();
		
		revalidate();
		repaint();
	}
	
	public void keyTyped(KeyEvent e) {
		
		
		
	}

	public void keyPressed(KeyEvent e) {
		if ( cursor.isActive() && editable ) {	
			boolean beep = false;
			Rectangle newCursor;
			Rectangle cursorRect = cursor.getRect();
			
			switch ( e.getKeyCode() ) {
			case KeyEvent.VK_DELETE:
				if( !cursor.delete() )
					beep = true;
				break;
			case KeyEvent.VK_BACK_SPACE:
				if( !cursor.performBackSpace() )
					beep = true;
				break;
			case KeyEvent.VK_SPACE:
				if ( !cursor.insertChar( '-' ) )
					beep = true;
				break;
			case KeyEvent.VK_LEFT:
				newCursor = new Rectangle( (int)(cursorRect.getMinX() - 1),(int)cursorRect.getMinY(),(int)cursorRect.getWidth(), (int)cursorRect.getHeight() );
				setCursorRect( newCursor);
				e.consume();
				break;
			case KeyEvent.VK_RIGHT:
				newCursor = new Rectangle( (int)(cursorRect.getMinX() + 1 ),(int)cursorRect.getMinY(),(int)cursorRect.getWidth(), (int)cursorRect.getHeight() );
				setCursorRect( newCursor);
				e.consume();
				break;
			case KeyEvent.VK_UP:
				newCursor = new Rectangle( (int)cursorRect.getMinX(),(int)(cursorRect.getMinY() - 1),(int)cursorRect.getWidth(), (int)cursorRect.getHeight() );
				setCursorRect( newCursor);
				e.consume();
				break;
			case KeyEvent.VK_DOWN:
				newCursor = new Rectangle( (int)cursorRect.getMinX(),(int)(cursorRect.getMinY() + 1),(int)cursorRect.getWidth(), (int)cursorRect.getHeight() );
				setCursorRect( newCursor);
				e.consume();
				break;
			default:
				if ( !cursor.insertChar( e.getKeyChar() ) )
					beep = true;
			}
			
			if ( beep )
				Toolkit.getDefaultToolkit().beep();
		
		}
	}

	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	
	public void alignmentChanged(AlignmentChangedEvent e) {
		Rectangle rect = e.getRectangle();
		Rectangle cursorRect = cursor.getRect();
		
		if ( editable && cursorRect != null ) {
			if ( ( cursorRect.getMinY() >= rect.getMinY() && cursorRect.getMinY() < rect.getMaxY() )
				|| ( cursorRect.getMaxY() > rect.getMinY() && cursorRect.getMaxY() <= rect.getMaxY() )) {
				if( cursorRect.getMinX() >= rect.getMinX() && e.getType() == MAEventType.INSERT ) {
					Rectangle newCursor = new Rectangle( (int)(cursorRect.getMinX() + rect.getWidth() ), (int)cursorRect.getY(), (int) cursorRect.getWidth(), (int) cursorRect.getHeight() );
					setCursorRect( newCursor );
				}
				if( cursorRect.getMinX() >= rect.getMaxX() && e.getType() == MAEventType.DELETE ) {
					Rectangle newCursor = new Rectangle( (int)(cursorRect.getMinX() - rect.getWidth() ), (int)cursorRect.getY(), (int) cursorRect.getWidth(), (int) cursorRect.getHeight() );
					setCursorRect( newCursor );
				}
			}
		} 
		sizeToFit();
	}

	public void alignmentStructureChanged(AlignmentStructureChangedEvent e) {
		needsRedraw = true;
		
		if ( e.getType() != MAEventType.UPDATE)
			sizeToFit();
		
		repaint();
	}
	
	public void visibilityChanged(AlignmentVisibilityChangedEvent e) {
		needsRedraw = true;
		repaint();	
	
	}

	public void setEditorMode(EditorMode editMode) {
		if ( editMode == EditorMode.EDIT )
			cursor.setActive( true );
		else if ( editMode == EditorMode.SELECT ) 
			cursor.setActive( false );
		else if ( editMode == EditorMode.NONE ) 
			cursor.setActive( false );
		this.editMode = editMode;
		repaint();
	}

	public EditorMode getEditorMode() {
		return editMode;
	}

	public void setEditable(boolean editable) {
		this.editable = editable;
	}

	public boolean isEditable() {
		return editable;
	}

	public void setCursorMode(int cursorMode) {
		cursor.setCursorMode( cursorMode );
	}

	public int getCursorMode() {
		return cursor.getCursorMode();
	}

    /**
     * @return the charBoxSize
     */
    public Dimension getCharBoxSize() {
        return charBoxSize;
    }

	public void setCharBoxSize(Dimension charBoxSize) {
		Dimension oldCharBoxSize = this.charBoxSize;
		this.charBoxSize = charBoxSize;
		if (renderer!=null) {
			renderer.setSize(charBoxSize);
		}
		firePropertyChange(PROP_CHARBOXSIZE, oldCharBoxSize, charBoxSize);
	}

    /**
     * @return the logoHeight
     */
    public int getLogoHeight() {
        return logoHeight;
    }

    /**
     * @param logoHeight the logoHeight to set
     */
    public void setLogoHeight(int logoHeight) {
        this.logoHeight = logoHeight;
    }

	/**
	 * @return the startDrag
	 */
	public Point getStartDrag() {
		return startDrag;
	}

	/**
	 * @return the endDrag
	 */
	public Point getEndDrag() {
		return endDrag;
	}

	/**
	 * @return the visibilityModel
	 */
	public AlignmentVisibilityModel getVisibilityModel() {
		return visibilityModel;
	}

	private class LogoView extends JPanel implements AlignmentListener, MouseListener {
		private boolean needsRedraw;
		private boolean isActive = false;
		private boolean needsRecalculate = true;
		private BufferedImage buffer;
		private BufferedImage tmpBuffer;
		private double currentX;
		private ArrayList<HashMap<Character,Double>> cache;
		private ArrayList<Double> hCache;
		private ArrayList<ArrayList<Character>> characterCache;
		private Font font;
		private Rectangle2D currentViewRect;
		private ArrayList<Double> conservation;
		private boolean showLogo = false;
		private double maxH=0;
		
		public LogoView() {
			addMouseListener( this );
			setPreferredSize( new Dimension( 0, getLogoHeight()) );
			cache = new ArrayList<HashMap<Character,Double>>();
			hCache = new ArrayList<Double>();
			characterCache = new ArrayList<ArrayList<Character>>();
			conservation = new ArrayList<Double>();
			setBackground( Color.WHITE );
//			setBorder(null);
		}
		
		public void setFont( Font f) {
			super.setFont(f);
			font = f;
			int size = f.getSize();
			size = size * 2;
			font = new Font( f.getFontName(), f.getStyle(), size );
			
			
		}
		
		public void setActive( boolean active) {
			if ( active && needsRecalculate) {
				maxH=0;
				cache.clear();
				hCache.clear();
				characterCache.clear();
				conservation.clear();
				for ( int i = 0; i < alignment.getLength(); i++) {
					fillCache( i);
				}
				needsRecalculate = false;
			}
			isActive = active;
			repaint();
		}
		
			public void paintComponent( Graphics g ) {
				((Graphics2D)g).setRenderingHint(RenderingHints.KEY_INTERPOLATION,RenderingHints.VALUE_INTERPOLATION_BICUBIC);
				((Graphics2D)g).setRenderingHint(RenderingHints.KEY_RENDERING,RenderingHints.VALUE_RENDER_QUALITY);		
				((Graphics2D)g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

				
				super.paintComponent( g);
				if ( isActive ) {
					Rectangle2D visibleRect = this.getVisibleRect();
					
					if ( buffer == null || visibleRect.getHeight() != buffer.getHeight() || visibleRect.getWidth() != buffer.getWidth() ) {
						buffer = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration().createCompatibleImage((int)visibleRect.getWidth(), (int)visibleRect.getHeight());
						tmpBuffer = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration().createCompatibleImage((int)visibleRect.getWidth(), (int)visibleRect.getHeight());
						this.needsRedraw = true;
					}
					
					if ( visibleRect.getX() != currentX ) {
						currentX = visibleRect.getX();
						currentY = visibleRect.getY();
						bufferedRedrawX = true;
						
					}
					
					if ( alignment != null && needsRedraw ) {
						
						Graphics2D g2 = (Graphics2D)buffer.getGraphics();
						//g2.setRenderingHint( RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON );
						
						draw( g2 );
						needsRedraw = false;
					}
					else if ( alignment != null && bufferedRedrawX ) {
						Graphics2D g2 = (Graphics2D)tmpBuffer.getGraphics();
						
						g2.setColor( Color.WHITE );
						g2.fillRect(0,0,(int)visibleRect.getWidth(),(int)visibleRect.getHeight());
						
						
						AffineTransform prev = g2.getTransform();
						g2.translate( currentViewRect.getX() - visibleRect.getX(),currentViewRect.getY() - visibleRect.getY() );
						g2.drawImage( buffer, (int)0, (int)0, null);
						g2.setTransform( prev );
						
						if ( bufferedRedrawX ) {
							if ( visibleRect.getX() < currentViewRect.getX() ) {
								int colFrom = xToColumn( visibleRect.getX() );
								int colTo = Math.min( xToColumn( currentViewRect.getX() ), alignment.getLength() - 1);
								
								int seqFrom = yToSequence( visibleRect.getY() );
								int seqTo = Math.min( yToSequence( visibleRect.getMaxY() ), alignment.getSequenceCount() - 1);
								
								g2.translate( columnToX(colFrom) - visibleRect.getX(), sequenceToY(seqFrom) - visibleRect.getY() );
								
								draw( g2,  colFrom, colTo );
							}
							else {
								
								int colFrom = xToColumn( currentViewRect.getMaxX() );
								int colTo = Math.min( xToColumn( visibleRect.getMaxX() ), alignment.getLength() - 1);
								
								int seqFrom = yToSequence( visibleRect.getY() );
								int seqTo = Math.min( yToSequence( visibleRect.getMaxY() ), alignment.getSequenceCount() - 1);
								
								g2.translate( columnToX(colFrom) - visibleRect.getX(), sequenceToY(seqFrom) - visibleRect.getY() );
								
								draw( g2, colFrom, colTo );
							}
							bufferedRedrawX = false;
							
						}
						
						Graphics gb = buffer.getGraphics();
						gb.drawImage( tmpBuffer, (int)0, (int)0, null);
					}
					g.drawImage( buffer, (int)visibleRect.getX(), (int)visibleRect.getY(), null);
					currentViewRect = visibleRect;
				}
			}
		
			
			public void draw( Graphics2D g2 ) {
				Rectangle2D visibleRect = getVisibleRect();
				
				
				g2.setColor( Color.WHITE );
				g2.fillRect(0,0,(int)visibleRect.getWidth(), getLogoHeight());
				//g2.setRenderingHint( RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON );
				
				int colFrom = xToColumn( visibleRect.getX() );
				int colTo = Math.min( xToColumn( visibleRect.getMaxX() ), alignment.getLength() - 1);
				
				g2.translate( columnToX(colFrom) - visibleRect.getMinX(), 0 );
				
				draw( g2, colFrom, colTo);
			}
			
			public void draw( Graphics2D g2, int colFrom, int colTo ) {
				if( showLogo && font.getSize() > 8) {
					g2.setFont( font);
					for ( int i = colFrom; i <= colTo; i++ ) {
						drawLogo( g2, i);
						g2.translate( getCharBoxSize().width + 2,0 );
					}
				}
				else{
					for ( int i = colFrom; i <= colTo; i++ ) {
						
						double conservation = this.conservation.get(i);
						double height = (getLogoHeight()-4) * conservation;
						Rectangle2D cRect = new Rectangle2D.Double( 2, getLogoHeight() - height - 2 , getCharBoxSize().width, height );
						
						Color fillColor = null;
						
						if ( conservation > 0.50 ) {
							fillColor = new Color( (int) (255 * (1 - ((conservation - 0.5) * 2))) , 255,0);
							
						}
						else {
							fillColor = new Color( 255, (int) (255 * conservation * 2),0);
							
						}
					
						
						g2.setColor( fillColor );
						g2.fill( cRect );
						

						g2.translate( getCharBoxSize().width + 2,0 );
					}
				}
				
			}
		
		public void fillCache( int column) {
			SequenceAlphabet alphabet = alignment.getAlphabet();
			ArrayList<Character> characters = new ArrayList<Character>();
			HashMap<Character,Double> charToValue = new HashMap<Character,Double>();
			double h = 0;
			for ( int i = 0; i < alphabet.size(); i++ ) {
				Character c = (Character) alphabet.characterAt( i );
				
				double relFreq = (double)getCharCount( column, c ) / (double)alignment.getSequenceCount();			
				if ( c.charValue() == '-' || relFreq == 0 )
					continue;
				
				h += relFreq * log2(relFreq );
				int j = 0;
				for ( j= 0; j < characters.size(); j++ ) {
					if (((Double)charToValue.get( characters.get(j) )).doubleValue() > relFreq )
						break;
				}
				characters.add( j, c );
				charToValue.put(c, new Double(relFreq) );
			}
			h = -h;
			cache.add( column, charToValue);
			hCache.add( column, h);
			if (h > maxH) {
				maxH = h;
			}
			characterCache.add( column, characters);
			conservation.add( column, getConservationAt( column));
		}
		
		public double getConservationAt( int column ) {
			int max = 0;
			HashMap<Character,Integer> map = new HashMap<Character,Integer>();
			for ( int i = 0; i < alignment.getSequenceCount(); i++ ) {
				Character character = alignment.characterAt( i, column );
				if ( character.charValue() == '-')
					continue;
				Integer integer = map.get( character );
				if ( integer != null )
					integer += 1;
				else {
					integer = 1;				
				}
				if ( max < integer )
					max = integer;
				map.put( character, integer );
			}
			return (double)max / (double)alignment.getSequenceCount();
		}
		
		public void drawLogo( Graphics2D g2, int column ) {
			g2 = (Graphics2D)g2.create();
			g2.setFont(g2.getFont().deriveFont(12.0f));
			FontRenderContext frc = g2.getFontRenderContext();
			SequenceAlphabet alphabet = alignment.getAlphabet();
			ArrayList<Character> characters = characterCache.get(column);
			if (maxH==0) maxH=1;
			double r = (log2( alphabet.size()-1 ) - hCache.get( column));
			double rscaled = r * getLogoHeight()/(log2(alphabet.size()-1 - maxH));
			g2.translate( 0, getLogoHeight());
			for (Character c : characters) {
				String s = String.valueOf( c.charValue() );
				double value = cache.get(column).get(c);
				double targetHeight = value * rscaled;
				int targetWidth = (getCharBoxSize().width-6);
				GlyphVector gv = g2.getFont().createGlyphVector(frc, s);				
				Rectangle bounds = gv.getPixelBounds(frc, 0, 0);
				double widthScale = targetWidth / bounds.getWidth();
				double heightScale = targetHeight / (bounds.getHeight());
				
				g2.setColor( colorModel.getColorForChar(c));
				double xpos = (getCharBoxSize().width + 2 - targetWidth) / 2.0 + 1;

				AffineTransform old = g2.getTransform();
				g2.translate(xpos, 0);				
				g2.scale( widthScale,  heightScale);
				g2.drawString( s, -bounds.x, 0 );
				g2.setTransform(old);
				
				g2.translate(0, -targetHeight - 1 );
			}
			g2.dispose();
		}
		
		public double log2( double d ) {
			return Math.log( d ) / Math.log( 2 );
		}
		
		public int getCharCount( int column, Character character ) {
			int count = 0;
			for ( int i = 0; i < alignment.getSequenceCount(); i++ ) {
				if ( alignment.characterAt( i, column ).equals( character ) )
					count++;
			}
			return count;
		}
		

		public void mouseClicked(MouseEvent e) {
			if ( e.getButton() == MouseEvent.BUTTON3)
				setActive( !isActive);
			else if ( e.getButton() == MouseEvent.BUTTON1) {
				showLogo = !showLogo;
				needsRedraw = true;
				repaint();
			}
			
		}

		public void mousePressed(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		public void mouseReleased(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		public void mouseEntered(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		public void mouseExited(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		public void alignmentChanged(AlignmentChangedEvent e) {
//			if ( isActive)
//				setActive( false);
			needsRecalculate = true;
			if (isActive)
				setActive(true);
		}

		public void alignmentStructureChanged(AlignmentStructureChangedEvent e) {
//			if ( isActive)
//				setActive( false);
			needsRecalculate = true;
			if (isActive)
				setActive(true);
		}
		
		
		
	}

	private class ConservationView extends JPanel implements MouseListener, AlignmentListener{
		private boolean isActive = false;
		private boolean needsRecalculate = true;
		private boolean needsRedraw;
		private BufferedImage buffer;
		private BufferedImage tmpBuffer;
		private double currentX;
		private Rectangle2D currentViewRect;
		private ArrayList<Double> conservation;
		
		public ConservationView() {
			setPreferredSize( new Dimension( 0, getLogoHeight()) );
			setBackground( Color.WHITE );
			conservation = new ArrayList<Double>();
			this.addMouseListener( this);
		}
		
		public void setActive( boolean active) {
			if ( active && needsRecalculate) {
				conservation.clear();
				for ( int i = 0; i < alignment.getLength(); i++) {
					conservation.add( i, getConservationAt( i));
				}
				needsRecalculate = false;
			}
			isActive = active;
			repaint();
		}
		
		public void paintComponent( Graphics g ) {
			super.paintComponent( g);
			if ( isActive) {
				Rectangle2D visibleRect = this.getVisibleRect();
				
				if ( buffer == null || visibleRect.getHeight() != buffer.getHeight() || visibleRect.getWidth() != buffer.getWidth() ) {
					buffer = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration().createCompatibleImage((int)visibleRect.getWidth(), (int)visibleRect.getHeight());
					tmpBuffer = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration().createCompatibleImage((int)visibleRect.getWidth(), (int)visibleRect.getHeight());
					this.needsRedraw = true;
				}
				
				if ( visibleRect.getX() != currentX ) {
					currentX = visibleRect.getX();
					currentY = visibleRect.getY();
					bufferedRedrawX = true;
					
				}
				
				if ( alignment != null && needsRedraw ) {
					
					Graphics2D g2 = (Graphics2D)buffer.getGraphics();
					//g2.setRenderingHint( RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON );
					
					draw( g2 );
					needsRedraw = false;
				}
				else if ( alignment != null && bufferedRedrawX ) {
					Graphics2D g2 = (Graphics2D)tmpBuffer.getGraphics();
					
					g2.setColor( Color.WHITE );
					g2.fillRect(0,0,(int)visibleRect.getWidth(),(int)visibleRect.getHeight());
					
					
					AffineTransform prev = g2.getTransform();
					g2.translate( currentViewRect.getX() - visibleRect.getX(),currentViewRect.getY() - visibleRect.getY() );
					g2.drawImage( buffer, (int)0, (int)0, null);
					g2.setTransform( prev );
					
					if ( bufferedRedrawX ) {
						if ( visibleRect.getX() < currentViewRect.getX() ) {
							int colFrom = xToColumn( visibleRect.getX() );
							int colTo = Math.min( xToColumn( currentViewRect.getX() ), alignment.getLength() - 1);
							
							int seqFrom = yToSequence( visibleRect.getY() );
							int seqTo = Math.min( yToSequence( visibleRect.getMaxY() ), alignment.getSequenceCount() - 1);
							
							g2.translate( columnToX(colFrom) - visibleRect.getX(), sequenceToY(seqFrom) - visibleRect.getY() );
							
							draw( g2,  colFrom, colTo );
						}
						else {
							
							int colFrom = xToColumn( currentViewRect.getMaxX() );
							int colTo = Math.min( xToColumn( visibleRect.getMaxX() ), alignment.getLength() - 1);
							
							int seqFrom = yToSequence( visibleRect.getY() );
							int seqTo = Math.min( yToSequence( visibleRect.getMaxY() ), alignment.getSequenceCount() - 1);
							
							g2.translate( columnToX(colFrom) - visibleRect.getX(), sequenceToY(seqFrom) - visibleRect.getY() );
							
							draw( g2, colFrom, colTo );
						}
						bufferedRedrawX = false;
						
					}
					
					Graphics gb = buffer.getGraphics();
					gb.drawImage( tmpBuffer, (int)0, (int)0, null);
				}
				g.drawImage( buffer, (int)visibleRect.getX(), (int)visibleRect.getY(), null);
				currentViewRect = visibleRect;
			}
		}
		
		public void draw( Graphics2D g2 ) {
			Rectangle2D visibleRect = getVisibleRect();
			
			g2.setColor( Color.WHITE );
			g2.fillRect(0,0,(int)visibleRect.getWidth(), getLogoHeight());
			//g2.setRenderingHint( RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON );
			
			int colFrom = xToColumn( visibleRect.getX() );
			int colTo = Math.min( xToColumn( visibleRect.getMaxX() ), alignment.getLength() - 1);
			
			g2.translate( columnToX(colFrom) - visibleRect.getMinX(), 0 );
			
			draw( g2, colFrom, colTo);
		}
		
		public void draw( Graphics2D g2, int colFrom, int colTo ) {
//			if ( colFrom == 0 )
//				g2.translate( getCharBoxSize() / 2, 0 );
			
			for ( int i = colFrom; i <= colTo; i++ ) {
				
				double conservation = this.conservation.get(i);
				double height = (26) * conservation;
				Rectangle2D cRect = new Rectangle2D.Double( 2, getLogoHeight() - height - 2 , getCharBoxSize().width, height );
				
				Color fillColor = null;
				
				if ( conservation > 0.50 ) {
					fillColor = new Color( (int) (255 * (1 - ((conservation - 0.5) * 2))) , 255,0);
					
				}
				else {
					fillColor = new Color( 255, (int) (255 * conservation * 2),0);
					
				}
			
				
				g2.setColor( fillColor );
				g2.fill( cRect );
				

				g2.translate( getCharBoxSize().width + 2,0 );
			}
		}
		
		public int getCharCount( int column, Character character ) {
			int count = 0;
			for ( int i = 0; i < alignment.getSequenceCount(); i++ ) {
				if ( alignment.characterAt( i, column ).equals( character ) )
					count++;
			}
			return count;
		}
		
		public double getConservationAt( int column ) {
			int max = 0;
			HashMap<Character,Integer> map = new HashMap<Character,Integer>();
			for ( int i = 0; i < alignment.getSequenceCount(); i++ ) {
				Character character = alignment.characterAt( i, column );
				if ( character.charValue() == '-')
					continue;
				Integer integer = map.get( character );
				if ( integer != null )
					integer += 1;
				else {
					integer = 1;				
				}
				if ( max < integer )
					max = integer;
				map.put( character, integer );
			}
			return (double)max / (double)alignment.getSequenceCount();
		}

		public void mouseClicked(MouseEvent arg0) {
			setActive( !isActive);
		}

		public void mousePressed(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		public void mouseReleased(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		public void mouseEntered(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		public void mouseExited(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		public void alignmentChanged(AlignmentChangedEvent e) {
			if ( isActive)
				setActive( false);
			needsRecalculate = true;
		}

		public void alignmentStructureChanged(AlignmentStructureChangedEvent e) {
			if ( isActive)
				setActive( false);
			needsRecalculate = true;
		}	
	}

	public void colorModelChanged(ColorChangedEvent e) {
		needsRedraw = true;
		repaint();
	}



	
}
