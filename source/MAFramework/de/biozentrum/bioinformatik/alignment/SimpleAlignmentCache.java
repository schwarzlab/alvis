/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package de.biozentrum.bioinformatik.alignment;

import de.biozentrum.bioinformatik.alignment.events.AlignmentChangedEvent;
import de.biozentrum.bioinformatik.alignment.events.AlignmentListener;
import de.biozentrum.bioinformatik.alignment.events.AlignmentStructureChangedEvent;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Roland Schwarz (rfs32@cam.ac.uk)
 */
public class SimpleAlignmentCache implements IAlignmentCachingStrategy, AlignmentListener, Serializable{
    private MultipleAlignment alignment;
    private transient char[][] columnCache;
	private transient Map<Character,Integer>[] symbolCountCache;
    private static final long serialVersionUID=1L;
	
    public SimpleAlignmentCache() {
    }

	private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();
		alignment.addAlignmentListener(this);
		rebuildCache();
    }
	
    @Override
    public char[] columnAt(int position) {
        return this.columnCache[position];
        //return this.cache[][position];
    }

	@Override
	public Map<Character, Integer> getSymbolCounts(int position) {
		return symbolCountCache[position];
	}
	
    @Override
    public void register(MultipleAlignment alignment) {
        this.alignment = alignment;
        this.alignment.addAlignmentListener(this);
        rebuildCache();
    }
    
    private void rebuildCache() {
        this.columnCache = new char[alignment.getLength()][alignment.getSequenceCount()];
        this.symbolCountCache = new HashMap[alignment.getLength()];
		
        for (int i=0;i<alignment.getSequenceCount();i++) {
            char[] row = alignment.getSequence(i).getAlignedSequence().toCharArray();
            for (int j=0;j<row.length;j++) {
				char c = row[j];
                this.columnCache[j][i]=c;
				if (symbolCountCache[j]==null) {
					symbolCountCache[j] = new HashMap<>();
				}
				if (symbolCountCache[j].containsKey(c)) {
					symbolCountCache[j].put(c, symbolCountCache[j].get(c) + 1);
				} else {
					symbolCountCache[j].put(c, 1);
				}
            }
        }
		
    }

    @Override
    public void alignmentChanged(AlignmentChangedEvent e) {
        rebuildCache();
    }

    @Override
    public void alignmentStructureChanged(AlignmentStructureChangedEvent e) {
        rebuildCache();
    }

    
}
