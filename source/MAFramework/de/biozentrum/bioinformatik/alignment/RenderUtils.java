package de.biozentrum.bioinformatik.alignment;

import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.font.FontRenderContext;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;

public class RenderUtils {
	
	public static int pixelSizeForFont( Font font ) {
		BufferedImage img = new BufferedImage( 10,10, BufferedImage.TYPE_INT_ARGB );
		FontRenderContext frc = ((Graphics2D)img.getGraphics()).getFontRenderContext();
		Rectangle2D r = font.getMaxCharBounds( frc );
		return (int) (Math.max( r.getWidth(), r.getHeight() ));
	}
	
}
