package de.biozentrum.bioinformatik.alignment;

import java.awt.Rectangle;

import de.biozentrum.bioinformatik.alignment.policy.AlignmentEditPolicy;

public class DefaultEditPolicy implements AlignmentEditPolicy {

	private MultipleAlignment alignment;
	
	public void setAlignment(MultipleAlignment alignment) {
		this.alignment = alignment;
	}

	public boolean canInsert(char c, Rectangle rect) {
		return ( c == '-' );
	}

	public boolean canDelete(Rectangle rect) {
		return true;
	}

}
