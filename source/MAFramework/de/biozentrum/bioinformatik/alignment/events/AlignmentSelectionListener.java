/*
 * Created on 20.05.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package de.biozentrum.bioinformatik.alignment.events;

import java.util.EventListener;


/**
 * @author pseibel
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface AlignmentSelectionListener extends EventListener {
	public void selectionChanged( AlignmentSelectionEvent e );
	public void selectionWillBegin();
	public void selectionDidEnd();
}
