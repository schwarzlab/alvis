package de.biozentrum.bioinformatik.alignment.events;

import java.util.EventObject;

public class MAEvent extends EventObject {

	public static enum MAEventType {
		INSERT,
		DELETE,
		UPDATE,
	}
	
	
	/**
	 * 
	 * @uml.property name="type" 
	 */
	protected MAEventType type;
	
	public MAEvent(Object source, MAEventType type) {
		super(source);
		this.type = type;
	}

	/**
	 * 
	 * @uml.property name="type"
	 */
	public MAEventType getType() {
		return type;
	}
}
