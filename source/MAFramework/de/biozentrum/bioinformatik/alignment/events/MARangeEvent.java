package de.biozentrum.bioinformatik.alignment.events;


public class MARangeEvent extends MAEvent {
	protected int firstPos;
	protected int lastPos;
	
	public MARangeEvent(Object source, int firstPos, int lastPos, MAEventType type ) {
		super(source, type);
		this.firstPos = firstPos;
		this.lastPos = lastPos;
		// TODO Auto-generated constructor stub
	}


	
	/**
	 * 
	 * @uml.property name="firstPos"
	 */
	public int getFirstPos() {
		return firstPos;
	}

	/**
	 * 
	 * @uml.property name="lastPos"
	 */
	public int getLastPos() {
		return lastPos;
	}

}
