/*
 * Created on 16.12.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package de.biozentrum.bioinformatik.alignment.events;

import java.util.EventListener;

/**
 * @author pseibel
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface AlignmentListener extends EventListener {
	public void alignmentChanged( AlignmentChangedEvent e );
	public void alignmentStructureChanged( AlignmentStructureChangedEvent e );
}
