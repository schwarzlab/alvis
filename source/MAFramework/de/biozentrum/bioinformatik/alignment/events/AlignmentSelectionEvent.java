/*
 * Created on 20.05.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package de.biozentrum.bioinformatik.alignment.events;

import java.awt.Rectangle;

import de.biozentrum.bioinformatik.sequence.selection.SequenceRange;

/**
 * @author pseibel
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class AlignmentSelectionEvent extends MAEvent {

	
	private SequenceRange[] ranges;
	private Rectangle rect;
	/**
	 * @param arg0
	 */
	public AlignmentSelectionEvent(Object source, SequenceRange[] ranges, Rectangle rect, MAEventType type ) {
		super(source,type);
		this.ranges = ranges;
		this.rect = rect;
	}
	
	public SequenceRange[] getRanges() {
		return ranges;
	}
	
	public Rectangle getRectangle() {
		return rect;
	}
}
