package de.biozentrum.bioinformatik.alignment.events;

public class AlignmentStructureChangedEvent extends MARangeEvent {
	
	public enum ModifiedStructure {
		COLUMNS,
		ROWS
	}
	
	private ModifiedStructure modified;
	
	public AlignmentStructureChangedEvent(Object source, int from, int to, ModifiedStructure modified, MAEventType type) {
		super(source, from, to, type);
		this.modified = modified;
	}

	public ModifiedStructure getModifiedStructure() {
		return modified;
	}
	
}
