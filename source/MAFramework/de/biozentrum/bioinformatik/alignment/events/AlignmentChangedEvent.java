/*
 * Created on 16.12.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package de.biozentrum.bioinformatik.alignment.events;

import java.awt.Rectangle;

import de.biozentrum.bioinformatik.alignment.MultipleAlignment;


/**
 * @author pseibel
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class AlignmentChangedEvent extends MAEvent {

	/**
	 * Comment for <code>serialVersionUID</code>
	 */
	private static final long serialVersionUID = 4049924857078560816L;

	private Rectangle rect;
	private boolean isAdjusting;
	
	public AlignmentChangedEvent( MultipleAlignment source,Rectangle rect, MAEventType type, boolean isAdjusting ) {
		super( source, type );
		this.rect = rect;
		this.isAdjusting = isAdjusting;
	}
	
	public boolean isAdjusting() {
		return isAdjusting;
	}
	
	public Rectangle getRectangle() {
		return rect;
	}
}
