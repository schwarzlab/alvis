/*
 * Created on 16.12.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package de.biozentrum.bioinformatik.alignment;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;

import de.biozentrum.bioinformatik.alignment.events.AlignmentChangedEvent;
import de.biozentrum.bioinformatik.alignment.events.AlignmentListener;
import de.biozentrum.bioinformatik.alignment.events.AlignmentStructureChangedEvent;
import de.biozentrum.bioinformatik.alignment.events.AlignmentStructureChangedEvent.ModifiedStructure;
import de.biozentrum.bioinformatik.alignment.events.MAEvent.MAEventType;
import de.biozentrum.bioinformatik.sequence.Sequence;
import de.biozentrum.bioinformatik.sequence.SequenceAlphabet;
import de.biozentrum.bioinformatik.sequence.events.SequenceChangedEvent;
import de.biozentrum.bioinformatik.sequence.events.SequenceListener;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.List;


/**
 * @author pseibelTODO To change the template for this generated type comment go toWindow - Preferences - Java - Code Style - Code Templates
 */

public class MultipleAlignment implements Iterable<Sequence>, SequenceListener, Serializable {
	private final static long serialVersionUID=1L;	
	protected transient ArrayList<AlignmentListener> listeners;
	
	protected List<Sequence> sequences;
	private SequenceAlphabet alphabet;
	
	public MultipleAlignment() {
		this.listeners = new ArrayList<>();
		this.sequences = new ArrayList<>();
	}
	
	public MultipleAlignment( List<Sequence> sequences ) {
		this.listeners = new ArrayList<>();
		this.sequences = sequences;
	}
	
	public void setAlphabet( SequenceAlphabet alphabet ) {
		this.alphabet = alphabet;
	}
	
	public void insertChar( char c, Rectangle rect ) {
		char[] str = new char[ (int) rect.getWidth() ];
		char[] gaps = new char[ (int) rect.getWidth() ];
		for ( int i = 0; i < str.length; i++ ) {
			str[i] = c;
			gaps[i] = '-';
		}
		
		
		
		for ( int seq = 0; seq < sequences.size(); seq++ ) {
			if ( seq >= (int)rect.getMinY() && seq < (int)rect.getMaxY() ) 
				sequences.get( seq ).insert( this, str, (int)rect.getMinX() );
			else 
				sequences.get( seq ).insert( this, gaps, sequences.get( seq ).length() );
		}
		if ( (int)rect.getMinY() > 0 ) {
			fireAlignmentModelEvent( new AlignmentChangedEvent( this, new Rectangle( sequences.get(0).length()-gaps.length-1 , 0, gaps.length , (int)rect.getMinY()+1), MAEventType.INSERT, true ));
		}
		if ( (int)rect.getMaxY() < sequences.size() ) {
			fireAlignmentModelEvent( new AlignmentChangedEvent( this, new Rectangle( sequences.get(0).length()-gaps.length-1 , (int)rect.getMaxY()+1, gaps.length , sequences.size()-(int)rect.getMaxY()), MAEventType.INSERT, true ));
		}
		fireAlignmentModelEvent( new AlignmentChangedEvent( this, rect, MAEventType.INSERT, false ));
	}
	
	public void insertChar( char c, int sequence, int position  ) {
		insertChar( c, new Rectangle( position, sequence, 0, 0 ) );
	}
	
	public void delete( Rectangle rect ) {
		deleteAdjusting( rect, false );
	}
		
	public void deleteAdjusting( Rectangle rect, boolean isAdjusting ) {
		if ( rect.getHeight() != sequences.size() ) {
			char[] gaps = new char[(int)rect.getWidth()];
			
			for ( int i = 0; i < gaps.length; i++ ) {
				gaps[i] = '-';
			}
			for ( int seq = (int)rect.getMinY(); seq < (int)rect.getMaxY(); seq++ ) {
				sequences.get( seq ).insert( this, gaps,sequences.get(seq).length());
			}
			fireAlignmentModelEvent( new AlignmentChangedEvent( this, new Rectangle( (int) ( this.length()-rect.getWidth()), (int) rect.getMinY(),(int) rect.getWidth(), (int) rect.getHeight() ), MAEventType.DELETE, true ));
		}
		
		for ( int seq = (int)rect.getMinY(); seq < (int)rect.getMaxY(); seq++ ) {
			sequences.get( seq ).delete( this, (int)rect.getMinX(), (int)rect.getMaxX() );
		}
		fireAlignmentModelEvent( new AlignmentChangedEvent( this, rect, MAEventType.DELETE, isAdjusting ));
	}
	
	public void pack() {
		int col = this.length()-1;
		while ( isGapOnlyColumn(col) && col > 0 ) {
			col--;
		}
		
		if ( col != this.length()-1 ) {
			this.deleteAdjusting( new Rectangle( col+1, 0, this.length()-(col+1), sequences.size() ), true );
		}
		
		col = 0;
		while ( isGapOnlyColumn(col) && col < length() ) {
			col++;
		}
		
		if ( col != 0 ) {
			this.deleteAdjusting( new Rectangle( col-1, 0, col-0, sequences.size() ), true );
		}
	}
	
	protected boolean isGapOnlyColumn( int column ) {
		char[] col = getColumn( column );
		boolean flag = true;
		for ( int i = 0 ; i < col.length; i++ ) {
			flag = ( col[i] == '-' );
			if ( !flag )
				break;
		}
		return flag;
	}
	
	protected boolean onlyGapsInColumn( int column, int seqFrom, int seqTo ) {
		boolean flag = true;
		for ( int i = seqFrom; i <= seqTo; i++ ) {
			flag = sequences.get( i ).charAt( column ) == '-';
			if ( !flag )
				break;
		}
		return flag;
	}
	
	protected boolean onlyGapsInColumnExclusive( int column, int seqFrom, int seqTo ) {
		return onlyGapsInColumn( column, 0, seqFrom-1 ) && onlyGapsInColumn( column, seqTo+1 , sequences.size()-1 );
	}
	
	public void deleteChar( int sequence, int position ) {
		delete( new Rectangle( position, sequence, 0, 0 ) );
	}
	
	public int rows() {
		return sequences.size();
	}
	
	public void addSequence( Sequence sequenceModel ) {
		sequences.add( sequenceModel );
		fireAlignmentModelEvent( new AlignmentStructureChangedEvent(this,sequences.size()-1,sequences.size()-1,ModifiedStructure.ROWS,MAEventType.INSERT));
	}
	
	public void addSequence( int pos, Sequence sequenceModel ) {
		sequences.add( pos, sequenceModel );
		fireAlignmentModelEvent( new AlignmentStructureChangedEvent(this,pos,pos,ModifiedStructure.ROWS,MAEventType.INSERT));
	}
	
	public void removeSequence( int seqPos) {
		sequences.remove( seqPos);
		AlignmentStructureChangedEvent e = new AlignmentStructureChangedEvent(this,seqPos,seqPos,ModifiedStructure.ROWS,MAEventType.DELETE);
		fireAlignmentModelEvent( e);
	}
	
	public Sequence getSequence( int position ) {
		if ( position >= 0 
				&& sequences.size() > position ) 
			return sequences.get(position);
		return null;
	}
	
	public int getSequenceIndex( Sequence sequence ) {
		return sequences.indexOf( sequence );
	}

	public char[] getColumn( int position ) {
		char[] column = new char[ sequences.size() ];
		
		int col = 0;
		
		for ( Sequence sequence : sequences ) {
			if ( sequence.length() > position )
				column[ col ] = sequence.charAt( position );
			col++;
		}
			
		return column;
	}
	
	public Map<Character, Integer> getSymbolCounts (int position) {
		return getSymbolCounts(position, null);
	}
	
	public Map<Character, Integer> getSymbolCounts( int position, int[] rows) 
	{
		Map<Character, Integer> symbolMap = new HashMap<>();
		
		char[] column = getColumn( position);
		int length;
		if (rows==null) {
			length = getSequenceCount();
		} else {
			length = rows.length;
		}
		for (int i=0;i<length;i++) {
			int rowIndex;
			if (rows==null) {
				rowIndex = i;
			} else {
				rowIndex = rows[i];
			}
			char c = column[rowIndex];
			if ( symbolMap.containsKey(c)) 
			{
				symbolMap.put(c, symbolMap.get(c) + 1);
			}
			else 
			{
				symbolMap.put(c, 1);
			}
		}
		
		return symbolMap;
	}
	
	public double getColumnConservation( int position) 
	{
		int max = 0;
		for ( Entry<Character, Integer> entry: getSymbolCounts(position).entrySet()) 
		{
			if ( entry.getKey() != '-' )
				max = Math.max(entry.getValue(), max);
		}
		return (double)max / (double)getSequenceCount();
	}
	
	public double getSymbolConservation( char symbol, int position) 
	{
		if ( !getSymbolCounts(position).containsKey(symbol))
			return 0.0;
		return (double)getSymbolCounts( position).get(symbol)/(double)getSequenceCount();
	}
	
	public int length() {
		if ( sequences.size() > 0 )
			return sequences.get( 0 ).length();
		else 
			return 0;
	}
	
	public void addAlignmentListener( AlignmentListener listener ) {
		listeners.add( listener );
	}
	
	public void removeAlignmentListener( AlignmentListener listener ) {
		listeners.remove( listener );
	}

	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.ca.ext.alignment.MultipleAlignment#characterAt(int, int)
	 */
	public Character characterAt(int sequence, int position) {
		return new Character(getColumn( position)[sequence]);
	}

	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.ca.ext.alignment.MultipleAlignment#getSequenceCount()
	 */
	public int getSequenceCount() {
		return rows();
	}

	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.ca.ext.alignment.MultipleAlignment#getLength()
	 */
	public int getLength() {
		return length();
	}

	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.ca.ext.alignment.MultipleAlignment#getAlphabet()
	 */
	public SequenceAlphabet getAlphabet() {
		return alphabet;
	}
	
	public void fireAlignmentModelEvent( AlignmentStructureChangedEvent e ) {
		for ( AlignmentListener listener : listeners ) {
			listener.alignmentStructureChanged( e );
		}
	}
	
	public void fireAlignmentModelEvent( AlignmentChangedEvent e ) {
		for ( AlignmentListener listener : listeners ) {
			listener.alignmentChanged( e );
		}
	}
	
	protected void fireAlignmentModelEvent( SequenceChangedEvent e ) {
		int seq = sequences.indexOf( e.getSource() );
		fireAlignmentModelEvent( new AlignmentChangedEvent( this, new Rectangle( e.getFirstPos(), seq, e.getLastPos()-e.getFirstPos(), seq),e.getType(), false ) );
	}

	public void sequenceChanged(SequenceChangedEvent e) {
		if ( !e.getSource().equals( this ) ) {
			fireAlignmentModelEvent( e );
		}
	}

	public List<Sequence> getSequences() {
		return sequences;
	}
	
	public Iterator<Sequence> iterator() {
		return sequences.iterator();
	}
	
	public Matcher[] match( String regex ) {
		Matcher[] matcher = new Matcher[getSequenceCount()];
		for ( int i = 0; i < matcher.length; i++ ) {
			matcher[i] = sequences.get( i ).match( regex, true );
		}
		return matcher;
	}
	
	public String toString() {
		StringBuffer s = new StringBuffer();
		for ( Sequence sequence : this ) {
			s.append( sequence.getAlignedSequence() + "\n");
		}
		return s.toString();
	}
	
	public void moveSequence( int from, int to) {
		Sequence seq = getSequence( from);
		sequences.remove( from);
		sequences.add( to, seq);
		AlignmentStructureChangedEvent e = new AlignmentStructureChangedEvent(this,Math.min(from,to),Math.max(from,to),ModifiedStructure.ROWS,MAEventType.UPDATE);
		fireAlignmentModelEvent( e);
	}
	
	private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        this.listeners = new ArrayList<AlignmentListener>();
    }
	
}
