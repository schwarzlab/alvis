/*
 * Created on 17.12.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package de.biozentrum.bioinformatik.alignment.io;

import de.biozentrum.bioinformatik.MAFactory;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import de.biozentrum.bioinformatik.alignment.MultipleAlignment;
import de.biozentrum.bioinformatik.sequence.Sequence;

/**
 * @author pseibel
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ClustalReader extends FileReader {

	/**
	 * @param arg0
	 * @throws FileNotFoundException
	 */
	public ClustalReader(String filename) throws FileNotFoundException {
		super(filename);
	}
	
	public MultipleAlignment readAlignment() throws IOException {
		MultipleAlignment model = MAFactory.createMultipleAlignment();
		
		BufferedReader br = new BufferedReader( this );
		ArrayList<StringBuffer> sequences = new ArrayList<StringBuffer>();
		ArrayList names = new ArrayList();
		
		int sequenceNum = 0;
		boolean isFirstBlock = true;
		
		String line = br.readLine();
		
		if ( !line.startsWith( "CLUSTAL W" ) )
			throw new IOException();
		
		br.readLine();
		br.readLine();
		line = br.readLine();
		
		while ( line != null ) {
			if ( line.startsWith(" ") ) {	
				br.readLine();			
			}
			if ( line.startsWith(" ") ) {
				sequenceNum = 0;
				line = br.readLine();
				isFirstBlock = false;
				continue;
			}
			
			
			if ( isFirstBlock ) {
				sequences.add( new StringBuffer( getSequence( line ) ) );
				names.add( getName( line ) );
			} else {
				StringBuffer tmp = (StringBuffer)sequences.get( sequenceNum );
				tmp.append( getSequence( line ) );
			}
			sequenceNum++;
			line = br.readLine();
		}
		
		for ( int i = 0; i < sequences.size(); i++ ) {
			model.addSequence( new Sequence( ( String ) names.get( i ), sequences.get( i ) ) );
		}
		return model;
	}
	
	protected String getSequence( String line ) {
		String[] splits = line.split( " " );
		return splits[ splits.length-1 ].toUpperCase();
	}
	
	protected String getName( String line ) {
		String[] splits = line.split( " " );
		return splits[ 0 ];
	}

}
