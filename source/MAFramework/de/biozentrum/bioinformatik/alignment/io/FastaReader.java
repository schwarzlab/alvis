package de.biozentrum.bioinformatik.alignment.io;

import de.biozentrum.bioinformatik.MAFactory;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import de.biozentrum.bioinformatik.alignment.MultipleAlignment;
import de.biozentrum.bioinformatik.sequence.Sequence;

public class FastaReader extends FileReader {
	public FastaReader(String filename) throws FileNotFoundException {
		super(filename);
	}
	
	public MultipleAlignment readAlignment() throws IOException {
		MultipleAlignment model = MAFactory.createMultipleAlignment();
		
		BufferedReader br = new BufferedReader( this );
		ArrayList sequences = new ArrayList();
		ArrayList names = new ArrayList();
		
		int sequenceNum = 0;
		boolean isFirstBlock = true;
				
		String line = br.readLine();
		StringBuffer sequence = new StringBuffer();
		String name = null;
		while ( line != null ) {
			if ( line.startsWith(">") ) {
				if ( sequence.length() != 0 && name != null ) {
					String seqString = sequence.toString();
					seqString = seqString.replaceAll( " ", "");
					seqString = seqString.replaceAll( "\\n", "");
					seqString = seqString.replaceAll( "\\r", "");
					model.addSequence(new Sequence( name, seqString ) );
					sequence = null;
					
				}
				name = line.substring(1,line.length());
				sequence = new StringBuffer();
			}
			else {
				sequence.append( line );
			}
			line = br.readLine();
		}
		if ( sequence.length() != 0 && name != null ) {
			String seqString = sequence.toString();
			seqString = seqString.replaceAll( " ", "");
			seqString = seqString.replaceAll( "\\n", "");
			seqString = seqString.replaceAll( "\\r", "");
			model.addSequence(new Sequence( name, seqString ) );
			sequence = null;
			name = null;
		}
		
		
		return model;
	}
}
