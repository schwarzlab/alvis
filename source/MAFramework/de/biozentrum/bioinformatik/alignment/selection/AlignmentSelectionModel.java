package de.biozentrum.bioinformatik.alignment.selection;

import java.awt.Rectangle;

import de.biozentrum.bioinformatik.alignment.MultipleAlignment;
import de.biozentrum.bioinformatik.alignment.events.AlignmentSelectionListener;

public interface AlignmentSelectionModel {
	public void addAlignmentSelectionListener( AlignmentSelectionListener listener );
	public void removeAlignmentSelectionListener( AlignmentSelectionListener listener );
	
	public MultipleAlignment getAlignment();
	
	
	public void begin();
	public void end();
	
	public boolean isSelected( Rectangle rect, boolean ignoreGaps );
	public boolean isDeselected( Rectangle rect, boolean ignoreGaps );
	public void select(  Rectangle rect, boolean ignoreGaps );
	public void deselect(  Rectangle rect );
	public void deselectAll();
	
	public void select( int sequence, int column, boolean ignoreGaps );
	public void deselect( int sequence, int column );
	public boolean isSelected( int sequence, int column, boolean ignoreGaps );
	public boolean isDeselected( int sequence, int column, boolean ignoreGaps );
	public boolean isAnySiteSelected(int sequence, boolean ignoreGaps);
}
