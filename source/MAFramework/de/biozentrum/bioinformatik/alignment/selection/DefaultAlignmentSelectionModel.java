/*
 * Created on 18.05.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package de.biozentrum.bioinformatik.alignment.selection;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Iterator;

import de.biozentrum.bioinformatik.alignment.MultipleAlignment;
import de.biozentrum.bioinformatik.alignment.events.AlignmentSelectionEvent;
import de.biozentrum.bioinformatik.alignment.events.AlignmentSelectionListener;
import de.biozentrum.bioinformatik.alignment.events.MAEvent.MAEventType;
import de.biozentrum.bioinformatik.sequence.Sequence;
import de.biozentrum.bioinformatik.sequence.selection.SequenceSelectionEvent;
import de.biozentrum.bioinformatik.sequence.selection.SequenceSelectionListener;
import de.biozentrum.bioinformatik.sequence.selection.SequenceSelectionModel;
import java.util.HashMap;
import java.util.Map;

/**
 * @author pseibel
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class DefaultAlignmentSelectionModel implements AlignmentSelectionModel, SequenceSelectionListener {
	
	protected MultipleAlignment alignment;
	protected ArrayList<AlignmentSelectionListener> listeners;
	protected Map<SequenceSelectionModel, Integer> indexCache = new HashMap<>();
	
	public DefaultAlignmentSelectionModel( MultipleAlignment alignment ) {
		this.alignment = alignment;
		listeners = new ArrayList<>();
		for (int i=0;i<alignment.getSequenceCount();i++) {
			Sequence sequence = alignment.getSequence(i);
			sequence.getSelectionModel().addSequenceSelectionListener(this);
			indexCache.put(sequence.getSelectionModel(), i);
		}
	}

	public MultipleAlignment getAlignment() {
		return alignment;
	}
	
	public void addAlignmentSelectionListener( AlignmentSelectionListener listener ) {
		listeners.add( listener );
	}
	
	public void removeAlignmentSelectionListener( AlignmentSelectionListener listener ) {
		listeners.remove( listener );
	}
	
	
	public void begin() {
		Iterator it = listeners.iterator();
		while ( it.hasNext() ) {
			((AlignmentSelectionListener)it.next()).selectionWillBegin();
		}
	}
	
	public void end() {
		Iterator it = listeners.iterator();
		while ( it.hasNext() ) {
			((AlignmentSelectionListener)it.next()).selectionDidEnd();
		}
	}
	
	protected void fireSelectionChanged( AlignmentSelectionEvent e ) {
		Iterator it = listeners.iterator();
		while ( it.hasNext() ) {
			((AlignmentSelectionListener)it.next()).selectionChanged( e );
		}
	}
	
	public void select( Rectangle rect, boolean ignoreGaps ) {
		if ( isSelected( rect, ignoreGaps ) )
			return;
		
		for ( int seq = (int) rect.getMinY(); seq < (int) rect.getMaxY(); seq++ ) {	
			alignment.getSequence( seq ).getSelectionModel().select( (int)rect.getMinX(), (int)rect.getMaxX()-1, ignoreGaps );
		}
		
		AlignmentSelectionEvent e = new AlignmentSelectionEvent( this, null, rect, MAEventType.INSERT);
		fireSelectionChanged( e );
	}
	
	public void deselect(  Rectangle rect ) {
		if ( isDeselected( rect, false ) ) {
			return;
		}
		
		for ( int seq = (int) rect.getMinY(); seq < (int) rect.getMaxY(); seq++ ) {	
			alignment.getSequence( seq ).getSelectionModel().deselect( (int)rect.getMinX(), (int)rect.getMaxX()-1 );
		}
		
		AlignmentSelectionEvent e = new AlignmentSelectionEvent( this, null,rect,MAEventType.DELETE);
		fireSelectionChanged( e );
	}
	
	public void deselectAll() {
		if (alignment.getLength()==0 || alignment.getSequenceCount()==0 || isDeselected( new Rectangle(0,0,alignment.getLength(),alignment.getSequenceCount()), false )) {
			return;
		}
		
		for ( Sequence sequence : alignment ) {
			sequence.getSelectionModel().deselectAll( this );
		}
		AlignmentSelectionEvent e = new AlignmentSelectionEvent( this, null, new Rectangle(0,0,alignment.length(), alignment.getSequenceCount()),MAEventType.DELETE);
		fireSelectionChanged( e );
	}
	
	public boolean isSelected( Rectangle rect, boolean ignoreGaps )  {
		boolean tmp = true;
		for ( int seq = (int) rect.getMinY(); seq < (int) rect.getMaxY(); seq++ ) {	
			if( !alignment.getSequence( seq ).getSelectionModel().isSelected( (int)rect.getMinX(), (int)rect.getMaxX()-1, ignoreGaps ) ) {
				tmp = false;
				break;
			}
		}
		return tmp;
	}
	
	public boolean isDeselected( Rectangle rect, boolean ignoreGaps )  {
		boolean tmp = true;
		for ( int seq = (int) rect.getMinY(); seq < (int) rect.getMaxY(); seq++ ) {	
			if( !alignment.getSequence( seq ).getSelectionModel().isDeselected( (int)rect.getMinX(), (int)rect.getMaxX()-1, ignoreGaps ) ) {
				tmp = false;
				break;
			}
		}
		return tmp;
	}

	public void select(int sequence, int column, boolean ignoreGaps) {
		this.select( new Rectangle(column,sequence,1,1), ignoreGaps );
	}

	public void deselect(int sequence, int column) {
		this.deselect( new Rectangle(column,sequence,1,1));	
	}
	
	public boolean isSelected( int sequence, int column, boolean ignoreGaps )  {
		return this.isSelected( new Rectangle(column,sequence,1,1), ignoreGaps);	
	}
	
	public boolean isAnySiteSelected(int sequence, boolean ignoreGaps) {
		return alignment.getSequence(sequence).getSelectionModel().isAnySiteSelected(ignoreGaps);
	}

	public boolean isDeselected( int sequence, int column, boolean ignoreGaps )  {
		return this.isDeselected( new Rectangle(column,sequence,1,1), ignoreGaps);	
	}
	
	public void selectionChanged(SequenceSelectionEvent e) {
		if ( e.getSource().equals( this ))
			return;
		
//		int seq = -1;
//		for ( int i = 0; i < alignment.getSequenceCount(); i++) {
//			if ( alignment.getSequence( i).getSelectionModel().equals( e.getSource())) {
//				seq = i;
//				break;
//			}
//		}
		int seq = indexCache.get((SequenceSelectionModel)e.getSource());
		
		AlignmentSelectionEvent ev = new AlignmentSelectionEvent( this, null, new Rectangle( e.getRange().getStart(), seq, e.getRange().getEnd() - e.getRange().getStart() + 1, 1), e.getType());
		fireSelectionChanged( ev );
	}
}
