package de.biozentrum.bioinformatik.alignment.visibility;

import com.general.utils.GUIutils;
import com.general.utils.GeneralUtils;
import de.biozentrum.bioinformatik.alignment.visibility.AlignmentVisibilityChangedEvent.Visibility;
import java.awt.Rectangle;
import javax.swing.DefaultListSelectionModel;
import javax.swing.ListSelectionModel;
import javax.swing.event.EventListenerList;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class SequenceVisibilityModel implements AlignmentVisibilityModel, ListSelectionListener {

	EventListenerList listenerList = new EventListenerList();
	private DefaultListSelectionModel delegate;
	
	public SequenceVisibilityModel() {
		delegate = new DefaultListSelectionModel();
		delegate.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		delegate.addListSelectionListener(this);
	}

	@Override
	public void addVisibilityListener(AlignmentVisibilityListener listener) {
		listenerList.add(AlignmentVisibilityListener.class, listener);
	}
	
	@Override
	public void removeVisibilityListener(AlignmentVisibilityListener listener) {
		listenerList.remove(AlignmentVisibilityListener.class, listener);
	}
	
	protected void fireVisibilityChangedEvent(AlignmentVisibilityChangedEvent e) {
		AlignmentVisibilityListener[] listeners = listenerList.getListeners(AlignmentVisibilityListener.class);
		for (AlignmentVisibilityListener l : listeners) {
			l.visibilityChanged(e);
		}		
	}
	
	private void setVisible(int from, int to, boolean b) {
		if (b) { // inverse logic, careful
			delegate.removeSelectionInterval(from, to);
		} else {
			delegate.addSelectionInterval(from, to);
		}
	}
	
	public void showAll() {
		delegate.clearSelection();
	}
	
	public void showSequences(int from, int to) {
		setVisible(from, to, true);
	}
	
	public void hideSequences(int from, int to) {
		setVisible(from, to, false);
	}
	
	@Override
	public boolean isVisible(int sequence, int column) {
		return !delegate.isSelectedIndex(sequence);
	}
	
	public int[] getInvisibleIndices() {
		int[] selected = GUIutils.getSelectedIndices(delegate);
		return selected;
	}
	
	@Override
	public boolean getValueIsAdjusting() {
		return delegate.getValueIsAdjusting();
	}

	@Override
    public void setValueIsAdjusting(boolean isAdjusting) {
		delegate.setValueIsAdjusting(isAdjusting);
	}

	@Override
	public void valueChanged(ListSelectionEvent e) {
		// translate selection event into visibility event
		int from = e.getFirstIndex();
		int to = e.getLastIndex();
		Visibility v;
		if (delegate.isSelectedIndex(from)) {
			v = Visibility.HIDDEN;
		} else {
			v = Visibility.VISIBLE;
		}
		Rectangle rect = new Rectangle(0,Math.min(from,to),-1,Math.abs(to-from));
		AlignmentVisibilityChangedEvent event = new AlignmentVisibilityChangedEvent(this, rect, v, e.getValueIsAdjusting());
		fireVisibilityChangedEvent(event);
	}
	
}
