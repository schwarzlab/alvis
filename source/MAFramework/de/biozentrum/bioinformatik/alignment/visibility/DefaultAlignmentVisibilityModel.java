/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package de.biozentrum.bioinformatik.alignment.visibility;

/**
 *
 * @author Roland Schwarz <rfs32@cam.ac.uk>
 */
public class DefaultAlignmentVisibilityModel implements AlignmentVisibilityModel{
	@Override
	public void addVisibilityListener(AlignmentVisibilityListener listener) {
	}

	@Override
	public void removeVisibilityListener(AlignmentVisibilityListener listener) {
	}

	@Override
	public boolean isVisible(int sequence, int column) {
		return true;
	}

	@Override
	public boolean getValueIsAdjusting() {
		return false;
	}

	@Override
	public void setValueIsAdjusting(boolean value) {
	}

}
