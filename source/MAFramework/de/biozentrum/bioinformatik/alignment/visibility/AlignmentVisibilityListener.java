package de.biozentrum.bioinformatik.alignment.visibility;

import java.util.EventListener;

public interface AlignmentVisibilityListener extends EventListener{
	public void visibilityChanged( AlignmentVisibilityChangedEvent e );
}
