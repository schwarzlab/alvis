package de.biozentrum.bioinformatik.alignment.visibility;

public interface AlignmentVisibilityModel {
	public void addVisibilityListener( AlignmentVisibilityListener listener );
	public void removeVisibilityListener( AlignmentVisibilityListener listener );
	
	public boolean isVisible( int sequence, int column );
	
	public boolean getValueIsAdjusting();
	public void setValueIsAdjusting(boolean value);
}
