package de.biozentrum.bioinformatik.alignment.visibility;

import java.awt.Rectangle;
import java.util.EventObject;

public class AlignmentVisibilityChangedEvent extends EventObject {
	
	public enum Visibility {
		VISIBLE,
		HIDDEN
	}
	
	private final Visibility visibility;
	private final Rectangle rect;
	private final boolean isAdjusting;
	
	public AlignmentVisibilityChangedEvent(Object source, Rectangle rect, Visibility v, boolean isAdjusting ) {
		super(source);
		this.rect = rect;
		this.visibility = v;
		this.isAdjusting=isAdjusting;
	}
	
	public Visibility getVisibility() {
		return visibility;
	}
	
	public Rectangle getRectangle() {
		return rect;
	}

	public boolean getValueIsAdjusting(){
		return isAdjusting;
	}
}
