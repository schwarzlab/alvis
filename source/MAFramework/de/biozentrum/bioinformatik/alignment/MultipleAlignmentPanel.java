package de.biozentrum.bioinformatik.alignment;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.MouseWheelListener;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

import de.biozentrum.bioinformatik.alignment.events.AlignmentChangedEvent;
import de.biozentrum.bioinformatik.alignment.events.AlignmentListener;
import de.biozentrum.bioinformatik.alignment.events.AlignmentStructureChangedEvent;
import de.biozentrum.bioinformatik.alignment.visibility.SequenceVisibilityModel;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseWheelEvent;
import javax.swing.BorderFactory;
import javax.swing.border.Border;

public class MultipleAlignmentPanel extends JPanel {

	private final NameModel nameModel;
	private JScrollPane alignmentScrollPane;
	private final SequenceNameList nameList;
	private MultipleAlignmentView alignmentView;
	SequenceVisibilityModel visibilityModel;
	private final JSplitPane splitPane;
	private JPanel spacerPanel;

	public MultipleAlignmentPanel() {
		nameModel = new NameModel();
		nameList = new SequenceNameList(nameModel);

		JScrollPane nameScrollPane = new JScrollPane(nameList);
		nameScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		nameScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
		Border b = BorderFactory.createMatteBorder(1, 0, 1, 0, Color.GRAY);
		nameScrollPane.setBorder(b);
		alignmentView = new MultipleAlignmentView();
		nameList.setSelectionModel(alignmentView.getSequenceSelectionModel());

		alignmentScrollPane = new JScrollPane(alignmentView);
		alignmentScrollPane.getVerticalScrollBar().setModel(nameScrollPane.getVerticalScrollBar().getModel());
		alignmentScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		alignmentScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		nameScrollPane.getVerticalScrollBar().setUnitIncrement(alignmentView.getCharBoxSize().height + 3);
		alignmentScrollPane.getVerticalScrollBar().setUnitIncrement(alignmentView.getCharBoxSize().height + 3);
		alignmentScrollPane.getVerticalScrollBar().setBlockIncrement(1);
		nameScrollPane.getVerticalScrollBar().setBlockIncrement(1);
		alignmentScrollPane.setBorder(b);
		nameScrollPane.setWheelScrollingEnabled(false);
		nameScrollPane.addMouseWheelListener(new MouseWheelListener() {
			@Override
			public void mouseWheelMoved(MouseWheelEvent e) {
				MouseWheelListener[] listeners = alignmentScrollPane.getMouseWheelListeners();
				e.setSource(alignmentScrollPane);
				for (MouseWheelListener l : listeners) {
					l.mouseWheelMoved(e);
				}
			}
		});

		JScrollPane headerPane = new JScrollPane(alignmentView.getLogoView());
		headerPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		headerPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
		headerPane.getHorizontalScrollBar().setModel(alignmentScrollPane.getHorizontalScrollBar().getModel());
		headerPane.setBorder(null);

		int scrollBarWidth = (int) alignmentScrollPane.getVerticalScrollBar().getPreferredSize().getWidth();
		JPanel scrollSpacer = new JPanel();
		scrollSpacer.setBackground(Color.WHITE);
		scrollSpacer.setPreferredSize(new Dimension(scrollBarWidth, 10));
		JPanel headerView = new JPanel();
		headerView.setLayout(new BorderLayout());
		headerView.add(headerPane, BorderLayout.CENTER);
		headerView.add(scrollSpacer, BorderLayout.EAST);

		JPanel rightPanel = new JPanel();
		rightPanel.setLayout(new BorderLayout());
		rightPanel.add(headerView, BorderLayout.NORTH);
		rightPanel.add(alignmentScrollPane, BorderLayout.CENTER);

		final JPanel leftPanel = new JPanel();
		leftPanel.setLayout(new BorderLayout());
		leftPanel.add(nameScrollPane, BorderLayout.CENTER);

		spacerPanel = new JPanel(new BorderLayout());
		spacerPanel.setPreferredSize(new Dimension(0, alignmentView.getLogoView().getPreferredSize().height));
		alignmentView.getLogoView().addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent e) {
				spacerPanel.setPreferredSize(alignmentView.getLogoView().getPreferredSize());
				spacerPanel.setSize(alignmentView.getLogoView().getSize());
				leftPanel.validate(); // strange graphics bug otherwise
			}
		});
		spacerPanel.setBackground(Color.WHITE);

		leftPanel.add(spacerPanel, BorderLayout.NORTH);

		splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		splitPane.add(leftPanel, SwingConstants.LEFT);
		splitPane.add(rightPanel, SwingConstants.RIGHT);
		splitPane.setDividerLocation(200);
		splitPane.setResizeWeight(0.0);

		setLayout(new BorderLayout());
		add(splitPane, BorderLayout.CENTER);

		setFont(new Font("d", Font.BOLD, 12));
	}

	public void setFont(Font font) {
		super.setFont(font);
		if (alignmentView != null) {
			alignmentView.setFont(font);
		}
		if (nameList != null) {
			nameList.setFont(font);
		}
	}

	public JList getNameList() {
		return nameList;
	}

	public void addMouseWheelListener(MouseWheelListener listener) {
		super.addMouseWheelListener(listener);
		getAlignmentScrollPane().getViewport().addMouseWheelListener(listener);
	}

	public void removeMouseWheelListener(MouseWheelListener listener) {
		super.removeMouseWheelListener(listener);
		getAlignmentScrollPane().getViewport().removeMouseWheelListener(listener);
	}

	public MultipleAlignmentView getAlignmentView() {
		return alignmentView;
	}

	public void setAlignment(MultipleAlignment alignment) {
		((NameModel) nameList.getModel()).setModel(alignment);
		alignmentView.setAlignment(alignment);
		visibilityModel = new SequenceVisibilityModel();
		alignmentView.setVisibilityModel(visibilityModel);
	}

	public void showSelectedSequences() {
		ListSelectionModel selectionModel = alignmentView.getSequenceSelectionModel();
		visibilityModel.setValueIsAdjusting(true);
		for (int i = 0; i < alignmentView.getAlignment().getSequenceCount(); i++) {
			if (selectionModel.isSelectedIndex(i)) {
				visibilityModel.showSequences(i, i);
			}
		}
		visibilityModel.setValueIsAdjusting(false);
	}

	public void hideSelectedSequences() {
		ListSelectionModel selectionModel = alignmentView.getSequenceSelectionModel();
		visibilityModel.setValueIsAdjusting(true);
		for (int i = 0; i < alignmentView.getAlignment().getSequenceCount(); i++) {
			if (selectionModel.isSelectedIndex(i)) {
				visibilityModel.hideSequences(i, i);
			}
		}
		visibilityModel.setValueIsAdjusting(false);
	}

	/**
	 * @return the splitPane
	 */
	public JSplitPane getSplitPane() {
		return splitPane;
	}

	/**
	 * @return the alignmentScrollPane
	 */
	public JScrollPane getAlignmentScrollPane() {
		return alignmentScrollPane;
	}

	/**
	 * @return the spacerPanel
	 */
	protected JPanel getSpacerPanel() {
		return spacerPanel;
	}

	/**
	 * @return the visibilityModel
	 */
	public SequenceVisibilityModel getVisibilityModel() {
		return visibilityModel;
	}

	private class NameModel extends DefaultListModel implements AlignmentListener {

		private MultipleAlignment model;

		public void setModel(MultipleAlignment model) {
			this.model = model;
			model.addAlignmentListener(this);
			fireContentsChanged(this, 0, model.getLength());
		}

		public int getSize() {
			if (model == null) {
				return 0;
			}
			return model.rows();
		}

		public Object getElementAt(int index) {
			return model.getSequence(index).getName();
		}

		public void alignmentChanged(AlignmentChangedEvent e) {
			// TODO Auto-generated method stub

		}

		public void alignmentStructureChanged(AlignmentStructureChangedEvent e) {
			fireContentsChanged(this, 0, model.getLength());
		}

	}

	private class SequenceNameList extends JList {

		public SequenceNameList(ListModel model) {
			super(model);
		}

		public void setFont(Font font) {
			super.setFont(font);
			this.setFixedCellHeight(RenderUtils.pixelSizeForFont(font) + 5);
		}
	}
}
