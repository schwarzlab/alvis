package de.biozentrum.bioinformatik.sequence;

public interface IGappedSequenceAlphabet<S extends ISymbol> extends ISequenceAlphabet<S> {
	public S getGapSymbol();
}
