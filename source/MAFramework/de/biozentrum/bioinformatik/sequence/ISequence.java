package de.biozentrum.bioinformatik.sequence;

public interface ISequence<S extends ISymbol, A extends ISequenceAlphabet<S>> {
	
	public A getAlphabet();
	
	public void deleteSymbolAt( int position );
	public void delete( int from, int to );
	
	public void insertCharAt( S symbol, int position );
	public void insert( S[] str, int position );
	
	public ISequence<S,A> subSequence( int start, int end );	
	
	public S symbolAt( int position );
	
	public S[] getSymbols();
	
	public int length();
}
