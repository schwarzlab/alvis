/*
 * Created on 17.04.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package de.biozentrum.bioinformatik.sequence;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Objects;



/**
 * @author pseibel
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class SequenceAlphabet implements Collection<Character>, Serializable {
	private final static long serialVersionUID=1L;
	public static SequenceAlphabet NUCLEOTIDES;
	public static SequenceAlphabet AMINOACIDS;
	public static SequenceAlphabet RNA;
	public static SequenceAlphabet RNA_STRUCTURE;
	
	private ArrayList<Character> list;
	
	static {
		NUCLEOTIDES = new SequenceAlphabet();
		NUCLEOTIDES.addCharacter( new Character( 'A' ) );
		NUCLEOTIDES.addCharacter( new Character( 'T' ) );
		NUCLEOTIDES.addCharacter( new Character( 'G' ) );
		NUCLEOTIDES.addCharacter( new Character( 'C' ) );
		NUCLEOTIDES.addCharacter('M');//          A or C
		NUCLEOTIDES.addCharacter('R');//         A or G
		NUCLEOTIDES.addCharacter('W');//          A or T
		NUCLEOTIDES.addCharacter('S');//          C or G
		NUCLEOTIDES.addCharacter('Y');//          C or T
		NUCLEOTIDES.addCharacter('K');//          G or T
		NUCLEOTIDES.addCharacter('V');//       A or C or G
		NUCLEOTIDES.addCharacter('H');//        A or C or T
		NUCLEOTIDES.addCharacter('D');//        A or G or T
		NUCLEOTIDES.addCharacter('B');//        C or G or T
		NUCLEOTIDES.addCharacter('X');//      G or A or T or C
		NUCLEOTIDES.addCharacter('N');//      G or A or T or C		
		
		RNA = new SequenceAlphabet();
		RNA.addCharacter( new Character( 'A' ) );
		RNA.addCharacter( new Character( 'U' ) );
		RNA.addCharacter( new Character( 'G' ) );
		RNA.addCharacter( new Character( 'C' ) );
		
		RNA_STRUCTURE = new SequenceAlphabet();
		RNA_STRUCTURE.addCharacter( new Character( '.' ) );
		RNA_STRUCTURE.addCharacter( new Character( '(' ) );
		RNA_STRUCTURE.addCharacter( new Character( ')' ) );

		
		AMINOACIDS = new SequenceAlphabet();
		AMINOACIDS.addCharacter( new Character( 'A' ) );
		AMINOACIDS.addCharacter( new Character( 'C' ) );
		AMINOACIDS.addCharacter( new Character( 'D' ) );
		AMINOACIDS.addCharacter( new Character( 'E' ) );
		AMINOACIDS.addCharacter( new Character( 'F' ) );
		AMINOACIDS.addCharacter( new Character( 'G' ) );
		AMINOACIDS.addCharacter( new Character( 'H' ) );
		AMINOACIDS.addCharacter( new Character( 'I' ) );
		AMINOACIDS.addCharacter( new Character( 'K' ) );
		AMINOACIDS.addCharacter( new Character( 'L' ) );
		AMINOACIDS.addCharacter( new Character( 'M' ) );
		AMINOACIDS.addCharacter( new Character( 'N' ) );
		AMINOACIDS.addCharacter( new Character( 'P' ) );
		AMINOACIDS.addCharacter( new Character( 'Q' ) );
		AMINOACIDS.addCharacter( new Character( 'R' ) );
		AMINOACIDS.addCharacter( new Character( 'S' ) );
		AMINOACIDS.addCharacter( new Character( 'T' ) );
		AMINOACIDS.addCharacter( new Character( 'U' ) );
		AMINOACIDS.addCharacter( new Character( 'V' ) );
		AMINOACIDS.addCharacter( new Character( 'W' ) );
		AMINOACIDS.addCharacter( new Character( 'Y' ) );
		
		AMINOACIDS.addCharacter( new Character( 'X' ) );
	}
	
	public SequenceAlphabet() {
		list = createImpl();
	}

        private ArrayList<Character> createImpl() {
            return new ArrayList<Character>();
        }
        
        public Collection<Character> getImpl() {
            return this.list;
        }
        
	public void addCharacter( Character o ) {
		if ( !list.contains( o ) ) {
			add( o );
		}
	}
	
	public int indexOf( Character o ) {
		return list.indexOf( o );
	}
	
	public Character characterAt( int index ) {
		return list.get( index );
	}
	
        @Override
        public boolean equals(Object other) {
            boolean retval;
            if (!(other instanceof SequenceAlphabet)) {
                retval = super.equals(other);
            } else {
                SequenceAlphabet otherAlpha = (SequenceAlphabet) other;
                retval = getImpl().equals(otherAlpha.getImpl());
            }
            return retval;
        }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.list);
        return hash;
    }

    // implement collection interface
        @Override
	public int size() {
		return list.size();
	}

        @Override
    public boolean isEmpty() {
        return list.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return list.contains(o);
    }

    @Override
    public Iterator<Character> iterator() {
        return list.iterator();
    }

    @Override
    public Object[] toArray() {
        return list.toArray();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return list.toArray(a);
    }

    @Override
    public boolean add(Character e) {
        return list.add(e);
    }

    @Override
    public boolean remove(Object o) {
        return list.remove(o);
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return list.containsAll(c);
    }

    @Override
    public boolean addAll(Collection<? extends Character> c) {
        return list.addAll(c);
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return list.removeAll(c);
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return list.retainAll(c);
    }

    @Override
    public void clear() {
        list.clear();
    }
}
