package de.biozentrum.bioinformatik.sequence;

import java.util.Set;

public interface ISequenceAlphabet<S extends ISymbol> {
	public Set<S> getSymbols();
	public boolean isAllowedSymbol( S symbol );
}
