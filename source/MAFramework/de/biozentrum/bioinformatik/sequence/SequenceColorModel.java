/*
 * Created on 21.11.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package de.biozentrum.bioinformatik.sequence;

import java.awt.Color;
import java.util.Properties;
import java.util.Map.Entry;

import de.biozentrum.bioinformatik.color.ColorChangedEvent;
import de.biozentrum.bioinformatik.color.ColorModel;




/**
 * @author pseibel
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class SequenceColorModel extends ColorModel<Character> {
	
	public static final SequenceColorModel NUCLEOTIDE_MODEL = new SequenceColorModel();
	public static final SequenceColorModel AMINOACID_MODEL = new SequenceColorModel();
	public static final SequenceColorModel RNA_STRUCTURE_MODEL = new SequenceColorModel();
	
	
	static {
		NUCLEOTIDE_MODEL.setColorForChar( Color.red, 'A' );
		NUCLEOTIDE_MODEL.setColorForChar( Color.red, 'a' );
		NUCLEOTIDE_MODEL.setColorForChar( Color.blue, 'T' );
		NUCLEOTIDE_MODEL.setColorForChar( Color.blue, 't' );
		NUCLEOTIDE_MODEL.setColorForChar( Color.white, '-' );
		NUCLEOTIDE_MODEL.setColorForChar( Color.yellow, 'G' );
		NUCLEOTIDE_MODEL.setColorForChar( Color.yellow, 'g' );
		NUCLEOTIDE_MODEL.setColorForChar( Color.green, 'C' );
		NUCLEOTIDE_MODEL.setColorForChar( Color.green, 'c' );
		NUCLEOTIDE_MODEL.setColorForChar( Color.blue, 'U' );
		NUCLEOTIDE_MODEL.setColorForChar( Color.blue, 'u' );
		NUCLEOTIDE_MODEL.setDefaultColor( Color.black );
		NUCLEOTIDE_MODEL.setSelectionColor( Color.ORANGE );
		
		RNA_STRUCTURE_MODEL.setColorForChar( Color.LIGHT_GRAY, '.' );
		RNA_STRUCTURE_MODEL.setColorForChar( Color.DARK_GRAY, '(' );
		RNA_STRUCTURE_MODEL.setColorForChar( Color.DARK_GRAY, ')' );
		RNA_STRUCTURE_MODEL.setColorForChar( Color.white, '-' );
		RNA_STRUCTURE_MODEL.setDefaultColor( Color.black );
		RNA_STRUCTURE_MODEL.setSelectionColor( Color.ORANGE );
		
		AMINOACID_MODEL.setColorForChar( new Color(200,200,200), 'A' );
		AMINOACID_MODEL.setColorForChar( new Color(200,200,200), 'a' );
		AMINOACID_MODEL.setColorForChar( new Color(230,230,0), 'C' );
		AMINOACID_MODEL.setColorForChar( new Color(230,230,0), 'c' );
		AMINOACID_MODEL.setColorForChar( new Color(230,10,10), 'D' );
		AMINOACID_MODEL.setColorForChar( new Color(230,10,10), 'd' );
		AMINOACID_MODEL.setColorForChar( new Color(230,10,10), 'E' );
		AMINOACID_MODEL.setColorForChar( new Color(230,10,10), 'e' );
		AMINOACID_MODEL.setColorForChar( new Color(50,50,170), 'F' );
		AMINOACID_MODEL.setColorForChar( new Color(50,50,170), 'f' );
		AMINOACID_MODEL.setColorForChar( new Color(235,235,235), 'G' );
		AMINOACID_MODEL.setColorForChar( new Color(235,235,235), 'g' );
		AMINOACID_MODEL.setColorForChar( new Color(130,130,210), 'H' );
		AMINOACID_MODEL.setColorForChar( new Color(130,130,210), 'h' );
		AMINOACID_MODEL.setColorForChar( new Color(15,130,15), 'I' );
		AMINOACID_MODEL.setColorForChar( new Color(15,130,15), 'i' );
		AMINOACID_MODEL.setColorForChar( new Color(20,90,255), 'K' );
		AMINOACID_MODEL.setColorForChar( new Color(20,90,255), 'k' );
		AMINOACID_MODEL.setColorForChar( new Color(15,130,15), 'L' );
		AMINOACID_MODEL.setColorForChar( new Color(15,130,15), 'L' );
		AMINOACID_MODEL.setColorForChar( new Color(230,230,0), 'M' );
		AMINOACID_MODEL.setColorForChar( new Color(230,230,0), 'm' );
		AMINOACID_MODEL.setColorForChar( new Color(0,220,220), 'N' );
		AMINOACID_MODEL.setColorForChar( new Color(0,220,220), 'n' );
		AMINOACID_MODEL.setColorForChar( new Color(220,150,130), 'P' );
		AMINOACID_MODEL.setColorForChar( new Color(220,150,130), 'p' );
		AMINOACID_MODEL.setColorForChar( new Color(0,220,220), 'Q' );
		AMINOACID_MODEL.setColorForChar( new Color(0,220,220), 'q' );
		AMINOACID_MODEL.setColorForChar( new Color(20,90,255), 'R' );
		AMINOACID_MODEL.setColorForChar( new Color(20,90,255), 'r' );
		AMINOACID_MODEL.setColorForChar( new Color(250,150,0), 'S' );
		AMINOACID_MODEL.setColorForChar( new Color(250,150,0), 's' );
		AMINOACID_MODEL.setColorForChar( new Color(250,150,0), 'T' );
		AMINOACID_MODEL.setColorForChar( new Color(250,150,0), 't' );
		AMINOACID_MODEL.setColorForChar( new Color(15,130,15), 'V' );
		AMINOACID_MODEL.setColorForChar( new Color(15,130,15), 'v' );
		AMINOACID_MODEL.setColorForChar( new Color(180,90,180), 'W' );
		AMINOACID_MODEL.setColorForChar( new Color(180,90,180), 'w' );
		AMINOACID_MODEL.setColorForChar( new Color(50,50,170), 'Y' );
		AMINOACID_MODEL.setColorForChar( new Color(50,50,170), 'y' );
		AMINOACID_MODEL.setColorForChar( Color.WHITE, '-' );
				
		AMINOACID_MODEL.setDefaultColor( Color.black );
		AMINOACID_MODEL.setSelectionColor( Color.ORANGE  );
		
	}
	
	protected Color selectionColor;
	
	public SequenceColorModel() {
		super();
	}
	
	public SequenceColorModel( Properties properties, Color selectionColor, Color defaultColor) {
		super();
		for (Entry<Object,Object> entry : properties.entrySet()) {
			setColorForChar(new Color(Integer.valueOf((String)entry.getValue())),((String)entry.getKey()).charAt(0));
		}
		this.selectionColor = selectionColor;
		this.defaultColor = defaultColor;
	}
	
	public Properties getProperties() {
		Properties prop = new Properties();
		
		for ( char c : getColoredChars()) {
			prop.put( String.valueOf( c), String.valueOf( getColorForChar( c).getRGB()));
		}
		
		return prop;
	}
	
	public char[] getColoredChars() {
		char[] chars = new char[ colorMap.keySet().size() ];
		int i=0;
		for (Character key:colorMap.keySet()) {
			chars[ i++ ] = key;
		}
		return chars;
	}
		
	public void setSelectionColor( Color color ) {
		selectionColor = color;
		fireColorModelChanged( new ColorChangedEvent(this));
	}
	
	public Color getSelectionColor() {
		return selectionColor;	
	}
	
	
	public void setColorForChar( Color color, char c ) {
		colorMap.put( c, color );
		fireColorModelChanged( new ColorChangedEvent(this));
	}
	
	public Color getColorForChar( char c ) {
		Color color = colorMap.get( c );
		if ( color != null )
			return color;
		return defaultColor;
	}
	
}
