package de.biozentrum.bioinformatik.sequence;

public interface IGappedSequence<S extends ISymbol, A extends IGappedSequenceAlphabet<S>> extends ISequence<S,A> {
	public ISequence<S,A> gappedSubSequence( int start, int end ); 
	public S[] getSymbolsIncludingGaps();
}
