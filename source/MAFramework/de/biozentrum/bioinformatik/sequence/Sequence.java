/*
 * Created on 16.12.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package de.biozentrum.bioinformatik.sequence;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.biozentrum.bioinformatik.alignment.events.MAEvent.MAEventType;
import de.biozentrum.bioinformatik.sequence.events.SequenceChangedEvent;
import de.biozentrum.bioinformatik.sequence.events.SequenceListener;
import de.biozentrum.bioinformatik.sequence.selection.DefaultSequenceSelectionModel;
import de.biozentrum.bioinformatik.sequence.selection.SequenceSelectionModel;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.Objects;

/**
 * @author pseibelTODO To change the template for this generated type comment go toWindow - Preferences - Java - Code Style - Code Templates
 */

public class Sequence  implements Serializable{

	protected transient ArrayList<SequenceListener> listeners;
	
	protected StringBuffer sequence;
	protected transient SequenceSelectionModel selectionModel;
	protected HashMap<String,Object> attributeList;
	private final static long serialVersionUID=1L;
	
	/**
	 * 
	 * @uml.property name="name" 
	 */
	protected String name;
	
	public Sequence(String name) {
		this(name, new StringBuffer());
	}
	
	public Sequence( String name, String sequence ) {
		this( name, new StringBuffer(sequence) );
	}
	
	public Sequence( String name, StringBuffer sequence ) {
		
		this.attributeList = new HashMap<String,Object>();
		this.name = name;
		this.sequence = sequence;
		this.listeners = new ArrayList<SequenceListener>();
		this.selectionModel = new DefaultSequenceSelectionModel( this );
	}

	private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();
		this.listeners = new ArrayList<SequenceListener>();
		this.selectionModel = new DefaultSequenceSelectionModel( this );
    }	
	
	public void setAttribute( String key, Object value ) {
		attributeList.put( key, value );
	}
	
	public Object getAttribute( String key ) {
		return attributeList.get( key );
	}
	
	public SequenceSelectionModel getSelectionModel() {
		return selectionModel;
	}
	
	public void setSelectionModel( SequenceSelectionModel selectionModel ) {
		this.selectionModel = selectionModel;
	}
	
	public void deleteCharAt( Object source, int position ) {
		sequence.deleteCharAt( position );
		fireSequenceChangedEvent( source, position, position+1, MAEventType.DELETE );
	}
	
	public void delete( Object source, int from, int to ) {
		sequence.delete( from, to );
		fireSequenceChangedEvent( source, from, to, MAEventType.DELETE );
	}
	
	public void insertCharAt( Object source, char c, int position ) {
		sequence.insert( position, c );
		fireSequenceChangedEvent( source, position, position+1, MAEventType.INSERT );
	}
	
	public void insert( Object source, char[] str, int position ) {
		sequence.insert( position, str );
		fireSequenceChangedEvent( source, position, position + str.length, MAEventType.INSERT );
	}
	
	public CharSequence subSequence( int start, int end ) {
		return sequence.subSequence( start, end);
	}
	
	public char charAt( int position ) {
		return sequence.charAt( position );
	}
	
	public String getAlignedSequence() {
		return sequence.toString();
	}
	
	public String getSequence() {
		return getAlignedSequence().replaceAll( "-", "" );
	}
	
	
	public Matcher match( String regex, boolean alignedSequence ) {
		if ( alignedSequence )
			return Pattern.compile(regex).matcher(getAlignedSequence());
		return Pattern.compile(regex).matcher(getSequence());
	}
	
	/**
	 * 
	 * @uml.property name="name"
	 */
	public String getName() {
		return name;
	}

	public void setName( String name ) {
		this.name = name;
	}
	
	public int length() {
		return sequence.length();
	}
	
	public String toString() {
		return ">" + getName() + "\n" + getSequence();
	}
	
	protected void fireSequenceChangedEvent( SequenceChangedEvent e ) {
		ArrayList<SequenceListener> lists = new ArrayList<SequenceListener>(listeners);
		for ( SequenceListener listener : lists ) {
			listener.sequenceChanged( e );
		}
	}
	
	protected void fireSequenceChangedEvent( Object source, int from, int to, MAEventType type ) {
		fireSequenceChangedEvent( new SequenceChangedEvent( source, this,from, to, type ) );
	}
	
	public void addSequenceListener( SequenceListener listener ) {
		listeners.add( listener );
	}
	
	public void removeSequenceListener( SequenceListener listener ) {
		listeners.remove( listener );
	}

	public void fillUpWithGaps(int length) {
		int diff = length - length();
		char[] gaps = new char[diff];
		for ( int i = 0; i < gaps.length; i++ ) {
			gaps[i] = '-';
		}
		insert(this, gaps, length());
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Sequence other = (Sequence) obj;
		if (!Objects.equals(this.name, other.name)) {
			return false;
		}
		if (!Objects.equals(this.sequence.toString(), other.sequence.toString())) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 37 * hash + Objects.hashCode(this.sequence.toString());
		hash = 37 * hash + Objects.hashCode(this.name);
//		System.out.println("hashcode [" + this.sequence + "/" + this.name + "] = [" + Objects.hashCode(this.sequence) + "/" + Objects.hashCode(this.name) + "] = " + hash);
		return hash;
	}

}
