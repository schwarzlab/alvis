/*
 * Created on 21.11.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package de.biozentrum.bioinformatik.sequence;

import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Polygon;
import java.awt.Transparency;
import java.awt.image.BufferedImage;
import java.util.Hashtable;

import de.biozentrum.bioinformatik.color.ColorChangedEvent;
import de.biozentrum.bioinformatik.color.ColorModelListener;




/**
 * @author pseibel
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class SequenceImages implements ColorModelListener {
	
	protected SequenceColorModel colorModel;
	protected Hashtable images;
	protected BufferedImage defaultImage;
	
	public SequenceImages( SequenceColorModel colorModel ) {
		this.colorModel = colorModel;
		this.colorModel.addColorModelListener( this );
		images = new Hashtable();
		refreshAllImages();
	}
	
	public void refreshAllImages() {
		char[] coloredChars = colorModel.getColoredChars();
		for ( int i = 0; i < coloredChars.length; i++ ) {
			generateImageForChar( coloredChars[i]  );
		}
		defaultImage = generateImage( colorModel.getDefaultColor() );
	}
	
	protected void generateImageForChar( char c ) {
		
		Color color = colorModel.getColorForChar( c );

		images.put( String.valueOf( c ), generateImage( color ) );
	}
	
	protected BufferedImage generateImage( Color color ) {
		
		Polygon p = new Polygon( new int[] { 3, 15, 18, 18, 15, 3, 0, 0 }, new int[] { 0, 0, 3, 15, 18, 18 , 15, 3 }, 8 );
		
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		GraphicsDevice gs = ge.getDefaultScreenDevice();
    		GraphicsConfiguration gc = gs.getDefaultConfiguration();

    		BufferedImage image = gc.createCompatibleImage( 18, 18 ,Transparency.TRANSLUCENT);
	
		Color startColor = Color.white;
		
		Graphics2D ig = (Graphics2D) image.getGraphics();
		
		GradientPaint paint = new GradientPaint(0, 0, startColor, 0, 25, color, false);
		
		ig.setPaint( paint );
		ig.fill( p );
		ig.setColor( color );
		ig.draw( p );
		
		return image;
	}
	
	public BufferedImage getBGImageForChar( char c ) {
		BufferedImage image = (BufferedImage) images.get( String.valueOf( c ) );
		if ( image == null ) {
			image = defaultImage;
		}
		return image;
	}
	
	
	
	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.align.ColorListener#onColorChanged(de.biozentrum.bioinformatik.align.ColorChangedEvent)
	 */
	public void colorModelChanged( ColorChangedEvent e ) {
		// TODO Auto-generated method stub
		
	}
}
