package de.biozentrum.bioinformatik.sequence.selection;

import java.util.Iterator;

public interface SequenceSelectionModel extends Iterable<SequenceRange> {
	
	public void removeSequenceSelectionListener( SequenceSelectionListener listener );
	public void addSequenceSelectionListener( SequenceSelectionListener listener );
	
	public void select( Object source, String regex );
	public void select( Object source, int start, int end, boolean ignoreGaps );
	public void select( Object source, SequenceRange range, boolean ignoreGaps );
	
	public void deselect( Object source, int start, int end );
	public void deselect( Object source, SequenceRange range );
	public void deselectAll( Object source );
	
	public void select( String regex );
	public void select( int start, int end, boolean ignoreGaps );
	public void select( SequenceRange range, boolean ignoreGaps );
	
	public boolean isDeselected( int from, int to, boolean ignoreGaps );
	public boolean isDeselected(SequenceRange range, boolean ignoreGaps );
	
	public boolean isSelected(int from, int to, boolean ignoreGaps );
	public boolean isSelected(SequenceRange range, boolean ignoreGaps );
	public boolean isAnySiteSelected(boolean ignoreGaps);
	
	public void deselect( int start, int end );
	public void deselect( SequenceRange range );
	public void deselectAll();
	
	public void reinit();
	
	public Iterator<SequenceRange> iterator();
	
}
