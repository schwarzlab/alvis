package de.biozentrum.bioinformatik.sequence.selection;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.biozentrum.bioinformatik.alignment.events.MAEvent.MAEventType;
import de.biozentrum.bioinformatik.sequence.Sequence;
import de.biozentrum.bioinformatik.sequence.events.SequenceChangedEvent;
import de.biozentrum.bioinformatik.sequence.events.SequenceListener;

public class DefaultSequenceSelectionModel implements SequenceListener, SequenceSelectionModel {

	protected ArrayList<Boolean> selectionMask;
	
	protected Sequence sequence;
	
	private ArrayList<SequenceSelectionListener> listeners;
	
	public DefaultSequenceSelectionModel( Sequence sequence ) {
			
		listeners = new ArrayList<SequenceSelectionListener>();
		this.sequence = sequence;
		this.sequence.addSequenceListener( this );
		selectionMask = new ArrayList<Boolean>( sequence.length() );
		for ( int i = 0; i < sequence.length(); i++ ) {
			selectionMask.add( false);
		}		
	}
	
	public void reinit() {
		selectionMask.clear();
		for ( int i = 0; i < sequence.length(); i++ ) {
			selectionMask.add( false);
		}		
	}
	
	public void removeSequenceSelectionListener(SequenceSelectionListener listener) {
		listeners.remove( listener );
	}

	public void addSequenceSelectionListener(SequenceSelectionListener listener) {
		listeners.add( listener );
	}

	public void select( Object source, int start, int end, boolean ignoreGaps ) {
		if ( isSelected( start, end, ignoreGaps ) )
			return;
		if ( ignoreGaps ) {
			for ( int i = start; i <= end; i++ ) {
				if ( sequence.charAt( i ) != '-' )
					selectionMask.set( i, true );
			}
		}
		else {
			for ( int i = start; i <= end; i++ ) {
				selectionMask.set( i, true );
			}
		}
		fireSequenceSelectionInserted( source, start,end);
	}

	public void select( Object source, SequenceRange range, boolean ignoreGaps ) {
		this.select( source, range.getStart(), range.getEnd(), ignoreGaps );
	}

	public void deselect(Object source, int start, int end) {
		if ( isDeselected( start, end, false ) )
			return;
		for ( int i = start; i <= end; i++ ) {
			selectionMask.set( i, false );
		}
		fireSequenceSelectionRemoved( source,start,end);
	}

	protected void fireSequenceSelectionInserted( Object source, int from, int to ) {
		fireSequenceSelectionEvent(new SequenceSelectionEvent(source,new SequenceRange(sequence,from,to),MAEventType.INSERT));	
	}
	
	protected void fireSequenceSelectionRemoved( Object source, int from, int to ) {
		fireSequenceSelectionEvent(new SequenceSelectionEvent(source,new SequenceRange(sequence,from,to),MAEventType.DELETE));	
	}
	
	protected void fireSequenceSelectionEvent( SequenceSelectionEvent e ) {
		for ( SequenceSelectionListener sequenceListener : listeners ) {
			sequenceListener.selectionChanged( e );
		}
	}
	
	public void deselect( Object source, SequenceRange range) {
		this.deselect( source, range.getStart(), range.getEnd() );
	}


	public Iterator<SequenceRange> iterator() {
		// TODO Auto-generated method stub
		return new SequenceRangeIterator();
	}
	
	private class SequenceRangeIterator implements Iterator<SequenceRange> {
		
		int currentPosition = 0;
		int lastSelectedPosition;
		
		public SequenceRangeIterator() {
			int i = selectionMask.size() - 1;
			while( !selectionMask.get(i) && i >= 0 )
				i--;
			lastSelectedPosition = i;
		}
		
		public boolean hasNext() {
			return currentPosition < lastSelectedPosition;
		}

		public SequenceRange next() {
			while( !selectionMask.get(currentPosition) && currentPosition < selectionMask.size() )
				currentPosition++;
			int start = currentPosition;
			while( selectionMask.get(currentPosition) && currentPosition < selectionMask.size() )
				currentPosition++;
			int end = currentPosition - 1;
			
			if ( end >= start ) {
				return new SequenceRange( sequence, start, end);
			}
			
			return null;
		}

		public void remove() {
			// TODO Auto-generated method stub
			
		}
		
	}

	public void deselectAll(Object source) {
		deselect( source, 0, selectionMask.size() - 1 );
	}


	public void select(int start, int end, boolean ignoreGaps) {
		this.select( this, start, end, ignoreGaps);
	}


	public void select(SequenceRange range, boolean ignoreGaps) {
		this.select( this, range, ignoreGaps);
	}


	public void deselect(int start, int end) {
		this.deselect( this, start, end);
	}


	public void deselect(SequenceRange range) {
		this.deselect( this, range );
	}


	public void deselectAll() {
		this.deselectAll( this );
	}


	public boolean isSelected(int from, int to, boolean ignoreGaps ) {
		boolean temp = true;
		int i = from;
		do {
			if ( !ignoreGaps || sequence.charAt( i ) != '-' )
				temp = selectionMask.get( i );
			i++;
		}
		while ( i <= to && temp );
		return temp;
	}
	
	public boolean isAnySiteSelected(boolean ignoreGaps ) {
		boolean temp = true;
		int from = 0;
		int to = sequence.length()-1;
		
		int i = from;
		do {
			if ( !ignoreGaps || sequence.charAt( i ) != '-' )
				temp = selectionMask.get( i );
			i++;
		}
		while ( i <= to && !temp );
		return temp;
	}


	public boolean isSelected(SequenceRange range, boolean ignoreGaps) {
		return this.isSelected( range.getStart(), range.getEnd(), ignoreGaps );
	}


	public void sequenceChanged( SequenceChangedEvent e ) {
		if ( e.getType() == MAEventType.DELETE ) {
			for ( int i = e.getLastPos()-1; i >= e.getFirstPos(); i-- ) {
				selectionMask.remove( i );
			}
		}
		else if ( e.getType() == MAEventType.INSERT ) {
			for ( int i = e.getFirstPos(); i < e.getLastPos(); i++ ) {
				selectionMask.add( i, false );
			}
		}
	}


	public void select(Object source, String regex) {
		Matcher m = Pattern.compile(regex).matcher(sequence.getAlignedSequence());
		int start = 0;
		while( m.find( start ) ) {
			select( m.start(), m.end()-1, false );
			start = m.start() + 1;
		}
	}


	public void select(String regex) {
		select( this, regex );
	}


	public boolean isDeselected(int from, int to, boolean ignoreGaps ) {
		boolean temp = true;
		int i = from;
		do {
			if ( !ignoreGaps || sequence.charAt( i ) != '-' )
				temp = !selectionMask.get( i );
			i++;
		}
		while ( i <= to && temp );
		return temp;
	}


	public boolean isDeselected(SequenceRange range, boolean ignoreGaps ) {
		return isDeselected( range.getStart(), range.getEnd(), ignoreGaps );
	}
}
