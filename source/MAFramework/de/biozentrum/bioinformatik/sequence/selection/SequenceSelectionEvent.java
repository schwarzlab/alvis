package de.biozentrum.bioinformatik.sequence.selection;


import de.biozentrum.bioinformatik.alignment.events.MAEvent;

public class SequenceSelectionEvent extends MAEvent {

	protected SequenceRange range;
	
	public SequenceSelectionEvent(Object source, SequenceRange range, MAEventType type ) {
		super(source, type);
		this.range = range;
	}
		
	
	public SequenceRange getRange() {
		return range;
	}
}
