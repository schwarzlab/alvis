package de.biozentrum.bioinformatik.sequence.selection;

import java.util.EventListener;

public interface SequenceSelectionListener extends EventListener {
	public void selectionChanged( SequenceSelectionEvent e );
}
