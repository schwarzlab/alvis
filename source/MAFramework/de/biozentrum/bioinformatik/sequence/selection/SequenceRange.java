package de.biozentrum.bioinformatik.sequence.selection;

import de.biozentrum.bioinformatik.sequence.Sequence;

public class SequenceRange {
	protected Sequence sequence;
	protected int start;
	protected int end;
	
	public SequenceRange( Sequence sequence, int start, int end ) {
		this.sequence = sequence;
		this.start = start;
		this.end = end;
	}
	
	public SequenceRange combine( SequenceRange otherRange ) {
		if ( start > otherRange.getEnd() + 1 || 
				otherRange.getStart() > end + 1 )
			return null;
		
		int newStart = Math.min( start, otherRange.getStart() );
		int newEnd = Math.max( end, otherRange.getEnd() );
		
		return new SequenceRange( sequence, newStart, newEnd );
	}
	
	public int getStart() {
		return start;
	}
	
	public int getEnd() {
		return end;
	}
	
	public CharSequence charSequence() {
		return sequence.subSequence( start, end );
	}
}
