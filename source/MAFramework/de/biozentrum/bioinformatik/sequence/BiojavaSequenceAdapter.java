/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.biozentrum.bioinformatik.sequence;

import de.biozentrum.bioinformatik.sequence.events.SequenceListener;
import de.biozentrum.bioinformatik.sequence.selection.DefaultSequenceSelectionModel;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import org.biojava.bio.BioException;
import org.biojava.bio.seq.io.SymbolTokenization;
import org.biojava.bio.symbol.AbstractSymbolList;
import org.biojava.bio.symbol.Edit;
import org.biojava.bio.symbol.IllegalAlphabetException;
import org.biojava.bio.symbol.IllegalSymbolException;
import org.biojava.bio.symbol.SimpleSymbolList;
import org.biojava.bio.symbol.Symbol;
import org.biojava.bio.symbol.SymbolList;
import org.biojava.utils.ChangeVetoException;

/**
 *	Immutable sequence adapter
 * @author Roland Schwarz <rfs32@cam.ac.uk>
 */
public class BiojavaSequenceAdapter extends Sequence implements Serializable{
	private final static long serialVersionUID=1L;
	
	final SymbolList delegate;
	private final SymbolTokenization token;
	private int hashCache;
	private final boolean toUpperCase;

	public BiojavaSequenceAdapter(org.biojava.bio.symbol.SymbolList sl, String name, boolean toUpperCase) throws BioException {
		super(name);
		this.delegate = sl;
		this.token = this.delegate.getAlphabet().getTokenization("token");
		this.selectionModel = new DefaultSequenceSelectionModel(this);
		this.hashCache = 0;
		this.toUpperCase = toUpperCase;
	}
	
	public BiojavaSequenceAdapter(org.biojava.bio.seq.Sequence sl, boolean toUpperCase) throws BioException {
		this(sl, sl.getName(), toUpperCase);
	}

	private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();
		this.listeners = new ArrayList<>();
		this.selectionModel = new DefaultSequenceSelectionModel( this );
    }	
	
	@Override
	public char charAt(int position) {
		try {
			char symbol = token.tokenizeSymbol(delegate.symbolAt(position+1)).charAt(0);
			if (toUpperCase) {
				symbol = Character.toUpperCase(symbol);
			}
			return symbol;
		} catch (IllegalSymbolException ex) { // really bad.. dunno how to do it better
			return 'x';
		}
	}

	@Override
	public void delete(Object source, int from, int to) {
		throw new UnsupportedOperationException("SymbolListSequenceAdapter is read only");
	}

	@Override
	public void deleteCharAt(Object source, int position) {
		throw new UnsupportedOperationException("SymbolListSequenceAdapter is read only");
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof BiojavaSequenceAdapter) {
			return ((BiojavaSequenceAdapter)obj).hashCode() == this.hashCode();
		} else if (obj instanceof Sequence) {
			Sequence other = (Sequence)obj;
			return this.name.equals(other.name) && this.delegate.seqString().equals(other.sequence.toString());
		} else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		if (hashCache!=0) return hashCache;
		
		int hash = 7;
		if (delegate instanceof AbstractSymbolList) {
			AbstractSymbolList asl = (AbstractSymbolList)delegate;
			hash = 97 * hash + asl.hashCode();
		} else { // slow
			String seq = delegate.seqString();
			if (toUpperCase) {
				seq = seq.toUpperCase();
			}
			hash = 97 * hash + Objects.hashCode(seq);
		}
		hash = 97 * hash + Objects.hashCode(name);
		hashCache = hash;
		return hash;
	}

	@Override
	public void fillUpWithGaps(int length) {
		if (length==length()) {
			return;
		} else {
			// fill up protein sequence with gaps
			Symbol[] gaps = new Symbol[length-length()];
			for (int j=0;j<gaps.length;j++) {
				gaps[j] = delegate.getAlphabet().getGapSymbol();
			}
			try {
				delegate.edit(new Edit(length()+1, 0, new SimpleSymbolList(gaps, gaps.length, delegate.getAlphabet())));
				selectionModel.reinit();
			} catch (IllegalAlphabetException ex) {
				ex.printStackTrace();
			} 
		}
	}

	@Override
	public String getAlignedSequence() {
		String seq = delegate.seqString();
		if (toUpperCase) {
			seq = seq.toUpperCase();
		}
		return seq;
	}

	@Override
	public String getSequence() {
		return getAlignedSequence().replaceAll("-", "");
	}

	@Override
	public void insert(Object source, char[] str, int position) {
		throw new UnsupportedOperationException("SymbolListSequenceAdapter is read only");
	}

	@Override
	public void insertCharAt(Object source, char c, int position) {
		throw new UnsupportedOperationException("SymbolListSequenceAdapter is read only");
	}

	@Override
	public CharSequence subSequence(int start, int end) {
		CharSequence charseq = delegate.subStr(start+1, end+1);
		if (toUpperCase) {
			return charseq.toString().toUpperCase();
		} else {
			return charseq;
		}
	}

	@Override
	public int length() {
		if (delegate==null) return 0;
		
		return delegate.length();
	}

	@Override
	public Matcher match(String regex, boolean alignedSequence) {
		throw new UnsupportedOperationException("NIY");
	}

	public SymbolList toSymbolList() {
		return delegate;
	}
	
	
	
	
	
	
	
}
