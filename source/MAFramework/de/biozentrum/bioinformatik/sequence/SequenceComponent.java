/*
 * Created on 20.11.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package de.biozentrum.bioinformatik.sequence;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Point;
import java.awt.Transparency;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;



/**
 * @author pseibel
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class SequenceComponent extends JPanel implements MouseListener {

	/**
	 * Comment for <code>serialVersionUID</code>
	 */
	private static final long serialVersionUID = 3761973756763320633L;
	
	protected final int boxPixels = 18;
	protected final int boxDistance = 1;
	
	protected Sequence sequenceModel;
	protected SequenceColorModel colorModel;
	protected SequenceImages images;
	
	protected boolean needsCompleteRedraw;
	protected BufferedImage buffer;
	
	protected Dimension preferredSize;
	
	public SequenceComponent( Sequence model ) {
		sequenceModel = model;
		addMouseListener( this );
		preferredSize = new Dimension( ( boxPixels + boxDistance ) * sequenceModel.length() , 20 );
		
		needsCompleteRedraw = true;
		
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		GraphicsDevice gs = ge.getDefaultScreenDevice();
    		GraphicsConfiguration gc = gs.getDefaultConfiguration();
    
	
    		this.buffer = gc.createCompatibleImage( preferredSize.width, preferredSize.height ,Transparency.TRANSLUCENT);
	
		
		//buffer = new BufferedImage(preferredSize.width, preferredSize.height,BufferedImage.TYPE_4BYTE_ABGR);
		
		/// TESTBEREICH
		colorModel = SequenceColorModel.NUCLEOTIDE_MODEL;
		images = new SequenceImages( colorModel );
	}
	
	public void setSequenceModel( Sequence model ) {
		sequenceModel = model;
	}
	
	public Sequence getSequenceModel() {
		return sequenceModel;
	}
	
	public void paintComponent( Graphics g ) {
		super.paintComponent( g );

		Graphics2D gc = (Graphics2D)g;	
		if ( needsCompleteRedraw ) {
			Graphics2D ic = (Graphics2D)buffer.getGraphics();
			for ( int i = 0; i < sequenceModel.length(); i++ ) {
				ic.drawImage( images.getBGImageForChar( sequenceModel.charAt( i ) ), null, 0, 0 );
				ic.setColor( Color.black );
				ic.drawString( new String( new char[]{ sequenceModel.charAt( i )} ),5, 13 );
				ic.translate( ( boxPixels + boxDistance ), 0 );		
			}
			needsCompleteRedraw = false;
		}
		gc.drawImage( buffer, null, 0, 0 );
	}
	
	protected int getCharPositionForPoint( Point p ) {
		return p.x / 19;
	}
	
	public Dimension getPreferredSize() {
		return preferredSize;
	}
	
	public int getWidth() {
		return sequenceModel.length() * 19;
	}
	
	public int getHeight() {
		return 20;
	}
	
	public Dimension getMinimumSize() {
		return preferredSize;	
	}

	/* (non-Javadoc)
	 * @see java.awt.event.MouseListener#mouseClicked(java.awt.event.MouseEvent)
	 */
	public void mouseClicked(MouseEvent e) {
	}

	/* (non-Javadoc)
	 * @see java.awt.event.MouseListener#mousePressed(java.awt.event.MouseEvent)
	 */
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see java.awt.event.MouseListener#mouseReleased(java.awt.event.MouseEvent)
	 */
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see java.awt.event.MouseListener#mouseEntered(java.awt.event.MouseEvent)
	 */
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see java.awt.event.MouseListener#mouseExited(java.awt.event.MouseEvent)
	 */
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}
