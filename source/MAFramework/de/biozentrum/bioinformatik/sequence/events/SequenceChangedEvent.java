/*
 * Created on 16.12.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package de.biozentrum.bioinformatik.sequence.events;

import de.biozentrum.bioinformatik.alignment.events.MARangeEvent;
import de.biozentrum.bioinformatik.sequence.Sequence;

/**
 * @author pseibel
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class SequenceChangedEvent extends MARangeEvent {
	
	/**
	 * Comment for <code>serialVersionUID</code>
	 */
	private static final long serialVersionUID = 3257565092433508662L;
	protected Sequence sequence;
	
	public SequenceChangedEvent( Object source, Sequence sequence, int firstPos, int lastPos, MAEventType type ) {
		super( source,firstPos,lastPos, type );
		

	}
	
	public Sequence getSequence() {
		return sequence;
	}



	


}
