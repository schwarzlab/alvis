/*
 * Created on 16.12.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package de.biozentrum.bioinformatik.ca;


import java.awt.geom.Rectangle2D;

import de.biozentrum.bioinformatik.ca.annotation.CAAnnotationProvider;
import org.jblas.DoubleMatrix;

/**
 * @author pseibel
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface CADataModel extends CAConstants {
	public static enum ScalingOptions {
		SPECIES ("Scale sequences"), 
		SITES ("Scale sites"), 
		BOTH ("Scale both"),
		NONE ("No scaling");
			
		private String text;
		
		ScalingOptions(String text) {
			this.text=text;
		}
		
		public String toString() {return text;}
		};
		
	public void setAnnotation( CAAnnotationProvider annotationProvider );
	public CAAnnotationProvider getAnnotation();
	
	public double[] getValues( int dataSetIdentifier, int position );
	
	public void setMatrix( int dataSetIdentifier, DoubleMatrix matrix );
	public DoubleMatrix getMatrix( int dataSetIdentifier );
	
	public CAData getData( int dataSetIdentifier, int position );
	public CAData[] getDataSet( int dataSetIdentifier );
	public int sizeOfDataSet( int dataSetIdentifer );
	
	public void setComponents( int[] components );
	public double getValue( int dataSetIdentifier, int position, int component );
	
	public void setScaling(ScalingOptions option);
	public DoubleMatrix getComponentInformations();
	
	public int[] getComponents();
	public int numberOfComponents();
	
	public double minValueForComponent( int component );
	public double maxValueForComponent( int component );
	
	// Selection
	
	public void beginSelectionTransaction();
	public void commitSelectionTransaction();
	
	public void select( int dataSetIdentifier, int position );
	public void select( int[] dataSetIdentifier, int[] positions );
	public void select( CAData[] data );
	
	public void removeSelection( int dataSetIdentifier, int position );
	public void removeSelection( int[] dataSetIdentifier, int[] positions );
	public void removeSelection( CAData[] data );
	public void clearSelection();
	
	public CAData[] getSelectedData();
	public CAData[] getSelectedData( int dataSetIdentifier );
	
	public CAData[] getDataInRect( int dataSetIdentifier, Rectangle2D rect );

	// Highlighting
	
	/*public void highlight(  int dataSetIdentifier, int position );
	public void highlight( int[] dataSetIdentifier, int[] positions );
	public void highlight( CAData[] data );
	
	public void removeHighlighted(  int dataSetIdentifier, int position );
	public void removeHighlighted( int[] dataSetIdentifier, int[] positions );
	public void removeHighlighted( CAData[] data );
	public void removeAllHighlighted();
	
	public CAData[] getHighlightedData();
	public CAData[] getHighlightedData( int dataSetIdentifier );
	*/
	// Listeners
	
	public void addCADataListener( CADataListener listener );
	public void removeCADataListener( CADataListener listener );
	
	public void addCAComponentListener( CAComponentListener listener );
	public void removeCAComponentListener( CAComponentListener listener );
}
