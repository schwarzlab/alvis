/*
 * Created on 16.01.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package de.biozentrum.bioinformatik.ca;

import java.util.EventObject;

/**
 * @author pseibel
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class CAComponentChangedEvent extends EventObject {
	/**
	 * Comment for <code>serialVersionUID</code>
	 */
	private static final long serialVersionUID = 3761966081656369715L;
	
	protected int[] previousComponents;
	protected int[] newComponents;
	
	/**
	 * @param source
	 */
	public CAComponentChangedEvent( Object source, int[] prevComponents, int[] newComponents ) {
		super( source );
		this.previousComponents = prevComponents;
		this.newComponents = newComponents;
	}
	
	public int[] previousComponents() {
		return previousComponents;
	}
	
	public int[] newComponents() {
		return newComponents;
	}
}
