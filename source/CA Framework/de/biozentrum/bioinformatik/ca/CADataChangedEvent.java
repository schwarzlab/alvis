/*
 * Created on 13.01.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package de.biozentrum.bioinformatik.ca;

import java.util.EventObject;

/**
 * @author binf024
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class CADataChangedEvent extends EventObject {
	
	public static final int INSERTED = 0;
	public static final int REMOVED  = 1;
	
	protected int action;
	protected int type;
	
	protected CAData[] data;
	
	/**
	 * @param source
	 */
	public CADataChangedEvent( Object source ) {
		super(source);
	}
	
	public CADataChangedEvent( Object source, CAData[] data, int type ) {
		super( source );
		this.data = data;
		this.type = type;
	}
	
	public int getType() {
		return type;
	}
	
	public CAData[] getData() {
		return data;
	}
}
