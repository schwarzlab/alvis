/*
 * Created on 05.01.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package de.biozentrum.bioinformatik.ca;



/**
 * @author pseibel
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface CAData {
	
	public void select();
	
	public void deselect();
	
	public boolean isSelected();
	
	public String getName();
	
	public Object getAnnotation( String key );
	
	public String getAnnotationLabel( String key );
	
	public Class getAnnotationClass( String key );
	
	public String[] getAnnotationKeys();
	
	public int getDataSetIdentifier();
	
	public double[] getValues();

	public String identifier();

	public int getPosition();
}
