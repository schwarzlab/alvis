/*
 * Created on 15.12.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package de.biozentrum.bioinformatik.ca;


import java.io.Serializable;

import javax.swing.SwingWorker;
import org.jblas.DoubleMatrix;
import org.jblas.MatrixFunctions;
import org.jblas.Singular;

/**
 * @author pseibelTODO To change the template for this generated type comment go toWindow - Preferences - Java - Code Style - Code Templates
 */

public class CorrespondenceAnalysis extends SwingWorker<CADataModel, Void> implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1623928104529950183L;

	private transient CADataProvider dataProvider;

	private DoubleMatrix GG;
	private DoubleMatrix FF;
	
	private DoubleMatrix singularValues;
	private DoubleMatrix information;
	
	boolean transposed = false;
	
	public CorrespondenceAnalysis( CADataProvider provider ) {
		dataProvider = provider;	
	}

	
	public DoubleMatrix getFF() {
		return FF;
	}
	
	public DoubleMatrix getGG() {
		return GG;
	}
	
	public DoubleMatrix getSingularValues() {
		return singularValues;
	}
	
	public DoubleMatrix getInformation() {
		return information;
	}
	
	public DoubleMatrix getFirstValuesForComponent( int comp ) {
		double[] row = new double[FF.columns];
		
		for ( int i = 0; i < FF.columns; i++ ) {
			row[i] = FF.get(comp, i );
		}
		
		
		return new DoubleMatrix( row );
	}
	
	public DoubleMatrix getSecondValuesForComponent( int comp ) {
		double[] row = new double[GG.columns];
		
		for ( int i = 0; i < GG.columns; i++ ) {
			row[i] = GG.get(comp, i );
		}
		
		return new DoubleMatrix( row );
	}

	public CADataModel performCA() throws Exception {
		setProgress(0);
		DoubleMatrix data = dataProvider.getDataMatrix();
		
		if ( data.rows < data.columns ) {
			transposed = true;
			data = data.transpose();
		}
		
		DoubleMatrix p = new DoubleMatrix();
		p.copy(data);
		
		//System.out.println( p.rows() + " ------ " + p.columns() );
		//System.exit(0 );
		
		double min = 0;
		
		//System.out.println( "Verschieben ins positive..." );

		for ( int i = 0; i < p.rows; i++ ) {
			for ( int j = 0; j < p.columns; j++ ) {
				double tmp = p.get( i, j );
				if ( tmp < min ) min = tmp;
			}
		}
		
		//System.out.println("Min:" +min);
		min = Math.abs(min);

		for ( int i = 0; i < p.rows; i++ ) {
			for ( int j = 0; j < p.columns; j++ ) {
				p.put( i, j, p.get( i, j ) + min +1 );
			}
		}
		
		
//		for ( int i = 0; i < p.rows(); i++ ) {
//			for ( int j = 0; j < p.columns(); j++ ) {
//				double tmp = Math.abs( p.get( i, j ) );
//				if ( tmp > max )
//					max = tmp;
//			}
//		}
//		
//		for ( int i = 0; i < p.rows(); i++ ) {
//			for ( int j = 0; j < p.columns(); j++ ) {
//				p.set( i, j, p.get( i, j ) + 2*max );
//			}
//		}
		
		//System.out.println( "fertig" );
		
		setProgress(1);
		
		//System.out.println( "Normieren auf Verteilung..." );
		double sum = p.sum();
				
		for ( int i = 0; i < p.rows; i++ ) {
			for ( int j = 0; j < p.columns; j++ ) {
				p.put( i, j, p.get( i, j ) / sum);
			}
		}
		
		
		//System.out.println( "fertig" );
		
		setProgress(2);
		//System.out.println( "Zeilen und Spaltensummen ..." );
		
		DoubleMatrix rowSums = p.rowSums();
		DoubleMatrix columnSums = p.columnSums();

		//System.out.println( "fertig" );
		
		
		setProgress(3);
		
		//System.out.println( "Äußeres Produkt + Wurzel ...." );
		DoubleMatrix q = rowSums.mmul(columnSums); // rfs: need to check

		DoubleMatrix qSqrt = MatrixFunctions.sqrt(q);
		
		DoubleMatrix f = p.sub(q);
		f.divi(qSqrt);
		
		p = null;
		q = null;
		qSqrt = null;
		data = null;
		data = null;
		
		//System.out.println( "fertig" );
		
		setProgress(4);
		
		/*for ( int i = 0; i < f.rows(); i++) {
			for ( int j = 0; j < f.columns(); j++) {
				System.out.print(  f.get(i, j) + " ");
			}
			System.out.println();
		}*/
		
		DoubleMatrix[] svd = Singular.sparseSVD(f);
		
		setProgress(5);
		
		DoubleMatrix sv = svd[1];
		this.singularValues=sv;
		
		double svSum = sv.sum();
		double[] info = new double[ sv.length ];

		
		for ( int i = 0; i < info.length; i++ ) {
			info[i] = sv.get(i) / svSum;
		}
		information = new DoubleMatrix( info );
		
		DoubleMatrix u = svd[0];
		
		FF = u;
		for ( int i = 0; i < FF.rows; i++ ) { 
			for ( int j = 0; j < FF.columns; j++ ) {
				FF.put( i, j, FF.get(i,j) / Math.sqrt( rowSums.get( i ) ) );
			}
		}
		
		setProgress(6);
		
		DoubleMatrix v = svd[2];
		
		GG = v;
		for ( int i = 0; i < GG.rows; i++ ) {
			for ( int j = 0; j < GG.columns; j++ ) {
				GG.put( i, j, GG.get(i,j) / Math.sqrt( columnSums.get( j ) ));
			}
		}
		
		setProgress(7);
		
		// Transponierte CA
		if ( transposed ) {
			DoubleMatrix tmp = FF;
			FF = GG;
			GG = tmp;
			
		}
		
		
		
		return  new CADefaultModel( this );
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	protected CADataModel doInBackground() throws Exception {
		return performCA();
	}
	
	public void done() {
		setProgress(8);
	}
}
