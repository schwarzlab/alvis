/*
 * Created on 14.04.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package de.biozentrum.bioinformatik.ca.annotation;

import de.biozentrum.bioinformatik.ca.CAData;

/**
 * @author binf024
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface CAAnnotationProvider {	
	public Object getAnnotation( CAData data, String key );
	public Class getClass( String key );
	public String getLabel( String key );
	public String getLabel( int dataSetIdentifier );
	public String[] getKeys( int dataSetIdentifier );
}
