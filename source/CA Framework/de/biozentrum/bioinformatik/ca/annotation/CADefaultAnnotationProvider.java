/*
 * Created on 14.04.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package de.biozentrum.bioinformatik.ca.annotation;

import de.biozentrum.bioinformatik.ca.CAData;
import de.biozentrum.bioinformatik.ca.CADataModel;

/**
 * @author binf024
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class CADefaultAnnotationProvider implements CAAnnotationProvider {

	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.ca.annotation.CAAnnotationProvider#getClass(java.lang.String)
	 */
	public Class getClass(String key) {
		if ( key.equals( CAAnnotation.NAME ) )
			return String.class;
		else if ( key.equals( CAAnnotation.IDENTIFIER ) )
			return String.class;
		
		return null;
	}

	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.ca.annotation.CAAnnotationProvider#getAnnotation(de.biozentrum.bioinformatik.ca.CAData, java.lang.String)
	 */
	public Object getAnnotation(CAData data, String key) {
		if ( key.equals( CAAnnotation.NAME ) )
			return data.getDataSetIdentifier() + "-" + data.getPosition();
		else if ( key.equals( CAAnnotation.IDENTIFIER ) )
			return data.getDataSetIdentifier() + "-" + data.getPosition();
		
		return null;
	}
	

	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.ca.annotation.CAAnnotationProvider#getLabel(java.lang.String)
	 */
	public String getLabel(String key) {
		return key;
	}

	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.ca.annotation.CAAnnotationProvider#getLabel(int)
	 */
	public String getLabel( int dataSetIdentifier ) {
		
		switch ( dataSetIdentifier ) {
			case CADataModel.FIRST_DATA_SET:
				return "First Data Set";
			case CADataModel.SECOND_DATA_SET:
				return "Second Data Set";
		
		}
		
		return "Data";
	}

	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.ca.annotation.CAAnnotationProvider#getKeys(int)
	 */
	public String[] getKeys( int dataSetIdentifier ) {
		return new String[] { CAAnnotation.NAME, CAAnnotation.IDENTIFIER };
	}

}
