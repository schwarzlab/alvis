/*
 * Created on 12.01.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package de.biozentrum.bioinformatik.ca;

import java.util.EventListener;

/**
 * @author binf024
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface CADataListener extends EventListener {
	public void selectionChanged( CADataChangedEvent e );
	public void selectionTransactionWillBegin();
	public void selectionTransactionDidCommit();
}
