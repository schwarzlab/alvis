/*
 * Created on 07.02.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package de.biozentrum.bioinformatik.ca.io;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import org.jblas.DoubleMatrix;

/**
 * @author binf024
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class MatrixWriter extends FileWriter {

	/**
	 * @param fileName
	 * @throws IOException
	 */
	public MatrixWriter( String fileName ) throws IOException {
		super( fileName );
	}
	
	public void writeMatrix( DoubleMatrix matrix ) throws IOException {
		BufferedWriter bw = new BufferedWriter( this );
		for ( int i = 0; i < matrix.rows; i++ ) {
			for ( int j = 0; j < matrix.columns; j++ ) {
				if ( j < matrix.columns - 1 )
					bw.write( String.valueOf( matrix.get( i, j ) ) + "\t" );
				else
					bw.write( String.valueOf( matrix.get( i, j ) ) + "\n" );
			}
		}
		bw.close();	
	}

}
