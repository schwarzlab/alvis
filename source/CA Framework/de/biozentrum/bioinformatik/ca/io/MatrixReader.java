/*
 * Created on 02.02.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package de.biozentrum.bioinformatik.ca.io;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import org.jblas.DoubleMatrix;

/**
 * @author pseibel
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class MatrixReader extends FileReader {

	/**
	 * @param arg0
	 * @throws FileNotFoundException
	 */
	public MatrixReader(String filename) throws FileNotFoundException {
		super(filename);
	}
	
	public DoubleMatrix readMatrix() throws IOException {
		BufferedReader br = new BufferedReader( this );
		ArrayList vectors = new ArrayList();
		String line = br.readLine();
		while ( line != null ) {
			String[] doubles = line.split( "	" );
			double[] vec = new double[ doubles.length ];
			for ( int i = 0; i < doubles.length; i++ ) {
				try {
					vec[ i ] = Double.parseDouble( doubles[ i ] );
				}
				catch (NumberFormatException e ) {
					vec[ i ] = 0;
				}
				//if ( vec[ i ] > 1 )
				 //vec[i] = 1;
			}
			vectors.add( vec );
			line = br.readLine();
		}
		
		double[][] vecs = (double[][]) vectors.toArray( new double[0][] );
		DoubleMatrix matrix = new DoubleMatrix( vecs );
		br.close();	
		return matrix;
	}

}