/*
 * Created on 16.01.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package de.biozentrum.bioinformatik.ca;

import java.util.EventListener;

/**
 * @author pseibel
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface CAComponentListener extends EventListener {
	public void componentsChanged( CAComponentChangedEvent e );
}
