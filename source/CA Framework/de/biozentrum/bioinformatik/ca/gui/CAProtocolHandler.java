/*
 * Created on 15.02.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package de.biozentrum.bioinformatik.ca.gui;

import de.biozentrum.bioinformatik.ca.CAData;

/**
 * @author binf024
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface CAProtocolHandler {
	public void invoke( String identifier, CAData data );
	public String protocol();
}
