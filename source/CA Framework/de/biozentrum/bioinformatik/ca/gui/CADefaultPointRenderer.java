/*
 * Created on 16.02.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package de.biozentrum.bioinformatik.ca.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.geom.Point2D;

import de.biozentrum.bioinformatik.ca.CAData;

/**
 * @author binf024
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class CADefaultPointRenderer implements CAPointRenderer {
	
	protected Color[] colors = new Color[] { Color.BLUE, Color.GREEN };
	
	protected CAData[][] pixelArray;
	protected Dimension[] dimensions = new Dimension[2];
	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.diplomarbeit.ca.gui.CAPointRenderer#setPixelArray(de.biozentrum.bioinformatik.diplomarbeit.ca.CAData[][])
	 */
	public void setPixelArray(CAData[][] data) {
		pixelArray = data;
	}

	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.diplomarbeit.ca.gui.CAPointRenderer#drawAt(java.awt.Graphics2D, java.awt.geom.Point2D, de.biozentrum.bioinformatik.diplomarbeit.ca.CAData, java.awt.geom.Rectangle2D)
	 */
	public void drawAt(Graphics2D g, Point2D point, CAData data) {
		int identifier = data.getDataSetIdentifier();
		
		g.setColor( colors[ identifier ] );
		drawIntern( g, point, data);
	}
	
	private void drawIntern(Graphics2D g, Point2D point, CAData data) {
		int identifier = data.getDataSetIdentifier();
		g.fillRect( (int)(point.getX()-(dimensions[identifier].getWidth()/2))
				, (int)(point.getY()-(dimensions[identifier].getHeight()/2))
				, (int)dimensions[identifier].getWidth()
				, (int)dimensions[identifier].getHeight() );
		
		for ( int i = 0; i < dimensions[identifier].getWidth(); i++ ) {
			for ( int j = 0; j < dimensions[identifier].getHeight(); j++ ) {
				try {
					pixelArray[(i + (int)(point.getX()-(dimensions[identifier].getWidth()/2))) ][j + (int)(point.getY()-(dimensions[identifier].getHeight()/2)) ] = data;
				}
				catch ( ArrayIndexOutOfBoundsException e ) {
				}
			}
		}
	}
	
	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.diplomarbeit.ca.gui.CAPointRenderer#drawHighlightingAt(java.awt.Graphics2D, java.awt.geom.Point2D, de.biozentrum.bioinformatik.diplomarbeit.ca.CAData, java.awt.geom.Rectangle2D)
	 */
	public void drawHighlightingAt(Graphics2D g, Point2D point, CAData data) {
		g.setColor( Color.RED );
		drawIntern( g, point, data);
		int identifier = data.getDataSetIdentifier();
		if ( dimensions[identifier].getWidth() < 4 || dimensions[identifier].getHeight() < 4 )
			g.drawRect( (int)(point.getX()-((dimensions[identifier].getWidth() + 2)/2))
				, (int)(point.getY()-((dimensions[identifier].getHeight() + 2)/2))
				, (int)dimensions[identifier].getWidth()+2
				, (int)dimensions[identifier].getHeight()+2 );
	}

	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.diplomarbeit.ca.gui.CAPointRenderer#drawSelectionAt(java.awt.Graphics2D, java.awt.geom.Point2D, de.biozentrum.bioinformatik.diplomarbeit.ca.CAData, java.awt.geom.Rectangle2D)
	 */
	public void drawSelectionAt(Graphics2D g, Point2D point, CAData data) {
		g.setColor( Color.RED );
		drawIntern( g, point, data);
		int identifier = data.getDataSetIdentifier();
		if ( dimensions[identifier].getWidth() < 3 || dimensions[identifier].getHeight() < 3 )
			g.drawRect( (int)(point.getX()-((dimensions[identifier].getWidth() + 3)/2))
				, (int)(point.getY()-((dimensions[identifier].getHeight() + 3)/2))
				, (int)dimensions[identifier].getWidth()+3
				, (int)dimensions[identifier].getHeight()+3 );
	}

	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.diplomarbeit.ca.gui.CAPointRenderer#annotateDataAt(java.awt.Graphics2D, java.awt.geom.Point2D, de.biozentrum.bioinformatik.diplomarbeit.ca.CAData)
	 */
	public void annotateDataAt(Graphics2D g, Point2D point, CAData data) {
		g.setColor( Color.DARK_GRAY );
		g.drawString( data.getName(), (int)point.getX() + 2 , (int)point.getY() - 2 );		
	}

	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.diplomarbeit.ca.gui.CAPointRenderer#setDataDimension(int, java.awt.Dimension)
	 */
	public void setDataDimension(int dataSetIdentifier, Dimension dimension) {
		dimensions[ dataSetIdentifier ] = dimension;
		
	}

}
