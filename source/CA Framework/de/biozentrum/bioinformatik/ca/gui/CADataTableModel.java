/*
 * Created on 17.02.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package de.biozentrum.bioinformatik.ca.gui;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import de.biozentrum.bioinformatik.ca.CAData;

/**
 * @author binf024
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class CADataTableModel extends AbstractTableModel {
	
	private ArrayList dataList;
	
	public CADataTableModel() {
		dataList = new ArrayList();
	}
	
	public void removeCAData( CAData[] data ) {
		for ( int i = 0; i < data.length; i++ ) {
			if ( dataList.contains( data[i] ) )
				dataList.remove( data[i] );
		}
		fireTableDataChanged();
	}
	
	public void addCAData( CAData[] data ) {
		for ( int i = 0; i < data.length; i++ ) {
			if ( !dataList.contains( data[i] ) )
				dataList.add( data[i] );
		}
		fireTableDataChanged();
	}
	/* (non-Javadoc)
	 * @see javax.swing.table.TableModel#getColumnCount()
	 */
	public int getColumnCount() {
		return 1;
	}

	/* (non-Javadoc)
	 * @see javax.swing.table.TableModel#getRowCount()
	 */
	public int getRowCount() {
		return dataList.size();
	}

	public Class getColumnClass( int column ) {
		return CAData.class;
	}
	
	public boolean isCellEditable(int arg0, int arg1) {
		return false;
	}
	/* (non-Javadoc)
	 * @see javax.swing.table.TableModel#getValueAt(int, int)
	 */
	public Object getValueAt(int rowIndex, int columnIndex) {
		return (CAData)dataList.get( rowIndex );
	}

}
