/*
 * Created on 10.05.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package de.biozentrum.bioinformatik.ca.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

/**
 * @author pseibel
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class StatusComponent extends JPanel {
	public static final int MESSAGE_STATUS = 3;
	public static final int NORMAL_STATUS = 0;
	public static final int PROGRESS_STATUS = 1;
	public static final int ERROR_STATUS = 2;
	
	private int status;
	
	private JLabel messageLabel;
	
	private JComponent contentComponent;
	private JComponent progressComponent;
	private JComponent errorComponent;
	private JComponent messageComponent;
	
	private JProgressBar progressBar;
	
	public StatusComponent() {
		
		
		messageLabel = new JLabel( "not available" );
		
		this.setLayout( new BorderLayout() );
		
		progressBar = new JProgressBar( 0, 100);
		progressComponent = new JPanel();
		progressComponent.setBackground( Color.WHITE );
		progressComponent.setLayout( new GridBagLayout() );
		progressComponent.add( progressBar, new GridBagConstraints( 0,0,1,1,0,0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets( 2,2,2,2), 0, 0));	
		errorComponent = new JPanel();
		errorComponent.setBackground( Color.WHITE );
		errorComponent.setLayout( new GridBagLayout() );
		errorComponent.add( new JLabel("unexpected error"), new GridBagConstraints( 0,0,1,1,0,0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets( 2,2,2,2), 0, 0));	
		
		messageComponent = new JPanel();
		messageComponent.setBackground( Color.WHITE );
		messageComponent.setLayout( new GridBagLayout() );
		messageComponent.add( messageLabel, new GridBagConstraints( 0,0,1,1,0,0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets( 2,2,2,2), 0, 0));	
		
		setStatus(MESSAGE_STATUS);
	}
	
	public void setContent( JComponent component ) {
		contentComponent = component;
	}
	
	public Dimension getPreferredSize() {
		if (contentComponent != null)
			return contentComponent.getPreferredSize();
		else
			return super.getPreferredSize();
	}
	
	public void setStatus( int status ) {
		removeAll();
		switch ( status ) {
			case NORMAL_STATUS:
				add( contentComponent, BorderLayout.CENTER );
				break;
			case PROGRESS_STATUS:
				add( progressComponent, BorderLayout.CENTER );
				break;
			case ERROR_STATUS:
				add( errorComponent, BorderLayout.CENTER );
				break;
			case MESSAGE_STATUS:
				add( messageComponent, BorderLayout.CENTER );
				break;
		}
		this.status = status;
		revalidate();
	}
	
	public void setMessage( String message ) {
		messageLabel.setText( message );
	}
	
	public void setIndeterminateProgress( boolean flag) {
		progressBar.setIndeterminate(flag);
	}
	
	public void setProgress( double percent ) {
		progressBar.setValue( (int)(percent * 100) );
	}	
}
