/*
 * Created on 05.01.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package de.biozentrum.bioinformatik.ca.gui;

import java.awt.Window;

import de.biozentrum.bioinformatik.ca.CAData;

/**
 * @author pseibel
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class CADataWindow extends Window {
	
	/**
	 * @param arg0
	 */
	public CADataWindow( Window window  ) {
		super( window );
	}

	/**
	 * Comment for <code>serialVersionUID</code>
	 */
	private static final long serialVersionUID = 1L;
	
	protected CAData data;

	public void setCAData( CAData data ) {
		this.data = data;
	}	
}
