/*
 * Created on 06.01.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package de.biozentrum.bioinformatik.ca.gui;

import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.geom.Point2D;

import de.biozentrum.bioinformatik.ca.CAData;

/**
 * @author pseibel
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface CAPointRenderer {
	
	public static final int NORMAL      = 0;
	public static final int HIGHLIGHTED = 1;
	public static final int MOUSE_OVER  = 2;
	public static final int SELECTED    = 3;
	public static final int DISABLED    = 4;
	public static final int ANNOTATION  = 5;
	/**
	 * @param g2d
	 * @param object
	 * @param data
	 * @param normal2
	 */
	public void setPixelArray( CAData[][] data );
	public void setDataDimension( int dataSetIdentifier, Dimension dimension );
	
	public void drawAt( Graphics2D g, Point2D point, CAData data );
	public void drawHighlightingAt( Graphics2D g, Point2D point, CAData data );
	public void drawSelectionAt( Graphics2D g, Point2D point, CAData data );
	public void annotateDataAt( Graphics2D g, Point2D point, CAData data );
}
