/*
 * Created on 16.01.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package de.biozentrum.bioinformatik.ca.gui;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GraphicsEnvironment;

import javax.swing.JLabel;
import javax.swing.JWindow;

import de.biozentrum.bioinformatik.ca.CAData;

/**
 * @author pseibel
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class CATooltip extends JWindow {
	
	/**
	 * Comment for <code>serialVersionUID</code>
	 */
	private static final long serialVersionUID = 3258128046614720568L;
	
	public CATooltip() {
		super( GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration() );
	}
	
	public void showCAData( CAData data, int x, int y ) {
		this.setLocation( x, y );
		Container c = this.getContentPane();
		c.removeAll();
		c.setLayout( new BorderLayout() );
		c.add( new JLabel( data.getName() ), BorderLayout.CENTER );
		this.pack();
		this.show();
	}
}
