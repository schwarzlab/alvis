/*
 * Created on 16.04.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package de.biozentrum.bioinformatik.ca;

import java.io.Serializable;
import org.jblas.DoubleMatrix;

/**
 * @author binf024
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface CADataProvider extends Serializable {
	public DoubleMatrix getDataMatrix();
}
