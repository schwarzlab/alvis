/*
 * Created on 05.01.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package de.biozentrum.bioinformatik.ca;

import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import de.biozentrum.bioinformatik.ca.annotation.CAAnnotation;
import de.biozentrum.bioinformatik.ca.annotation.CAAnnotationProvider;
import de.biozentrum.bioinformatik.ca.annotation.CADefaultAnnotationProvider;
import org.jblas.DoubleMatrix;

/**
 * @author pseibel
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class CADefaultModel implements CADataModel {

	protected int[] components;
	protected DoubleMatrix[] matrices;
	protected CAData[][] dataSets;
	protected ArrayList[] selection;
	protected ArrayList[] highlighting;
	
	protected ArrayList<CADataListener> dataListeners;
	protected ArrayList<CAComponentListener> componentListeners;
	
	private transient CAAnnotationProvider annotationProvider;
	private DoubleMatrix information;
	private DoubleMatrix singularValues;
	private DoubleMatrix backupFF;
	private DoubleMatrix backupGG;
	
	protected double[][] componentValueRanges;
	
	public CADefaultModel( CorrespondenceAnalysis analysis ) {
		matrices = new DoubleMatrix[ 2 ];
		dataSets = new CAData[ 2 ][];
		annotationProvider = new CADefaultAnnotationProvider();
		
		componentValueRanges = new double[ Math.min( analysis.getFF().rows, analysis.getGG().rows )][ 2];

		setMatrix( FIRST_DATA_SET, analysis.getFF() );
		setMatrix( SECOND_DATA_SET, analysis.getGG() );

		backupFF=new DoubleMatrix();
		backupFF.copy(analysis.getFF());
		backupGG=new DoubleMatrix();
		backupGG.copy(analysis.getGG());
		
		information = analysis.getInformation();
		dataListeners = new ArrayList();
		componentListeners = new ArrayList();
		singularValues=analysis.getSingularValues();
		
		selection = new ArrayList[] { new ArrayList(), new ArrayList() };
		highlighting = new ArrayList[] { new ArrayList(), new ArrayList() };
		components = new int[] { 0, 1 };
		this.setScaling(ScalingOptions.SPECIES);
	}

	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.diplomarbeit.ca.CAModel#getValues(int, int)
	 */
	public double[] getValues( int dataSetIdentifier, int position ) {
		double[] values = new double[ matrices[ dataSetIdentifier ].columns ];
		
		for ( int i = 0; i < values.length; i++ ) {
			values[i] = matrices[ dataSetIdentifier ].get(position, i );
		}
		
		return values;
	}
	
	
	public CAData[] getDataInRect( int dataSetIdentifier, Rectangle2D rect ) {
		
		List<CAData> list = new ArrayList<CAData>();
		for ( int i = 0; i < dataSets[dataSetIdentifier].length; i++ ) {
			
			double x = dataSets[dataSetIdentifier][i].getValues()[0];
			double y = dataSets[dataSetIdentifier][i].getValues()[1];
			if ( rect.contains( x, y ) ) {
				list.add( this.getData( dataSetIdentifier, i ) );
			}
		}
		return list.toArray( new CAData[0] );
	}

	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.diplomarbeit.ca.CAModel#setMatrix(int, javax.vecmath.GMatrix)
	 */
	public void setMatrix(int dataSetIdentifier, DoubleMatrix matrix) {
		matrices[ dataSetIdentifier ] = matrix; 
		dataSets[ dataSetIdentifier ] = new CADefaultData[ matrix.rows ];
		
		for ( int i = 0; i < matrix.rows; i++ ) {
			for ( int j = 0; j < matrix.columns; j++ ) {
				double entry = matrix.get( i,j );
				if ( ( componentValueRanges.length > j ) && entry > componentValueRanges[j][1] )
					componentValueRanges[j][1] = entry;
				if ( ( componentValueRanges.length > j ) && entry < componentValueRanges[j][0] )
					componentValueRanges[j][0] = entry;
			}
			dataSets[ dataSetIdentifier ][ i ] = new CADefaultData( i, dataSetIdentifier );
		}
	}

	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.diplomarbeit.ca.CAModel#getMatrix(int)
	 */
	public DoubleMatrix getMatrix(int dataSetIdentifier) {
		return matrices[ dataSetIdentifier ];
	}

	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.diplomarbeit.ca.CAModel#getDataSet(int)
	 */
	public CAData[] getDataSet( int dataSetIdentifier ) {
		if ( dataSetIdentifier < 2 )
			return dataSets[ dataSetIdentifier ];
		
		CAData[] bothSets = new CAData[ dataSets[ 0 ].length + dataSets[ 1 ].length ];
		
		System.arraycopy( dataSets[ 0 ], 0, bothSets, 0, dataSets[ 0 ].length );
		System.arraycopy( dataSets[ 1 ], 0, bothSets, dataSets[ 0 ].length, dataSets[ 1 ].length );
		
		return bothSets;
	}

	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.diplomarbeit.ca.CAModel#setComponents(int[])
	 */
	public void setComponents(int[] components) {
		int[] oldComponents = this.components;
		this.components = components;
		fireComponentChanged( oldComponents, components );
	}

	public void setScaling(ScalingOptions option) {
		DoubleMatrix speciesMatrix=new DoubleMatrix();
		speciesMatrix.copy(backupFF);
		DoubleMatrix siteMatrix=new DoubleMatrix();
		siteMatrix.copy(backupGG);
		
		//System.out.println(this.singularValues);
		//System.out.println("Site matrix:" + siteMatrix.rows()+ " X " + siteMatrix.columns());
		//System.out.println("Species matrix:" + speciesMatrix.rows()+ " X " + speciesMatrix.columns());
		
		
		switch(option) {
		case SPECIES:
			for (int i = 0;i<speciesMatrix.rows;i++) {
				for (int j = 0; j< singularValues.length; j++) {
					speciesMatrix.put(i, j, speciesMatrix.get(i, j) / Math.sqrt(this.singularValues.get(j)));
				}
			}
			break;
		case SITES:
			for (int i = 0;i<siteMatrix.rows;i++) {
				for (int j = 0; j< singularValues.length; j++) {
					siteMatrix.put(i, j, siteMatrix.get(i, j) / Math.sqrt(this.singularValues.get(j)));
				}
			}
			break;
		case BOTH:
			for (int i = 0;i<speciesMatrix.rows;i++) {
				for (int j = 0; j< singularValues.length; j++) {
					speciesMatrix.put(i, j, speciesMatrix.get(i, j) / Math.sqrt(Math.sqrt(this.singularValues.get(j))));
				}
			}
			for (int i = 0;i<siteMatrix.rows;i++) {
				for (int j = 0; j< singularValues.length; j++) {
					siteMatrix.put(i, j, siteMatrix.get(i, j) / Math.sqrt(Math.sqrt(this.singularValues.get(j))));
				}
			}
			break;
		}
		
		setMatrix(FIRST_DATA_SET, speciesMatrix);
		setMatrix(SECOND_DATA_SET, siteMatrix);
		
		fireComponentChanged(getComponents(), getComponents());
		
	} 

	/**
	 * @param model
	 * @param oldComponents
	 * @param newComponents
	 */
	protected void fireComponentChanged( int[] oldComponents, int[] newComponents ) {
		CAComponentChangedEvent e = new CAComponentChangedEvent( this, oldComponents, newComponents );
		Iterator it = componentListeners.iterator();
		while ( it.hasNext() ) {
			(( CAComponentListener ) it.next()).componentsChanged( e );
		}
	}

	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.diplomarbeit.ca.CAModel#getValue(int, int, int)
	 */
	public double getValue(int dataSetIdentifier, int position, int component ) {
		// TODO Auto-generated method stub
		return matrices[ dataSetIdentifier ].get( position, component );
	}

	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.diplomarbeit.ca.CAModel#getComponents()
	 */
	public int[] getComponents() {
		return components;
	}

	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.diplomarbeit.ca.CAModel#minValueForComponent(int)
	 */
	public double minValueForComponent(int component) {
		return componentValueRanges[ component ][ 0 ];
	}

	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.diplomarbeit.ca.CAModel#maxValueForComponent(int)
	 */
	public double maxValueForComponent(int component) {
		return componentValueRanges[ component ][ 1 ];
	}
	
	protected void fireSelectionChangedEvent( CADataChangedEvent e ) {
		Iterator it = dataListeners.iterator();
		while ( it.hasNext() ) {
			(( CADataListener ) it.next()).selectionChanged( e );
		}
	}
	
	protected void fireSelectedData( CAData[] data ) {
		CADataChangedEvent e = new CADataChangedEvent( this, data, CADataChangedEvent.INSERTED );
		fireSelectionChangedEvent( e );
	}
	
	protected void fireDeselectedData( CAData[] data ) {
		CADataChangedEvent e = new CADataChangedEvent( this, data, CADataChangedEvent.REMOVED );
		fireSelectionChangedEvent( e );
	}
	
	
	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.diplomarbeit.ca.CAModel#select(int, int)
	 */
	public void select( int dataSetIdentifier, int position ) {
		CAData data = (CAData) dataSets[ dataSetIdentifier ][ position ];
		if( !selection[dataSetIdentifier].contains( data ) ) {
			selection[dataSetIdentifier].add( data );
			fireSelectedData( new CAData[] { data } );
		}
		
	}

	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.diplomarbeit.ca.CAModel#select(int[], int[])
	 */
	public void select( int[] dataSetIdentifier, int[] positions ) {
		ArrayList mySelection = new ArrayList();
		
		for ( int i = 0; i < dataSetIdentifier.length; i++ ) {
			CAData data = (CAData) dataSets[ dataSetIdentifier[i] ][ positions[i] ];
			if( !selection[dataSetIdentifier[i]].contains( data ) ) {
				selection[dataSetIdentifier[i]].add( data );
				mySelection.add( data );
			}
		}
		fireSelectedData( (CAData[]) mySelection.toArray( new CAData[ 0 ] ) );
	}

	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.diplomarbeit.ca.CAModel#select(de.biozentrum.bioinformatik.diplomarbeit.ca.CAData[])
	 */
	public void select(CAData[] data) {
		List<CAData> mySelection = new ArrayList<CAData>();
		
		for ( int i = 0; i < data.length; i++ ) {
			int dataSetIdentifier = data[i].getDataSetIdentifier();
			if( !selection[dataSetIdentifier].contains( data[i] ) ) {
				selection[dataSetIdentifier].add( data[i] );
				mySelection.add( data[i] );
			}
		}
		fireSelectedData( (CAData[]) mySelection.toArray( new CAData[ 0 ] ) );
		
	}

	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.diplomarbeit.ca.CAModel#removeSelection()
	 */
	public void removeSelection( int dataSetIdentifier, int position ) {
		CAData data = (CAData) dataSets[ dataSetIdentifier ][ position ];
		if( selection[dataSetIdentifier].contains( data ) ) {
			selection[dataSetIdentifier].remove( data );
			fireDeselectedData( new CAData[] { data } );
		}	
	}

	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.diplomarbeit.ca.CAModel#removeSelection(int[], int[])
	 */
	public void removeSelection(int[] dataSetIdentifier, int[] positions) {
		List<CAData> mySelection = new ArrayList<CAData>();
		
		for ( int i = 0; i < dataSetIdentifier.length; i++ ) {
			CAData data = (CAData) dataSets[ dataSetIdentifier[i] ][ positions[i] ];
			if( !selection[dataSetIdentifier[i]].contains( data ) ) {
				selection[dataSetIdentifier[i]].remove( data );
				mySelection.add( data );
			}
		}
		fireDeselectedData( (CAData[]) mySelection.toArray( new CAData[ 0 ] ) );
	}

	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.diplomarbeit.ca.CAModel#removeSelection(de.biozentrum.bioinformatik.diplomarbeit.ca.CAData[])
	 */
	public void removeSelection(CAData[] data) {
		List<CAData> mySelection = new ArrayList<CAData>();
		
		for ( int i = 0; i < data.length; i++ ) {
			int dataSetIdentifier = data[i].getDataSetIdentifier();
			if( !selection[dataSetIdentifier].contains( data[i] ) ) {
				selection[dataSetIdentifier].remove( data[i] );
				mySelection.add( data[i] );
			}
		}
		fireDeselectedData( (CAData[]) mySelection.toArray( new CAData[ 0 ] ) );
	}

	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.diplomarbeit.ca.CAModel#clearSelection()
	 */
	public void clearSelection() {
		CAData[] data = getSelectedData();
		selection[0].clear();
		selection[1].clear();
		fireDeselectedData( data );
	}

	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.diplomarbeit.ca.CAModel#getSelectedData()
	 */
	public CAData[] getSelectedData() {
		ArrayList sel = new ArrayList();
		sel.addAll( selection[0] );
		sel.addAll( selection[1] );
		CAData[] data = (CAData[])sel.toArray( new CAData[0] );
		return data;
	}

	public void addCADataListener( CADataListener listener ) {
		if ( !dataListeners.remove( listener ) )
			dataListeners.add( listener );
	}
	
	public void removeCADataListener( CADataListener listener ) {
		dataListeners.remove( listener );
	}


	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.diplomarbeit.ca.CAModel#addCAComponentListener(de.biozentrum.bioinformatik.diplomarbeit.ca.CAComponentListener)
	 */
	public void addCAComponentListener(CAComponentListener listener) {
		if ( !componentListeners.contains( listener ) )
			componentListeners.add( listener );
	}

	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.diplomarbeit.ca.CAModel#removeCAComponentListener(de.biozentrum.bioinformatik.diplomarbeit.ca.CAComponentListener)
	 */
	public void removeCAComponentListener(CAComponentListener listener) {
		componentListeners.remove( listener );
	}

	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.diplomarbeit.ca.CAModel#numberOfComponents()
	 */
	public int numberOfComponents() {
		return Math.min( matrices[0].columns, matrices[1].columns );
	}

	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.diplomarbeit.ca.CAModel#sizeOfDataSet(int)
	 */
	public int sizeOfDataSet(int dataSetIdentifer) {
		if ( dataSetIdentifer < 2 )
			return dataSets[dataSetIdentifer].length;
		else if ( dataSetIdentifer == CADataModel.BOTH_DATA_SETS )
			return sizeOfDataSet( 0 ) + sizeOfDataSet( 1 );
		return 0;
	}
	
	class CADefaultData implements CAData {
		
		private int dataSetIdentifier;
		private int position;
		private String name;
		
		public CADefaultData( String name, int position, int dataSet ) {
			this.name = name;
			this.position = position;
			this.dataSetIdentifier = dataSet;
		}
		
		public CADefaultData( int position, int dataSet ) {
			this.name = dataSet + "_" + position;
			this.position = position;
			this.dataSetIdentifier = dataSet;
		}
		
		public void select() {
			CADefaultModel.this.select( dataSetIdentifier, position );
		} 
		
		public void deselect() {
			removeSelection( dataSetIdentifier, position );
		}
		
		public boolean isSelected() {
			return CADefaultModel.this.selection[dataSetIdentifier].contains( this );
		}
		
		public String getName() {
			return (String) CADefaultModel.this.annotationProvider.getAnnotation( this, CAAnnotation.NAME );
		}
		
		public int getDataSetIdentifier() {
			return dataSetIdentifier;
		}
		
		public double[] getValues() {
			int[] components = getComponents();
			double[] values = CADefaultModel.this.getValues( dataSetIdentifier, position );
			double[] reducedValues = new double[ components.length ];
			
			for ( int i = 0; i < components.length; i++ ) {
				reducedValues[ i ] = values[ components[ i ] ];
			}
			return reducedValues;
		}

		/**
		 * @return
		 */
		public String identifier() {
			return dataSetIdentifier + "-" + position;
		}
		
		public int getPosition() {
			return position;
		}

		/* (non-Javadoc)
		 * @see de.biozentrum.bioinformatik.ca.CAData#getAnnotation(java.lang.String)
		 */
		public Object getAnnotation(String key) {
			return CADefaultModel.this.annotationProvider.getAnnotation( this, key );
		}

		/* (non-Javadoc)
		 * @see de.biozentrum.bioinformatik.ca.CAData#getAnnotationLabel(java.lang.String)
		 */
		public String getAnnotationLabel(String key) {
			return CADefaultModel.this.annotationProvider.getLabel( key );
		}

		/* (non-Javadoc)
		 * @see de.biozentrum.bioinformatik.ca.CAData#getAnnotationClass(java.lang.String)
		 */
		public Class getAnnotationClass(String key) {
			return CADefaultModel.this.annotationProvider.getClass( key );
		}

		/* (non-Javadoc)
		 * @see de.biozentrum.bioinformatik.ca.CAData#getAnnotationKeys()
		 */
		public String[] getAnnotationKeys() {
			return CADefaultModel.this.annotationProvider.getKeys( this.dataSetIdentifier );
		}
	}

	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.ca.CADataModel#setAnnotation(de.biozentrum.bioinformatik.ca.annotation.CAAnnotationProvider)
	 */
	public void setAnnotation(CAAnnotationProvider annotationProvider) {
		this.annotationProvider = annotationProvider;
	}

	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.ca.CADataModel#getAnnotation()
	 */
	public CAAnnotationProvider getAnnotation() {
		return annotationProvider;
	}

	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.ca.CADataModel#getData(int, int)
	 */
	public CAData getData(int dataSetIdentifier, int position) {
		return dataSets[dataSetIdentifier][position];
	}

	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.ca.CADataModel#getSelectedData(int)
	 */
	public CAData[] getSelectedData(int dataSetIdentifier) {
		switch( dataSetIdentifier ) {
			case FIRST_DATA_SET:
			case SECOND_DATA_SET:
				return (CAData[])selection[dataSetIdentifier].toArray( new CAData[0] );
			default:
				return getSelectedData();
		}
	}

	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.ca.CADataModel#getComponentInformations()
	 */
	public DoubleMatrix getComponentInformations() {
		return information;
	}

	public void beginSelectionTransaction() {
		Iterator it = dataListeners.iterator();
		while ( it.hasNext() ) {
			(( CADataListener ) it.next()).selectionTransactionWillBegin();
		}
		
	}

	public void commitSelectionTransaction() {
		Iterator it = dataListeners.iterator();
		while ( it.hasNext() ) {
			(( CADataListener ) it.next()).selectionTransactionDidCommit();
		}
	}

}
