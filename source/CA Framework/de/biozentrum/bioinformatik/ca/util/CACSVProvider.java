package de.biozentrum.bioinformatik.ca.util;

import java.io.IOException;

import de.biozentrum.bioinformatik.ca.CAData;
import de.biozentrum.bioinformatik.ca.CADataModel;
import de.biozentrum.bioinformatik.ca.CADataProvider;
import de.biozentrum.bioinformatik.ca.annotation.CAAnnotation;
import de.biozentrum.bioinformatik.ca.annotation.CAAnnotationProvider;
import org.jblas.DoubleMatrix;

public class CACSVProvider extends CSVDoubleReader implements CAAnnotationProvider, CADataProvider {

	
	public CACSVProvider(String filename, String seperator) throws IOException {
		super(filename, seperator);
	}

	public Object getAnnotation(CAData data, String key) {
		if ( key.equals( CAAnnotation.NAME ) ) {
			switch ( data.getDataSetIdentifier() ) {
				case CADataModel.FIRST_DATA_SET:
					return rowHeader[data.getPosition()];
				case CADataModel.SECOND_DATA_SET:
					return colHeader[data.getPosition()];
			}
		}
		else if ( key.equals( CAAnnotation.IDENTIFIER ) ) {
			return data.getDataSetIdentifier() + "-" + data.getPosition();
		}
		return null;
	}

	public Class getClass(String key) {
		if ( key.equals( CAAnnotation.NAME ) )
			return String.class;
		if ( key.equals( CAAnnotation.IDENTIFIER ) )
			return String.class;
		return null;
	}

	public String getLabel(String key) {
		if ( key.equals( CAAnnotation.IDENTIFIER )) {
			return "Identifier";
		}
		else if ( key.equals( CAAnnotation.NAME )) {
			return "Name";
		}
		return null;
	}

	public String getLabel(int dataSetIdentifier) {
		switch ( dataSetIdentifier ) {
		case 0:
			return "Row data";
		case 1:
			return "Column data";
			
		}
		return null;
	}

	public String[] getKeys(int dataSetIdentifier) {
		return new String[]{CAAnnotation.IDENTIFIER,CAAnnotation.NAME};
	}

	public DoubleMatrix getDataMatrix() {
		return doubledataMatrix;
	}

}
