package de.biozentrum.bioinformatik.ca.util;

import java.io.File;
import java.io.IOException;
import org.jblas.DoubleMatrix;

public class CSVDoubleReader {
	protected String[] rowHeader;

	protected String[] colHeader;

	protected DoubleMatrix doubledataMatrix;

	protected String filename;

	protected String seperator;

	public CSVDoubleReader(String filename, String seperator)
			throws IOException {
		this.filename = filename;
		this.seperator = seperator;
		read();
	}

	public void read() throws IOException {

		CSVReader csvReader = new CSVReader( new File(filename), seperator, true, true);
		String[][] mat = csvReader.readCSV();
		
		rowHeader = csvReader.getRowHeader();
		colHeader = csvReader.getColumnHeader();
		
		
		
		doubledataMatrix = new DoubleMatrix( mat.length, mat[0].length);

		for ( int i = 0; i < mat.length; i++) {
			for ( int j = 0; j < mat[0].length; j++) {
				doubledataMatrix.put(i,j,Double.valueOf( mat[i][j]).doubleValue());
			}
		}
	}

	public String[] getColHeader() {
		return colHeader;
	}

	public void setColHeader(String[] colHeader) {
		this.colHeader = colHeader;
	}

	public DoubleMatrix getDoubleDataMatrix() {
		return doubledataMatrix;
	}

	public void setDoubleDataMatrix(DoubleMatrix dataMatrix) {
		this.doubledataMatrix = dataMatrix;
	}

	public String[] getRowHeader() {
		return rowHeader;
	}

	public void setRowHeader(String[] rowHeader) {
		this.rowHeader = rowHeader;
	}

}
