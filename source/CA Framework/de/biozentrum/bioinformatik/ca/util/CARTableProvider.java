package de.biozentrum.bioinformatik.ca.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import de.biozentrum.bioinformatik.ca.CAData;
import de.biozentrum.bioinformatik.ca.CADataModel;
import de.biozentrum.bioinformatik.ca.CADataProvider;
import de.biozentrum.bioinformatik.ca.annotation.CAAnnotation;
import de.biozentrum.bioinformatik.ca.annotation.CAAnnotationProvider;
import org.jblas.DoubleMatrix;

public class CARTableProvider implements CAAnnotationProvider, CADataProvider {

	private String[] rowHeader;
	private String[] colHeader;
	private DoubleMatrix dataMatrix;
	
	public CARTableProvider( String filename, String seperator ) throws IOException {
		BufferedReader reader = new BufferedReader( new FileReader( filename ) );
		String columns = reader.readLine();
		int m = columns.split( seperator ).length;
		int n = 0;
		while ( reader.readLine() != null )
			n++;
		reader.close();
		
		
		rowHeader = new String[n];
		colHeader = new String[m];
		dataMatrix = new DoubleMatrix( n,m);
		
		reader = new BufferedReader( new FileReader( filename ) );
		String[] tmpColHeader = reader.readLine().split( seperator );
		for ( int i = 0; i < tmpColHeader.length; i++ ) {
			colHeader[ i] = tmpColHeader[i];
		}
		
		String line = reader.readLine();
		int lineCount = 0;
		
		while ( line != null ) {
			String[] tmpRow = line.split( seperator );
			rowHeader[lineCount] = tmpRow[0];
			
			for ( int i = 1; i < tmpRow.length; i++ ) {
				double d =  new Double(tmpRow[i]).doubleValue();
				//if (d > 0)
					//d = 1;
					
				dataMatrix.put( lineCount, i-1, d );
			}
			
			line = reader.readLine();
			lineCount++;
		}
		reader.close();
	}
	
	public Object getAnnotation(CAData data, String key) {
		if ( key.equals( CAAnnotation.NAME ) ) {
			switch ( data.getDataSetIdentifier() ) {
				case CADataModel.FIRST_DATA_SET:
					return rowHeader[data.getPosition()];
				case CADataModel.SECOND_DATA_SET:
					return colHeader[data.getPosition()];
			}
		}
		else if ( key.equals( CAAnnotation.IDENTIFIER ) ) {
			return data.getDataSetIdentifier() + "-" + data.getPosition();
		}
		return null;
	}

	public Class getClass(String key) {
		if ( key.equals( CAAnnotation.NAME ) )
			return String.class;
		if ( key.equals( CAAnnotation.IDENTIFIER ) )
			return String.class;
		return null;
	}

	public String getLabel(String key) {
		if ( key.equals( CAAnnotation.IDENTIFIER )) {
			return "Identifier";
		}
		else if ( key.equals( CAAnnotation.NAME )) {
			return "Name";
		}
		return null;
	}

	public String getLabel(int dataSetIdentifier) {
		switch ( dataSetIdentifier ) {
		case 0:
			return "Row data";
		case 1:
			return "Column data";
			
		}
		return null;
	}

	public String[] getKeys(int dataSetIdentifier) {
		return new String[]{CAAnnotation.IDENTIFIER,CAAnnotation.NAME};
	}

	public DoubleMatrix getDataMatrix() {
		return dataMatrix;
	}

}
