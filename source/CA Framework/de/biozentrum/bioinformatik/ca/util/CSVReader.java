package de.biozentrum.bioinformatik.ca.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class CSVReader {
	private File file;
	private String seperator;
	
	private String[] 	columnHeader;
	private String[] 	rowHeader;
	
	private boolean hasRowHeaders;
	private boolean hasColumnHeaders;
	
	public CSVReader( File file, String seperator, boolean hasRowHeaders, boolean hasColumnHeaders) {
		this.file = file;
		this.seperator = seperator;
		
		this.hasRowHeaders = hasRowHeaders;
		this.hasColumnHeaders = hasColumnHeaders;
	}
	
	public String[] getColumnHeader() {
		return columnHeader;
	}
	
	public String[] getRowHeader() {
		return rowHeader;
	}
	
	
	public String[][] readCSV() throws IOException {
		BufferedReader reader = new BufferedReader( new FileReader( file));
		String columns = reader.readLine();
		int m = columns.split(seperator).length;
		int n = 0;
		while (reader.readLine() != null)
			n++;

		rowHeader = new String[n];
		columnHeader = new String[m];
		String[][] matrix = new String[n][m];
		reader.close();
		
		reader = new BufferedReader( new FileReader( file));
		if ( hasColumnHeaders) {
			String[] tmpColHeader = reader.readLine().split(seperator);
			for (int i = 0; i < tmpColHeader.length; i++) {
				columnHeader[i] = tmpColHeader[i];
			}
		}

		String line = reader.readLine();
		int lineCount = 0;

		while (line != null) {
			String[] tmpRow = line.split(seperator);
			int i = 0;
			if ( hasRowHeaders) {
				rowHeader[lineCount] = tmpRow[0];
				i = 1;
			}

			for (; i < tmpRow.length; i++) {
				String d = "";
				try {
					d = new String(tmpRow[i]);
				} catch (NumberFormatException nfo) {
					System.out.println(tmpRow[i]);
					System.exit(0);
				}
				// if (d > 0)
				// d = 1;
				if ( hasRowHeaders) {
					matrix[lineCount][i-1] = d;
				}
				else {
					matrix[lineCount][i] = d;
				}
				
			}

			line = reader.readLine();
			lineCount++;
		}
		reader.close();
		return matrix;
	}
}
