package de.biozentrum.bioinformatik.ca.ext.selection;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.AbstractTableModel;

import de.biozentrum.bioinformatik.ca.CAData;
import de.biozentrum.bioinformatik.ca.CADataChangedEvent;
import de.biozentrum.bioinformatik.ca.CADataListener;
import de.biozentrum.bioinformatik.ca.CADataModel;
import de.biozentrum.bioinformatik.ca.ext.CAExtension;

public class SelectionExtension implements CAExtension {

	private JTable table;
	private CADataModel model;
	private SelectionTableModel tableModel;
	private JScrollPane scrollPane;
	
	public SelectionExtension() {
		super();
		tableModel = new SelectionTableModel();
		table = new JTable( tableModel);
		table.addKeyListener( new KeyListener() {

			public void keyPressed(KeyEvent e) {
				if ( e.getKeyCode() == KeyEvent.VK_BACK_SPACE
						|| e.getKeyCode() == KeyEvent.VK_DELETE) {
					
					ListSelectionModel selModel = table.getSelectionModel();
					List<CAData> toRemove = new ArrayList<CAData>();
					for ( int i = selModel.getMinSelectionIndex(); i <= selModel.getMaxSelectionIndex(); i++) {
						if ( selModel.isSelectedIndex(i)) {
							toRemove.add( model.getSelectedData()[i]);
						}
					}
					
					for ( CAData d : toRemove) {
						d.deselect();
					}
					

				}
				
			}

			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}

			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}});
		scrollPane = new JScrollPane(table);
	}
	
	public JComponent getComponent() {
		return scrollPane;
	}

	public String getName() {
		return "Selection";
	}

	public void setModel(CADataModel model) {
		this.model = model;
		model.addCADataListener( tableModel);
	}
	
	private class SelectionTableModel extends AbstractTableModel implements CADataListener {

		private boolean inSelectionTransaction = false;
		
		public String getColumnName(int col) {
			 if ( col == 0)
				 return "Type";
			 return "Name";

		 }
		
		public int getColumnCount() {
			return 2;
		}

		public int getRowCount() {
			if ( model == null)
				return 0;
			
			return model.getSelectedData().length;
		}

		public Object getValueAt(int rowIndex, int columnIndex) {
			CAData data = model.getSelectedData()[rowIndex];
			if ( columnIndex == 0)
				return (data.getDataSetIdentifier()==0) ? "Seq" : "Site";
			
			return data.getName();
		}

		public void selectionChanged(CADataChangedEvent e) {
			if ( inSelectionTransaction) return;
			fireTableDataChanged();
		}

		public void selectionTransactionDidCommit() {
			fireTableDataChanged();
			inSelectionTransaction = false;
			
		}

		public void selectionTransactionWillBegin() {
			inSelectionTransaction = true;
			
		}
	}
}
