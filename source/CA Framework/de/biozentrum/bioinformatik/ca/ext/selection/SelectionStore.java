package de.biozentrum.bioinformatik.ca.ext.selection;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;

import de.biozentrum.bioinformatik.ca.CAData;
import de.biozentrum.bioinformatik.ca.CADataModel;

public class SelectionStore implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 541326016135262468L;

	private List<Selection> selections;
	private HashMap<Character, Selection> hash;
	private transient CADataModel model;
	private transient SelectionTableModel tableModel;
	
	class SelectionTableModel extends AbstractTableModel {

		public String getColumnName(int col) {
			 if ( col == 0)
				 return "Key";
			 if ( col == 1) 
				 return "Name";
			 if ( col == 2)
				 return "Seq's";
			 return "Sites";
		 }
		
		public int getColumnCount() {
			return 4;
		}

		public int getRowCount() {
			return size();
		}

		public Object getValueAt(int rowIndex, int columnIndex) {
			Selection s = getSelection(rowIndex);
			switch (columnIndex) {
			case 0:
				return s.getKey();
			case 1:
				return s.getName();
			case 2:
				return new Integer(s.getSequenceCount());
			default:
				return new Integer(s.getSiteCount());
			}
		}
		
		public boolean isCellEditable(int row, int col) { 
			return col==1; 
		}
		public void setValueAt(Object value, int row, int col) {
			if (col == 1) {
				Selection s = getSelection(row);
				s.setName( (String)value);
				fireTableCellUpdated(row, col);
			}
		}
		
	}
	
	public SelectionStore() {
		this.selections = new ArrayList<Selection>();
		this.hash = new HashMap<Character, Selection>();
		this.tableModel = new SelectionTableModel();
	}
	
	   private void readObject(
			     ObjectInputStream aInputStream
			   ) throws ClassNotFoundException, IOException {
			     //always perform the default de-serialization first
			     aInputStream.defaultReadObject();
			     this.tableModel = new SelectionTableModel(); 
	}
	
	
	
	
	
	

	
	
	public int size() {
		return this.selections.size();
	}
	
	
	
	public int indexForKey( char key) {
		return selections.indexOf(hash.get(key));
	}
	
	public void remove( int index) {
		selections.remove(index);
		tableModel.fireTableRowsDeleted(index, index);
	}
	
	public Selection getSelection( int index) {
		return selections.get( index);
	}
	
	private void addSelection( Selection sel) {
		if ( hash.containsKey(sel.getKey())) {
			Selection s = hash.get( sel.getKey());
			int index = selections.indexOf( s);
			if ( index > -1) {
				remove( index);
			}
		}
		if ( sel.selections[0].length > 0 
				|| sel.selections[1].length > 0 ) {
			selections.add( sel);
			Collections.sort(selections, new Comparator<Selection>() {

				public int compare(Selection o1, Selection o2) {
					return (new Character(o1.getKey())).compareTo(o2.getKey());
				}
			});
			
			hash.put( sel.getKey(), sel);
			int index = selections.size()-1;
			tableModel.fireTableRowsInserted(index, index);
		}
	}
	
	public TableModel getTableModel() {
		return tableModel;
	}
	
	public void addSelection( String name, char key) {
		Selection sel = new Selection(name, key);
		this.addSelection(sel);
	}
	
	public class Selection implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = -2619702800941119716L;
		
		private int[][] selections;
		private String name;
		private char key;
		private transient CAData[][] selectionData;
		
		private int[] getPositions( CAData[] data) {
			int[] tmp = new int[data.length];
			for ( int i = 0; i < data.length; i++) {
				tmp[i] = data[i].getPosition();
			} 
			return tmp;
		}
		
		private CAData[] getData( int dataSetIdentifier, int[] positions) {
			CAData[] tmp = new CAData[positions.length];
			for ( int i = 0; i < positions.length; i++) {
				tmp[i] = getModel().getData(dataSetIdentifier, positions[i]);
			}
			return tmp;
		}
		
		public Selection( String name, char key) {
			this.name = name;
			this.key = key;
			this.selections = new int[2][];
			this.selections[0] = getPositions(getModel().getSelectedData(0));
			this.selections[1] = getPositions(getModel().getSelectedData(1));
		}
		
		public void setName( String name) {
			this.name = name;
		}
		
		public char getKey() {
			return key;
		}
		
		public int getSiteCount() {
			return this.selections[1].length;
		}
		
		public int getSequenceCount() {
			return this.selections[0].length;
		}
		
		public CAData[][] getData() {
			if ( selectionData == null) {
				selectionData = new CAData[2][];
				selectionData[0] = getData(0,selections[0]);
				selectionData[1] = getData(1,selections[1]);
			}
			return selectionData;
		}
		
		public String getName() {
			return name;
		}
	}


	public void setModel(CADataModel model) {
		this.model = model;
	}


	public CADataModel getModel() {
		return model;
	}

}
