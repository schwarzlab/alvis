/*
 * Created on 17.02.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package de.biozentrum.bioinformatik.ca.ext.selection;

import java.awt.BorderLayout;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import de.biozentrum.bioinformatik.ca.CAData;
import de.biozentrum.bioinformatik.ca.CADataChangedEvent;
import de.biozentrum.bioinformatik.ca.CADataListener;
import de.biozentrum.bioinformatik.ca.CADataModel;
import de.biozentrum.bioinformatik.ca.ext.CAExtension;
import de.biozentrum.bioinformatik.ca.gui.CADataTableCellRenderer;

/**
 * @author binf024
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class CASelectionExtension extends JPanel implements  CAExtension {
	
	private CADataModel model;
	
	public CASelectionExtension() {
		setName("Selection");
		setLayout( new BorderLayout() );
	}

	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.ca.ext.CAExtension#setModel(de.biozentrum.bioinformatik.ca.CADataModel)
	 */
	public void setModel(CADataModel model) {
		this.model = model;
		if ( model == null )
			return;
		
		JSplitPane split = new JSplitPane( JSplitPane.VERTICAL_SPLIT );

		JTable first = new JTable( new CADataSelectionModel( model, CADataModel.FIRST_DATA_SET )  );
		first.setAutoResizeMode( JTable.AUTO_RESIZE_LAST_COLUMN );
		first.setTableHeader( null );
		first.setDefaultRenderer( CAData.class, new CADataTableCellRenderer() );
		TableColumn checkBoxCol = first.getColumnModel().getColumn( 0 );
		checkBoxCol.setMinWidth( 10 );
		checkBoxCol.setMaxWidth( 30 );
		checkBoxCol.setPreferredWidth( 20 );
		
		JTable second = new JTable( new CADataSelectionModel( model, CADataModel.SECOND_DATA_SET )  );
		second.setAutoResizeMode( JTable.AUTO_RESIZE_LAST_COLUMN );
		second.setTableHeader( null );
		second.setDefaultRenderer( CAData.class, new CADataTableCellRenderer() );
		checkBoxCol = second.getColumnModel().getColumn( 0 );
		checkBoxCol.setMinWidth( 10 );
		checkBoxCol.setMaxWidth( 30 );
		checkBoxCol.setPreferredWidth( 20 );
		
		split.add( new JScrollPane( first ), JSplitPane.TOP );
		split.add( new JScrollPane( second ), JSplitPane.BOTTOM );
		
		removeAll();
		add( split, BorderLayout.CENTER );
		revalidate();
	}

	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.ca.ext.CAExtension#getComponent()
	 */
	public JComponent getComponent() {
		return this;
	}
	
	private class CADataSelectionModel extends DefaultTableModel implements CADataListener {
		
		private CADataModel caModel;
		private int dataSetIdentifier;
		private boolean inSelectionTransaction;
		
		public CADataSelectionModel( CADataModel caModel, int dataSetIdentifier ) {
			CADataSelectionModel.this.caModel = caModel;
			caModel.addCADataListener( this );
			this.dataSetIdentifier = dataSetIdentifier;
		}
		
		public Class getColumnClass( int column ) {
			if ( column == 0 )
				return Boolean.class;	
			return CAData.class;
		}
		
		/* (non-Javadoc)
		 * @see javax.swing.table.TableModel#getRowCount()
		 */
		public int getRowCount() {
			if ( caModel == null ) {
				return 0;
			}
			return caModel.getSelectedData( dataSetIdentifier ).length;
		}

		/* (non-Javadoc)
		 * @see javax.swing.table.TableModel#getColumnCount()
		 */
		public int getColumnCount() {
			return 2;
		}

		/* (non-Javadoc)
		 * @see javax.swing.table.TableModel#isCellEditable(int, int)
		 */
		public boolean isCellEditable(int row, int column) {
			if ( column == 0 )
				return true;
			
			return false;
		}

		public void setValueAt( Object value, int row, int column ) {
			CAData data = caModel.getSelectedData( dataSetIdentifier )[ row ];
			
			if ( value instanceof Boolean ) {
				if ( ((Boolean)value).booleanValue() )
					data.select();
				else
					data.deselect();
			}
		}
		
		/* (non-Javadoc)
		 * @see javax.swing.table.TableModel#getValueAt(int, int)
		 */
		public Object getValueAt( int row, int column ) {
			CAData data = caModel.getSelectedData( dataSetIdentifier )[ row ];
			if ( column == 0 )
				return new Boolean( data.isSelected() );
			return data;
		}

		/* (non-Javadoc)
		 * @see de.biozentrum.bioinformatik.ca.CADataListener#selectionChanged(de.biozentrum.bioinformatik.ca.CADataChangedEvent)
		 */
		public void selectionChanged(CADataChangedEvent e) {
			if ( inSelectionTransaction) return;
			fireTableDataChanged();
		}

		public void selectionTransactionDidCommit() {
			fireTableDataChanged();
			inSelectionTransaction = false;
			
		}

		public void selectionTransactionWillBegin() {
			inSelectionTransaction = true;
			
		}
	}
	
}
