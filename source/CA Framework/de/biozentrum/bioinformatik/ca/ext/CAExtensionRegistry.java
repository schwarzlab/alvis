/*
 * Created on 17.04.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package de.biozentrum.bioinformatik.ca.ext;

import java.util.HashMap;
import java.util.Iterator;

import de.biozentrum.bioinformatik.ca.CADataModel;

/**
 * @author pseibel
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class CAExtensionRegistry {
	
	private CADataModel model;
	private HashMap extensions;
	
	public CAExtensionRegistry() {
		extensions = new HashMap( 0 );
	}
	
	public void add( CAExtension extension ) {
		extensions.put( extension.getName(), extension );
		if ( model != null ) {
			extension.setModel( model );
		}
	}
	
	public void setDataModel( CADataModel model ) {
		CAExtension[] ext = getExtensions();
		for ( int i = 0; i < ext.length; i++ ) {
			ext[i].setModel( model );
		}
		this.model = model;
	}
	
	public void remove( CAExtension extension ) {
		extensions.remove( extension.getName() );
		extension.setModel( null );
	}
	
	public CAExtension[] getExtensions() {
		return (CAExtension[])extensions.values().toArray( new CAExtension[ 0 ] );
	}
	
	public CAExtension getExtension( String name ) {
		return (CAExtension)extensions.get( name );
	}
	
	public Iterator iterator() {
		return extensions.values().iterator();
	}
}
