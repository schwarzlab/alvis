/*
 * Created on 15.04.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package de.biozentrum.bioinformatik.ca.ext;

import javax.swing.JComponent;

import de.biozentrum.bioinformatik.ca.CADataModel;

/**
 * @author binf024
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface CAExtension {
	public String getName();
	public void setModel( CADataModel model );
	public JComponent getComponent();
}
