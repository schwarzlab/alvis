/*
 * Created on 20.04.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package de.biozentrum.bioinformatik.ca.ext.alignment;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

import de.biozentrum.bioinformatik.alignment.MultipleAlignment;
import de.biozentrum.bioinformatik.alignment.MultipleAlignmentView;
import de.biozentrum.bioinformatik.alignment.selection.AlignmentSelectionModel;


/**
 * @author pseibel
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public class AlignmentComponent extends JPanel {

	private MultipleAlignment model;

	private JList names;

	private ListSelectionModel sequenceSelectionModel;

	private MultipleAlignmentView view;
	
	public AlignmentComponent(MultipleAlignment model) {
		this.model = model;
		this.setLayout(new BorderLayout());
		names = new JList(new NameModel());
		sequenceSelectionModel = names.getSelectionModel();
		view = new MultipleAlignmentView();
		view.setAlignment( model );
		names.setFixedCellHeight((int)view.sequenceToY(1));
		JScrollPane pane = new JScrollPane( view );
		pane.setColumnHeaderView( view.getConservationView() );
		pane.setBackground(Color.WHITE);
		pane.setRowHeaderView(names);
		this.add(pane, BorderLayout.CENTER);
	}
	
	public AlignmentSelectionModel getAlignmentSelectionModel() {
		return view.getSelectionModel();
	}
	
	public ListSelectionModel getSequenceSelectionModel() {
		return sequenceSelectionModel;
	}
	
	public ListSelectionModel getColumnSelectionModel() {
		return view.getColumnSelectionModel();
	}

	private class NameModel extends DefaultListModel {
		public int getSize() {
			return model.rows();
		}
 
		public Object getElementAt(int index) {
			return model.getSequence(index).getName();
		}
	}
}