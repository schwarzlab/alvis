/*
 * Created on 10.05.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package de.biozentrum.bioinformatik.ca.ext.alignment;

import de.biozentrum.bioinformatik.alignment.MultipleAlignment;
import org.biojava.bio.symbol.Alphabet;
import org.biojava.bio.symbol.AlphabetIndex;


/**
 * @author pseibel
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface MAExtension {
	public void setAlignment( MultipleAlignment model, AlphabetIndex alphabetIndex, Alphabet alphabet );
}
