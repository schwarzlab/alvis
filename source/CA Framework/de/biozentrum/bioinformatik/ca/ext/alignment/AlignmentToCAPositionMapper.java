/*
 * Created on 17.04.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package de.biozentrum.bioinformatik.ca.ext.alignment;

import alvis.algorithm.matrix.FisherScoresMatrix;
import com.general.containerModel.impl.MapDefaultValueDecorator;
import com.general.utils.GUIutils;
import de.biozentrum.bioinformatik.alignment.MultipleAlignment;
import de.biozentrum.bioinformatik.alignment.events.AlignmentSelectionEvent;
import de.biozentrum.bioinformatik.alignment.events.AlignmentSelectionListener;
import de.biozentrum.bioinformatik.alignment.selection.AlignmentSelectionModel;
import de.biozentrum.bioinformatik.ca.CAData;
import de.biozentrum.bioinformatik.ca.CADataChangedEvent;
import de.biozentrum.bioinformatik.ca.CADataListener;
import de.biozentrum.bioinformatik.ca.CADataModel;
import de.biozentrum.bioinformatik.ca.ext.CAExtension;
import de.biozentrum.bioinformatik.sequence.Sequence;
import java.awt.Rectangle;
import java.awt.geom.Area;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComponent;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.biojava.bio.BioException;
import org.biojava.bio.symbol.Symbol;

/**
 * @author pseibel
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class AlignmentToCAPositionMapper implements CAExtension, CADataListener, ListSelectionListener, MAHMMPositionMap, AlignmentSelectionListener {
	
	private final ListSelectionModel sequenceSelectionModel;
//	private ListSelectionModel columnSelectionModel;
	private CADataModel caModel;
//	private CAMASelectionConverter converter;
	private final AlignmentSelectionModel alignmentSelectionModel;
	private boolean[] columnMask;
	private boolean updatingSequenceSelection=false;
	private final FisherScoresMatrix fisherScores;
	private final MultipleAlignment alignment;
	private boolean updatingAlignmentSelection;
	private boolean updatingCASelection;
	private Area alignmentSelectionChangedArea = new Area();
	
	public AlignmentToCAPositionMapper(MultipleAlignment alignment, FisherScoresMatrix fisherScores, ListSelectionModel sequenceSelectionModel, AlignmentSelectionModel alignmentSelectionModel) {
		this.alignment = alignment;
		this.fisherScores = fisherScores;
		this.alignmentSelectionModel = alignmentSelectionModel;
		this.sequenceSelectionModel = sequenceSelectionModel;
		this.sequenceSelectionModel.addListSelectionListener(this);
		this.alignmentSelectionModel.addAlignmentSelectionListener(this);
	}
	
	public void setColumnMask( boolean[] columnMask) {
		this.columnMask = columnMask;
	}
	
	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.ca.CADataProvider#getDataMatrix()
	 */
	

	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.ca.ext.CAExtension#getName()
	 */
	public String getName() {
		return "Alignment View";
	}

	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.ca.ext.CAExtension#setModel(de.biozentrum.bioinformatik.ca.CADataModel)
	 */
	public void setModel(CADataModel model) {
		caModel = model;
		caModel.addCADataListener( this );
//		converter = new CAMASelectionConverter( model, alignmentSelectionModel, sequenceSelectionModel, columnSelectionModel, alphabetIndex, alphabet, this );
	}

	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.ca.ext.CAExtension#getComponent()
	 */
	public JComponent getComponent() {
		return null;
	}

	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.ca.CADataListener#selectionChanged(de.biozentrum.bioinformatik.ca.CADataChangedEvent)
	 */
	public void selectionChanged(CADataChangedEvent e) {
		if (updatingCASelection) {
			return;
		}
		System.out.println("ca data selection changed");
		CAData[] data = e.getData();
		updatingSequenceSelection = true;
		updatingAlignmentSelection = true;
		sequenceSelectionModel.setValueIsAdjusting(true);
		alignmentSelectionModel.begin();
		if ( e.getType() == CADataChangedEvent.INSERTED ) {
			for ( int i = 0; i < data.length; i++ ) {
				if ( data[i].getDataSetIdentifier() == CADataModel.FIRST_DATA_SET && !sequenceSelectionModel.isSelectedIndex( data[i].getPosition() ) ) {
					sequenceSelectionModel.addSelectionInterval( data[i].getPosition(), data[i].getPosition());
				} else if (data[i].getDataSetIdentifier()==CADataModel.SECOND_DATA_SET) {
					selectInAlignment(data[i].getPosition(), true);
				}
			}
		}
		else {
			for ( int i = 0; i < data.length; i++ ) {
				if ( data[i].getDataSetIdentifier() == CADataModel.FIRST_DATA_SET && sequenceSelectionModel.isSelectedIndex( data[i].getPosition() ) ) {
					sequenceSelectionModel.removeSelectionInterval( data[i].getPosition(), data[i].getPosition() );
				} else if (data[i].getDataSetIdentifier()==CADataModel.SECOND_DATA_SET) {
					selectInAlignment(data[i].getPosition(), false);
				}
			}
		}
		alignmentSelectionModel.end();
		sequenceSelectionModel.setValueIsAdjusting(false);
		updatingSequenceSelection = false;
		updatingAlignmentSelection = false;
	}

	private void selectInAlignment(int dataPosition, boolean select) {
		int column = fisherScores.columnToHMMPosition(dataPosition);
		Symbol sym = fisherScores.getSymbolForColumn(dataPosition);
		String textSymbol="-";
		try {
			textSymbol = fisherScores.getAlphabet().getTokenization("token").tokenizeSymbol(sym).toUpperCase();
		} catch (BioException ex) {
			Logger.getLogger(AlignmentToCAPositionMapper.class.getName()).log(Level.SEVERE, null, ex);
		}
		column = getMAColumn(column+1);
		int row=0;
		for (Sequence s:alignment) {
			if (s.charAt(column)==textSymbol.charAt(0)) {
				if (select) {
					alignmentSelectionModel.select(row, column, false);
				} else {
					alignmentSelectionModel.deselect(row, column);
				}
			}
			row++;
		}
	}
	
	/* (non-Javadoc)
	 * @see javax.swing.event.ListSelectionListener#valueChanged(javax.swing.event.ListSelectionEvent)
	 */
	public void valueChanged( ListSelectionEvent e ) {
		if (updatingSequenceSelection) {
			return;
		}
		if (!e.getValueIsAdjusting()) {
			System.out.println("sequence selection changed");
			ListSelectionModel listModel = (ListSelectionModel)e.getSource();
			int[] selectedIndices = GUIutils.getSelectedIndices(listModel);
			int[] ids = new int[selectedIndices.length];
			for (int i=0;i<selectedIndices.length;i++) {
				ids[i] = CADataModel.FIRST_DATA_SET;
			}
			
			updatingCASelection = true;
			caModel.beginSelectionTransaction();
			CAData[] data = caModel.getDataSet( CADataModel.FIRST_DATA_SET );
			for ( int i = e.getFirstIndex(); i <= e.getLastIndex(); i++ ) {
				if ( data[ i].isSelected() != listModel.isSelectedIndex( data[i].getPosition() ) ) {
					if (listModel.isSelectedIndex( data[i].getPosition() )) {
						data[ i ].select();
					} else {
						data[ i ].deselect();
					}
				}
			}
			caModel.commitSelectionTransaction();
			updatingCASelection = false;
			
		}
	}

	public int getHMMPosition(int maPosition) {
		int count = 1;
		if (!columnMask[maPosition])
			return -1;
		for ( int i = 0; i < maPosition; i++) {
			if ( columnMask[i])
				count++;
		}
		
		return count;
	}

	public int getMAColumn(int hmmPosition) {
		int count = 0;
		while ( hmmPosition > 0) {
			if ( columnMask[count])
				hmmPosition--;
			count++;
		}
		return count-1;
	}

	public void selectionTransactionDidCommit() {
		// TODO Auto-generated method stub
		
	}

	public void selectionTransactionWillBegin() {
		// TODO Auto-generated method stub
		
	}

	// AlignmentSelectionListener interface
	@Override
	public void selectionChanged(AlignmentSelectionEvent e) {
		if (updatingAlignmentSelection) {
			return;
		}
		System.out.println("alignment selection changed");
		alignmentSelectionChangedArea.add(new Area(e.getRectangle()));
		
	}

	@Override
	public void selectionWillBegin() {
		System.out.println("alignment selection begin");
		alignmentSelectionChangedArea = new Area();
	}

	@Override
	public void selectionDidEnd() {
		Map<Symbol, Boolean> flag = new MapDefaultValueDecorator<>(new HashMap<Symbol, Boolean>(), false);
		System.out.println("alignment selection end");
		updatingCASelection = true;
		caModel.beginSelectionTransaction();
		Rectangle sel = alignmentSelectionChangedArea.getBounds();
		for (int col=(int)sel.getMinX();col<sel.getMaxX();col++) {
			for (int row=(int)sel.getMinY();row<sel.getMaxY();row++) {
				String textSymbol = alignment.characterAt(row, col).toString();
				Symbol sym;
				try {
					sym = fisherScores.getAlphabet().getTokenization("token").parseToken(textSymbol);
				} catch (BioException ex) {
					sym = null;
				}
				if (sym!=null && !sym.equals(fisherScores.getAlphabet().getGapSymbol())) {
					int pos = getHMMPosition(col);
					if (pos>0) {
						pos = fisherScores.hmmPositionToColumn(pos-1, sym);
						if (alignmentSelectionModel.isSelected(row, col, false)) {
							caModel.select(CADataModel.SECOND_DATA_SET, pos);
							flag.put(sym, true);
						} else {
							if (!flag.get(sym)) {
								caModel.removeSelection(CADataModel.SECOND_DATA_SET, pos);
							}
						}
					}
				}
			}
		}
		caModel.commitSelectionTransaction();
		updatingCASelection = false;
	}
}
