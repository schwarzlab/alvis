/*
 * Created on 19.05.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package de.biozentrum.bioinformatik.ca.ext.plot;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.font.FontRenderContext;
import java.awt.font.TextLayout;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RoundRectangle2D;
import java.awt.image.BufferedImage;
import java.util.HashMap;

import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.entity.EntityCollection;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.labels.XYItemLabelGenerator;
import org.jfree.chart.plot.CrosshairState;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.PlotRenderingInfo;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.AbstractXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRendererState;
import org.jfree.data.xy.XYDataset;
import org.jfree.ui.RectangleEdge;
import org.jfree.util.ShapeUtilities;

/**
 * @author pseibel
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class CAItemRenderer extends AbstractXYItemRenderer {

	private static BufferedImage[] bufferedImages;
	
	private static void createBuffer( Color c, Font f) { 
		bufferedImages = new BufferedImage[5];
		
		BufferedImage dummy = new BufferedImage(1,1,BufferedImage.TYPE_4BYTE_ABGR);
		Graphics2D g2Dummy = (Graphics2D)dummy.getGraphics();
		
		StringBuffer b = new StringBuffer();
		b.append("A , 7");
		for ( int i = 0; i < 5; i++) 
		{
			
			
			Font labelFont = f;
			
			FontRenderContext frc = g2Dummy.getFontRenderContext();
			
			String label = b.toString();

			
		    TextLayout layout = new TextLayout( label, labelFont,frc);
			
			Rectangle2D textBounds = layout.getBounds();
			
			double height = textBounds.getHeight() + 4;
			double width = textBounds.getWidth() + height + 4;
			Shape shape = null;

			width = textBounds.getWidth() + height + 4;
			shape = new RoundRectangle2D.Double( 0, 0, width, height, height, height );
			
			BufferedImage img = new BufferedImage((int)width, (int)height, BufferedImage.TYPE_4BYTE_ABGR);
			Graphics2D g2 = (Graphics2D)img.getGraphics();
			
			GradientPaint paint = new GradientPaint(0, 0, Color.WHITE, 0, (float) height, c, false);
			
			g2.setPaint( paint );
			g2.fill( shape );
			g2.setColor( c);
			g2.draw( shape );
			

			bufferedImages[i] = img;
			
			b.append("7");
		}
		
	}
	
	private HashMap seriesColors = new HashMap();
	
	private int intSeries = -1;
	private Color fromColor;
	private Color toColor;
	private int maximum;

	/* (non-Javadoc)
	 * @see org.jfree.chart.renderer.xy.XYItemRenderer#drawItem(java.awt.Graphics2D, org.jfree.chart.renderer.xy.XYItemRendererState, java.awt.geom.Rectangle2D, org.jfree.chart.plot.PlotRenderingInfo, org.jfree.chart.plot.XYPlot, org.jfree.chart.axis.ValueAxis, org.jfree.chart.axis.ValueAxis, org.jfree.data.xy.XYDataset, int, int, org.jfree.chart.plot.CrosshairState, int)
	 */
	public void drawItem(Graphics2D g2, XYItemRendererState state,
			Rectangle2D dataArea, PlotRenderingInfo info, XYPlot plot,
			ValueAxis domainAxis, ValueAxis rangeAxis, XYDataset dataset, int series, int item,
			CrosshairState crosshairState, int pass) {
		
		EntityCollection entities = null;
		if ( info != null ) {
			entities = info.getOwner().getEntityCollection();
		}
		
		double x1 = dataset.getXValue( series, item );
		double y1 = dataset.getYValue( series, item );
		
		if ( Double.isNaN(x1) || Double.isNaN(y1) ) {
			return;
		}
		
		RectangleEdge xAxisLocation = plot.getDomainAxisEdge();
		RectangleEdge yAxisLocation = plot.getRangeAxisEdge();
		
		double transX1 = domainAxis.valueToJava2D( x1, dataArea, xAxisLocation );
		double transY1 = rangeAxis.valueToJava2D( y1, dataArea, yAxisLocation );

		Shape shape = getSeriesShape( series );
		
		Shape translatedShape = null;
		if ( plot.getOrientation() == PlotOrientation.HORIZONTAL ) {
			translatedShape = ShapeUtilities.createTranslatedShape( shape, transY1, transX1 );
		}
		else {
			translatedShape = ShapeUtilities.createTranslatedShape( shape, transX1, transY1 );
		}
		
		if ( translatedShape.intersects( dataArea ) ) {
			if ( series == intSeries) {
				XYItemLabelGenerator generator = getItemLabelGenerator(series, item);
				if ( generator != null) {
					String label = generator.generateLabel( dataset, series, item);
					int pos = Integer.valueOf(label.split(",")[1].trim());
					g2.setColor( getInterpolatedColor(pos));
				}
				else 
				{
					g2.setColor( getSeriesColor( series ) );
					g2.setStroke( new BasicStroke(3));//(  getSeriesColor( series ) );
				}
			}
			else {
				g2.setColor( getSeriesColor( series ) );
			}
			if ( translatedShape instanceof Area) 
			{
				g2.draw( translatedShape );
			}
			else {
				g2.fill( translatedShape );
			}
		}
		
		if ( isItemLabelVisible( series, item ) ) {
			drawItemLabel( g2, plot.getOrientation(), dataset, series, item, transX1, transY1, (y1 < 0.0));
		}
		
		addEntity( entities, translatedShape, dataset, series, item, transX1, transY1 );
	}
	
	protected void drawItemLabel( Graphics2D g2, PlotOrientation orientation, XYDataset dataset, int series, int item, double x, double y, boolean negative ) {
		XYItemLabelGenerator generator = getItemLabelGenerator( series, item );
		if ( generator != null ) {
			
			String label = generator.generateLabel( dataset, series, item);
			ItemLabelPosition position = null;
			if ( !negative ) {
				position = getPositiveItemLabelPosition( series, item);
			}
			else {
				position = getNegativeItemLabelPosition( series, item);
			}
			
			Point2D anchorPoint = calculateLabelAnchorPoint( position.getItemLabelAnchor(), x, y, orientation );
			
			
			Font labelFont = getItemLabelFont( series, item);
			g2.setFont( labelFont );
			
			if ( bufferedImages == null && series == 1) 
			{
				createBuffer(getSeriesColor( series ), labelFont);
				
			}
			
			double height = 0;
			double width = 0;
			double textX = 0;
			double textY = 0;
			Shape shape = null;
			if ( series == 1 ) { // site labels
				
				int imgWidth=bufferedImages[4].getWidth();
				int imgHeight=bufferedImages[4].getHeight();
				
				int textWidth=g2.getFontMetrics().stringWidth(label);
				int textHeight=g2.getFontMetrics().getMaxAscent();
				
				textX = (imgWidth / 2) - (textWidth / 2);
				textY = (imgHeight /2) + (textHeight / 2);
				} else { // species labels
				
				int textWidth=g2.getFontMetrics().stringWidth(label);
				int textHeight=g2.getFontMetrics().getMaxAscent();
				
				height = textHeight + 4;
				width = textWidth + 8;
				
				textX = 4;
				textY = height - 2;
				shape = new Rectangle2D.Double( 0, 0, width, height );
			}

			
			AffineTransform oldTransform = g2.getTransform();
			g2.translate( anchorPoint.getX(), anchorPoint.getY() );
			if (series == 0) { // species labels
				GradientPaint paint = new GradientPaint(0, 0, Color.WHITE, 0, (float) height, getSeriesColor( series ), false);
				g2.setPaint( paint );
				g2.fill( shape );
				g2.setStroke(new BasicStroke());
				g2.setColor( getSeriesColor( series ) );
				g2.draw( shape );
			}
			else // site labels
			{
				g2.drawImage(bufferedImages[4], null, 0, 0);
				
			}
			g2.setColor( Color.BLACK );
			g2.drawString( label, (int)textX, (int)textY);
			g2.setTransform( oldTransform );
		}
	}
	
	public void setInterpolation( int series, Color from, Color to, int max)
	{
		intSeries = series;
		fromColor = from;
		toColor = to;
		maximum = max;
	}
	
	private Color getInterpolatedColor( int value) 
	{
		double percentage = (double)value/(double)maximum;
		
		int red = (int)(fromColor.getRed() - ((fromColor.getRed() - toColor.getRed()) * percentage));
		int green = (int)(fromColor.getGreen() - ((fromColor.getGreen() - toColor.getGreen()) * percentage));
		int blue = (int)(fromColor.getBlue() - ((fromColor.getBlue() - toColor.getBlue()) * percentage));
		
		return new Color( red, green, blue);
	}
	
	public void setSeriesColor( int series, Color color ) {
		seriesColors.put( new Integer( series), color );
	}
	
	public Color getSeriesColor( int series ) {
		return (Color)seriesColors.get( new Integer( series ) );
	}

}
