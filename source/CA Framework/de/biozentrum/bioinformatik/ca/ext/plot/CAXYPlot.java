package de.biozentrum.bioinformatik.ca.ext.plot;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Paint;
import java.awt.Shape;
import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.batik.dom.GenericDocument;
import org.apache.batik.dom.svg.SVGDOMImplementation;
import org.apache.batik.svggen.SVGGraphics2D;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.labels.XYItemLabelGenerator;
import org.jfree.chart.labels.XYToolTipGenerator;
import org.jfree.chart.plot.DatasetRenderingOrder;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.PlotRenderingInfo;
import org.jfree.chart.plot.PlotState;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.data.general.DatasetChangeEvent;
import org.jfree.data.xy.AbstractXYDataset;
import org.jfree.data.xy.XYDataset;

import de.biozentrum.bioinformatik.ca.CAComponentChangedEvent;
import de.biozentrum.bioinformatik.ca.CAComponentListener;
import de.biozentrum.bioinformatik.ca.CAData;
import de.biozentrum.bioinformatik.ca.CADataChangedEvent;
import de.biozentrum.bioinformatik.ca.CADataListener;
import de.biozentrum.bioinformatik.ca.CADataModel;
import de.biozentrum.bioinformatik.ca.annotation.CAAnnotation;
import de.biozentrum.bioinformatik.ca.annotation.CAAnnotationProvider;

public class CAXYPlot extends XYPlot {
	
	private CADataModel model;
	private double plotBufferWidth;
	private double plotBufferHeight;
	
	private SelectedItemRenderer selectionRenderer;
	
	public CAXYPlot( CADataModel model) {
		this.model = model;
		
		this.setDomainPannable(true);
		this.setRangePannable(true);
		
		DS dataset = new DS(false);
		DS selection = new DS(true);
		
		NumberAxis xAxis = new NumberAxis(null);
        NumberAxis yAxis = new NumberAxis(null);
     
        Shape firstShape = new Ellipse2D.Double(-3,-3,6,6);

		Area a = new Area( new Rectangle2D.Double( -3, -0.5, 6, 1));
		a.add( new Area( new Rectangle2D.Double(-0.5, -3, 1, 6)));
		Shape secondShape=a;

		
		ItemRenderer renderer = new ItemRenderer(StandardXYItemRenderer.SHAPES);
		renderer.setSeriesShape(0, firstShape);
		renderer.setSeriesShape(1, secondShape);
		renderer.setToolTipGenerator((XYToolTipGenerator)dataset);
        
		selectionRenderer = new SelectedItemRenderer(StandardXYItemRenderer.SHAPES);
		selectionRenderer.setSeriesShape(0, firstShape);
		selectionRenderer.setSeriesShape(1, secondShape);
		selectionRenderer.setToolTipGenerator((XYToolTipGenerator)selection);
		selectionRenderer.setBaseItemLabelGenerator(selection);
		
		
		this.setDomainAxis( xAxis);
        this.setRangeAxis(yAxis);
        this.setDataset( dataset);
        this.setDataset(1, selection);
        this.setRenderer( renderer);
        this.setRenderer(1, selectionRenderer);
        this.setDatasetRenderingOrder(DatasetRenderingOrder.FORWARD);
        this.setOrientation(PlotOrientation.VERTICAL);
	}
	
	public void showLabels( boolean labels) {
		selectionRenderer.setBaseItemLabelsVisible( labels);
	}
	
	public boolean showsLabels() {
		return selectionRenderer.getBaseItemLabelsVisible();
	}
	
	public void writeSVG( OutputStream outStream, PlotRenderingInfo renderingInfo, Dimension size ) throws ParserConfigurationException, IOException  {
		GenericDocument doc = new GenericDocument( null, new SVGDOMImplementation() );
		SVGGraphics2D g = new SVGGraphics2D( doc );
		Rectangle2D bufferArea = new Rectangle2D.Double(0, 0,
				size.width, size.height);
		this.draw(g, bufferArea, null, new PlotState(), renderingInfo);
		Writer outWriter = new OutputStreamWriter(outStream, "UTF-8");
		g.stream( outWriter );
	}
	
	private class DS extends AbstractXYDataset implements XYItemLabelGenerator, CADataListener, CAComponentListener, XYToolTipGenerator {

		private boolean selectionOnly;
		private boolean inSelectionTransaction;
		
		public DS( boolean selectionOnly) {
			super();
			this.selectionOnly = selectionOnly;
			model.addCADataListener(this);
			model.addCAComponentListener(this);
		}
		@Override
		public int getSeriesCount() {
			return 2;
		}

		@Override
		public Comparable getSeriesKey(int series) {
			return new Integer( series);
		}

		public int getItemCount(int series) {
			if ( selectionOnly) {
				return model.getSelectedData(series).length;
			}
			return model.getDataSet(series).length;
		}

		public Number getX(int series, int item) {
			if ( selectionOnly) {
				return model.getSelectedData(series)[item].getValues()[0];
			}
			return model.getData(series, item).getValues()[0];
		}

		public Number getY(int series, int item) {
			if ( selectionOnly) {
				return model.getSelectedData(series)[item].getValues()[1];
			}
			return model.getData(series, item).getValues()[1];
		}
		
		public void notifyListeners() {
			if ( selectionOnly) {
				this.notifyListeners( new DatasetChangeEvent(this, this));
			}
		}
		
		public void selectionChanged(CADataChangedEvent e) {
			if (inSelectionTransaction) return;
			notifyListeners();
		}
		
		public void componentsChanged(CAComponentChangedEvent e) {
			this.notifyListeners( new DatasetChangeEvent(this, this));
		}
		
		public String generateToolTip(XYDataset data, int series, int item) {
			CAAnnotationProvider annotation = model.getAnnotation();
			CAData d = null;
			
			if ( selectionOnly) {
				d = model.getSelectedData(series)[item];
			}
			else {
				d = model.getDataSet(series)[item];
			}
			return (String) annotation.getAnnotation(d, CAAnnotation.NAME);
		}
		
		public String generateLabel(XYDataset dataset, int series, int item) {	
			if ( selectionOnly) {
				return generateToolTip( dataset, series, item );
			}
			else {
				return null;
			}
		}
		public void selectionTransactionDidCommit() {
			notifyListeners();
			inSelectionTransaction = false;
			
		}
		public void selectionTransactionWillBegin() {
			inSelectionTransaction = true;
			
		}
		
	}
	
	private class SelectedItemRenderer  extends StandardXYItemRenderer {
		
		public SelectedItemRenderer(int shapes) {
			super( shapes);
		}

		public Paint getItemPaint( int series, int item) {
			return Color.RED;
		}
		

		/*protected void drawItemLabel( Graphics2D g2, PlotOrientation orientation, XYDataset dataset, int series, int item, double x, double y, boolean negative ) {
			XYItemLabelGenerator generator = getItemLabelGenerator( series, item );
			if ( generator != null ) {
				
				String label = generator.generateLabel( dataset, series, item);
				ItemLabelPosition position = null;
				if ( !negative ) {
					position = getPositiveItemLabelPosition( series, item);
				}
				else {
					position = getNegativeItemLabelPosition( series, item);
				}
				
				Point2D anchorPoint = calculateLabelAnchorPoint( position.getItemLabelAnchor(), x, y, orientation );
				
				
				Font labelFont = getItemLabelFont( series, item);
				g2.setFont( labelFont );
				
				
				double height = 0;
				double width = 0;
				double textX = 0;
				double textY = 0;
				Shape shape = null;
				
				int textWidth=g2.getFontMetrics().stringWidth(label);
				int textHeight=g2.getFontMetrics().getMaxAscent();
					
				height = textHeight + 4;
				width = textWidth + 8;
					
				textX = 4;
				textY = height - 2;
				shape = new Rectangle2D.Double( 0, 0, width, height );
				

				
				AffineTransform oldTransform = g2.getTransform();
				g2.translate( anchorPoint.getX(), anchorPoint.getY() );
				g2.setPaint( Color.WHITE );
				g2.fill( shape );
				
				g2.setColor( Color.BLACK );
				g2.drawString( label, (int)textX, (int)textY);
				g2.setTransform( oldTransform );
			}
		}*/
		
	}
	
	private class ItemRenderer extends StandardXYItemRenderer {
		
		public ItemRenderer(int shapes) {
			super( shapes);
		}

		public Paint getItemPaint( int series, int item) {
			return (series == 0) ? Color.GREEN : Color.BLUE;
		}
		
		
		
	}
}
