/*
 * Created on 02.02.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package de.biozentrum.bioinformatik.ca.ext.plot;

import java.awt.FlowLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JComboBox;
import javax.swing.JComponent;

import de.biozentrum.bioinformatik.ca.CADataModel;
import de.biozentrum.bioinformatik.ca.ext.CAExtension;
import org.jblas.DoubleMatrix;

/**
 * @author binf024
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class CA2DComponentChooser extends JComponent implements ItemListener,CAExtension {
	protected CADataModel model;
	protected JComboBox menuA;
	protected JComboBox menuB;
	
	
	
	public CA2DComponentChooser() {
		setLayout( new FlowLayout() );	
	}
	
	protected JComboBox generateMenu( String label, int itemcount, DoubleMatrix information ) {
		JComboBox menu = new JComboBox();
		
		for ( int i = 0; i < itemcount; i++ ) {
			menu.addItem( String.valueOf( i + 1  ) + " ( " + (int)(information.get(i) *100) + "% )"  );
		}
		
		return menu;
	}

	/* (non-Javadoc)
	 * @see java.awt.event.ItemListener#itemStateChanged(java.awt.event.ItemEvent)
	 */
	public void itemStateChanged(ItemEvent e) {
		int idxA = menuA.getSelectedIndex();
		int idxB = menuB.getSelectedIndex();
		model.setComponents( new int[] { idxA, idxB } );
	}

	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.ca.ext.CAExtension#setModel(de.biozentrum.bioinformatik.ca.CADataModel)
	 */
	public void setModel(CADataModel model) {
		this.model = model;
		
		this.removeAll();
		
		int components = model.numberOfComponents();
		
		
		menuA = generateMenu( "x Achse", components, model.getComponentInformations() );
		menuB = generateMenu( "y Achse", components, model.getComponentInformations() );
		
		menuB.setSelectedIndex( 1 );
		
		menuA.addItemListener( this );
		menuB.addItemListener( this );
		
		add( menuA );
		add( menuB );
		
		revalidate();
	}

	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.ca.ext.CAExtension#getComponent()
	 */
	public JComponent getComponent() {
		return this;
	}
	
	public String getName() {
		return "Component Chooser";
	}
	
}
