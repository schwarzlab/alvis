/*
 * Created on 16.05.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package de.biozentrum.bioinformatik.ca.ext.plot;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JToolBar;

import de.biozentrum.bioinformatik.ca.CADataModel;
import de.biozentrum.bioinformatik.ca.ext.CAExtension;

/**
 * @author pseibel
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class CAPlotToolbar extends JToolBar implements CAExtension, ActionListener {
	
	//private CAPlot plot;
	
	private CA2DComponentChooser componentChooser;
	private CAChartPanel chartPanel;
	private CADataModel model;
	
	private JComboBox cmbScaling=new JComboBox(CADataModel.ScalingOptions.values());
	
	public CAPlotToolbar( CAChartPanel chartPanel ) {
		this.componentChooser = new CA2DComponentChooser();	
		this.chartPanel = chartPanel;

		JButton allData = new JButton("show all");
	

	
		allData.addActionListener( this );

		cmbScaling.setEditable(false);
		cmbScaling.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				model.setScaling((CADataModel.ScalingOptions)cmbScaling.getSelectedItem());
			}
		});
		
		this.add( allData );
		this.add( componentChooser );
		JComponent c = new JPanel();
		c.setLayout( new FlowLayout());
		c.add( cmbScaling);
		this.add(c);
		
	}

	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.ca.ext.CAExtension#setModel(de.biozentrum.bioinformatik.ca.CADataModel)
	 */
	public void setModel(CADataModel model) {
		this.model = model;
		componentChooser.setModel( model );
	}
	

	public String getName() {
		return "CAPlotToolbar";
	}
	
	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.ca.ext.CAExtension#getComponent()
	 */
	public JComponent getComponent() {
		return this;
	}
	
	private ImageIcon loadIcon( String filename ) {
		java.net.URL imageURL = CAPlotToolbar.class.getResource("images/" + filename);
		ImageIcon icon = null;
		if (imageURL != null) {
		    icon = new ImageIcon(imageURL);
		}
		return icon;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent e) {
		chartPanel.restoreAutoBounds();
	}
}
