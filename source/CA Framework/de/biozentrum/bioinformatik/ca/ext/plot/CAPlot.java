/*
 * Created on 28.04.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package de.biozentrum.bioinformatik.ca.ext.plot;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;

import javax.swing.JLayeredPane;
import javax.swing.JPanel;

import org.jfree.chart.ChartRenderingInfo;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.encoders.ImageEncoder;
import org.jfree.chart.encoders.ImageEncoderFactory;
import org.jfree.chart.entity.ChartEntity;
import org.jfree.chart.entity.EntityCollection;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.xy.XYDataset;
import org.jfree.ui.RectangleEdge;

import de.biozentrum.bioinformatik.ca.CAData;
import de.biozentrum.bioinformatik.ca.CADataModel;


/**
 * @author pseibel
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public class CAPlot extends JLayeredPane {

	public static final int ZOOM_MODE = 0;
	public static final int SELECT_MODE = 1;
	public static final int MOVE_MODE = 2;
	
	private int mouseMode;
	private XYPlot plot;
	private ChartRenderingInfo info;
	private int plotBufferWidth = 0;
	private int plotBufferHeight = 0;
	private CADataModel model = null;
	private CAChartPanel panel;
	private Point2D lastMousePos;
	private Rectangle2D selectionRect;
	private boolean drawSelectionRect=false;
	private SelectionPanel selectionPanel;
	private Dimension lastSize;
		
	public void setModel( CADataModel model ) {
		this.model = model;
		//XYDataset dataset = new XYDataSetAdapter(model);

		

		this.panel.setDataModel(model);
		
		
		
		
		

		/**/
		
		
	}
	
	public CADataModel getModel() {return model;}
	
	 /**
     * Creates a chart.
     * 
     * @param dataset the data for the chart.
     * @return a chart.
     */
    private void configurePlot(XYDataset dataset, XYDataset selection)
    {

    	
    }
	
	public CAPlot() {
		super();
		selectionPanel = new SelectionPanel();

		plot = new XYPlot();
        
		/*this.panel.addMouseWheelListener(new MouseWheelListener() {

		    public void mouseWheelMoved(MouseWheelEvent e) {
		        if (e.getScrollType() != MouseWheelEvent.WHEEL_UNIT_SCROLL) return;
		        if (e.getWheelRotation()< 0) increaseZoom((ChartPanel)e.getComponent(), true);
		        else                          decreaseZoom((ChartPanel)e.getComponent(), true);
		    }
		   
		    public synchronized void increaseZoom(JComponent chart, boolean saveAction){
		        ChartPanel ch = (ChartPanel)chart;
		        zoomChartAxis(ch, true);
		    } 
		   
		    public synchronized void decreaseZoom(JComponent chart, boolean saveAction){
		        ChartPanel ch = (ChartPanel)chart;
		        zoomChartAxis(ch, false);
		    } 
		   
		    private void zoomChartAxis(ChartPanel chartP, boolean increase){              
		        int width = chartP.getMaximumDrawWidth() - chartP.getMinimumDrawWidth();
		        int height = chartP.getMaximumDrawHeight() - chartP.getMinimumDrawWidth();       
		        if(increase){
		           chartP.zoomInBoth(width/2, height/2);
		        }else{
		           chartP.zoomOutBoth(width/2, height/2);
		        }

		    }
		});*/
		
		this.add(panel, JLayeredPane.DEFAULT_LAYER);
		this.add(selectionPanel, JLayeredPane.DRAG_LAYER);

		this.lastSize=getSize();
		
	}
	
	public static void writeBufferedImage(BufferedImage image, String format, 
	        OutputStream outputStream) throws IOException {
	        ImageEncoder imageEncoder = ImageEncoderFactory.newInstance(format);
	        imageEncoder.encode(image, outputStream);
	}

	public void showAllData() {
		this.panel.restoreAutoBounds();
	}

	public Dimension getPreferredSize() {
		return new Dimension(300,300);
	}

	public void moveGraph(Point p) {
		if (lastMousePos!=null) {
			ValueAxis domainAxis=plot.getDomainAxis();
			ValueAxis rangeAxis=plot.getRangeAxis();

			double domainValueNew=domainAxis.java2DToValue(p.getX(), panel.getScreenDataArea(), plot.getDomainAxisEdge());
			double domainValueOld=domainAxis.java2DToValue(lastMousePos.getX(), panel.getScreenDataArea(), plot.getDomainAxisEdge());
			double domainOffset=domainValueOld-domainValueNew;

			double rangeValueNew=rangeAxis.java2DToValue(p.getY(), panel.getScreenDataArea(), plot.getRangeAxisEdge());
			double rangeValueOld=rangeAxis.java2DToValue(lastMousePos.getY(), panel.getScreenDataArea(), plot.getRangeAxisEdge());
			double rangeOffset=rangeValueOld-rangeValueNew;

			domainAxis.setRange(domainAxis.getRange().getLowerBound()+domainOffset, domainAxis.getRange().getUpperBound()+domainOffset);
			rangeAxis.setRange(rangeAxis.getRange().getLowerBound()+rangeOffset, rangeAxis.getRange().getUpperBound()+rangeOffset);
		}
	}

	protected void setSelectionRectangle(MouseEvent e) {
		double width=e.getX()-lastMousePos.getX();
		double height=e.getY()-lastMousePos.getY();
		
		double originX=lastMousePos.getX();
		double originY=lastMousePos.getY();
		if (width<0) {
			width=Math.abs(width);
			originX-=width;
		}
		if (height<0) {
			height=Math.abs(height);
			originY-=height;
		}
		
		selectionRect=new Rectangle2D.Double(originX, originY, width, height);
		selectionPanel.repaint();
	}

	protected void selectItems() {
		//this.panel.select(selectionRect);
		/*Rectangle2D dataArea=panel.getScreenDataArea();
		RectangleEdge xAxisLocation = plot.getDomainAxisEdge();
		RectangleEdge yAxisLocation = plot.getRangeAxisEdge();
		
		double minX1 = plot.getDomainAxis().java2DToValue( selectionRect.getMinX(), dataArea, xAxisLocation );
		double minY1 =  plot.getRangeAxis().java2DToValue( selectionRect.getMinY(), dataArea, yAxisLocation );

		double maxX1 =  plot.getDomainAxis().java2DToValue( selectionRect.getMaxX(), dataArea, xAxisLocation );
		double maxY1 =  plot.getRangeAxis().java2DToValue( selectionRect.getMaxY(), dataArea, yAxisLocation );

		double minX2 = Math.min( minX1, maxX1 );
		double maxX2 = Math.max( minX1, maxX1 );
		
		double minY2 = Math.min( minY1, maxY1 );
		double maxY2 = Math.max( minY1, maxY1 );
		
		Rectangle2D rect = new Rectangle2D.Double( minX2, minY2, maxX2 - minX2, maxY2-minY2);
		
		model.select(getDataInRect( CADataModel.FIRST_DATA_SET, rect ) );
		model.select(getDataInRect( CADataModel.SECOND_DATA_SET, rect ) );*/

		selectionPanel.repaint();
	}
	
	protected void deselectItems() {
		model.clearSelection();
		selectionPanel.repaint();
	}
	
	public CAData[] getDataInRect( int dataSetIdentifier, Rectangle2D rect ) {
		XYDataset dataset = plot.getDataset();
		ArrayList<CAData> list = new ArrayList<CAData>();
		for ( int i = 0; i < dataset.getItemCount( dataSetIdentifier ); i++ ) {
			
			double x = dataset.getXValue( dataSetIdentifier, i );
			double y = dataset.getYValue( dataSetIdentifier, i );
			if ( rect.contains( x, y ) ) {
				list.add( model.getData( dataSetIdentifier, i ) );
			}
		}
		return list.toArray( new CAData[0] );
	}

	public String getToolTipText(MouseEvent e) {

		String result = null;

		if (this.info != null) {
			EntityCollection entities = this.info.getEntityCollection();
			if (entities != null) {
				Insets insets = getInsets();

				ChartEntity entity = entities.getEntity(
						(int) ((e.getX() - insets.left)),
						(int) ((e.getY() - insets.top)));
				if (entity != null) {
					result = entity.getToolTipText();
				}
			}
		}

		return result;

	}

	

	private void resizePlot(Dimension newSize) {
		selectionPanel.setBounds( 0,0, newSize.width, newSize.height);
		
		ValueAxis rangeAxis = plot.getRangeAxis();
		ValueAxis domainAxis = plot.getDomainAxis();

		double translatedWidthChange=java2DToLength(newSize.getWidth()-lastSize.getWidth(), domainAxis, panel.getScreenDataArea(), plot.getDomainAxisEdge());
		double translatedHeightChange=java2DToLength(newSize.getHeight()-lastSize.getHeight(), domainAxis, panel.getScreenDataArea(), plot.getDomainAxisEdge());
			
		domainAxis.setRange( domainAxis.getLowerBound() - (translatedWidthChange / 2), domainAxis.getUpperBound() + (translatedWidthChange / 2));
		rangeAxis.setRange( rangeAxis.getLowerBound() - (translatedHeightChange / 2) , rangeAxis.getUpperBound() + (translatedHeightChange / 2));

		panel.setBounds(0,0, newSize.width, newSize.height);
	}

	private double java2DToLength(double length,ValueAxis axis, Rectangle2D area, RectangleEdge edge) {
		double retval=-1;
		
		double origin=axis.java2DToValue(0, area, edge);
		double dest=axis.java2DToValue(length, area, edge);
		
		retval=dest-origin;
		return retval;
	}
	
	

	/*
	private class XYDataSetAdapter extends XYSeriesCollection implements XYDataset, XYToolTipGenerator,
			CAComponentListener, XYItemLabelGenerator, CADataListener {

		private CADataModel model;

		public XYDataSetAdapter(CADataModel model) {
			super();
			this.model = model;
			updateSeries(true);
			
			
			this.model.addCADataListener(this);
			this.model.addCAComponentListener(this);
		}

		public void selectionChanged(CADataChangedEvent e) {
			notifyListeners( new DatasetChangeEvent(this, this));
		}
		
		private void updateSeries(boolean initialize) {
			
			if (!initialize) {
				this.removeAllSeries();
			}
			
			for ( int dataset = 0; dataset < 2; dataset++) {
				
				XYSeries series = new XYSeries( new Integer(dataset));
				for ( int item = 0; item < this.model.sizeOfDataSet(dataset); item++) {
					double x = this.model.getValue(dataset, item, this.model.getComponents()[0]);
					double y = this.model.getValue(dataset, item, this.model.getComponents()[1]);
					series.add(x, y);
				}
				this.addSeries(series);
				
			}
			
		}
		

		public DomainOrder getDomainOrder() {
			return DomainOrder.ASCENDING;
		}



		public String generateToolTip(XYDataset data, int series, int item) {
			CAAnnotationProvider annotation = model.getAnnotation();
			CAData d = model.getDataSet(series)[item];
			return (String) annotation.getAnnotation(d, CAAnnotation.NAME) + " : " + d.getPosition() + " | " + getXValue(series, item) + " " + getYValue(series, item);
			//return (String) annotation.getAnnotation(d, CAAnnotation.NAME)+ " " + d.identifier() + " " + d.isSelected();

		}

		public void componentsChanged(CAComponentChangedEvent e) {
			updateSeries(false);
			CAPlot.this.showAllData();
		}

		public String generateLabel(XYDataset dataset, int series, int item) {
			CAData d = model.getDataSet(series)[item];
			if ( d.isSelected()) {
				return generateToolTip( dataset, series, item );
			}
			else {
				return null;
			}
		}
	}
	*/
	

	
	private class SelectionPanel extends JPanel {
		
		private SelectionPanel() {
			setOpaque( false);
			setBackground( new Color( 0,0,0,0));
		}
		
		public void paintComponent( Graphics g) {
			super.paintComponent( g);
			if(drawSelectionRect) {
				Graphics2D g2=(Graphics2D)g;
				g2.setColor(new Color(10, 10, 10, 50));
				g2.fill(selectionRect);
				g2.setColor(Color.black);
				g2.draw(selectionRect);
			}
		}
	}
}
