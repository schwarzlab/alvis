/*
 * Created on 01.05.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package de.biozentrum.bioinformatik.ca.ext.plot;

import org.jfree.data.xy.XYDataset;

/**
 * @author pseibel
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface XYMarkableDataset extends XYDataset {
	public boolean isMarked( int series, int item );
}
