/*
 * Created on 19.01.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package de.biozentrum.bioinformatik.ca.ext.inspector;

import java.awt.BorderLayout;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PipedInputStream;
import java.util.Hashtable;

import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.text.EditorKit;
import javax.swing.text.html.HTMLDocument;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.w3c.dom.Element;

import de.biozentrum.bioinformatik.ca.CAData;
import de.biozentrum.bioinformatik.ca.CADataChangedEvent;
import de.biozentrum.bioinformatik.ca.CADataListener;
import de.biozentrum.bioinformatik.ca.CADataModel;
import de.biozentrum.bioinformatik.ca.ext.CAExtension;
import de.biozentrum.bioinformatik.ca.gui.BrowserLauncher;
import de.biozentrum.bioinformatik.ca.gui.CAProtocolHandler;

/**
 * @author binf024
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public class CADataInspector extends JComponent implements CAExtension,CADataListener {

	protected JEditorPane inspector;
	protected Hashtable protocolHandler;
	protected CADataModel model = null;
	protected CAData data = null;
	
	protected boolean inSelectionTransaction;
	
	public CADataInspector() {
		inspector = new JEditorPane();
		protocolHandler = new Hashtable();
		inspector.setEditable(false);
		inspector.addHyperlinkListener(new CustomHyperlinkListener());

		EditorKit htmlKit = inspector.getEditorKitForContentType( "text/html" );
		HTMLDocument doc = (HTMLDocument) htmlKit.createDefaultDocument();
		inspector.setEditorKit(htmlKit);

		setLayout(new BorderLayout());
		add(inspector, BorderLayout.CENTER);
	}
	
	public void registerProtocolHandler( CAProtocolHandler handler ) {
		protocolHandler.put( handler.protocol(), handler );
	}
	
	private void setData( CAData data ) {
		this.data = data;
		String html = "";
		
		/*try {
			html = transform( data.getXmlElement() );
		} catch (TransformerException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}*/
		
		setHTML( html );
	} 
	
	public void setHTML(String html) {

		inspector.setText(html);
	}

	public String transform(Element element) throws TransformerException,
			IOException {
		// Use a Transformer for output
		TransformerFactory tFactory = TransformerFactory.newInstance();
		StreamSource stylesource = new StreamSource(new File("data/data.xsl"));
		Transformer transformer = tFactory.newTransformer(stylesource);

		DOMSource source = new DOMSource(element);
		DOMResult dr = new DOMResult();

		PipedInputStream input = new PipedInputStream();

		ByteArrayOutputStream output = new ByteArrayOutputStream();
		StreamResult result = new StreamResult(output);
		transformer.transform(source, result);

		return ((ByteArrayOutputStream) result.getOutputStream()).toString();
	}

	class CustomHyperlinkListener implements HyperlinkListener {

		/*
		 * (non-Javadoc)
		 * 
		 * @see javax.swing.event.HyperlinkListener#hyperlinkUpdate(javax.swing.event.HyperlinkEvent)
		 */
		public void hyperlinkUpdate(HyperlinkEvent e) {
			if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
				String description = e.getDescription();
				String[] protocolSplit = description.split( ":\\/\\/" );
				if ( protocolHandler.get( protocolSplit[0] ) != null )
					((CAProtocolHandler)protocolHandler.get( protocolSplit[0] )).invoke( protocolSplit[1], data );
				else {
					try {
						if (e.getURL() != null)
							BrowserLauncher.openURL(e.getURL().toString());
						else {
						}
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				}
			}

		}
	}

	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.ca.ext.CAExtension#setModel(de.biozentrum.bioinformatik.ca.CADataModel)
	 */
	public void setModel(CADataModel model) {
		this.model = model;
	}

	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.ca.ext.CAExtension#getComponent()
	 */
	public JComponent getComponent() {
		return this;
	}

	
	public void selectionTransactionDidCommit() {
		updateData();
		inSelectionTransaction = false;
		
	}

	public void selectionTransactionWillBegin() {
		inSelectionTransaction = true;
		
	}
	
	public void updateData() {
		
		CAData[] selection = model.getSelectedData();
		
		if ( selection.length == 1 )
			setData( selection[0] );
		else if ( selection.length == 0)
			setData( null );
	}
	
	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.ca.CADataListener#selectionChanged(de.biozentrum.bioinformatik.ca.CADataChangedEvent)
	 */
	public void selectionChanged(CADataChangedEvent e) {
		if ( inSelectionTransaction) return;
		updateData();
	}

	/* (non-Javadoc)
	 * @see de.biozentrum.bioinformatik.ca.CADataListener#highlightingChanged(de.biozentrum.bioinformatik.ca.CADataChangedEvent)
	 */
	public void highlightingChanged(CADataChangedEvent e) {
	}

	
}