% -*-coding:latin-1-*-
\RequirePackage{fix-cm}

\documentclass[a4paper,notitlepage,abstracton]{scrartcl}
\pdfminorversion=4 % needed to ensure PDF version 1.4 for Journal tools
\usepackage{fixltx2e}
\usepackage{setspace}
\usepackage{lineno}
\usepackage{amsmath}
\usepackage{amsfonts}
%\usepackage{amsthm}
\usepackage{graphicx}
\usepackage{latexsym}
\usepackage[numbers,sort&compress]{natbib}
\citestyle{nature}
\usepackage{nomencl}
\usepackage{hyperref}
%\usepackage{svninfo}
\usepackage[latin1]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{textcomp}
\usepackage{eurosym}
\usepackage{upgreek}
\usepackage{setspace}
\usepackage{multirow}
%\usepackage[CaptionAfterwards]{fltpage}
\usepackage[colorinlistoftodos]{todonotes} %\usepackage[disable]{todonotes}
\usepackage{xr}

\makenomenclature
\let\nc\nomenclature

\newcommand{\piqe}{\left(\pi_0 \ Q \ e^{tQ}\right)_i}
\newcommand{\piqqe}{\left(\pi_0 \ Q^2 \ e^{tQ}\right)_i}
\newcommand{\pie}{\left(\pi_0 \ e^{tQ}\right)_i}
\newcommand{\suppapp}{Supplementary Appendix}

\newcommand{\field}[1]{\mathbb{#1}}
\newcommand{\C}{\field{C}}
\newcommand{\R}{\field{R}}
\newcommand{\N}{\field{N}}
\newcommand{\Q}{\field{Q}}
\newcommand{\A}{\ifmmode{\field{A}}      \else{$\field{A}$}\fi}
\renewcommand{\P}{\field{P}}
\newcommand{\g}[1]{\emph{#1}}

\DeclareMathOperator*{\argmax}{argmax}
\DeclareMathOperator*{\Var}{Var}
\DeclareMathOperator*{\modell}{Modell}
\DeclareMathOperator*{\rang}{rang}
\DeclareMathOperator*{\Mn}{\ifmmode{{\bf M(n;\R) }}\else{$\bf M(n;\R)  $}\fi}
\DeclareMathOperator*{\cof}{cof}
\DeclareMathOperator*{\adj}{Adj}
\DeclareMathOperator*{\tr}{tr}
\DeclareMathOperator*{\EMP}{{\boldmath EMP}}
\graphicspath{{figures/}}

%% florian added:
%\usepackage[usenames,dvipsnames]{xcolor}
\usepackage{color}
%\newcommand{\todo}[1]{\textbf{\color{red} #1}}
\newcommand{\fm}[1]{{\color{red} #1}}
\newcommand{\revised}[1]{{\color{blue} #1}}
\definecolor{gray}{rgb}{0.6,0.6,0.6}

%% Reviewer comments
\usepackage{paralist} \usepackage{framed} \usepackage{fourier}
\definecolor{shadecolor}{rgb}{0.85,0.95,1}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\title{Visualisation of multiple sequence alignments - Sequence
  Bundles in \emph{Alvis}}

\author{Roland F Schwarz, Nick Goldman}

\date{\today}

\setcapindent{0pt}

\begin{document}
\maketitle

%\doublespacing

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}
Sequence Bundles, as implemented in the software \emph{Alvis}, are a
novel method for visualisation of large multiple sequence alignments
(MSAs) \citep{Kultys2014}, in which each sequence is drawn as a
tranlucent bezier curve through a grid representing the sequence
alphabet along the alignment (Figure \ref{fig:bundle}). 

\begin{figure}[h!]
\centerline{\includegraphics[width=\textwidth]{bundle_proceedings}}
\caption{\label{fig:bundle}Sequence Bundle visualising the bacterial
  adenylate kinase Lid (AKL) domain across 1809 protein sequences of
  932 gram-negative (blue) and 886 gram-positive (black)
  bacteria. Bundled visualisation plots sequences as stacked lines
  against a Y-axis of letters arranged on a scale representing e.g.
  physiochemical properties. The lines' curved paths expose the
  conservation of residues by converging at matched positions.}
\end{figure}

The visualisation is non-aggregative,
i.e. it does not make use of summary statistics, and retains the full
information of the MSA in the visualisation. Therefore, and in
contrast to, traditional sequence logos \citep{Schneider1990} and more
recent variants, such as HMMLogos \citep{Schuster-Boeckler2004} and
pLogos \citep{OShea2013}, Sequence Bundles have the following
particular advantages:
\begin{enumerate}
\item \textbf{Direct visualisation of horizontal dependencies.} Information
  such as: ``All sequences that have a \textbf{A} at position $x$ also
  have a \textbf{C} at position $y$'' is immediately available from
  the visualisation.
\item \textbf{Direct visualisation of dependencies between sequence
    groups.} Because each individual sequence is fully represented in
  the Bundle, sequence groups can easily be compared and contrasted,
  allowing for example for the display of phylogenetic information.
\item \textbf{Interactivity.} The non-aggregative nature of the method
  enables a bi-directional mapping between the MSA and its
  visualisation. An interactive view, such as implemented in
  \emph{Alvis}, allows for detailed explorative analysis of MSAs.
\end{enumerate}

In the following we illustrate the power of the method on some
real-world examples.

\section{Examples}
\subsection{Mammalian haloacid dehalogenase (HAD)-type phosphatases \citep{Seifried2014}}
HAD-type phosphatases are a family of at least 40 human enzymes with
important functions in physiology and disease
\citep{Seifried2014}. Two paralogue members of this family, chronophin
and the aspartate-based, ubiquitous, Mg$^2+$-dependent phosphatase
(AUM) are of particular interest due to their differences in substrate
specificity: AUM acts on tyrosine, whereas chronophin dephosphorelates
serine/threonine-phosphorylated proteins.

AUM and chronophin evolved through gene duplication at the origin of
the vertebrates. In their paper \citeauthor{Seifried2014} applied
evolutionary and experimental analyses of these paralogues across the
mammals to identify sequence elements responsible for the differences
in substrate specificity. They ultimately reported that alignment
position 241, containing a Leu in AUM and a His in chronophin, is
responsible for the majority of difference in substrate
specificity. They showed experimentally that introduction of a His
residue at that position into AUM transfers chronophin-like substrate
recruitment onto AUM.

We visualised the original alignment of the paper, comprising 53
sequences over 367 sites across both paralogue groups as well as a
\emph{Ciona} outgroup (prior to duplication) using \emph{Alvis}. Sequences were
assigned to one of three groups: AUM (green), Chronophin (blue) and
\emph{Ciona} (orange). Sequence Bundle visualisation immediately shows
co-dependent sites (Figure \ref{fig:aum_bundle}). For example, all
\emph{Ciona} sequences have a Met in position 241 and also exclusively
have a Tyr residue in position 232 and a Val at position 249. This
information is not available from the classical sequence logo view
(Figure \ref{fig:aum_bundle}, below the Bundle).

\begin{figure}[h!]
\centerline{\includegraphics[width=\textwidth]{aum_bundle}}
\caption{\label{fig:aum_bundle}\emph{Alvis}' Sequence Bundle visualisation of
  the HAD family. The Bundle shows the different sequence groups in
  different color. Horizontal dependencies are immediately
  visible. For example, all \emph{Ciona} sequences have a Met in
  position 241 and also exclusively have a Tyr residue in position 232
  and a Val at position 249. This information is not available from
  the standard sequence logo below. Above the Bundle green shaded
  markers indicate which sites are most likely responsible for the
  grouping. Here, in agreement with the original paper, site 241
  (marked with an orange triangle) is found as being most
  significant.}
\end{figure}

\emph{Alvis} further automatically trains a profile-HMM \citep{Durbin1998} on
the underlying MSA and uses the Fisher scores
\citep{Jaakkola1999,Schwarz2009a} of the HMM as a numerical embedding
of sequences. This allows \emph{Alvis} to search for sites that distinguish
best between the three sequence groups. Green markers whose opacity
increases with significance display these results and indicate which
sites are most likely responsible for the grouping. Here, in agreement
with the original paper, site 241 (marked with an orange triangle) is
found as being most significant (Figure \ref{fig:bundle}, above
Bundle).

\subsection{Calmodulin-dependent protein kinase II (CaMKII) motif}
Recently \citeauthor{OShea2013} proposed \emph{pLogo}, a variant of a
conventional sequence logo that makes use of a probability model to
scale the height of the characters. Additionally it allows to ``fix''
specific residues, creating a logo for the remaining sites given the
one that was fixed. This partially overcomes the lack of a good
visualisation for horizontal dependencies in traditional sequence
logos, by giving the user the possibility to iteratively click through
each site, fix it and see how the remaining logo changes.

Figure \ref{fig:church_bundle} shows the CaMKII sequences taken from
\citep{OShea2013}. Three sites show a certain degree of conservation:
Site 5 dominantly shows an Arg residue, site 8 exclusively a Ser
residue and site 10 sometimes shows a certain enrichment for
Asp. \citeauthor{OShea2013} used the ``fixing'' of sites in
\emph{pLogo} to investigate whether the sequences that have an Asp at
site 10 are also the ones that have an Arg at site 5, and concluded
that they are not, i.e. that there is no dependency or correlation
between the amino acid distributions at sites 5 and 10.

By retaining individual sequence information and not using site-wise
averages or counts of residue frequencies Sequence Bundles immediately
visualise this result. Sequences that have an Asp residue at position
10 are marked in pink, the rest in blue. It is immediately evident
that at position 5 the pink group is widely spread, with seemingly no
residue being preferred over another.

\begin{figure}[h!]
\centerline{\includegraphics[width=.8\textwidth]{church_bundle}}
\caption{\label{fig:church_bundle}The immediate visualisation of
  horizontal dependencies is a powerful tool to quickly investigate an
  alignment for co-dependent sites. Here, the Asp-10 containing
  sequences showed no co-clustering at any of the other not fully
  conserved positions.}
\end{figure}

\section{Conclusion}
Sequence Bundles are a novel visualisation technique capable of
condensing the information of large MSAs with many thousands of
sequences into a visually appealing, coherent plot that retains
individual sequence information without appearing cluttered. Combined
with an interactive user interface it turns into a powerful tool for
explorative analyses of sequential data. It is worth pointing out that
it is not limited to traditional amino acid or nucleotide
sequences. \emph{Alvis} supports custom alphabets which can be used to
model SNP information or copy number data. Future work will
combine this visualisation with our previous algorithm for detecting
sequence-site dependencies in MSAs \citep{Schwarz2009a} towards an
integrated tool for explorative analyses of MSAs. \emph{Alvis} already
connects to a local R installation for certain advanced features. A
future R API will allow the user to push and pull data in and out of
\emph{Alvis} from their own R projects.

\clearpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\bibliographystyle{abbrvnat}
\bibliography{alvis_examples}

\end{document}
